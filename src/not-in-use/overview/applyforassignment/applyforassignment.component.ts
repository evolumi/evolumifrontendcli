import { Input, Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LocalStorage } from '../../../services/localstorage';
import { UserDetailModel } from '../../../models/user.models';
import { MarketplaceService } from '../../../services/marketplaceService';
import { LinkModel } from '../../../models/link.models';
declare var $: JQueryStatic;

@Component({
    selector: 'apply-for-assignment',
    templateUrl: './applyforassignment.html',
})

export class ApplyForAssignment {

    @Input() assignmentId: string;

    private _userdetails: UserDetailModel;

    public displayApply: boolean = false;
    public links: Array<LinkModel> = [];

    public newLink: LinkModel = new LinkModel("", "");
    public assignmentForm: FormGroup;
    public assignmentPersonal: string = '';

    constructor(
        private _localStore: LocalStorage,
        private _marketplaceService: MarketplaceService,
        _form: FormBuilder
    ) {
        this.assignmentForm = _form.group({
            personal: ['', Validators.required]
        });
        this._userdetails = <UserDetailModel>JSON.parse(this._localStore.get('userDetails'));
    };


    public routerOnActivate() {
        return new Promise((res, rej) => setTimeout(() => res(1), 500));
    };

    public routerOnDeactivate() {
        return new Promise((res, rej) => setTimeout(() => res(1), 500));
    };

    public back() {
        this.displayApply = false;
        this.links = [];
        this.assignmentPersonal = '';
        this.newLink = new LinkModel("", "");
    };

    public showApply() {
        this.displayApply = true;
    }

    public addNewLink(event: JQueryKeyEventObject): void {
        if (event.keyCode === 13 && this.newLink.name !== '' && this.newLink.url !== '') {
            this.links.push(this.newLink);
            this.newLink = { name: "", url: "" };
        }
    }

    public deleteLink(link: LinkModel): void {
        var itemPosition = this.links.indexOf(link);
        this.links.splice(itemPosition, 1);
    }

    public hiddenErrorForPersonal(): boolean {

        return this.assignmentForm.controls["personal"].untouched || this.assignmentForm.controls["personal"].valid;
    }

    public disabledSubmit(): boolean {
        return this.assignmentPersonal.length == 0
    }


    public applyForAssignment(): void {
        if (this.assignmentForm.dirty && this.assignmentForm.value.personal.length != 0) {
            let formData: FormData = new FormData();

            //add data
            formData.append('personalMessage', this.assignmentPersonal);
            formData.append('userId', this._userdetails.UserId);

            //add links  
            formData.append('linksString', JSON.stringify(this.links));

            //add files
            var files = (<any>$('#file-input')[0]).files;
            if (files.length > 0) {
                for (var i = 0; i < files.length; i++) {
                    formData.append('files' + i, files[i]);
                }
            }

            this._marketplaceService.applyToAssignment(this.assignmentId, formData);
            this.back();
        }
    }
}