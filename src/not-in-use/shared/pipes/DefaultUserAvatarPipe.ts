﻿import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name: "DefaultUserAvatarPipe"
})

export class DefaultUserAvatarPipe implements PipeTransform {
    transform(userAvatarUrl: string): string {

        return userAvatarUrl == "" || userAvatarUrl == null ? "assets/images/avatar.jpg" : userAvatarUrl;
    }
}