import { Pipe } from '@angular/core'

@Pipe({
  name: 'dictionary'
})
export class DictionaryPipe {
  transform(dict: Object): any {
    var a: Array<any> = [];
    for (var key in dict) {
      if (dict.hasOwnProperty(key)) {
        a.push({ key: key, val: (<any>dict)[key] });
      }
    }
    return a;
  }
}