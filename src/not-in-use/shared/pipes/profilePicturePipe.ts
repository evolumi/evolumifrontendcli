import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: 'profilePicture' })
export class ProfilePicturePipe implements PipeTransform {
    transform(value: any, small: boolean = true): any {
        if (!value) {
            return "assets/images/avatar.jpg";
        }
        else if (value == "assets/images/avatar.jpg") {
            return value;
        }
        else if (small) {
            return value + "-small";
        }
        else {
            return value;
        }
    }
}