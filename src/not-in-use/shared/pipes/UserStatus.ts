﻿import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name: "UserStatus"
})

export class UserStatusPipe implements PipeTransform {
    transform(items: any[], args: string): any {
        return items.filter(item => item.OrganisationStatus === args);
    }
}