import { Pipe } from '@angular/core'
import { ContactsCollection } from '../../../services/contacts.service';
import { AccountsCollection } from '../../../services/accounts.service';
import { AccountsModel } from "../../../models/account.models";

@Pipe({
    name: 'contactname',
    pure: false
})
export class ContactnamePipe {

    private contactname: string = ''

    constructor(private contacts: ContactsCollection) { }

    transform(id: string): any {
        if (!this.contactname) {
            this.contactname = this.contacts.getName(id).toString();
        }
        return this.contactname;
    }
}

@Pipe({
    name: 'accountname',
    pure: false
})
export class AccountNamePipe {

    //private accountname: string = "";

    constructor(private accSvc: AccountsCollection) { }

    transform(id: string): any {
        //  if(!this.accountname) {
        if (this.accSvc.accounts.length == 0) {
            this.accSvc.getAccounts();
        }
        let acc = this.accSvc.accounts.find((x: AccountsModel) => x.Id == id);
        let accountname = acc ? acc.ShownName : id;
        //}
        return accountname;
    }
}