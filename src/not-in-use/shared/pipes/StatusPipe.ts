﻿import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name: "statuspipe"
})

export class StatusPipe implements PipeTransform {
    transform(status: string): boolean {

        return status === 'Open' ? false : true;
    }
}

// @Pipe({ name: 'dealsobj', pure: false })
// export class DealsObj implements PipeTransform {
// 	transform(value: any, args: any[] = null): any {
// 		var formatted: any  = [];
// 		for(var key in value)
// 			formatted.push({ deals: value[key]['deals'], pro: value[key]['process']});
// 		return formatted;
// 	}
// }

// @Pipe({ name: 'dealstatus', pure: false })
// export class DealStatusPipe implements PipeTransform {
//     transform(value: any, args: any[] = null): any {      
//         return args.sort(function(a,b) {
//             return (a.value > b.value) ? 1 : ((b.value > a.value) ? -1 : 0);
//         })
//     }
// }



