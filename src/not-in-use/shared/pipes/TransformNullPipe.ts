import { Pipe } from '@angular/core';

@Pipe({
  name: 'TransformNullPipe'
})
export class TransformNullPipe {

  // Transforms Null or 0 to given value of res
  transform(value: any, args?: any) {
    var res = value;
    if (res == undefined || res == 0) {
      res = "N/A";
    }
    return res;
  }

}
