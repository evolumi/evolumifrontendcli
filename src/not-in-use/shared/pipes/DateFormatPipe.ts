﻿import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name: "DateFormatPipe"
})

export class DateFormatPipe implements PipeTransform {
    transform(value: string): string {

        var dateNow = new Date();
        var dateValue = new Date(value);

        var diff = dateNow.getTime() - dateValue.getTime();
        if (diff <= 3600000) {
            return dateValue.toLocaleTimeString();
        }
        if (diff > 3600000 && diff < 86400000) {
            if (dateNow.getDay() == dateValue.getDay()) {
                return "Today at " + dateValue.toLocaleTimeString();
            } else {
                return "Yesterday at " + dateValue.toLocaleTimeString();
            }
        }
        return dateValue.toLocaleString();
    }
}