//take from example https://github.com/zouabimourad/angular2-chosen

import { Component, OnInit, OnChanges, AfterViewInit, OnDestroy, EventEmitter, ElementRef, Input, Output } from '@angular/core';
import { LocalStorage } from '../../../../services/localstorage';
declare var $: JQueryStatic;


@Component({
    selector: 'air-datepicker',
    templateUrl: './airdatepicker.html'
})

export class AirDatepickerComponent implements OnInit, AfterViewInit, OnChanges, OnDestroy {

    @Output() change: EventEmitter<any>;
    @Input() range: boolean;
    @Input() timepicker: boolean;
    @Input() currentStartDate: number;
    @Input() currentEndDate: number;

    elementRef: ElementRef;
    selectElement: any;

    public height: string = '34px';
    public width: string = '100%';
    private options: any = { language: "en" };
    private datepikcker: any;
    private value: Array<Date> = [];

    constructor(
        public storage: LocalStorage,
        elementRef: ElementRef,
    ) {
        this.elementRef = elementRef;
        this.change = new EventEmitter();

    }

    ngOnInit() {

        if (this.storage.get('lan')) {
            this.options.language = this.storage.get('lan');
        }

        this.options.todayButton = true;

        if (this.range) {
            this.options.range = this.range;
            this.options.multipleDatesSeparator = ' - ';
        }

        if (this.timepicker) {
            this.options.timepicker = this.timepicker;
            this.options.dateTimeSeparator = ' ';
            this.options.minutesStep = 5;
        }

        var el: any = this.elementRef.nativeElement;
        this.selectElement = $(el).find(".datepicker-here");
        this.options.onSelect = (formattedDate: any, date: Array<Date>, inst: any) => {
            this.value = date;
            this.change.emit(this.value);
        }

        this.datepikcker = this.selectElement.datepicker(this.options).data('datepicker');



        //TODO make this work! =)
        //this.authService.currentAccountObserver().subscribe(response => {

        //    let IsWeekStartFromSunday = response.IsWeekStartFromSunday == "True" || response.IsWeekStartFromSunday == "true";
        //    let Is24DateTimeFormat = response.Is24DateTimeFormat == "True" || response.Is24DateTimeFormat == "true";

        //    this.options.timeFormat = Is24DateTimeFormat ? 'hh:ii' : 'hh:ii aa';
        //    this.options.firstDay = IsWeekStartFromSunday ? 0 : 1;

        //    this.datepikcker = this.selectElement.datepicker(this.options).data('datepicker');     
        //});


    }

    ngOnChanges(changes: any) {
        if (changes["currentStartDate"] && changes.currentStartDate.currentValue != changes.currentStartDate.previousValue && changes.currentStartDate.currentValue != null) {
            setTimeout(() => {
                this.datepikcker.selectDate(new Date(changes.currentStartDate.currentValue));
            });
        }
        if (changes["currentEndDate"] && changes.currentEndDate.currentValue != changes.currentEndDate.previousValue && changes.currentEndDate.currentValue != null) {
            setTimeout(() => {
                this.datepikcker.selectDate(new Date(changes.currentEndDate.currentValue));
            });
        }

    }

    ngAfterViewInit() {

    }

    ngOnDestroy() {

    }

    openBtnClicked() {
        this.datepikcker.show();
    }

    removeBtnClicked() {
        this.datepikcker.clear();
        this.value = [];
        this.change.emit(this.value);
    }

    isDateSelected(): boolean {
        return this.value.length > 0;
    }
}

