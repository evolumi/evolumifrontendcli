import { Component, Input } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { ModalService } from '../../../../services/modal.service';
import { NotificationService } from '../../../../services/notification-service';

import { ActionsModel } from '../../../../models/actions.models';
import { ContactsCollectionModel } from '../../../../models/contact.models';
import { DealsModel, OwnerPercentage, ProductModel } from '../../../../models/deals.models';
import { AccountsModel } from '../../../../models/account.models';
import { ChosenOption, ChosenOptionsGroup, Chosen } from "../chosen/chosen";
import { ChosenOptionModel } from '../chosen/chosen.models';
import { CreateNewsfeedModel } from '../../../../models/newsfeed.models';

import { ActionsCollection } from '../../../../services/actionservice';
import { DealAddEdit } from '../../../../services/dealAddEdit.service';
import { ContactsCollection } from '../../../../services/contacts.service';
import { AccountsCollection } from '../../../../services/accounts.service';
import { OrgService } from '../../../../services/organisation.service';
import { UsersService } from '../../../../services/users.service';
import { NewsfeedService } from '../../../../services/newfeed.service';

@Component({
    selector: 'quick-actions',
    templateUrl: 'quick-actions.html',
    styleUrls: ['quick-actions.css']
})

export class QuickActions {

    @Input() accountId: string;

    public selectedDate: number = new Date().getTime();

    public textArea: string;
    public showMenu: boolean = false;

    public action: ActionsModel;
    public contact: ContactsCollectionModel;
    public account: AccountsModel;
    public deal: DealsModel;

    public actionReminderDates: Array<ChosenOption> = [];
    public dealReminderDates: Array<ChosenOption> = [];

    public users: Array<ChosenOptionModel> = [];
    public accounts: Array<ChosenOption> = [];
    public accountTypes: Array<ChosenOption> = [];
    public saleProcess: Array<ChosenOption> = [];
    public contacts: Array<ChosenOption> = [];

    private userSub: any;
    private accSub: any;
    private orgSub: any;
    private contactSub: any;

    public loading: boolean;

    constructor(
        public modal: ModalService,
        private notify: NotificationService,
        private newsService: NewsfeedService,
        private userService: UsersService,
        private orgService: OrgService,
        private translate: TranslateService,
        private actionService: ActionsCollection,
        private dealService: DealAddEdit,
        private accountService: AccountsCollection,
        private contactService: ContactsCollection
    ) {

        let userId = localStorage.getItem("userId");

        this.action = new ActionsModel({ AccountId: this.accountId, Type: "Email", UserAssignedId: userId });
        this.action.Labels = [];
        this.action.Priority = 'Normal';
        this.action.Status = 'Open';
        this.action.Type = 'Meeting';

        this.deal = new DealsModel({ AccountId: this.accountId });
        this.account = new AccountsModel();
        this.contact = new ContactsCollectionModel();

        this.account.AccountManagerId = userId;
        this.account.Tags = [];
        this.contact.AccountId = this.accountId;
        this.deal.Owners = [new OwnerPercentage(userId, 100)];

        this.actionReminderDates.push(new ChosenOptionModel("StartDate", this.translate.instant("STARTDATE")));
        this.actionReminderDates.push(new ChosenOptionModel("EndDate", this.translate.instant("ENDDATE")));
        this.dealReminderDates.push(new ChosenOptionModel("DateStart", this.translate.instant("STARTDATE")));
        this.dealReminderDates.push(new ChosenOptionModel("DateEnd", this.translate.instant("ENDDATE")));
        this.dealReminderDates.push(new ChosenOptionModel("DateInvoice", this.translate.instant("DATEINVOICE")));
        this.dealReminderDates.push(new ChosenOptionModel("DateInvoiceExpire", this.translate.instant("DATEINVOICEEXPIRE")));

        // this.accSub = this.accountService.accountListObservable().subscribe(accs => {
        //     this.accounts = accs;
        //     if (accs && accs.length) {
        //         if (!this.accountId) {
        //             this.action.AccountId = accs[0].value + "";
        //             this.deal.AccountId = accs[0].value + "";
        //             this.contact.AccountId = accs[0].value + "";
        //             this.updateContacts(accs[0].value.toString());
        //         }
        //         else {
        //             this.updateContacts(this.accountId);
        //         }
        //     }
        // });

        this.orgSub = this.orgService.currentOrganisationObservable().subscribe(org => {
            if (org) {
                this.accountTypes = [];
                this.saleProcess = [];

                for (let type of org.AccountTypes) {
                    this.accountTypes.push(new ChosenOptionModel(type.Id, type.Name));
                }
                for (let sp of org.Settings.SaleProcesses) {
                    this.saleProcess.push(new ChosenOptionModel(sp.Id, sp.Name));
                }

                this.account.AccountTypeId = org.AccountTypes[0].Id;
                this.deal.SaleProcessId = org.Settings.SaleProcesses[0].Id;
                this.deal.Currency = Number(org.Currency);
                this.deal.CurrencyName = org.CurrencyName;
                this.deal.Vat = 0;
            }
        });

        this.userSub = this.userService.activeUserListObervable().subscribe(users => {
            if (users) {
                this.users = users;
            }
        });

        this.contactSub = this.contactService.contactsByAccountListObservable().subscribe(contacts => {
            if (contacts) {
                this.contacts = [new ChosenOptionModel("", this.translate.instant('LINKTO'))]
                this.contacts = this.contacts.concat(contacts);
            }
        });

    }

    public updateContacts(id: string) {
        this.contactService.getContactListByAccountId(id);
    }

    public createNote() {
        if (this.accountId) {
            let newsModel = new CreateNewsfeedModel();
            newsModel.AccountId = this.accountId;
            newsModel.Text = this.textArea;

            this.newsService.addNews(newsModel);
        }
    }

    public createAccount() {

        this.loading = true;

        this.accountService.addAccount(this.account, false).subscribe(res => {
            if (res.IsSuccess) {
                this.account.ShownName = "";
                this.account.Tags = [];
            }
            this.loading = false;
        });


    }

    public createDeal() {

        this.loading = true;

        this.deal.Note = this.textArea;
        this.deal.ProductItems.push(new ProductModel(0, "Product", 1, 0, this.deal.Value, this.deal.Value, "Pcs", "Product", "Sum"));
        this.dealService.createDealApi(this.deal).subscribe((res: any) => {
            if (res.IsSuccess) {
                this.deal.ProductItems = [];
                this.textArea = "";
                this.deal.Value = 0;
            }
            else {
                this.notify.error(this.translate.instant("ERROR"));
            }
            this.loading = false;
        });


    }

    public createAction() {

        this.loading = true;

        this.action.Name = this.translate.instant(this.getLabel(this.action.Type));
        this.action.EndDate = this.action.StartDate = this.selectedDate;
        this.action.Comments = this.textArea;

        this.actionService.createAction(this.action).subscribe(res => {
            if (res.IsSuccess) {
                this.notify.success(this.translate.instant("ACTIONCREATED"));
                this.textArea = "";
                this.action.Reminder = null;
            }
            else {
                this.notify.error(this.translate.instant("ERROR"));
            }
            this.loading = false;
        });


    }

    public createContact() {

        this.loading = true;

        this.contact.Notes = this.textArea;
        this.contactService.contactData = this.contact;
        this.contactService.addContact().subscribe(a => {
            if (a.IsSuccess) {

                this.textArea = "";
                this.contact.FirstName = "";
                this.contact.LastName = "";
                this.contact.Email = "";
                this.contact.PhoneNumber = "";
            }
            else {
                console.log("[Contact] Adding Contact: Fail! -> " + a.Message);
            }
            this.loading = false;
        });
    }

    ngOnInit() {

    }

    ngOnDestroy() {
        if (this.userSub) this.userSub.unsubscribe();
        if (this.accSub) this.accSub.unsubscribe();
        if (this.orgSub) this.orgSub.unsubscribe();
        if (this.contactSub) this.contactSub.unsubscribe();
    }

    getLabel(type: string): string {
        if (type === "Email") return "ACTIONTYPEEMAIL";
        if (type === "Phone") return "ACTIONTYPEPHONECALL";
        if (type === "Meeting") return "ACTIONTYPEMEETING";
        if (type === "Reminder") return "ACTIONTYPEREMINDER";
        if (type === "Events") return "ACTIONTYPEEVENT";
        if (type === "Other") return "ACTIONTYPEOTHER";
        if (type === "Low") return "PRIORITYLOW";
        if (type === "Normal") return "PRIORITYNORMAL";
        if (type === "High") return "PRIORITYHIGH";
        if (type === "Open") return "OPEN";
        if (type === "Closed") return "CLOSED";
        else return '';
    }

}
