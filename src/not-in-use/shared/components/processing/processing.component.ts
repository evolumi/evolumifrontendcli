import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'processing',
    template: `
        <div class="customPopup processing-popup system-active" id="processing-popup">
            <div class="popupBox">
                <div class="title">
                    {{ title }}
                </div>
                <div class="popupContent">
                    <img class="loading" src="assets/images/preloader1.gif" />
                    <div class="text-center">
                        {{ message }}
                    </div>
                    <div (click)="abort.emit(true)" class="evo-cancel-link pull-right clickable">{{'CANCELBTN' | translate}}</div>
                </div>
            </div>
        </div>
    `,
    styles: [`
    	.customPopup.processing-popup {
	    	background: transparent;
	    }
        .customPopup.processing-popup .popupBox{
            border: #CCC solid 1px;
        }	
    `]
})

export class ProcessingComponent {

    @Input() title: string;
    @Input() message: string;
    @Output() abort = new EventEmitter<boolean>();

    constructor() { }

}