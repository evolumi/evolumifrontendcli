import { Component, Input, Output, EventEmitter } from '@angular/core';
import { SponsorsService } from './sponsors.service';

@Component({
	selector: 'ads-for-agenda',
	templateUrl: './adsforagenda.html'
})
export class AdsForAgendaComponent {
	@Input() agendaInfo: any;
	@Output() dateChanged: EventEmitter<Object> = new EventEmitter();
	public sponsors: any;
	public sponsorsArray: Array<any> = [];
	public activeAgenda: any;
	public activeAgendaArray: Array<any> = [];
	public sponsorItem: any = {};
	public days = [1, 2, 3, 4, 5, 6];
	public someEvent: any;
	public selectionDayTxt: any;
	constructor(public _sponsors: SponsorsService) {
		this._sponsors = _sponsors;
		this.someEvent = new EventEmitter();
	}
	ngOnInit() {
		this.getSponsors();
		this._sponsors.activeAgenda$.subscribe(latest => {
			this.activeAgendaArray = latest;
			//this.agendaMapIndex();
		});
	}
	getSponsors() {
		// this._sponsors.sponsors$.subscribe(latest => {
		// 	this.sponsorsArray = latest;
		// });
		//this._sponsors.getSponsors();
	}
	addDays(index: number) {
		this.sponsorItem.SponsorId = this.sponsorsArray[index].Id;
		this.dateChanged.emit({ date: {}, formatted: this.selectionDayTxt, epoc: 0 });
		var added = this._sponsors.addDays(this.sponsorItem);
		if (added) {
			this.dateChanged.emit({ date: {}, formatted: this.selectionDayTxt, epoc: 0 });
		}
	}
	deleteAd(Id: any) {
		this._sponsors.deleteAd(Id);
	}
	/*enablePop(index, showElement) {
		if(showElement === 'days') {
			this.sponsorsArray[index].showDays = true;
		} else if(showElement === 'schedule') {
			this.sponsorsArray[index].showSchedule = true;
		}
	}
	disablePop(index, showElement) {
		if(showElement === 'days') {
			this.sponsorsArray[index].showDays = false;
		} else if(showElement === 'schedule') {
			this.sponsorsArray[index].showSchedule = false;
		}
	}*/
	addSchedule(Id: string, agenda_date: any) {
		console.log('testing');
		this.sponsorItem = {};
		this.sponsorItem.SponsorId = Id;
		this.sponsorItem.Date = agenda_date;
		this._sponsors.addSchedule(this.sponsorItem);
	}
	reSchedule(oldId: string, Id: string, agenda_date: any) {
		this.sponsorItem = {};
		this.sponsorItem.OldSponsorId = oldId;
		this.sponsorItem.SponsorId = Id;
		this.sponsorItem.Date = 'agenda_date';
		this._sponsors.reSchedule(this.sponsorItem);
	}
}
