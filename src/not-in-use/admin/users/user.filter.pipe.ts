import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
    name: 'userFilter'
})
export class UserFilterPipe implements PipeTransform {
    transform(value: any, args: string[]): any {
        let filter = args[0].toLocaleLowerCase();
        return filter ? value.filter((user: any) => user.FullName.indexOf(filter) != -1) : value;
    }
}