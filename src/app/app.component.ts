import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from "../environments/environment";

@Component({
  selector: 'sd-app',
  templateUrl: 'app.html',
  styleUrls: ['../css/main.css'],
  encapsulation: ViewEncapsulation.None,
})

export class AppComponent {
  constructor(public router: Router) {
    this.changeFavIcon();
  }

  changeFavIcon() {
    let link = <HTMLLinkElement>document.querySelector("link[rel*='icon']");
    if (link) {
      link.href = environment.faviconUrl;
    }
  }
}
