import { NgModule } from '@angular/core';
import { RouterModule, PreloadAllModules } from '@angular/router';

// import { LoginComponent } from './modules/login/login.component';
// import { ForgotPassword } from './modules/login/forgotpassword/forgotpassword.component';
// import { SignupComponent } from './modules/login/signup.component';
// import { FirstLoginComponent } from './modules/login/firstLogin.component';
// import { ResetPasswordComponent } from './modules/login/resetPassword/setNewPassword';
// import { EmailValidateComponent } from './modules/login/email-validate.component';
// import { CalendarAuthComponent } from './modules/login/calendar-auth.component';
// import { UserQuestionsComponent } from "./modules/login/user-questions/user-questions";

import { CrmRoutes } from './modules/crm/crm-routing.module';
import { AdminRoutes } from './modules/admin/admin-routing.module';
import { OverviewRoutes } from './modules/my-overview/overview-routing.module';
import { LoginRoutes } from './modules/login/login-routing.module';

@NgModule({
  imports: [
    RouterModule.forRoot([

      { path: '', redirectTo: 'Login', pathMatch: 'full' },

      // Lazy loading works but need to be worked on more before implemntation
      // { path: 'CRM', loadChildren: './modules/crm/crm.module#CrmModule'},
      // { path: 'Admin', loadChildren: './modules/admin/admin.module#AdminModule'},
      // { path: 'Overview', loadChildren: './modules/my-overview/overview.module#OverviewModule'},

      ...LoginRoutes,
      ...CrmRoutes,
      ...AdminRoutes,
      ...OverviewRoutes
    ], 
    // {
    //     preloadingStrategy: PreloadAllModules
    //   }
    )
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }

