import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Injectable } from '@angular/core';

@Injectable()
export class LoaderService {

  private _loader = { called: 0, completed: 0 };
  private loader$: BehaviorSubject<any> = new BehaviorSubject(this._loader);
  public loaderObservable(): any {
    return this.loader$.asObservable();
  }

  constructor() {

  }

  clear() {
    this._loader.completed = this._loader.called = 0;
    this.loader$.next(this._loader);
  }


  addCounter() {
    this._loader.called++;
    this.loader$.next(this._loader);
  }

  removeCounter() {
    this._loader.completed++;
    this.loader$.next(this._loader);
    if (this._loader.completed == this._loader.called) {
      this._loader.completed = this._loader.called = 0;
    }
  }
}
