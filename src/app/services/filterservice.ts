import { Api } from './api';
import { LoaderService } from './loader.service';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import { LocalStorage } from './localstorage';
import { AccountModel } from '../models/account.models';
import { ContactsCollectionModel } from '../models/contact.models';
import { AllUsersModel } from '../models/user.models';
import { IntercomService } from '../services/interecomService';
import { OrgService } from '../services/organisation.service';
import { Injectable } from "@angular/core";

@Injectable()
export class FilterCollection {

    private orgSub: any;

    public isGuestUser: boolean = false;
    public organisationPlan: string;

    public orgId: string;
    private _updateOrganisationInProgress: boolean = false;
    private _updateOrganisationId: string;
    //observers goes here

    //account type observer goes here
    public currentOrganisation$: Observable<any>;
    private _currentOrganisationObserver: Observer<any>;
    public fullOrganisation: any;

    //account type observer goes here
    // public accountTypes$: Observable<AccountTypesModel[]>;
    // private _accountTypeObserver: Observer<AccountTypesModel[]>;
    // private accountTypes: Array<AccountTypesModel>;

    //Plan observer goes here
    public plan$: Observable<string>;
    private _planObserver: Observer<string>;
    private plan: string;

    //Permission observer goes here
    public permissions$: Observable<Array<string>>;
    private _permissionsObserver: Observer<Array<string>>;
    private permissions: Array<string>;

    //Plan observer goes here
    public userRole$: Observable<string>;
    private _userRoleObserverr: Observer<string>;
    private userRole: string;

    //account observer
    public account$: Observable<AccountModel[]>;
    private _accountObserver: Observer<AccountModel[]>;

    //actions observer
    public actionStatus$: Observable<any[]>;
    private _actionStatusObserver: Observer<any[]>;
    private actionStatus: Array<string[]>;
    private _actionStatusesLoading: boolean = false;

    //users observer
    public users$: Observable<AllUsersModel[]>;
    private _usersObserver: Observer<AllUsersModel[]>;

    //contacts observer
    public contact$: Observable<ContactsCollectionModel[]>;
    private _contactObserver: Observer<ContactsCollectionModel[]>;
    private contact: ContactsCollectionModel[];
    private _contactsLoading: boolean = false;

    //contactsByAccount observer
    public contactByAccount$: Observable<ContactsCollectionModel[]>;
    private _contactByAccountObserver: Observer<ContactsCollectionModel[]>;

    //contacts observer
    public actionType$: Observable<string[]>;
    private _actionTypeObserver: Observer<string[]>;
    private actionType: Array<string>;
    private _actionTypeLoading: boolean = false;

    //priorityobserver
    public priority$: Observable<string[]>;
    private _priorityObserver: Observer<string[]>;
    private priority: Array<string>;
    private _priorityLoading: boolean = false;

    //tags observer
    public accountTags$: Observable<string[]>;
    private _accountTagsObserver: Observer<string[]>;
    private accountTags: Array<string>;

    //address observer
    public addressTypes$: Observable<string[]>;
    private _addressTypesObserver: Observer<string[]>;
    private addressTypes: Array<string>;
    private _addressTypesLoading: boolean = false;

    //salesprocess observer
    // public salesProcess$: Observable<any[]>;
    // private _salesProcessObserver: Observer<any[]>;
    // public salesProcess: Array<any>;

    //currency observer
    public currency$: Observable<any[]>;
    private _currencyObserver: Observer<any[]>;
    private currency: any;
    private _currencyLoading: boolean = false;

    //vat observer
    public vat$: Observable<string[]>;
    private _vatObserver: Observer<string[]>;
    private vat: any;

    //vat observer
    public unit$: Observable<string[]>;
    private _unitObserver: Observer<string[]>;
    private unit: any;

    //organisation settings observer
    public orgSettings$: Observable<any>;
    private _orgSettingsObserver: Observer<any>;

    //deal change detector
    public dealchange$: Observable<string>;
    private _dealchangObserver: Observer<string>;

    //contact change detector
    public contactchange$: Observable<string>;
    private _contactchangeObserver: Observer<string>;

    //account change detector
    public accountchange$: Observable<string>;

    //action change detector
    public actionchange$: Observable<string>;
    private _actionchangeObserver: Observer<string>;
    //labels 
    public labels$: Observable<string[]>;
    private _labelsObserver: Observer<string[]>;
    private labels: Array<string>;

    public searchParams: any;

    constructor(
        public _api: Api,
        public intercomService: IntercomService,
        private _load: LoaderService,
        private _localstore: LocalStorage,
        private orgService: OrgService
    ) {
        // this.accountTypes$ = new Observable(observer => {
        //     this._accountTypeObserver = observer;
        // }).share();
        this.currentOrganisation$ = new Observable((observer: any) => {
            this._currentOrganisationObserver = observer;
        }).share();
        this.plan$ = new Observable<string>((observer: any) => {
            this._planObserver = observer;
        }).share();
        this.permissions$ = new Observable<Array<string>>((observer: any) => {
            this._permissionsObserver = observer;
        }).share();
        this.userRole$ = new Observable<string>((observer: any) => {
            this._userRoleObserverr = observer;
        }).share();
        this.account$ = new Observable<Array<AccountModel>>((observer: any) => {
            this._accountObserver = observer;
        }).share();
        this.actionStatus$ = new Observable<Array<any>>((observer: any) => {
            this._actionStatusObserver = observer;
        }).share();
        this.users$ = new Observable<Array<AllUsersModel>>((observer: any) => {
            this._usersObserver = observer;
        }).share();
        this.contact$ = new Observable<Array<ContactsCollectionModel>>((observer: any) => {
            this._contactObserver = observer;
        }).share();
        this.contactByAccount$ = new Observable<Array<ContactsCollectionModel>>((observer: any) => {
            this._contactByAccountObserver = observer;
        }).share();
        this.actionType$ = new Observable<Array<string>>((observer: any) => {
            this._actionTypeObserver = observer;
        }).share();
        this.priority$ = new Observable<Array<string>>((observer: any) => {
            this._priorityObserver = observer;
        }).share();
        this.accountTags$ = new Observable<Array<string>>((observer: any) => {
            this._accountTagsObserver = observer;
        }).share();
        this.addressTypes$ = new Observable<Array<string>>((observer: any) => {
            this._addressTypesObserver = observer;
        }).share();
        // this.salesProcess$ = new Observable(observer => {
        //     this._salesProcessObserver = observer;
        // }).share();
        this.currency$ = new Observable<Array<any>>((observer: any) => {
            this._currencyObserver = observer;
        }).share();
        this.vat$ = new Observable<Array<string>>((observer: any) => {
            this._vatObserver = observer;
        }).share();
        this.orgSettings$ = new Observable((observer: any) => {
            this._orgSettingsObserver = observer;
        }).share();
        this.dealchange$ = new Observable<string>((observer: any) => {
            this._dealchangObserver = observer;
        }).share();
        this.contactchange$ = new Observable<string>((observer: any) => {
            this._contactchangeObserver = observer;
        }).share();

        this.actionchange$ = new Observable<string>((observer: any) => {
            this._actionchangeObserver = observer;
        }).share();
        this.labels$ = new Observable<Array<string>>((observer: any) => {
            this._labelsObserver = observer;
        }).share();

        this.orgSub = this.orgService.currentOrganisationObservable().subscribe(org => {
            if (org) {
                this.getCurrentOrganisation();
            }
        });

        this.searchParams = { ParentCompany: '', AccountTypeId: '', AccountManager: '', Type: '', Tags: '', Owners: '', Search: '', AccountId: '', Page: 1, PageSize: 20, UserId: '' };
    }

    public getAccountTypes(forceRefresh?: boolean): void {
        if (this.fullOrganisation == null || this.fullOrganisation == undefined || forceRefresh) {
            //this.orgId = this._localstore.get('orgId');
            //this._api.get('Organisations/' + this.orgId + '/AccountTypes')
            //    .map(res => res.json())
            //    .subscribe(res => {
            //        this.accountTypes = [];
            //        for (var x of res.Data) {
            //            this.accountTypes.push(new AccountTypesModel(x));
            //        }
            //        this._accountTypeObserver.next(this.accountTypes);
            //    });
            this.getFullOrganisation();
        }
        else {
            // if (this._accountTypeObserver)
            //     this._accountTypeObserver.next(this.accountTypes);
        }
    }

    public getPlan(forceRefresh?: boolean): void {
        if (this.fullOrganisation == null || this.fullOrganisation == undefined || forceRefresh) {
            this.getFullOrganisation();
        }
        else {
            if (this._planObserver)
                this._planObserver.next(this.plan);
        }
    }
    public getPermissions(forceRefresh?: boolean): void {
        if (this.fullOrganisation == null || this.fullOrganisation == undefined || forceRefresh) {
            this.getFullOrganisation();
        }
        else {
            if (this._permissionsObserver)
                this._permissionsObserver.next(this.permissions);
        }
    }
    public getUserRole(forceRefresh?: boolean): void {
        if (this.fullOrganisation == null || this.fullOrganisation == undefined || forceRefresh) {
            this.getFullOrganisation();
        }
        else {
            if (this._userRoleObserverr)
                this._userRoleObserverr.next(this.userRole);
        }
    }

    // public addAccountType(newAccount:any, orgId:string) {
    //     this._api.requestPost(
    //         `Organisations/${orgId}/AccountTypes`,
    //         newAccount,
    //         (data) => {
    //             this.getAccountTypes(true);
    //         }
    //     );
    // }

    public getVat(forceRefresh?: boolean): void {
        if (this.fullOrganisation == null || this.fullOrganisation == undefined || forceRefresh) {
            //this.orgId = this._localstore.get('orgId');
            //this._api.get('Organisations/' + this.orgId + '/Vats')
            //    .map(res => res.json())
            //    .subscribe(res => {
            //        this.vat = res.Data;
            //        this._vatObserver.next(this.vat);
            //    });
            this.getFullOrganisation();
        }
        else {
            if (this._vatObserver)
                this._vatObserver.next(this.vat);
        }
    }

    public getUnit(forceRefresh?: boolean): void {
        if (this.fullOrganisation == null || this.fullOrganisation == undefined || forceRefresh) {
            this.getFullOrganisation();
        }
        else {
            if (this._unitObserver) {
                this._unitObserver.next(this.unit);
            }
        }
    }

    public getTags(forceRefresh?: boolean): void {
        if (this.fullOrganisation == null || this.fullOrganisation == undefined || forceRefresh) {
            //this.orgId = this._localstore.get('orgId');
            //this._api.get('Organisations/' + this.orgId + '/Tags')
            //    .map(res => res.json())
            //    .subscribe(res => {
            //        if (res.IsSuccess === true) {
            //            this.accountTags = res.Data;
            //            this._accountTagsObserver.next(this.accountTags);
            //        } else { return false; }
            //    }, err => {
            //        console.log('error 77');
            //    });
            this.getFullOrganisation();
        }
        else {
            if (this._accountTagsObserver)
                this._accountTagsObserver.next(this.accountTags);
        }
    }

    public getLabels(forceRefresh?: boolean): void {
        if (this.fullOrganisation == null || this.fullOrganisation == undefined || forceRefresh) {
            this.getFullOrganisation();
        }
        else {
            if (this._labelsObserver)
                this._labelsObserver.next(this.labels);
        }

    }

    // public getAllSalesProcess(forceRefresh?: boolean): void {
    //     if (this.fullOrganisation == null || this.fullOrganisation == undefined || forceRefresh) {
    //         //var orgId = this._localstore.get('orgId');
    //         //return this._api.get('Organisations/' + orgId + '/Settings')
    //         //    .map(res => res.json())
    //         //    .subscribe(res => {
    //         //        this.salesProcess = res.Data.SaleProcesses;
    //         //        this._salesProcessObserver.next(this.salesProcess);
    //         //    })
    //         this.getFullOrganisation();
    //     }
    //     else {
    //         if (this._salesProcessObserver)
    //             this._salesProcessObserver.next(this.salesProcess);
    //     }
    // }

    public getOrganisationSettings(forceRefresh?: boolean): void {
        if (this.fullOrganisation == null || this.fullOrganisation == undefined || forceRefresh) {
            //var orgId = this._localstore.get('orgId');
            //this._api.get('/Organisations/' + orgId)
            //    .map(res => res.json())
            //    .subscribe(res => {
            //        this._orgSettingsObserver.next(res.Data);
            //    });
            this.getFullOrganisation();
        }
        else {
            if (this._orgSettingsObserver)
                this._orgSettingsObserver.next(this.fullOrganisation);
        }
    }

    public getCurrentOrganisation(forceRefresh?: boolean): void {
        if (this.fullOrganisation == null || this.fullOrganisation == undefined || forceRefresh) {

            this.getFullOrganisation();
        }
        else {
            if (this._currentOrganisationObserver) {
                this._currentOrganisationObserver.next(this.fullOrganisation);
            }
        }
    }

    public setOrganisation(orgId: string) {
        this.orgId = orgId;
        this.getFullOrganisation();
    }

    private getFullOrganisation(): void {
        var userId = this._localstore.get('userId');
        this.orgId = this._localstore.get('orgId');
        if (this.orgId != this._updateOrganisationId || !this._updateOrganisationInProgress) {

            if (this.orgId != "null" && this.orgId && this.orgId != "false") {
                this._updateOrganisationInProgress = true;
                this._api.requestGet(
                    `Organisations/${this.orgId}/FullInfo`,
                    (data) => {

                        this.organisationPlan = data.Plan;
                        this.isGuestUser = data.UserRole == "Guest";

                        this.fullOrganisation = data;

                        this.intercomService.updateUserOrgInfo(userId, data.Plan, data.Id, data.Email, data.OrganisationName)

                        if (this._currentOrganisationObserver) {
                            this._currentOrganisationObserver.next(this.fullOrganisation);
                        }

                        this.plan = data.Plan;
                        if (this._planObserver) {
                            this._planObserver.next(this.plan);
                        }

                        this.permissions = data.userPermissions;
                        if (this._permissionsObserver)
                            this._permissionsObserver.next(this.permissions);

                        this.userRole = data.UserRole;
                        if (this._userRoleObserverr)
                            this._userRoleObserverr.next(this.userRole);

                        // this.salesProcess = data.Settings.SaleProcesses;

                        // for (let a in this.salesProcess) {
                        //     this.salesProcess[a].DealStatuses = this.sort(this.salesProcess[a].DealStatuses);
                        // }
                        // if (this._salesProcessObserver)
                        //     this._salesProcessObserver.next(this.salesProcess);

                        this.accountTags = data.Tags;
                        if (this._accountTagsObserver)
                            this._accountTagsObserver.next(this.accountTags);

                        this.labels = data.Labels;
                        if (this._labelsObserver)
                            this._labelsObserver.next(this.labels);

                        this.vat = data.Vats;
                        if (this._vatObserver)
                            this._vatObserver.next(this.vat);

                        this.unit = data.Units
                        if (this._unitObserver) {
                            this._unitObserver.next(this.unit);
                        }

                        // this.accountTypes = [];
                        // for (var x of data.AccountTypes) {
                        //     this.accountTypes.push(new AccountTypesModel(x));
                        // }
                        // if (this._accountTypeObserver)
                        //     this._accountTypeObserver.next(this.accountTypes);

                        if (this._orgSettingsObserver)
                            this._orgSettingsObserver.next(data);

                        this._updateOrganisationInProgress = false;
                    },
                    () => {
                        this._updateOrganisationInProgress = false;
                    }
                );
            }
        }
    }

    sort(array: any) {
        return array.sort(function (a: any, b: any) {
            return (a.SortOrder > b.SortOrder) ? 1 : ((b.SortOrder > a.SortOrder) ? -1 : 0);
        })
    }

    getCurrency() {
        if (!this._currencyLoading) {
            this._currencyLoading = true;
            if (this.currency == null || this.currency == undefined || this.currency.length == 0) {
                this._api.get('Organisations/Currencies')
                    .map(res => res.json())
                    .subscribe(res => {
                        this.currency = [];
                        for (let index in res.Data) {
                            this.currency.push({ id: index, name: res.Data[index] });
                        }
                        this._currencyLoading = false;
                        this._currencyObserver.next(this.currency);
                    },
                    (err) => {
                    },
                    () => {
                        this._currencyLoading = false;
                    });
            }
            else {
                this._currencyLoading = false;
                this._currencyObserver.next(this.currency);
            }
        }
    }

    // WARNING! This cant be used. Loading "ALL" accounts can cause some realy bad performace if the users has alot of them.

    getAccounts(scroll = false, force = false) {
        console.log("[Filter] Get Accounts: Aborted!!");
        //if (!this._accountLoading || force === true) {
        //    this._accountLoading = true;
        //    if (this._account == null || this._account == undefined || this._account.length == 0 || force === true) {
        //        this.orgId = this._localstore.get('orgId');
        //        let query = this.getSearchString();
        //        if (this.orgId != "null" && this.orgId) {
        //            this._api.requestGet('Organisations/' + this.orgId + '/Accounts',
        //                (res) => {
        //                    console.log("[Filter] Get Accounts: Success! -> Found " + res.length + " of them");
        //                    this._account = [];
        //                    for (var x of res) {
        //                        this._account.push(new AccountModel(x.Id, x.ShownName));
        //                    }
        //                    this._accountLoading = false;

        //                    if (this._accountObserver)
        //                        this._accountObserver.next(this._account);
        //                }, () => {
        //                    console.log("[Filter] Get Accounts: Failed!");
        //                    this._accountLoading = false;
        //                });
        //        }
        //    }
        //    else {
        //        console.log("[Filter] Get Accounts: Aborted!");
        //        this._accountLoading = false;
        //        this._accountObserver.next(this._account);
        //    }
        //}
    }

    onScroll(ele: any) {
        var ele = ele.srcElement || ele.target;
        if (ele.scrollTop === (ele.scrollHeight - ele.offsetHeight)) {
            // if (this.apiInProgress == true) return;
            // this.apiInProgress = true;
            this.searchParams.Page = (this.searchParams.Page | 0) + 1;
            this.getAccounts(true);
        }
    }

    getSearchString() {
        var str: Array<any> = [];
        for (var p in this.searchParams)
            if (this.searchParams.hasOwnProperty(p)) {
                if (this.searchParams[p] === '' || this.searchParams[p] === 'All' || typeof this.searchParams[p] === 'undefined')
                    continue;
                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(this.searchParams[p]));
            }
        return str.join("&");

    }

    getActionStatuses() {
        if (!this._actionStatusesLoading) {
            this._actionStatusesLoading = true;
            if (this.actionStatus == null || this.actionStatus == undefined || this.actionStatus.length == 0) {
                this._api.get('Actions/ActionStatuses')
                    .map(res => res.json())
                    .subscribe(res => {
                        this.actionStatus = [];
                        for (var actionStatus of res.Data) {
                            this.actionStatus.push(actionStatus);
                        }
                        this._actionStatusesLoading = false;
                        this._actionStatusObserver.next(this.actionStatus);
                    },
                    (err) => {
                    },
                    () => {
                        this._actionStatusesLoading = false;
                    });
            }
            else {
                this._actionStatusesLoading = false;
                this._actionStatusObserver.next(this.actionStatus);
            }
        }
    }

    getUsers(force: boolean = false) {
        console.log("[Filter] Get Users: Started!");
        // if (!this._usersLoading || force === true) {
        //     this._usersLoading = true;
        //     if (this.users == null || this.users == undefined || this.users.length == 0 || force === true) {
        //         this.orgId = this._localstore.get('orgId');
        //         this._api.requestGet('Organisations/' + this.orgId + '/Users?onlyActive=true',
        //             (res) => {
        //                 console.log("[Filter] Get Users: Success! -> Found " + res.length + " of them");
        //                 this.users = [];
        //                 for (var tmpusers of res) {
        //                     this.users.push(new AllUsersModel(tmpusers));
        //                 }
        //                 this._usersLoading = false;
        //                 this._usersObserver.next(this.users);
        //             },
        //             (err) => {
        //                 console.log("[Filter] Get Users: Failed!");
        //                 this._usersLoading = false;
        //             });
        //     }
        //     else {
        //         console.log("[Filter] Get Users: Aborted!");
        //         this._usersLoading = false;
        //         this._usersObserver.next(this.users);
        //     }
        // }
    }

    changeDeal() {
        if (typeof this._dealchangObserver != 'undefined') this._dealchangObserver.next('reload');
    }

    changeContact() {
        if (typeof this._contactchangeObserver != 'undefined') this._contactchangeObserver.next('reload');
    }


    changeAction() {
        if (typeof this._actionchangeObserver != 'undefined') this._actionchangeObserver.next('reload');
    }

    getContacts() {
        if (!this._contactsLoading) {
            this._contactsLoading = true;
            if (this.contact == null || this.contact == undefined || this.contact.length == 0) {
                this.orgId = this._localstore.get('orgId');
                this._api.get('Organisations/' + this.orgId + '/Contacts')
                    .map(res => res.json())
                    .subscribe(res => {
                        this.contact = [];
                        for (var tmpcontacts of res.Data) {
                            this.contact.push(new ContactsCollectionModel(tmpcontacts));
                        }
                        this._contactsLoading = false;
                        this._contactObserver.next(this.contact);
                    },
                    (err) => {
                    },
                    () => {
                        this._contactsLoading = false;
                    });
            }
            else {
                this._contactsLoading = false;
                this._contactObserver.next(this.contact);
            }
        }
    }

    getActionTypes() {
        if (!this._actionTypeLoading) {
            this._actionTypeLoading = true;
            if (this.actionType == null || this.actionType == undefined || this.actionType.length == 0) {
                this._api.get('Actions/ActionTypes')
                    .map(res => res.json())
                    .subscribe(res => {
                        this.actionType = [];
                        for (var tmpactionType of res.Data) {
                            this.actionType.push(tmpactionType);
                        }
                        this._actionTypeLoading = false;
                        this._actionTypeObserver.next(this.actionType);
                    },
                    (err) => {
                    },
                    () => {
                        this._actionTypeLoading = false;
                    });
            }
            else {
                this._actionTypeLoading = false;
                this._actionTypeObserver.next(this.actionType);
            }
        }
    }

    getPriorityStatuses() {
        if (!this._priorityLoading) {
            this._priorityLoading = true;
            if (this.priority == null || this.priority == undefined || this.priority.length == 0) {
                this._api.get('Actions/ActionTypes')
                    .map(res => res.json())
                    .subscribe(res => {
                        this.actionType = [];
                        for (var tmpactionType of res.Data) {
                            this.actionType.push(tmpactionType);
                        }
                        this._actionTypeLoading = false;
                        this._actionTypeObserver.next(this.actionType);
                    },
                    (err) => {
                    },
                    () => {
                        this._priorityLoading = false;
                    });
            }
            else {
                this._priorityLoading = false;
                this._priorityObserver.next(this.priority);
            }
        }
    }

    getAddressTypes() {
        if (!this._addressTypesLoading) {
            this._addressTypesLoading = true;
            if (this.addressTypes == null || this.addressTypes == undefined || this.addressTypes.length == 0) {
                this._api.get('Accounts/address.models')
                    .map(res => res.json())
                    .subscribe(res => {
                        this._addressTypesLoading = false;
                        if (res.IsSuccess === true) {
                            this.addressTypes = res.Data;
                            this._addressTypesObserver.next(this.addressTypes);
                            return true;
                        } else { return false; }
                    },
                    err => {
                    },
                    () => {
                        this._addressTypesLoading = false;
                    });
            }
            else {
                this._addressTypesLoading = false;
                this._addressTypesObserver.next(this.addressTypes);
            }
        }
    }

    public clear() {

    }

    ngOnDestroy() {
        if (this.orgSub) this.orgSub.unsubscribe();

    }

}