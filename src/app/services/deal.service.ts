import { Injectable } from '@angular/core';
import { Api } from './api';
import { LocalStorage } from './localstorage';
import { Subscription } from 'rxjs/Subscription';
import { SalesProcessModel } from '../models/deals.models';
import { OrgService } from './organisation.service';
import { DealsModel } from "../models/deals.models";
import { AccountsModel } from "../models/account.models";

@Injectable()

export class DealService {

    public DealStatus: any;
    public prerenderd: any;

    public saleProcess: Array<SalesProcessModel> = new Array<SalesProcessModel>();
    public settings: any;
    public SalesProcessId: string;
    public userId: string;

    public rightPanelAccount: AccountsModel;

    private orgSub: Subscription;

    constructor(
        public _api: Api,
        public _localstore: LocalStorage,
        public orgService: OrgService
    ) {

        this.orgSub = this.orgService.currentOrganisationObservable().subscribe(org => {
            if (org) this.settings = org.Settings;
        });

    }
    getAllDealStatus() {
        var orgId = this._localstore.get('orgId');
        this._api.get('Organisations/' + orgId + '/Dealstatus')
            .map(res => res.json())
            .subscribe(res => {
                for (var status of res.Data.SaleProcesses)
                    this.saleProcess.push(new SalesProcessModel(status.DealStatuses.Id, status.DealStatuses.Name));
            })
    }
    setSalesProcessId(salesProcessId: string) {
        console.log("[Deal] Setting saleprocess: " + salesProcessId);
        this.SalesProcessId = salesProcessId;
    }

    setUserId(userId: string) {
        console.log("[Deal] Setting userId: " + userId);
        this.userId = userId;
    }

    getTrelloBoard() {
        var orgId = this._localstore.get('orgId');
        let query = "?IncludeNextAction=true&IncludeAccountColor=true" + (this.userId ? "&UserId=" + this.userId : "");
        console.log("[Deal-Trello] Loading Deals: Started!");
        this._api.get('Organisations/' + orgId + '/Deals' + query).map(res => res.json())
        // this._api.get('Organisations/' + orgId + '/DealsActive' + query).map(res => res.json()) // to be implemented
            .subscribe(res2 => {
                console.log("[Deal-Trello] Loading Deals: Finished! -> Found " + res2.Data.length + " of them");
                res2.Data = res2.Data.map((x:any) => new DealsModel(x));
                this.prepareData(this.settings.SaleProcesses, res2.Data, this.SalesProcessId);
            });
    }

    prepareData(salesProcess: any, allDeals: any, selectedSales: any) {

        var deals = allDeals;
        var arraged: any = {};
        var count = 0;
        if (salesProcess) {
            for (var sales of salesProcess) {
                arraged[sales.Id] = { 'salePipe': [], 'postSales': [] };

                for (var dealStatuses of sales.DealStatuses) {
                    var dealArray: Array<any> = [];
                    var postSalesArray: Array<any> = [];
                    var forcast = 0;
                    for (var deal of deals) {
                        if (deal.DealStatusId == dealStatuses.Id && deal.SaleProcessId == sales.Id && deal) {
                            forcast = forcast + Number(deal.TotalInCompanyCurrency);
                            dealArray.push(deal);
                        }
                    }
                    if (dealStatuses.Type == 1) {
                        arraged[sales.Id]['salePipe'].push({ process: dealStatuses, forcast: forcast, deals: dealArray });
                    }
                    if (dealStatuses.Type == 2) {
                        arraged[sales.Id]['postSales'].push({ process: dealStatuses, forcast: forcast, deals: dealArray });
                    }

                }
                console.log("[Deals] Trying to sort." + sales.Id)
                console.log(arraged[sales.Id]['salePipe']);
                arraged[sales.Id]['salePipe'] = arraged[sales.Id]['salePipe'].sort((n1: any, n2: any) => {
                    if (n1.process.Percentage > n2.process.Percentage) {
                        return 1;
                    }

                    if (n1.process.Percentage < n2.process.Percentage) {
                        return -1;
                    }

                    return 0;
                });

                arraged[sales.Id]['postSales'] = arraged[sales.Id]['postSales'].sort((n1: any, n2: any) => {
                    if (n1.process.SortOrder > n2.process.SortOrder) {
                        return 1;
                    }

                    if (n1.process.SortOrder < n2.process.SortOrder) {
                        return -1;
                    }

                    return 0;
                });
            }
        }
        this.prerenderd = arraged[selectedSales];
    }

    updateDealStatus(dealId: string, dealStatusId: string, callback: (success:boolean) => any = null) {
        this._api.put('Deals/' + dealId + '/UpdateStatus/' + dealStatusId, {}).map(res => res.json()).subscribe(data => {
            if(callback) callback(data.IsSuccess);
        });
    }
}