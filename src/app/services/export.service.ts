import { Injectable } from '@angular/core';
import { Api } from './api';
import { ExportCompanyAccountModel, ExportActionModel, ExportPersonAccountModel, ExportModel } from "../models/export.models";
import { NotificationService } from "./notification-service";
import { TranslateService } from "@ngx-translate/core";
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { Observable } from "rxjs/Observable";

@Injectable()
export class ExportService {

    private dataStore: { templates: Array<any> };

    private exportTemplates$ = new BehaviorSubject<any>(null);
    public exportTemplatesObs(): Observable<any> {
        return this.exportTemplates$.asObservable();
    }

    // To use if export is aborted
    public aborted: boolean;

    constructor(private api: Api,
        private notify: NotificationService,
        private translate: TranslateService) {
        this.dataStore = { templates: [] };
    }

    downloadExport(path: string, fileName: string, contentType: string) {
        let a = document.createElement("a") as HTMLAnchorElement;
        document.body.appendChild(a);
        a.style.display = 'none';

        this.api.getDownloadFile(path)
            .subscribe(data => {
                console.log('EXPORT', data);
                var blob = new Blob([data], { type: contentType });
                var url = window.URL.createObjectURL(blob);
                a.href = url;
                a.download = fileName;
                a.click();
                window.URL.revokeObjectURL(url);
                a.parentNode.removeChild(a);
            },
            error => {
                console.log('[Export-Service] Failed!');
            });

    }

    downloadExportNew(path: string, fileName: string, contentType: string, model: ExportModel, success?: Function, fail?: Function) {
        let a = document.createElement("a") as HTMLAnchorElement;
        document.body.appendChild(a);
        a.style.display = 'none';

        this.api.postDownloadFileWithModel(path, model)
            .subscribe(data => {
                console.log('EXPORT', data);
                if (!this.aborted) {
                    var blob = new Blob([data], { type: contentType });
                    var url = window.URL.createObjectURL(blob);
                    a.href = url;
                    a.download = fileName;
                    a.click();
                    window.URL.revokeObjectURL(url);
                    a.parentNode.removeChild(a);
                    if (success) success();
                }
                this.aborted = false;
            },
            error => {
                if (!this.aborted) {
                    if (fail) fail();
                }
                this.aborted = false;
                console.log('[Export-Service] Failed!');
            });

    }

    getTemplates(type: string) {
        this.api.get('Users/ExportTemplates/' + type)
            .map(res => res.json())
            .subscribe(res => {
                if (res.IsSuccess) {
                    this.dataStore.templates = res.Data;
                    this.exportTemplates$.next(res.Data);
                }
            });
    }

    addTemplate(model: ExportModel, type: string, succFunc?: Function) {
        this.api.post('Users/ExportTemplates/' + type, model)
            .map(res => res.json())
            .subscribe(res => {
                if (res.IsSuccess) {
                    this.notify.success(this.translate.instant('ADDTEMPLATESUCCESS'));
                    this.dataStore.templates.push(res.Data);
                    this.exportTemplates$.next(this.dataStore.templates);
                    if (succFunc) succFunc();
                } else {
                    this.notify.error(this.translate.instant('ADDTEMPLATEFAIL'));
                }
            }, err => this.notify.error(this.translate.instant('ADDTEMPLATEFAIL')));
    }

    updateTemplate(model: ExportModel, type: string, succFunc?: Function) {
        this.api.put('Users/ExportTemplates/' + type, model)
            .map(res => res.json())
            .subscribe(res => {
                if (res.IsSuccess) {
                    this.notify.success(this.translate.instant('UPDATETEMPLATESUCCESS'));
                    let index = this.dataStore.templates.findIndex((x: any) => x.Id === res.Data.Id)
                    if (index > -1) {
                        this.dataStore.templates.splice(index, 1, res.Data);
                    } else {
                        this.dataStore.templates.push(res.Data);
                    }
                    this.exportTemplates$.next(this.dataStore.templates);
                    if (succFunc) succFunc();
                } else {
                    this.notify.error(this.translate.instant('UPDATETEMPLATEFAIL'));
                }
            }, err => this.notify.error(this.translate.instant('UPDATETEMPLATEFAIL')));
    }

    getFullExportModel(type: string) {
        switch (type) {
            case 'Action':
                return this.setAllPropsToTrue(new ExportActionModel());
            case 'Company':
                return this.setAllPropsToTrue(new ExportCompanyAccountModel());
            case 'Person':
                return this.setAllPropsToTrue(new ExportPersonAccountModel());
            default:
                return null;
        }
    }

    private setAllPropsToTrue(obj: ExportActionModel | ExportCompanyAccountModel | ExportPersonAccountModel) {
        for (let prop in obj) {
            if (obj.hasOwnProperty(prop)) {
                for (let innerProp in obj[prop]) {
                    if (innerProp !== 'InExport' && innerProp !== 'CustomFields') {
                        obj[prop][innerProp] = true;
                    }
                }
            }
        }
        return obj;
    }
}