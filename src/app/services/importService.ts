import { Injectable, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Api } from './api';
import { BaseService } from './baseService';
import { LocalStorage } from './localstorage';
import { NotificationService } from './notification-service';
import { ImportExcelAnswerModel } from '../models/import.models';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class ImportService extends BaseService {

    private _fileProceedObserver: any;
    public importCompleted: EventEmitter<boolean>;
    public fileProceed$: Observable<ImportExcelAnswerModel>;

    constructor(
        private _api: Api,
        private _notify: NotificationService,
        private _translate: TranslateService,
        _localStore: LocalStorage
    ) {
        super(_localStore);

        this.fileProceed$ = new Observable<ImportExcelAnswerModel>((observer: any) => {
            this._fileProceedObserver = observer;
        }).share();

        this.importCompleted = new EventEmitter<boolean>();
    }

    public uploadFile(data: FormData) {
        this._api.upload(`Accounts/UploadFileForInport`, data)
            .then(response => {
                var resp = JSON.parse(response);
                if (resp.IsSuccess || resp.isSuccess) {
                    this._fileProceedObserver.next(<ImportExcelAnswerModel>resp.data);
                    //this.clearInputFile($('#file-input')[0]);
                }
                else {
                    this._fileProceedObserver.next(resp.Label || resp.Message);
                }
            });
    }

    public importUploadedFile(model: any) {
        this._api.requestPut(
            `Accounts/ImportUploadedFile`,
            model,
            (data) => {
                this._notify.success(this._translate.instant('IMPORTEDSUCCESSFULLY'));
                if (data.DuplicateRecords) {
                    let message = this._translate.instant('IMPORTDUPLICATEACCOUNTS', { 'count': data.DuplicateRecords });
                    this.importCompleted.emit(message);
                }
                else {
                    this.importCompleted.emit(true);
                }
            },
            (error) => {
                this.importCompleted.emit(false);
                this._notify.error(error);
            }
        );
    }
}
