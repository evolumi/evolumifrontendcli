import { Injectable } from '@angular/core';
import { NotificationService } from './notification-service';
import { environment } from '../../environments/environment';

export type IntercomEvents = 'account-created'
    | 'deals-created'
    | 'years-created'
    | 'contacts-created'
    | 'action-created'
    | 'action-closed'
    | 'notes-saved'
    | 'org-created'
    | 'invited-users'
    | 'click-on-add'
    | 'workgroup-created'
    | 'chanel-created'
    | 'conversation-messages-send';

@Injectable()
export class IntercomService {

    private appId: string;

    constructor(
        public notifications: NotificationService) {
        this.appId = environment.intercomAppId;
    }

    ngOnInit() {

    }

    handleError(error: any) {
        console.log(error);
        this.notifications.error(error);
    }

    ngOnDestroy() {

    }

    public boot(userId: string) {
        console.log("[Intercom] Boot user [" + userId + "]");
        (<any>window).Intercom('boot', {
            app_id: this.appId,
            user_id: userId,
            custom_launcher_selector: "#chatLauncher"
        });

    }

    public updateAllUserData(user: any) {
        console.log("[Intercom] Update User information");
        (<any>window).Intercom('update', {
            email: user.Email,
            user_id: user.UserId,
            firstName: user.FirstName,
            lastName: user.LastName,
            language: user.Language,
            company: {
                id: user.LastOrganisationId,
                name: user.LastOrganisationName
            }
        });
    }

    public updateUserOrgInfo(userId: string, plan: string, orgId: string, orgEmail: string, orgName: string) {
        console.log("[Intercom] Update Organisation information");
        (<any>window).Intercom('update', {
            user_id: userId,
            company: {
                id: orgId,
                name: orgName,
                plan: plan,
                email: orgEmail
            }
        });
    }

    public updateOrgUserData(userId: string, userOrgId: string, userOrgName: string) {
        console.log("[Intercom] Update Organisation user-data");
        (<any>window).Intercom('update', {
            user_id: userId,
            company: {
                id: userOrgId,
                name: userOrgName
            }
        });
    }

    public shutdown() {
        console.log("[Intercom] Shutdown!");
        (<any>window).Intercom('shutdown');
    }


    public trackEvent(name: IntercomEvents) {
        console.log("[Intercom] Track Event [" + name + "]");
        (<any>window).Intercom('trackEvent', name);
    }
}
