import { Injectable } from '@angular/core';
import { Api } from './api';
import { NotificationService } from './notification-service';
import { TranslateService } from '@ngx-translate/core';
import { IntercomService } from '../services/interecomService';
import { AuthService } from './auth-service';
import { Router } from '@angular/router';

import { Subject } from "rxjs/Subject";
import { Observable } from "rxjs/Observable";
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { Configuration } from './configuration';
import { OrganisationModel, OrganisationListModel, OrganisationUpdateModel } from "../models/organisation.models";
declare var _: any;

@Injectable()
export class OrgService {

    // Current Organisation 
    private $currentOrganisation: BehaviorSubject<OrganisationModel> = new BehaviorSubject(undefined);
    public currentOrganisationObservable(): Observable<OrganisationModel> {
        return this.$currentOrganisation.asObservable();
    }
    public currentOrganisation(): OrganisationModel {
        return this.$currentOrganisation.value;
    }

    // Organisation List
    private $organisations: BehaviorSubject<Array<OrganisationListModel>> = new BehaviorSubject([]);
    public organisationsObservable(): Observable<Array<OrganisationListModel>> {
        return this.$organisations.asObservable();
    }

    //Created first org event, send new orgID
    private firstOrgCreated$: Subject<string> = new Subject();
    public firstOrgCreatedObservable(): Observable<string> {
        return this.firstOrgCreated$.asObservable();
    }

    public userRole: string;
    public plan: string;
    public creatingOrganisation: boolean;
    private loadingOrgId: string = "";
    private userId: string;

    public isLoaded: boolean = false;
    public redirectUrl: string = "";

    public inviteToOrganisation: string;
    public newOrgCreated: boolean;

    constructor(
        private api: Api,
        private auth: AuthService,
        private notify: NotificationService,
        private translate: TranslateService,
        private intercom: IntercomService,
        private config: Configuration,
        private router: Router
    ) {
        this.auth.currentUserObservable().subscribe(user => {
            if (user == undefined) {
                this.isLoaded = false;
            }
            else {
                if (!this.$currentOrganisation.value) {

                    let orgId = localStorage.getItem("orgId");
                    if (orgId && orgId != 'false' && orgId != this.loadingOrgId) {
                        console.log("[Org] No Organisation Loaded. Trying to load it from Localstorage");
                        this.getOrganisation(orgId);
                    }
                }
            }
        });
    }

    toggleInviteUserPopup(orgId: string = null) {
        if (!this.inviteToOrganisation) {
            this.inviteToOrganisation = orgId ? orgId : localStorage.getItem("orgId");
        }
        else {
            this.inviteToOrganisation = null;
            this.newOrgCreated = false;
        }
    }

    public getOrganisation(id: string) {
        if (!id) id = localStorage.getItem("orgId");
        this.loadingOrgId = id;
        console.log("[Org] Loading Organisation [" + id + "] : Started!");
        this.api.requestGet(
            `Organisations/${id}/FullInfo`,
            (data) => {
                console.log("[Org] Loading Organisation [" + id + "] : Finished! -> " + data.OrganisationName);
                this.intercom.updateOrgUserData(this.userId, id, data.OrganisationName);
                this.isLoaded = true;
                this.api.setOrganisationHeader(data.Id);
                this.$currentOrganisation.next(data);
                localStorage.setItem("orgId", data.Id);
                this.plan = data.Plan;
                this.loadingOrgId = "";
                this.userRole = data.UserRole;
                var lang = localStorage.getItem("lan");
                console.log("[Org] Checking resource file " + lang + " version " + this.config.languageFileVersion);
                this.translate.getTranslation(lang + "." + this.config.languageFileVersion).subscribe(obj => {
                    for (let key in data.CustomTitles) {
                        if (data.CustomTitles[key]) {
                            console.log("[Org] Replacing tag [" + key + "] with new value [" + data.CustomTitles[key] + "] in language file: " + lang + ".json");
                            obj[key] = data.CustomTitles[key];
                        }
                    }
                }, error => {
                    console.log("[Org] Could not find Language file: " + lang + "." + this.config.languageFileVersion);
                });
                if (this.redirectUrl) {

                    let url = this.redirectUrl;
                    this.redirectUrl = undefined;
                    console.log("[Org-Guard] Redirecting to " + url);
                    this.router.navigate([url]);
                }
            },
            error => {
                console.log("[Org-Service] Redirecting to Org selection");
                this.router.navigate(["/Overview"]);
            });
    }

    public getOrganisations(callback: (orgList: any) => any = null) {
        console.log("[Org] Loading Organisations : Started!");
        this.api.requestGet('Organisations',
            (res) => {
                console.log("[Org] Loading Organisations : Finished! -> " + res.length);
                this.$organisations.next(res);
                if (callback) callback(res);
            });
    }

    public getInvites() {
        return this.api.get("Users/Invites").map(res => res.json());
    }

    public deleteOrganisation(id: string): void {
        this.api.requestDelete(`Organisations/${id}`,
            (res) => {
                console.log("[Org] Delete Organisation [" + id + "] : Success!");
                this.notify.success(this.translate.instant("ORGDELETED"));
                this.getOrganisations();
            });
    }

    public addOrganisation(data: any, openInvitePopup: boolean = false): void {
        this.creatingOrganisation = true;
        this.api.requestPost('Organisations', data,
            (res) => {
                let newOrgId = res;
                if (this.$organisations.value.length == 0) {
                    this.firstOrgCreated$.next(res);
                }
                // get roles and invite users
                this.api.requestGet(`Organisations/${newOrgId}/Roles`,
                    (res) => {
                        this.notify.success(this.translate.instant("ORGCREATED"));
                        this.intercom.trackEvent('org-created');
                    });
                if (openInvitePopup) {
                    this.redirectUrl = "/CRM/Dashboard";
                    this.getOrganisation(newOrgId);
                    this.newOrgCreated = true;
                    this.toggleInviteUserPopup(newOrgId);
                }
                this.creatingOrganisation = false;
                this.getOrganisations();
            },
            (err) => {
                this.creatingOrganisation = false;
                this.notify.error(err);
            });
    }

    public updateOrganisation(org: OrganisationUpdateModel | OrganisationModel) {
        this.api.put('Organisations/' + org.Id, org)
            .map(res => res.json())
            .subscribe(res => {
                console.log("[Org] Update Organisation [" + org.Id + "] : Success!");
                this.notify.success(this.translate.instant("ORGUPDATED"));
                this.getOrganisation(org.Id);
            });
    }

    public publishOrganisation(org: OrganisationModel) {
        this.$currentOrganisation.next(org);
    }

    public addPhonenumber(number: any, description: string) {
        let org = this.$currentOrganisation.value;
        this.api.requestPost("Organisations/" + org.Id + "/Phonenumbers", { Number: number, Description: description }, res => {
            org.PhoneNumbers = res;
            this.$currentOrganisation.next(org);
            this.notify.success(this.translate.instant("ORGPHONEUPDATED"));
        });
    }

    public deletePhonenumber(id: string) {
        let org = this.$currentOrganisation.value;
        this.api.requestDelete("Organisations/ " + org.Id + " /Phonenumbers/" + id, res => {
            org.PhoneNumbers = res;
            this.$currentOrganisation.next(org);
            this.notify.success(this.translate.instant("ORGPHONEUPDATED"));
        });
    }

    public setDefaultPhonenumber(number: any) {
        let org = this.$currentOrganisation.value;
        this.api.requestPut("Organisations/ " + org.Id + " /Phonenumbers/SetDefault", number, res => {
            org.DefaultPhoneNumber = res;
            this.$currentOrganisation.next(org);
            this.notify.success(this.translate.instant("USERPHONEUPDATED"));
        });
    }

    public selectPhonenumber(number: any) {
        let org = this.$currentOrganisation.value;
        this.api.requestPut("Organisations/ " + org.Id + " /Phonenumbers/SetPersonal", number, res => {
            org.UserPhoneNumber = res;
            this.$currentOrganisation.next(org);
            this.notify.success(this.translate.instant("USERPHONEUPDATED"));
        });
    }

    public uploadImage(orgId: string, formData: FormData) {
        this.api.upload('Organisations/UploadLogo', formData)
            .then(res => {
                console.log("[Org] Uploaded new Logo for Organisation [" + orgId + "]");
                this.notify.success(this.translate.instant("ORGUPDATED"));
                this.getOrganisation(orgId);
            })
    }

    // private inviteUser(orgId: string, inviteData: any): void {
    //     this.api.requestPost(`Organisations/${orgId}/InviteUser`, inviteData,
    //         (res) => {
    //             this.intercom.trackEvent('invited-users');
    //             this.notify.success(this.translate.instant("INVITATIONSSENT"));
    //         },
    //         (err) => {
    //             this.notify.error(err);
    //         });
    // }

    public settingsChange(settings: any = null) {
        if (settings) {
            this.updateSettings(settings);
        }
        else {
            let orgId = localStorage.getItem("orgId");
            this.api.get(`Organisations/${orgId}/Settings`).map(res => res.json()).subscribe(res => {
                this.updateSettings(res.Data);
            })
        }
    }

    public rolesChange(roles: Array<any>) {
        let org = this.$currentOrganisation.value;
        if (roles) {
            org.Roles = roles;
            this.$currentOrganisation.next(org);
        }
        else {
            this.api.get('Organisations/' + org.Id + '/Roles')
                .map((res: any) => res.json())
                .subscribe((res: any) => {
                    org.Roles = res.Data;
                    this.$currentOrganisation.next(org);
                });
        }
    }

    private updateSettings(settings: any) {
        let org = this.$currentOrganisation.value;
        org.Settings = settings;
        this.$currentOrganisation.next(org);
    }

    public insightsChange(insights: any) {
        console.log("[Organisation] Updating Insights", insights);
        let org = this.$currentOrganisation.value;
        org.FavoriteInsights = insights;
        this.$currentOrganisation.next(org);
    }

    public accountTypesChange(accountTypes: Array<any>) {
        console.log("[Organisation] Updating AccountTypes", accountTypes);
        let org = this.$currentOrganisation.value;
        org.AccountTypes = accountTypes;
        this.$currentOrganisation.next(org);
    }

    public tagsChange(tags: Array<string>) {
        let org = this.$currentOrganisation.value;
        let needUpdate = false;
        for (let tag of tags) {
            if (!org.Tags.find(x => x == tag)) {
                org.Tags.push(tag);
                needUpdate = true;
            }
        }
        if (needUpdate) {
            this.$currentOrganisation.next(org);
            console.log("[Organisation] Updating Tags");
        }
    }

    public hasPermission(permission: Permissions | string): boolean {
        if (this.auth.isEvolumi) return true;
        for (let p of this.$currentOrganisation.value.UserPermissions) {
            if (p == permission || p == Permissions[<Permissions>permission])
                return true;
        }
        return false;
    }

    public hasFeature(feature: Features | string): boolean {
        if (this.auth.isEvolumi) return true;
        for (let f of this.$currentOrganisation.value.Features) {
            if (f == feature || f == Features[<Features>feature]) {
                return true;
            }
        }
        return false;
    }

    public labelsChange(labels: Array<string>) {
        let org = this.$currentOrganisation.value;
        let needUpdate = false;
        for (let label of labels) {
            if (!org.Labels.find(x => x == label)) {
                org.Labels.push(label);
                needUpdate = true;
            }
        }
        if (needUpdate) {
            this.$currentOrganisation.next(org);
            console.log("[Organisation] Updating Labels");
        }
    }

    public getCreateOptions(callback: (data: any) => any) {
        this.api.get("Organisations/OrgAnswers").map(res => res.json()).subscribe(result => {
            if (result.IsSuccess) {
                callback(result.Data);
            }
        })
    }

    public setDefaultRole(roleId: string, callback: (res: boolean) => any = null) {
        this.api.post("Roles/DefaultRole", roleId).map(result => result.json()).subscribe(data => {
            if (callback) {
                callback(data.IsSuccess);
            }
        })
    }
}

export enum Features {
    Branding = 1,
    ESign = 10,
    Contracts = 15,
    Filters = 20,
    MailTemplates = 30,
    MailSignature = 31,
    EmailTracking = 32,
    Goals = 40,
    SalesCompetition = 41,
    SalesCommisions = 42,
    Reports = 45,
    Products = 50,
    Click2Call = 60,
    GuestUsers = 70,
    CustomLabels = 80,
    CustomFields = 81,
    Storage5GIG = 90,
    CalendarSync = 100,
    DocumentTemplates = 110
}

export enum Permissions {
    ViewAccounts = 1,
    ViewOwnAccounts,
    AddAccounts,
    ImportAccounts,
    EditAccounts,
    EditOwnAccounts,
    DeleteAccounts,
    DeleteOwnAccount,
    DeleteDeals,
    DeleteOwnDeals,
    DeleteActions,
    DeleteOwnActions,
    DeleteContacts,
    DeleteOwnContacts,
    HandleUsers,
    HandleUserRoles,
    HandleGoals,
    HandleProducts,
    HandleDealStatus,
    HandlePayment,
    ViewSalesBoard,
    ChangeOwner,
    DeleteAccount,
    HandlePaymentDetails,
    HandleGeneralSettings,
    HandleContracts,
    ViewContracts,
    DeleteContracts,
    CreateContracts,
    ViewSalesCompetitions,
    CreateSalesCompetitions,
    HandleSalesCommissions,
    EvolumiAdmin,
    Export,
    EditOrganisationNotes
};