import { Injectable } from '@angular/core';
import { LocalStorage } from './localstorage';
import { NotificationService } from './notification-service';
import { Api } from './api';
import { IntercomService } from '../services/interecomService';
import { DealsModel } from "../models/deals.models"


@Injectable()
export class DealAddEdit {

	public deal: DealsModel;

	constructor(
		public intercomService: IntercomService,
		public _http: Api, public _local: LocalStorage, public _notify: NotificationService) { }


	ngOnInit() {

	}

	updateDealValue() {
		if (this.deal.ProductItems.length != 0) {
			var sum: number = this.deal.ProductItems.reduce(function (res: number, current: any) {
				res += Number(current.Sum);
				return res;
			}, 0);
			this.deal.Value = Number(sum);
		}
	}

	getDealById(id: string) {
		this._http.get('Deals/' + id)
			.map(res => res.json()).subscribe(res => {
				this.showDeal(res.Data);
			});
	}

	showDeal(deal: any) {
		this.deal = deal;
		console.log(deal);
		if (this.deal.Id) {
			this.deal.Editmode = false;
			this.updateDealValue();
			if (this.deal.ProductItems.length > 0) {
				this.deal.DealAsProducts = true;
			}
		}
		else {
			this.deal.Editmode = true;
			this.deal.Value = 0;
		}
	}

	clear() {
		this.deal = null;
	}

	AddOrUpdateDeal(success: () => any) {
		if (this.validateDeals(this.deal)) {
			console.log('DEAL', this.deal);
			if (this.deal.Id) {
				this.updateDealApi(this.deal).subscribe((data: any) => {
					if (data.IsSuccess) {
						success();
					}
				});
			}
			else {
				this.createDealApi(this.deal).subscribe((data: any) => {
					if (data.IsSuccess) {
						if (this.deal)
							success();
						this.clear();
					}
				});
			}
		}
	}


	updateDealApi(deal: any): any {
		this._notify.success("Deal updated");
		return this._http.put('/Deals/' + deal.Id, deal)
			.map(res => res.json())

	}

	createDealApi(deal: any): any {
		let orgId = this._local.get('orgId');
		this.intercomService.trackEvent('deals-created');
		this._notify.success("Deal added");
		deal.Currency = deal.CurrencyName;
		return this._http.post('Organisations/' + orgId + '/Deals', deal)
			.map(res => res.json());
	}

	checkOwnerAndPercentage(owners: Array<any>) {
		var ownersSoFar = Object.create(null);
		var percentage = 0;
		for (var i = 0; i < owners.length; ++i) {
			var value = owners[i].UserId;
			percentage = Number(percentage) + Number(owners[i].Percent);
			if (value in ownersSoFar) { this._notify.error('Duplicate Owner found'); return true; }
			ownersSoFar[value] = true;
		}
		if (percentage != 100) { this._notify.error('Sum of percentage should be 100'); return true; }
		return false;
	}

	checkDate(deal: any) {
		if (deal.DateInvoice > deal.DateInvoiceExpire) {
			this._notify.error('Date of invoice must be less than or equal to Invoice expire date'); return true;
		}
		if (deal.DateStart > deal.DateEnd) {
			this._notify.error('Deal start date must be less than or equal to deal end date'); return true;
		}
		return false;
	}

	validateDeals(deals: any) {
		var validationresults = (!this.checkOwnerAndPercentage(deals.Owners) && !this.checkDate(deals));
		return (validationresults) ? true : false;
	}
}
