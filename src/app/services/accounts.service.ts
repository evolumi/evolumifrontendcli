import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Api } from './api';
import { TranslateService } from '@ngx-translate/core';

import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { Subject } from "rxjs/Subject";
import { Observable } from 'rxjs/Observable';

import { AccountsModel } from '../models/account.models';
import { AddressModelBilling } from '../models/address.models';
import { DocumentModel } from '../models/document.models';
import { NotificationService } from './notification-service';
import { IntercomService } from './interecomService';
import { OrgService } from './organisation.service';
import { ExportService } from './export.service';
import { DealListing } from "./deallisting.service";
import { DealsModel } from "../models/deals.models";
import { AccountRelation } from "../models/account.models";

import { ChosenOptionModel } from '../modules/shared/components/chosen/chosen.models';
import { MapService, CollorArray } from './map.service';
import { ExportModel } from "../models/export.models";

export interface searchParams {
    SortingFieldName: string,
    ParentCompany: string,

    AccountTypeId: Array<string>,
    AccountManager: Array<string>,
    Tags: Array<string>,
    Owners: Array<string>,
    AccountIds: Array<string>,

    Search: string,
    AccountId: string,
    Page: number,
    PageSize: number,
    UserId: string,
    SortingReversed: boolean,
    IsPerson?: boolean;
    scroll: boolean;
    [key: string]: any
}

export class SearchParams implements searchParams {
    SortingFieldName: string;
    ParentCompany: string;

    AccountTypeId: Array<string>;
    AccountManager: Array<string>;
    Tags: Array<string>;
    Owners: Array<string>;
    AccountIds: Array<string>;

    Search: string;
    AccountId: string;
    Page: number;
    PageSize: number;
    UserId: string;
    SortingReversed: boolean;
    IsPerson?: boolean = undefined;
    scroll: boolean = false;

    TagsAll: boolean;

    [key: string]: any;

    constructor(params: any = null) {
        if (params) {
            Object.assign(this, params)
        }
    };
}

export class FinancialStatement {
    Employee: string;
    Revenue: string;
    Year: string;
    CurrencyName: any;
    Currency: any;
}

export class filterContainer {
    Filter: searchParams;
    Id: string;
    Name: string;
}

@Injectable()

export class AccountsCollection {
    public orgId: any;

    public searchParams: searchParams;
    private searchParams$: Subject<searchParams> = new Subject();
    private searchObservable(): Observable<searchParams> {
        return this.searchParams$.asObservable();
    }

    private currentAccount$: BehaviorSubject<AccountsModel> = new BehaviorSubject(undefined);
    public currentAccountObserver(): Observable<AccountsModel> {
        return this.currentAccount$.asObservable();
    }

    private accountsChanges$: Subject<AccountsModel> = new Subject();
    public accountChange(): Observable<AccountsModel> {
        return this.accountsChanges$.asObservable();
    }

    public currentAccount: AccountsModel = new AccountsModel();
    public currentAddress: AddressModelBilling = new AddressModelBilling();

    public accounts: Array<AccountsModel> = new Array<AccountsModel>();

    public account: AccountsModel = new AccountsModel();
    public address: AddressModelBilling = new AddressModelBilling();

    public apiInProgress: boolean = false;
    public endOfLine: boolean = false;

    public selectAll: boolean;
    public selectedAccounts: Array<any> = [];
    public selectedOption: string = 'selected';

    private popupAccount$: Subject<AccountRelation> = new Subject();
    public popupAccountObservable(): Observable<AccountRelation> {
        return this.popupAccount$.asObservable();
    }

    private contactAccounts$ = new BehaviorSubject<AccountRelation[]>([]);
    public contactAccountsObservable(): Observable<AccountRelation[]> {
        return this.contactAccounts$.asObservable();
    }

    private accountAddedObservable$: Subject<string> = new Subject<string>();
    public accountAddedObservable(): Observable<string> {
        return this.accountAddedObservable$.asObservable();
    }

    public tagList: Array<ChosenOptionModel> = [];
    public accountTypeList: Array<ChosenOptionModel> = [];

    public filters: Array<filterContainer> = [];
    public defaultFilter: string;

    private loadigRelatedAccountsFor: string;
    private loadigDocumentsFor: string;
    private orgSub: any;

    public addingContactForAccount: AccountsModel;
    public forceNeeded: string;

    private addressTypes$: BehaviorSubject<Array<string>> = new BehaviorSubject([]);
    public getAddressTypes(): Observable<Array<string>> {
        return this.addressTypes$.asObservable();
    }

    private defaultPriceList: any;


    constructor(public _http: Api,
        private translate: TranslateService,
        public _notify: NotificationService,
        private router: Router,
        private exporter: ExportService,
        public orgService: OrgService,
        private dealListing: DealListing,
        private _map: MapService,
        public intercomService: IntercomService) {
        this.reset();

        this._http.get('Accounts/AddressTypes')
            .map(res => res.json())
            .subscribe(res => {
                if (res.IsSuccess === true) {
                    this.addressTypes$.next(res.Data)
                }
            });

        this.orgSub = this.orgService.currentOrganisationObservable().subscribe(org => {
            if (org) {

                this.filters = [];
                for (let filter of org.AccountFilters)
                    this.filters.push(filter);
                this.defaultFilter = org.DefaultAccountFilter;

                this.tagList = [];
                for (let tag of org.Tags) {
                    this.tagList.push(new ChosenOptionModel(tag, tag));
                }
                this.accountTypeList = [];
                for (let accType of org.AccountTypes) {
                    this.accountTypeList.push(new ChosenOptionModel(accType.Id, accType.Name))
                }

                this.defaultPriceList = org.PriceLists.find(x => x.Id == org.DefaultPriceList) || org.PriceLists[0];
            }
        });

        this.searchObservable().switchMap(search => {
            if (!search.scroll) {
                this.accounts = search.scroll ? this.accounts : [];
                this.selectAll = false;
            }
            return this.doSearch(search)
        }).subscribe(accounts => {
            var accs: Array<AccountsModel> = [];

            for (var account of accounts) {
                account.IsChecked = this.selectAll;
                accs.push(account);
            }
            this.accounts = [...this.accounts, ...accs]
            this.endOfLine = accounts.length == 0;
        })
    }

    accountIsPerson(id: string, callback: (isPerson: boolean) => any) {
        this._http.get(`Accounts/${id}/IsPerson`).map(res => res.json().Data).subscribe(isPerson => {
            callback(isPerson);
        })
    }


    //reset all old search queries
    reset(isPerson: boolean = undefined) {
        this.searchParams = { SortingFieldName: '', SortingReversed: false, ParentCompany: '', AccountTypeId: [], AccountManager: [], AccountIds: [], Tags: [], Owners: [], Search: '', AccountId: '', Page: 1, PageSize: 40, UserId: '', IsPerson: isPerson, scroll: false };
    }

    public getAccountTypeColor(accountId: string): Observable<string> {

        return this._http.get(`Accounts/${accountId}/Color`).map(res => res.json().Data);

    }

    public showMap() {
        let addressList: any[] = []

        if (this.selectAll) {
            this.getAddressesForSelectedAccounts().subscribe(res => {
                if (res.IsSuccess) {
                    for (let address of res.Data) {
                        address.MarkerCollor = CollorArray[address.Type];
                        addressList.push(address);
                    }
                    if (addressList.length) {
                        this._map.showLocations(addressList);
                    }
                }
            })

        }
        else {
            for (let accountId of this.selectedAccounts) {
                let choosenAccount = this.accounts.find(x => x.Id == accountId);
                if (choosenAccount.Address) {
                    for (let address of choosenAccount.Address) {
                        address.AccountId = choosenAccount.Id;
                        address.MarkerCollor = CollorArray[address.Type];
                        address.AccountName = choosenAccount.ShownName;
                        addressList.push(address)
                    }
                }
            }
            this._map.showLocations(addressList);
        }
    }

    public getAddressesForSelectedAccounts() {
        let query = this.getSearchString();
        return this._http.get(`Accounts/GetAddresses?${query}`).map(res => res.json());
    }

    // Filter handling.
    public setFilter(filter: filterContainer) {
        this.searchParams = null;
        this.searchParams = new SearchParams(filter.Filter);
        this.getAccounts();
    }

    public addorUpdateFilter(filter: filterContainer) {
        if (filter.Id) {
            this._http.put(`AccountFilters/${filter.Id}`, filter).map(res => res.json()).subscribe(res => {
                if (res.IsSuccess) {
                    this._notify.success(this.translate.instant("SUCCESS"));
                }
                else {
                    this._notify.error(this.translate.instant("FAILED"));
                }
            })
        }
        else {
            this._http.post(`AccountFilters`, filter).map(res => res.json()).subscribe(res => {
                if (res.IsSuccess) {
                    this._notify.success(this.translate.instant("SUCCESS"));
                    this.filters.push(res.Data);
                }
                else {
                    this._notify.error(this.translate.instant("FAILED"));
                }
            })
        }
    }

    public deleteFilter(id: string) {
        this._http.delete(`AccountFilters/${id}`).map(res => res.json()).subscribe(res => {
            if (res.IsSuccess) {
                this._notify.success(this.translate.instant("SUCCESS"))
                let filter = this.filters.find(x => x.Id == id);
                this.filters.splice(this.filters.indexOf(filter));
            }
            else {
                this._notify.error(this.translate.instant("FAILED"))
            }
        })
    }

    public updateDefaultFilter(id: string) {
        this._http.post(`AccountFilters/Default`, id).map(res => res.json()).subscribe(res => {
            if (res.IsSuccess) {
                this._notify.success(this.translate.instant("SUCCESS"));
                this.defaultFilter = id;
            }
            else {
                this._notify.error(this.translate.instant("FAILED"));
            }
        });
    }

    public resetFilters() {
        this.reset();
    }

    // Generate search query
    public getSearchString() {
        var str: Array<string> = [];
        // Reverse sort order for dateCreated.
        this.searchParams.SortingReversed = this.searchParams.SortingFieldName == "DateCreated" || this.searchParams.SortingFieldName == "CustomData.f2b2a1a7-d695-4c82-89f4-58204a6631ed";

        for (let p in this.searchParams) {
            if (this.searchParams.hasOwnProperty(p)) {
                if (typeof (this.searchParams[p]) == "object") {
                    for (let i in this.searchParams[p]) {
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(this.searchParams[p][i]));
                    }
                }
                else {
                    if (this.searchParams[p] === '' || this.searchParams[p] === 'All' || typeof this.searchParams[p] === 'undefined')
                        continue;
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(this.searchParams[p]));
                }
            }
        }
        return str.join("&");
    }

    public accountList(searchString: string, page: number, success: (accountList: Array<ChosenOptionModel>) => any, fail: (err: Error) => any = null) {
        let query = `?Search=${searchString}&Page=${page}&PageSize=50`
        this.getAccountList(query, page, success, fail);
    }

    public companyList(searchString: string, page: number, success: (accountList: Array<ChosenOptionModel>) => any, fail: (err: Error) => any = null) {
        let query = `?IsPerson=false&Search=${searchString}&Page=${page}&PageSize=50`
        this.getAccountList(query, page, success, fail);
    }

    public personList(searchString: string, page: number, success: (accountList: Array<ChosenOptionModel>) => any, fail: (err: Error) => any = null) {
        let query = `?IsPerson=true&Search=${searchString}&Page=${page}&PageSize=50`
        this.getAccountList(query, page, success, fail);
    }


    private getAccountList(query: string, page: number, success: (accountList: Array<ChosenOptionModel>) => any, fail: (err: Error) => any = null) {
        this._http.get(`Accounts/List${query}`).map(res => res.json()).subscribe(res => {
            if (res.IsSuccess) {
                let list: Array<ChosenOptionModel> = [];
                for (var a of res.Data) {
                    list.push(new ChosenOptionModel(a.Id, a.Name));
                }
                if (success) success(list);
            }
            else {
                if (fail) fail(res);
            }
        }, err => {
            if (fail) fail(err);
        });
    }

    // Get accounts from Api
    getAccounts(scroll = false) {
        if (!scroll) {
            this.searchParams.Page = 1;
        }
        this.searchParams.scroll = scroll;
        this.searchParams$.next(this.searchParams);
    }

    doSearch(search: searchParams): Observable<Array<AccountsModel>> {
        this.apiInProgress = true;
        console.log("SearchParams:", this.searchParams);
        let query = this.getSearchString();
        return this._http.get(`Accounts?${query}`)
            .map(res => {
                let result = res.json();
                this.apiInProgress = false;
                return result.Data;
            });
    }

    // update all loaded accounts. from server
    updateAccounts() {

        let savePage = this.searchParams.Page;
        let saveSize = this.searchParams.PageSize;

        this.searchParams.PageSize = this.searchParams.Page * this.searchParams.PageSize;
        this.searchParams.Page = 1;

        let query = this.getSearchString();
        this._http.get(`Accounts?${query}`)
            .map(res => res.json())
            .subscribe(res => {
                this.apiInProgress = false;
                this.accounts = [];
                for (var account of res.Data) {
                    account.IsChecked = false;
                    this.accounts = [...this.accounts, new AccountsModel(account)]

                }
            });

        this.searchParams.PageSize = saveSize;
        this.searchParams.Page = savePage;
    }

    //export accounts 
    exportAccounts() {
        let query = this.getSearchString();
        const path = `Accounts/ExportAccounts?${query}`;
        let date = new Date();
        const fileName = `Account_export_${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}.xlsx`;
        const contentType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
        this.exporter.downloadExport(path, fileName, contentType);
    }

    exportAccountsNew(model: ExportModel, type: string, success?: Function, fail?: Function) {
        if (this.selectedAccounts.length) this.searchParams.AccountIds = this.selectedAccounts;
        let query = this.getSearchString();
        const path = `Accounts/Export?${query}`;
        let date = new Date();
        const fileName = `${type}Account_export_${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}.xlsx`;
        const contentType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
        this.exporter.downloadExportNew(path, fileName, contentType, model, success, fail);
    }

    onScrollBottom() {
        if (!this.apiInProgress && !this.endOfLine) {
            this.apiInProgress = true;
            this.searchParams.Page = (this.searchParams.Page | 0) + 1;
            this.getAccounts(true);
        }
    }

    public search() {
        this.searchParams = new SearchParams(this.searchParams);
        this.getAccounts();
    }

    public UpdateSearchFields(updateArray: any, isPerson?: boolean) {
        this.searchParams.Page = 1;

        var params = new SearchParams();
        for (var arr in updateArray) {
            (updateArray[arr] === 'All') ? params[arr] = '' : params[arr] = updateArray[arr];
        }
        this.searchParams = params;
        this.getAccounts();
    }

    deleteAsContactFromAccountsAndDeals(accountIds: string[]) {
        let curAccChanged = false;
        let dealsChanged = false;
        let accsChanged = false;
        for (let i = 0; i < accountIds.length; i++) {
            this.dealListing.deals.map((x: DealsModel) => {
                if (x.ContactAccounts && x.ContactAccounts.length > 0) {
                    let index = x.ContactAccounts.indexOf(accountIds[i]);
                    if (index != -1) {
                        x.ContactAccounts.splice(index, 1);
                        dealsChanged = true;
                    }
                }
            })

            this.accounts.map((x: AccountsModel) => {
                if (x.ContactAccounts && x.ContactAccounts.length > 0) {
                    let index = x.ContactAccounts.indexOf(accountIds[i]);
                    if (index != -1) {
                        x.ContactAccounts.splice(index, 1);
                        accsChanged = true;
                    }
                }
            })

            if (this.currentAccount.ContactAccounts && this.currentAccount.ContactAccounts.length > 0) {
                let index = this.currentAccount.ContactAccounts.indexOf(accountIds[i]);
                if (index != -1) {
                    this.currentAccount.ContactAccounts.splice(index, 1);
                    curAccChanged = true;
                }
            }
        }
        if (curAccChanged) {
            this.currentAccount$.next(this.currentAccount);
        }
    }

    getAccountDetails(accountId: string) {
        let loadedAccount = this.accounts.find(x => x.Id == accountId);
        if (loadedAccount != null) {
            return new BehaviorSubject<AccountsModel>(loadedAccount);
        }
        else {
            return this._http.get("Accounts/" + accountId).map(res => res.json().Data);
        }
    }

    showAccountRelationPopup(relation: AccountRelation) {
        console.log("[Account] Showing Relation", relation);
        this.popupAccount$.next(relation);
    }

    // delete account
    deleteAccount(accId: string) {
        this._http.delete('Accounts', accId)
            .map(res => res.json())
            .subscribe(body => {
                if (body.IsSuccess === true) {
                    let ind = this.accounts.findIndex(x => x.Id == accId);
                    (<any>this.accounts).splice(ind, 1);
                    this._notify.success(this.translate.instant("ACCDELETED"));
                    this.deleteAsContactFromAccountsAndDeals([accId]);
                    this.getAccounts();

                }
            });
    }

    deleteMany(ids: any) {
        this.selectedOption = 'selected';

        var searchQuery: string = "";
        if (this.selectAll) {
            searchQuery = "/?" + this.getSearchString();
        }

        this._http.post('Accounts/DeleteMany' + searchQuery, { GetAccountsFromFilter: this.selectAll, Ids: ids })
            .map(res => res.json())
            .subscribe(body => {
                this.selectedOption = 'selected';
                if (body.IsSuccess === true) {
                    this.selectedAccounts = [];
                    for (let a of ids) {
                        let ind = this.accounts.findIndex(x => x.Id === a);
                        this.accounts.splice(ind, 1);
                    }
                    this._notify.success(this.translate.instant("ACCSDELETED"));
                    this.updateAccount();
                }
            });
    }


    //add account
    addAccount(accountModel: AccountsModel, takeMeToNewAccount: boolean): Observable<any> {
        if (accountModel.IsPerson) {
            accountModel.ShownName = `${accountModel.FirstName} ${accountModel.LastName}`;
        }

        return this._http.post('Accounts', accountModel)
            .map(res => {
                var response = res.json();
                if (response.IsSuccess) {
                    this.intercomService.trackEvent('account-created');
                    this._notify.success('Account created');
                    this.getAccounts();

                    if (takeMeToNewAccount) {
                        if (accountModel.IsPerson) {
                            this.router.navigate(['/CRM/PersonAccounts/', response.Data]);
                        }
                        else {
                            this.router.navigate(['/CRM/Accounts/', response.Data]);
                        }
                    }

                    this.orgService.tagsChange(accountModel.Tags);
                }
                else {
                    let message = response.Label ? this.translate.instant(response.Label) : this.translate.instant("ACCCREATEFAIL");
                    this._notify.error(this.translate.instant(message));
                    console.log("[Account] Create Failed!", response.Message);
                }
                return response;
            });
    }

    // Load currentAccount by id. Can also load currentAddress if Id is provided.
    getAccount(accountId: string, addressId = '', force: boolean = false, success: (acc: AccountsModel) => any = null) {

        console.log("[Account] Get Account: [" + accountId + "] Started!");

        // Check if already loaded.
        if (this.currentAccount && this.currentAccount.Id == accountId && !force) {
            console.log("[Account] Get Account: [" + accountId + "] Success!. Account was already loaded.. -> " + this.account.ShownName);
            if (success) success(this.currentAccount);

        }

        // Then check if it account can be fetched from account list.
        else if (!force && !this.forceNeeded) {
            this.accounts.forEach(acc => {
                if (acc.Id == accountId) {
                    this.setAccount(acc);
                    console.log("[Account] Get Account: [" + accountId + "] Success!. Loaded from AccountList -> " + this.account.ShownName);
                    if (addressId) this.getAddress(addressId);
                    if (success) success(acc);
                }
            });
        }

        // Else get data from API.
        if (this.currentAccount == undefined || this.currentAccount.Id != accountId || force) {
            this._http.get('Accounts/' + accountId)
                .map(res => res.json())
                .subscribe(a => {
                    if (a.IsSuccess) {
                        this.setAccount(a.Data);
                        console.log("[Account] Get Account [" + accountId + "] Success! -> " + this.account.ShownName);
                        if (addressId) this.getAddress(addressId);
                        if (success) success(this.currentAccount);
                    }
                    else {
                        console.log("[Account] Get Account [" + accountId + "] Failed!");
                    }
                }, err => {
                    console.log("[Account] Get Account [" + accountId + "] Failed!");
                });
        }

        // If Related Accounts not laoded. Get them.
        if (this.currentAccount && !this.currentAccount.IsPerson && this.currentAccount.RelatedAccounts == undefined) {
            this.getRelatedAccounts(accountId);
        }
    }

    private setAccount(account: AccountsModel) {

        if (account && (account.PriceListId == "00000000-0000-0000-0000-000000000000" || !account.PriceListId)) {
            account.PriceListId = this.defaultPriceList.Id;
            account.PriceList = this.defaultPriceList;
            console.log("Setting pricelist", account.PriceListId);
        }
        this.currentAccount = account;
        this.currentAccount$.next(account);
        this.account = new AccountsModel(account);
        this.getDocuments(account.Id);
        if (account.IsPerson)
            this.getRelatedAccounts(account.Id);
    }

    // Load Documents for CurrentAccount.
    public getDocuments(accountId: string) {

        if (this.loadigDocumentsFor != accountId) {
            this.loadigDocumentsFor = accountId;
            console.log("[Account] Get Documents for [" + accountId + "] Started!");
            this._http.get(`Accounts/${accountId}/Documents`)
                .map(res => res.json())
                .subscribe(res => {
                    this.apiInProgress = false;
                    this.currentAccount.Documents = [];
                    res.Data.forEach((doc: any) => {
                        this.currentAccount.Documents.push(new DocumentModel(doc));
                    });
                    console.log("[Account] Get Documents for [" + accountId + "] Success! -> Found " + this.currentAccount.Documents.length + " of them");
                    this.loadigDocumentsFor = "";
                }, error => {
                    console.log("[Account] Get Documents for [" + accountId + "] Fail! -> " + error);
                    this.loadigDocumentsFor = "";
                });
        }
    }

    // Load Related accounts for CurrentAccount.
    private getRelatedAccounts(accountId: string) {

        if (this.loadigRelatedAccountsFor != accountId) {
            this.loadigRelatedAccountsFor = accountId;

            console.log("[Account] Get Related Accounts for [" + accountId + "] Started!");

            let query = encodeURIComponent("ParentCompany") + "=" + encodeURIComponent(accountId) + "&IsPerson=false";

            var sub = this._http.get(`Accounts?${query}`);

            sub.map(res => res.json()).subscribe(res => {
                if (res.IsSuccess) {
                    this.apiInProgress = false;
                    this.currentAccount.RelatedAccounts = [];
                    res.Data.forEach((acc: any) => {
                        let related = new AccountsModel(acc);
                        this.currentAccount.RelatedAccounts.push(related);
                        this.currentAccount$.next(this.currentAccount);
                    });

                    console.log("[Account] Get Related Accounts for [" + accountId + "] Success! -> Found " + this.currentAccount.RelatedAccounts.length + " of them");
                    this.loadigRelatedAccountsFor = "";
                }
            }, error => {
                console.log("[Account] Get Related Accounts for [" + accountId + "] Fail! -> " + error);
                this.loadigRelatedAccountsFor = "";
            });
        }
    }


    //create an account instance
    createAccount() {
        this.account = new AccountsModel();
    }

    addAccountAsContact(accountId: string, contactAccountId: string, callback: () => any = null) {
        let acc = this.accounts.find(x => x.Id == accountId);

        if (acc) {
            acc.ContactAccounts.push(contactAccountId);
            this._http.put(`Accounts/${acc.Id}`, acc).map(res => res.json()).subscribe(data => {
                if (data.IsSuccess) {
                    console.log("[Account] Contact " + contactAccountId + " added to [" + acc.ShownName + "]");
                    if (callback) {
                        callback();
                    }
                }
                else {
                    console.log("nowork: ", acc, this.currentAccount);
                }
            });
        }
        else {
            console.log("no account found");
        }
    }
    saveRelation(relation: AccountRelation, callback: () => any = null) {
        let call = relation.Id == null ? this._http.post("Accounts/Relation", relation) : this._http.put("Accounts/Relation/" + relation.Id, relation);
        call.map(res => res.json()).subscribe(data => {
            if (data.IsSuccess) {
                console.log("new Relation:", data.Data);
                if (!relation.Id) {
                    relation.PersonId = data.Data.PersonId;
                    relation.Id = data.Data.Id;
                    console.log("new Relation:", relation);
                    this.contactAccounts$.value.push(relation);
                    this.contactAccounts$.next(this.contactAccounts$.value);
                }
                if (callback) {
                    callback();
                }
            }
        });
    }

    saveRelations(relations: AccountRelation[], callback: (success: boolean) => any = null) {
        this._http.put("Accounts/Relation/UpdateMany", relations).map(res => res.json()).subscribe(data => {
            callback(data.IsSuccess);
        });
    }

    removeAccountRelation(relationId: string, callback: () => any) {
        this._http.delete(`Accounts/Relation/${relationId}`).map(res => res.json()).subscribe(data => {
            if (data.IsSuccess) {
                console.log("[Account] Relation " + relationId + " removed");
                if (callback) {
                    callback();
                }
            }
        });
    }

    //edit account 
    editAccount() {
        if (this.account.IsPerson) {
            this.account.ShownName = `${this.account.FirstName} ${this.account.LastName}`;
        }

        this._http.put('Accounts/' + this.account.Id, this.account)
            .map(res => res.json())
            .subscribe(res => {
                console.log("[Account] Updated account [" + this.account.Id + "] Success!");
                this._notify.success(this.translate.instant("ACCUPDATED"));
                this.updateAccount(res.Data);
                this.orgService.tagsChange(this.account.Tags);
            });
    }

    public updateAccount(account: AccountsModel = null) {

        if (account) {
            let acc = new AccountsModel(account);

            if (this.currentAccount && this.currentAccount.Id == account.Id) {
                this.currentAccount = acc;
                this.currentAccount$.next(acc);
            }

            let index = this.accounts.findIndex(x => x.Id == account.Id);
            if (index != -1) {
                console.log("[Account-List] Uppdating account", acc);
                this.accounts.splice(index, 1, acc);
                this.accounts = [...this.accounts];
            }
            else {
                this.getAccounts();
            }
        }
        else {
            this.getAccounts();
        }
        this.accountsChanges$.next(account);
    }

    //cancel edit
    editAccountCancelled() {
    }

    //address section
    createAddressModel() {
        this.address = new AddressModelBilling();
    }


    // Add new Address to currentAccount.
    addAddress() {
        console.log("Add Address");
        this._map.setGeoCode(this.currentAddress, (address: AddressModelBilling) => {
            this.currentAddress = address;
            console.log("Add Address", address);
            // Make sure Account is loaded.
            if (this.currentAccount == undefined) {
                console.log("[Account] Add Address: Fail! -> No Account loaded.");
                return;
            }

            // Add adress to Current account and call API.
            this._http.post('Accounts/' + this.currentAccount.Id + '/Address', this.currentAddress)
                .map(res => res.json())
                .subscribe(a => {
                    console.log("[Account] Add Address: Success!.");
                    this._notify.success(this.translate.instant("ADDRESSADDED"));
                    this.currentAccount.Address = a.Data;
                    this.currentAccount$.next(this.currentAccount);
                    this.currentAddress = new AddressModelBilling();
                    this.updateAccount(this.currentAccount);
                });
        });
    }

    public getYear(yearId: string, accountId = '') {

        console.log("[Account] Loading year id: " + yearId);

        // Check that the account is loaded.
        if (accountId) {
            if (this.currentAccount == undefined || this.currentAccount.Id != accountId)
                this.getAccount(accountId, yearId);
        }

        // Return if no account could be found.
        if (this.currentAccount == undefined) {
            console.log("[Account] Get Year: Fail! -> No Account loaded.");
            return;
        }

        // Get address.
        if (this.currentAccount.FinancialStatements.length) {
            this.currentAccount.FinancialStatements.forEach((year: any) => {
                if (year.Year == yearId) {
                    console.log("[Account] Years loaded!");
                }
            });
        }
    }

    // Load Address from Account.
    public getAddress(addressId: string, accountId = '') {

        console.log("[Account] Loading address id: " + addressId);

        // Check that the account is loaded.
        if (accountId) {
            if (this.currentAccount == undefined || this.currentAccount.Id != accountId)
                this.getAccount(accountId, addressId);
        }

        // Return if no account could be found.
        if (this.currentAccount == undefined) {
            console.log("[Account] Get Address: Fail! -> No Account loaded.");
            return;
        }

        // Get address.
        if (this.currentAccount.Address.length) {
            this.currentAccount.Address.forEach((address: any) => {
                if (address.Id == addressId) {
                    this.currentAddress = address;
                    console.log("[Account] Address loaded!");
                }
            });
        }
    }

    updateAddressWithLongAndLat(incoming: any, account: any) {

        // this._map.setGeoCode(incoming, (adress => {
        //     this._http.put('Accounts/' + account.Id + '/UpdateLongLat', adress)
        //         .map(res => res.json())
        //         .subscribe(a => {
        //             this.getAccount(account.Id);
        //             console.log("[Account] AddressLongLat Update: Success!");
        //         }, err => {
        //             console.log("[Account] AddressLongLat Update: Fail! -> Unknown error :(");
        //         });
        // }));
    }

    // update address
    updateAddress() {
        this._map.setGeoCode(this.currentAddress, address => {
            this.currentAddress = address;
            // Make sure this is a existing Address.
            if (this.currentAddress.Id == undefined) {
                console.log("[Account] Address Update: Fail! -> Id is missing.");
                return;
            }

            // Make sure Account is loaded.
            if (this.currentAccount == undefined) {
                console.log("[Account] Address Update: Fail! -> No account loaded.");
                return;
            }

            // Update adress on currentAccount and call API.
            this._http.put('Accounts/' + this.currentAccount.Id + '/Address/' + this.currentAddress.Id, this.currentAddress)
                .map(res => res.json())
                .subscribe(a => {
                    this._notify.success(this.translate.instant("ADDRESSUPDATED"));
                    this.currentAccount.Address = a.Data;
                    this.currentAccount$.next(this.currentAccount);
                    this.currentAddress = new AddressModelBilling();
                    this.updateAccount(this.currentAccount);
                    console.log("[Account] Address Update: Success!");
                }, err => {
                    console.log("[Account] Address Update: Fail! -> Unknown error :(");
                });
        })
    }


    public deleteAddress(addressId: string) {
        // Update adress on currentAccount and call API.
        this._http.delete('Accounts/' + this.currentAccount.Id + '/Address/' + this.currentAddress.Id)
            .map(res => res.json())
            .subscribe(a => {
                this._notify.success(this.translate.instant("ADDRESSDELETED"));
                this.currentAccount.Address = a.Data;
                this.currentAccount$.next(this.currentAccount);
                this.updateAccount(this.currentAccount);
                console.log("[Account] Address Delete: Success!");
            }, err => {
                console.log("[Account] Address Delete: Fail! -> Unknown error :(");
            });
    }

    addTagsToMany(data: any) {

        this.orgService.tagsChange(data.Labels);
        var searchQuery = "";
        if (this.selectAll) {
            data.GetAccountsFromFilter = true;
            searchQuery = "/?" + this.getSearchString();
        }
        this._http.put('Accounts/AddTagsOnMany' + searchQuery, data)
            .map(res => res.json())
            .subscribe(a => {
                this.selectedAccounts = [];
                this.selectedOption = 'selected';
                if (a.IsSuccess === true) {
                    console.log("[Account] Add Tags to many: Success!");
                    this._notify.success(this.translate.instant("TAGSADDED"));
                    this.updateAccount();
                    this.orgService.tagsChange(data.Labels);
                } else {
                    this._notify.error(a.Message);
                    console.log("[Account] Add Tags to many: Fail! -> " + a.Message);
                }
            }, err => {
                console.log("[Account] Add Tags to many: Fail! ->  Unknown error :(");
            });
    }

    removeTagsFromMany(data: any) {

        var searchQuery = '';
        if (this.selectAll) {
            data.GetAccountsFromFilter = true;
            searchQuery = "/?" + this.getSearchString();
        }
        this._http.put('Accounts/RemoveTagsFromMany' + searchQuery, data)
            .map(res => res.json())
            .subscribe(a => {
                this.selectedOption = 'selected';
                this.selectedAccounts = [];
                if (a.IsSuccess === true) {
                    console.log("[Account] Remove Tags from many: Success!");
                    this._notify.success(this.translate.instant("TAGSREMOVED"));
                    this.updateAccount();
                } else {
                    this._notify.error(a.Message);
                    console.log("[Account] Remove Tags from many: Fail! -> " + a.Message);
                }
            }, err => {
                console.log("[Account] Remove Tags from many: Fail! -> Unknown error :(");
            });

    }

    updateAccountManagerOnMany(data: any) {
        var searchQuery = "";
        if (this.selectAll) {
            data.GetAccountsFromFilter = true;
            searchQuery = "/?" + this.getSearchString();
        }
        this._http.put('Accounts/UpdateManagerOnMany' + searchQuery, data)
            .map(res => res.json())
            .subscribe(a => {
                this.selectedAccounts = [];
                this.selectedOption = 'selected';
                if (a.IsSuccess === true) {
                    console.log("[Account] Change Manager on many: Success!");
                    this._notify.success(this.translate.instant("ACCMANAGERCHANGED"));
                    this.updateAccount();
                } else {
                    this._notify.error(a.Message);
                    console.log("[Account] Change Manager on many: Fail! -> " + a.Message);
                }
            }, err => {
                console.log("[Account] Change Manager on many: Fail! -> Unknown error :(");
            });

    }

    updateAccountTypeOnMany(data: any) {
        var searchQuery = "";
        if (this.selectAll) {
            data.GetAccountsFromFilter = true;
            searchQuery = "/?" + this.getSearchString();
        }
        this._http.put('Accounts/UpdateTypeOnMany' + searchQuery, data)
            .map(res => res.json())
            .subscribe(a => {
                this.selectedAccounts = [];
                this.selectedOption = 'selected';
                if (a.IsSuccess === true) {
                    console.log("[Account] Change Type on many: Success!");
                    this._notify.success(this.translate.instant("ACCTYPECHANGED"));
                    this.updateAccount();
                } else {
                    this._notify.error(a.Message);
                    console.log("[Account] Change Type on many: Fail! -> " + a.Message);
                }
            }, err => {
                console.log("[Account] Change Type on many: Fail! -> Unknown error :(");
            });

    }

    public uploadFile(accountId: string, formData: FormData) {
        this._http.upload('Accounts/' + accountId + '/UploadFile', formData)
            .then(res => {
                let result = JSON.parse(res);
                console.log(result);
                if (result.IsSuccess) {
                    console.log("[Account] Uploaded new File [" + + "] for Account [" + accountId + "]");
                    let acc = this.accounts.find(x => x.Id == accountId);
                    if (acc) {
                        acc.Files.push(result.Data);
                    }
                    this._notify.success(this.translate.instant("FILEUPLOADED"));
                    this.getAccount(accountId, null, true);
                }
            })
    }

    public deleteFile(accountId: string, id: string) {
        this._http.delete('Accounts/' + accountId + '/DeleteFile/' + id).map(res => res.json()).subscribe(res => {
            if (res.IsSuccess) {
                console.log("[Account] Deleting File [" + id + "] for Account [" + accountId + "]: Success!");
                this._notify.success(this.translate.instant("FILEDELETED"));
                this.getAccount(accountId, null, true);
                let acc = this.accounts.find(x => x.Id == accountId);
                if (acc) {
                    acc.Files = acc.Files.filter(x => x.Id != id);
                }
            }
            else {
                console.log("[Account] Deleting File [" + id + "] for Account [" + accountId + "] Failed -> " + res.Message);
                this._notify.success(this.translate.instant("ERROR"));
            }
        })
    }

    public deleteDocument(id: string) {
        this._http.delete('Documents/' + id).map(res => res.json()).subscribe(res => {
            if (res.IsSuccess) {
                console.log("[Account] Deleting Document [" + id + "]: Success!");
                this._notify.success(this.translate.instant("FILEDELETED"));
                this.getAccount(this.currentAccount.Id, null, true);
            }
            else {
                console.log("[Account] Deleting File [" + id + "]: Failed -> " + res.Message);
                this._notify.success(this.translate.instant("ERROR"));
            }
        })
    }

    public getContactsForAccount(accountId: string, person: boolean = false, callback: (data: any) => any = null) {
        this._http.get(`Accounts/${accountId}/Related` + (person ? "Companies" : "Persons")).map(res => res.json()).subscribe(res => {
            if (res.IsSuccess) {
                this.contactAccounts$.next(res.Data);
                if (callback) callback(res.Data);
            }
            else {
                this.contactAccounts$.next([]);
            }
        });
    }

    public getRelationFromAccountIds(companyId: string, personId: string) {
        return this._http.get(`Accounts/${companyId}/Relation/${personId}`).map(res => res.json());
    }

    public getRelation(relationId: string) {
        return this._http.get(`Accounts/Relation/${relationId}`).map(res => res.json());
    }

    public getRelationsForPerson(personId: string, callback: (success: boolean, relations: AccountRelation[]) => any) {
        this._http.get(`Accounts/${personId}/RelatedCompanies`).map(res => res.json()).subscribe(data => {
            callback(data.IsSuccess, data.Data);
        });
    }

    public getAccountName(accountId: string) {
        return this._http.get("Accounts/" + accountId + "/Accountname").map(res => res.json());
    }

    public getAccountNames(accountIds: Array<string>) {
        return this._http.post("Accounts/List", accountIds).map(res => res.json());
    }

    public getReferredAccounts(accountId: string, callback: (referredAccounts: any) => any) {
        this._http.get("Accounts/" + accountId + "/ReferredAccounts").map(res => res.json()).subscribe(data => {
            if (data.IsSuccess) {
                callback(data.Data);
            }
        })
    }

    public getCount() {
        let query = this.getSearchString();
        return this._http.get('Accounts/Count?' + query)
            .map(res => res.json());
    }
}
