import { Injectable } from '@angular/core';
import { Api } from './api';
import { TranslateService } from '@ngx-translate/core';
import { NotificationService } from './notification-service';
import { OrgService } from './organisation.service';
import * as moment from 'moment';

@Injectable()
export class HighchartService {

    constructor(private orgService: OrgService, private api: Api, private translate: TranslateService, private notify: NotificationService) {
        this.options = undefined;
    }
    // Filter data

    public filter: InsightFilter = new InsightFilter({});

    public selectedOption: string = "";
    public documents: Array<any> = [];

    public insightType: string;
    public insisghtId: string;
    public insightName: string;
    public goalInsight: any

    public showModal: boolean;
    public modalOption: any;

    public latestRequest: { type: string, id: string }; // To get the latest chart even if a slower one comes after

    public isDocumentListShortened: boolean = false;

    // Chart data
    // public options: any
    private _options: any;
    get options() {
        return this._options;
    }
    set options(value: any) {
        if (value) {
            this.translateOptions(value);
        }
        this._options = value;
    }

    // public multiOptions: Array<any> = [];
    private _multiOptions: Array<any> = [];
    get multiOptions() {
        return this._multiOptions;
    }
    set multiOptions(value: Array<any>) {
        if (value && value.length) {
            value.map(x => { this.translateOptions(x.Charts); });
        }
        this._multiOptions = value;
    }

    public getSingleChart(docType: string, insightId: string, filter: InsightFilter): any {
        return this.api.post(`Reports/${docType}/${insightId}`, filter).map(res => {
            let response = res.json();
            this.translateOptions(response.Data.Charts);
            return response;
        });
    }

    public update(type: string, id: string, succFunc?: Function) {
        let query = this.getQueryObject(this.filter);
        this.options = [];
        this.latestRequest = { type: type, id: id };

        this.api.post(`Reports/${type}/${id}`, query).map(res => res.json())
            .subscribe(body => {
                if (this.latestRequest.type === type && this.latestRequest.id === id) {
                    this.insightName = body.Data.Name;
                    this.insightType = type;
                    this.insisghtId = id;
                    this.isDocumentListShortened = body.Data.IsDocumentListShortened;
                    this.documents = body.Data.Documents;
                    if (body.Data.GoalInsight) {
                        this.goalInsight = body.Data.GoalInsight
                    }
                    if (body.Data.Charts.find((x: any) => x.chart.type === 'solidgauge')) {
                        this.positionGaugeLabel(body.Data.Charts);
                    }
                    this.options = body.Data.Charts;
                    if (succFunc) succFunc();
                }
            })
    }

    public updateMultiOption(url: string, index: number) {
        const query = this.getQueryObject(this.filter);

        this.api.post(url, query).map(res => res.json())
            .subscribe(body => {
                this.multiOptions.splice(index, 1, body.Data);
                this.multiOptions = this.multiOptions.slice();
            })
    }

    public updatePersonalGoals(url: string, succFunc?: Function) {
        const query = this.getQueryObject(this.filter);

        this.api.post(url, query)
            .map(res => res.json())
            .subscribe(res => {
                if (res.IsSuccess) {
                    this.options = this.options.map((x: any) => {
                        if (x.chart.type === 'solidgauge') {
                            x = res.Data.Charts.find((x: any) => x.chart.type === 'solidgauge');
                            this.positionGaugeLabel(res.Data.Charts);
                        } else if (x.chart.type === 'progress') {
                            x = res.Data.Charts.find((x: any) => x.chart.type === 'progress');
                        }
                        return x;
                    });
                    if (succFunc) succFunc();
                }
            })
    }

    // To bes used if having diagrams from multiple data sources. Was used by Goals but no longer.
    // public updateMany(type: string, insightId: string, succFunc?: Function) {
    //     const query = this.getQueryObject(this.filter);
    //     let orgId = localStorage.getItem("orgId");
    //     this.multiOptions = [];

    //     this.api.post(`Organisations/${orgId}/Reports/${type}/${insightId}`, query)
    //         .map(res => res.json())
    //         .subscribe(res => {
    //             this.insightName = res.Data[0].Name;
    //             this.insightType = type;
    //             this.insisghtId = insightId;
    //             this.documents = res.Data.Documents;
    //             this.multiOptions = res.Data;
    //             if (succFunc) succFunc();
    //         });
    // }

    public getQueryObject(filter: InsightFilter) {
        console.log("[Insight] Getting Query from ", filter);
        let queryObject = {
            AccountTimeSpanName: filter.accountCreateDateTimespan,
            AccountManagerId: filter.accountManagerId.length > 0 ? filter.accountManagerId : [],
            AccountId: filter.accountId.length > 0 ? filter.accountId : [],
            AccountTypeId: filter.accountTypeId.length > 0 ? filter.accountTypeId : [],
            AccountTag: filter.accountTag.length > 0 ? filter.accountTag : [],
            AccountCreateStart: filter.accountCreateStart || 0,
            AccountCreateEnd: filter.accountCreateEnd || 0,
            AccountPersons: filter.accountPersons,
            AccountCompanies: filter.accountCompanies,

            ActionTimeSpanName: filter.actionDateTimespan,
            ActionAssignedUserId: filter.actionAssignedUserId,
            ActionType: filter.actionType.length > 0 ? filter.actionType : [],
            ActionStatus: filter.actionStatus.length > 0 ? filter.actionStatus : [],
            ActionDateStart: filter.actionDateStart || 0,
            ActionDateEnd: filter.actionDateEnd || 0,

            DealTimeSpanName: filter.dealDateTimespan,
            DealOwnerId: filter.dealOwnerId.length > 0 ? filter.dealOwnerId : [],
            DealStatusId: filter.dealStatusId.length > 0 ? filter.dealStatusId : [],
            DealSaleprocesId: filter.dealSaleprocesId.length > 0 ? filter.dealSaleprocesId : [],
            DateFieldName: filter.dateFieldName,
            DealStart: filter.dealStart || 0,
            DealEnd: filter.dealEnd || 0,
            GroupingType: filter.groupingType,
            OnlyOneChartId: filter.onlyOneChartId,
            
            GoalsUserIds: filter.goalsUserIds.length > 0 ? filter.goalsUserIds : [],
            GoalsId: filter.goalsId || '',
            GoalsIntervals: filter.goalsIntervals > 0 ? filter.goalsIntervals : 6,
            GoalsStartDate: filter.goalsStartDate || 0,
            GoalsEndDate: filter.goalsEndDate || 0,

            SalesCommissionDateTimeSpanName: filter.salesCommissionTimespan,
            SalesCommissionStartDate: filter.salesCommissionStartDate ? filter.salesCommissionStartDate : 0,
            SalesCommissionEndDate: filter.salesCommissionEndDate ? filter.salesCommissionEndDate : 0,
            SalesCommissionUserIds: filter.salesCommissionUsers.length > 0 ? filter.salesCommissionUsers : [],
            SalesCommissionIds: filter.salesCommissionIds.length ? filter.salesCommissionIds : [],
        };
        return queryObject;
    }

    public pin(id: string) {
        let pinFilter = Object.assign(this.filter);
        pinFilter.onlyOneChartId = id;
        let query = this.getQueryObject(pinFilter);
        this.api.post(`Reports/${this.insightType}/${this.insisghtId}/Pin`, query).map(res => res.json()).subscribe(res => {
            if (res.IsSuccess) {
                this.notify.success(this.translate.instant("PINSUCCESS"));
                let insights = this.orgService.currentOrganisation().FavoriteInsights;
                insights.push(res.Data);
                this.orgService.insightsChange(insights);
                this.filter.onlyOneChartId = "";
            }
        });
    }

    public unPin(id: string): any {
        return this.api.delete(`Reports/Unpin/${id}`).map(res => {
            this.notify.success(this.translate.instant("UNPINSUCCESS"));
            let insights = this.orgService.currentOrganisation().FavoriteInsights;
            insights.splice(insights.findIndex(x => x.Id == id), 1);
            this.orgService.insightsChange(insights);
            return res.json()
        }
        );
    }

    public clear() {
        this.insightName = "";
        this.options = [];
        this.multiOptions = [];
    }

    public getQueryString(filter: InsightFilter) {

        let queryObject: any = this.getQueryObject(filter);

        var str: Array<string> = [];
        for (var p in queryObject)
            if (queryObject.hasOwnProperty(p)) {
                if (queryObject[p] === '' || typeof queryObject[p] === 'undefined' || queryObject[p] === null)
                    continue;
                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(queryObject[p]));
            }
        return str.join("&");
    }

    public positionGaugeLabel(charts: any) {
        let index = charts.findIndex((x: any) => x.chart.type === 'solidgauge');
        charts[index].tooltip.positioner = (labelWidth: any, labelHeight: any, point: any) => {
            return {
                x: point.plotX - labelWidth / 3,
                y: point.plotY + labelHeight / 3
            };
        }
    }

    private translateOptions(options: any) {
        for (let i = 0; i < options.length; i++) {
            let currOption = options[i];
            if (currOption.title && currOption.title.text && currOption.title.translate) {
                currOption.title.text = this.translate.instant(currOption.title.text)
            }
            if (currOption.subtitle && currOption.subtitle.text && currOption.subtitle.translate) {
                if (currOption.subtitle.translateObj) {
                    currOption.subtitle.text = this.translate.instant(currOption.subtitle.text, currOption.subtitle.translateObj)
                } else {
                    currOption.subtitle.text = this.translate.instant(currOption.subtitle.text)
                }
            }
            if (currOption.xAxis && currOption.xAxis.title && currOption.xAxis.title.text && currOption.xAxis.title.translate) {
                currOption.xAxis.title.text = this.translate.instant(currOption.xAxis.title.text);
            }
            if (currOption.yAxis) {
                if (currOption.yAxis.title && currOption.yAxis.title.text && currOption.yAxis.title.translate) {
                    currOption.yAxis.title.text = this.translate.instant(currOption.yAxis.title.text);
                }
                if (currOption.yAxis.plotLines && currOption.yAxis.plotLines.length) {
                    for (let i = 0; i < currOption.yAxis.plotLines.length; i++) {
                        let currPlot = currOption.yAxis.plotLines[i];
                        if (currPlot.label && currPlot.label.text && currPlot.label.translate) {
                            currPlot.label.text = this.translate.instant(currPlot.label.text);
                        }
                    }
                }
            }
            if (currOption.series) {
                for (let i = 0; i < currOption.series.length; i++) {
                    let currSerie = currOption.series[i];
                    if (currSerie.name && currSerie.translate) {
                        currSerie.name = this.translate.instant(currSerie.name);
                    }
                    if (currSerie.data && currSerie.data.length && currSerie.translateData) {
                        for (let j = 0; j < currSerie.data.length; j++) {
                            let currData = currSerie.data[j];
                            if (currData.name && currData.translate) {
                                currData.name = this.translate.instant(currData.name);
                            }
                        }
                    }
                }
            }
        }
    }
}

export class InsightFilter {

    accountCreateDateTimespan: string;
    accountManagerId: Array<string>;
    accountId: Array<string>;
    accountTypeId: Array<string>;
    accountTag: Array<string>;
    accountCreateStart: number;
    accountCreateEnd: number;
    accountCompanies: boolean = true;
    accountPersons: boolean = true;

    actionDateTimespan: string;
    actionAssignedUserId: Array<string>;
    actionType: Array<string>;
    actionStatus: Array<string>;
    actionDateStart: number;
    actionDateEnd: number;

    dealDateTimespan: string;
    dealOwnerId: Array<string>;
    dealStatusId: Array<string>;
    dealSaleprocesId: Array<string>;
    dealStart: number;
    dealEnd: number;
    dateFieldName: string;
    groupingType: string;
    pinFilter: string;

    goalsUserIds: Array<string>;
    goalsId: string;
    goalsIntervals: number;
    goalsStartDate: number;
    goalsEndDate: number;

    salesCommissionTimespan: string;
    salesCommissionDateName: string;
    salesCommissionStartDate: number;
    salesCommissionEndDate: number;
    salesCommissionUsers: Array<string>;
    salesCommissionIds: Array<string>;
    onlyOneChartId: string;


    [key: string]: any;

    constructor(serverFilter: any) {

        this.accountCreateDateTimespan = serverFilter.AccountTimeSpanName ? serverFilter.AccountTimeSpanName : "";
        this.accountManagerId = serverFilter.AccountManagerId ? serverFilter.AccountManagerId : [];
        this.accountId = serverFilter.AccountId ? serverFilter.AccountId : [];
        this.accountTypeId = serverFilter.AccountTypeId ? serverFilter.AccountTypeId : [];
        this.accountTag = serverFilter.AccountTag ? serverFilter.AccountTag : [];
        this.accountCreateStart = serverFilter.AccountCreateStart || moment().subtract(30, 'days').valueOf();
        this.accountCreateEnd = serverFilter.AccountCreateEnd || moment().subtract(1, 'days').valueOf();
        this.accountPersons = serverFilter.AccountPersons ? serverFilter.AccountPersons : true;
        this.accountCompanies = serverFilter.AccountCompanies ? serverFilter.AccountCompanies : true;

        this.actionDateTimespan = serverFilter.ActionTimeSpanName ? serverFilter.ActionTimeSpanName : "Last30Days";
        this.actionAssignedUserId = serverFilter.ActionAssignedUserId ? serverFilter.ActionAssignedUserId : [];
        this.actionType = serverFilter.ActionType ? serverFilter.ActionType : [];
        this.actionStatus = serverFilter.ActionStatus ? serverFilter.ActionStatus : [];
        this.actionDateStart = serverFilter.ActionDateStart || moment().subtract(30, 'days').valueOf();
        this.actionDateEnd = serverFilter.ActionDateEnd || moment().subtract(1, 'days').valueOf();

        this.dealDateTimespan = serverFilter.DealTimeSpanName ? serverFilter.DealTimeSpanName : "Last30Days";
        this.dealOwnerId = serverFilter.DealOwnerId ? serverFilter.DealOwnerId : [];
        this.dealStatusId = serverFilter.DealStatusId ? serverFilter.DealStatusId : [];
        this.dealSaleprocesId = serverFilter.DealSaleprocesId ? serverFilter.DealSaleprocesId : [];
        this.dealStart = serverFilter.DealStart || moment().subtract(30, 'days').valueOf();
        this.dealEnd = serverFilter.DealEnd || moment().subtract(1, 'days').valueOf();

        this.dateFieldName = serverFilter.DateFieldName ? serverFilter.DateFieldName : "DateCreated";
        this.groupingType = serverFilter.GroupingType ? serverFilter.GroupingType : 'Auto';
        this.onlyOneChartId = serverFilter.OnlyOneChartId ? serverFilter.OnlyOneChartId : "";

        this.goalsId = serverFilter.GoalsId || '';
        this.goalsUserIds = serverFilter.GoalsUserIds ? serverFilter.GoalsUserIds : [];
        this.goalsIntervals = serverFilter.GoalsIntervals || 6;
        this.goalsStartDate = serverFilter.GoalsStartDate || new Date().getTime();
        this.goalsEndDate = serverFilter.GoalsEndDate || new Date().getTime();

        this.salesCommissionTimespan = serverFilter.salesCommissionDateTimeSpanName || 'Last30Days';
        this.salesCommissionStartDate = serverFilter.SalesCommissionStartDate || moment().subtract(30, 'days').valueOf();
        this.salesCommissionEndDate = serverFilter.SalesCommissionEndDate || moment().valueOf();
        this.salesCommissionUsers = serverFilter.SalesCommissionUsers || [];
        this.salesCommissionIds = serverFilter.salesCommissionIds || [];
    }
}

