import { Injectable, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from "rxjs/Observable";
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { Api } from './api';
import { BaseService } from './baseService';
import { LocalStorage } from './localstorage';
import { NotificationService } from './notification-service';
import { ConversationModel } from '../models/conversation.models';
import { MessageModel } from '../models/message.models';
import { TranslateService } from '@ngx-translate/core';
import { IntercomService } from '../services/interecomService';
import { ChannelTypes } from '../models/channel.models';
import { OrgService } from '../services/organisation.service';
import { SignalRService } from '../services/signalRService';
import { ButtonPanelService } from '../services/button-panel.service';

declare var $: JQueryStatic;

@Injectable()
export class ChatService extends BaseService {

    private orgSub: any;
    private orgId: any;

    private _conversationObserver: any;
    private _filesObserver: any;
    private _messagesObserver: any;
    private _messageSendrObserver: any;

    private newMessages: number = 0;

    private channels$: BehaviorSubject<Array<ConversationModel>> = new BehaviorSubject(null);
    public channelsObservable(): Observable<Array<ConversationModel>> {
        return this.channels$.asObservable();
    }

    private accountChannels$: BehaviorSubject<Array<ConversationModel>> = new BehaviorSubject([]);
    public accountChannelsObservable(): Observable<Array<ConversationModel>> {
        return this.accountChannels$.asObservable();
    }

    public conversation$: Observable<ConversationModel>;
    public conversationId: string;

    public files$: Observable<Array<MessageModel>>;
    public messages$: Observable<Array<MessageModel>>;
    public messageSend$: Observable<any>;

    public conversationDeleted: EventEmitter<any>;
    public conversationCreated: EventEmitter<any>;

    constructor(
        private _http: Api,
        _localStore: LocalStorage,
        private _notify: NotificationService,
        private _translate: TranslateService,
        private _intercomService: IntercomService,
        private orgService: OrgService,
        private router: Router,
        private signalRService: SignalRService,
        private buttons: ButtonPanelService
    ) {
        super(_localStore);

        this.conversation$ = new Observable<ConversationModel>((observer: any) => {
            this._conversationObserver = observer;
        }).share();

        this.files$ = new Observable<Array<MessageModel>>((observer: any) => {
            this._filesObserver = observer;
        }).share();

        this.messages$ = new Observable<Array<MessageModel>>((observer: any) => {
            this._messagesObserver = observer;
        }).share();

        this.messageSend$ = new Observable((observer: any) => {
            this._messageSendrObserver = observer;
        }).share();

        this.conversationDeleted = new EventEmitter<any>();
        this.conversationCreated = new EventEmitter<any>();

        this.orgSub = this.orgService.currentOrganisationObservable().subscribe(org => {
            if (org) {
                this.orgId = org.Id;
                console.log("[Chat] Loading Org Chats");
                this.getConversations();
            }
        });

        this.signalRService.messageReceived.subscribe((messagecontainer: any) => {

            let message = messagecontainer.message; 
            if (message.userCreatedId != localStorage.getItem("userId")) {
                let channels = this.channels$.value;

                for (let channel of channels) {
                    if (channel.id == messagecontainer.conversationId && messagecontainer.conversationId != this.conversationId) {
                        if (!channel.newMessages) channel.newMessages = 0;
                        channel.newMessages++;
                        console.log("[Chat] Added Message to Channel [" + channel.name + "]");
                    }

                }
                this.addNewMessage(1);
                this.channels$.next(channels);
            }

        });

    }

    public addConversation(model: any, redirect: boolean = true) {
        this._http.requestPost(
            `Organisations/${this.orgId}/Conversations`,
            model,
            (data) => {
                if (model.type == ChannelTypes.channel) {
                    this._intercomService.trackEvent('chanel-created');
                } else {
                    this._intercomService.trackEvent('workgroup-created');
                }
                console.log("[Chat] Add Conversation: Success!");
                this._notify.success(this._translate.instant('CONVERSATIONCREATED'));
                this.conversationCreated.emit(data);
                this.getConversations();
                console.log("[Chat] Redirecting to Workgroups/" + data);
                if (redirect) {
                    this.router.navigate(["/CRM/Chat/WorkGroups/", data]);
                }
            },
            (error) => {
                console.log("[Chat] Add Conversation: Failed! -> " + error);
                this._notify.error(error);
                this.conversationCreated.emit(null);
            }
        );
    }

    public getConversations() {
        this._http.requestGet('Organisations/' + this.orgId + '/Channels', (data) => {
            console.log("[Chat] Load Channels for Organisation [" + this.orgId + "]: Success! -> Found [" + data.length + "] of them");
            for (let chan of data) {
                if (chan.newMessages) {
                    console.log("[Chat] Found new Messages: " + chan.newMessages);
                    this.addNewMessage(chan.newMessages);
                }
            }
            this.channels$.next(data);
        },
            (error) => {
                console.log("[Chat] Load Channels for Organisation [" + this.orgId + "]: Failed ->" + error);
                this._notify.error(error);
            }
        );
    }

    private addNewMessage(amount: number) {
        console.log(this.newMessages, amount);
        this.newMessages += amount;
        console.log(this.newMessages.toString(), amount);
        this.buttons.toggleClass("chat", "", this.newMessages.toString())
    }

    private removeMessage(amount: number) {
        this.newMessages -= amount;
        this.buttons.toggleClass("chat", "", this.newMessages ? this.newMessages.toString() : "");
    }

    public getWorkGroupForAccount(accountId: string) {
        this._http.requestGet(
            `Accounts/${accountId}/PublicGroups`,
            (data) => {
                console.log("[Chat] Load Workgroups for Account [" + accountId + "] Success!");
                this.accountChannels$.next(data);
            },
            (error) => {
                console.log("[Chat] Load Workgroups for Account [" + accountId + "] Failed! -> " + error);
                this._notify.error(error);
            }
        );
    }

    public getConversation(conversationId: string) {
        this._http.requestGet(
            `Conversations/${conversationId}`,
            (data) => {
                console.log("[Chat] Load Conversation [" + conversationId + "] Success!");
                this._conversationObserver.next(data);
                this.conversationId = conversationId;
                let channels = this.channels$.value
                for (let channel of channels) {
                    if (channel.id == conversationId) {
                        if (channel.newMessages) this.removeMessage(channel.newMessages);
                        channel.newMessages = 0;
                        this.channels$.next(channels);
                    }
                }
            },
            (error) => {
                console.log("[Chat] Load Conversation [" + conversationId + "] Failed! -> " + error);
                this._notify.error(error);
            }
        );
    }

    public getFiles(conversationId: string) {
        this._http.requestGet(
            `Conversations/${conversationId}/Files`,
            (data) => {
                console.log("[Chat] Get Files: Success!");
                this._filesObserver.next(data);
            },
            (error) => {
                console.log("[Chat] Get Files: Failed! -> " + error);
                this._notify.error(error);
            }
        );
    }

    public getMessages(conversationId: string, currentPage: number, pageSize: number) {
        this._http.requestGet(
            `Conversations/${conversationId}/Messages?Page=${currentPage}&PageSize=${pageSize}`,
            (data) => {
                console.log("[Chat] Load messages: Success!");
                this._messagesObserver.next(data);
            },
            (error) => {
                console.log("[Chat] Load messages: Failed! -> " + error);
                this._notify.error(error);
            }
        );
    }

    public addUsersToConversation(conversationId: string, userIds: Array<string>, guestUserIds: Array<string>) {

        this._http.requestPut(
            `Conversations/${conversationId}/Subscribe/Users`,
            {
                userIds: userIds,
                guestUserIds: guestUserIds
            },
            (data) => {
                console.log("[Chat] Add users to channel: Success!");
                this._notify.success(this._translate.instant('USERADDEDTOCONVERSATION'));
            },
            (error) => {
                console.log("[Chat] Add users to channel: Failed! -> " + error);
                this._notify.error(error);
            }
        );
    }

    public leaveChannel(conversationId: string, userId: string, callback: (data: any) => void) {
        this._http.requestDelete(
            `Conversations/${conversationId}/Unsubscribe/${userId}`,
            (data) => {
                console.log("[Chat] Leave channel: Success!");
                callback(true);
                this.getConversations();
            },
            (error) => {
                console.log("[Chat] Leave channel: Failed! -> " + error);
                this._notify.error(error);
                callback(false);
            }
        );
    }

    public deleteConvrsation(id: string, callback: () => void = null) {
        this._http.requestDelete(
            `Conversations/${id}`,
            (data) => {
                console.log("[Chat] Conversation Delete: Success!");
                this._notify.success(this._translate.instant('CONVERSATIONDELETED'));
                this.channels$.next(this.channels$.value.filter(x => { return x.id != id }));
                this.conversationDeleted.emit(true);
                if (callback) {
                    callback();
                }
            },
            (error) => {
                console.log("[Chat] Conversation Delete: Failed! -> " + error);
                this._notify.error(error);
                this.conversationDeleted.emit(false);
            }
        );
    }

    public sendMessage(conversationId: string, data: FormData) {
        this._http.upload(`Conversations/${conversationId}/Messages`, data)
            .then(response => {
                console.log("[Chat] Send Message: Success!");
                this._intercomService.trackEvent('conversation-messages-send');
                this._messageSendrObserver.next(true);
                this.clearInputFile($('#file-input-chat')[0]);
            },
            reject => {
                console.log("[Chat] Send Message: Rejected!");
                this._messageSendrObserver.next(true);
                this.clearInputFile($('#file-input-chat')[0]);
            });
    }

    public toggleHidden(conversationId: string) {
        this._http.put('Conversations/' + conversationId + '/ToggleHidden', null).subscribe(data => {
            console.log("[Chat] Toggle Visibilityp on [" + conversationId + "]: Success!");
            this.getConversation(conversationId);
        },
            (error) => {
                console.log("[Chat] Toggle Visibilityp on [" + conversationId + "]: Failed! -> " + error);
            });
    }
}
