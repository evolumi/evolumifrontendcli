import { Injectable } from '@angular/core';
import { Api } from './api';
import { LocalStorage } from './localstorage';
import { LoaderService } from './loader.service';
import { NotificationService } from './notification-service';
import { OrgService } from './organisation.service';
import { TranslateService } from '@ngx-translate/core';

export interface DealStatus {
    SalesPipe: Array<string>[],
    PostSalesPipe: Array<string>[],
    LostSalesPipe: Array<string>[],
    ClosedSalesPipe: Array<string>[]
}

export interface SalesProcessList {
    Id: string,
    Name: string,
    SalesStatus: DealStatus
}

@Injectable()

export class SettingsDealsOperations {
    public salesProcessList: Array<SalesProcessList>;
    public tmpDealStatus: DealStatus;
    private orgSub: any;
    constructor(
        public _localstore: LocalStorage,
        public _notify: NotificationService,
        public _load: LoaderService,
        public _http: Api,
        public _orgService: OrgService,
        private translate: TranslateService
    ) {
        this.tmpDealStatus = {
            SalesPipe: [],
            PostSalesPipe: [],
            LostSalesPipe: [],
            ClosedSalesPipe: []
        };
        this.salesProcessList = [{
            Id: '',
            Name: '',
            SalesStatus: this.tmpDealStatus
        }];

        this.orgSub = this._orgService.currentOrganisationObservable().subscribe(org => {

            if (org) {
                this.salesProcessList = [];
                for (let salePipe of org.Settings.SaleProcesses) {
                    this.tmpDealStatus = {
                        SalesPipe: [],
                        PostSalesPipe: [],
                        LostSalesPipe: [],
                        ClosedSalesPipe: []
                    };

                    for (let DealStatuses of salePipe.DealStatuses) {
                        if (DealStatuses.Type === 1) {
                            this.tmpDealStatus.SalesPipe.push(DealStatuses);
                        } else if (DealStatuses.Type === 2) {
                            this.tmpDealStatus.PostSalesPipe.push(DealStatuses);
                        } else if (DealStatuses.Type === 3) {
                            this.tmpDealStatus.LostSalesPipe.push(DealStatuses);
                        } else {
                            this.tmpDealStatus.ClosedSalesPipe.push(DealStatuses);
                        }
                    };
                    this.tmpDealStatus.SalesPipe = this.tmpDealStatus.SalesPipe.sort(this.propComparator('Percentage'));
                    this.tmpDealStatus.PostSalesPipe = this.tmpDealStatus.PostSalesPipe.sort(this.propComparator('SortOrder'));
                    this.tmpDealStatus.LostSalesPipe = this.tmpDealStatus.LostSalesPipe.sort(this.propComparator('SortOrder'));
                    this.tmpDealStatus.ClosedSalesPipe = this.tmpDealStatus.ClosedSalesPipe.sort(this.propComparator('SortOrder'));
                    this.salesProcessList.push({
                        Id: salePipe.Id,
                        Name: salePipe.Name,
                        SalesStatus: this.tmpDealStatus
                    });
                };
            }
        });
    }

    propComparator(key: string) {
        return function (a: any, b: any) {
            return (a[key] > b[key]) ? 1 : ((b[key] > a[key]) ? -1 : 0);
        }
    }
    addSalesProcess(name: string) {
        var orgId = this._localstore.get('orgId');
        this._http.post('Organisations/' + orgId + '/SalesProcess', name)
            .map(res => res.json())
            .subscribe(res => {
                if (res.IsSuccess) {
                    console.log("[Deal-Settings] Add saleprocess: Success!");
                    this._notify.success(this.translate.instant("SALEPROCESSADDED"));
                    this._orgService.settingsChange();
                }
                else {
                    console.log("[Deal-Settings] Add saleprocess: Fail!");
                }
            });

    }

    updateSalesProcess(saleProcessId: string, name: string) {
        var orgId = this._localstore.get('orgId');
        this._http.requestPut(
            `Organisations/${orgId}/SalesProcess/${saleProcessId}`,
            name,
            (res) => {
                if (res.IsSuccess) {
                    console.log("[Deal-Settings] Update saleprocess: Success!");
                    this._notify.success(this.translate.instant("SALEPROCESSUPDATED"));
                    this._orgService.settingsChange();
                }
                else {
                    console.log("[Deal-Settings] Update saleprocess: Fail!");
                }
            });
    }

    deleteSalesProcess(id: string) {
        var orgId = this._localstore.get('orgId');
        this._http.delete('Organisations/' + orgId + '/SalesProcess/' + id, '')
            .map(res => res.json())
            .subscribe(res => {
                if (res.IsSuccess === true) {
                    this._notify.success(this.translate.instant("DELETESUCCESSFUL"));
                    console.log("[Deal-Settings] Delete saleprocess: Success!");
                    this._orgService.settingsChange();
                } else {
                    this._notify.error(this.translate.instant("ERROR"));
                    console.log("[Deal-Settings] Delete saleprocess: Fail -> " + res.Message);
                }
            }, com => {
                //
            });
    }

    addDealStatus(saleProcessId: string, data: any) {
        var orgId = this._localstore.get('orgId');
        this._http.post('Organisations/' + orgId + '/SalesProcess/' + saleProcessId + '/DealStatus', data)
            .map(res => res.json())
            .subscribe(res => {
                if (res.IsSuccess === true) {
                    console.log("[Deal-Settings] Add new Status: Success!");
                    this._notify.success(this.translate.instant("DEALSTATUSADDED"));
                    this._orgService.settingsChange();
                }
                else {
                    console.log("[Deal-Settings] Add new Status: Fail! -> " + res.Message);
                }
            });
    }

    updateSortOrder(processId: string, item: any) {
        var orgId = this._localstore.get('orgId');
        let salesOrder = item;
        let index = 0;
        for (let i of salesOrder) {
            i.SortOrder = index;
            index++;
        }
        this._http.put('Organisations/' + orgId + '/SalesProcess/' + processId + '/UpdateSortOrder', salesOrder)
            .map(res => res.json())
            .subscribe(res => {
                if (res.IsSuccess === true) {
                    console.log("[Deal-Settings] Order update: Success!");
                    this._orgService.settingsChange();
                    this._notify.success(this.translate.instant("DEALSTATUSORDERUPDATED"));
                } else {
                    this._notify.error(res.Message);
                    console.log("[Deal-Settings] Order update: Fail! -> " + res.Message);
                }
            });

    }

    deleteDealStatus(dealType: number, salesId: string, fieldid: string) {
        var orgId = this._localstore.get('orgId');
        this._http.delete('Organisations/' + orgId + '/SalesProcess/' + salesId + '/DealStatus/' + fieldid, '')
            .map(res => res.json())
            .subscribe(res => {
                for (let salePipe in this.salesProcessList) {
                    let pipeName: string;
                    if (salesId === this.salesProcessList[salePipe].Id) {
                        if (dealType === 1) {
                            pipeName = 'SalesPipe';
                        } else if (dealType === 2) {
                            pipeName = 'PostSalesPipe';
                        } else if (dealType === 3) {
                            pipeName = 'LostSalesPipe';
                        } else {
                            pipeName = 'ClosedSalesPipe';
                        }
                        for (let salePipeDetails in (<any>this.salesProcessList)[salePipe]['SalesStatus'][pipeName]) {
                            if ((<any>this.salesProcessList)[salePipe]['SalesStatus'][pipeName][salePipeDetails].Id === fieldid) {
                                (<any>this.salesProcessList)[salePipe]['SalesStatus'][pipeName].splice(salePipeDetails, 1);
                                this._notify.success(this.translate.instant("DELETESUCCESSFUL"));
                            }
                        };
                    };
                };
            });
    }

    updateDealStatus(salesId: string, fieldid: string, data: any) {
        var orgId = this._localstore.get('orgId');
        this._http.put('Organisations/' + orgId + '/SalesProcess/' + salesId + '/DealStatus/' + fieldid, data)
            .map(res => res.json())
            .subscribe(res => {
                this._orgService.settingsChange();
                this._notify.success(this.translate.instant("DEALSTATUSUPDATED"));
            });
    }

    ngOnDestroy() {
        if (this.orgSub) this.orgSub.unsubscribe();
    }
}