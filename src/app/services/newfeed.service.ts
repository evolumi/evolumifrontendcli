import { Injectable } from '@angular/core';
import { Api } from './api';
import { NewsfeedModel, CommentModel } from '../models/newsfeed.models';
import { LocalStorage } from './localstorage';
import { AuthService } from './auth-service';
import { LogedInUserModel } from '../models/user.models';
import { NotificationService } from './notification-service';

import { TranslateService } from '@ngx-translate/core';

export interface searchParams {
	type: string,
	tag: string,
	Owners: string,
	Search: string,
	AccountId: string,
	Timestamp: number,
	PageSize: number,
	UserId: string
}

@Injectable()

export class NewsfeedService {

	private currentUser: LogedInUserModel = new LogedInUserModel({});
	private userSub: any;

	public currentTimestamp: number;

	public searchParams: searchParams;
	public queryString: string;
	public feeds: Array<NewsfeedModel> = new Array<NewsfeedModel>();
	public apiProgress: boolean = false;
	public continueLoading: boolean;

	constructor(public _http: Api,
		public _store: LocalStorage,
		private translate: TranslateService,
		public authService: AuthService,
		public _notify: NotificationService) {
		this.searchParams = { type: '', tag: '', Owners: '', Search: '', AccountId: '', Timestamp: 0, PageSize: 20, UserId: '' };
		this.userSub = this.authService.currentUserObservable().subscribe(user => {
			if (user)
				this.currentUser = user;
		});
	}


	//reset all old search queries
	reset() {
		this.searchParams = { type: '', tag: '', Owners: '', Search: '', AccountId: '', Timestamp: 0, PageSize: 20, UserId: '' };
	}

	clear() {
		this.feeds = [];
	}


	//generate search query
	getSearchString() {
		var str: any = [];
		for (var p in this.searchParams) {
			if (this.searchParams.hasOwnProperty(p)) {
				if ((<any>this.searchParams)[p] === '' || typeof (<any>this.searchParams)[p] === 'undefined' || typeof (<any>this.searchParams)[p] === 'function')
					continue;
				str.push(encodeURIComponent(p) + "=" + encodeURIComponent((<any>this.searchParams)[p]));
			}
		}
		return str.join("&");
	}

	getNewsFeeds(scroll = false) {
		var query = this.getSearchString();
		this.apiProgress = true;
		this._http.get(`News?${query}`)
			.map(res => {
				return res.json()
			})
			.subscribe(res => {
				if (scroll == false) this.feeds = [];

				let resCount = res.Data.length;

				for (var feed of res.Data) {
					this.feeds = [...this.feeds, new NewsfeedModel(feed)]
				}
				if (this.feeds.length > 0) {
					this.currentTimestamp = this.feeds[this.feeds.length - 1].DateCreated;
					this.apiProgress = false;
					if (this.feeds.length < this.searchParams.PageSize && resCount > 0 && this.continueLoading) {
						this.makeSroll();
					}
				} else {
					this.apiProgress = false;
				}
			});
	}

	public UpdateSearchFields(updateArray: any) {
		//this.searchParams = updateArray;
		for (var arr in updateArray) {
			(updateArray[arr] === 'All Activity' || updateArray[arr] === 'All') ? (<any>this.searchParams)[arr] = '' : (<any>this.searchParams)[arr] = updateArray[arr];
		}
		this.getNewsFeeds();
	}

	deleteNews(id: string) {
		this._http.delete('News/' + id)
			.map(res => res.json())
			.subscribe(re => {
				if (re.IsSuccess === true) {
					this._notify.success('News deleted');
					this.getNewsFeeds();
				} else {
					this._notify.error(re.Message);
				}
			});
	}
	editNews(id: string, data: any, accid = ''): any {
		return this._http.put('News/' + id, data)
			.map(res => res.json())
	}
	addcomment(news: NewsfeedModel, comment: string) {
		news.commentloader = true;

		this._http.post(`News/${news.Id}/Comment`, { 'Text': comment })
			.map(res => res.json())
			.subscribe(res => {
				news.commentloader = false;
				news.showcomment = !news.showcomment;
				console.log(this.currentUser);
				news.Comments.push(new CommentModel({ Id: res.Data, UserCreatedId: this.currentUser.Id, UserName: this.currentUser.FirstName + " " + this.currentUser.LastName, Text: comment, DateCreated: new Date() }));
				news.CommentsCount = news.Comments.length;
			});
	}
	resetFeeds() {
		this.feeds = new Array<NewsfeedModel>();
	}
	makeSroll() {
		if (this.apiProgress == true) return;
		this.searchParams.Timestamp = this.currentTimestamp;
		console.log("[Newsfeed] Scrolling. Getting new news from timestamp [" + this.searchParams.Timestamp + "] and forward");
		this.getNewsFeeds(true);
	}

	addNews(data: any) {
		var orgId = this._store.get('orgId');
		this._http.post(`Organisations/${orgId}/News`, data)
			.map(res => res.json())
			.subscribe(res => {
				res.Data.account = data.accountName;
				res.Data.user = this.currentUser.UserName;
				this.feeds = [new NewsfeedModel(res.Data), ...this.feeds];
				this._notify.success(this.translate.instant("NOTECREATED"));
			})
	}
	deleteComment(news: NewsfeedModel, commentid: string) {
		var commentId = news.Comments[commentid].Id;
		news.Comments.splice(commentid, 1);
		this._http.delete(`News/${news.Id}/Comment/${commentId}`)
			.map(res => res.json())
			.subscribe(res => {
				news.CommentsCount = news.Comments.length;
			})
	}

	ngOnDestroy() {
		if (this.userSub) this.userSub.unsubscribe();
	}
}
