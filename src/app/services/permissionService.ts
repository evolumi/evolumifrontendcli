import { Injectable } from '@angular/core';
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { LoaderService } from './loader.service';
import { OrgService } from '../services/organisation.service';

@Injectable()
export class PermissionService {
    constructor(
        private router: Router,
        private loader: LoaderService,
        private orgService: OrgService) {

        this.orgService.currentOrganisationObservable().subscribe(org => {
            if (org) {
                console.log("[Permissions] Loaded permissions for Organisation : " + org.OrganisationName);
                let permissions: Array<string> = org.UserPermissions;
                if (permissions.find(x => x == "EvolumiAdmin") != undefined) {
                    this.handleGeneralSettings = true;
                    this.handleBilling = true;
                    this.handleUsers = true;
                    this.handleRoles = true;
                    this.handleSalesProcess = true;
                    this.handleAccountTypes = true;
                    this.handleVat = true;
                    this.isGuest = false;
                    this.isEvolumiAdmin = true;
                    this.handleProducts = true;
                    this.handleContracts = true;
                    this.viewContracts = true;
                }
                else {
                    this.handleGeneralSettings = permissions.find(x => x == "HandleDealStatus") != undefined;
                    this.handleBilling = permissions.find(x => x == "HandlePayment" || x == "HandlePaymentDetails") != undefined;
                    this.handleUsers = permissions.find(x => x == "HandleUsers") != undefined;
                    this.handleRoles = permissions.find(x => x == "HandleUserRoles") != undefined;
                    this.handleSalesProcess = permissions.find(x => x == "HandleDealStatus") != undefined;
                    this.handleAccountTypes = permissions.find(x => x == "HandleDealStatus") != undefined;
                    this.handleVat = permissions.find(x => x == "HandleDealStatus") != undefined;
                    this.handleContracts = permissions.find(x => x == "HandleContracts") != undefined;
                    this.viewContracts = permissions.find(x => x == "ViewContracts") != undefined;
                    this.isGuest = org.UserRole == "Guest";
                    this.handleProducts = permissions.find(x => x == "HandleProducts") != undefined;
                }
                this.plan = org.Plan;
                this.isLoaded.next(true);
            }
        });

    }

    private isLoaded: BehaviorSubject<boolean> = new BehaviorSubject(false);
    public IsLoaded(): Observable<boolean> {
        return this.isLoaded.asObservable();
    }

    public isEvolumiAdmin: boolean = false;
    public isGuest: boolean = false;
    public plan: string;

    public handleGeneralSettings: boolean;
    public handleBilling: boolean;
    public handleUsers: boolean;
    public handleRoles: boolean;
    public handleSalesProcess: boolean;
    public handleAccountTypes: boolean;
    public handleVat: boolean;

    public handleProducts: boolean;
    public handleUnits: boolean;

    public handleContracts: boolean;
    public viewContracts: boolean;

    public RedirectForbidden() {
        this.loader.clear();
        this.router.navigate(["/CRM/Error/Forbidden"]);
    }

}
