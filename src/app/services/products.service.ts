import { Injectable, OnInit } from '@angular/core';
import { Api } from './api';
import { OrgService } from './organisation.service';
import { Observable } from "rxjs/observable";
import { Subject } from "rxjs/Subject";
import { DealsModel } from "../models/deals.models";

@Injectable()

export class ProductService implements OnInit {
    private organisationId: string;
    public apiInProgress: boolean = false;
    public productsCount: number;
    public pageSize: number = 40;

    private pickedProducts: Subject<Product[]> = new Subject<Product[]>();
    private displayProductPicker: Subject<boolean> = new Subject<boolean>();
    public showProductPicker(): Observable<boolean> {
        return this.displayProductPicker.asObservable();
    }
    public catalogues: Catalogue[];

    public productPickerDeal: DealsModel;

    private orgSub: any;

    constructor(
        private api: Api,
        private orgService: OrgService

    ) {
        this.organisationId = localStorage.getItem("orgId");
        //Get productcount after organisation changes. (Dissabled for now. Api not up yet)
        this.orgSub = this.orgService.currentOrganisationObservable().subscribe(org => {
            if (org) {
                this.organisationId = org.Id;
                this.api.get(`Organisations/${org.Id}/Products/GetProductCount`).map(res => res.json()).subscribe(data => {
                    this.productsCount = data.Data;
                });
            }
        });
    }

    public getPickedProducts(): Observable<Product[]> {
        return this.pickedProducts.asObservable();
    }
    public addPickedProducts(products: Product[]) {
        this.pickedProducts.next(products);
    }

    public Units() {
        return this.api.get(`Organisations/${this.organisationId}/Units`).map(res => res.json());
    }

    //CATALOGUE FUNCTIONS
    public AddOrUpdateCatalogue(catalogue: Catalogue) {
        if (catalogue.Id) {
            return this.api.put("Catalogues/Put/" + catalogue.Id, catalogue).map(res => res.json());
        }
        else {
            return this.api.post("Organisations/" + this.organisationId + "/Catalogues/Post", catalogue).map(res => res.json());
        }
    }

    public GetCatalogue(catalogueId: string) {
        return this.api.get("Catalogues/Get/" + catalogueId).map(res => {
            var result = res.json();
            if (result.IsSuccess) {
                return result.Data;
            }
            else {

            }
        }, (err: any) => console.log(err));
    }

    public Catalogues() {
        return this.api.get("Organisations/" + this.organisationId + "/Catalogues").map(res => {
            var result = res.json();
            if (result.IsSuccess) {
                this.catalogues = result.Data;
                return result.Data;
            }
            else {
                return [];
            }
        });
    }

    public DeleteCatalogue(catalogueId: string) {
        return this.api.delete("Catalogues/Delete/" + catalogueId).map(res => res.json());
    }

    // PRICE LIST FUNCTIONS
    public AddOrUpdatePriceList(priceList: any) {
        if (priceList.Id) {
            return this.api.put(`Organisations/${this.organisationId}/PriceLists/${priceList.Id}`, priceList).map(res => res.json());
        }
        else {
            return this.api.post("Organisations/" + this.organisationId + "/PriceLists/Post", priceList).map(res => res.json());
        }
    }

    public GetPriceList(priceListId: string) {
        return this.api.get("Organisations/" + this.organisationId + "/PriceLists/Get/" + priceListId).map(res => res.json());
    }

    public PriceLists() {
        return this.api.get("Organisations/" + this.organisationId + "/PriceLists").map(res => {
            var result = res.json();
            if (result.IsSuccess) {
                return result.Data;
            }
            else {
                return [];
            }
        });
    }

    public DeletePriceList(priceListId: string) {
        return this.api.delete("Organisations/" + this.organisationId + "/PriceLists/Delete/" + priceListId).map(res => res.json());
    }

    // PRODUCT FUNCTIONS
    public AddOrUpdateProduct(productitem: any) {
        var product = this.ConvertProductToBackend(productitem);
        //console.log(product);
        if (product.Id) {
            return this.api.put("Products/Put/" + product.Id, product).map(res => res.json());

        }
        else {
            return this.api.post("Organisations/" + this.organisationId + "/Products/Post", product).map(res => {
                var result = res.json()
                return result;
            });
        }
    }

    public GetProduct(productId: string) {
        return this.api.get("Products/Get/" + productId).map(res => {
            var result = res.json();
            if (result.IsSuccess) {
                return this.ConvertProductToFrontend(result.Data);
            }
        });
    }

    public Products(page: number, filters: ProductFilterParameters) {
        return this.api.get(`Organisations/${this.organisationId}/Products?Page=${page}&PageSize=${this.pageSize}${this.GetUrlParameters(filters)}`).map(res => {
            var result = res.json();
            if (result.IsSuccess) {
                return result.Data.map((p: any) => this.ConvertProductToFrontend(p));
            }
            console.log(result);
        });
    }
    public GetUrlParameters(filter: ProductFilterParameters) {
        var res = "";
        for (let prop in filter) {
            if (filter[prop]) {
                res += `&${prop}=${filter[prop]}`
            }
        }
        return res;
    }
    public GetProductsForCatalogue(catalogueId: string, page: number, filters: ProductFilterParameters) {
        return this.api.get(`Products/GetProductsForCatalogue/${catalogueId}?Page=${page}&PageSize=${this.pageSize}${this.GetUrlParameters(filters)}`).map(res => {
            var result = res.json();
            if (result.IsSuccess) {
                return result.Data.map((p: any) => this.ConvertProductToFrontend(p));
            }
            else {
                return [];
            }
        });
    }

    public DeleteProduct(productId: string) {
        return this.api.delete("Products/Delete/" + productId).map(res => {
            var result = res.json();
            if (result.IsSuccess) {
                this.productsCount--;
                this.Catalogues();
            }
            return result;
        });
    }


    public ConvertProductToBackend(product: any) {
        product.PriceLists = product.PriceLists.reduce(function (res: any, current: any) {
            var prices = current.Prices;
            var pricearray = Object.getOwnPropertyNames(prices).reduce(function (res, current) {
                res.push({
                    PriceListId: current,
                    ProductPrice: prices[current]
                })
                return res;
            }, []);

            res.push({
                Range: current.Range,
                Unit: current.Unit,
                Prices: pricearray
            })
            return res;
        }, [])
        return product
    }
    public ConvertProductToFrontend(product: any) {
        var that = this;
        product.PriceLists = product.PriceLists.reduce(function (res: any, current: any) {
            var prices = current.Prices.reduce(function (res: any, current: any) {
                res[current.PriceListId] = current.ProductPrice
                return res;
            }, {})
            var obj = {
                Range: current.Range,
                Unit: current.Unit,
                Prices: prices,
                code: that.getCode()
            }
            res.push(obj);
            return res;
        }, []);
        return product;
    }

    public getCode() {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < 15; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        return text;
    }

    public showProductPickerModal() {
        this.displayProductPicker.next(true);
    }
    public hideProductPickerModal() {
        this.displayProductPicker.next(false);
    }

    ngOnInit() {

    }

    ngOnDestroy() {
        if (this.orgSub) this.orgSub.unsubscribe();
    }
}
export class Product {
    constructor(productObj: any) {
        for (var key in productObj) {
            if (productObj.hasOwnProperty(key)) {
                this[key] = productObj[key];
            }
        }
    }

    Id: string;
    ArticleNumber: string;
    Name: string;
    Description: string;
    Catalogues: string[];
    PriceLists: any;
    Image: string;
    Unit: string;
    Units: number;
    Discount: number;
    Price: number;
    Sum: number;
    PricesAsTotal: boolean;
    PriceIsManual: boolean;
    [key: string]: any;
}

export class ProductPriceList {
    PriceListId: string;
    ProductPrice: number;
    [key: string]: any;
}
export class Catalogue {
    constructor(catalogueObj: any) {
        for (var key in catalogueObj) {
            if (catalogueObj.hasOwnProperty(key)) {
                this[key] = catalogueObj[key];
            }
        }
        this.Products = [];
    }

    Id: string;
    Name: string;
    Description: string;
    Image: string = "";
    ProductsCount: number;
    IsActive: boolean;
    Products: Product[];
    [key: string]: any;
};
export class PriceList {
    constructor() {
        this.Name = "";
        this.Description = "";
        this.Currency = "";
        this.Id = "";
    }
    Id: string
    Name: string;
    Description: string;
    Currency: string;
    Editable: boolean;
    [key: string]: any;
}
export class ProductFilterParameters {
    constructor() {

    }
    Name: string;
    CatalogueId: string;
    PriceListId: string;
    SortingFieldName: string;
    SortingReversed: boolean;
    [key: string]: any;
}