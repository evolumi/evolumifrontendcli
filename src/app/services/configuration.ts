import { Injectable } from '@angular/core';

@Injectable()
export class Configuration {

    // This should match the frontendVersion.key file in the wwwroot folder on the Backend server.
    public version: string = "2.4";

    constructor() { }

    //     // Profile Names ==================================================================================
    //     let Test = "Test";   // Using Default values. (Test API and Test accounts on external services).
    //     let Local = "Local"; // If you have a local API running.
    //     let Live = "Live";   // For Production builds.
    //     // ================================================================================================

    //     // Set profile here ====//
    //     var profile = Local;     // 
    //     //======================//

    //     // Define profiles (Override default values) ======================================================
    //     // Default values listed below.

    //     if (profile == Local) {
    //         this.apiUrl = "http://localhost:30409";
    //     }

    //     if (profile == Live) {
    //         this.apiUrl = "https://api.evolumi.se";
    //         this.intercomAppId = 'vw8751x3';
    //         this.useSinchTestNumber = false;
    //         this.sinchApplicationKey = 'ee822eee-6e0b-48db-9512-27a5bb1351ee';
    //         this.customerSuccessOrg = "f43f32fa-7427-4321-848b-02f7bf277e96";
    //         this.lastActiveCustomField = 'f2b2a1a7-d695-4c82-89f4-58204a6631ed';
    //     } 
    //     // ================================================================================================
    // }

    // // Default values. (Development values is default, Set values for production on the "Live" profile above)
    // public apiUrl = 'http://evolumidev.azurewebsites.net';

    public defaultLanguage = "en";
    public languageFileVersion = 2.47;

    // public sinchApplicationKey = '6a5d904e-30d8-4a6d-a00c-c85eaa3576ee';
    // public useSinchTestNumber = false;
    // public sinchTestNumber = "+46000000000";
    // public intercomAppId = 'zfbzudgl';
    // public customerSuccessOrg = "dfe4d4d3-ac7d-406d-a0a5-f3be5d5132bf";
    // public lastActiveCustomField = 'unknown';

    // The order is imporrtant. so dont change. (matches enum in backend)
    public currencies: Array<string> = ["USD", "SEK", "HKD", "EUR", "GBP", "CNY", "NOK", "JPY", "ZAR"];

    public defaultLanguageFile(): string {
        return this.defaultLanguage + "." + this.languageFileVersion;
    }
}

// Set keboard shortcuts here ALT+[key] will trigger the command.
export const Shortcuts = {
    phone: 80, // p
    quickadd: 81, // q
    search: 83, // s
    chat: 67, // p        
}





