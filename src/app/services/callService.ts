import { Injectable } from '@angular/core';
import { Api } from './api';
import { NotificationService } from './notification-service';
import { TranslateService } from '@ngx-translate/core';
import { OrgService } from '../services/organisation.service';
import { AuthService } from './auth-service';
import { ActionsCollection } from './actionservice';
import { ButtonPanelService } from './button-panel.service';
import { environment } from "../../environments/environment";

declare var $: any;
declare var SinchClient: any;

interface MyWindow extends Window {
    callServiceRef: CallService;
}

declare var window: MyWindow;

@Injectable()
export class CallService {

    public active: boolean = false;
    public status: string = "";
    public calling: boolean = false;
    public number: string;
    public call: any;
    public callData: any;

    private sinchClient: any;
    private callClient: any;

    private activeOrgId: string;
    private activeUserId: string;

    private timer: any;
    private callTimer: any;
    private callDuration: number;
    public timeLeft: number;
    private orgSub: any;
    private userSub: any;

    public cli: PhoneNumber;

    private actionId: string;
    public action: any;
    public actionSaved: boolean = false;

    constructor(private buttonService: ButtonPanelService, public notifications: NotificationService, private translate: TranslateService, private _api: Api, private orgService: OrgService, private auth: AuthService, private actionService: ActionsCollection) {
        window.callServiceRef = this;

        // Dont start sinch in Safari. Seems to be not working.
        let isSafari = navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1;

        if (isSafari) {
            console.log("[Sinch] Browser incompability! Sinch is not supported in Safari browser. [Shutting down]");
        }
        else {
            console.log("[Sinch] Starting up..");

            this.orgSub = this.orgService.currentOrganisationObservable().subscribe(org => {
                if (org) {
                    if (org.Id == this.activeOrgId) {
                        return;
                    }

                    if (this.activeOrgId) {
                        console.log("[Sinch] Client Terminated!");
                        this.sinchClient.terminate();
                    }

                    this.cli = org.UserPhoneNumber ? org.UserPhoneNumber : org.DefaultPhoneNumber;
                    console.log("[Sinch] Setting CLI: ", this.cli ? this.cli.Number : "");

                    // For now only allow callService for Evolumi users.
                    if (this.auth.isEvolumi) {
                        this.newClient();
                        this.startClient(org.Id);
                    }
                    else {
                        console.log("[Sinch] Disabled for now");
                        this.status = "Waiting for Auth";
                    }
                }
            });

            this.userSub = this.auth.currentUserObservable().subscribe(user => {
                if (user) {
                    this.activeUserId = user.Id;
                    if (this.status == "Waiting for Auth" && this.auth.isEvolumi) {
                        this.newClient();
                        this.startClient(localStorage.getItem("orgId"));
                    }
                }
            });
        }
    }

    // Setup

    private newClient() {
        this.sinchClient = new SinchClient({
            applicationKey: environment.sinchApplicationKey,
            capabilities: { messaging: true, calling: true },
            onLogMessage: function (message: string) {
                //console.log(message);
            }
        });
        this.status = "";
        console.log("[Sinch] Client Created with applicationKey[" + environment.sinchApplicationKey + "]");
    }

    private startClient(orgId: string) {
        this._api.get("Organisations/" + orgId + "/SinchTicket").map(res => res.json()).subscribe(response => {
            if (response.IsSuccess) {
                this.sinchClient.start(response.Data)
                    .then(function () {
                        console.log("[Sinch] Start: Success!");
                        window.callServiceRef.activeOrgId = orgId;
                        window.callServiceRef.active = true;
                    }).fail(function (error: any) {
                        console.log(error);
                    });
            }
            else {
                console.log("[Sinch] Start: Failed! Did not recive a Ticket");
            }
        },
            error => {
                console.log("[Sinch] Start: Failed! Unknown error when getting a Ticket :" + error);
            });

    }

    private update(call: any) {
        console.log("[Sinch] Updating call.");
        this._api.requestGet("Organisations/" + this.activeOrgId + "/UpdateCall/" + call.callId, res => {
            if (res) {
                console.log("[Sinch] Call updated!", res);
                this.callData = res;
                this.action = res.Action;

                // Check for hangup
                if (res.Status == "HangingUp") {
                    this.hangUp();
                }
                console.log("STOP:", res.StopTime, new Date(res.StopTime));
                console.log("NOW :", +new Date(), new Date());
                this.timeLeft = (res.StopTime - +new Date()) / 1000;
            }

        }, error => {
            console.log("[Sinch] Error updating call!");
        });
    }


    // Functions

    // Close action with chosen status and create a new Open copy.
    // public closeAndCreateNewAction(status: string) {
    //     var newAction: any;
    //     Object.assign(newAction, this.action);
    //     newAction.ExternalId = "";

    //     this.actionService.createAction(newAction).subscribe(res => {
    //         this.notifications.success(this.translate.instant("ACTIONCREATED"));
    //     }, err => {
    //         this.notifications.error(this.translate.instant("ERROR"));
    //     });

    //     this.action.Status = status;
    //     this.actionService.updateAction(this.action).subscribe(res => {
    //         this.action = res;
    //     }, err => {
    //         this.notifications.error(this.translate.instant("ERROR"));
    //     });;
    // }

    public startPhoneCall(number: string, user: string, person: string = "", account: string = "", action: string = "") {

        if (this.calling) {
            this.notifications.error(this.translate.instant("PHONECALLALREADYACTIVE"));
            return;
        }

        this.actionId = action;

        let headers = { user: user, contact: person, account: account, action: action }
        console.log("[Sinch] Setting headers: ", headers);
        let callListeners = {
            onCallEstablished: function (call: any) {
                window.callServiceRef.onCallStarted(call);
            },
            onCallProgressing: function (call: any) {
                window.callServiceRef.onCallProgressing(call);
            },
            onCallEnded: function (call: any) {
                window.callServiceRef.onCallEnded(call);
            }
        }

        this.calling = true;
        this.number = number;
        this.status = this.translate.instant("CALLING");
        this.buttonService.toggleClass("phone", "ringing");

        this.callClient = this.sinchClient.getCallClient();
        console.log("[Sinch] Calling number : " + (environment.useSinchTestNumber ? "(TestNumber) " + environment.sinchTestNumber : number));
        this.call = this.callClient.callPhoneNumber(environment.useSinchTestNumber ? environment.sinchTestNumber : number, headers);
        this.call.addEventListener(callListeners);

    }

    public hangUp() {

        console.log("[Sinch] Reset");
        if (this.call && this.calling) {
            this.call.hangup();
        }
        this.clear();
    }

    public validateNumber(number: PhoneNumber) {
        console.log("[Sinch] Verification started on number[" + number.Number + "]");
        var customData = { UserId: this.activeUserId, OrganisationId: this.activeOrgId };
        console.log(customData);
        var verification = this.sinchClient.createCalloutVerification(number.Number, JSON.stringify(customData))
        verification.initiate().then((success: any) => {
            console.log("[Sinch] Verification Successful! on number[" + number.Number + "]");
            console.log(success);
            number.Verified = true;
        }).fail((error: any) => {
            console.log("[Sinch] Verification Failed! on number[" + number.Number + "]");
            console.log(error);
        });
    }

    // Event handlers.

    private onCallProgressing(call: any) {
        console.log("[Sinch] Call Progressing" + JSON.stringify(call.getDetails()));
        this.status = this.translate.instant('CALLING');

        $('audio#ringback').prop("currentTime", 0); //Ensure ringback start from beginning
        $('audio#ringback').trigger("play");

        this.callDuration = 0;
    }

    private onCallStarted(call: any) {
        console.log("[Sinch] Call Started");
        console.log(call);
        if (call.error) {
            console.log("[Sinch] Connecting call Failed! -> " + call.error.message);
            this.clear();
            this.notifications.error(this.translate.instant("ERROR"));
            return;
        }
        // If no error. setup stream.
        $('audio#incoming').attr('src', call.incomingStreamURL);
        $('audio#ringback').trigger("pause"); //End ringback

        this.status = this.translate.instant('CONNECTED');
        this.notifications.success(this.translate.instant("PHONECALLESTABLISHED"));

        // Every 5 seconds. Update call to check if we need to hangup.
        this.timer = setInterval(() =>
            this.update(call), 5000);
        this.update(call);

        this.callTimer = setInterval(() => {
            this.callDuration++;
            this.timeLeft--;
        }, 1000);
    }

    private onCallEnded(call: any) {
        console.log("[Sinch] Call Ended: " + call.getEndCause());
        console.log(call);
        this.status = this.translate.instant('DISCONNECTED');
        $('audio#incoming').removeAttr('src');
        $('audio#ringback').trigger("pause"); //End the ringback

        this.update(call);

        if (this.timer) clearInterval(this.timer);
        if (this.callTimer) clearInterval(this.callTimer);
        this.timer = undefined;
        this.notifications.success(this.translate.instant("PHONECALLENDED"));
        this.clear();
        if (this.actionId)
            this.actionService.triggerActionUpdate(this.actionId);
        this.buttonService.toggleClass("phone", "ringing");
    }


    // Cleanup

    private clear() {
        console.log("[Sinch] Reset");
        this.calling = false;
        this.number = "";
        this.call = undefined;
    }

    // private handleError(error: any) {
    //     this.clear();
    //     console.log("[Sinch] Error: " + error);
    // }

    public updateActionComment(text: string) {
        if (this.action.Comments != text) {
            this.action.Comments = text;
            this.actionService.updateComment(this.action, text).subscribe(res => {
                this.actionSaved = res;
            });
        }
    }


    ngOnDestroy() {
        if (this.orgSub) this.orgSub.unsubscribe();
        window.callServiceRef = null;
    }

}

export class PhoneNumber {
    Id: string = "";
    Number: string = "";
    Description: string = "";
    Verified: boolean = false;
}
