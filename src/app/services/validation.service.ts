import { Injectable } from '@angular/core';

@Injectable()
export class ValidationService {
    static getValidatorErrorMessage(code: string) {
        let config: any = {
            'required': 'Required',
            'invalidCreditCard': 'Is invalid credit card number',
            'invalidEmailAddress': 'Invalid email address',
            'invalidCvv': 'Invalid Cvv',
            'invalidPassword': 'Invalid password. Password must be at least 6 characters long, and contain a number.',
            'invalidHex': 'Invalid hex.'
        };

        return config[code];
    }

    static creditCardValidator(control: any) {
        // Visa, MasterCard, American Express, Diners Club, Discover, JCB
        let ex1 = /^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$/;
        let card = control.value.match(ex1);
        if (card) {
            return null;
        } else {
            return { 'invalidCreditCard': true };
        }
    }

    static cvvValidator(control: any) {
        // Visa, MasterCard, American Express, Diners Club, Discover, JCB
        if (control.value.length != 0 && control.value.length === 3 && control.value.match(/^[0-9]+$/)) {
            return null;
        } else {
            return { 'invalidCvv': true };
        }
    }

    static emailValidator(control: any) {
        let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (control.value === null || typeof control.value === 'undefined') {
            return { 'invalidEmailAddress': true };
        } else if (control.value.match(re)) {
            return null;
        } else {
            return { 'invalidEmailAddress': true };
            //return null;
        }
    }
    static emailValidatorIfNotEmpty(control: any) {
        let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (control.value === null || typeof control.value === 'undefined' || control.value == "") {
            return null;
        } else if (control.value.match(re)) {
            return null;
        } else {
            return { 'invalidEmailAddress': true };
            //return null;
        }
    }

    static passwordValidator(control: any) {
        // {6,16}           - Assert password is between 6 and 100 characters
        // (?=.*[0-9])       - Assert a string has at least one number 
        // one none letter none number
        let re = /^(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?=.*[!@=?#$%^&*()\-\_{\}])[a-zA-Z0-9!@=?#$%^&*!@=?#$%^&*()\-\_\{\}\[\]\;\:\'\'?\/>\.\<\`\,]{6,16}$/;
        if (control.value === null || typeof control.value === 'undefined') {
            return { 'invalidPassword': true };
        } else if (control.value.match(re)) {
            return null;
        } else {
            return { 'invalidPassword': true };
            //return null;
        }
    }
    static typeCheckor(control: any) {
        if (control.value === null || typeof control.value === 'undefined' || control.value === 'Select') {
            return { 'required': true };
        } else {
            return null;
        }
    }

    static isValidEmail(email: any) {
        let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (email === null || typeof email === 'undefined') {
            return false;
        } else if (email.match(re)) {
            return true;
        } else {
            return false;
        }
    }
    static isValidNumber(value: any) {
        var numbers = /^[0-9]+$/;
        if (value === null || typeof value === 'undefined') {
            return false;
        }
        else if (value.toString().match(numbers)) {
            return true;
        }
        else {
            return false;
        }
    }

    static hexvalidator(control: any) {
        var hexformat = /(^#[0-9A-Fa-f]{6}$)|(^#[0-9A-Fa-f]{3}$)/;
        if (control.value === null || typeof control.value === 'undefined') {
            return { 'invalidHex': true };
        } else if (control.value.match(hexformat)) {
            return null;
        } else {
            return { 'invalidHex': true };
            //return null;
        }
    }
}
