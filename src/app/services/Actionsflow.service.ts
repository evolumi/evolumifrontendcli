import { Injectable } from '@angular/core';
import { Api } from './api';
import { ActionsModel } from '../models/actions.models';
import { LocalStorage } from './localstorage';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';

export interface searchParams {
	SearchString: string,
	CreatorId: string,
	AssignedId: string,
	Types: any,
	Priority: string,
	Status: string,
	Labels: any,
	ContactId: string,
	DealId: string,
	StartDate: string,
	EndDate: string,
	Page: number,
	PageSize: number,
	Owners: string,
	[key: string]: any
}

@Injectable()

export class ActionsflowCollection {

	public searchParams: searchParams;
	public queryString: string;
	public apiInProgress: boolean = false;
	Actionsflow: Array<ActionsModel> = new Array<ActionsModel>();

	public actionFlowChange$: Observable<boolean>;
	private _actionFlowChangeObserver: Observer<boolean>;

	constructor(public _http: Api, public _store: LocalStorage) {
		this.searchParams = {
			SearchString: '', CreatorId: '', AssignedId: '', Types: '', Priority: '', Status: '', Labels: '', ContactId: '', DealId: ''
			, StartDate: '', EndDate: '', Page: 1, PageSize: 20, Owners: ''
		}
		this.actionFlowChange$ = new Observable<boolean>((observer: any) => {
			this._actionFlowChangeObserver = observer;
		}).share();
	}

	reset() {
		this.searchParams = {
			SearchString: '', CreatorId: '', AssignedId: '', Types: '', Priority: '', Status: "0", Labels: '', ContactId: '', DealId: ''
			, StartDate: '', EndDate: '', Page: 1, PageSize: 20, Owners: ''
		};
	}

	getSearchString() {
		var str: any[] = [];
		for (var p in this.searchParams)
			if (this.searchParams.hasOwnProperty(p)) {
				if (this.searchParams[p] === '' || typeof this.searchParams[p] === 'undefined')
					continue;
				str.push(encodeURIComponent(p) + "=" + this.searchParams[p]);
			}


		return str.join("&");
	}
	getActions(scroll = false) {
		if (!scroll) this.searchParams.Page = 1;
		var orgId = this._store.get('orgId');
		var query = this.getSearchString();
		console.log("[Action-Flow] Loading actions. Query: " + query);
		this._http.get(`Organisations/${orgId}/Actions?${query}`)
			.map(res => res.json())
			.subscribe(res => {
				if (scroll == false) this.Actionsflow = [];
				for (var feed of res.Data) {
					this.Actionsflow = [...this.Actionsflow, new ActionsModel(feed)];
				}
				this.apiInProgress = false;
				// _actionFlowChangeObserver is undefined when clickg action in ActionBlock inside Contact modal.
				if (this._actionFlowChangeObserver) this._actionFlowChangeObserver.next(true);
			});
	}

	UpdateSearchFields(updateArray: any) {
		for (var arr in updateArray) {
			(updateArray[arr] === 'All Activity') ? this.searchParams[arr] = '' : this.searchParams[arr] = updateArray[arr];
		}
		this.getActions();

	}

	onScroll() {
		if (this.apiInProgress == true) return;
		this.apiInProgress = true;
		this.searchParams.Page = (this.searchParams.Page | 0) + 1;
		this.getActions(true);
	}


}
