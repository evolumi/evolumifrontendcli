import { Injectable } from '@angular/core';

const url = "//fast.wistia.com/assets/external/E-v1.js//fast.wistia.com/assets/external/E-v1.js"
const charset = "ISO-8859-1"

@Injectable()
export class WistiaService {

  constructor() { }

  public loadScript() {
    let script = document.createElement('script');
    script.src = url;
    script.charset = charset;
    script.type = 'text/javascript';

    if (document.body.contains(script)) {
      return;
    }
    document.getElementsByTagName('head')[0].appendChild(script);
  }
}
