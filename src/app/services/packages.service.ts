import { Injectable } from '@angular/core';
import { Api } from './api';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs/Subscription';

import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { Subject } from "rxjs/Subject";
import { Observable } from 'rxjs/Observable';

import { LocalStorage } from './localstorage';
import { LoaderService } from './loader.service';
import { OrgService } from './organisation.service';
import { NotificationService } from './notification-service';

export class Package {
    public Id: string;
    public NameLabel: string;
    public Color: string;
    public DescriptionLabel: string;
    public ImageUrl: string;
    public Information: Array<Information>;
    public hasPackage: boolean;
    public HasVolumePricing: boolean;
    public MonthlyPrices: any;
    public YearlyPrices: any;

    public Features: Array<string> = new Array<string>();
    public Campaigns: Array<Campaign> = new Array<Campaign>();


}

export class Feature {
    public Name: string;
    public NameLabel: string;
    public DescriptionLabel: string;
    public ImageUrl: string;
    public Information: Array<Information>;
    public VolumeTextLabel: string;
    public VolumeQuantityLabel: string;
    public VolumePricing: PriceList;

    public selectedValue: number;
    public selectedPrice: number;
}

export class Information {
    public ImageUrl: string;
    public VideoUrl: string;
    public Label: string;
    public Link: string;
}

export class VolumePrice {
    public Max: number;
    public Price: number;
}

export class PriceList {
    [key: string]: Array<VolumePrice>;
}

export class Campaign {
    public Code: string;
    public Discount: number;
    public StartDate: Date;
    public EndDate: Date;
}

export class AdminAddPackageModel {
    public PackageId: string;
    public Interval: string;
    public Licences: number;
    public Currency: string;
    public UserLicenses: string[];
    public Trial: boolean;
    public PaidUntil: Date;
}

export class VolumePriceSelection {
    [key: string]: number;
}

export class AddPackageModel {
    public PackageId: string;
    public Interval: string;
    public Licences: number;
    public Currency: string;
    public UserLicenses: Array<any> = new Array<any>();
    public choosenPackage: Package;
    public Active: boolean;
    public Trial: boolean;
    public ManualPayment: boolean;
    public ReActivate: boolean;
    public DiscountCode: string;
    public Volumes: VolumePriceSelection = {};

    public volumePriceTotal: number;
}

export class PackageSummary {
    public MonthlyTotal: number;
    public YearlyTotal: number;
    public FirstPayment: number;
    public Vat: number;
    public ToPay: number;
    public NextMonthlyPayDay: number;
    public NextYearlyPayDay: number;
    public MonthDaysToPay: number;
    public YearDaysToPay: number;
    public Currency: string;
}



@Injectable()
export class PackagesCollection {

    private orgSub: Subscription;
    public packagesWithFeature: Array<Package> = [];
    public packagesToShow: Array<any> = [];
    public ownedPackages: Array<any> = [];
    public orgPackages: Array<AddPackageModel>;
    public Currency: any;
    public orgPaymentData: any;
    public orgCurrency: string;

    public cart: Array<AddPackageModel> = [];
    public cartSummary: PackageSummary;
    public creditCard: any;

    public loadingPrices: boolean = false;
    private packagesLoaded: Date;

    public buildingPackage: AddPackageModel = new AddPackageModel();

    private features$: BehaviorSubject<Array<Feature>> = new BehaviorSubject([]);
    public features(): Observable<Array<Feature>> {
        return this.features$.asObservable();
    }

    private packages$: BehaviorSubject<Array<Package>> = new BehaviorSubject([]);
    public packages(): Observable<Array<Package>> {
        return this.packages$.asObservable();
    }

    private selectedFeature$: Subject<string> = new Subject();
    public selectedFeature(): Observable<string> {
        return this.selectedFeature$.asObservable();
    }

    private selectedPackage$: Subject<Package> = new Subject();
    public selectedPackageObservable(): Observable<Package> {
        return this.selectedPackage$.asObservable();
    }
    constructor(
        public _loader: LoaderService,
        public _http: Api,
        public _store: LocalStorage,
        private notify: NotificationService,
        private translate: TranslateService,
        private orgService: OrgService
    ) {
        this.orgSub = this.orgService.currentOrganisationObservable().subscribe(org => {
            if (org) {
                this.creditCard = org.PaymentData.CreditCard;
                this.orgPackages = org.Packages;
                this.orgPaymentData = org.PaymentData;
                this.orgCurrency = org.CurrencyName;
                this.getPackages();
            }
        })
    }

    public getAllPackages() {
        return this._http.get("Packages").map(res => res.json());
    }

    public getPackages(force: boolean = false) {
        if (!this.packagesLoaded || new Date().getMinutes() - this.packagesLoaded.getMinutes() >= 2 || force) {
            this._http.get('Features')
                .map((res: any) => res.json()).subscribe(res => this.features$.next(res.Data));
            this._http.get('Organisations/' + this._store.get('orgId') + '/Packages/Active')
                .map((res: any) => res.json()).subscribe(res => {
                    this.packages$.next(res.Data)
                    this.removingAlreadyExsistingPackages();
                });
            this.packagesLoaded = new Date();
        }
    }

    public loadFeature(featureName: string) {
        this.selectedFeature$.next(featureName);
    }

    public showSelectedPackageWithUsers(thisPackage: Package) {
        this.selectedPackage$.next(thisPackage);
    }

    public checkingTotalSum() {
        if (this.cart.length) {
            this.cart.forEach(x => x.Currency = this.Currency);
            this.loadingPrices = true;
            this._http.post('Packages/PriceSummary', this.cart)
                .map(res => res.json())
                .subscribe(response => {
                    this.loadingPrices = false;
                    if (response.IsSuccess) {
                        this.cartSummary = response.Data
                    }
                });
        }
    }

    public calcVolumeCost(pack: AddPackageModel): number {
        let volumePrice = 0;
        for (let featureName in pack.Volumes) {
            let feature = this.features$.value.find(x => x.Name == featureName);
            volumePrice += feature.VolumePricing[this.Currency].find(x => x.Max == pack.Volumes[featureName]).Price;
        }
        return volumePrice;
    }

    public getPackageLowestVolumePrice(pack: Package, currency: string): Observable<number> {

        return this.features$.asObservable().map(features => {
            if (features.length) {
                let price = 0;

                for (let featureName of pack.Features) {
                    let feature = features.find(x => x.Name == featureName);
                    let PriceList = feature.VolumePricing[currency];
                    if (PriceList && PriceList.length) {
                        price += PriceList[0].Price;
                    }
                }
                return price;
            }
            return undefined;
        });
    }

    public getUpdatePrice(): Observable<PackageSummary> {
        return this._http.post('Packages/' + this.buildingPackage.PackageId + '/UpdateSummary', this.buildingPackage).map(res => {
            var result = res.json();
            if (result.IsSuccess) {
                return result.Data;
            }
            else {
                return null
            }
        });
    }


    public settingValueOnBuildingPackage() {
        this.buildingPackage.Licences = 1;
    }

    public settingCurrencyIfYouhavePackage() {
        if (this.orgPackages.length) {
            for (let savedPackages of this.orgPackages) {
                this.Currency = savedPackages.Currency
            }
        } else {
            this.Currency = this.orgCurrency;
        }
    }




    public sendPackagesBackend() {

        this.buildingPackage.ReActivate = false;
        this.buildingPackage.volumePriceTotal = this.calcVolumeCost(this.buildingPackage);

        this.cart.push(this.buildingPackage);
        this.buildingPackage = new AddPackageModel();
    }

    public updatePackagesObject: AddPackageModel = new AddPackageModel();
    public updatePackagesBackend() {
        console.log("userlist setting package", this.buildingPackage);
        this.updatePackagesObject = this.buildingPackage;
        this.buildingPackage = new AddPackageModel();
    }

    public removingAlreadyExsistingPackages() {
        this.packagesToShow = [];
        this.ownedPackages = [];

        for (let alPackage of this.packages$.value) {
            if (!this.cart) {
                if (this.orgPackages.find(x => x.PackageId == alPackage.Id)) {
                    this.ownedPackages.push(alPackage);
                } else {
                    this.packagesToShow.push(alPackage);
                }
            }
            else if (!this.cart.find(x => x.PackageId == alPackage.Id)) {
                if (this.orgPackages.find(x => x.PackageId == alPackage.Id)) {
                    this.ownedPackages.push(alPackage);
                } else {
                    this.packagesToShow.push(alPackage);
                }
            }
        }
    }

    public adminAddPackages(orgId: string, packageObj: AdminAddPackageModel, callback: (success: boolean) => any = null) {
        this._http.post("Admin/Organisation/" + orgId + "/Package", packageObj).map(res => res.json()).subscribe(data => {
            callback(data.IsSuccess);
        });
    }

    public adminEditPackages(orgId: string, packageObj: AdminAddPackageModel, callback: (success: boolean) => any = null) {
        this._http.put("Admin/Organisation/" + orgId + "/Package", packageObj).map(res => res.json()).subscribe(data => {
            callback(data.IsSuccess);
        });
    }

    public addPackages(success : () => any = null) {
        console.log("[Package] Adding Packages: Start!");
        let orgId = this._store.get('orgId');
        this._http.post('Organisations/' + orgId + '/Packages', this.cart)
            .map(res => res.json())
            .subscribe(a => {
                if (a.IsSuccess) {
                    console.log("[Package] Add Packages: Success!.");
                    this.notify.success(this.translate.instant("PACKAGESADDED"));
                    this.orgService.getOrganisation(orgId);
                    this.cart = [];
                }
            });
    }

    public updatePackages(Id: string) {
        console.log("[Package] Updating Packages: Start!");
        let orgId = this._store.get('orgId');
        this._http.put('Organisations/' + orgId + '/Packages/' + Id, this.updatePackagesObject)
            .map(res => res.json())
            .subscribe(a => {
                console.log("[Package] Update Packages: Success!.");
                this.notify.success(this.translate.instant("PACKAGESUPDATED"));
                this.cart = a.Data;
                this.orgService.getOrganisation(orgId);
            });
    }

    public cancelPackage(packageId: any) {
        console.log("[Package] Cancel Packages: Start!");
        console.log(packageId)
        let orgId = this._store.get('orgId');
        this._http.delete('Organisations/' + orgId + '/Packages/' + packageId)
            .map(res => res.json())
            .subscribe(a => {
                console.log("[Package] Cancel Packages: Success!.");
                this.notify.success(this.translate.instant("PACKAGESCANCEL"));
                this.orgService.getOrganisation(orgId);
            });
    }

    ngOnDestroy() {
        if (this.orgSub) this.orgSub.unsubscribe();
    }

}