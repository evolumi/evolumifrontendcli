import { CanDeactivate } from '@angular/router';
import { ConfirmationService } from "../confirmation.service";
import { TranslateService } from "@ngx-translate/core";
import { Injectable } from "@angular/core";
import { TemplateMainComponent } from "../../modules/crm/templates/scaffolding/template-main/template-main.component";

@Injectable()
export class TemplateSaveGuard implements CanDeactivate<any> {

    constructor(private confSvc: ConfirmationService,
        private translate: TranslateService) { }

    canDeactivate(comp: any): any {
        if (comp.isUnsaved()) {
            this.confSvc.showModal(this.translate.instant('UNSAVEDCHANGESHEADER'), this.translate.instant('UNSAVEDCHANGESTEXT'));
            return this.confSvc.modalResponseObs().first();
        } else {
            return true;
        }
    }
}