import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from '../auth-service';

@Injectable()
export class EvolumiGuard implements CanActivate {

    constructor(private authService: AuthService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

        // Check if user has evolumi claim
        if (this.authService.isEvolumi) {
            return true;
        }

        return false;
    }
}

@Injectable()
export class EvolumiHandleUsersGuard implements CanActivate {

    constructor(private authService: AuthService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

        // Check if user has evolumi claim
        if (this.authService.evolumiAdminType == "HandleUsers") {
            return true;
        }

        return false;
    }
}

@Injectable()
export class EvolumiSuperAdminGuard implements CanActivate {

    constructor(private authService: AuthService, private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

        // Check if user has evolumi claim
        if (this.authService.evolumiAdminType == "SuperAdmin") {
            return true;
        }

        console.log("[Guard] Evolumi SuperAdmin claim not found! -> Redirecting to Overview");
        this.router.navigate(['CRM/Overview']);

        return false;
    }
}