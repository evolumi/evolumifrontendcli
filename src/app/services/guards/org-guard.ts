import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { OrgService } from '../organisation.service';

@Injectable()
export class OrgGuard implements CanActivate {

    constructor(private router: Router, private orgService: OrgService) {

    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

        if (this.orgService.isLoaded) {
            return true;
        }

        let orgId = localStorage.getItem("orgId");
        if (orgId) {
            console.log("[Org-Guard] Waiting for Organisation Login. Saving URL : " + state.url);
            this.orgService.redirectUrl = state.url;
        }
        else {
            console.log("[Org-Guard] Stopped navigation to this URL. No Organisation selected.");
            console.log("[Org-Guard] Navigating to Overview.");
            this.router.navigateByUrl('/Overview');
        }

        return false;
    }
}