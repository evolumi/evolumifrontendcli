import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { OrgService, Permissions, Features } from '../organisation.service';

@Injectable()
export class EvoRequireGuard implements CanActivate {

    constructor(private router: Router, private orgService: OrgService) {


    }

    canActivate(route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot) {

        // Set data property of route like this.. data: { permissions: ['SuperAdmin', 'HandleProducts'] }
        let permissions = route.data["permissions"] as Array<Permissions>;
        let features = route.data["features"] as Array<Features>;

        if (permissions && permissions.length) {
            for (let p of permissions) {
                if (!this.orgService.hasPermission(p))
                    this.redirect();
            }
        }

        if (features && features.length) {
            for (let f of features) {
                if (!this.orgService.hasFeature(f))
                    this.redirect();
            }
        }

        return true;
    }

    private redirect() {
        console.log("[Evo-Require-Guard] Stopping navigation to this page. Requirements not met.");
        console.log("[Evo-Require-Guard] Redirecting to Error/Forbidden");
        this.router.navigate(["/CRM/Error/Forbidden"]);
    }
}
