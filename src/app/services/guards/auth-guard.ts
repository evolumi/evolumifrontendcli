import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from '../auth-service';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private authService: AuthService, private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

        // Check if user is Authenitcated.
        if (this.authService.isLoggedIn) {
            return true;
        }

        console.log("[Auth-Guard] Stopped navigation to this URL. User not Authenticated.");

        // Save URL.
        this.authService.setRedirectUrl(state.url);

        // Try login with Token if one exists.
        if (localStorage.getItem("userDetails")) {
            this.authService.TryTokenLogin();
        }
        // Else Navigate to the login page
        else {
            console.log("[Auth-Guard] Navigating to Login.");
            this.router.navigate(['/Login']);
        }

        return false;
    }
}