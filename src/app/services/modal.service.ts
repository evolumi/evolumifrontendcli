import { Injectable } from '@angular/core';
import { Subject } from "rxjs/Subject";

@Injectable()
export class ModalService {

    private clickToMail$: Subject<any> = new Subject();
    public ClickToMailTrigger(): any {
        return this.clickToMail$.asObservable();
    }

    private document$: Subject<any> = new Subject();
    public DocumentTrigger(): any {
        return this.document$.asObservable();
    }


    private contact$: Subject<any> = new Subject();
    public contactTrigger(): any {
        return this.contact$.asObservable();
    }

    private action$: Subject<any> = new Subject();
    public actionTrigger(): any {
        return this.action$.asObservable();
    }

    public showMailModal(email: string, account: any = null, actionGroupId: string = null, contact: any = null) {
        console.log("[Modal] ShowMailModal Group: " + actionGroupId);
        this.clickToMail$.next({ email: email, account: account, actionGroupid: actionGroupId, contact: contact });
    }

    public showMassMailModal(accounts: Array<string>) {
        this.clickToMail$.next({ accounts: accounts });
    }

    public showMassMailFilterModal(searchQuery: string) {
        this.clickToMail$.next({ searchQuery: searchQuery });
    }

    public showDocumentModalById(documentId: string, accountId: string) {
        this.document$.next({ documentId: documentId, accountId: accountId });
    }

    public showDocumentModal(document: any, account: any) {
        this.document$.next({ document: document, account: account });
    }

    public showContactModal(contactId: string, accountId: string) {
        this.contact$.next({ contactId: contactId, accountId: accountId });
    }

    public showCreateContactModal(contact: any, accountId: string = null) {
        this.contact$.next({ contact: contact, accountId: accountId });
    }

    public showMultiActionModal(source: string) {
        console.log("[Modal] ShowAction Multi");
        this.action$.next({ multiMode: true, source: source });
    }
}

