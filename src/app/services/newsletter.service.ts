import { Injectable } from '@angular/core';
import { Api } from './api';
import { NotificationService } from './notification-service';
import { TranslateService } from '@ngx-translate/core';
import { MailListModel, NewsletterModel } from '../models/newsletter.models'
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/observable';

export interface SearchParams {
    // Name: string;
    // MailLists: Array<string>;
    // StartDate?: number;
    // EndDate?: number;
    // SortingFieldName: string;
    Page: number;
    PageSize: number;

    [key: string]: any;
}

@Injectable()
export class NewsletterService {

    private dataStore: {
        newsletters: Array<NewsletterModel>,
        mailLists: Array<MailListModel>
    }

    public searchParams: SearchParams;

    private apiInProgress: boolean;

    private newslettersList$ = new BehaviorSubject<Array<NewsletterModel>>(null);
    public newslettersListObs(): Observable<Array<NewsletterModel>> {
        return this.newslettersList$.asObservable();
    }

    private currentNewsletter$ = new BehaviorSubject<NewsletterModel>(null);
    public currentNewsletterObs(): Observable<NewsletterModel> {
        return this.currentNewsletter$.asObservable();
    }

    private maillistList$ = new BehaviorSubject<Array<MailListModel>>(null);
    public maillistListObs(): Observable<Array<MailListModel>> {
        return this.maillistList$.asObservable();
    }

    private currentMailList$ = new BehaviorSubject<MailListModel>(null);
    public currentMailListObs(): Observable<MailListModel> {
        return this.currentMailList$.asObservable();
    }

    // private searchNewsletters$ = new Subject<string>();

    constructor(private api: Api,
        private notify: NotificationService,
        private translate: TranslateService) {
        this.resetSearch();
        this.dataStore = { newsletters: [], mailLists: [] };
        this.searchParams = { Page: 0, PageSize: 20 };

        // this.searchNewsletters$
        //     .debounceTime(700)
        //     .distinctUntilChanged()
        //     .subscribe(x => this.getNewsletters())
    }

    getNewsletters(scroll = false) {
        const query = this.getSearchString();
        this.api.get(`Newsletters?${query}`)
            .map(res => res.json())
            .subscribe(res => {
                if (!scroll) this.dataStore.newsletters = [];
                if (res.IsSuccess) {
                    this.dataStore.newsletters.push(...res.Data);
                    this.newslettersList$.next(Object.assign({}, this.dataStore).newsletters);
                }
                this.apiInProgress = false;
            }, err => {
                this.apiInProgress = false;
            })
    }

    getNewsletter(id: string, fn?: Function) {
        this.api.get(`Newsletters/${id}`)
            .map(res => res.json())
            .subscribe(res => {
                if (res.IsSuccess) {
                    this.currentNewsletter$.next(res.Data);
                    if (fn) fn(res.Data);
                }
            })
    }

    addNewsletter(newsletter: NewsletterModel, fn?: Function) {
        this.api.post('Newsletters', newsletter)
            .map(res => res.json())
            .subscribe(res => {
                if (res.IsSuccess) {
                    this.dataStore.newsletters.push(res.Data);
                    this.newslettersList$.next(Object.assign({}, this.dataStore).newsletters);
                    this.notify.success(this.translate.instant('CREATENEWSLETTERSUCCESS'));
                    if (fn) fn();
                } else {
                    this.notify.error(this.translate.instant('CREATENEWSLETTERFAIL'));
                }
            }, err => {
                this.notify.error(this.translate.instant('CREATENEWSLETTERFAIL'));
            });
    }

    updateNewsletter(newsletter: NewsletterModel, fn?: Function) {
        this.api.put(`Newsletters/${newsletter.Id}`, newsletter)
            .map(res => res.json())
            .subscribe(res => {
                if (res.IsSuccess) {
                    this.dataStore.newsletters = this.dataStore.newsletters.map(x => x.Id === newsletter.Id ? res.Data : x);
                    this.newslettersList$.next(Object.assign({}, this.dataStore).newsletters);
                    this.currentNewsletter$.next(res.Data);
                    this.notify.success(this.translate.instant('UPDATENEWSLETTERSUCCESS'));
                    if (fn) fn();
                } else {
                    this.notify.error(this.translate.instant('UPDATENEWSLETTERFAIL'));
                }
            }, err => {
                this.notify.error(this.translate.instant('UPDATENEWSLETTERFAIL'));
            })
    }

    deleteNewsletter(id: string, success?: Function, fail?: Function) {
        this.api.delete('Newsletters/' + id)
            .map(res => res.json())
            .subscribe(res => {
                if (res.IsSuccess) {
                    this.notify.success(this.translate.instant('DELETENEWSLETTERSUCCESS'));
                    this.dataStore.newsletters = this.dataStore.newsletters.filter(news => news.Id !== id);
                    this.newslettersList$.next(this.dataStore.newsletters);
                    if (success) success();
                } else {
                    this.notify.error(this.translate.instant('DELETENEWSLETTERFAIL'));
                    if (fail) fail();
                }
            }, err => {
                this.notify.error(this.translate.instant('DELETENEWSLETTERFAIL'));
                if (fail) fail();
            });
    }

    setCurrentNewsletter(newsletter: NewsletterModel) {
        this.currentNewsletter$.next(newsletter);
    }

    //MAIL LISTS
    getMailLists() {
        this.api.get('Organisations/MailLists')
            .map(res => res.json())
            .subscribe(res => {
                if (res.IsSuccess) {
                    this.dataStore.mailLists = res.Data.filter((x: any) => x.Active);
                    this.maillistList$.next(this.dataStore.mailLists);
                }
            });
    }

    createMailList(model: any, success?: Function, fail?: Function) {
        this.api.post('Organisations/MailList', model)
            .map(res => res.json())
            .subscribe(res => {
                if (res.IsSuccess) {
                    this.dataStore.mailLists.push(res.Data);
                    this.maillistList$.next(this.dataStore.mailLists);
                    this.notify.success(this.translate.instant('CREATEMAILLISTSUCCESS'));
                    if (this.currentNewsletter$.value) { // if editing newsletter add it to its maillists
                        this.addMailListToCurrentNewsletter(res.Data);
                    }
                    if (success) success(res.Data);
                } else {
                    this.notify.error(this.translate.instant('CREATEMAILLISTFAIL'));
                    if (fail) fail();
                }
            }, err => {
                this.notify.error(this.translate.instant('CREATEMAILLISTFAIL'));
                if (fail) fail();
            })
    }

    updateMailList(model: any, success?: Function, fail?: Function) {
        this.api.put('Organisations/MailList/' + model.Id, model)
            .map(res => res.json())
            .subscribe(res => {
                if (res.IsSuccess) {
                    let index = this.dataStore.mailLists.findIndex(x => x.Id === res.Data.Id);
                    this.dataStore.mailLists.splice(index, 1, res.Data);
                    this.maillistList$.next(this.dataStore.mailLists);
                    this.notify.success(this.translate.instant('UPDATEMAILLISTSUCCESS'));
                    if (success) success(res.Data);
                } else {
                    this.notify.error(this.translate.instant('UPDATEMAILLISTFAIL'));
                    if (fail) fail();
                }
            }, err => {
                this.notify.error(this.translate.instant('UPDATEMAILLISTFAIL'));
                if (fail) fail();
            });
    }

    deleteMailList(id: string, success?: Function, fail?: Function) {
        this.api.delete('Organisations/MailList', id)
            .map(res => res.json())
            .subscribe(res => {
                if (res.IsSuccess) {
                    this.dataStore.mailLists = this.dataStore.mailLists.filter(x => x.Id !== id);
                    this.maillistList$.next(this.dataStore.mailLists);
                    this.notify.success(this.translate.instant('DELETEMAILLISTSUCCESS'));
                    if (success) success(res.Data);
                } else {
                    this.notify.error(this.translate.instant('DELETEMAILLISTFAIL'));
                    if (fail) fail();
                }
            }, err => {
                this.notify.error(this.translate.instant('DELETEMAILLISTFAIL'));
                if (fail) fail();
            });
    }

    getMailListCount(model: MailListModel): Observable<number> {
        return this.api.post('Organisations/MailList/Count', model)
            .map(res => {
                let resJson = res.json();
                if (resJson.IsSuccess) {
                    return resJson.Data;
                }
                return null;
            });
    }

    openAddEditMailList(mailList?: MailListModel) {
        this.currentMailList$.next(mailList || new MailListModel({}));
    }

    closeAddEditMailList() {
        this.currentMailList$.next(null);
    }

    addMailListToCurrentNewsletter(mailList: MailListModel) {
        this.currentNewsletter$.value.MailLists.push(mailList.Id);
        this.currentNewsletter$.next(this.currentNewsletter$.value)
    }
    //

    // openAddEditNewsletter(newsletter?: NewsletterModel){
    //     if (newsletter){
    //         this.currentNewsletter$.next(newsletter);
    //     } else {
    //         this.currentNewsletter$.next(new NewsletterModel({}));
    //     }
    // }

    // closeAddEditNewsletter(){
    //     this.currentNewsletter$.next(null);
    // }

    // searchNewsletters(val: string) {
    //     this.searchNewsletters$.next(val);
    // }

    resetSearch() {
        this.searchParams = { Page: 0, PageSize: 20 };
    }

    getSearchString() {
        var str: Array<string> = [];
        for (let p in this.searchParams) {
            if (this.searchParams.hasOwnProperty(p)) {
                if (typeof (this.searchParams[p]) == "object") {
                    for (let i in this.searchParams[p]) {
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(this.searchParams[p][i]));
                    }
                }
                else {
                    if (this.searchParams[p] === '' || this.searchParams[p] === 'All' || typeof this.searchParams[p] === 'undefined')
                        continue;
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(this.searchParams[p]));
                }
            }
        }
        return str.join("&");
    }

    onScroll() {
        if (this.apiInProgress === true) return;
        this.apiInProgress = true;
        this.searchParams.Page += 1;
        this.getNewsletters(true);
    }
}