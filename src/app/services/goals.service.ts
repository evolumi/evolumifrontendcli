import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { TranslateService } from '@ngx-translate/core';
import { LocalStorage } from './localstorage';
import { Api } from './api';
import { NotificationService } from './notification-service';

import { GoalsModel, GoalData } from '../models//goals.models';
import { ChosenOptionModel } from "../modules/shared/components/chosen/chosen.models";


@Injectable()

export class GoalsService {

    private goal$: BehaviorSubject<GoalsModel> = new BehaviorSubject(null);
    public goalObservable(): Observable<GoalsModel> {
        return this.goal$.asObservable();
    }

    private goalList$: BehaviorSubject<GoalsModel[]> = new BehaviorSubject(null);
    public goalListObservable(): Observable<GoalsModel[]> {
        return this.goalList$.asObservable();
    }

    constructor(private translateService: TranslateService, private api: Api, private localStorage: LocalStorage, private notify: NotificationService) {

    }

    getGoal(goalId: string) {
        this.api.get(`Goals/${goalId}`)
            .map(res => res.json())
            .subscribe(res => {
                if (res.IsSuccess) {
                    this.goal$.next(res.Data);
                }
            })
    }

    getGoals() {
        const orgId = this.localStorage.get('orgId');
        this.api.get(`Organisations/${orgId}/Goals`)
            .map(res => res.json())
            .subscribe(res => {
                if (res.IsSuccess) {
                    this.goalList$.next(res.Data);
                }
            });
    }

    getGoalDataLatest(goalId: string, timestamp: number) {
        return this.api.get(`Goals/GoalDataLatest/${goalId}/${timestamp}`)
            .map(res => {
                const response = res.json();
                if (response.IsSuccess) {
                    return response.Data.map((model: any) => new GoalData(model.Id, model.UserId, model.RelatedGoalId, model.GoalValue, new Date(model.Timestamp).getTime(), model.IsActive, model.Name));
                }
            });
    }

    createGoal(goal: GoalsModel) {
        const orgId = this.localStorage.get('orgId');
        return this.api.post(`Organisations/${orgId}/Goals`, goal)
            .map(res => {
                const resJson = res.json()
                if (resJson.IsSuccess) {
                    this.goal$.next(resJson.Data);
                    this.getGoals();
                    return true;
                } else {
                    return false;
                }
            });
    }

    createGoalData(goalData: GoalData) {
        var orgId = this.localStorage.get('orgId');
        return this.api.post(`Organisations/${orgId}/GoalData`, goalData)
            .map(res => res.json());
    }

    updateGoalData(goalData: GoalData) {
        return this.api.put('Goals/UpdateGoalData/' + goalData.Id, goalData)
            .map(res => res.json());
    }

    deleteGoal(id: string): any {
        return this.api.delete('Goals/Delete/' + id)
            .map(res => res.json())
            .subscribe(res => {
                if (res.IsSuccess) {
                    this.getGoals();
                    this.notify.success(this.translateService.instant('GOALSDELETESUCCESS'));
                } else {
                    this.notify.success(this.translateService.instant('GOALSDELETEFAIL'));
                }
            })
    }

    getGoalTypes() {
        return [
            { 'value': 'CreatedActions', 'label': this.translateService.instant('GOALSCREATEDACTIONS') },
            { 'value': 'ClosedActions', 'label': this.translateService.instant('GOALSCLOSEDACTIONS') },
            { 'value': 'DealsValueOf', 'label': this.translateService.instant('GOALSDEALSVALUEOF') },
            { 'value': 'CreatedDeals', 'label': this.translateService.instant('GOALSCREATEDDEALS') },
            { 'value': 'NumberOfDeals', 'label': this.translateService.instant('GOALSNUMBEROFDEALS') },
        ];
    }

    getTimeIntervals() {
        return [
            new ChosenOptionModel('Daily', this.translateService.instant('DAY')),
            new ChosenOptionModel('Weekly', this.translateService.instant('WEEK')),
            new ChosenOptionModel('Monthly', this.translateService.instant('MONTH')),
            new ChosenOptionModel('Quarterly', this.translateService.instant('QUARTER')),
            new ChosenOptionModel('Yearly', this.translateService.instant('YEAR'))
        ];
    }

    getLabels() {
        var orgId = this.localStorage.get('orgId');
        return this.api.get(`Organisations/${orgId}/Labels`)
            .map(res => {
                const response = res.json();
                if (response.IsSuccess) {
                    return response.Data.map((value: any) => new ChosenOptionModel(value, value));
                }
            });
    }

    changeUserCount(goalId: string, count: number) {
        let goal = this.goalList$.value.find(x => x.Id === goalId);
        if (goal) goal.UserCount = count;
    }

    openAddModal() {
        this.goal$.next(new GoalsModel({}));
    }

    closeModal() {
        this.goal$.next(null);
    }
}