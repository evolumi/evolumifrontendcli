import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from "rxjs/Subject";
import { Subscription } from "rxjs/Subscription";
import { Api } from './api';
import { ActionsModel } from '../models/actions.models';
import { LocalStorage } from './localstorage';
import { ActionslistCollection } from './ActionList.service';
import { ActionsflowCollection } from './Actionsflow.service';
import { NotificationService } from './notification-service';
import { FilterCollection } from './filterservice';
import { IntercomService } from './interecomService';
import { TranslateService } from '@ngx-translate/core';
import { ChosenOptionModel } from "../modules/shared/components/chosen/chosen.models";
import { ExportModel } from "../models/export.models";
import { ExportService } from "./export.service";
import { DealsModel } from "../models/deals.models";

export interface searchParams {
	SearchString: string,
	CreatorId: string,
	AssignedId: string,
	Types: any,
	Priority: string,
	Status: string,
	Labels: any,
	ContactId: string,
	DealId: string,
	StartDate: string,
	EndDate: string,
	Page: number,
	PageSize: number

	[key: string]: any
}

@Injectable()

export class ActionsCollection {
	public orgId: string;
	public searchParams: searchParams;
	public queryString: string;

	public selectedGroup: ActionGroup;

	actions: Array<ActionsModel> = new Array<ActionsModel>();
	actionsByAccountId: Array<ActionsModel> = new Array<ActionsModel>();
	actionsByContact: Array<ActionsModel> = new Array<ActionsModel>();

	private actionTypeIcons: any = {};

	private typesLoaded: boolean = false;

	private actionChanged$: Subject<ActionsModel> = new Subject;
	public actionChangeObservable(): Observable<ActionsModel> {
		return this.actionChanged$.asObservable();
	}

	private actionGroupChanged$: Subject<ActionGroup> = new Subject;
	public actionGroupChangeObservable(): Observable<ActionGroup> {
		return this.actionGroupChanged$.asObservable();
	}

	private actionListChangeSub: Subscription;

	constructor(
		public _http: Api, public _localstore: LocalStorage,
		public _actionList: ActionslistCollection,
		public actionFlow: ActionsflowCollection, public _notify: NotificationService,
		public _filter: FilterCollection,
		private translate: TranslateService,
		private exportSvc: ExportService,
		public intercomService: IntercomService) {
		this.searchParams = {
			SearchString: '', CreatorId: '', AssignedId: '', Types: '', Priority: '', Status: '', Labels: '', ContactId: '', DealId: ''
			, StartDate: '', EndDate: '', Page: 1, PageSize: 20
		};

		this.actionListChangeSub = this._actionList.actionChangeObservable().subscribe(res => {
			console.log("[Action-Service] Change detected in ActionList");
			this.actionsChange();
		})

		this.actionTypeIcons = ActionTypes.reduce((res: any, curr: ActionType) => {
			res[curr.value] = curr.icon;
			return res;
		}, {});
	}

	public getActionsForDeal(dealId: string, callback: (success: boolean, data: ActionsModel[]) => any) {
		this._http.get("Actions/ForDeal/" + dealId).map(res => res.json()).subscribe(data => {
			callback(data.IsSuccess, data.Data.map((x: any) => new ActionsModel(x)));
		})
	}

	public getGroups(filter: GroupFilter) {
		let query = this.getGroupQuery(filter);
		return this._http.get("Actions/Groups" + query).map(res => res.json());
	}

	public getGroupList(accountId: string): Observable<Array<ChosenOptionModel>> {
		let query = "?AccountId=" + accountId;
		return this._http.get("Actions/Groups/List" + query).map(res => {
			let result = res.json();
			if (result.IsSuccess) {
				console.log("[ActionService] Result", result.Data);
				let list: ChosenOptionModel[] = [];
				result.Data.forEach((group: any) => {
					list.push(new ChosenOptionModel(group.Id, group.Name));
				});
				console.log("[ActionService] List", list);
				return list;
			}
			else {
				console.log("[ActionService] Problem getting ActionGroups for account: " + accountId, result.Message);
				return [];
			}
		});
	}


	public getGroup(id: string) {
		return this._http.get("Actions/Groups/" + id).map(res => res.json());
	}

	public getGroupByActionId(id: string) {
		return this._http.get("Actions/" + id + "/Group").map(res => res.json());
	}

	// Creates a new Group with optional new action. (opens modal)
	public newGroup(group: ActionGroup = null, newAction: ActionsModel = null) {
		let newGroup = group || new ActionGroup();
		if (newAction)
			newGroup.Actions.push(newAction);

		this.selectedGroup = newGroup;
	}

	// Gets and sets SelectedGroup with optional new action (opens up modal).
	public showGroup(id: string, actionId: string = null, newAction: ActionsModel = null) {
		this.getGroup(id).subscribe(res => {
			if (res.IsSuccess) {
				if (newAction) {
					res.Data.Actions.push(newAction);
				}
				res.Data.editActionId = actionId;
				this.selectedGroup = res.Data;
			}
		})
	}

	// Get action group from actionId with optional new action (opens up modal)
	public showGroupByActionId(actionId: string, newAction: ActionsModel = null) {
		this.getGroupByActionId(actionId).subscribe(res => {
			if (res.IsSuccess) {
				if (newAction) {
					res.Data.Actions.push(newAction)
				}
				res.Data.editActionId = actionId;
				this.selectedGroup = res.Data;
			}
		});
	}

	deleteGroup(id: string) {
		return this._http.delete("Actions/Groups/" + id).map(res => res.json());
	}

	getGroupQuery(filter: GroupFilter): string {
		let query = "?";
		if (filter.Open != undefined) {
			query += `&Open=${filter.Open}&`
		}

		for (let key in filter) {
			let value = (<any>filter)[key];
			if (key != "Open" && value) {
				query += `${key}=${value}&`;
			}
		}
		query = query.slice(0, -1);

		return query;
	}


	reset() {
		this.searchParams = {
			SearchString: '', CreatorId: '', AssignedId: '', Types: '', Priority: '', Status: '', Labels: '', ContactId: '', DealId: ''
			, StartDate: '', EndDate: '', Page: 1, PageSize: 20
		};
	}
	syncActionToCalendar(action: ActionsModel) {
		this._http.post("SyncEvent", action).map(data => data.json()).subscribe(data => console.log(data));
	}

	// Let stuff know that a action has been changed. 
	public triggerActionUpdate(id: string) {
		console.log("[ActionService] Updating action: " + id + " in services");
		if (id) {
			this.getActionById(id).subscribe(res => {

				if (res.IsSuccess) {
					let action = res.Data;
					console.log(action);
					this.updateActionInServices(action);
					this.actionChanged$.next(action);
				}
			})
		}
		else {
			this.actionChanged$.next();
			this.updateActionInServices(undefined);
		}
	}

	public actionsChange(action: ActionsModel = null) {
		if (action) {
			this.updateActionInServices(action);
			this.actionChanged$.next(action);
		} else
			this.triggerActionUpdate("");
	}

	public actionGroupChanged(group: ActionGroup = null) {
		this.actionGroupChanged$.next(group);
	}

	// This will try to update loaded Action lists with new data. 
	private updateActionInServices(action: ActionsModel) {

		if (action) {
			// Update or add action to local ActionByAccountList.
			let a1 = this.actionsByAccountId.find(x => x.Id == action.Id);
			if (a1) {
				Object.assign(a1, action);
			}
			else if (this.actionsByAccountId.length && this.actionsByAccountId[0].AccountId == action.AccountId) {
				this.actionsByAccountId.push(action);
			}

			// Update Local Action list. If action does not exist in list. Reaload it.
			let a3 = this.actions.find(x => x.Id == action.Id);
			if (a3) {
				Object.assign(a3, action)
			}
			else {
				this.getActions();
			}

			// If action is currently in actionFlow list. Update it. Does not reaload it to prevent disruption in workflow.
			let a2 = this.actionFlow.Actionsflow.find(x => x.Id == action.Id);
			if (a2) Object.assign(a2, action);

		} else {
			this.actionFlow.getActions();
		}

		// Realoads actionList.
		this._actionList.getActions();

		// TEMPORARY. Trigger the actionChange in filterColletcion. remove when all dependencies of this is gone.
		this._filter.changeAction();
	}

	getSearchString() {
		var str: Array<any> = [];
		for (var p in this.searchParams)
			if (this.searchParams.hasOwnProperty(p)) {
				if (this.searchParams[p] === '' || typeof this.searchParams[p] === 'undefined')
					continue;
				str.push(encodeURIComponent(p) + "=" + encodeURIComponent(this.searchParams[p]));
			}
		return str.join("&");

	}

	createAction(action: ActionsModel) {
		this.orgId = this._localstore.get('orgId');
		return this._http.post('Organisations/' + this.orgId + '/Actions', action)
			.map(res => {
				let response = res.json();
				if (response.IsSuccess) {

					this._filter.changeAction();
					this.actionsChange(response.Data);

					// Update the connected ActionGroup
					if (this.selectedGroup && response.Data.ActionGroupId == this.selectedGroup.Id) {
						this.selectedGroup.Actions.push(response.Data);
						this.actionGroupChanged$.next(this.selectedGroup);
					}
					else {
						this.getGroup(response.Data.ActionGroupId).subscribe(groupResponse => {
							if (groupResponse.IsSuccess) {
								this.actionGroupChanged$.next(groupResponse.Data);
							}
						})
					}
					return response;
				}
			});
	}

	updateGroupLocaly(group: ActionGroup) {
		this.actionGroupChanged$.next(group);
	}

	updateGroup(group: ActionGroup) {

		return this._http.put("Actions/Groups/" + group.Id, group).map(res => {
			let response = res.json()
			if (response.IsSuccess) {
				this.actionGroupChanged$.next(response.Data);
				if (this.selectedGroup && response.Data.Id == this.selectedGroup.Id)
					this.selectedGroup == response.Data;
			}
			return response;
		});
	}

	createManyAction(action: any) {
		this.orgId = this._localstore.get('orgId');
		console.log("Create Many Started!", action);
		return this._http.post('Organisations/' + this.orgId + '/Actions/CreateMany', action)
			.map(res => {
				this._filter.changeAction();
				this.actionsChange();
				return res.json();
			});
	}

	updateAction(action: ActionsModel) {
		this.orgId = this._localstore.get('orgId');
		return this._http.put('/Actions/' + action.Id, action)
			.map(res => {
				this._filter.changeAction();
				this.actionsChange();
				return res.json()
			});
	}

	getActions() {
		var orgId = this._localstore.get('orgId');
		var query = this.getSearchString();
		this._http.get(`Organisations/${orgId}/Actions?${query}`)
			.map(res => res.json())
			.subscribe(res => {
				this.actions = []
				for (var feed of res.Data) {
					this.actions.push(new ActionsModel(feed));
				}
			});
	}

	UpdateSearchFields(updateArray: any) {
		for (var arr in updateArray) {
			(updateArray[arr] === 'All Activity') ? this.searchParams[arr] = '' : this.searchParams[arr] = updateArray[arr];
		}
		this.getActions();
	}

	getActionByAccountId(accountId: string) {
		this._http.get(`Accounts/${accountId}/Actions`)
			.map(res => res.json())
			.subscribe(res => {
				console.log("[Actions] Loaded actions by accountID[" + accountId + "] -> Found " + res.length + " of them");
				this.actionsByAccountId = []
				for (var feed of res.Data) {
					this.actionsByAccountId.push(new ActionsModel(feed));
				}
			});
	}

	getActionByContact(accountId: string, contactId: string) {
		this._http.get(`Accounts/${accountId}/Actions?Status=0&ContactId=` + contactId)
			.map(res => res.json())
			.subscribe(res => {
				this.actionsByContact = []
				for (var feed of res.Data) {
					this.actionsByContact.push(new ActionsModel(feed));;
				}
			});
	}
	// delete action
	deleteAction(accId: string, callback: (success: any) => any = null) {
		this._http.delete('Actions/' + accId)
			.map(res => res.json())
			.subscribe(body => {
				if (body.IsSuccess === true) {
					this._notify.success('Action Deleted');
					let ind = this.actionsByAccountId.findIndex(x => x.Id === accId);
					(<any>this.actionsByAccountId).splice(ind, 1);

					let ind2 = this._actionList.Actionslist.findIndex(x => x.Id === accId);
					(<any>this._actionList.Actionslist).splice(ind2, 1);

					let ind3 = this.actionFlow.Actionsflow.findIndex(x => x.Id === accId);
					(<any>this.actionFlow.Actionsflow).splice(ind3, 1);
					this._filter.changeAction();
					this.actionsChange();
					if (callback) callback(body.IsSuccess);
				} else {
					this._notify.success('Deletion Deleted');
				}
			}, err => {
				console.log('error 77');
			});
	}

	updateStatusForFlow(actionId: string, status: any) {

		return this._http.put('Actions/' + actionId + '/Status', status)
			.map(res => res.json())

	}

	// update action status
	updateActionStatus(actionId: string, status: any) {
		this._http.put('Actions/' + actionId + '/Status', status)
			.map(res => res.json())
			.subscribe(res => {
				if (res.IsSuccess === true) {
					let ind = this.actionsByAccountId.findIndex(x => x.Id === actionId);

					if (ind != -1) {
						status = (status === true) ? 'Closed' : 'Open';
						if (status === true) {
							this.intercomService.trackEvent('action-closed');
						}
						this.actionsByAccountId[ind].Status = status;
						this._notify.success('Action status updated');
					}
					let ind2 = this.actionFlow.Actionsflow.findIndex(x => x.Id === actionId);

					if (ind2 != -1) this.actionFlow.Actionsflow.splice(ind2, 1);
					this._actionList.getActions();
					this.actionsChange();
					this._filter.changeAction();
				}
			}, err => {
				// this._notify.error('Error Occured during action status updation');
			});

	}
	getActionById(id: string) {
		return this._http.get(`/Actions/${id}`)
			.map(res => res.json());

	}

	updateComment(action: any, comment: string): Observable<boolean> {
		console.log("[Action] Updating comment");
		return this._http.put('Actions/' + action.Id + '/Comment', comment)
			.map(res => {
				let response = res.json()
				if (response.IsSuccess) {
					this._filter.changeAction();
					return true;
				} else {
					this._notify.error(response.Message);
					return false;
				}
			});
	}
	setSearchParams(value: any) {
		for (var key in value) {
			this.searchParams[key] = value[key];
		}
		this.getActions();
	}

	exportNew(model: ExportModel, type: string, success?: Function, fail?: Function) {
		let query: string;
		switch (type) {
			case 'ActionFlow':
				query = this.actionFlow.getSearchString();
				break;
			case 'ActionList':
				if (this._actionList.selectedActions.length) {
					this._actionList.searchParams.ActionIds = this._actionList.selectedActions;
				}
				query = this._actionList.getSearchString();
				break;
			case 'Action':
				query = this.getSearchString();
		}
		const path = `Actions/Export?${query}`;
		let date = new Date();
		const fileName = `Action_export_${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}.xlsx`;
		const contentType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
		this.exportSvc.downloadExportNew(path, fileName, contentType, model, success, fail);
	}

	getCount(type: string) {
		let query: string;
		switch (type) {
			case 'ActionFlow':
				query = this.actionFlow.getSearchString();
				break;
			case 'ActionList':
				if (this._actionList.selectedActions.length) {
					this._actionList.searchParams.ActionIds = this._actionList.selectedActions;
				}
				query = this._actionList.getSearchString();
				break;
			case 'Action':
				query = this.getSearchString();
		}
		return this._http.get('Actions/Count?' + query)
			.map(res => res.json());
	}

	getActionsForAccountAndRelated(accountId: string, callback: (success: boolean, data: ActionsModel[]) => any) {
		this._http.get("Actions/ActionsForAccountAndRelated/" + accountId).map(res => res.json()).subscribe(data => {
			callback(data.IsSuccess, data.Data);
		});
	}

	public getActionTypelabel(type: string) {
		return this.translate.instant(ActionTypes.find(x => x.value == type).label);
	}

	public getActionTypes(): Array<ActionType> {
		return ActionTypes.filter(x => x.active).map(x => { x.label = this.translate.instant(x.label); return x; });
	}

	public getActionTypesAndStatuses(onlyClosed: boolean = false): Array<ActionType> {
		return ActionTypes.filter(x => x.active).map(x => { x.label = this.translate.instant(x.label); x.statuses = x.statuses.filter(y => !onlyClosed || y.closed).map(y => { y.label = this.translate.instant(y.label); return y; }); return x; });
	}

	getStatusLabel(status: string): string {
		let st = ActionStatuses.find(x => x.value == status);
		return st ? st.label : "";
	}

	getTypeLabel(type: string): string {
		let ty = ActionTypes.find(x => x.value == type)
		return ty ? ty.label : "";
	}

	public getActionStatuses(): Array<ActionStatus> {
		return ActionStatuses.map(x => { x.label = this.translate.instant(x.label); return x; });
	}

	public getActionPriorities(): Array<ActionPriority> {
		return ActionPriorities.map(x => { x.label = this.translate.instant(x.label); return x; });
	}

	public getActionEvents(): Array<ActionEvent> {
		return ActionEvents.map(x => { x.label = this.translate.instant(x.label); return x; });
	}

	ngOnDestroy() {
		if (this.actionListChangeSub) this.actionListChangeSub.unsubscribe();
	}
}

export class ActionGroup {
	Open: boolean;
	Id: string;
	Actions: Array<ActionsModel> = [];
	Name: string;
	Organisation: string;
	AccountId: string;
	DealId?: string;
	DealName: string;
	AccountName: string;
	NextAction: ActionsModel;
	PreviousAction: ActionsModel;

	editActionId: string;
	// Those fields are used for creating Actions/Groups for multiple accounts.
	multi: boolean = false;
	accountIds: Array<string>;
	accountQuery: any;
	actionIds: Array<string>;
	actionQuery: any;
	[key: string]: any;
	constructor(group: any = null) {
		for (let key in group) {
			if (group.hasOwnProperty(key)) {
				this[key] = group[key]
			}
		}
		if (group && group.Actions) {
			this.NextAction = group.Actions.filter((x: any) => x.Status == "Open").sort((a: ActionsModel, b: ActionsModel) => {
				return a.StartDate - b.StartDate;
			})[0];

			this.PreviousAction = group.Actions.filter((x: any) => x.Status != "Open").sort((a: ActionsModel, b: ActionsModel) => {
				return b.DateClosed - a.DateClosed;
			})[0];
		}
	}
}

export interface GroupFilter {
	Open?: boolean;
	AccountId?: string;
	DealId?: string;
	Name?: string;
	Page: number;
	PageSize: number;
}

export abstract class EnumName {
	value: string;
	label: string;

	constructor(value: string, label: string) {
		this.value = value;
		this.label = label;
	}
}

export class ActionType extends EnumName {

	icon: string;
	statuses: ActionStatus[] = [];
	active: boolean;

	constructor(value: string, label: string, icon: string, statuses: ActionStatus[], active: boolean = true) {
		super(value, label);
		this.icon = icon;
		this.statuses = statuses;
		this.active = active;
	}
}

export class ActionStatus extends EnumName {
	value: string;
	label: string;
	closed: boolean;

	constructor(value: string, label: string, closed: boolean = true) {
		super(value, label);
		this.closed = closed
	}
}

export class ActionPriority extends EnumName {
	value: string;
	label: string;

	constructor(value: string, label: string) {
		super(value, label);
	}
}

export class ActionEvent extends EnumName {
	constructor(value: string, label: string) {
		super(value, label);
	}
}

export const ActionStatuses: Array<ActionStatus> = [
	new ActionStatus("Open", "ACTIONPLANNED", false),		// 0
	new ActionStatus("Closed", "CLOSED"),			// 1
	new ActionStatus("Recived", "DELIVERED"),		// 2
	new ActionStatus("NotRecived", "NOTRECIVED"),	// 3
	new ActionStatus("NoAnswer", "NOANSWER"),		// 4
	new ActionStatus("Answered", "ANSWER"),			// 5
	new ActionStatus("WrongNumber", "WRONGNUMBER"),	// 6
	new ActionStatus("Busy", "BUSY"),				// 7
	new ActionStatus("Canceled", "CANCELED"),		// 8
	new ActionStatus("Sent", "SENT"),				// 9
	new ActionStatus("Postponed", "POSTPONED"),		// 10
	new ActionStatus("Incoming", "INCOMING"),		// 11
	new ActionStatus("Scheduled", "SCHEDULED")		// 12
];

export const ActionTypes: Array<ActionType> = [
	new ActionType("Email", "ACTIONTYPEEMAIL", "envelope-o", [ActionStatuses[0], ActionStatuses[9], ActionStatuses[11]]),
	new ActionType("Phone", "ACTIONTYPEPHONECALL", "phone", [ActionStatuses[0], ActionStatuses[4], ActionStatuses[5], ActionStatuses[6], ActionStatuses[7], ActionStatuses[11]]),
	new ActionType("Meeting", "ACTIONTYPEMEETING", "handshake-o", [ActionStatuses[0], ActionStatuses[1], ActionStatuses[8], ActionStatuses[10]]),
	new ActionType("Events", "ACTIONTYPEEVENT", "calendar-o", [ActionStatuses[0], ActionStatuses[1], ActionStatuses[8], ActionStatuses[10]]),
	new ActionType("Other", "ACTIONTYPEOTHER", "tree", [ActionStatuses[0], ActionStatuses[1], ActionStatuses[8], ActionStatuses[10]]),
	new ActionType("NewsLetter", "ACTIONTYPENEWS", "envelope-o", [ActionStatuses[0], ActionStatuses[9], ActionStatuses[11]], false),
];

export const ActionPriorities: Array<ActionPriority> = [
	new ActionPriority("High", "PRIORITYHIGH"),
	new ActionPriority("Normal", "PRIORITYNORMAL"),
	new ActionPriority("Low", "PRIORITYLOW")
]

export const ActionEvents: Array<EnumName> = [
	new ActionEvent("Created", "CREATED"),
	new ActionEvent("Sent", "SENT"),
	new ActionEvent("NewStatus", "EVENTNEWSTATUS"),
	new ActionEvent("Recived", "RECIVED"),
	new ActionEvent("Bounce", "EVENTBOUNCE"),
	new ActionEvent("Spam", "EVENTSPAM"),
	new ActionEvent("Dropped", "EVENTDROPPED"),
	new ActionEvent("Opened", "EVENTOPENED"),
	new ActionEvent("Clicked", "EVENTCLICKED"),
	new ActionEvent("Note", "EVENTNOTE"),
	new ActionEvent("CallStart", "EVENTCALLSTART"),
	new ActionEvent("Answered", "EVENTANSWERED"),
	new ActionEvent("Hangup", "EVENTHANGUP"),
	new ActionEvent("NoAnswer", "EVENTNOANSWER"),
	new ActionEvent("Closed", "CLOSED"),
	new ActionEvent("ReOpened", "EVENTREOPENED"),
]
