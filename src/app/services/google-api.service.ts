import { Injectable } from '@angular/core';

const API_KEY = 'AIzaSyC25Th1XFvWxUaVV1K5rG5PsF5vtV2aGTk';
const mapUrl = `https://maps.googleapis.com/maps/api/js?key=${API_KEY}&callback=initMap`;
const markerUrl = `https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js` // Not sure if needed

@Injectable()
export class GoogleApiService {
  private loadMap: Promise<any>;

  constructor() {
    this.loadMap = new Promise((resolve) => {
      (<any>window)['initMap'] = () => {
        resolve();
      };
      this.loadMapScript();
      this.loadMarkerScript();
    });
  }

  public initMap(): Promise<any> {
    return this.loadMap;
  }

  private loadMapScript() {
    let mapScript = document.createElement('script');
    mapScript.src = mapUrl;
    mapScript.type = 'text/javascript';

    if (document.body.contains(mapScript)) {
      return;
    }
    document.getElementsByTagName('head')[0].appendChild(mapScript);
  }

  private loadMarkerScript() {
    let markerScript = document.createElement('script');
    markerScript.src = markerUrl;
    markerScript.type = 'text/javascript';

    if (document.body.contains(markerScript)) {
      return;
    }
    document.getElementsByTagName('head')[0].appendChild(markerScript);
  }
}
