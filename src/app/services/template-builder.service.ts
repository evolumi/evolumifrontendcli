import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { TemplateModel, TemplateBlockModel, TemplateRowModel, TemplateColumnModel } from '../models/template.models';
import { Observable } from 'rxjs/observable';
import { Api } from './api';
import { NotificationService } from './notification-service';
import { TranslateService } from '@ngx-translate/core';
import { FileModel } from "../models/file.models";

interface SearchParams {
    Name: string;
    Type: string;
    SortingFieldName: string;
    Page: number;
    PageSize: number;

    [key: string]: any;
}

@Injectable()
export class TemplateBuilderService {

    private dataStore: {
        templates: Array<TemplateModel>
    }

    private searchParams: SearchParams;

    private currentTemplate$ = new BehaviorSubject<TemplateModel>(null);
    public currentTemplateObs(): Observable<TemplateModel> {
        return this.currentTemplate$.asObservable();
    }

    private currentTemplateBlock$ = new BehaviorSubject<TemplateBlockModel>(null);
    public currentTemplateBlockObs(): Observable<TemplateBlockModel> {
        return this.currentTemplateBlock$.asObservable();
    }

    private currentTemplateRow$ = new BehaviorSubject<TemplateRowModel>(null);
    public currentTemplateRowObs(): Observable<TemplateRowModel> {
        return this.currentTemplateRow$.asObservable();
    }

    private currentTemplateCol$ = new BehaviorSubject<TemplateColumnModel>(null);
    public currentTemplateColObs(): Observable<TemplateColumnModel> {
        return this.currentTemplateCol$.asObservable();
    }

    private templateList$ = new BehaviorSubject<Array<TemplateModel>>(null);
    public templateListObs(type?: string): Observable<Array<TemplateModel>> {
        return this.templateList$.asObservable();
    }

    private mailTemplateList$ = new BehaviorSubject<Array<TemplateModel>>(null);
    public mailTemplateListObs(): Observable<Array<TemplateModel>> {
        return this.mailTemplateList$.asObservable();
    }

    // Special for knowing when a Redactor modal is open to not mak the deselect event trigger on click
    public redactorModalActive: boolean;

    // To store all link stylesheets and being able to clear all of them on destroy
    public styleElements: Array<any> = [];

    constructor(private api: Api,
        private notify: NotificationService,
        private translate: TranslateService) {
        this.resetSearch();
        this.dataStore = { templates: [] };
    }

    getTemplates(type?: string) {
        if (type) this.searchParams.Type = type;
        const query = this.getSearchString();
        this.api.get(`Templates?${query}`)
            .map(res => res.json())
            .subscribe(res => {
                if (res.IsSuccess) {
                    this.dataStore.templates = res.Data;
                    this.templateList$.next(Object.assign({}, this.dataStore).templates);
                    this.mailTemplateList$.next(Object.assign({}, this.dataStore).templates.filter(x => x.Type === 'Mail'));
                }
            })
    }

    getMailTemplates() {
        this.resetSearch();
        this.searchParams.Type == 'Mail';
        this.getTemplates();
    }

    getTemplate(id: string, fn?: Function) {
        this.api.get(`Templates/${id}`)
            .map(res => res.json())
            .subscribe(res => {
                if (res.IsSuccess) {
                    this.currentTemplate$.next(res.Data);
                    if (fn) fn();
                }
            })
    }

    addTemplate(template: TemplateModel, fn?: Function) {
        this.api.post('Templates', template)
            .map(res => res.json())
            .subscribe(res => {
                if (res.IsSuccess) {
                    this.dataStore.templates.push(res.Data);
                    this.currentTemplate$.next(res.Data);
                    this.templateList$.next(Object.assign({}, this.dataStore).templates);
                    this.mailTemplateList$.next(Object.assign({}, this.dataStore).templates.filter(x => x.Type === 'Mail'));
                    this.notify.success(this.translate.instant('CREATETEMPLATESUCCESS'))
                    if (fn) fn();
                } else {
                    this.notify.error(this.translate.instant('CREATETEMPLATEFAIL'));
                }
            }, err => {
                this.notify.error(this.translate.instant('CREATETEMPLATEFAIL'));
            })
    }

    updateTemplate(template: TemplateModel, fn?: Function) {
        this.api.put(`Templates/${template.Id}`, template)
            .map(res => res.json())
            .subscribe(res => {
                if (res.IsSuccess) {
                    this.dataStore.templates = this.dataStore.templates.map(x => x.Id === template.Id ? res.Data : x);
                    console.log('UPDATE', this.dataStore.templates);
                    this.templateList$.next(Object.assign({}, this.dataStore).templates);
                    this.mailTemplateList$.next(Object.assign({}, this.dataStore).templates.filter(x => x.Type === 'Mail'));
                    this.currentTemplate$.next(res.Data);
                    this.notify.success(this.translate.instant('UPDATETEMPLATESUCCESS'))
                    if (fn) fn();
                } else {
                    this.notify.error(this.translate.instant('UPDATETEMPLATEFAIL'));
                }
            }, err => {
                this.notify.error(this.translate.instant('UPDATETEMPLATEFAIL'));
            })
    }

    deleteTemplate(id: string) {
        this.api.delete(`Templates/${id}`)
            .map(res => res.json())
            .subscribe(res => {
                if (res.IsSuccess) {
                    this.dataStore.templates = this.dataStore.templates.filter(x => x.Id !== id);
                    this.templateList$.next(Object.assign({}, this.dataStore).templates);
                    this.mailTemplateList$.next(Object.assign({}, this.dataStore).templates.filter(x => x.Type === 'Mail'));
                    this.notify.success(this.translate.instant('DELETETEMPLATESUCCESS'));
                } else {
                    this.notify.error(this.translate.instant('DELETETEMPLATEFAIL'));
                }
            }, err => {
                this.notify.error(this.translate.instant('DELETETEMPLATEFAIL'));
            });
    }

    resetService(){
        this.setCurrentTemplate(null);
        this.setCurrentTemplateBlock(null);
        this.setCurrentTemplateColumn(null);
        this.setCurrentTemplateRow(null);
        this.templateList$.next(null);
    }

    setCurrentTemplate(template: TemplateModel) {
        this.currentTemplate$.next(template);
    }

    setCurrentTemplateBlock(block: TemplateBlockModel) {
        this.currentTemplateBlock$.next(block);
        this.currentTemplateRow$.next(null);
    }

    setCurrentTemplateRow(row: TemplateRowModel) {
        this.currentTemplateRow$.next(row);
        if (!row) {
            this.currentTemplateCol$.next(null);
        } else {
            this.currentTemplateCol$.next(row.Columns[0]);
        }
        this.currentTemplateBlock$.next(null);
    }

    setCurrentTemplateColumn(col: TemplateColumnModel) {
        this.currentTemplateCol$.next(col);
    }

    addTempFile(file: any) {
        let formData: FormData = new FormData();
        formData.append('file', file);
        return this.api.upload('Templates/UploadTempFile', formData)
    }

    addFile(file: any) {
        let formData: FormData = new FormData();
        formData.append('file', file);
        return this.api.upload('Templates/UploadFile', formData)
    }

    downloadTempFile(file: FileModel) {
        return this.api.postDownloadFile('Templates/DownloadTempFile', file);
    }

    deleteTempFile(id: string) {
        return this.api.delete('Templates/DeleteTempFile', id)
            .map(res => res.json());
    }

    deleteFile(id: string) {
        return this.api.delete('Templates/DeleteFile', id)
            .map(res => res.json());
    }

    adjustLinkElements(links: Array<HTMLElement>, linkStyles: any) {
        let newLinks: Array<any> = [];
        links.map(x => {
            Object.assign(x.style, linkStyles);
            let newNode = x.cloneNode(true);
            newNode.addEventListener('click', function (event: Event) {
                event.preventDefault();
            }, true)
            x.parentNode.replaceChild(newNode, x);
            newLinks.push(newNode);
        });
        return newLinks;
    }

    clearStyleSheets() {
        this.styleElements.map(x => {
            let head = document.getElementsByTagName('head')[0];
            if (head.contains(x)) {
                document.getElementsByTagName('head')[0].removeChild(x)
            }
        });
    }

    private resetSearch() {
        this.searchParams = { SortingFieldName: 'Name', Name: '', Type: '', Page: 0, PageSize: 30 };
    }

    private getSearchString() {
        var str: Array<string> = [];
        for (let p in this.searchParams) {
            if (this.searchParams.hasOwnProperty(p)) {
                if (typeof (this.searchParams[p]) == "object") {
                    for (let i in this.searchParams[p]) {
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(this.searchParams[p][i]));
                    }
                }
                else {
                    if (this.searchParams[p] === '' || this.searchParams[p] === 'All' || typeof this.searchParams[p] === 'undefined')
                        continue;
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(this.searchParams[p]));
                }
            }
        }
        return str.join("&");
    }
}