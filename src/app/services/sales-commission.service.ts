import { Injectable } from '@angular/core';
import { SalesCommission, SalesCommissionRule } from '../models/sales-commission.models';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { LocalStorage } from './localstorage';
import { Api } from './api';
import { NotificationService } from './notification-service';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from "rxjs/Observable";

class DataStore {
    salesCommissions: Array<SalesCommission> = [];
}

export interface SearchParams {
    SortingFieldName: string;
    Name: string;
    StartDate?: number;
    EndDate?: number;
    Active: boolean;
    Inactive: boolean;
    Page: number;
    PageSize: number;
    [key: string]: any;
}

@Injectable()
export class SalesCommissionService {

    private dataStore: DataStore = new DataStore();

    private currentSalesCommission$ = new BehaviorSubject<SalesCommission>(null);
    public currentSalesCommissionObs(): Observable<SalesCommission> {
        return this.currentSalesCommission$.asObservable();
    }

    private salesCommissionList$ = new BehaviorSubject<Array<SalesCommission>>(null);
    public salesCommissionListObs(): Observable<Array<SalesCommission>> {
        return this.salesCommissionList$.asObservable();
    }

    private currentRule$ = new BehaviorSubject<SalesCommissionRule>(null);
    public currentRuleObs() {
        return this.currentRule$.asObservable();
    }

    constructor(private storage: LocalStorage,
        private api: Api,
        private notify: NotificationService,
        private translate: TranslateService) { }

    getManySalesCommission() {
        const orgId = this.storage.get('orgId');
        this.api.get(`Organisations/${orgId}/SalesCommissions`)
            .map(res => res.json())
            .subscribe(res => {
                if (res.IsSuccess) {
                    this.dataStore.salesCommissions = [...res.Data];
                    this.salesCommissionList$.next(this.dataStore.salesCommissions);
                }
            }, err => {
                this.notify.error(this.translate.instant('GETSALESCOMMISSIONFAIL'));
            })
    }

    getUsersBySalesCommission(id: string) {
        return this.api.get(`SalesCommission/${id}/Users`)
            .map(res => res.json());
        // .subscribe(res => {
        //     if (res.IsSuccess) {
        //         this.salesCommissionList$.next(this.dataStore.salesCommissions);
        //         this.loader.endLoader();
        //     }
        // }, err => {
        //     this.loader.endLoader();
        //     this.notify.error(this.translate.instant('GETSALESCOMMISSIONFAIL'));
        // })
    }

    addSalesCommission(salesComm: SalesCommission, fn?: Function) {
        const orgId = this.storage.get('orgId');
        this.api.post(`Organisations/${orgId}/SalesCommission`, salesComm)
            .map(res => res.json())
            .subscribe(res => {
                if (res.IsSuccess) {
                    this.dataStore.salesCommissions.push(res.Data);
                    this.salesCommissionList$.next(Object.assign({}, this.dataStore).salesCommissions);
                    this.notify.success(this.translate.instant('CREATESALESCOMMISSIONSUCCESS'));
                    this.currentSalesCommission$.next(null);
                    if (fn) fn();
                } else {
                    this.notify.error(this.translate.instant('CREATESALESCOMMISSIONFAIL'));
                }
            }, err => {
                this.notify.error(this.translate.instant('CREATESALESCOMMISSIONFAIL'));
            })
    }

    updateSalesCommission(salesComm: SalesCommission, fn?: Function) {
        this.api.put(`SalesCommission/${salesComm.Id}`, salesComm)
            .map(res => res.json())
            .subscribe(res => {
                if (res.IsSuccess) {
                    const index = this.dataStore.salesCommissions.map(x => x.Id).indexOf(salesComm.Id);
                    this.dataStore.salesCommissions.splice(index, 1, salesComm);
                    this.salesCommissionList$.next(Object.assign({}, this.dataStore).salesCommissions);
                    this.currentSalesCommission$.next(null);
                    this.notify.success(this.translate.instant('UPDATESALESCOMMISSIONSUCCESS'))
                    if (fn) fn();
                } else {
                    this.notify.error(this.translate.instant('UPDATESALESCOMMISSIONFAIL'));
                }
            }, err => {
                this.notify.error(this.translate.instant('UPDATESALESCOMMISSIONFAIL'));
            })
    }

    deleteSalesCommission(id: string) {
        this.api.delete('SalesCommission/' + id)
            .map(res => res.json())
            .subscribe(res => {
                if (res.IsSuccess) {
                    this.notify.success('DELETESALESCOMMISSIONSUCCESS');
                    this.dataStore.salesCommissions = this.dataStore.salesCommissions.filter(obj => !new Set([id]).has(obj.Id));
                    this.salesCommissionList$.next(Object.assign({}, this.dataStore).salesCommissions);
                } else {
                    this.notify.error('DELETESALESCOMMISSIONFAIL');
                }
            },
            err => {
                this.notify.error('DELETESALESCOMMISSIONFAIL');
            });
    }

    openAddEdit(salesCommission?: SalesCommission) {
        if (salesCommission) {
            this.currentSalesCommission$.next(salesCommission)
        } else {
            this.currentSalesCommission$.next(new SalesCommission({}));
        }
    }

    closeAddEdit() {
        this.currentSalesCommission$.next(null);
    }

    openAddRule(rule?: SalesCommissionRule) {
        if (rule) {
            this.currentRule$.next(rule);
        } else {
            this.currentRule$.next(new SalesCommissionRule({}));
        }
    }

    closeAddRule() {
        this.currentRule$.next(null);
    }

    getValueTypes() {
        // return [
        //     { 'value': 'CreatedActions', 'label': this.translateService.instant('GOALSCREATEDACTIONS') },
        //     { 'value': 'ClosedActions', 'label': this.translateService.instant('GOALSCLOSEDACTIONS') },
        //     { 'value': 'DealsValueOf', 'label': this.translateService.instant('GOALSDEALSVALUEOF') },
        //     { 'value': 'CreatedDeals', 'label': this.translateService.instant('GOALSCREATEDDEALS') },
        //     { 'value': 'NumberOfDeals', 'label': this.translateService.instant('GOALSNUMBEROFDEALS') },
        // ];

    }
}