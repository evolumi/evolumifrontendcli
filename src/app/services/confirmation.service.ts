import { Injectable } from '@angular/core';
import { Subject } from "rxjs/Subject";
import { Observable } from "rxjs/Observable";

@Injectable()
export class ConfirmationService {
    public displayModal: boolean;
    public boxTitle: string;
    public boxMessage: string;
    private callback: () => any;

    private modalResponse$: Subject<boolean> = new Subject<boolean>();
    // return obs().first() to await response
    public modalResponseObs(): Observable<boolean> {
        return this.modalResponse$.asObservable();
    }

    constructor() { }

    public showModal(boxTitle: string, boxMessage: string, callback?: () => any) {
        this.boxTitle = boxTitle;
        this.boxMessage = boxMessage;
        this.callback = callback;
        this.displayModal = true;
    }

    public hideModal(success: boolean) {
        this.modalResponse$.next(success);
        if (success && this.callback) {
            this.callback();
        }
        this.displayModal = false;
    }
}