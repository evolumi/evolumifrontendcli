import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from "rxjs/Observable";

@Injectable()
export class DragAndDropService {

    private dragInProcess: string; // Only has value when drag is in process

    private dragArray$ = new Subject<Array<any>>();
    public dragArrayObs(): Observable<Array<any>> {
        return this.dragArray$.asObservable();
    }

    private onDrop$ = new Subject<any>();
    public onDropObs() {
        return this.onDrop$.asObservable();
    }
    // private onDragStart$ = new Subject();
    // public onDragStartObs() {
    //     return this.onDragStart$.asObservable();
    // }

    constructor() { }

    onStartDrag(drag: string, dragArray: Array<any>) {
        this.dragArray$.next(dragArray);
        this.dragInProcess = drag;
    }

    onEndDrag() {
        this.dragInProcess = '';
    }

    onDrop(obj: any) {
        this.onDrop$.next(obj);
    }

    checkIfSameDrag(drag: string) {
        return this.dragInProcess === drag;
    }
}