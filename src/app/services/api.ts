import { Injectable, EventEmitter } from '@angular/core';
import { Http, Headers, Response, RequestOptionsArgs, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { LoaderService } from './loader.service';
import { LocalStorage } from './localstorage';
import { Configuration } from './configuration';
import { NotificationService } from './notification-service';
import { FileModel } from '../models/file.models';
import { environment } from '../../environments/environment';

@Injectable()
export class Api {

    public apiUrl: string;
    public needRefreshPagination: EventEmitter<any> = new EventEmitter<any>();
    //creds = {};
    public options: RequestOptionsArgs = {};
    //uploadheaders = {};
    public bearertoken: string = "";
    constructor(
        private _http: Http,
        private _load: LoaderService,
        private _localStore: LocalStorage,
        private notify: NotificationService,
        private config: Configuration
    ) {
        this.apiUrl = environment.apiUrl + '/api/'
        this.setHeaders();
    }

    process(res: Observable<Response>): Observable<Response> {
        return res.map(res => {

            let version = res.headers.get("Evo-Version");
            let message = res.headers.get("Evo-Information-Message");
            if (version && Number(this.config.version) < Number(version)) {
                this.notify.urgent("UPDATEAVALIBLE", "RELOADEXPLAIN");
            }
            if (message) {
                let title = res.headers.get("Evo-Information-Title");
                let link = res.headers.get("Evo-Information-Link");
                let linkText = res.headers.get("Evo-Information-LinkText");
                this.notify.inform(title, message, linkText, link);
            }
            return res;
        }).finally(() => this._load.removeCounter());
    }

    setHeaders() {
        var userdetails = this._localStore.getObject('userDetails');
        if (userdetails) {
            this.bearertoken = 'Bearer ' + userdetails.Token;
            this.options.headers = new Headers({
                'Authorization': this.bearertoken,
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            });
        }
    }

    getHeaders() {
        var userdetails = this._localStore.getObject('userDetails');
        if (userdetails) {
            this.bearertoken = 'Bearer ' + userdetails.Token;
            console.log(new Headers({
                'Authorization': this.bearertoken,
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            }))
            return {
                'Authorization': this.bearertoken,
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            };
        }
        return null;
    }

    // Set Organisation Id in header.
    public setOrganisationHeader(id: string) {
        if (id) {
            this.options.headers.delete('OrganisationId');
            this.options.headers.append('OrganisationId', id);
        }
    }

    post(path: string, data: any): Observable<Response> {
        this._load.addCounter();
        path = this.apiUrl + path;
        data = (data === '' || typeof (data) === 'undefined') ? '' : JSON.stringify(data);
        return this.process(this._http.post(path, data, this.options));
    }

    authCheck(path: string, data: any): Observable<Response> {
        this._load.addCounter();
        path = this.apiUrl + path;
        return this.process(this._http.get(path, this.options));
    }

    get(path: string): Observable<Response> {
        this._load.addCounter();
        path = this.apiUrl + path;
        return this.process(this._http.get(path, this.options));

    }

    getForLogin(path: string): Observable<Response> {
        this._load.addCounter();
        path = this.apiUrl + path;
        return this.process(this._http.get(path, this.options));
    }

    put(path: string, data: any): Observable<Response> {
        this._load.addCounter();
        path = this.apiUrl + path;
        data = (data === '' || typeof (data) === 'undefined') ? '' : JSON.stringify(data);
        return this.process(this._http.put(path, data, this.options));
    }

    delete(path: string, id: string = null): Observable<Response> {
        this._load.addCounter();
        path = this.apiUrl + path;
        if (id === '' || typeof id === 'undefined' || id === null) {
            return this.process(this._http.delete(path, this.options));
        } else {
            return this.process(this._http.delete(`${path}/${id}`, this.options));
        }
    }

    upload(path: any, formData: any) {
        return new Promise<any>((resolve) => {
            let xhr: XMLHttpRequest = new XMLHttpRequest();
            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(xhr.response);
                    } else {
                        resolve(xhr.response)
                    }
                }
            };
            path = this.apiUrl + path;
            xhr.open('POST', path, true);;
            xhr.setRequestHeader('Authorization', this.bearertoken);
            xhr.setRequestHeader('processData', "false");
            xhr.setRequestHeader('contentType', "false");
            xhr.setRequestHeader('contentType', "false");
            xhr.setRequestHeader('OrganisationId', localStorage.getItem("orgId"));
            xhr.send(formData);
        })
    }

    payment(path: any, formData: any, token: any) {
        return new Promise((resolve) => {
            let xhr: XMLHttpRequest = new XMLHttpRequest();
            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(xhr.response);
                    } else {

                    }
                }
            };
            xhr.open('POST', path, true);;
            xhr.setRequestHeader('Authorization', token);
            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            xhr.send(formData);
        })
    }

    //common behaviour for request
    private request(response: Observable<Response>, successCallback: (data: any) => void, errorCallback: (error: string) => void, deleteLoader: boolean): Subscription {
        this._load.addCounter();
        return response
            .finally(() => this._load.removeCounter())
            .map(res => res.json())
            .subscribe(response => {
                if (response.isSuccess || response.IsSuccess) {  // fix for pascalCase 
                    successCallback(response.isSuccess != null ? response.data : response.Data);
                } else {
                    errorCallback(response.isSuccess != null ? response.message : response.Message);
                    console.log("error: " + response.isSuccess != null ? response.message : response.Message);
                }
            },
            err => {
                var errData = err._body;
                if (err.status != 200) {
                    if (errData == null || errData == undefined) {
                        console.log(err.isSuccess != null ? err.message : err.Message);
                        errorCallback(err.isSuccess != null ? err.message : err.Message);
                    } else {
                        var message = errData.Message ? errData.Message : errData.message
                        console.log("[Api] Error! : " + message);
                        errorCallback(message);
                    }
                }
            });
    }

    public requestPost(url: string, data: any, successCallback: (data: any) => void, errorCallback: (error: string) => void = null): Subscription {
        var response = this.post(url, data);
        return this.request(response, successCallback, errorCallback == null ? (error) => { } : errorCallback, false);
    }

    public requestGet(url: string, successCallback: (data: any) => void, errorCallback: (error: string) => void = null): Subscription {
        var response = this.get(url);
        return this.request(response, successCallback, errorCallback == null ? (error) => { } : errorCallback, true);
    }

    public requestPut(url: string, data: any, successCallback: (data: any) => void, errorCallback: (error: string) => void = null): Subscription {
        var response = this.put(url, data);
        return this.request(response, successCallback, errorCallback == null ? (error) => { } : errorCallback, false);
    }

    public requestDelete(url: string, successCallback: (data: any) => void, errorCallback: (error: string) => void = null): Subscription {
        var response = this.delete(url, '');
        return this.request(response, successCallback, errorCallback == null ? (error) => { } : errorCallback, false);
    }

    public refreshPaginationWithUrl(url: string): void {
        this.needRefreshPagination.emit(url);
    }

    public download(path: string, contentType: string, fileName: string) {
        path = this.apiUrl + path;

        let opt: RequestOptionsArgs = {};

        opt.headers = new Headers({
            'Authorization': this.bearertoken,
            'responseType': 'arraybuffer',
            'Content-Type': contentType,
        });

        return this._http.get(path, opt).map(res => new Blob([(<any>res)['_body']], { type: contentType }));
    }

    getDownloadFile(path: any) {
        this._load.addCounter();
        let options: RequestOptionsArgs = {};
        options.headers = new Headers({
            'Authorization': this.bearertoken,
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'OrganisationId': localStorage.getItem("orgId")
        });
        options.responseType = ResponseContentType.ArrayBuffer;
        path = this.apiUrl + path;
        return this._http.get(path, options).finally(() => this._load.removeCounter())
            .map(res => res.arrayBuffer());
    }

    public postDownloadFileWithModel(path: string, model: any) {
        let options: RequestOptionsArgs = {};
        options.headers = new Headers({
            'Authorization': this.bearertoken,
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'OrganisationId': localStorage.getItem("orgId")
        });
        path = this.apiUrl + path;
        options.responseType = ResponseContentType.ArrayBuffer;
        return this._http.post(path, model, options)
            .map(res => res.arrayBuffer());
    }

    public postDownloadFile(path: string, file: FileModel) {
        let options: RequestOptionsArgs = {};
        options.headers = new Headers({
            'Authorization': this.bearertoken,
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'OrganisationId': localStorage.getItem("orgId")
        });
        path = this.apiUrl + path;
        options.responseType = ResponseContentType.ArrayBuffer;
        return this._http.post(path, file, options)
            .map(res => res.arrayBuffer());
    }
}
