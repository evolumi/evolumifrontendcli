import { Injectable } from '@angular/core';
import { Api } from './api';
import { TranslateService } from '@ngx-translate/core';
import { NotificationService } from './notification-service';
import { AccountsCollection } from './accounts.service';

@Injectable()

export class accountdataSerivce {

    public sendYears: string;

    constructor(
        public api: Api,
        private notify: NotificationService,
        private translate: TranslateService,
        public _accountCollection: AccountsCollection,
    ) {
        this.smallScreen = window.innerWidth < 801;
    }

    public updateYear(financialStatement: any, Id: string) {

        // Update statement on currentAccount and call API.
        this.api.put('Accounts/' + Id + '/UpdatedYears', financialStatement)
            .map(res => res.json())
            .subscribe(a => {
                this.notify.success(this.translate.instant("YEARSUPDATED"));
                this._accountCollection.updateAccount();
                this._accountCollection.getAccount(Id, '', true);
                console.log("[Account-Data] Year Update: Success!");
            }, err => {
                console.log("[Account-Data] Year Update: Fail! -> Unknown error :(");
            });
    }


    public deleteYear(financialStatementYear: any, Id: string, ) {

        this.api.delete('Accounts/' + Id + '/Year', financialStatementYear)
            .map(res => res.json())
            .subscribe(a => {
                this.notify.success(this.translate.instant("YEARDELETED"));
                this._accountCollection.updateAccount();
                console.log("[Account-Data] Year Delete: Success!");
            }, err => {
                console.log("[Account-Data] Year Delete: Fail! -> Unknown error :(");
            });
    }

    public smallScreen: boolean;
    public setWidth(width: number) {
        this.smallScreen = width < 801;
    }

    public addYear(financialStatement: any, Id: string) {
        console.log("[Account-Data] Adding Year: Start!");
        this.api.post('Accounts/' + Id + '/Year', financialStatement)
            .map(res => res.json()).subscribe(res => {
                if (res.IsSuccess) {
                    console.log("[Account-Data] Adding Year: Success!");
                    this.notify.success(this.translate.instant("YEARCREATED"));
                    this._accountCollection.updateAccount();
                    this._accountCollection.getAccount(Id, '', true);
                }
                else {
                    console.log("[Acccount-Data] Adding Year: Fail! -> " + res.Message);
                    this.notify.error(this.translate.instant("FAILEDYEARCREATED"));
                }
            }, err => {
                this.notify.error(this.translate.instant("FAILEDYEARCREATED"))
            });
    }
}