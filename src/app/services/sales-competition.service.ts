import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { SalesCompRuleModel, SalesCompetitionModel, SalesCompLeaderboard } from '../models/sales-competition.models';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { LocalStorage } from './localstorage';
import { Api } from './api';
import { NotificationService } from './notification-service';
import { Subject } from 'rxjs/Subject';
import { HighchartService } from './highchart.service';
import { Observable } from "rxjs/Observable";

class DataStore {
    salesComps: Array<SalesCompetitionModel> = [];
}

export interface SearchParams {
    SortingFieldName: string;
    Name: string;
    StartDate?: number;
    EndDate?: number;
    Active: boolean;
    Inactive: boolean;
    Page: number;
    PageSize: number;
    [key: string]: any;
}

@Injectable()
export class SalesCompetitionService {

    private dataStore: DataStore = new DataStore();
    private apiInProgress: boolean;
    public searchParams: SearchParams;

    private currentRule$ = new BehaviorSubject<SalesCompRuleModel>(null);
    public currentRuleObs(): Observable<SalesCompRuleModel> {
        return this.currentRule$.asObservable();
    }

    private currentSalesComp$ = new BehaviorSubject<SalesCompetitionModel>(null);
    public currentSalesCompObs() {
        return this.currentSalesComp$.asObservable();
    }

    private currentSalesCompLeaderboard$ = new BehaviorSubject<SalesCompLeaderboard>(null);
    public currentSalesCompLeaderboardObs() {
        return this.currentSalesCompLeaderboard$.asObservable();
    }

    private addEditSalesComp$ = new BehaviorSubject<SalesCompetitionModel>(null);
    public addEditSalesCompObs() {
        return this.addEditSalesComp$.asObservable();
    }

    private salesCompList$ = new BehaviorSubject<Array<SalesCompetitionModel>>(null);
    public salesCompListObs() {
        return this.salesCompList$.asObservable();
    }

    private searchSalesComps$: Subject<string> = new Subject<string>();

    constructor(private translate: TranslateService,
        private localStorage: LocalStorage,
        private api: Api,
        private notify: NotificationService,
        private router: Router,
        private highchartSvc: HighchartService) {
        this.init();
    }

    init() {
        this.reset();
        this.searchSalesComps$
            .debounceTime(800)
            .distinctUntilChanged()
            .subscribe(x => this.getSalesComps());
    }

    searchSalesComps(val: string) {
        this.searchSalesComps$.next(val);
    }

    getSalesComps(scroll = false) {
        const orgId = this.localStorage.get('orgId');
        const query = this.getSearchString();
        this.api.get(`Organisations/${orgId}/SalesCompetitions/?${query}`)
            .map(res => res.json())
            .subscribe(res => {
                if (scroll === false) this.dataStore.salesComps = [];
                if (res.IsSuccess) {
                    this.dataStore.salesComps = [...this.dataStore.salesComps, ...res.Data];
                    this.salesCompList$.next(this.dataStore.salesComps);
                }
                this.apiInProgress = false;
            }, err => {
                this.apiInProgress = false;
            });
    }

    getSalesComp(id: string) {
        this.api.get(`SalesCompetition/${id}`)
            .map(res => res.json())
            .subscribe(res => {
                if (res.IsSuccess) {
                    this.currentSalesComp$.next(res.Data);
                }
            });
    }

    getSalesCompLeaderboard(id: string) {
        this.api.get(`SalesCompetitionLeaderboard/${id}`)
            .map(res => res.json())
            .subscribe(res => {
                if (res.IsSuccess) {
                    // this.setLocalStorage(res.Data.SalesCompetition.Id);
                    this.currentSalesComp$.next(res.Data.SalesCompetition);
                    this.currentSalesCompLeaderboard$.next(res.Data);
                }
            }, err => {
                this.currentSalesComp$.next(null);
                this.notify.error('SALESCOMPLOADFAIL');
            });
    }

    addSalesComp(salesComp: SalesCompetitionModel, fn?: Function) {
        const orgId = this.localStorage.get('orgId');
        this.api.post(`Organisations/${orgId}/SalesCompetition`, salesComp)
            .map(res => res.json())
            .subscribe(res => {
                if (res.IsSuccess) {
                    this.highchartSvc.clear();
                    this.dataStore.salesComps.push(res.Data);
                    this.salesCompList$.next(Object.assign({}, this.dataStore).salesComps);
                    this.notify.success(this.translate.instant('CREATESALESCOMPSUCCESS'));
                    this.addEditSalesComp$.next(null);
                    this.currentSalesComp$.next(res.Data);
                    if (fn) fn();
                    this.router.navigateByUrl(`/CRM/SalesCompetition/${res.Data.Id}`);
                    this.reset();
                    this.getSalesCompLeaderboard(res.Data.Id);
                } else {
                    this.notify.error(this.translate.instant('CREATESALESCOMPFAIL'));
                }
            }, err => {
                this.notify.error(this.translate.instant('CREATESALESCOMPFAIL'));
            })
    }

    updateSalesComp(salesComp: SalesCompetitionModel, fn?: Function) {
        this.api.put(`SalesCompetition/${salesComp.Id}`, salesComp)
            .map(res => res.json())
            .subscribe(res => {
                if (res.IsSuccess) {
                    this.highchartSvc.clear();
                    const index = this.dataStore.salesComps.map(x => x.Id).indexOf(salesComp.Id);
                    this.dataStore.salesComps.splice(index, 1, salesComp);
                    this.salesCompList$.next(Object.assign({}, this.dataStore).salesComps);
                    this.currentSalesComp$.next(salesComp);
                    this.addEditSalesComp$.next(null);
                    this.notify.success(this.translate.instant('UPDATESALESCOMPSUCCESS'))
                    if (fn) fn();
                    this.router.navigateByUrl(`/CRM/SalesCompetition/${salesComp.Id}`);
                    this.reset();
                    this.getSalesCompLeaderboard(salesComp.Id);
                } else {
                    this.notify.error(this.translate.instant('UPDATESALESCOMPFAIL'));
                }
            }, err => {
                this.notify.error(this.translate.instant('UPDATESALESCOMPFAIL'));
            })
    }

    deleteSalesComp(id: string, callback: (success: boolean) => any = null) {
        this.api.delete('SalesCompetition/' + id)
            .map(res => res.json())
            .subscribe(res => {
                if (res.IsSuccess) {
                    this.notify.success('DELETESALESCOMPSUCCESS');
                    this.dataStore.salesComps = this.dataStore.salesComps.filter(obj => !new Set([id]).has(obj.Id));
                    this.salesCompList$.next(Object.assign({}, this.dataStore).salesComps);
                    // this.checkLocalStorage(id);
                    // this.reroute(true);
                } else {
                    this.notify.error('DELETESALESCOMPFAIL');
                }
                if (callback) callback(res.IsSuccess);
            },
            err => {
                this.notify.error('DELETESALESCOMPFAIL');
            });
    }

    openAddSalesComp(id?: string) {
        if (id) {
            this.addEditSalesComp$.next(this.currentSalesComp$.value);
        } else {
            this.addEditSalesComp$.next(new SalesCompetitionModel({}));
        }
    }

    closeAddSalesComp() {
        this.addEditSalesComp$.next(null);
    }

    openAddRule(rule?: SalesCompRuleModel) {
        if (rule) {
            // this.currentRule$.next(rule);
        } else {
            this.currentRule$.next(new SalesCompRuleModel({}));
        }
    }

    closeAddRule() {
        this.currentRule$.next(null);
    }

    onScroll() {
        if (this.apiInProgress === true) return;
        this.apiInProgress = true;
        this.searchParams.Page += 1;
        this.getSalesComps(true);
    }

    reset() {
        this.searchParams = { SortingFieldName: 'EndDate', Name: '', Active: false, Inactive: false, Page: 0, PageSize: 30 };
    }

    resetPaging() {
        this.searchParams.Page = 0;
        this.searchParams.PageSize = 30
    }

    // reroute(toNew?: boolean) {
    //     console.log('REROUTE');
    //     const userId = this.localStorage.get('userId');
    //     const orgId = this.localStorage.get('orgId');
    //     let salesCompId = this.localStorage.get(`SalesComp-${userId}-${orgId}`);
    //     if ((!salesCompId && this.salesCompList$.value || toNew)) {
    //         if (this.salesCompList$.value.length) {
    //             salesCompId = this.salesCompList$.value[this.salesCompList$.value.length - 1].Id;
    //         }
    //     }
    //     if (salesCompId) {
    //         this.router.navigateByUrl('/CRM/SalesCompetition/' + salesCompId);
    //     } else {
    //         this.openAddSalesComp();
    //     }
    // }

    // private setLocalStorage(salesCompId: string) {
    //     const userId = this.localStorage.get('userId');
    //     const orgId = this.localStorage.get('orgId');
    //     this.localStorage.set(`SalesComp-${userId}-${orgId}`, salesCompId);
    // }

    // private checkLocalStorage(id: string) {
    //     const userId = this.localStorage.get('userId');
    //     const orgId = this.localStorage.get('orgId');
    //     let localStorageId = this.localStorage.get(`SalesComp-${userId}-${orgId}`);
    //     if (id === localStorageId) {
    //         this.localStorage.remove(`SalesComp-${userId}-${orgId}`);
    //     }

    // }

    private getSearchString() {
        var str: Array<string> = [];
        for (let p in this.searchParams) {
            if (this.searchParams.hasOwnProperty(p)) {
                if (typeof (this.searchParams[p]) == "object") {
                    for (let i in this.searchParams[p]) {
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(this.searchParams[p][i]));
                    }
                }
                else {
                    if (this.searchParams[p] === '' || this.searchParams[p] === 'All' || typeof this.searchParams[p] === 'undefined')
                        continue;
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(this.searchParams[p]));
                }
            }
        }
        return str.join("&");
    }
}