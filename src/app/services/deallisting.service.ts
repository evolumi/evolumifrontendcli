import { Injectable } from '@angular/core';
import { Api } from './api';
import { NotificationService } from './notification-service';
import { LocalStorage } from './localstorage';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import { DealService } from './deal.service';
import { UserDetailModel } from '../models/user.models';
import { ExportService } from './export.service';

@Injectable()

export class DealListing {
	//deals observer
	public deals$: Observable<any[]>;
	private _dealsObserver: Observer<any[]>;
	public deals: any[] = [];
	public usersSubscription: any;
	//public saleProcess: Array<SalesProcessModel> = new Array<SalesProcessModel>();
	public search: any = { Page: 1, PageSize: 20 };
	public apiInProgress: boolean = false;
	constructor(public _api: Api, public _localstore: LocalStorage, public _notify: NotificationService, public _trello: DealService, private exporter: ExportService) {

		this.deals$ = new Observable<Array<any>>((observer: any) => {
			this._dealsObserver = observer;
		}).share();
	}

	getAlldeals(scroll = false) {
		var query = this.getSearchString();
		var orgId = this._localstore.get('orgId');

		this._api.get('Organisations/' + orgId + '/Deals?' + query)
			.map(res => res.json())
			.subscribe(res => {
				this.apiInProgress = false;
				if (!scroll) this.deals = [];
				if (res.Data.length != 0) {
					for (var deal of res.Data) {
						var onwerString = '';
						for (var owners of deal.Owners) {
							onwerString = onwerString + owners.OwenerName + ", "
						}
						onwerString = onwerString.slice(0, onwerString.length - 2);

						var dealListObject = deal;
						dealListObject.OwnerName = onwerString;
						dealListObject.Value = deal.TotalInCompanyCurrency;

						this.deals.push(dealListObject);
					}
				}
				if (this._dealsObserver)
					this._dealsObserver.next(this.deals);
			});
	}

	public getDealsForAccountId(accountId: string): Observable<Array<any>> {
		console.log("[Deals] Getting deals for account [" + accountId + "]");
		return this._api.get(`Accounts/${accountId}/Deals`)
			.map(res => {
				let response = res.json();
				return response.Data;
			});
	}

	//export accounts 
	exportDeals() {
		let orgId = this._localstore.get('orgId');
		let userdetails = <UserDetailModel>JSON.parse(this._localstore.get('userDetails'));
		let query = this.getSearchString();
		const path = `Organisations/${orgId}/ExportDeals/${userdetails.UserId}/?${query}`;
		const date = new Date();
		const fileName = `Export-Deals-${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}.xlsx`
		const contentType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
		this.exporter.downloadExport(path, fileName, contentType);
		// window.open(this._api.apiUrl + `Organisations/${orgId}/ExportDeals/${userdetails.UserId}/?${query}`, "_blank");
	}

	//getAllDetalStatus() {
	//	var orgId = this._localstore.get('orgId');
	//	return this._api.get('Organisations/' + orgId + '/Settings')
	//		.map(res => res.json())
	//		.subscribe(res => {
	//			this.saleProcess = res.Data.SaleProcesses;
	//		})
	//}
	setSearch(search: any) {
		this.search = search;
		this.getAlldeals();
	}
	onScroll() {
		if (this.apiInProgress == true) return;
		this.apiInProgress = true;
		this.search.Page = (this.search.Page | 0) + 1;
		this.getAlldeals(true);
	}
	getSearchString() {
		this.search.IncludeAccountColor = true;
		var str: Array<any> = [];
		for (var p in this.search)
			if (this.search.hasOwnProperty(p)) {
				if (this.search[p] === '' || typeof this.search[p] === 'undefined')
					continue;
				str.push(encodeURIComponent(p) + "=" + encodeURIComponent(this.search[p]));
			}
		return str.join("&");
	}

	resetSearch() {
		this.search = { searchString: '', UserId: '', SaleProcessId: '', DealStatusIds: '', AccountId: '' };
		this.getAlldeals();
	}

	deleteDeal(dealId: string) {
		this._api.delete('Deals/' + dealId)
			.map(res => res.json())
			.subscribe(res => {
				this._notify.success('Deal deleted');
				this._trello.getTrelloBoard();
			});
	}
	ngOnDestroy() {
		if (this.usersSubscription) this.usersSubscription.unsubscribe();
	}

}