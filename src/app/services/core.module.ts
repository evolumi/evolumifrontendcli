import { NgModule, Optional, SkipSelf } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../modules/shared/shared.module';
import { AccountsCollection, FinancialStatement } from './accounts.service';
import { ActionslistCollection } from './ActionList.service';
import { ActionsCollection } from './actionservice';
import { ActionsflowCollection } from './Actionsflow.service';
import { Api } from './api';
import { AuthGuard } from './guards/auth-guard';
import { AuthService } from './auth-service';
import { BaseService } from './baseService';
import { CallService } from './callService';
import { ChatService } from './chatService';
import { Configuration } from './configuration';
import { ContactsCollection } from './contacts.service';
import { DealService } from './deal.service';
import { DealAddEdit } from './dealAddEdit.service';
import { DealListing } from './deallisting.service';
import { DealsBlockService } from './deals-block.service';
import { EvolumiGuard, EvolumiHandleUsersGuard, EvolumiSuperAdminGuard } from './guards/evolumi-guard';
import { FilterCollection } from './filterservice';
import { GeolocationService } from './geolocationService';
import { ImportService } from './importService';
import { IntercomService } from './interecomService';
import { LoaderService } from './loader.service';
import { LocalStorage } from './localstorage';
import { MarketplaceService } from './marketplaceService';
import { ModalService } from './modal.service';
import { NewsfeedService } from './newfeed.service';
import { NotificationService } from './notification-service';
import { OrgGuard } from './guards/org-guard';
import { OrgService } from './organisation.service';
import { PermissionService } from './permissionService';
import { SettingsDealsOperations } from './settings-deals.service';
import { SignalRService } from './signalRService';
import { UsersService } from './users.service';
import { ValidationService } from './validation.service';
import { DateService } from './date.service';
import { ConfirmationService } from "./confirmation.service";
import { ContractsService } from './contracts.service';
import { ExportService } from './export.service';
import { HighchartService } from './highchart.service';
import { GoalsService } from './goals.service';
import { SalesCompetitionService } from './sales-competition.service';
import { EvoRequireGuard } from './guards/evo-require-guard';
import { PackagesCollection } from './packages.service';
import { SalesCommissionService } from './sales-commission.service';
import { accountdataSerivce } from './account-data.service'
import { CelebrationService } from "./celebration.service";
import { CalendarService } from './calendar.service';
import { MediaService } from "./media.service";
import { EvoDatepickerService } from '../modules/shared/components/evo-datepicker/evo-datepicker.service';
import { ButtonPanelService } from "./button-panel.service";
import { NewsletterService } from './newsletter.service';
import { TemplateBuilderService } from './template-builder.service';
import { MapService } from "./map.service";
// import { CookieService } from 'ngx-cookie';
import { CategoryService } from './category.service';
import { TemplateSaveGuard } from "./guards/template-save.guard";
import { GoogleApiService } from './google-api.service';
import { WistiaService } from './wistia.service';
import { DragAndDropService } from './drag-and-drop.service';

@NgModule({
    imports: [
        FormsModule,
        SharedModule,
    ],
    declarations: [],
    providers: [
        UsersService,
        ContactsCollection,
        AccountsCollection,
        ActionslistCollection,
        ActionsCollection,
        ActionsflowCollection,
        Api,
        AuthGuard,
        AuthService,
        BaseService,
        OrgService,
        NewsfeedService,
        CallService,
        ChatService,
        Configuration,
        DealService,
        DealListing,
        DealsBlockService,
        EvolumiGuard,
        EvolumiHandleUsersGuard,
        EvolumiSuperAdminGuard,
        EvoRequireGuard,
        GeolocationService,
        ImportService,
        IntercomService,
        LoaderService,
        FilterCollection,
        LocalStorage,
        MarketplaceService,
        ModalService,
        NotificationService,
        OrgGuard,
        GoogleApiService,
        PermissionService,
        SettingsDealsOperations,
        SignalRService,
        ValidationService,
        DateService,
        ConfirmationService,
        ContractsService,
        ExportService,
        GoalsService,
        SalesCompetitionService,
        PackagesCollection,
        HighchartService,
        accountdataSerivce,
        FinancialStatement,
        CelebrationService,
        SalesCommissionService,
        CalendarService,
        MediaService,
        EvoDatepickerService,
        DealAddEdit,
        ButtonPanelService,
        NewsletterService,
        TemplateBuilderService,
        MapService,
        // CookieService,
        CategoryService,
        TemplateSaveGuard,
        WistiaService,
        DragAndDropService
    ],
    exports: [
        SharedModule
    ]
})
export class CoreModule {
    // To make sure this module is only ever loaded in the AppModule 
    constructor( @Optional() @SkipSelf() parentModule: CoreModule) {
        if (parentModule) {
            throw new Error(
                'CoreModule is already loaded. Import it in the AppModule only');
        }
    }

}