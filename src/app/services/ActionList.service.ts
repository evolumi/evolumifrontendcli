import { Injectable } from '@angular/core';
import { Api } from './api';
import { ActionsModel } from '../models/actions.models';
import { LocalStorage } from './localstorage';
import { NotificationService } from './notification-service';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import { Subject } from 'rxjs/Subject';
import { UserDetailModel } from '../models/user.models';
import { ExportService } from './export.service';

export interface searchParams {
	ActionIds: Array<string>;
	SearchString: string,
	AssignedId: Array<string>,
	Types: Array<string>,
	Status: Array<string>,
	Labels: Array<string>,
	CloseStartDate?: number;
	CloseEndDate?: number;
	Page: number,
	PageSize: number,

	[key: string]: any
}

@Injectable()

export class ActionslistCollection {

	public searchParams: searchParams;
	public queryString: string;
	Actionslist: Array<ActionsModel> = new Array<ActionsModel>();
	//account type observer goes here
	public actionList$: Observable<any>;
	private _actionListObserver: Observer<any>;

	public selectAll: boolean = false;
	public selectedActions: Array<string> = [];

	public apiInProgress: boolean = false;

	private actionChange$: Subject<any> = new Subject();
	public actionChangeObservable(): Observable<any> {
		return this.actionChange$.asObservable();
	}

	constructor(
		public _http: Api,
		public notify: NotificationService,
		private translate: TranslateService,
		public _store: LocalStorage,
		private exporter: ExportService) {
		this.reset();
		this.actionList$ = new Observable((observer: any) => {
			this._actionListObserver = observer;
		}).share();
	}

	reset() {
		this.selectAll = false;
		this.selectedActions = [];
		this.searchParams = {
			SearchString: '', AssignedId: [], Types: [], Status: [], Labels: []
			, Page: 1, PageSize: 20, ActionIds: []
		}
	}

	getSearchString() {
		var str: Array<any> = [];
		for (var p in this.searchParams)
			if (this.searchParams.hasOwnProperty(p)) {
				if (this.searchParams[p] === '' || typeof this.searchParams[p] === 'undefined')
					continue;
				str.push(encodeURIComponent(p) + "=" + encodeURIComponent(this.searchParams[p]));
			}
		return str.join("&");

	}
	getActions(scroll = false) {
		var orgId = this._store.get('orgId');
		var query = this.getSearchString();
		this._http.get(`Organisations/${orgId}/Actions?${query}`)
			.map(res => res.json())
			.subscribe(res => {
				console.log('RES', res);
				this.apiInProgress = false;
				if (scroll == false) this.Actionslist = []
				for (var feed of res.Data) {
					this.Actionslist = [...this.Actionslist, new ActionsModel(feed)];
				}
			});
	}

	//export Actions
	exportActions() {
		const orgId = this._store.get('orgId');
		const userdetails = <UserDetailModel>JSON.parse(this._store.get('userDetails'));
		const query = this.getSearchString();
		const path = `Organisations/${orgId}/ExportActions/${userdetails.UserId}/?${query}`;
		const date = new Date();
		const fileName = `Export-Actions-${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}.xlsx`
		const contentType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
		this.exporter.downloadExport(path, fileName, contentType);
		// window.open(this._http.apiUrl, "_blank");
	}

	UpdateSearchFields(updateArray: any) {
		for (var arr in updateArray) {
			(updateArray[arr] === 'All Activity') ? this.searchParams[arr] = '' : this.searchParams[arr] = updateArray[arr];
		}
		this.getActions();
	}
	onScroll() {
		if (this.apiInProgress == true) return;
		this.apiInProgress = true;
		this.searchParams.Page = (this.searchParams.Page | 0) + 1;
		this.getActions(true);
	}

	public deleteMany() {
		var searchQuery: string = "";
		if (this.selectAll) {
			searchQuery = "/?" + this.getSearchString();
		}
		let orgId = localStorage.getItem("orgId");
		this._http.post('Actions/DeleteMany' + searchQuery, { GetAccountsFromFilter: this.selectAll, Ids: this.selectedActions, OrganisationId: orgId })
			.map(res => res.json())
			.subscribe(body => {
				if (body.IsSuccess === true) {
					this.selectAll = false;
					this.selectedActions = [];
					this.notify.success(this.translate.instant("ACTIONDELETED"));
					this.actionChange$.next(undefined);
				}
			});
	}
}
