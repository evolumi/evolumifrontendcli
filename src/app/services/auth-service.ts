import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptionsArgs } from '@angular/http';
import { Router } from '@angular/router';
import { Api } from './api';
import { LogedInUserModel } from '../models/user.models';
import { LoaderService } from './loader.service';
import { NotificationService } from './notification-service';
import { LocalStorage } from './localstorage';
import { TranslateService } from '@ngx-translate/core';
import { IntercomService } from '../services/interecomService';
import { Observable } from "rxjs/Observable";
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Configuration } from './configuration';

@Injectable()
export class AuthService {

    private login = { remeberMe: false, Username: "", Password: "" };
    private options: RequestOptionsArgs = {
        headers: new Headers({
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        })
    };

    //private timerId: any = null;

    private redirectUrl: string;
    public isLoggedIn: boolean = false;
    public isEvolumi: boolean = false;
    public evolumiAdminType: string;
    public error: string;

    // Observables
    private currentUser$: BehaviorSubject<LogedInUserModel> = new BehaviorSubject(undefined);
    public currentUserObservable(): Observable<LogedInUserModel> {
        return this.currentUser$.asObservable();
    }

    public currentUser: LogedInUserModel;

    public processing: boolean = false;
    public updatingUser: boolean;

    constructor(
        private http: Http,
        private api: Api,
        private _intercom: IntercomService,
        private translate: TranslateService,
        private notifications: NotificationService,
        private loader: LoaderService,
        private router: Router,
        private localStorage: LocalStorage,
        private config: Configuration
    ) {
        loader.clear();
    }

    // Sets a URL to navigate to after Login success.
    public setRedirectUrl(url: string) {
        this.redirectUrl = url;
    }

    // Login user and set Auth-Token and APi Headers and redirect user to CRM system.
    public Login(userName: string, password: string) {

        if (this.processing === false) {

            this.login.Username = userName;
            this.login.Password = password;
            this.processing = true;
            this.loader.addCounter();

            console.log("[Auth] Login: Started! -> " + userName);
            this.http.post(this.api.apiUrl + 'Users/Login', JSON.stringify(this.login), this.options)
                .map(res => res.json())
                .finally(() => this.loader.removeCounter())
                .subscribe(res => {
                    this.error = "";
                    if (res.IsSuccess) {
                        console.log("[Auth] Login: Success!");
                        this.isLoggedIn = true;
                        this.notifications.success(this.translate.instant("LOGINSUCCESS"));
                        this.localStorage.setObject('userDetails', res.Data);
                        this.api.setHeaders();
                        this.getCurrentUser();

                        let redirect = this.redirectUrl ? this.redirectUrl : 'CRM/Dashboard';
                        this.redirectUrl = "";
                        console.log("[Auth] Redirecting to: " + redirect);
                        this.router.navigate([redirect]);

                    } else {
                        this.error = this.translate.instant("LOGINCHECKEMAILPASSWORD");
                        this.notifications.error(this.translate.instant("LOGINFAILED"));
                        console.log("[Auth] Login: Failed!");
                    }
                },
                (err) => {
                    this.error = this.translate.instant("LOGINCHECKEMAILPASSWORD");
                    this.notifications.error(this.translate.instant("LOGINFAILED"));
                    console.log("[Auth] Login: Failed!");

                    this.processing = false;
                },
                () => {
                    this.processing = false;
                });
        }
    }

    // Login as any user. Only usable by SuperAdmin.
    public AdminLogin(userId: string) {
        this.processing = true;
        this.api.post(`Users/AdminLogin/${userId}`, null)
            .map(res => res.json())
            .finally(() => {
                this.processing = false;
            })
            .subscribe(res => {
                if (res.IsSuccess) {
                    console.log("[Auth] Admin-Login: Success!");
                    this.isLoggedIn = true;
                    this.notifications.success(this.translate.instant("LOGINSUCCESS"));
                    this.localStorage.setObject('userDetails', res.Data);
                    this.api.setHeaders();
                    this.getCurrentUser();

                    console.log("[Auth] Redirecting to: Overview");
                    this.router.navigate(['Overview']);
                }
            },
            err => {
                this.notifications.error(this.translate.instant("LOGINFAILED"));
                console.log("[Auth] Admin-Login: Failed!");
            });
    }

    // Tro to get the current user data with the stored Auth-Token. If bad token clear it and navigate to Login page.
    public TryTokenLogin() {

        console.log("[Auth] Trying to Auto-Login User");
        this.api.requestGet(
            `Users/LoggedInUser`,
            (data) => {
                console.log("[Auth] Auto-Login: Success!");
                this.isLoggedIn = true;
                this.setCurrentUser(data);

                let redirect = this.redirectUrl;
                this.redirectUrl = "";
                console.log("[Auth] Redirecting to: " + redirect);
                this.router.navigate([redirect]);
            },
            (error) => {
                this.Logout();
            }
        );
    }

    // Reload current user data.
    public GetCurrentUser() {

        this.api.requestGet(
            `Users/LoggedInUser`,
            (data) => {
                console.log("[Auth] User data re-loaded");
                this.setCurrentUser(data);
            },
            (error) => {
                console.log("[Auth] Failed to get user data");
            }
        );
    }

    // Logs out the user by removing all the stuff stored in localstorage. Redirects to Login page.
    public Logout() {
        console.log("[Auth] Logging out -> Navigating to Login");

        this.clearLocalStorage();
        this.isLoggedIn = false;
        this._intercom.shutdown();

        this.currentUser$.next(undefined);
        this.router.navigateByUrl("/Login");
    }

    // Update User Data
    public updateUser(user: any): void {
        if (!this.updatingUser) {
            this.updatingUser = true;

            this.api.requestPut(
                'Users/NameChange',
                user,
                (res) => {
                    this.updatingUser = false;
                    this.notifications.success(this.translate.instant('USERSAVEDSUCESSFULY'));
                    this.getCurrentUser();
                },
                (err) => {
                    this.updatingUser = false;
                    this.notifications.error(err);
                }
            );
        }
    }

    // Upload new Profile picture.
    public uploadAvatar(formData: FormData): void {
        this.api.upload('Users/Upload/ProfilePicture', formData)
            .then(res => {
                this.getCurrentUser();
            })
    }

    // Load user data. 
    private getCurrentUser(): void {
        console.log("[Auth] Loading User-Data: Started!");
        this.api.requestGet(
            `Users/LoggedInUser`,
            (user) => {
                console.log("[Auth] Loading User-Data: Complete! -> " + user.Email);
                this.setCurrentUser(user);
            },
            (error) => {
                console.log("[Auth] Loading User-Data: Error! -> " + error);
            }
        );
    }

    // Updates Language and sets User Data.
    private setCurrentUser(user: LogedInUserModel) {
        if (user.EvolumiAdmin) {
            console.log("[Auth] Found Evolumi User -> " + user.EvolumiAdmin);
            this.evolumiAdminType = user.EvolumiAdmin;
            this.isEvolumi = true;
        }
        else {
            this.evolumiAdminType = user.EvolumiAdmin;
            this.isEvolumi = false;
        }

        // Boot Intercom
        this._intercom.boot(user.Id);
        this._intercom.updateAllUserData(user);

        user.timestamp = new Date().getTime();
        this.currentUser$.next(user);
        this.currentUser = user;
        var lan = user.Language ? user.Language : this.config.defaultLanguage;

        localStorage.setItem('userId', user.Id);
        localStorage.setItem('userName', user.FirstName);
        localStorage.setItem('lastTime', user.timestamp.toString());
        localStorage.setItem('lan', lan);
        this.setLanguage(lan);

    }

    public addPhonenumber(number: any, description: string) {
        this.api.requestPost("Phonenumbers", { Number: number, Description: description }, res => {
            let user = this.currentUser$.value;
            user.PhoneNumbers = res;
            this.currentUser$.next(user);
            this.notifications.success(this.translate.instant("USERPHONEUPDATED"));
        },
            error => {
                console.log("[Auth] Adding phonenumber failed! -> " + error);
                this.notifications.error(error);
            });
    }

    public deletePhonenumber(id: string) {
        this.api.requestDelete("Phonenumbers/" + id, res => {
            let user = this.currentUser$.value;
            user.PhoneNumbers = res;
            this.currentUser$.next(user);
            this.notifications.success(this.translate.instant("USERPHONEUPDATED"));
        });
    }

    // Sets language based on user settings.
    public setLanguage(lan: string) {

        this.translate.setDefaultLang(lan + "." + this.config.languageFileVersion);
        this.translate.use(lan + "." + this.config.languageFileVersion);
        console.log("[Language] Setting language to [" + lan + "." + this.config.languageFileVersion + "]");
    }

    private clearLocalStorage(): void {
        localStorage.removeItem('userDetails');
        localStorage.removeItem('userName');
        localStorage.removeItem('lastTime');
        localStorage.removeItem('userId');
        localStorage.removeItem('lan');
        localStorage.removeItem('orgId');
        localStorage.removeItem('orgName');
        localStorage.removeItem('LastCity');
        localStorage.removeItem('LastCountry');
        localStorage.removeItem('LastLatitude');
        localStorage.removeItem('LastLongitude');
        localStorage.removeItem('LocationTimestamp');
        localStorage.removeItem('lastTime');
    }
    // Check if to update location for the users.
    //private updateLocation() {
    //    var user = this.currentUser$.value;
    //    if (user) {
    //        var locationTimestampInt = user.LocationTimestamp;
    //        //86400000 - miliseconds in 1 day
    //        if (Date.now() - locationTimestampInt >= 86400000 && !this._updateLocationInProgress) {
    //            this._updateLocationInProgress = true;
    //            this.updateGeolocation(user.UserId);
    //        }
    //    }
    //}

    // Update location data for User.
    //private updateGeolocation(userId: string): void {
    //    this.geoLocation.getLocation({
    //        enableHighAccuracy: false
    //    }).subscribe(
    //        (location) => {
    //            if (location != null) {
    //                var data = {
    //                    Latitude: location.coords.latitude,
    //                    Longitude: location.coords.longitude,
    //                    Timestamp: location.timestamp,
    //                };
    //                this.api.requestPut(
    //                    `Users/${userId}/UpdateGeolocation`,
    //                    data,
    //                    (response) => {
    //                        this._updateLocationInProgress = false;
    //                        localStorage.setItem('LastCity', response.LastCity);
    //                        localStorage.setItem('LastCountry', response.LastCountry);
    //                        localStorage.setItem('LastLatitude', response.LastLatitude);
    //                        localStorage.setItem('LastLongitude', response.LastLongitude);
    //                        localStorage.setItem('LocationTimestamp', response.LocationTimestamp);
    //                    },
    //                    () => {
    //                        this._updateLocationInProgress = false;
    //                    }
    //                );
    //            }
    //        },
    //        (error) => {
    //            console.log("Error: " + error);
    //            this._updateLocationInProgress = false;
    //        });
    //}

    ngOnDestroy() {
    }
}