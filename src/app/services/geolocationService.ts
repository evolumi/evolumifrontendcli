import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class GeolocationService {
    constructor(
        private _translate: TranslateService
    ) { }

    public getLocation(opts: PositionOptions): Observable<Position> {
        return Observable.create((observer: any) => {
            if (window.navigator && window.navigator.geolocation) {
                window.navigator.geolocation.getCurrentPosition(
                    (position: Position) => {
                        observer.next(position);
                        observer.complete();
                    },
                    (error: any) => {
                        switch (error.code) {
                            case 1:
                                observer.error(this._translate.instant('LOCATIONTPERMISSIONDENIED'));
                                break;
                            case 2:
                                observer.error(this._translate.instant('LOCATIONPOSITIONUNAVAILABLE'));
                                break;
                            case 3:
                                observer.error(this._translate.instant('LOCATIONTIMEOUT'));
                                break;
                        }
                    },
                    opts);
            }
            else {
                observer.error(this._translate.instant('LOCATIONUNSUPORTEDBROWSER'));
            }

        });
    }
}