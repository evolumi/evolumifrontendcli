import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { Subject } from "rxjs/Subject";
import { Observable } from 'rxjs/Observable';
import { ContractsModel } from '../models/contracts.models';

import { Api } from './api';
import { NotificationService } from './notification-service';
import { LocalStorage } from './localstorage';
import { OrganisationModel } from "../models/organisation.models";

export interface SearchParams {
    SortingFieldName: string;
    Name: string;
    OurPartyData: Array<string>;
    Categories: Array<string>;
    CategoryGroups: Array<string>;
    CategoryAll: boolean;
    CategoryAny: boolean;
    StartDate?: number;
    EndDate?: number;
    DateType: string;
    Page: number;
    PageSize: number;
    [key: string]: any;
}

@Injectable()
export class ContractsService {

    private dataStore: {
        contracts: ContractsModel[]
    }

    public org: OrganisationModel;

    public searchParams: SearchParams;

    //CreateEdit modal
    public showAddModal: boolean;

    private contractsList$: BehaviorSubject<Array<ContractsModel>> = new BehaviorSubject<Array<ContractsModel>>([]);
    public contractsListObs(): Observable<Array<ContractsModel>> {
        return this.contractsList$.asObservable();
    }

    private contract$: BehaviorSubject<ContractsModel> = new BehaviorSubject<ContractsModel>(null);
    public contractObs(): Observable<ContractsModel> {
        return this.contract$.asObservable();
    }

    //Special because it was impossible to do this in subcomponent with debounceTime
    private searchContracts$: Subject<string> = new Subject<string>();

    private apiInProgress: boolean;
    public selectAll: boolean;

    constructor(private translateService: TranslateService,
        private api: Api,
        private notify: NotificationService,
        private localStorage: LocalStorage,
        private router: Router) {
        this.init();
    }

    init() {
        this.searchParams = { SortingFieldName: 'NextDate', Name: '', OurPartyData: [], Categories: [], CategoryGroups: [], CategoryAll: true, CategoryAny: false, Page: 0, PageSize: 30, DateType: 'enddate' };
        this.dataStore = { contracts: [] };
        this.searchContracts$
            .debounceTime(800)
            .distinctUntilChanged()
            .subscribe(x => this.getContracts());
    }

    searchContracts(val: string) {
        this.searchContracts$.next(val);
    }

    getContracts(scroll = false) {
        const query = this.getSearchString();
        this.api.get(`Contracts?${query}`)
            .map(res => res.json())
            .subscribe(res => {
                if (!scroll) this.dataStore.contracts = [];
                if (res.IsSuccess) {
                    this.dataStore.contracts = [...this.dataStore.contracts, ...res.Data];
                    this.contractsList$.next(this.dataStore.contracts);
                }
                this.apiInProgress = false;
            }, err => {
                this.apiInProgress = false;
            });
    }

    getContract(id: string) {
        this.api.get(`Contracts/${id}`)
            .map(res => res.json())
            .subscribe(res => {
                if (res.IsSuccess) {
                    this.contract$.next(res.Data);
                }
            })
    }

    addContract(contract: ContractsModel, fn?: Function) {
        const orgId = this.localStorage.get('orgId');
        this.api.post(`Contracts`, contract)
            .map(res => res.json())
            .subscribe(res => {
                if (res.IsSuccess) {
                    this.dataStore.contracts.push(res.Data);
                    this.contractsList$.next(Object.assign({}, this.dataStore).contracts);
                    this.notify.success(this.translateService.instant('CREATECONTRACTSUCCESS'))
                    this.reroute(res.Data.Id);
                    if (fn) fn();
                } else {
                    this.notify.error(this.translateService.instant('CREATECONTRACTFAIL'));
                }
            }, err => {
                this.notify.error(this.translateService.instant('CREATECONTRACTFAIL'));
            })
    }

    updateContract(contract: ContractsModel, fn?: Function, failFn?: Function, noNotify?: boolean) {
        this.api.put(`Contracts/${contract.Id}`, contract)
            .map(res => res.json())
            .subscribe(res => {
                if (res.IsSuccess) {
                    const index = this.dataStore.contracts.map(x => x.Id).indexOf(contract.Id);
                    this.dataStore.contracts.splice(index, 1, contract);
                    this.contractsList$.next(Object.assign({}, this.dataStore).contracts);
                    this.contract$.next(contract);
                    if (noNotify) {
                        this.notify.success(this.translateService.instant('UPDATECONTRACTSUCCESS'))
                    }
                    if (fn) fn();
                } else {
                    if (failFn) failFn();
                    this.notify.error(this.translateService.instant('UPDATECONTRACTFAIL'));
                }
            }, err => {
                this.notify.error(this.translateService.instant('UPDATECONTRACTFAIL'));
            })
    }

    deleteContract(id: string) {
        this.api.delete('Contracts/' + id)
            .map(res => res.json())
            .subscribe(res => {
                if (res.IsSuccess) {
                    this.notify.success(this.translateService.instant('DELETECONTRACTSUCCESS'));
                    this.dataStore.contracts = this.dataStore.contracts.filter(obj => !new Set([id]).has(obj.Id));
                    this.contractsList$.next(Object.assign({}, this.dataStore).contracts);
                } else {
                    this.notify.error(this.translateService.instant('DELETECONTRACTFAIL'));
                }
            },
            err => {
                this.notify.error(this.translateService.instant('DELETECONTRACTFAIL'));
            });
    }

    createNew() {
        this.contract$.next(new ContractsModel({ StartDate: new Date().getTime() }))
        this.showModal(true);
    }

    showModal(show: boolean) {
        this.showAddModal = show;
    }

    openContractDetails(id: string) {
        this.getContract(id);
    }

    addTempFile(file: any) {
        let formData: FormData = new FormData();
        formData.append('file', file);
        return this.api.upload('Contract/UploadTempFile', formData)
    }

    addFile(file: any) {
        const orgId = this.localStorage.get('orgId');
        let formData: FormData = new FormData();
        formData.append('file', file);
        return this.api.upload(`Organisations/${orgId}/Contract/UploadFile`, formData)
    }

    deleteTempFile(id: string) {
        return this.api.delete(`Contract/DeleteTempFile`, id)
            .map(res => res.json());
    }

    deleteFile(id: string) {
        return this.api.delete(`Organisations/{id}/Contract/DeleteFile`, id)
            .map(res => res.json());
    }

    reset() {
        this.searchParams = { SortingFieldName: 'NextDate', Name: '', OurPartyData: [], Categories: [], CategoryGroups: [], Page: 0, PageSize: 30, CategoryAll: true, CategoryAny: false, DateType: 'enddate' };
    }

    getSearchString() {
        var str: Array<string> = [];
        for (let p in this.searchParams) {
            if (this.searchParams.hasOwnProperty(p)) {
                if (typeof (this.searchParams[p]) == "object") {
                    for (let i in this.searchParams[p]) {
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(this.searchParams[p][i]));
                    }
                }
                else {
                    if (this.searchParams[p] === '' || this.searchParams[p] === 'All' || typeof this.searchParams[p] === 'undefined')
                        continue;
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(this.searchParams[p]));
                }
            }
        }
        return str.join("&");
    }

    reroute(id: string) {
        this.router.navigateByUrl('/CRM/Contracts/' + id);
    }

    setContract(contract?: ContractsModel) {
        this.contract$.next(contract);
    }

    onScroll() {
        if (this.apiInProgress === true) return;
        this.apiInProgress = true;
        this.searchParams.Page += 1;
        this.getContracts(true);
    }
}