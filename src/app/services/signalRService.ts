import { Injectable, EventEmitter } from '@angular/core';
import { LocalStorage } from './localstorage';
import { ConversationModel } from '../models/conversation.models';
import { environment } from "../../environments/environment";
declare var $: any

@Injectable()
export class SignalRService {

    private proxy: SignalR.Hub.Proxy;
    private proxyName: string = 'MessagesHub';
    private connection: SignalR.Hub.Connection;

    public messageReceived: EventEmitter<any>;
    public conversationReceived: EventEmitter<ConversationModel>;
    public usersListReceived: EventEmitter<any>;

    public connectionEstablished: EventEmitter<Boolean>;
    public connectionStatus: EventEmitter<String>;
    public connectionExists: Boolean;

    constructor(
        private _localStore: LocalStorage
    ) {
        this.connectionEstablished = new EventEmitter<Boolean>();
        this.connectionStatus = new EventEmitter<String>();

        this.messageReceived = new EventEmitter<any>();
        this.conversationReceived = new EventEmitter<ConversationModel>();
        this.usersListReceived = new EventEmitter<any>();
        this.connectionExists = false;

        this.connection = $.hubConnection(environment.apiUrl);
        this.proxy = this.connection.createHubProxy(this.proxyName);

        this.connection.disconnected(() => {
            this.connectionEstablished.emit(false);
            this.connectionStatus.emit("Disconnected from server. Please refresh page");
        });

        this.connection.reconnecting(() => {
            this.connectionEstablished.emit(false);
            this.connectionStatus.emit("Reconnecting...");
        });

        this.connection.reconnected(() => {
            this.connectionEstablished.emit(true);
            this.connectionStatus.emit("");
        });

        this.registerOnServerEvents();
        this.startConnection();
    }

    private startConnection(): void {
        var jsdonData = this._localStore.get("userDetails");
        var userdetails = JSON.parse(jsdonData);
        console.log('[SignalR] Trying to connect with UserId [' + userdetails.UserId + ']');
        this.connection.qs = { "userId": userdetails.UserId };
        this.connection.start().done((data) => {
            console.log('[SignalR] Connected via ' + data.transport.name + ', connection ID= ' + data.id);
            this.connectionEstablished.emit(true);
            this.connectionStatus.emit("");
            this.connectionExists = true;
        }).fail((error) => {
            console.log('[SignalR] Could not connect! ' + error);
            this.connectionEstablished.emit(false);
            this.connectionStatus.emit("Could not connect to server");
        });
    }

    private registerOnServerEvents(): void {
        this.proxy.on('Tell_About_New_Message', (data) => {
            console.log('SIGNALR', data);
            var conversationIdMessageBundle = JSON.parse(data);
            console.log('[SignalR] New message received in SignalR, conversationId=' + conversationIdMessageBundle.conversationId + ' message="' + conversationIdMessageBundle.message.text + '"');
            this.messageReceived.emit(conversationIdMessageBundle);
        });
        this.proxy.on('Tell_About_New_Conversation', (data) => {
            var conversation = <ConversationModel>JSON.parse(data);
            console.log('[SignalR] New conversation received, conversationId=' + conversation.id);
            this.conversationReceived.emit(conversation);
        });
        this.proxy.on('Tell_About_Updated_Userlist', (data) => {
            var conversationIdUsersBundle = JSON.parse(data);
            console.log('[SignalR] Updated users list received, conversationId=' + conversationIdUsersBundle.conversationId + ' count=' + conversationIdUsersBundle.users.length);
            this.usersListReceived.emit(conversationIdUsersBundle);
        });
    }
}
