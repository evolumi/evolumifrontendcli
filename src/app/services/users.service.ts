import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subscription } from 'rxjs/Subscription';

import { Api } from './api';
import { OrgService } from '../services/organisation.service';
import { NotificationService } from './notification-service';

import { ChosenOptionModel } from '../modules/shared/components/chosen/chosen.models';
import { AllUsersModel, ChangeManyUserSettingsModel } from '../models/user.models';
import { TranslateService } from '@ngx-translate/core';

export interface searchParams {
    Page: number,
    PageSize: number,
    onlyActive: boolean
    OrganisationUserStatus: string,
    UserStatus: string,
    Email: string,
    Name: string,
    DateCreatedStart: string,
    DateCreatedEnd: string
}

@Injectable()
export class UsersService {

    private userList: any[];
    public searchParams: searchParams;
    public user: AllUsersModel = new AllUsersModel({ 'FirstName': '', 'LastName': '', 'Email': '', 'OrganisationRole': '' });

    // Observables 
    private users$: BehaviorSubject<Array<AllUsersModel>> = new BehaviorSubject([]);
    public usersObservable(): Observable<Array<AllUsersModel>> {
        return this.users$.asObservable();
    }

    private invitedUsers$: BehaviorSubject<Array<any>> = new BehaviorSubject([]);
    public invitedUsersObservable(): Observable<Array<any>> {
        return this.invitedUsers$.asObservable();
    }

    private activeUserList$: BehaviorSubject<Array<ChosenOptionModel>> = new BehaviorSubject([]);
    public activeUserListObervable(): Observable<Array<ChosenOptionModel>> {
        return this.activeUserList$.asObservable();
    }

    private userList$: BehaviorSubject<Array<ChosenOptionModel>> = new BehaviorSubject([]);
    public userListObervable(): Observable<Array<ChosenOptionModel>> {
        return this.userList$.asObservable();
    }

    // Subscriptions
    private orgSub: Subscription;
    private userSub: Subscription;

    private loadingUsersFor: string = '';

    constructor(
        private api: Api,
        orgService: OrgService,
        private notify: NotificationService,
        private translate: TranslateService
    ) {

        this.orgSub = orgService.currentOrganisationObservable().subscribe(org => {
            if (org) {
                this.getAllUsers();
            }
        });

        // this.userSub = this.auth.currentUserObservable().subscribe(user => {
        //     if (user) { this.getAllUsers(); }
        // })
    }

    public getProfilePictureForUser(userId: string) {
        let url: string;
        if (this.userList) {
            url = this.userList.filter(function (obj) {
                return obj.Id === userId;
            })[0].ProfilePictureUrl;
        }
        return url;
    }

    // Load (ID/Name) pairs for All Users and Active Users.
    public getUserList(orgId: string = null) {
        this.getUsers();
    }

    // Load All Users.
    public getAllUsers(): void {
        this.resetSearchParams();
        this.getUsers();
    }

    // Load Uasers.
    public getUsers(orgId: string = null) {
        orgId = orgId ? orgId : localStorage.getItem('orgId');

        if (!orgId) {
            return;
        }

        if (orgId == this.loadingUsersFor) {
            console.log("[Users] Load Users: Aborted! -> Already loading users for this Organisation.");
            return;
        }

        this.loadingUsersFor = orgId;
        var query = this.getSearchString();
        this.api.requestGet(`Organisations/${orgId}/Users?${query}`,
            (users) => {
                this.users$.next(users);
                console.log("[User] Loaded all users for Organisation [" + orgId + "] Found [" + users.length + "] of them");

                let list: Array<any> = [];
                let activeList: Array<any> = [];
                for (let u of users) {
                    list.push(new ChosenOptionModel(u.Id, u.FullName));
                    if (u.OrganisationStatus == "Active")
                        activeList.push(new ChosenOptionModel(u.Id, u.FullName));
                }
                console.log("[Users] " + activeList.length + " of them Active");
                this.userList$.next(list);
                this.activeUserList$.next(activeList);
                this.loadingUsersFor = null;
                this.userList = users;
            },
            (error) => {
                console.log("[Users] Error Loading Users!");
            });

    }

    // Get User
    public getUserById(userId: string) {

        // Try to first load user from UserList.
        for (let user of this.users$.value) {
            if (user.Id == userId) {
                this.user = user;
                return;
            }
        }

        this.api.get('Users/' + userId)
            .map(res => res.json())
            .subscribe(res => {
                if (res.IsSuccess === true) {
                    this.user = new AllUsersModel(res.Data);
                } else {
                    this.notify.error('error on fetching data');
                }
            });
    }

    // Update Role on a User
    public updateUserRole(data: any) {
        let orgId = localStorage.getItem('orgId');
        this.api.put('Organisations/' + orgId + '/SetUserRole', data)
            .map(res => res.json())
            .subscribe(res => {
                if (res.IsSuccess === true) {
                    this.notify.success(this.translate.instant("USERDATAUPDATED"));
                    this.getUsers();
                } else {
                    this.notify.error(this.translate.instant("UPDATEUSERDATAFAILED"));
                }
            }
            );
    }

    // Deletes a user.
    public deleteUser(id: string) {
        let orgId = localStorage.getItem('orgId');
        this.api.post('Organisations/' + orgId + '/RemoveUser/' + id, '')
            .map(res => res.json())
            .subscribe(res => {
                if (res.IsSuccess === true) {
                    this.notify.success(this.translate.instant("USERDELETED"));
                    this.getUsers();
                } else {
                    this.notify.error(this.translate.instant("SOMETHINGWRONG"));
                }
            }, err => {
                var ErrR = err.json();
                if (typeof ErrR.Message !== 'undefined' && ErrR.Message !== '') {
                    this.notify.error(ErrR.Message);
                }
            });
    }

    public deleteMany(model: ChangeManyUserSettingsModel) {
        this.api.post('Users/RemoveMany', model)
            .map(res => res.json())
            .subscribe(res => {
                if (res.IsSuccess) {
                    this.notify.success(this.translate.instant('USERSDELETED'));
                    this.getUsers();
                } else {
                    this.notify.error(this.translate.instant('USERSDELETEDFAIL'));
                }
            }, err => {
                this.notify.error(this.translate.instant('USERSDELETEDFAIL'));
            });
    }

    // Invite User
    public inviteUser(data: any) {
        let orgId = localStorage.getItem('orgId');
        this.api.post('Organisations/' + orgId + '/InviteUser', data)
            .map(res => res.json())
            .subscribe(res => {
                if (res.IsSuccess === true) {
                    this.notify.success(this.translate.instant("USERINVITED"));
                    this.getInvitedUsers();

                } else {
                    this.notify.error(this.translate.instant("SOMETHINGWRONG"));
                }
            });
    }

    // Update Invitation
    public updateInvite(inviteId: string, data: any) {
        this.api.put('Invites/' + inviteId, data)
            .map(res => res.json())
            .subscribe(res => {
                if (res.IsSuccess === true) {
                    this.notify.success(this.translate.instant("USERDATAUPDATED"));
                    this.getInvitedUsers();
                } else {
                    this.notify.error(this.translate.instant("UPDATEUSERDATAFAILED"));
                }
            });
    }

    // Re-Invite User. (Send another email)
    public reInviteUser(id: string) {
        let orgId = localStorage.getItem('orgId');
        this.api.post('Organisations/' + orgId + '/Invites/' + id + '/Reinvite', '')
            .map(res => res.json())
            .subscribe(res => {
                this.notify.success(this.translate.instant("USERINVITED"));
                console.log(res);
            });
    }

    public reinviteMany(obj: ChangeManyUserSettingsModel) {
        console.log('REINVITEAPI', obj);
        this.api.post('Users/Invites/ReinviteMany', obj)
            .map(res => res.json())
            .subscribe(res => {
                this.notify.success(this.translate.instant("USERINVITED"));
                console.log('REINVITEMANY', res);
            });
    }

    // Gets all invited users.
    public getInvitedUsers() {
        let orgId = localStorage.getItem('orgId');
        this.api.get(`Organisations/${orgId}/Invites`)
            .map(res => res.json())
            .subscribe(res => {
                if (res.IsSuccess) {
                    this.invitedUsers$.next(res.Data);
                    console.log('INVITE', res.Data);
                    console.log("[User] Loaded Invited Users. Found [" + res.Data.length + "] of them.");
                }
                else {
                    console.log("[User] Load Invited users: Failed! -> " + res.Message);
                }
            }, (error) => {
                console.log("[User] Failed to get Invited users!");
            });
    }

    public getInvitedUser(id: string) {
        let invite = this.invitedUsers$.value.find(x => x.Id == id);
        this.user = new AllUsersModel(invite);
        this.user.RoleId = invite.Role;
    }

    // Deletes a invite making the invite Link invalid.
    public deleteInvitedUser(id: string) {
        let orgId = localStorage.getItem('orgId');
        this.api.delete('Organisations/' + orgId + '/RemoveInvite/' + id)
            .map(res => res.json())
            .subscribe(res => {
                if (res.IsSuccess === true) {
                    this.notify.success(this.translate.instant("USERDELETED"));
                    this.getInvitedUsers();
                } else {
                    this.notify.error(this.translate.instant("DELETEFAILED"));
                }
            }, err => {
                this.notify.error(this.translate.instant("DELETEFAILED"));
            });
    }

    // Deletes a invite making the invite Link invalid.
    public deleteInvitedUserMany(obj: ChangeManyUserSettingsModel) {
        this.api.post('Users/RemoveInvitesMany/', obj)
            .map(res => res.json())
            .subscribe(res => {
                if (res.IsSuccess === true) {
                    this.notify.success(this.translate.instant("USERSDELETED"));
                    this.getInvitedUsers();
                } else {
                    this.notify.error(this.translate.instant("DELETEFAILED"));
                }
            }, err => {
                this.notify.error(this.translate.instant("DELETEFAILED"));
            });
    }

    // Updates status on a user.
    public changeStatus(item: any, id: string) {

        item = {
            'OrganisationId': localStorage.getItem('orgId'),
            'Status': item
        };
        this.api.put('Users/' + id + '/ChangeUserOrganisationStatus', item)
            .map(res => res.json())
            .subscribe(res => {
                if (res.IsSuccess === true) {
                    this.notify.success(this.translate.instant("USERDATAUPDATED"));
                    this.getUsers();
                } else {
                    this.notify.error(this.translate.instant("UPDATEUSERDATAFAILED"));
                }

            });
    }

    public changeStatusMany(model: ChangeManyUserSettingsModel) {
        this.api.put('Users/ChangeUserOrganisationStatusMany', model)
            .map(res => res.json())
            .subscribe(res => {
                if (res.IsSuccess === true) {
                    this.notify.success(this.translate.instant("USERDATAUPDATED"));
                    this.getUsers();
                } else {
                    this.notify.error(this.translate.instant("UPDATEUSERDATAFAILED"));
                }

            });
    }

    // Update Serach Parameters
    public UpdateSearchFields(updateArray: any) {
        for (var arr in updateArray) {
            (updateArray[arr] === 'All') ? (<any>this.searchParams)[arr] = '' : (<any>this.searchParams)[arr] = updateArray[arr];
        }
        this.getUsers();
    }

    // Reset Usermodel
    public createUserModel() {
        this.user = new AllUsersModel({ 'FirstName': '', 'LastName': '', 'Email': '', 'OrganisationRole': '' });
    }

    // Generate QueryString from search parameters.
    private getSearchString() {
        var str: Array<any> = [];
        for (var p in this.searchParams)
            if (this.searchParams.hasOwnProperty(p)) {
                if ((<any>this.searchParams)[p] === '' || typeof (<any>this.searchParams)[p] === 'undefined')
                    continue;
                str.push(encodeURIComponent(p) + '=' + encodeURIComponent((<any>this.searchParams)[p]));
            }

        return str.join('&');
    }

    // Clear Search parameters
    private resetSearchParams() {
        this.searchParams = { OrganisationUserStatus: '', onlyActive: false, UserStatus: '', Email: '', Name: '', DateCreatedStart: '', DateCreatedEnd: '', Page: 1, PageSize: 20 };
    }

    public getName(id: string) {
        let userList = this.userList$.value;
        if (!userList || userList.length < 1) {
            return '';
        }
        const obj = this.userList$.value.find(x => x.value === id);
        return obj ? obj.label : '';
    }

    getUserAnswerOptions(callback: (callbackData: any) => any) {
        this.api.get("Users/UserAnswers").map(res => res.json()).subscribe((data) => {
            if (data.IsSuccess) {
                callback(data.Data);
            }
        });
    }

    saveQuestions(questions: any, callback: () => any = null) {
        this.api.post("Users/UserQuestions", questions).map(res => res.json()).subscribe((data) => {
            if (data.IsSuccess) {
                if (callback) {
                    callback();
                }
            }
        });
    }

    ngOnDestroy() {
        if (this.orgSub) this.orgSub.unsubscribe();
        if (this.userSub) this.userSub.unsubscribe();
    }
}


