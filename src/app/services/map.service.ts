import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { AddressModelBilling } from '../models/address.models';

@Injectable()
export class MapService {

    public show = false;
    private addressArray: Array<any>;

    public selectedImage$: Subject<string> = new Subject();
    public selectedImageObservable(): Observable<string> {
        return this.selectedImage$.asObservable();
    }


    constructor() {
    }

    public showLocations(addressArray: Array<any>) {
        this.show = true;
        this.addressArray = addressArray;
    }

    public showLocaton(address: any) {
        this.show = true;
        this.addressArray = [address];
    }

    public hide() {
        this.show = false;
        this.addressArray = [];
    }

    public setGeoCode(address: AddressModelBilling, success: (address: AddressModelBilling) => any) {
        var geocoder = new google.maps.Geocoder();
        let textAdress: string = '';
        if (address) {
            textAdress = address.Street;
            textAdress += " ";
            textAdress += address.State;
            textAdress += " ";
            textAdress += address.PostalCode;
            textAdress += " ";
            textAdress += address.City;
            textAdress += " ";
            textAdress += address.Country;
        }

        geocoder.geocode({
            'address': textAdress
        },
            function (results: any, status: any) {
                if (status == google.maps.GeocoderStatus.OK) {
                    address.Latitude = results[0].geometry.location.lat();
                    address.Longitude = results[0].geometry.location.lng();
                    address.ShowAddress = results[0].formatted_address;
                }
                success(address);
            });
    }
}

export const CollorArray: any = {
    'Billing': 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png',
    'Visiting': 'http://maps.google.com/mapfiles/ms/icons/green-dot.png',
    'Shipping': 'http://maps.google.com/mapfiles/ms/icons/purple-dot.png',
    'Delivery': 'http://maps.google.com/mapfiles/ms/icons/yellow-dot.png',
    'Other': 'http://maps.google.com/mapfiles/ms/icons/red-dot.png'
}