import { Injectable } from '@angular/core';
import { CalendarSettings } from '../models/calendar.models';
import { CalendarEvent } from "../models/calendar.models";
import { Api } from './api';

export interface AuthCodeObject {
    AuthCode: string;
    UserId: string;
    OrganisationId: string;
}

@Injectable()
export class CalendarService {
    constructor(
        private api: Api
    ) { }

    public authModel: any;
    public showAuthModal: any;
    public refreshTokenExpired: boolean;

    syncActions() {
        this.api.post("SyncEvents", {}).map(res => res.json()).subscribe(data => console.log(data));
    }

    authorize(model: AuthCodeObject, provider: string, callback: () => any = null) {
        if (model) {
            switch (provider) {
                case "Microsoft": {
                    this.api.post("Calendar/UserAuthorizedMicrosoft", model).map(res => res.json()).subscribe(data => {
                        if (callback) {
                            callback();
                        }
                    });
                    break;
                }
                case "Google": {
                    this.api.post("Calendar/UserAuthorizedGoogle", model).map(res => res.json()).subscribe(data => {
                        if (callback) {
                            callback();
                        }
                    })
                    break;
                }
            }
        }
    }
    waitForAuthResult(provider: string, callback: (done: boolean, status: boolean) => any = null) {
        this.api.get("Calendar/AuthCompleted/" + provider).map(res => res.json()).subscribe(data => {
            if (data.IsSuccess) {
                this.refreshTokenExpired = false;
            }
            if (callback) callback(data.IsSuccess, data.Data);
        });
    }

    saveSettings(settings: CalendarSettings, callback: (success: boolean) => any = null) {
        this.api.put("Calendar/Settings", settings).map(res => res.json()).subscribe(data => {
            if (callback) callback(data.IsSuccess);
        });
    }

    getSettings(callback: (success: boolean, result: CalendarSettings) => any = null) {
        this.api.get("Calendar/Settings").map(res => res.json()).subscribe(data => {
            if (callback) callback(data.IsSuccess, data.Data);
        })
    }

    getCalendarEvents(year: number, month: number, callback: (data: any) => any = null) {
        this.api.get("Calendar/Events/" + year + "/" + month).map(res => res.json()).subscribe(data => {
            if (data.Data) {
                data.Data = data.Data.map((x: any) => new CalendarEvent(x));
            }
            if (!data.IsSuccess && data.Label == "REFRESHTOKENEXPIRED") {
                this.refreshTokenExpired = true;
                console.log(data.Label);
            }
            if (callback) callback(data);
        });
    }
}