import { Injectable } from '@angular/core';
import { LocalStorage } from './localstorage';

@Injectable()
export class BaseService {


    constructor(
        public localStore: LocalStorage
    ) {
    }

    public clearInputFile(f: any) {
        if (f.value) {
            try {
                f.value = ''; //for IE11, latest Chrome/Firefox/Opera...
            } catch (err) { }
            if (f.value) { //for IE5 ~ IE10
                var form = document.createElement('form'),
                    parentNode = f.parentNode, ref = f.nextSibling;
                form.appendChild(f);
                form.reset();
                parentNode.insertBefore(f, ref);
            }
        }
    }
}
