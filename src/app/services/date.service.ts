import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from './auth-service';
import { Subscription } from 'rxjs/Subscription';
import { LogedInUserModel } from '../models/user.models';
import * as moment from 'moment';

@Injectable()
export class DateService {

    private currentUser: LogedInUserModel;

    private authSub: Subscription;

    constructor(private translate: TranslateService,
        private auth: AuthService) {
        this.authSub = this.auth.currentUserObservable()
            .subscribe(user => {
                if (user) {
                    this.currentUser = user;
                };
            });
    }

    //Default date format is milliseconds
    public getTodaysDate(dateFormat = 'x') {
        return moment().format(dateFormat);
    }

    public addToDate(date: string | number | Date, quantity: number, period: string, dateFormat = 'x'): string | number {
        var momentDate = moment(date);
        switch (period.toLowerCase()) {
            case 'day':
            case 'daily':
                return momentDate.add(quantity, 'day').format(dateFormat);
            case 'week':
            case 'weekly':
                return momentDate.add(quantity, 'week').format(dateFormat);
            case 'month':
            case 'monthly':
                return momentDate.add(quantity, 'month').format(dateFormat);
            case 'quarter':
            case 'quarterly':
                return momentDate.add(quantity, 'quarter').format(dateFormat);
            case 'year':
            case 'yearly':
                return momentDate.add(quantity, 'year').format(dateFormat);
            default:
                return momentDate.format(dateFormat);
        }
    }

    public subtractFromDate(date: string | number | Date, quantity: number, period: string, dateFormat = 'x') {
        var momentDate = moment(date);
        switch (period.toLowerCase()) {
            case 'day':
            case 'daily':
                return momentDate.subtract(quantity, 'day').format(dateFormat);
            case 'week':
            case 'weekly':
                return momentDate.subtract(quantity, 'week').format(dateFormat);
            case 'month':
            case 'monthly':
                return momentDate.subtract(quantity, 'month').format(dateFormat);
            case 'quarter':
            case 'quarterly':
                return momentDate.subtract(quantity, 'quarter').format(dateFormat);
            case 'year':
            case 'yearly':
                return momentDate.subtract(quantity, 'year').format(dateFormat);
            default:
                return momentDate.format(dateFormat);
        }
    }

    getDateStartOfDay(timestamp: number) {
        let date = new Date(timestamp);
        date.setHours(0);
        date.setMinutes(0);
        date.setSeconds(0);
        date.setMilliseconds(0);
        return date.getTime();
    }

    getDateEndOfDay(timestamp: number) {
        let date = new Date(timestamp);
        date.setHours(23);
        date.setMinutes(59);
        date.setSeconds(59);
        date.setMilliseconds(999);
        return date.getTime();
    }

    //Default date format is milliseconds
    public getFirstDateOfPeriod(interval: string, dateFormat = 'x', date?: string | number | Date) {
        let momentDate: any
        if (date) {
            momentDate = moment(date);
        } else {
            momentDate = moment();
        }
        switch (interval.toLowerCase()) {
            case 'day':
            case 'daily':
                return momentDate.startOf('day').format(dateFormat);
            case 'week':
            case 'weekly':
                return momentDate.startOf('isoWeek').format(dateFormat);
            case 'month':
            case 'monthly':
                return momentDate.startOf('month').format(dateFormat);
            case 'quarter':
            case 'quarterly':
                return momentDate.startOf('quarter').format(dateFormat);
            case 'year':
            case 'yearly':
                return momentDate.startOf('year').format(dateFormat);
            default:
                return momentDate.format(dateFormat);
        }
    }

    //Default date format is milliseconds
    public getLastDateOfPeriod(interval: string, dateFormat = 'x', date?: string | number | Date) {
        let momentDate: any
        if (date) {
            momentDate = moment(date);
        } else {
            momentDate = moment();
        }
        switch (interval.toLowerCase()) {
            case 'day':
            case 'daily':
                return momentDate.endOf('day').format(dateFormat);
            case 'week':
            case 'weekly':
                return momentDate.endOf('isoWeek').format(dateFormat);
            case 'month':
            case 'monthly':
                return momentDate.endOf('month').format(dateFormat);
            case 'quarter':
            case 'quarterly':
                return momentDate.endOf('quarter').format(dateFormat);
            case 'year':
            case 'yearly':
                return momentDate.endOf('year').format(dateFormat);
            default:
                return momentDate.format(dateFormat);
        }
    }

    //Default date format is milliseconds
    public getNextDateOfPeriod(date: string | number, interval: string, dateFormat = 'x'): any {
        const momentDate = moment(date);
        switch (interval.toLowerCase()) {
            case 'day':
            case 'daily':
                return this.addToDate(date, 1, 'day', dateFormat);
            case 'week':
            case 'weekly':
                return this.addToDate(date, 1, 'week', dateFormat);
            case 'month':
            case 'monthly':
                return this.addToDate(date, 1, 'month', dateFormat);
            case 'quarter':
            case 'quarterly':
                return this.addToDate(date, 1, 'quarter', dateFormat);
            case 'year':
            case 'yearly':
                return this.addToDate(date, 1, 'year', dateFormat);
            default:
                return momentDate.format(dateFormat);
        }
    }

    //Default date format is milliseconds
    public getPrevDateOfPeriod(date: string | number, interval: string, dateFormat = 'x'): any {
        const momentDate = moment(date);
        switch (interval.toLowerCase()) {
            case 'day':
            case 'daily':
                return this.subtractFromDate(date, 1, 'day', dateFormat);
            case 'week':
            case 'weekly':
                return this.subtractFromDate(date, 1, 'week', dateFormat);
            case 'month':
            case 'monthly':
                return this.subtractFromDate(date, 1, 'month', dateFormat);
            case 'quarter':
            case 'quarterly':
                return this.subtractFromDate(date, 1, 'quarter', dateFormat);
            case 'year':
            case 'yearly':
                return this.subtractFromDate(date, 1, 'year', dateFormat);
            default:
                return momentDate.format(dateFormat);
        }
    }

    public convertStringToTimestamp(date: string) {
        return moment(date).valueOf();
    }

    public convertTimestampToString(timestamp: number, dateFormat: string) {
        return moment(timestamp).format(dateFormat);
    }

    public defaultDateFormat(value?: number | string | Date, dateOnly = false, timeOnly = false, showTimeAgo = false) {
        if (!value) return '';
        let defaultDateFormat: string;
        const date = moment(value);
        const userTime: string = this.currentUser ? (this.currentUser.Is24DateTimeFormat ? '24h' : '12h') : '';
        const userDate: string = ''; // get from service
        let timeFormat = '';
        let dateFormat = '';

        switch (userTime) {
            case '12h':
                timeFormat = 'h:mm a';
                break;
            case '24h':
                timeFormat = 'HH:mm';
                break;
            default:
                timeFormat = 'HH:mm';
                break;
        }

        switch (userDate) {
            case 'dateFirst':
                dateFormat = 'D MMM YYYY';
                break;
            case 'monthFirst':
                dateFormat = 'MMM D YYYY';
                break;
            default:
                dateFormat = 'D MMM YYYY';
                break;
        }

        if (showTimeAgo) {
            const dateNow = moment();
            const date24h = moment().subtract(1, 'day');
            const dateYesterday = moment().subtract(1, 'day').startOf('day');

            if (date.isBetween(date24h, dateNow)) {
                const hour = dateNow.diff(date, 'hour', true);
                if (hour >= 1) {
                    defaultDateFormat = hour === 1 ? this.translate.instant('ONEHOURAGO') : this.translate.instant('HOURSAGO', { hour });
                    return defaultDateFormat;
                } else {
                    const minute = dateNow.diff(date, 'minute', true);
                    defaultDateFormat = minute >= 1 ? (minute === 1 ? this.translate.instant('ONEMINUTEAGO') :
                        this.translate.instant('MINUTESAGO', { minute })) :
                        this.translate.instant('JUSTNOW');
                    return defaultDateFormat;
                }
            } else if (date.isBetween(dateYesterday, dateNow)) {
                defaultDateFormat = this.translate.instant('YESTERDAYAT', { time: date.format(timeFormat) });
                return defaultDateFormat;
            }
        }

        if (timeOnly) {
            defaultDateFormat = date.format(timeFormat);
            return defaultDateFormat;
        }

        if (dateOnly) {
            // Maybe localize Moment instead of hs haxxy solution?
            const month = this.getTranslatedMonth(date.format('MMMM'), 3);
            const replace = date.format('MMM');
            defaultDateFormat = date.format(dateFormat).replace(replace, month);
            return defaultDateFormat;
        }

        // Maybe localize Moment instead of hs haxxy solution?
        const month = this.getTranslatedMonth(date.format('MMMM'), 3);
        const replace = date.format('MMM');
        defaultDateFormat = date.format(`${dateFormat} ${timeFormat}`).replace(replace, month);
        return defaultDateFormat;
    }

    public changeTimeOfDate(timestamp: number, endOfDay?: boolean): number {
        const date = new Date(timestamp);
        if (endOfDay) {
            return new Date(date.getFullYear(), date.getMonth(), date.getDate(), 23, 59, 59).getTime();
        } else {
            return new Date(date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0).getTime();
        }
    }

    private getTranslatedMonth(month: string, substring?: number) {
        if (substring) {
            return this.translate.instant(month.toUpperCase()).substring(0, substring);
        }
        return this.translate.instant(month.toUpperCase());
    }
}