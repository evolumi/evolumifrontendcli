import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ButtonPanelService {

    private command$: Subject<string> = new Subject();
    public commandObservable(): Observable<string> {
        return this.command$.asObservable();
    }

    private toggleClass$: Subject<ClassToggle> = new Subject();
    public toggleClassObservable(): Observable<ClassToggle> {
        return this.toggleClass$.asObservable();
    }

    public headerButtons: Array<PanelButton> = [];
    public panelButtons: Array<PanelButton> = [];

    public isPhone: boolean;
    public compact: boolean = false;
    private screenWidth: number;

    constructor() {
        this.setWidth(window.innerWidth);
    }

    public click(command: string) {
        this.command$.next(command);
    }

    public toggleClass(id: string, className: string, data: string = "", color: string = "") {
        this.toggleClass$.next({ id: id, className: className, data: data, color: "" });
    }


    public addPanelButton(label: string, command: string, icon: string, shortcut: number = 0) {
        if (this.panelButtons.findIndex(x => x.command == command) == -1)
            this.panelButtons.push({ label: label, command: command, icon: icon, shortcut: shortcut, shortcutString: this.shortcutString(shortcut) });
        this.buttonCount();
    }

    public addHeaderButton(label: string, command: string, icon: string, shortcut: number = 0) {
        if (this.headerButtons.findIndex(x => x.command == command) == -1)
            this.headerButtons.push({ label: label, command: command, icon: icon, shortcut: shortcut, shortcutString: this.shortcutString(shortcut) });
        this.buttonCount();
    }

    public removeButtons(ids: string[] = null) {
        ids.forEach(x => this.removeButton(x));
        this.buttonCount();
    }

    public clearPanel() {
        this.panelButtons = [];
    }

    buttonCount() {
        let count = this.headerButtons.length + this.panelButtons.length;
        this.compact = this.screenWidth - (count * 25) < 220;
    }

    shortcutString(keyCode: number): string {
        return keyCode ? " [Alt+" + String.fromCharCode(keyCode) + "]" : "";
    }

    public removeButton(command: string) {
        let index = this.headerButtons.findIndex(x => x.command == command);
        if (index != -1) {
            this.headerButtons.splice(index);
        }
        index = this.panelButtons.findIndex(x => x.command == command);
        if (index != -1) {
            this.panelButtons.splice(index);
        }
        this.buttonCount();
    }

    public setWidth(width: number) {
        this.isPhone = width < 992;
        this.screenWidth = width;
        this.buttonCount();
    }
}

export interface ClassToggle {
    data: string;
    id: string;
    color: string;
    className: string;
}

export interface PanelButton {
    icon: string;
    label: string;
    command: string;
    shortcut: number;
    shortcutString?: string;
}
