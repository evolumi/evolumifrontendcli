import { Injectable } from '@angular/core';
import { Subject } from "rxjs/Subject";
import { Observable } from "rxjs/Observable";
import { BehaviorSubject } from "rxjs/BehaviorSubject";

@Injectable()
export class CelebrationService {

    //Moneyrain
    private showMoneyRain$: Subject<boolean> = new Subject<boolean>();
    moneyRainObserable(): Observable<boolean> {
        return this.showMoneyRain$.asObservable();
    }

    private showFireworks$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    fireworksObserable(): Observable<boolean> {
        return this.showFireworks$.asObservable();
    }

    constructor() {
    }

    startMoneyRain() {
        this.showMoneyRain$.next(true);
    }

    startFireworks() {
        this.showFireworks$.next(true);
    }
}
