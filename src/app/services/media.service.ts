import { Injectable, OnInit } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';

interface MyWindow extends Window {
    _hq: Array<any>;
}

declare var window: MyWindow;

@Injectable()

export class MediaService implements OnInit {

    public selectedImage$: Subject<string> = new Subject();
    public seletedImageObservable(): Observable<string> {
        return this.selectedImage$.asObservable();
    }


    constructor() {
        window._hq = window._hq || [];
    }

    ngOnInit() {

    }

    public showImage(url: string) {
        console.log("[Media] Showing Image: " + url);
        this.selectedImage$.next(url);
    }
}