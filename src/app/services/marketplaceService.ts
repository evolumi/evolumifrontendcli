import { Injectable, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subscriber } from 'rxjs/Subscriber';
import { Api } from './api';
import { LocalStorage } from './localstorage';
import { BaseService } from './baseService';
import { NotificationService } from './notification-service';
import { TranslateService } from '@ngx-translate/core';
import { MarketplaceOrgProfileModel } from '../models/marketplace.models';
import { MarketplaceUserProfileModel } from '../models/marketplace.models';
import { CandidateModel } from '../models/candidate.models';
import { AssignmentFilterModel, AssignmentOrgModel, AssignmentUserModel, AssignmentShortModel } from '../models/assignment.models';
import { PaginationStructure } from '../models/pagination.models';
declare var $: JQueryStatic;

@Injectable()
export class MarketplaceService extends BaseService {

    private _userAssignmentsObserver: Subscriber<Array<AssignmentShortModel>>;

    private _orgProfileObserver: Subscriber<MarketplaceOrgProfileModel>;
    private _userProfileObserver: Subscriber<MarketplaceUserProfileModel>;

    private _candidateObserver: Subscriber<CandidateModel>;
    private _orgAssignmentObserver: Subscriber<AssignmentOrgModel>;
    private _userAssignmentObserver: Subscriber<AssignmentUserModel>;

    private _assignmentsFilter: AssignmentFilterModel = new AssignmentFilterModel("", "", "", []);
    private _allAssignmentsFilter: AssignmentFilterModel = new AssignmentFilterModel("", "", "", []);

    public userAssignments$: Observable<Array<AssignmentShortModel>>;

    public orgProfile$: Observable<MarketplaceOrgProfileModel>;
    public userProfile$: Observable<MarketplaceUserProfileModel>;

    public candidate$: Observable<CandidateModel>;
    public orgAssignment$: Observable<AssignmentOrgModel>;
    public userAssignment$: Observable<AssignmentUserModel>;

    public assignmentCreated: EventEmitter<any>;
    public appliedToAssignment: EventEmitter<any>;

    constructor(
        private _api: Api,
        private _localStore: LocalStorage,
        private _notify: NotificationService,
        private _translate: TranslateService
    ) {
        super(_localStore);

        this.userAssignments$ = new Observable<Array<AssignmentShortModel>>((observer: any) => {
            this._userAssignmentsObserver = observer;
        }).share();

        this.orgProfile$ = new Observable<MarketplaceOrgProfileModel>((observer: any) => {
            this._orgProfileObserver = observer;
        }).share();
        this.userProfile$ = new Observable<MarketplaceUserProfileModel>((observer: any) => {
            this._userProfileObserver = observer;
        }).share();

        this.candidate$ = new Observable<CandidateModel>((observer: any) => {
            this._candidateObserver = observer;
        }).share();
        this.orgAssignment$ = new Observable<AssignmentOrgModel>((observer: any) => {
            this._orgAssignmentObserver = observer;
        }).share();
        this.userAssignment$ = new Observable<AssignmentUserModel>((observer: any) => {
            this._userAssignmentObserver = observer;
        }).share();

        this.assignmentCreated = new EventEmitter<any>();
        this.appliedToAssignment = new EventEmitter<any>();
    }

    private fillFilterParametres(filter: AssignmentFilterModel): string {
        var langs: string = "";
        for (var lang of filter.selectedLanguaes) {
            langs += "selectedLanguaes[]=" + lang + "&"
        }
        var filterString = `?selectedType=${filter.selectedType}&selectedEmployment=${filter.selectedEmployment}&selectedGeography=${filter.selectedGeography}&${langs}`
        return filterString;
    }

    private getOrganisationAssignmentsUrl(): string {
        let orgId = localStorage.getItem("orgId");
        var filterString: string = this.fillFilterParametres(this._assignmentsFilter);
        var url = `/organization/${orgId}/marketplace/assignments` + filterString;
        url += "updated=" + Date.now()
        return url
    }

    private getAllAssignmentsUrl(): string {
        var filterString: string = this.fillFilterParametres(this._allAssignmentsFilter);
        var url = `marketplace/assignments` + filterString;
        url += "updated=" + Date.now()
        return url
    }

    private getUserAppliedAssignmentsUrl(userId: string): string {
        var url = `users/${userId}/marketplace/assignments`;
        url += "?updated=" + Date.now()
        return url
    }

    // assignment for organisation

    public updateAssignmentsFilter(filter: AssignmentFilterModel): void {
        this._assignmentsFilter = filter;
        this.getOrganisationAssignments();
    }

    public getOrganisationAssignments() {
        this._api.refreshPaginationWithUrl(this.getOrganisationAssignmentsUrl());
    }

    public addAssignmentToOrganisation(data: FormData) {
        let orgId = localStorage.getItem("orgId");
        this._api.upload(
            `organization/${orgId}/marketplace/assignments`,
            data)
            .then(response => {
                this._notify.success(this._translate.instant('ASSIGNMENTCREATED'));
                this.assignmentCreated.emit(response);
                this.clearInputFile($('#file-input')[0]);
                // refresh assignments
                this.getOrganisationAssignments();
                this.getOrganisationProfile();
            });
    }

    public getOrganisationAssignment(assignmentId: string) {
        this._api.requestGet(
            `marketplace/assignments/${assignmentId}?forOrganisation=true`,
            (data: AssignmentOrgModel) => {
                this._orgAssignmentObserver.next(data);
            }
        );
    }

    public confirmCandidateToAssignment(candidateId: string, assignmentId: string) {
        this._api.requestPost(
            `marketplace/assignments/${assignmentId}/candidate/${candidateId}/choose`,
            null,
            (data: any) => {
                this.getOrganisationAssignment(assignmentId);
                this._notify.success(this._translate.instant('CANDIDATESAVED'));
            },
            (error) => {
                this._notify.error(error);
            }
        );
    }

    public getCandidate(candidateId: string, assignmentId: string) {
        this._api.requestGet(
            `marketplace/assignments/${assignmentId}/candidate/${candidateId}`,
            (data: CandidateModel) => {
                this._candidateObserver.next(data);
            }
        );
    }

    public closeAssignment(assignmentId: string) {
        this._api.requestPut(
            `marketplace/assignments/${assignmentId}`,
            null,
            () => {
                this._notify.success(this._translate.instant('ASSIGNMENTCLOSED'));
                //for refresh already getted assignment
                this.getOrganisationAssignment(assignmentId);
            },
            (error) => {
                this._notify.error(error);
            }
        );
    }

    // assignment for user

    public updateAllAssignmentsFilter(filter: AssignmentFilterModel): void {
        this._allAssignmentsFilter = filter;
        this.getAllAssignments();
    }

    public getAllAssignments() {
        this._api.refreshPaginationWithUrl(this.getAllAssignmentsUrl());
    }

    public getUserAssignment(assignmentId: string) {
        this._api.requestGet(
            `marketplace/assignments/${assignmentId}?forOrganisation=false`,
            (data: AssignmentUserModel) => {
                this._userAssignmentObserver.next(data);
            }
        );
    }

    public getUserAppliedAssignments(userId: string) {
        this._api.refreshPaginationWithUrl(this.getUserAppliedAssignmentsUrl(userId));
    }

    public getTopUserAppliedAssignments() {
        var userId = this._localStore.get('userId')
        this._api.requestGet(
            `users/${userId}/marketplace/assignments?page=1&pageSize=6`,
            (data: PaginationStructure) => {
                this._userAssignmentsObserver.next(data.records);
            }
        );
    }

    public applyToAssignment(assignmentId: string, data: FormData) {
        this._api.upload(
            `marketplace/assignments/${assignmentId}/apply`,
            data)
            .then(response => {
                this.getUserAssignment(assignmentId);
                this._notify.success(this._translate.instant('APPLIEDTOASSIGNMENT'));
                this.appliedToAssignment.emit(response);
                this.clearInputFile($('#file-input')[0]);
                // refresh assignments
            });
    }

    // org profile

    public getOrganisationProfile() {
        let orgId = localStorage.getItem("orgId");
        this._api.requestGet(
            `organization/${orgId}/marketplace/profile`,
            (data: MarketplaceOrgProfileModel) => {
                if (this._orgProfileObserver != null)
                    this._orgProfileObserver.next(data);
            }
        );
    }

    public uploadOrganisationProfilePicture(data: FormData) {
        let orgId = localStorage.getItem("orgId");
        this._api.upload(
            `organization/${orgId}/marketplace/profile/logo`,
            data)
            .then(response => {
                this._notify.success(this._translate.instant('PROFILEPICTURECHANGED'));
                this.clearInputFile($('#file-input-profile')[0]);
                //for update profilePictureUurl
                this.getOrganisationProfile();
            });
    }

    public saveOrganisationProfile(data: any) {
        let orgId = localStorage.getItem("orgId");
        this._api.requestPut(
            `organization/${orgId}/marketplace/profile`,
            data,
            () => {
                this._notify.success(this._translate.instant('PROFILESAVED'));
                //for refresh already getted profile
                //this.getOrganisationProfile();
            },
            (error) => {
                this._notify.error(error);
            }
        );
    }

    // user profile

    public getUserProfile(userId: string) {
        //need to be implement on server
        this._api.requestGet(
            `users/${userId}/marketplace/profile`,
            (data: MarketplaceUserProfileModel) => {
                this._userProfileObserver.next(data);
            }
        );
    }

    public uploadUserProfilePicture(userId: string, data: FormData) {
        //need to be implement on server
        this._api.upload(
            `users/${userId}/marketplace/profile/logo`,
            data)
            .then(response => {
                this._notify.success(this._translate.instant('PROFILEPICTURECHANGED'));
                this.clearInputFile($('#file-input-profile')[0]);
                //for update profilePictureUurl
                this.getUserProfile(userId);
            });
    }

    public saveUserProfile(userId: string, data: FormData) {
        //need to be implement on server
        this._api.upload(
            `users/${userId}/marketplace/profile`,
            data)
            .then(response => {
                this._notify.success(this._translate.instant('PROFILESAVED'));
                //for refresh already getted profile
                this.getUserProfile(userId);
            });

    }
}
