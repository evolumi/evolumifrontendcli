import { Injectable } from '@angular/core';
import { Api } from './api';
import { NotificationService } from './notification-service';
import { LocalStorage } from './localstorage';
import { DealsModel } from '../models/deals.models';

@Injectable()

export class DealsBlockService {

	public accountId: string = '';
	public prerenderd: any;
	public deals: Array<DealsModel> = new Array<DealsModel>();
	constructor(public _api: Api, public _localstore: LocalStorage, public _notify: NotificationService) { }

	getDealsByAccount(accountId: any) {
		console.log("[Deal-Box] Loading deals for account[" + accountId + "]");
		this.accountId = accountId;
		this._api.get('Accounts/' + accountId + '/Deals')
			.map(res => res.json())
			.subscribe(res => {
				if (res.IsSuccess === true) {
					this.deals = [];
					for (let a of res.Data) {
						this.deals.push(new DealsModel(a));
					}
				}

			})
	}

	deleteDeal(dealId: any) {
		this._api.delete('Deals/' + dealId)
			.map(res => res.json())
			.subscribe(res => {
				this._notify.success('Deal deleted');
				this.getDealsByAccount(this.accountId);
			});
	}
}