import { Injectable } from '@angular/core';
import { Api } from './api';
import { LocalStorage } from './localstorage';
import { ContactsCollectionModel } from '../models/contact.models';
import { FilterCollection } from './filterservice';
import { NotificationService } from './notification-service';
import { TranslateService } from '@ngx-translate/core';

import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { IntercomService } from '../services/interecomService';
import { ChosenOptionModel } from '../modules/shared/components/chosen/chosen.models';

export interface searchParams {
    Page: string,
    PageSize: string
    [key: string]: any
}

@Injectable()

export class ContactsCollection {

    private listLoaded: boolean;

    public searchParams: searchParams;
    public queryString: string;

    public contacts: Array<ContactsCollectionModel> = new Array<ContactsCollectionModel>();

    public contactByAccount: Array<ContactsCollectionModel> = new Array<ContactsCollectionModel>();

    private contactsByAccountList$: BehaviorSubject<Array<ChosenOptionModel>> = new BehaviorSubject([]);
    public contactsByAccountListObservable(): Observable<Array<ChosenOptionModel>> {
        return this.contactsByAccountList$.asObservable();
    }

    public contactData: ContactsCollectionModel = new ContactsCollectionModel();

    private currentContact$: BehaviorSubject<ContactsCollectionModel> = new BehaviorSubject(new ContactsCollectionModel());
    public currentContactObservable(): Observable<ContactsCollectionModel> {
        return this.currentContact$.asObservable();
    }

    private contactByOrganisation$: BehaviorSubject<Array<ContactsCollectionModel>> = new BehaviorSubject<Array<ContactsCollectionModel>>([]);
    public contactByOrganisationObs(): Observable<Array<ContactsCollectionModel>> {
        return this.contactByOrganisation$.asObservable();
    }

    constructor(public intercomService: IntercomService,
        public api: Api,
        private notes: NotificationService,
        private translate: TranslateService,
        public _store: LocalStorage,
        private notify: NotificationService,
        public _filter: FilterCollection) {
        this.searchParams = { Page: '1', PageSize: '' };

        this.getContacts();
    }

    reset() {
        this.searchParams = { Page: '1', PageSize: '' };
    }

    getSearchString() {
        var str: Array<any> = [];
        for (var p in this.searchParams)
            if (this.searchParams.hasOwnProperty(p)) {
                if (this.searchParams[p] === '' || this.searchParams[p] === 'All' || typeof this.searchParams[p] === 'undefined')
                    continue;
                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(this.searchParams[p]));
            }
        return str.join("&");

    }

    getContacts() {
        let orgId = this._store.get('orgId');
        let query = this.getSearchString();
        this.api.get(`Organisations/${orgId}/Contacts?${query}`)
            .map(res => res.json())
            .subscribe(res => {
                this.contacts = []
                for (var contact of res.Data) {
                    this.contacts = [...this.contacts, new ContactsCollectionModel(contact)]
                }
                this.contactByOrganisation$.next(this.contacts);
                this.listLoaded = true;
            });
    }

    getContactByAccountId(accountId = '', contactId = '') {
        console.log("[Contacts] Getting contacts for account [" + accountId + "]");
        this.api.get(`Accounts/${accountId}/Contacts`)
            .map(res => res.json())
            .subscribe(res => {
                this.contactByAccount = [];
                for (var contact of res.Data) {
                    this.contactByAccount = [...this.contactByAccount, new ContactsCollectionModel(contact)]
                }
                if (contactId) {
                    this.getContact(contactId);
                }
            });
    }

    getContactForAccountId(accountId = ''): Observable<Array<ContactsCollectionModel>> {
        console.log("[Contacts] Getting contacts for account [" + accountId + "]");
        return this.api.get(`Accounts/${accountId}/Contacts`)
            .map(res => {
                let response = res.json();
                return response.Data;
            });
    }

    getContactListByAccountId(accountId = '') {
        console.log("[Contacts] Getting contact list for account [" + accountId + "]");
        this.api.get(`Accounts/${accountId}/Contacts`)
            .map(res => res.json())
            .subscribe(res => {
                let list: Array<any> = [];
                for (var contact of res.Data) {
                    list.push(new ChosenOptionModel(contact.Id, contact.FullName));
                }
                this.contactsByAccountList$.next(list);
            });
    }

    addContact() {
        console.log("[Contact] Adding Contact: Start!");
        let orgId = this._store.get('orgId');
        return this.api.post('Organisations/' + orgId + '/Contacts', this.contactData)
            .map(res => {
                var response = res.json();

                if (response.IsSuccess) {
                    console.log("[Contact] Adding Contact: Success!");
                    this.notify.success(this.translate.instant("CONTACTCREATED"));
                    this.intercomService.trackEvent('contacts-created');
                    this.getContactByAccountId(this.contactData.AccountId);
                    this._filter.changeContact();
                }
                else {
                    console.log("[Contact] Adding Contact: Fail! -> " + response.Message);
                }
                return response;
            });
    }

    //create an model 
    createContactModel(contact: ContactsCollectionModel = null) {
        this.contactData = contact ? contact : new ContactsCollectionModel();
    }

    //cancel edit contact
    cancelEditContact() {
        this.contactData = new ContactsCollectionModel(this.currentContact$.value);
    }

    //get contact

    public getContact(id: string, success: (res: ContactsCollectionModel) => any = null) { //
        console.log("[Contacts] Loading Contact [" + id + "] : Started!");

        // Do nothing if already set. 
        if (this.currentContact$.value.Id == id) {
            console.log("[Contacts] Contact already loaded -> Doing nothing ");
            if (success) success(this.currentContact$.value);
            return;
        }

        // Check if contact already loaded.
        this.contactByAccount.forEach(contact => {
            if (contact.Id == id) {
                console.log("[Contacts] Loading Contact [" + id + "] : Success! -> Loaded from Account-contactList");
                this.setContact(contact);
                if (success) success(contact);
            }
        });

        if (this.currentContact$.value.Id != id) {
            this.contacts.forEach(contact => {
                if (contact.Id == id) {
                    console.log("[Contacts] Loading Contact [" + id + "] : Success! -> Loaded from organisation ContactList");
                    this.setContact(contact);
                    if (success) success(contact);
                }
            });
        }

        // If not found get it from API. 
        if (this.currentContact$.value.Id != id) {
            this.api.get(`Contacts/` + id)
                .map(res => res.json())
                .subscribe(res => {
                    if (res.IsSuccess) {
                        console.log("[Contacts] Loading Contact [" + id + "] : Success! -> Loaded from API");
                        if (success) success(res.Data);
                        this.setContact(res.Data);
                    }
                    else {
                        console.log("[Contacts] Loading Contact [" + id + "] : Failed! -> " + res.Message);
                    }
                });
        };
    }

    private setContact(contact: any) {
        this.contactData = new ContactsCollectionModel(contact);
        this.currentContact$.next(contact);
        console.log("[Contacts] Setting Contact: " + this.contactData.FirstName + " " + this.contactData.LastName);
    }

    //update contact
    updateContact() {
        this.api.put('Contacts/' + this.contactData.Id, this.contactData)
            .map(res => res.json())
            .subscribe(a => {
                if (a.IsSuccess === true) {
                    this.currentContact$.next(this.contactData);
                    this.getContactByAccountId(this.currentContact$.value.AccountId);
                    this._filter.changeContact();
                    this.notes.success(this.translate.instant('CONTACTUPDATED'));
                }
            });
    }

    deleteContact(contId: string) {
        this.api.delete('Contacts/' + contId)
            .map(res => res.json())
            .subscribe(res => {
                if (res.IsSuccess === true) {
                    let ind = this.contactByAccount.findIndex(x => x.Id === contId);
                    (<any>this.contactByAccount).splice(ind, 1);
                    this._filter.changeContact();
                    this.notes.success(this.translate.instant('CONTACTDELETED'));
                }
            });
    }

    public getName(id: string): string {
        let contactList = this.contactByOrganisation$.value;
        if (!contactList || contactList.length < 1) {
            return '';
        }
        const obj = this.contactByOrganisation$.value.find(x => x.Id === id);
        return obj ? obj.FullName : '';
    }
}



