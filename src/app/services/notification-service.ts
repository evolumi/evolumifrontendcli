import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { Subject } from "rxjs/Subject";

export class NotificationService {

	private notifications: BehaviorSubject<Array<string>> = new BehaviorSubject([]);

	public collection$(): Observable<Array<string>> {
		return this.notifications.asObservable();
	}

	private information$: Subject<Information> = new Subject();
	public informationObservable(): Observable<Information> {
		return this.information$.asObservable();
	}

	//private _collectionObserver: any;
	private _collection: any = [];

	constructor() {
		//this._collection = [];
		//this.collection$ = new Observable(observer => {
		//	this._collectionObserver = observer;
		//}).share();
	}
	load() {
		this.notifications.next(this._collection);
	}
	success(value: string) {
		//this._collection = [];
		this._collection.push({ class: 'success', msg: value });
		this.notifications.next(this._collection);
		this.remove();
	}
	error(value: string) {
		//this._collection = [];
		this._collection.push({ class: 'error', msg: value });
		this.notifications.next(this._collection);
		this.remove();
	}
	clear() {
		this._collection = [];
	}
	remove() {
		setTimeout(() => {
			this._collection.splice(0, 1);
			this.notifications.next(this._collection);
		}, 5000);
	}

	public inform(title: string, message: string, linktext: string = "", link: string = "") {
		var info = new Information(title, message, linktext, link);
		this.information$.next(info);
	}

	public urgent(title: string, message: string, linktext: string = "", link: string = "") {
		var info = new Information(title, message, linktext, link);
		info.urgent = true;
		this.information$.next(info);
	}
}

export class Information {
	public title: string;
	public message: string;
	public linktext: string;
	public link: string;
	public urgent: boolean = false;

	constructor(title: string, message: string, linktext: string, link: string) {
		this.title = title;
		this.message = message;
		this.linktext = linktext;
		this.link = link;
	}
}
