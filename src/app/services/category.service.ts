import { Injectable } from '@angular/core';
import { OrgService } from './organisation.service';
import { Api } from './api';
import { NotificationService } from './notification-service';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs/Subscription';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { CategoryGroup } from '../models/category.models';
import { OrganisationModel } from "../models/organisation.models";
import { Observable } from "rxjs/Observable";

@Injectable()
export class CategoryService {

    private org: OrganisationModel;

    private currentCategoryGroup$ = new BehaviorSubject<CategoryGroup>(null);
    public currentCategoryGroupObs(): Observable<CategoryGroup> {
        return this.currentCategoryGroup$.asObservable();
    }

    private contractsCategoryList$ = new BehaviorSubject<Array<CategoryGroup>>(null);
    public contractsCategoryListObs() {
        return this.contractsCategoryList$.asObservable();
    }

    private productsCategoryList$ = new BehaviorSubject<Array<CategoryGroup>>(null);
    public productsCategoryListObs() {
        return this.productsCategoryList$.asObservable();
    }

    private orgSub: Subscription;

    constructor(private orgSvc: OrgService,
        private api: Api,
        private translate: TranslateService,
        private notify: NotificationService) {
        this.orgSub = this.orgSvc.currentOrganisationObservable()
            .subscribe(org => {
                if (org) {
                    this.org = org;
                    if (org.CategoryGroups && org.CategoryGroups.length) {
                        let categoryGroups = this.org.CategoryGroups.reduce((res: any, curr: CategoryGroup) => {
                            if (!res.contracts) {
                                res.contracts = [];
                                res.products = [];
                            }
                            switch (curr.Type) {
                                case 'Contracts':
                                    res.contracts.push(curr);
                                    break;
                                case 'Products':
                                    res.products.push(curr);
                                    break;
                                default:
                                    break;
                            }
                            return res;
                        }, {});

                        this.contractsCategoryList$.next(categoryGroups.contracts);
                        this.productsCategoryList$.next(categoryGroups.products);
                    }
                }
            });
    }

    getCategoryGroup(id: string) {
        this.currentCategoryGroup$.next(this.org.CategoryGroups.find(x => x.Id === id));
    }

    addCategoryGroup(catGroup: CategoryGroup, success?: Function, fail?: Function) {
        this.api.post('Organisations/CategoryGroup', catGroup)
            .map(res => res.json())
            .subscribe(resJson => {
                if (resJson.IsSuccess) {
                    if (this.org && this.org.CategoryGroups && !this.org.CategoryGroups.length) {
                        this.org.CategoryGroups = [];
                    }
                    this.org.CategoryGroups.push(resJson.Data)
                    this.orgSvc.publishOrganisation(this.org);
                    this.notify.success(this.translate.instant('CREATECATGORYGROUPSUCCESS'))
                    if (success) success();
                } else {
                    this.notify.error(this.translate.instant('CREATECATEGORYGROUPFAIL'));
                    if (fail) fail();
                }
            }, err => {
                this.notify.error(this.translate.instant('CREATECATEGORYGROUPFAIL'));
                if (fail) fail();
            });
    }

    updateCategoryGroup(catGroup: CategoryGroup, success?: Function, fail?: Function) {
        this.api.put('Organisations/CategoryGroup', catGroup)
            .map(res => res.json())
            .subscribe(resJson => {
                if (resJson.IsSuccess) {
                    this.org.CategoryGroups = this.org.CategoryGroups.map(x => x.Id === resJson.Data.Id ? resJson.Data : x)
                    this.orgSvc.publishOrganisation(this.org);
                    this.notify.success(this.translate.instant('UPDATECATGORYGROUPSUCCESS'))
                    if (success) success();
                } else {
                    this.notify.error(this.translate.instant('UPDATECATEGORYGROUPFAIL'));
                    if (fail) fail();
                }
            }, err => {
                this.notify.error(this.translate.instant('UPDATECATEGORYGROUPFAIL'));
                if (fail) fail();
            });
    }

    deleteCategoryGroup(id: string, success?: Function, fail?: Function) {
        this.api.delete(`Organisations/CategoryGroup/${id}`)
            .map(res => res.json())
            .subscribe(resJson => {
                if (resJson.IsSuccess) {
                    this.org.CategoryGroups = this.org.CategoryGroups.filter(x => x.Id !== id)
                    this.orgSvc.publishOrganisation(this.org);
                    this.notify.success(this.translate.instant('CATEGORYGROUPDELETESUCCESS'))
                    if (success) success();
                } else {
                    this.notify.error(this.translate.instant('CATEGORYGROUPDELETEFAIL'));
                    if (fail) fail();
                }
            }, err => {
                this.notify.error(this.translate.instant('CATEGORYGROUPDELETEFAIL'));
                if (fail) fail();
            });
    }

    openEditCreateCategoryGroup(id?: string) {
        if (id) {
            this.getCategoryGroup(id);
        } else {
            this.currentCategoryGroup$.next(new CategoryGroup({}));
        }
    }

    closeEditCreateCategoryGroup() {
        this.currentCategoryGroup$.next(null);
    }

}