﻿import { DocumentModel } from './document.models';
import { PriceList } from "../services/products.service";
import { FileModel } from './file.models';

export class AccountsModel {
    public Id: string;
    public FullName: string;
    public CompanyNumber: string;
    public Website: string;
    public Email: string;
    public Phone: string;
    public Vat: string;
    public AccountTypeId: string;
    public ParentAccount: string;
    public RelatedAccounts: Array<any>;
    public RelatedAccountsLoaded: boolean = false;
    public AccountManagerId: string;
    public AccountManagerName: string;
    public ParentAccountName: string;
    public AccountTypeName: string;
    public AccountTypeCollor: string;
    public Status: string;
    public ShownName: string;
    public OrganisationId: string
    public Address: any;
    public Tags: Array<string>;
    public Deals: any;
    public Actions: any;
    public Contacts: any;
    public IsChecked: boolean;
    public Files: Array<any>;
    public Documents: Array<DocumentModel>;
    public PriceList: PriceList;
    public PriceListId: string;
    public FirstName: string;
    public LastName: string;
    public IsPerson: boolean;
    public ContactAccounts: string[] = [];
    public FinancialStatements: Array<FinancialStatement>;
    public RelationData: Relation[];
    public ReferredBy: string;
    public CustomData: any = {};
    public ReferdByAccountName : string;
    [key: string]: any;

    constructor(account = { ShownName: '', FullName: '', CompanyNumber: '', Website: '', Email: '', Phone: '', Vat: '', AccountTypeId: '', ParentAccount: '', AccountManagerId: '', IsChecked: false, Files: new Array<FileModel>(), CustomData: {} }) {
        for (var key in account) {
            this[key] = (<any>account)[key];
        }

        if (!this.CustomData)
            this.CustomData = {};
    }
}

export class Relation {
    public AccountId: string;
    public Email: string;
    public Note: string;
    public Phone: string;
    public Title: string;
    public Tags: string[];
    public CustomData: any = {};
    [key: string]: any;

    constructor(obj: any) {
        for (let key in obj) {
            if (obj.hasOwnProperty(key)) {
                this[key] = obj[key]
            }
        }
        if(!this.CustomData){
            this.CustomData = {};
        }

    }
}
export class FinancialStatement {
    Employee: string;
    Revenue: string;
    Year: string;
    CurrencyName: any;
    Currency: any;
}

export class AccountRelation {
    public Id: string;
    public CompanyId: string;
    public PersonId: string;
    public Phone: string;
    public Email: string;
    public Title: string;
    public Note: string;
    public Tags: string[];
    public FirstName: string;
    public LastName: string;
    public PersonColor: string;
    public CompanyName: string;
    public CompanyColor: string;
    public currentAccount: any;
    public value: any;
    public CustomData: any;

    [key:string]: any;

    constructor(obj: any = null){
        for(let key in obj){
                this[key] = obj[key];
        }
    }
}

export class AccountInfoModel {
    public fullName: string;
    public shownName: string;
    public vat: string;
    public accCompanyId: string;
    public managerId: string;
    public typeId: string;
    public phone: string;
    public email: string;
    public website: string;
    public tags: string;
    public isPerson: boolean;

    constructor(fullName: string, shownName: string, vat: string, managerId: string, typeId: string, phone: string, email: string, website: string, accCompanyId : string, tags:string, isPerson: boolean) {
        this.fullName = fullName;
        this.shownName = shownName;
        this.vat = vat;
        this.managerId = managerId;
        this.typeId = typeId;
        this.phone = phone;
        this.email = email;
        this.website = website;
        this.accCompanyId = accCompanyId;
        this.tags = tags;
        this.isPerson = isPerson;
    }
}

export class AccountModel {
    public id: string;
    public shownName: string;
    constructor(id: string, shownName: string) {
        this.id = id;
        this.shownName = shownName;
    }
}