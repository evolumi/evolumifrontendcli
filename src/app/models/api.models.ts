﻿export class ResponseModel {
    public isSuccess: boolean;
    public message: string;
    public data: any;
}