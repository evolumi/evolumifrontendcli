import { FileModel } from './file.models';
import { CategoryGroup } from './category.models';

export class ContractsModel {
    Id: string;
    Name: string;
    StartDate: number | Date;
    EndDate: number | Date;
    MilestoneDates: Array<MilestoneDate> = [];
    OurParties: Array<PartyModel> = [];
    TheirParties: Array<PartyModel> = [];
    Categories: Array<ContractCategory> = [];
    Notes: string;
    AccountId: string;
    Files: Array<FileModel> = [];
    IsChecked: boolean;
    NextDate?: NextDateModel;
    [key: string]: any;

    constructor(contract: any) {
        for (var key in contract) {
            this[key] = (<any>contract)[key];
        }
    }
}

export class MilestoneDate {
    public Name: string;
    public Description: string;
    public Date: number;
    public ReminderDate: number;
    [key: string]: any;

    constructor(date: any) {
        for (var key in date) {
            this[key] = (<any>date)[key];
        }
    }
}

export class PartyModel {
    public Id: string;
    public AccountId: string;
    public Contacts: Array<{AccountId: string, DisplayName: string}> = [];
    public UserId: string;
    public FirstName: string;
    public LastName: string;
    public Email: string;
    public Phone: string;
    public PersonalNo: string;
    public CompanyName: string;
    public CompanyOrgNo: string;
    public Status: string;
    public AllowedSignTypes: Array<string>;
    public DateSigned: number;
    public ProfilePictureUrl: string;
    public DisplayName: string; //Only frontend
    public IsPerson: boolean;
    [key: string]: any;

    constructor(party: any) {
        for (var key in party) {
            this[key] = (<any>party)[key];
        }
    }
}

export class ContractCategory {
    Id: string;
    Name: string;
    Color?: string;

    constructor(cat: any) {
        Object.assign(this, cat);
    }
}

export class NextDateModel {
    EndDate?: number;
    MilestoneDate?: MilestoneDate;
    [key: string]: any;

    constructor(date: any) {
        for (var key in date) {
            this[key] = (<any>date)[key];
        }
    }
}