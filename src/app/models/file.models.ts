﻿export class FileModel {
    Id: string;
    Name: string;
    Location: string;
    Size: number;
    ContentType: string;
    AccountId: string;
    DeleteTime: number;
    IsTemp: boolean; // Only frontend
    [key: string]: any;

    constructor(file: any) {
        for (var key in file) {
            this[key] = (<any>file)[key];
        }
    }
}