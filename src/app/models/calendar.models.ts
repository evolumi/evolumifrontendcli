export class CalendarDay {
    Date: number;
    WeekDay: string;
    ThisMonth: boolean;
    Today: boolean;
    Expanded: boolean;
    Events: CalendarEvent[];
    [key: string]:any;

    constructor(obj: any = null) {
        for(let key in obj) {
            this.Events = [];
            if(obj.hasOwnProperty(key)) {
                this[key] = obj[key]
            }
        }
    }
}

export class CalendarEvent {
    Subject: string;
    Body: string;
    Attendees: Attendee[]
    Start: Date;
    End: Date;
    Location: string;
    ActionId: string;
    Provider: string;

    [key: string]: any;

    constructor(obj: any = null) {
        for (let key in obj) {
            if (obj.hasOwnProperty(key)) {
                if ((key == "Start" || key == "End") && obj[key] != null) {
                    this[key] = new Date(obj[key]);
                }
                else {
                    this[key] = obj[key]
                }
            }
        }
    }
}

export class Attendee {
    EmailAddress: string;
    Status: string;
    [key: string]: any;

    constructor(obj: any = null) {
        for (let key in obj) {
            if (obj.hasOwnProperty(key)) {
                this[key] = obj[key]
            }
        }
    }
}

export class CalendarSettings {
    public UseCalendarSync: boolean;
    public ProvidersToUse: any[] = [];
    public SyncRules : any[];
    public Office365Email: string;
    public GoogleEmail: string;
    [key: string]:any;

    constructor(obj: any = null) {
        this.ActionTagsToUse = [];
        this.ProvidersToUse = [];
        this.ActionTypesToSync = [];
        this.SyncRules = [];
        
        for(let key in obj) {
            if(obj.hasOwnProperty(key)) {
                this[key] = obj[key]
            }
        }
    }
}