import { FileModel } from './file.models';

export class TemplateModel {
    public Id: string;
    public Name: string;
    public Styles: { [key: string]: string } = {};
    public LinkStyles: { [key: string]: string } = {};
    public MobileStyles: { [key: string]: string } = {};
    public ContentWidth: number;
    public ContentBackground: string;
    public Rows: Array<TemplateRowModel> = [];
    public Type: string; // enum backend

    [key: string]: any;

    constructor(obj: any = null) {
        for (let key in obj) {
            if (obj.hasOwnProperty(key)) {
                this[key] = obj[key]
            }
        }
    }
}

export class TemplateDocumentModel extends TemplateModel {
    public HeaderRows: Array<TemplateRowModel> = [];
    public FooterRows: Array<TemplateRowModel> = [];
}

export class TemplateRowModel {
    public TemplateId: string;
    public Styles: { [key: string]: string } = {};
    public Columns: Array<TemplateColumnModel> = [];
    public ContentBackground: string;

    [key: string]: any;

    constructor(obj: any = null) {
        for (let key in obj) {
            if (obj.hasOwnProperty(key)) {
                this[key] = obj[key]
            }
        }
    }
}

export class TemplateColumnModel {
    public TemplateId: string;
    public Styles: { [key: string]: string } = {};
    public Blocks: Array<TemplateBlockModel> = [];
    public WidthRatio: number;

    [key: string]: any;

    constructor(obj: any = null) {
        for (let key in obj) {
            if (obj.hasOwnProperty(key)) {
                this[key] = obj[key]
            }
        }
    }
}

export class TemplateBlockModel {
    public TemplateId: string = '';
    public Styles: { [key: string]: string } = {};
    public Type: string;

    [key: string]: any;
}

export interface ITemplateBlockModel {
    TemplateId: string;
    Styles: { [key: string]: string };
    Type: string;

    [key: string]: any;
}

export class TemplateBlockText extends TemplateBlockModel {
    public Content: string;
    public LinkStyles: { [key: string]: string } = {};
    public LinkContent: string; // to use to convert the links in the builder

    [key: string]: any;

    constructor(obj: any = null) {
        super();
        for (let key in obj) {
            if (obj.hasOwnProperty(key)) {
                this[key] = obj[key]
            }
        }
        this.Type = 'TextBlock';
    }
}

export class TemplateBlockButton extends TemplateBlockModel {
    public Content: string;
    public Url: string;
    public ContentStyles: { [key: string]: string } = {};
    public Width: string;

    [key: string]: any;

    constructor(obj: any = null) {
        super();
        for (let key in obj) {
            if (obj.hasOwnProperty(key)) {
                this[key] = obj[key]
            }
        }
        this.Type = 'Button';
    }
}

export class TemplateBlockImage extends TemplateBlockModel {
    public ImageFile: FileModel;
    public AltText: string;
    public Width: string;
    public Height: string;
    public LinkUrl: string;
    public BlobUrl: string;

    [key: string]: any;

    constructor(obj: any = null) {
        super();
        for (let key in obj) {
            if (obj.hasOwnProperty(key)) {
                this[key] = obj[key]
            }
        }
        this.Type = 'Image';
    }
}

export class TemplateBlockDivider extends TemplateBlockModel {
    public ContentStyles: { [key: string]: string } = {};

    [key: string]: any;

    constructor(obj: any = null) {
        super();
        for (let key in obj) {
            if (obj.hasOwnProperty(key)) {
                this[key] = obj[key]
            }
        }
        this.Type = 'Divider';
    }
}

export class TemplateBlockSocial extends TemplateBlockModel {
    public Icons: Array<TemplateSocialMediaIcon> = [];
    public Spacing: string;
    public IconCollectionId: string;

    [key: string]: any;

    constructor(obj: any = null) {
        super();
        for (let key in obj) {
            if (obj.hasOwnProperty(key)) {
                this[key] = obj[key]
            }
        }
        this.Type = 'Social';
    }
}

export interface TemplateSocialMediaIcon {
    Type: string;
    AltText: string;
    LinkUrl: string;
    ImgSrc: string;

    [key: string]: any;
}

export class TemplateBlockUnsubscribe extends TemplateBlockModel {
    public Content: string;
    public LinkStyles: { [key: string]: string } = {};

    [key: string]: any;

    constructor(obj: any = null) {
        super();
        for (let key in obj) {
            if (obj.hasOwnProperty(key)) {
                this[key] = obj[key]
            }
        }
        this.Type = 'Unsubscribe';
    }
}

export class TemplateBlockProduct extends TemplateBlockModel {

    public Content: string;
    public ProductStyles: ProductTableStyles = { TableStyles: {}, HeaderRowStyles: {}, RowStyles: {}, TotalTableStyles: {}, LeftColumnStyles: {}, RightColumnStyles: {}, InfoStyles: {}, TableAlign: 'left', TotalTableAlign: 'right' };
    public Columns: Array<TemplateProductValues> = [];
    public SumRows: Array<TemplateProductValues> = [];

    [key: string]: any;

    constructor(obj: any = null) {
        super();
        for (let key in obj) {
            if (obj.hasOwnProperty(key)) {
                this[key] = obj[key]
            }
        }
        this.Type = 'Product';
    }
}

export interface ProductTableStyles {
    TableStyles: { [key: string]: string }
    HeaderRowStyles: { [key: string]: string }
    RowStyles: { [key: string]: string }
    TotalTableStyles: { [key: string]: string }
    LeftColumnStyles: { [key: string]: string }
    RightColumnStyles: { [key: string]: string }
    InfoStyles: { [key: string]: string }
    TableAlign: string;
    TotalTableAlign: string;
}

export interface TemplateProductValues {
    Title: string;
    Fields: Array<string>;
    WidthRatio?: number;
    CellAlign?: string;
    Styles: { [key: string]: string }
}

export class TemplateBlockAddress extends TemplateBlockModel {

    public AddressBlock: AddressExtra = { BindTo: '', AddressType: '', Rows: [], ContentStyles: {} };

    [key: string]: any;

    constructor(obj: any = null) {
        super();
        for (let key in obj) {
            if (obj.hasOwnProperty(key)) {
                this[key] = obj[key]
            }
        }
        this.Type = 'Address';
    }
}

export interface AddressExtra {
    BindTo: string;
    AddressType: string;
    Rows: Array<AddressRow>;
    ContentStyles: { [key: string]: string };
}

export interface AddressRow {
    IsManual: boolean;
    Field?: string;
    Fields?: Array<string>;
    Styles: { [key: string]: string }
}

export class TemplateBlockTable extends TemplateBlockModel {

    public TableBlock: TableExtra
    public Columns: number;
    public Rows: number;

    [key: string]: any;

    constructor(obj: any = null) {
        super();
        for (let key in obj) {
            if (obj.hasOwnProperty(key)) {
                this[key] = obj[key]
            }
        }
        this.Type = 'Table';
    }
}

export interface TableExtra {
    ContentStyles: { [key: string]: string }
    Columns: number;
    Rows: number;
    Cells: Array<Array<TableCell>>;
    RowStyles: Array<{ [key: string]: string }>;
    ColumnStyles: Array<{ [key: string]: string }>;
}

export interface TableColumn {
    Styles: { [key: string]: string }
}

export interface TableRow {
    Styles: { [key: string]: string }
}

export interface TableCell { // could be just string but for future maybe we need object
    Content: string;
}