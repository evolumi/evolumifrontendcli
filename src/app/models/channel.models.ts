﻿export class ChannelTypes {
    static channel: string = "Channel";
    static publicWorkGroup: string = "PublicWorkGroup";
    static privateWorkGroup: string = "PrivateWorkGroup";
}