﻿export class AllUsersModel {
    public Id: string;
    public FirstName: string;
    public LastName: string;
    public FullName: string;
    public Email: string;
    public Status: string;
    public OrganisationRole: string;
    public HasImage: boolean;
    public DateLastLogin: Date;
    public DateCreated: Date;
    public DateActivate: Date;
    public DateInactivate: Date;
    public Language: string;
    public RoleId: string;
    public ProfilePictureUrl: string;
    public OrganisationStatus: string;
    public IsChecked: boolean;
    [key: string]: any;
    constructor(user: any) {
        for (var key in user) {
            this[key] = user[key];
        }
    }
}

export class ChangeManyUserSettingsModel {
    public OrganisationId: string;
    public UserIds: Array<string> = [];
    public SelectAll: boolean;
    public Status: string;
    public ChangeToStatus: string;

    [key: string]: any;

    constructor(user: any) {
        for (var key in user) {
            this[key] = user[key];
        }
    }
}

export class UsersModel {
    public id: string;
    public fullName: string;
    public email: string;
    public userAvatarUrl: string;
    public userCreatedId: string;  

    constructor(id: string, fullName: string, email: string, userAvatarUrl: string, userCreatedId: string) {
        this.id = id;
        this.fullName = fullName;
        this.email = email;
        this.userAvatarUrl = userAvatarUrl;
        this.userCreatedId = userCreatedId;
    }
}

export class UserIconListModel {
    public icon: string;
    public id: string;
    public name: string;
    public email: string;
}

export class UserDetailModel {
    public Token: string;
    public TokenExpires: string;
    public UserId: string;
}

export class LogedInUserModel {
    public UserId: string;
    public Id: string;
    public Email: string;
    public FirstName: string;
    public Is24DateTimeFormat: string; 
    public IsWeekStartFromSunday: string;
    public Language: string;
    public LastName: string;
    public ProfilePictureUrl: string;
    public Status: string;
    public City: string;
    public Country: string;
    public Latitude: number;
    public Longitude: number;
    public Timestamp: number;
    public timestamp: number;
    public EvolumiAdmin: string;
    public Sender: string;
    public Url: string;
    public Host: string;
    public Port: string;
    public EnableSSL: boolean;
    public UserName: string;
    public Password: string;
    public Signature: string;
    public PhoneNumbers: Array<any>;
    public ValidEmails: Array<string>;
    [key: string]: any;

    constructor(logedinUser: any) {
        for(var key in logedinUser) {
            this[key] = logedinUser[key];
        }
        if (this.Language === '' || this.Language === null) {
            this.Language = 'en';
        }
        if (this.ProfilePictureUrl === '' || this.ProfilePictureUrl === null) {
            this.ProfilePictureUrl = 'assets/images/avatar.jpg';
        }
        this.UserId = this.Id;
        this.UserName = this.Email;
    }
}

export class UserListModel {
    public Id: string;
    public FullName: string;
}