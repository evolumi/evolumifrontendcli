export class NewsfeedModel {
    public AccountId: string;
    public Comments: any;
    public DateCreated: number;
    public DocumentId: string;
    public IsWeekStartFromSunday: string;
    public HasLiked: boolean;
    public Id: string;
    public LabelNames: any;
    public LabelString: string;
    public Likes: string;
    public LikesCount: number;
    public CommentsCount: number;
    public Text: string;
    public Type: string;
    public UserId: string;
    public UserProfilePictureUrl: string;
    public showcomment: boolean = false;
    public commentloader: boolean = false;
    public newEdit: boolean = false;
    public _http: any;
    public commentLoading: boolean;
    [key: string]: any;

    constructor(newfeed: any) {
        for (var key in newfeed) {
            this[key] = newfeed[key];
        }
        this.LikesCount = (newfeed.Likes) ? newfeed.Likes.length : 0;
        this.CommentsCount = (newfeed.Comments) ? newfeed.Comments.length : 0;

        if (this.UserProfilePictureUrl === null)
            this.UserProfilePictureUrl = '/assets/images/avatar.jpg';
    }
    likeit(): void {
        this.HasLiked = true;
        this.LikesCount++;
    }
    unlikeit(): void {
        this.HasLiked = false;
        this.LikesCount--;
    }
    showCommentBox() {
        this.showcomment = !this.showcomment;
    }
}

export class CreateNewsfeedModel {

    public HideFromDashboard: boolean = false;
    public AccountId: string;
    public Text: string;
}

export class CommentModel {
    public id: string;
    public dateCreated: number;
    public text: string;
    public userCreatedId: string;
    public userName: string;
    public userProfilePictureUrl: string;
    [key: string]: any;
    constructor(comment: any) {
        for (var key in comment) {
            this[key] = comment[key];
        }
        if (!this.dateCreated) this.dateCreated = new Date().getTime();
        if (this.userProfilePictureUrl === null) this.userProfilePictureUrl = 'assets/images/avatar.jpg'
    }
}
