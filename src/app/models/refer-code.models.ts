import { ChosenOptionModel } from '../modules/shared/components/chosen/chosen.models';
export class ReferCodeModel {
    public Name: string;
    public Source: string;
    public Type: string;
    public Campaign: string;
    public Sources: ChosenOptionModel[];
    public Types: ChosenOptionModel[];
    public Campaigns: ChosenOptionModel[];
    
    [key: string]:any;

    constructor(obj: any = null) {
        for(let key in obj) {
            if(obj.hasOwnProperty(key)) {
                this[key] = obj[key]
            }
        }
    }
}