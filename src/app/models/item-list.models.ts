export class ItemListModel {

    columns: ItemListColumn[];
    items: any[];
    routerLink: string; // baseRouterLink
    routerLinkItemProperty: string; // the property of object that will pass in a routeparamter
    noItemsMessage: string; // message for when no items in list
    showPreloader: boolean; // property to decide if preloader is shown
    itemsIsAsync: boolean = true; // if items are an observable or no
    nameColumn: string = 'Name'; // this is which is the firstcolumn
    [key: string]: any;

    constructor(itemList: any) {
        for (var key in itemList) {
            this[key] = (<any>itemList)[key];
        }
    }
}

export class ItemListColumn {

    name: string;
    label: string;
    className: string = '';
    link: boolean = false;
    tag: boolean = false;
    act: boolean = false;
    isArray: boolean = false;
    [key: string]: any;

    constructor(column: any) {
        for (var key in column) {
            this[key] = (<any>column)[key];
        }
    }
}

export interface ItemListItems {
    Id: string;
    IsChecked: boolean;
}