import { CategoryGroup } from "./category.models";
import { MailListModel } from "./newsletter.models";
import { TemplateModel } from "./template.models";

export interface OrganisationModel {
    Id: string;
    OrganisationName: string;
    PhoneNumber: string;
    Email: string;
    Currency: number;
    CurrencyName: string;
    PaymentData: any;
    Packages: Array<any>; //Denna kan skricka på flera ställen då det är ett interface
    Address: any;
    Settings: any;
    Tags: Array<string>;
    Labels: Array<string>;
    AccountTypes: Array<any>;
    Roles: Array<any>;
    Vats: Array<number>;
    HasImage: boolean;
    ImageUrl: string;
    OrganisationAdminRole: string;
    UserRole: string;
    UserPermissions: Array<string>;
    Plan: string;
    DefaultSignature: string;
    timestamp: any;
    SMTPSettings: any;
    CustomTitles: any;
    PhoneNumbers: Array<any>;
    DefaultPhoneNumber: any;
    UserPhoneNumber: any;
    AccountFilters: Array<any>;
    DefaultAccountFilter: string;
    Units: Array<string>;
    CategoryGroups: Array<CategoryGroup>;
    Features: Array<string>;
    MailLists: Array<MailListModel>;
    Templates: Array<TemplateModel>;
    FavoriteInsights: Array<any>;
    AccountCustomFields: Array<any>;
    ActionCustomFields: Array<any>;
    DealCustomFields: Array<any>;
    PersonCustomFields: Array<any>;
    RelationCustomFields: Array<any>;
    PriceLists: Array<any>;
    DefaultPriceList: string;
    IsPlus: boolean;
    DefaultRole: string;
}

export interface OrganisationListModel {
    Id: string;
    OrganisationName: string;
    PhoneNumber: string;
    Email: string;
    Currency: string;
    CurrencyName: string;
    HasImage: boolean;
    IsLocked: boolean;
    UserRole: string;
    UserRoleName: string;
    ImageUrl: string;
    NumberOfAccounts: number;
    NumberOfDeals: number;
    NumberOfUsers: number;
    NumberOfActions: number;
    Plan: string;
    UserPermissions: Array<string>;
    CreatedBy: string;
    DateCreated: number;
    IsAdmin: boolean;
}

export interface OrganisationUpdateModel {
    Id: string;
    Currency: string;
    OrganisationName: string;
    PhoneNumber: string;
    Email: string;
    CustomTitles: any;
    AccountCustomFields: Array<any>;
    PersonCustomFields: Array<any>;
    ActionCustomFields: Array<any>;
    DealCustomFields: Array<any>;
    RelationCustomFields: Array<any>;
}