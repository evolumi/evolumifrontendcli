﻿import { FileModel } from './file.models';

export class MessageModel {
    public id: string;
    public text: string;
    public fileUrl: string;
    public file: FileModel;
    public dateCreated: Date;
    public isMe: boolean;
    public isImage: boolean;
    public isFile: boolean;
    public userName: string;
    public userAvatarUrl: string;
    public userCreatedId: string;
}