export class CategoryGroup {
    Id: string;
    Name: string;
    Color: string = '';
    Type: string;
    FontColor: string = '#FFF'; // Sorry for the mixup with Css-Color but came in late
    IsRequired: boolean;
    IsEditable: boolean;
    CanMultiSelect: boolean;
    Categories: Array<string> = [];
    CategoriesToAdd: Array<string> = []; // only used frontend to store categories to save
    NoDelete: boolean;
    [key: string]: any;

    constructor(category: any) {
        for (var key in category) {
            this[key] = (<any>category)[key];
        }
    }
}