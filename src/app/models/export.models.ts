// Order of props is how they will get shown in frontend
export class ExportModel {
    public Id: string;

    [key: string]: any;
}

export interface PropertyHolder {
    MainProps: Array<string>;

    [key: string]: any
}

// ACCOUNTS
export class ExportCompanyAccountModel extends ExportModel {
    public AccountData = new ExportCompanyAccountData();
    public AccountInfo = new ExportAccountInfo();
    public AccountAddress = new ExportAccountAddress();
    public AccountAddressData = new ExportAccountAddressData();
    public AccountRelation = new ExportAccountRelation();
    public AccountFinancial = new ExportAccountFinancial();
    public AccountCustomFields = new ExportAccountCustomFields();

    [key: string]: any;
}

export class ExportPersonAccountModel extends ExportModel {
    public AccountData = new ExportPersonAccountData();
    public AccountInfo = new ExportAccountInfo();
    public AccountAddress = new ExportAccountAddress();
    public AccountAddressData = new ExportAccountAddressData();
    public AccountRelation = new ExportAccountRelation();
    public AccountContact = new ExportAccountContact();
    public AccountCustomFields = new ExportAccountCustomFields();

    [key: string]: any;
}

export class ExportCompanyAccountData {
    public InExport: boolean = false;
    public ShownName: boolean = false;
    public FullName: boolean = false;
    public CompanyNumber: boolean = false;
    public Vat: boolean = false;
    public AccountType: boolean = false;
    public AccountOwner: boolean = false;
    public ParentAccount: boolean = false;
    public Tags: boolean = false;
    public ReferredBy: boolean = false;
    public DateCreated: boolean = false;
    public UserCreated: boolean = false;
    public DateModified: boolean = false;
    public UserModified: boolean = false;

    [key: string]: any
}

export class ExportPersonAccountData {
    public InExport: boolean = false;
    public FirstName: boolean = false;
    public LastName: boolean = false;
    public Title: boolean = false;
    public CompanyNumber: boolean = false;
    public Vat: boolean = false;
    public AccountType: boolean = false;
    public AccountOwner: boolean = false;
    public ParentAccount: boolean = false;
    public Tags: boolean = false;
    public ReferredBy: boolean = false;
    public DateCreated: boolean = false;
    public UserCreated: boolean = false;
    public DateModified: boolean = false;
    public UserModified: boolean = false;

    [key: string]: any
}

export class ExportAccountInfo {
    // Not to show in frontend just assign if any checkbox is chosen
    public InExport: boolean = false;
    public Email: boolean = false;
    public Phone: boolean = false;
    public Website: boolean = false;

    [key: string]: any
}

export class ExportAccountAddress {
    // Not to show in frontend just assign if any checkbox is chosen
    public InExport: boolean = false;
    public VisitingAddress: boolean = false;
    public BillingAddresss: boolean = false;
    public DeliveryAddress: boolean = false;
    public ShippingAddress: boolean = false;
    public OtherAddress: boolean = false;

    [key: string]: any
}

export class ExportAccountAddressData {
    // Not to show in frontend just assign if any checkbox is chosen
    public InExport: boolean = false;
    public Street: boolean = false;
    public PostalCode: boolean = false;
    public City: boolean = false;
    public State: boolean = false;
    public Country: boolean = false;
    public Description: boolean = false;

    [key: string]: any
}

export class ExportAccountRelation {
    // Not to show in frontend just assign if any checkbox is chosen
    public InExport: boolean = false;
    public FirstName: boolean = false;
    public LastName: boolean = false;
    public Title: boolean = false;
    public CompanyName: boolean = false;
    public Email: boolean = false;
    public Phone: boolean = false;
    public Note: boolean = false;
    public Tags: boolean = false;
    public CustomFields: { [key: string]: boolean } = {};

    [key: string]: any
}

export class ExportAccountContact {
    // Not to show in frontend just assign if any checkbox is chosen
    public InExport: boolean = false;
    public FirstName: boolean = false;
    public LastName: boolean = false;
    public Title: boolean = false;
    public Department: boolean = false;
    public Email: boolean = false;
    public Phone: boolean = false;
    public Notes: boolean = false;
    public ExternalId: boolean = false;
    public InternalId: boolean = false;

    [key: string]: any
}

export class ExportAccountFinancial {
    // Not to show in frontend just assign if any checkbox is chosen
    public InExport: boolean = false;
    public Employee: boolean = false;
    public Revenue: boolean = false;
    public Year: boolean = false;
    public Currency: boolean = false;

    [key: string]: any
}

export class ExportAccountCustomFields {
    public InExport: boolean = false;
    public CustomFields: { [key: string]: boolean } = {};

    [key: string]: any;
}

//ACTIONS
export class ExportActionModel extends ExportModel {
    public ActionData = new ExportActionData();
    public ActionDeal = new ExportActionDeal();
    public ActionPerson = new ExportActionPerson();
    public ActionExtra = new ExportActionExtra();

    [key: string]: any;
}

export class ExportActionData {
    public InExport: boolean = false;
    public Name: boolean = false;
    public AssignedTo: boolean = false;
    public StartDate: boolean = false;
    public EndDate: boolean = false;
    public ClosedDate: boolean = false;
    public Account: boolean = false;
    public ActionGroup: boolean = false;
    public ActionType: boolean = false;
    public Priority: boolean = false;
    public Status: boolean = false;
    public Comment: boolean = false;
    public Labels: boolean = false;
    public DateCreated: boolean = false;
    public UserCreated: boolean = false;
    public DateModified: boolean = false;
    public UserModified: boolean = false;

    [key: string]: any;
}

export class ExportActionDeal {
    public InExport: boolean = false;
    public Name: boolean = false;
    public SalesProcess: boolean = false;
    public DealStatus: boolean = false;

    [key: string]: any;
}

export class ExportActionPerson {
    public InExport: boolean = false;
    public FirstName: boolean = false;
    public LastName: boolean = false;
    public FullName: boolean = false;
    public Title: boolean = false;
    public Email: boolean = false;
    public Phone: boolean = false;
    public Tags: boolean = false;
    public CustomFields: { [key: string]: boolean } = {};

    [key: string]: any
}

export class ExportActionExtra {
    public InExport: boolean = false;
    public Location: boolean = false;
    public ExternalId: boolean = false;
    public InternalId: boolean = false;

    [key: string]: any
}