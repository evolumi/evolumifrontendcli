export class AddressModel {
    public Id: string;
    public Type: string;
    public PostalCode: string;
    public Street: string;
    public City: string;
    public State: string;
    public Country: string;
    public Description: string;
    constructor(
        id: string,
        type: string,
        postalCode: string,
        street: string,
        city: string,
        state: string,
        country: string,
        description: string
    ) {
        this.Id = id;
        this.Type = type;
        this.PostalCode = postalCode;
        this.Street = street;
        this.City = city;
        this.State = state;
        this.Country = country;
        this.Description = description;
    }

}

export class AddressModelBilling {
	public Id: string;
    public Type: string;
    public PostalCode: string;
    public Street: string;
    public City: string;
    public State: string;
    public Country: string;
    public Description: string;
	public Latitude: number;        
	public Longitude: number;
	public ShowAddress: any;
	[key: string]: any;
	constructor(
	address = {
			Id : '',
			Type: 'Billing',
			PostalCode: '',
			Street: '',
			City: '',
			State: '',
			Country: '',
			Description: '',
			}
		) {
			 for (var key in address) {
			this[key] = (<any>address)[key];
            }
		}
}

export class AddressTypes {
    static billing: string = 'Billing';
    static visiting: string = 'Visiting';
    static shipping: string = 'Shipping';
    static delivery: string = 'Delivery';
    static other: string = 'Other';
}