import { ActionsModel } from "./actions.models";

export class Address {
	public Types = ['Billing'];
	public PostalCode: string;
	public Street: string;
	public City: string;
	public State: string;
	public Country: string;
	public Description: string;
	[key: string]: any;

	constructor(address = { PostalCode: '', Street: '', City: '', State: '', Country: '', Description: '' }) {
		for (var add in address) {
			this[add] = (<any>address)[add];
		}
	}
}
export class ProductModel {
	public Article: number = 0;
	public Product: string;
	public Units: number;
	public Discount: number;
	public ProductDiscountType: string = "Persent";
	public Price: number;
	public Currency: string;
	public Sum: number;
	public UnitsType: string;
	public Description: string;
	public InitialCurrency: string;
	public ProductId: string;
	public ManualPrice: number;
	public PricesAsTotal: boolean;

	constructor(Article: number, Product: string, Units: number, Discount: number, Price: number, Sum: number, UnitsType: string, Description: string, ProductDiscountType = "Persent") {
		this.Article = Article;
		this.Product = Product;
		this.Units = Units;
		this.Discount = Discount;
		this.Price = Price;
		this.Sum = Sum;
		this.UnitsType = UnitsType;
		this.Description = Description;
		this.ProductDiscountType = ProductDiscountType;
	}
}
export class OwnerPercentage {
	public UserId: string;
	public Percent: number;
	public Name: string;
	constructor(UserId: any, Percent: any, Name = "") {
		this.UserId = UserId;
		this.Percent = Percent;
		this.Name = Name;
	}

}

export class DealsModel {
	public Id: string;
	public AccountId: string;
	public Name: string;
	public DealStatusId: string;
	public SaleProcessId: string;
	public Note: string;
	public DateStart: number;
	public DateEnd: number;
	public Currency: number;
	public CurrencyName: string;
	public DateInvoice: number;
	public DateInvoiceExpire: number;
	public Vat: number;
	public ShippingAddress: Address = new Address();
	public BillingAddress: Address = new Address();
	public ProductItems: ProductModel[] = [];
	public Owners: OwnerPercentage[] = [];
	public Contacts: string[] = [];
	public ContactAccounts: string[] = [];
	public ExternalId: string;
	public InternalId: string;
	public OCR: string;
	public Reminder: any;
	public Value: number;
	public DealAsProducts: boolean = false;
	public Editmode: boolean = true;
	public ExpectedCloseDate: number;
	public AccountName: string;
	public AccountIsPerson: boolean;

	public NextAction: ActionsModel;
	public ActionEarlier: boolean;
	public ActionToday: boolean;
	public ActionThisWeek: boolean;
	public CloseDateMissed: boolean;
	public CloseDateToday: boolean;
	public CloseDateThisWeek: boolean;

	public index: number;


	constructor(Deal: any = null) {
		this.ContactAccounts = [];
		if (Deal) {
			Object.assign(this, Deal);
			if (this.Reminder && !this.Reminder.TimeSpan) {
				this.Reminder = undefined;
			}
			let dayInMilliseconds = 86400000;
			let now = new Date().setHours(0, 0, 0, 0);
			if (Deal.NextAction) {
				//Skaffa tiden från 00:00 idag
				if (now > Deal.NextAction.StartDate) {
					this.ActionEarlier = true;
				}
				else if (now + dayInMilliseconds > Deal.NextAction.StartDate) {
					this.ActionToday = true;
				}
				//Idag 00:00 + 7 dagar blir början av 7 dagen. 8 För att räkna med 7e Kalenderdagen från idag
				else if (now + (7 * dayInMilliseconds) > Deal.NextAction.StartDate) {
					this.ActionThisWeek = true;
				}
			}
			if (Deal.ExpectedCloseDate) {
				if (now > Deal.ExpectedCloseDate) {
					this.CloseDateMissed = true;
				}
				else if ((now + dayInMilliseconds) > Deal.ExpectedCloseDate) {
					this.CloseDateToday = true;
				}
				else if ((now + 7 * dayInMilliseconds) > Deal.ExpectedCloseDate) {
					this.CloseDateThisWeek = true;
				}
			}
		}
	}
}

export class SalesProcessModel {
	public Id: string;
	public Name: string;
	constructor(id: string, Name: string) {
		this.Id = id;
		this.Name = Name;
	}
}