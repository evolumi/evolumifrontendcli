﻿import { UserIconListModel } from './user.models';
import { ContactListModel } from './contact.models';

export class ConversationModel {
    public id: string;
    public name: string;
    public accountId: string;
    public accountName: string;
    public type: string;
    public dateCreated: Date;
    public users: Array<UserIconListModel> = [];
    public contacts: Array<ContactListModel> = [];
    public hidden: boolean;
    public newMessages: number = 0;
    public creator: boolean;
    public accountIsPerson: boolean;
}