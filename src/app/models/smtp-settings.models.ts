﻿import { FileModel } from './file.models';

export class SMTPSettingsModel {
    public sender: string;
    public url: string;
    public host: string;
    public port: number;
    public enableSSL: boolean;  
    public userName: string;
    public password: string;
    public signature: string;
    public organisationId: string;
    public sendWithSMTP: boolean;
    public validated: boolean;
    public images: Array<FileModel> = [];

    constructor(model: any) {
        if (model) {
            this.sender = model.Sender;
            this.url = model.Url;
            this.host = model.Host;
            this.port = model.Port;
            this.enableSSL = model.EnableSSL;
            this.userName = model.UserName;
            this.password = model.Password;
            this.signature = model.Signature;
            this.organisationId = model.OrganisationId;
            this.sendWithSMTP = model.SendWithSMTP;
            this.validated = model.Validated;
            this.images = [];
        }
        this.organisationId = "";
    }
}