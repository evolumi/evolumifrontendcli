﻿import { BidShortModel } from '../models/bid.models';
import { SelectedCandidateModel } from './candidate.models';
import { FileModel } from "./file.models";
import { LinkModel } from "./link.models";

// model using for output list
export class AssignmentShortModel {
    public id: string;
    public title: string;
    public description: string;
    public status: string;
}
// assignment model for organisation
export class AssignmentOrgModel extends AssignmentShortModel {
    public selectedCandidate: SelectedCandidateModel;
    public bids: Array<BidShortModel> = []; // candidates
}

export class AssignmentFilterModel {
    public selectedType: string;
    public selectedEmployment: string;
    public selectedGeography: string;
    public selectedLanguaes: Array<string> = [];
    constructor(selectedType: string, selectedEmployment: string, selectedGeography: string, selectedLanguaes: Array<string>) {
        this.selectedType = selectedType;
        this.selectedEmployment = selectedEmployment;
        this.selectedGeography = selectedGeography;
        this.selectedLanguaes = selectedLanguaes;
    }
}

export class AssignmentStatus {
    static open: string = "Open";
    static ongoing: string = "Ongoing";
    static closed: string = "Closed";
}

export class AssignmentTypes {
    static insideSales: string = "Inside sales";
    static fieldSales: string = "Field sales";
    static bookingMeetings: string = "Booking meetings";
}

// extended assignment model for user
export class AssignmentUserModel extends AssignmentShortModel {

    public files: Array<FileModel> = [];
    public links: Array<LinkModel> = [];
    public organisationId: string;
    public orgLogoUrl: string;
    public orgName: string;
    public assignmentsOpen: string;
    public assignmentsOngoing: string;
    public assignmentsPrevious: string;
    public isAppliedRequestSend: boolean;
}

export class EmploymentTypes {
    static partTime: string = "Part Time";
    static fullTime: string = "Full Time";
}