﻿import {AssignmentShortModel} from './assignment.models';
import {FileModel} from '../models/file.models';
import {LinkModel} from '../models/link.models';

export class MarketplaceOrgProfileModel {
    public id: string;
    public logoUrl: string;
    public title: string;
    public description: string;
    public assignments: Array<AssignmentShortModel>;
}

export class MarketplaceUserProfileModel {
    public id: string;
    public logoUrl: string;
    public description: string;
    public files: Array<FileModel> = [];
    public links: Array<LinkModel> = [];
}