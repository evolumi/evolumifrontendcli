export class SalesCommission {
    Id: string;
    Name: string;
    Description: string;
    Rules: Array<SalesCommissionRule> = [];
    IsActive: boolean = true;
    Data: Array<SalesCommissionData> = [];
    RemoveData: Array<string> = [];

    //Frontend variables
    Count: number;

    [key: string]: any;

    constructor(comp: any) {
        for (var key in comp) {
            this[key] = (<any>comp)[key];
        }
    }
}

export class SalesCommissionRule {
    public Id: string;
    public SalesGoalType : string; //enum backend
    public Value: number = 1;
    public CommissionType: string;
    public Quantity: number = 1;
    public DealStatusId: string;
    public ActionStatuses: Array<string> = [];
    public ActionTypes: Array<string> = [];
    public Labels: Array<string> = [];
    public UseActualCloseDate: boolean;
    public SalesProcessId: string;
    public Currency: string;

    [key: string]: any;

    constructor(comp: any) {
        for (var key in comp) {
            this[key] = (<any>comp)[key];
        }
    }
}

export class SalesCommissionData {
    public Id: string;
    public RelatedSalesCommissionId: string;
    public UserId?: string;
    public RoleId?: string;
    public FromDate: number;
    public EndDate?: number;
    public IsTemporary: boolean;

    [key: string]: any;

    constructor(comp: any) {
        for (var key in comp) {
            this[key] = (<any>comp)[key];
        }
    }
}

export class SalesCommissionUserModel {
    public BaseUsers: Array<SalesCommissionData> = [];
    public TemporaryUsers: Array<SalesCommissionData> = [];
    public FutureBaseUsers: Array<SalesCommissionData> = [];
    public FutureTemporaryUsers: Array<SalesCommissionData> = [];

    [key: string]: any;

    constructor(obj: any) {
        for (var key in obj) {
            this[key] = (<any>obj)[key];
        }
    }
}

export class SalesCommissionUserEditModel {
    FromDate: number = new Date().getTime();
    EndDate?: number;
    IsTemporary: boolean;

    [key: string]: any;

    constructor(obj: any) {
        for (var key in obj) {
            this[key] = (<any>obj)[key];
        }
    }
}