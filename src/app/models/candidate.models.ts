﻿import {MarketplaceUserProfileModel } from './marketplace.models';
import {BidExtendedModel } from '../models/bid.models';

export class CandidateModel {
    public userProfile: MarketplaceUserProfileModel;
    public bid: BidExtendedModel;
}

export class SelectedCandidateModel {
    public id: string;
}