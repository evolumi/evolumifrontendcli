export class Visitor {
    public Ip: string;
    public CompanyName: string;
    public Visits: Visit[];
    [key : string] : any
    
    constructor(obj: any) {
        for(let key in obj) {
            if(obj.hasOwnProperty(key)) {
                this[key] = obj[key]
            }
        }
    }
}

export class Visit {
    public Url: string;
    public Time: Date;
    [key : string] : any

    constructor(obj: any) {
        for(let key in obj) {
            if(obj.hasOwnProperty(key)) {
                this[key] = obj[key]
            }
        }
    }
}