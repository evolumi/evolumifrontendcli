﻿export class ImportExcelAnswerModel {
    public fileName: string;
    public columns: Array<KeyValuePair>;
    constructor(fileName: string, columns: Array<KeyValuePair>) {
        this.fileName = fileName;
        this.columns = columns;
    }
}
export class KeyValuePair {
    public key: string;
    public value: string;
}

export class ImportExcelModel {
    public organisationId: string;
    public accountManagerId: string;
    public accountTypeId: string;
    public fileName: string;
    public namesCorresponding: any;
    public addressCount: number;
    public contactsCount: number;
    public notesCount: number;
    public statementCount: number;
    public personAccount : boolean;
    public statementCurrency : string;

    constructor(organisationId: string, accountManagerId: string, accountTypeId: string, fileName: string, namesCorresponding: any, addressCount: number, contactsCount: number, notesCount: number, statementCount: number, personAccount : boolean, statementCurrency: string) {
        this.organisationId = organisationId;
        this.accountManagerId = accountManagerId;
        this.accountTypeId = accountTypeId;
        this.fileName = fileName;
        this.namesCorresponding = namesCorresponding;
        this.addressCount = addressCount;
        this.contactsCount = contactsCount;
        this.notesCount = notesCount; 
        this.statementCount = statementCount;
        this.personAccount = personAccount;
        this.statementCurrency = statementCurrency;
    }
}

export class AccountImportSpecification {
    public static fullName = "FullName";
    public static shownName = "ShownName";
    public static vat = "Vat";
    public static acountEmail = "AcountEmail";
    public static phone = "Phone";
    public static website = "Website";
    public static addressType = "AddressType";
    public static postalCode = "PostalCode";
    public static street = "Street";
    public static city = "City";
    public static state = "State";
    public static country = "Country";
    public static description = "Description";
    public static title = "Title";
    public static firstName = "FirstName";
    public static lastName = "LastName";
    public static department = "Department";
    public static phoneNumber = "PhoneNumber";
    public static contsctEmail = "ContctEmail";
    public static contactNote = "ContactNotes";
    public static profileUrl = "ProfileUrl";
    public static notes = "Notes";
    public static accCompanyId = "CompanyNumber";
    public static tags = "Tags";
    public static statementYear = "StatementYear";
    public static statementEmployee = "StatementEmployee";
    public static statementRevenue = "StatementRevenue";
}
