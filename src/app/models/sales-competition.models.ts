export class SalesCompetitionModel {
    public Id: string;
    public Name: string;
    public Description: string;
    public Participants: Array<string> = [];
    public Observers: Array<string> = [];
    public StartDate: number;
    public EndDate: number;
    public Rules: Array<SalesCompRuleModel> = [];
    public IsDateOnly: boolean = true;

    [key: string]: any;

    constructor(comp: any) {
        for (var key in comp) {
            this[key] = (<any>comp)[key];
        }
    }
}

export class SalesCompRuleModel {
    public Id: string;
    public SalesGoalType : string; //enum backend
    public Value: number = 1;
    public Quantity: number = 1;
    public DealStatusId: string;
    public ActionStatuses: Array<string> = [];
    public ActionTypes: Array<string> = [];
    public Labels: Array<string> = [];
    public UseActualCloseDate: boolean;
    public SalesProcessId: string;
    public Currency: string;

    [key: string]: any;

    constructor(comp: any) {
        for (var key in comp) {
            this[key] = (<any>comp)[key];
        }
    }
}

export class UserCompetitionData {
    public UserId: string;
    public TotalPoints: number;
    public PerRule: Array<CompetitionPerRuleData> = [];
    public FormCurve: string;
}

export class CompetitionPerRuleData {
    public SalesCompRuleId: string;
    public Points: number;
    public AmountDone: number;
}

export class SalesCompLeaderboard {
    public SalesCompetition: SalesCompetitionModel;
    public UserDataList: Array<UserCompetitionData>;
    public Charts: any;
}