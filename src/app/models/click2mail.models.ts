﻿export class Click2MailModel {
    public id: string;
    public name: string;
    public subject: string;
    public body: string;
    public email: string;
    public organisaonId: string;
    public isPersonal: boolean;
    public attachments: Array<any> = [];
    public accountId: string;
    public contactId: string;
    public actionGroupId: string;

    constructor(model: any) {
        if (model) {
            this.id = model.id;
            this.name = model.name
            this.subject = model.subject;
            this.body = model.body;
            this.email = model.email;
            this.isPersonal = model.isPersonal;
            this.organisaonId = model.organisaonId;
            this.attachments = model.attachments ? model.attachments : [];
            this.contactId = model.contactId;
            this.accountId = model.accountId;
            this.actionGroupId = model.actionGroupId;
        }
        else {
            this.id = "";
            this.name = "";
            this.subject = "";
            this.body = "";
            this.email = "";
            this.organisaonId = "";
            this.isPersonal = false;
            this.attachments = [];
            this.contactId = "";
            this.accountId = "";
            this.actionGroupId = "";
        }
    }
    
}