export class ActionsModel {
	public AccountId: string;
	public ActionGroupId: string;
	public AccountName: string;
	public Comments: string;
	public EndDate: number;
	public Id: string;
	public Labels: any = [];
	public Location: string;
	public Name: string;
	public Priority: string;
	public Account: any;
	public RelatedPersons: string[] = [];
	public RelatedPersonsData: any[] = [];
	public RelatedContactName: string;
	public RelatedDealId: string;
	public RelatedDealName: string;
    public StartDate: number;
	public Status: string;
	public Type: string;
	public UserAssignedId: string;
	public UserAssignedName: string;
    public hidden: boolean = false;
    public Reminder: any;
	public IsChecked: boolean;
	public AccountIsPerson : boolean;
	public History: any[] = [];
	[key: string]: any;

	public icon: string;

	constructor(actions: any) {
		 for (var key in actions) {
             this[key] = actions[key];
             if (key == "Reminder" && actions[key] && !actions[key].TimeSpan) {
                 this[key] = undefined;
             }
        }	
	}
}
