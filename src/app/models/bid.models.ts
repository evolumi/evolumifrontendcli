﻿import {FileModel } from '../models/file.models';
import {LinkModel } from '../models/link.models';

export class BidShortModel {
    public userId: string;
    public firstName: string;
    public lastName: string;
    public dateCreated: Date;
}

export class BidExtendedModel extends BidShortModel {
    public files: Array<FileModel> = [];
    public links: Array<LinkModel> = [];
    public personalMessage: string;
}