export class DocumentModel {

    public Id: string;
    public OrganisationId: string;
    public AccountId: string;
    public ConversationId: string;
    public DealId: string;
    public Name: string;
    public Title: string;
    public Body: string;
    public Footer: string;
    public SendOnlyFiles: boolean;
    public Status: string;
    public IsEsign: boolean;
    public Files: Array<FileModel>;
    public Parties: Array<PartyModel>;
    public Deadline: number;
    public DateSignComplete: number;
    public DateCreated: number;
    public UserCreatedId: string;
    public OrganisationName: string;
    public AccountName: string;
    public DealName: string;
    public Reminder: number;
    [key: string]: any

    constructor(document = {
        Id: '', OrganisationId: '', AccountId: '', ConversationId: '', DealId: '', Name: '', Title: '', Body: '', Footer: '', SendOnlyFiles: false, Status: 'Draft', IsEsign: false, Files: new Array<FileModel>(), Parties: new Array<PartyModel>(), Deadline: 0, DateSignComplete: 0, DateCreated: 0, UserCreatedId: '', OrganisationName: '', AccountName: '', DealName: '', Reminder : 0
    }) {
        for (var key in document) {
            this[key] = (<any>document)[key];
        }
    }
}

export class PartyModel {
    public Id :string;
    public contactId: string;
    public UserId: string;
    public Name: string;
    public Email: string;
    public Status: string;
    public AllowedSignTypes: Array<string>;
    public DateSigned: number;
}

export class FileModel {
    public Id: string;
    public Name: string;
    public ContentType: string;
    public Location: string;
    [key: string]: any

    constructor(file = {
        Id: '', Name: '', AccountId: '', ContentType: '', Location: ''}) {
        for (var key in file) {
            this[key] = (<any>file)[key];
        }
    }

}
