export class ContactsCollectionModel {
	public AccountName: string;
	public FullName: string;
	public Notes: string;
	public FirstName: string;
	public LastName: string;
	public Title: string;
	public Department: string;
	public PhoneNumber: string;
	public Email: string;
	public ProfileUrl: string;
	public AccountId: string;
	public OrganisationId: string;
	public Id: string;
	public DateCreated: Date;
	public DateModified: Date;
	public UserModifiedId: string;
	public UserCreatedId: string;
    public IsDeleted: boolean = false;
    public MailPriority: boolean = false;
	[key: string]: any;

	constructor(contacts={AccountId:'',FirstName:'',LastName:'',Title:'',Department:'',PhoneNumber:'',Email:'',ProfileUrl:'', MailPriority:false}) {
		 for (var key in contacts) {
			this[key] = (<any>contacts)[key];
        }
    }	
} 

export class ContactPersonModel {
    public firstName: string;
    public lastName: string;
    public email: string;
    public title: string;
    public phoneNumber: string;
    public note: string;

    constructor(firstName: string, lastName: string, email: string, title: string, phoneNumber: string, note: string) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.title = title;
        this.phoneNumber = phoneNumber;
        this.note = note;
    }
}

export class ContactsModel {
    public id: string;
    public fullName: string;
    public email: string;
    public profileUrl: string;
    public userCreatedId: string;
    public mailPriority: boolean;


    constructor(id: string, fullName: string, email: string, profileUrl: string, userCreatedId: string, mailPriority: boolean
) {
        this.id = id;
        this.fullName = fullName;
        this.email = email;
        this.profileUrl = profileUrl;
        this.userCreatedId = userCreatedId;
        this.mailPriority = mailPriority;
    }
}

export class ContactListModel {
    public id: string;
    public name: string;
    public Email: string
}
