﻿export class PaginationStructure {
    public page: number;
    public pageSize: number;
    public pagesCount: number;
    public nearPageNumbers: Array<number>;
    public records: Array<any>;
}