import { TemplateModel } from './template.models';
import { SearchParams } from "../services/accounts.service";

export class NewsletterModel {

    public Id: string;
    public Description: string;
    public MailLists: Array<string> = [];
    public Subject: string;
    public SendDate: number;
    public Template: TemplateModel;

    // dto only
    public MailListNames: Array<string> = [];
    public Recipients: number;
    public Sent: number;
    public Opened: number;
    public Delivered: number;
    public Unsubscribed: number;
    public Spam: number;
    public SendNow: boolean;

    [key: string]: any;

    constructor(obj: any = null) {
        for (let key in obj) {
            if (obj.hasOwnProperty(key)) {
                this[key] = obj[key]
            }
        }
    }
}

export class MailListModel {

    public Id: string;
    public Name: string;
    public PersonFilter: SearchParams = null;
    public CompanyFilter: SearchParams = null;
    public NonAccounts: Array<NonAccount> = [];
    public PersonIds: Array<string> = [];
    public CompanyIds: Array<string> = [];
    public Recipients: number;
    public TotalRecipients: number;

    [key: string]: any;

    constructor(obj: any = null) {
        for (let key in obj) {
            if (obj.hasOwnProperty(key)) {
                this[key] = obj[key]
            }
        }
    }
}

export class NonAccount {
    FirstName: string;
    LastName: string;
    OrganisationName: string;
    Email: string;

    [key: string]: any;

    constructor(obj: any = null) {
        for (let key in obj) {
            if (obj.hasOwnProperty(key)) {
                this[key] = obj[key]
            }
        }
    }
}