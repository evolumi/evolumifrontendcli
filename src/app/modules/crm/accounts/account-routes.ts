import { Routes } from '@angular/router';

// Components
import { AccountsComponent } from './accounts.component';
import { AccountOptionsComponent } from './account-options/account-options.component';
import { AccountComponent } from './account-details/account.component';
import { AccountDetailsComponent } from './account-details/account-details.component';
import { AddAccountComponent } from './add/add-account.component';
import { AddressAddComponent } from './address/add/address-add.component';
import { VoidComponent } from '../../shared/components/void';


export const AccountsRoutes: Routes = [
    {
        path: 'Accounts',
        component: AccountsComponent,
        children: [
            { path: '', component: VoidComponent },
            { path: 'AddAccount', component: AddAccountComponent },
            { path: 'Options/:task', component: AccountOptionsComponent }
        ],
    },
    {
        path: 'PersonAccounts',
        component: AccountsComponent,
        children: [
            { path: '', component: VoidComponent },
            { path: 'AddAccount', component: AddAccountComponent },
            { path: 'Options/:task', component: AccountOptionsComponent }
        ],
    },
    {
        path: 'Accounts/:accountId',
        component: AccountComponent,
        children: [
            {
                path: '', component: AccountDetailsComponent, children: [
                    { path: '', component: VoidComponent },
                    { path: 'AddAccount', component: AddAccountComponent },

                    { path: 'AddAddress', component: AddressAddComponent },
                    { path: 'EditAddress/:addressId', component: AddressAddComponent }
                ]
            },

        ]
    },
    {
        path: 'PersonAccounts/:accountId',
        component: AccountComponent,
        children: [
            {
                path: '', component: AccountDetailsComponent, children: [
                    { path: '', component: VoidComponent },
                    { path: 'AddAccount', component: AddAccountComponent },

                    { path: 'AddAddress', component: AddressAddComponent },
                    { path: 'EditAddress/:addressId', component: AddressAddComponent }
                ]
            },

        ]
    }
];
