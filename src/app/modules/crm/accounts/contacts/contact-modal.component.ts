import { Component, ElementRef } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ValidationService } from '../../../../services/validation.service';
import { ContactsCollection } from '../../../../services/contacts.service';
import { AccountsCollection } from '../../../../services/accounts.service';
import { ActionsCollection } from '../../../../services/actionservice';
import { ChosenOptionModel } from '../../../shared/components/chosen/chosen.models';
import { ContactsCollectionModel } from '../../../../models/contact.models';
import { ModalService } from '../../../../services/modal.service';

import { OrgService } from '../../../../services/organisation.service';

declare var $: JQueryStatic;

@Component({
    selector: 'contact-modal',
    templateUrl: './contact-modal.html',
})

export class ContactModalComponent {

    private modalSub: any;

    el: Element;
    private sub: any;

    public contact: ContactsCollectionModel;

    public new: boolean = false;
    public edit: boolean = false;

    public contactForm: FormGroup;

    public task: string;
    private orgSub: any;
    public accountTypes: Array<ChosenOptionModel> = [];
    public accType: string;
    public accountColor: string;

    constructor(
        public formBuilder: FormBuilder,
        public element: ElementRef,
        public _contactsCollection: ContactsCollection,
        public _accountCollection: AccountsCollection,
        public _actionCollection: ActionsCollection,
        private modalService: ModalService,
        private orgService: OrgService,
    ) {
        this.resetForm();
    }

    resetForm() {
        this.contactForm = this.formBuilder.group({
            FirstName: ['', Validators.required],
            LastName: [''],
            Title: [''],
            Email: ['', ValidationService.emailValidatorIfNotEmpty],
        });
    }

    ngOnInit() {

        this.el = this.element.nativeElement;
        $(this.el).hide();

        this.sub = this._contactsCollection.currentContactObservable().subscribe(contact => {
            if (contact.Id) {
                this.contact = contact;
                this.updateName(contact.AccountId);
                this._actionCollection.getActionByContact(contact.AccountId, contact.Id);
            }
        });

        this.modalSub = this.modalService.contactTrigger().subscribe((res: any) => {
            if (res) {
                if (res.contact) {
                    this.showModalFromModel(res.contact, res.accountId);
                }
                else {
                    this.showModal(res.contactId, res.accountId);
                }
            }
        });

        if (this.task === 'ChangeAccountType') {
            this.orgSub = this.orgService.currentOrganisationObservable().subscribe(res => {
                if (res) {
                    this.accountTypes = [];
                    console.log("[Accounts-Options] Loaded account Types", res.AccountTypes);

                    for (let type of res.AccountTypes) {
                        this.accountTypes.push(new ChosenOptionModel(type.Id, type.Name));;
                    }
                    this.accType = res.AccountTypes[0].value;
                }
            })
        }
    }



    public updateName($event: any) {
        //this.contact.AccountName = this.accounts.find(r => r.value == $event).label.toString();
        this._accountCollection.getAccountTypeColor($event).subscribe(res => this.accountColor = res);
    }

    public showModal(contactId: string, accountId: string) {
        console.log("[Contact-Modal] Opening Modal for Contact [" + contactId + "]" + (accountId ? " in account: " + accountId : ""));
        if (contactId) {
            console.log("EditContact");
            this.new = false;
            this.edit = false;
            this._contactsCollection.getContact(contactId);
        }
        else {
            console.log("NewContact");
            this.new = true;
            this.edit = true;
            this._contactsCollection.createContactModel();
            this._contactsCollection.contactData.AccountId = accountId;
        }

        $(".sidePopup").css('overflow', 'visible');
        $(".contentPanel").css('overflow', 'visible');
        $(this.el).show();
    }

    public showModalFromModel(contact: ContactsCollectionModel, accountId: string) {
        if (contact) {
            this._contactsCollection.createContactModel(contact);
            this.new = true;
            this.edit = true;
            this._accountCollection.getAccountTypeColor(contact.AccountId).subscribe(res => this.accountColor = res);
            this._contactsCollection.contactData.AccountId = accountId;
        }
        $(".sidePopup").css('overflow', 'visible');
        $(".contentPanel").css('overflow', 'visible');
        $(this.el).show();
    }


    updateContact() {

        this._contactsCollection.updateContact();
        this.edit = false;
    }

    addContact() {
        console.log("[Contact-Modal] Trying to Add Contact!");
        this._contactsCollection.addContact().subscribe(res => {
            if (res.IsSuccess) {
                this.close();
            }
            else {

            }
        });

    }

    clickToClose(event: any) {
        var target = event.target || event.srcElement;
        if (target.id === 'editContact') {
            this.close();
        }
    }

    public cancel() {

        if (this.edit && !this.new) {
            this.edit = false;
            return;
        }
        this.close();
    }

    public close() {
        console.log("[Contact-Modal] Closing!");
        $(this.el).hide();
        $(".sidePopup").css('overflow', 'auto');
        $(".contentPanel").css('overflow', 'auto');
        //this.resetForm();
        this.edit = false;
        this.new = false;
    }


    ngOnDestroy() {
        if (this.sub) this.sub.unsubscribe();
        if (this.modalSub) this.modalSub.unsubscribe();
        if (this.orgSub) this.orgSub.unsubscribe();
    }
}
