import { Component, Input, SimpleChanges } from '@angular/core';
import { ContactsCollection } from '../../../../../services/contacts.service';
import { TranslateService } from '@ngx-translate/core';
import { CallService } from '../../../../../services/callService';
import { AuthService } from '../../../../../services/auth-service';
import { ModalService } from '../../../../../services/modal.service';
import { AccountsCollection } from "../../../../../services/accounts.service";
import { AccountsModel } from "../../../../../models/account.models";
import { Subscription } from "rxjs/Subscription";
import { AccountRelation } from "../../../../../models/account.models";
import { ChosenOptionModel } from "../../../../shared/components/chosen/chosen.models";
import { ConfirmationService } from '../../../../../services/confirmation.service';

@Component({
    selector: 'contactblock',
    styles: [`
        .contactblockbubble {
            font-weight: initial;
            margin: -3px 22px 0px 0px;
        }

        .contactblockbubble .btn {
            positon: relative;
        }

        .contact-title {
            font-size: 14px;
            color: #888;
        }
    `],
    templateUrl: 'contact-block.html'
})

export class ContactBlockComponent {
    @Input() enableDelete: boolean;
    @Input() enableAdd = true;
    @Input() account: AccountsModel;

    private personAccountList: ChosenOptionModel[] = [];

    private accountRelations: AccountRelation[] = [];
    constructor(
        public _contact: ContactsCollection,
        private translate: TranslateService,
        private callService: CallService,
        public auth: AuthService,
        public modal: ModalService,
        private accSvc: AccountsCollection,
        private confirmSvc: ConfirmationService
    ) { }
    public test: any;
    bubbleOpened() {

    }

    ngOnInit() {
        this.accountListSearch("");

        // this.personAccountSub = this.accSvc.contactAccountsObservable().subscribe(data => {
        //     this.accountRelations = data;
        //     console.log("NEW RELS: ", data);
        //     this.updateNotContactAccounts();
        // });
    }

    public accountListSearch(search: string = "") {
        this.accSvc.personList(search, 1, (accounts) => {
            this.personAccountList = [...accounts];
            this.updateNotContactAccounts();
        })
    }

    public accountListScroll(search: string = "", page: number = 1) {
        this.accSvc.personList(search, page, (accounts) => {
            this.personAccountList.push(...accounts);
            this.updateNotContactAccounts();
        })
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes && changes['account'] && changes['account'].currentValue && changes['account'].currentValue.Id != '00000000-0000-0000-0000-000000000000') {
            if (this.account.IsPerson) {
                console.log("[Contact-Box] Getting relatives for [" + this.account.Id + "]");
                this._contact.getContactByAccountId(this.account.Id);
            }
            else {
                console.log("[Contact-Box] Getting contacts for [" + this.account.Id + "]");
            }

            this.accSvc.getContactsForAccount(this.account.Id, this.account.IsPerson, (data: any) => {
                this.accountRelations = data;
                this.updateNotContactAccounts();
            })
        }
    }

    // Todo! This wont work if org has more then 100 personAccounts
    getAccName(accountId: string) {
        let acc = this.personAccountList.find(x => x.value == accountId);
        if (acc) {
            return acc.label;
        }
        else {
            return accountId;
        }
    }

    contactClicked(relation: AccountRelation) {
        relation.currentAccount = this.account;
        this.accSvc.showAccountRelationPopup(relation);
    }

    addNewContact() {
        let relation = new AccountRelation({
            CompanyId: this.account.Id,
            CompanyName: this.account.ShownName,
            PersonId: null
        });

        this.accSvc.showAccountRelationPopup(relation);

        // this.accSvc.addingContactForAccount = this.account;
        // this.router.navigate(["AddAccount"], { relativeTo: this.route });
    }

    public notContactAccounts: ChosenOptionModel[] = [];
    updateNotContactAccounts() {
        this.notContactAccounts = this.personAccountList.filter((x: ChosenOptionModel) => {
            return this.accountRelations.find(y => y.PersonId == x.value) == null;
        });
    }

    deleteFromContacts(relationId: string) {
        let warnTitle = this.translate.instant("WARNING");
        let warnText = this.translate.instant("WARNDELCONTACT");
        this.confirmSvc.showModal(warnTitle, warnText, () => {
            this.accSvc.removeAccountRelation(relationId, () => {
                this.accountRelations.splice(this.accountRelations.findIndex(x => x.Id == relationId), 1);
                this.updateNotContactAccounts();
            });
        });
    }
    addToContacts(personId: string) {
        this.accSvc.getAccountDetails(personId).subscribe(res => {
            let relation = new AccountRelation({
                CompanyId: this.account.Id,
                PersonId: personId
            });
            if (res) {
                relation.FirstName = res.FirstName;
                relation.LastName = res.LastName;
            }
            this.accSvc.showAccountRelationPopup(relation);
            this.updateNotContactAccounts();
        });
    }

    public callContact(person: AccountsModel) {
        let userId = localStorage.getItem("userId");
        console.log("[ContactBlcok] Calling conact", person, this.account.Id);
        this.callService.startPhoneCall(person.Phone, userId, person.Id, this.account.Id);
    }
    ngOnDestroy() {
    }
}
