import { Component, Input } from '@angular/core';
import { ModalService } from '../../../../../services/modal.service';
import { TranslateService } from '@ngx-translate/core';
import { ContactsCollection } from '../../../../../services/contacts.service';
import { AccountsModel } from '../../../../../models/account.models';
import { ConfirmationService } from '../../../../../services/confirmation.service';
import { CallService } from "../../../../../services/callService";

@Component({

    selector: 'relativesblock',
    templateUrl: './relatives-block.html',
    styleUrls: ['./relatives-block.css']
})

export class RelativesBlockComponent {
    @Input() account: AccountsModel;
    @Input() enableDelete: boolean;

    constructor(
        public modal: ModalService,
        private translate: TranslateService,
        public contactSvc: ContactsCollection,
        private confirmSvc: ConfirmationService,
        private callService: CallService
    ) { }

    public callContact(person: AccountsModel) {
        let userId = localStorage.getItem("userId");
        this.callService.startPhoneCall(person.Phone, userId, person.Id, this.account.Id);
    }

    deleteContact(id: string) {
        let confirmTitle = this.translate.instant("WARNING");
        let confirmText = this.translate.instant("WARNDELCONTACT");
        this.confirmSvc.showModal(confirmTitle, confirmText, () => {
            this.contactSvc.deleteContact(id);
        });
    }
}