import { Component, Input } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AccountsCollection } from '../../../../services/accounts.service';
declare var $: JQueryStatic;

@Component({
    selector: 'file-box',
    template: ` 
                    <div class="title">
                        <h4>{{ 'FILES' | translate }}</h4>
                        <label for="file-input-acc" class="btn pull-right">+</label>
                        <input style="display:none" id="file-input-acc" name="file" (change)="addFile()" type="file" />
                    </div>

                    <div class="content">
                        <ul class="iconList" *ngIf="account.Files.length!=0">
                            <li *ngFor="let file of account.Files">
                                <i class="fa fa-file left icon"></i>
                                <a [downloadFile]="file" target="_blank">               
                                    {{file.Name}}
                                </a>
                                <a [downloadFile]="file">               
                                    <i class="fa fa-download right icon download"></i>
                                </a>
                                <a (click)="delete(file.Id)">               
                                    <i class="fa fa-trash right icon" ></i>
                                </a>
                            </li>
                        </ul>
                        <span *ngIf="account.Files.length==0">{{'NOFILES' | translate }}</span>
                    </div>                       
                  `
})

export class FileBlockComponent {
    @Input() account: any;

    constructor(private translate: TranslateService, private accountService: AccountsCollection) {
    }

    public delete(id: string) {
        var conf = confirm(this.translate.instant("WARNDELFILE"));
        if (conf) {
            this.accountService.deleteFile(this.account.Id, id);
        }
    }

    public addFile() {
        let formData: FormData = new FormData();
        formData.append('file', (<any>$('#file-input-acc')[0]).files[0]);

        this.accountService.uploadFile(this.account.Id, formData);
    }
    ngOnChanges(change: any) {
        if (change && change["account"] && change["account"].currentValue) {

        }
    }
}
