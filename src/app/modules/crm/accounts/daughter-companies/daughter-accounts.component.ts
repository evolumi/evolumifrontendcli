import { Component, Input } from '@angular/core';
import { AccountsCollection } from '../../../../services/accounts.service';

@Component({
    selector: 'daughter-companies-list',
    templateUrl: './list.html',
    styles: [`

    p{
		font-family: Arial, Helvetica, sans-serif;
		font-size: 17px;
	}
    span.label{
        margin: 5px 5px 3px 0;
    }  
    .title {
        border-bottom: 1px solid #eeeeee;
        display: block;
        padding-bottom: 10px;
        margin-bottom: 10px;
        font-weight: bold;
    }

    @media screen and (max-width: 767px) {  
       .col-sm-11 {
           margin-left:30px;
       } 
    }
    @media screen and (max-width: 1358px) {
    .actionIcon {
       width:28px;
        height:28px;
        float: left;
    }
    }
    `]
})

export class DaughterAccountsListComponent {
    @Input() account: any;
    @Input() enableEditButton: any;

    public enableAddButton: boolean = true;

    constructor(public accountService: AccountsCollection)
    { }

    gotoAccount(id: any) {

    }

    ngOnChanges() {
    }

    ngOnInit() {

    }
}
