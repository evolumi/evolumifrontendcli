import { Component, Input } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from '../../../../../services/auth-service';
import { ModalService } from '../../../../../services/modal.service';
import { AccountsCollection } from '../../../../../services/accounts.service';

@Component({
    selector: 'document-box',
    template: ` 
                    <div class="title">
                        <h4>{{ 'DOCUMENTS' | translate }}</h4>
                        <a class="btn pull-right" (click)="modal.showDocumentModal(null, account)">+</a>
                    </div>

                    <div class="content">
                        <ul class="iconList" *ngIf="account.Documents && account.Documents.length > 0">
                            <li *ngFor="let doc of account.Documents">
                                <i class="fa fa-file left icon"></i>
                                <a (click)="modal.showDocumentModal(doc, account)">{{doc.Name}}</a>
                                <a *ngIf="doc.Status=='Draft'|| doc.Status=='Rejected'" (click)="delete(doc.Id)" download>               
                                    <i class="fa fa-trash right icon"></i>
                                </a>
                            </li>
                        </ul>
                        <span *ngIf="!account.Documents || account.Documents.length == 0">{{'NOFILES' | translate }}</span>
                    </div>                       
                  `
})

export class DocumentBlockComponent {
    @Input() account: any;

    constructor(private translate: TranslateService, public auth: AuthService, public modal: ModalService, private accountService: AccountsCollection) {
    }

    public delete(id: string) {
        var conf = confirm(this.translate.instant("WARNDELFILE"));
        if (conf) {
            this.accountService.deleteDocument(id);
        }
    }


}
