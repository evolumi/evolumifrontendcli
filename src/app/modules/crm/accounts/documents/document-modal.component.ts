import { Component, ElementRef } from '@angular/core';
import { Location } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import { AccountsCollection } from '../../../../services/accounts.service';
import { UsersService } from '../../../../services/users.service';
import { ContactsCollection } from '../../../../services/contacts.service';
import { ChosenOption } from "../../../shared/components/chosen/chosen";
import { ChosenOptionModel } from '../../../shared/components/chosen/chosen.models';
import { ModalService } from '../../../../services/modal.service';
import { Api } from '../../../../services/api';
import { AccountsModel } from '../../../../models/account.models';
import { DocumentModel, PartyModel, FileModel } from '../../../../models/document.models';
import { Subscription } from "rxjs/Subscription";
declare var $: JQueryStatic;

@Component({
    selector: 'document-modal',
    templateUrl: './document-modal.html',
    styles: [`
    .party-add div {
        background: #fff;
        padding-left: 0px !important;
        padding-right: 0px !important;
    }
    
    .party-add .nav-tabs li a {
        font-size: 14px;
    }
    `]
})

export class DocumentModalComponent {

    public displayModal: boolean = false;
    public showAddParty: boolean;
    public edit: boolean = false;

    public deals: Array<ChosenOption> = [];
    public users: Array<ChosenOption> = [];
    public contacts: Array<ChosenOption> = [];
    public reminderOptions: Array<ChosenOption>;

    public currentDocument: DocumentModel;
    public account: AccountsModel;

    public party: PartyModel;
    public partySource: string = 'email';

    private docSub: any;
    private userSub: any;
    private contactSub: any;
    private transSub: Subscription;

    constructor(
        public location: Location,
        public element: ElementRef,
        private accountService: AccountsCollection,
        modalService: ModalService,
        userService: UsersService,
        private api: Api,
        private contactService: ContactsCollection,
        private translate: TranslateService
    ) {
        this.party = new PartyModel();
        this.docSub = modalService.DocumentTrigger().subscribe((res: any) => {
            if (res) {
                if (res.document || res.account)
                    this.showModal(res.account, res.document);

                if (res.documentId)
                    this.showModalById(res.documentId);
            }
        });

        this.userSub = userService.activeUserListObervable().subscribe(users => {
            console.log("[Document-Modal] Loaded User-List! Found " + users.length + " of them");
            this.users = users;
        });

        this.contactSub = contactService.contactsByAccountListObservable().subscribe(contacts => {
            console.log("[Document-Modal] Loaded Contact-List! Found " + contacts.length + " of them");
            this.contacts = contacts;
        });
    }

    ngOnInit() {
        this.initReminders();
    }

    private initReminders() {
        this.reminderOptions = [
            new ChosenOptionModel(0, this.translate.instant("NOREMINDER")),
            new ChosenOptionModel(1, this.translate.instant("1DAY")),
            new ChosenOptionModel(3, this.translate.instant("3DAYS")),
            new ChosenOptionModel(7, this.translate.instant("1WEEK"))
        ];
        this.transSub = this.translate.onLangChange.subscribe((res: any) => {
            this.reminderOptions = [
                new ChosenOptionModel(0, this.translate.instant("NOREMINDER")),
                new ChosenOptionModel(1, this.translate.instant("1DAY")),
                new ChosenOptionModel(3, this.translate.instant("3DAYS")),
                new ChosenOptionModel(7, this.translate.instant("1WEEK"))
            ];
        });
    }

    private showModalById(documentId: string) {
        console.log("[Document-Modal] Opening Document modal and loading Document[" + documentId + "] ( WARNING! This is not implemented yet!)");

        // TODO!
        // this.document = ....
        // this.account = ...
        // this.getDeals();

        this.displayModal = true;
    }

    private showModal(account: any, document: DocumentModel = null) {
        console.log("Showing modal", this.users);
        this.account = account;
        if (document) {
            this.edit = false;
            this.currentDocument = new DocumentModel(document);
        }
        else {
            this.edit = true;
            this.currentDocument = new DocumentModel();
            this.currentDocument.IsEsign = true;
            this.currentDocument.OrganisationId = localStorage.getItem("orgId");
            this.currentDocument.AccountId = account.Id;
        }

        if (account) {
            this.contactService.getContactListByAccountId(account.Id);
            this.getDeals();
        }
        this.displayModal = true;
    }

    private getDeals() {
        this.api.requestGet("Accounts/" + this.account.Id + "/Deals", (deals) => {
            console.log("[Document-Modal] Loaded Deal-List! Found " + deals.length + " of them");
            this.deals = [];
            for (var d of deals) {
                this.deals.push(new ChosenOptionModel(d.Id, d.Name));
            }
        });
    }

    public showNewParty() {
        this.party = new PartyModel();
        this.showAddParty = true;
    }

    public createNewParty() {
        console.log("[Document] Adding party to Document.");
        console.log(this.party);
        if (this.partySource == 'email') this.party.contactId = this.party.UserId = '';

        this.api.requestPost("Documents/" + this.currentDocument.Id + "/AddParty", this.party, (res) => {
            console.log("[Document] Party Added");
            console.log(res);
            this.currentDocument.Parties.push(res);
            this.accountService.getDocuments(this.account.Id);
            this.clearAddParty();
        },
            (error) => {
                console.log("[Document] Failed to add Party to document!");
            });
    }

    public deleteParty(party: PartyModel) {
        var conf = confirm(this.translate.instant("WARNDELPARTY"));
        if (conf) {
            this.api.requestDelete("Documents/" + this.currentDocument.Id + "/DeleteParty/" + party.Id, (res) => {
                console.log("[Document-Modal] Party Deleted!");
                this.currentDocument.Parties.splice(this.currentDocument.Parties.indexOf(party), 1);
            });
        }
    }

    public setContact(id: string) {
        let contact = this.contacts.find(x => x.value == id)
        console.log("[Document] Getting partydata from Contact! -> " + contact.label);
        this.party.contactId = id;
        this.party.Name = contact.label.toString();
        this.party.UserId = '';
        this.party.Email = '';
    }

    public setUser(id: string) {
        let user = this.users.find(x => x.value == id)
        console.log("[Document] Getting partydata from User! -> " + user.label);
        this.party.UserId = id;
        this.party.Name = user.label.toString();
        this.party.contactId = '';
        this.party.Email = '';
    }

    public update() {
        if (this.currentDocument.Id) {
            this.api.requestPut("Documents/" + this.currentDocument.Id + "", this.currentDocument, (res) => {
                console.log("[Document-Modal] Document Update: Sucess!");
                this.accountService.getDocuments(this.account.Id);
                this.edit = false;

            }, (err) => {
                console.log("[Document-Modal] Document Update: Failed!");
            });
        }
        else {
            this.api.requestPost("Documents/", this.currentDocument, (res) => {
                this.currentDocument.Id = res;
                this.edit = true;
                this.accountService.getDocuments(this.account.Id);
                console.log("[Document-Modal] Document Create: Sucess!");
            }, (err) => {
                console.log("[Document-Modal] Document Create: Failed!");
            });
        }
    }

    public addFile() {
        let formData: FormData = new FormData();
        formData.append('file', (<any>$('#file-input-doc')[0]).files[0]);

        this.api.upload("Documents/" + this.currentDocument.Id + "/AddFile", formData).then(res => {
            let result = JSON.parse(res);
            console.log("[Document-Modal] File-Upload: Sucess!");
            console.log(result);
            console.log(result.Data);
            let file = new FileModel(result.Data);
            console.log(file);
            this.currentDocument.Files.push(file);
            this.accountService.getDocuments(this.account.Id);

        }, (err) => {
            console.log("[Document-Modal] File-Upload: Failed!");
            console.log(err);
        });
    }

    public deleteFile(file: FileModel) {
        var conf = confirm(this.translate.instant("WARNDELFILE"));
        if (conf) {
            this.api.requestDelete("Documents/" + this.currentDocument.Id + "/DeleteFile/" + file.Id, (res) => {
                console.log("[Document-Modal] File Deleted!");
                this.currentDocument.Files.splice(this.currentDocument.Files.indexOf(file), 1);
            });
        }
    }

    public delete() {
        var conf = confirm(this.translate.instant("WARNDELDOC"));
        if (conf) {
            this.accountService.deleteDocument(this.currentDocument.Id);
            this.cancel();
        }
    }

    public revoke() {
        var conf = confirm(this.translate.instant("WARNRECALLDOC"));
        if (conf) {
            this.api.requestPut("Documents/" + this.currentDocument.Id + "/Recall", null, (res) => {
                console.log("[Document-Modal] Document Recall: Sucess!");
                this.currentDocument.Status = "Draft";
                this.accountService.getDocuments(this.account.Id);

            }, (err) => {
                console.log("[Document-Modal] Document Recall: Failed!");
            });
        }
    }

    public clearAddParty() {
        this.showAddParty = false;
        this.party = new PartyModel();
        this.partySource = "email";
    }

    public clickToClose(event: any) {
        var target = event.target || event.srcElement;
        if (target.id === 'editDocument') {
            this.edit = false;
            this.cancel();
        }
    }

    public cancel() {

        if (this.edit && this.currentDocument.Id) {
            this.edit = false;
            return;
        }

        console.log("[Document-Modal] Closing!");
        this.party = new PartyModel();
        this.account = null;
        this.currentDocument = new DocumentModel();
        this.displayModal = false;
        this.clearAddParty();
    }

    public send() {
        this.api.requestPut("Documents/" + this.currentDocument.Id + "/Send", this.currentDocument, (res) => {
            console.log("[Document-Modal] Document Send: Sucess!");
            this.accountService.getDocuments(this.account.Id);
            this.currentDocument.Status = "Pending";
        }, (err) => {
            console.log("[Document-Modal] Document Send: Failed!");
        });
    }

    ngOnDestroy() {
        if (this.docSub) this.docSub.unsubscribe();
        if (this.userSub) this.contactSub.unsubscribe();
        if (this.contactSub) this.contactSub.unsubscribe();
        if (this.transSub) this.transSub.unsubscribe();
    }
}