import { Component, Input } from '@angular/core';
import { MapService, CollorArray } from '../../../../services/map.service';
import { AccountsCollection } from "../../../../services/accounts.service";

@Component({
	selector: 'address-list',
	templateUrl: './list.html',
	styles: [`        
	p{
		font-family: Arial, Helvetica, sans-serif;
		font-size: 17px;
	}		
	#lowerHouse{
		font-size:22px;
	}

	#lowerHouse and (min-width:991px){
		font-size:40px;
	}
	#positionHouse{
		padding-top:7px;
		float:left;
		height:50px;
	}
	.title {
        border-bottom: 1px solid #eeeeee;
        display: block;
        padding-bottom: 10px;
        margin-bottom: 10px;
        font-weight: bold;
    }
    
	`]
})

export class AddressListComponent {
	@Input() account: any;
	@Input() enableAddButton: any;
	@Input() enableEditButton: any;
	@Input() isPerson: boolean = true;

	constructor(public accountService: AccountsCollection,
		private _map: MapService) {
	}
	ngOnInit() {
		if (!this.enableAddButton) {
			this.enableAddButton = false;
		}
		if (!this.enableEditButton) {
			this.enableEditButton = false;
		}
	}

	openMap() {
		let addressList: Array<any> = [];
		for (let address of this.account.Address) {
			address.AccountId = this.account.Id;
			address.MarkerCollor = CollorArray[address.Type];
			address.AccountName = this.account.ShownName;
			addressList.push(address);
		}
		if (addressList.length) {
			this._map.showLocations(addressList);
		}
	}
}
