import { Component } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { AccountsCollection } from '../../../../../services/accounts.service';

@Component({

    templateUrl: './add.html',
})

export class AddressAddComponent {
    private sub: any;

    public editAddress: boolean = false;
    public accountId: string = '';
    public addressId: string = '';
    public addressForm: any;
    public addressTypes: any;

    public addresTypesSubscriber: any;

    constructor(
        public location: Location,
        public route: ActivatedRoute,
        public _accountCollection: AccountsCollection,

    ) {
    }
    ngOnInit() {
        this.sub = this.route.params.subscribe(params => {
            this.addressId = params['addressId'];
            this.accountId = this._accountCollection.currentAccount.Id;

            if (this.addressId) {
                this.editAddress = true;
                this._accountCollection.getAddress(this.addressId);
            } else {
                this.editAddress = false;
                this._accountCollection.createAddressModel();
            }

            this.addresTypesSubscriber = this._accountCollection.getAddressTypes().subscribe(res => {
                this.addressTypes = res;
            });

            if (!this.addressId) {
                this._accountCollection.address.Type = this.addressTypes[0];
            }
        });
    }
    public back() {
        this.location.back();
        //this._router.navigate(['/CRM/Accounts/', this.accountId]);
    }
    OutClick(event: any) {
        var target = event.target || event.srcElement;
        if (target.id === 'add') {
            this.back();
        }
    }
    public addAddress() {
        this._accountCollection.addAddress();
        this.back();
    }
    public updateAddress() {
        this._accountCollection.updateAddress();
        this.back();
    }
    ngOnDestroy() {
        if (this.addresTypesSubscriber) this.addresTypesSubscriber.unsubscribe();
        if (this.sub) this.sub.unsubscribe();
    }
}
