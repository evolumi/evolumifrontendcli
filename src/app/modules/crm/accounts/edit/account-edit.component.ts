import { Component, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { AccountsCollection } from '../../../../services/accounts.service';
import { ValidationService } from '../../../../services/validation.service';
import { ChosenOptionModel } from '../../../shared/components/chosen/chosen.models';
import { OrgService } from '../../../../services/organisation.service';
import { UsersService } from '../../../../services/users.service';

@Component({
	selector: 'account-edit',
	templateUrl: './edit.html',
	styles: [`
	.buttonRow.morePadding {
		padding-top: 10px;
		padding-bottom: 10px;
	}
	`]
})

export class AccountEditComponent {
	@Output() showAccount = new EventEmitter();
	public accountForm: any;
	public tags: Array<string> = [];
	public priceLists: Array<ChosenOptionModel> = [];
	public accountTypes: any;
	public users: any;
	public refereeAccountList: Array<ChosenOptionModel> = [];
	public parentAccountList: Array<ChosenOptionModel> = [];
	private orgSub: any;
	private userSub: any;
	private personAccount: boolean;
	private customFields: Array<any>;
	public clear: boolean;

	constructor(
		public _accountsCollection: AccountsCollection,
		private userService: UsersService,
		private orgService: OrgService,
		public form: FormBuilder,
		router: Router) {
		this.personAccount = _accountsCollection.currentAccount.IsPerson;
		if (this.personAccount) {
			this.accountForm = form.group({
				ShownName: [''],
				FirstName: ['', Validators.required],
				LastName: ['', Validators.required],
				FullName: [''],
				CompanyNumber: [''],
				Website: [''],
				Email: ['', ValidationService.emailValidatorIfNotEmpty],
				Phone: [''],
				Vat: [''],
				AccountTypeId: [''],
				ParentAccount: [''],
				AccountManagerId: [''],
			});
		}
		else {
			this.accountForm = form.group({
				ShownName: ['', Validators.required],
				FirstName: [''],
				LastName: [''],
				FullName: [''],
				CompanyNumber: [''],
				Website: [''],
				Email: ['', ValidationService.emailValidatorIfNotEmpty],
				Phone: [''],
				Vat: [''],
				AccountTypeId: [''],
				ParentAccount: [''],
				AccountManagerId: [''],
			});
		}
	}


	ngOnInit() {
		this.userSub = this.userService.activeUserListObervable().subscribe(users => {
			if (users) {
				this.users = users;
			}
		});

		this.orgSub = this.orgService.currentOrganisationObservable().subscribe(org => {
			if (org) {
				this.accountTypes = [];
				this.customFields = this._accountsCollection.account.IsPerson ? org.PersonCustomFields : org.AccountCustomFields;
				for (let a of org.AccountTypes) {
					this.accountTypes.push(new ChosenOptionModel(a.Id, a.Name));
				}
				for (let a of org.PriceLists) {
					this.priceLists.push(new ChosenOptionModel(a.Id, a.Name));
				}

				this._accountsCollection.account.AccountTypeId = org.AccountTypes[0].Id;
			}
		});
	}

	changeType(id: string) {
		this._accountsCollection.account.AccountTypeId = id;
	}

	setCustom(id: string, value: any) {
		if (!this._accountsCollection.account.CustomData[id] || this._accountsCollection.account.CustomData[id].toString() != value.toString()) {
			console.log("Setting value", value, this._accountsCollection.account.CustomData[id]);
			this._accountsCollection.account.CustomData[id] = value;
		}
	}

	setReferee(id: string) {
		this._accountsCollection.account.ReferredBy = id;
		this._accountsCollection.getAccountName(id).subscribe(res => {
			this._accountsCollection.account.ReferdByAccountName = res.Data;
		})
	}

	onitemsChange(event: any) {
		this._accountsCollection.account.Tags = event;
	}
	Create() {
		this.updateAccount();
	}
	updateAccount() {
		this._accountsCollection.editAccount();
		this.showAccount.next(false);
	}
	back() {
		console.log('back');
		this.showAccount.next(false);
	}
	ngOnDestroy() {
		if (this.userSub) this.userSub.unsubscribe();
		if (this.orgSub) this.orgSub.unsubscribe();
	}
}
