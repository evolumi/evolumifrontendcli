import { Component, EventEmitter } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ChosenOption } from "../../../shared/components/chosen/chosen";
import { LocalStorage } from '../../../../services/localstorage';
import { ImportService } from '../../../../services/importService';
import { ChosenOptionModel } from '../../../shared/components/chosen/chosen.models';
import { ImportExcelModel, AccountImportSpecification } from '../../../../models/import.models';
import { AddressModel } from '../../../../models/address.models';
import { ContactPersonModel } from '../../../../models/contact.models';
import { AddressTypes } from '../../../../models/address.models';
import { UsersService } from '../../../../services/users.service';
import { AccountsCollection } from '../../../../services/accounts.service';
import { OrgService } from '../../../../services/organisation.service';
import { ButtonPanelService } from '../../../../services/button-panel.service';
import { Configuration } from '../../../../services/configuration';
import { AccountInfoModel } from "../../../../models/account.models";
import { Subscription } from "rxjs/Subscription";

@Component({
    selector: 'import',
    templateUrl: './import.html',
    styleUrls: ['./import.css']
})

export class ImportComponent {

    private _crunchingCompletedSubscription: Subscription;

    public displayModal: boolean = false;
    public processing: boolean = false;
    public processingMessage: string = "";
    public step: number = 1;
    public fileName: string;
    public errorMessage: string;
    public aborted: boolean;

    public fileHeaders: Array<ChosenOption> = [];
    public accountTags: Array<ChosenOption> = [];
    public accountTypes: Array<ChosenOption> = [];
    public currencies: Array<ChosenOptionModel> = [];
    public users: Array<ChosenOption> = [];
    public addressTypes: Array<ChosenOption> = [
        new ChosenOptionModel(AddressTypes.billing, this._translateService.instant(AddressTypes.billing.toUpperCase())),
        new ChosenOptionModel(AddressTypes.visiting, AddressTypes.visiting),
        new ChosenOptionModel(AddressTypes.shipping, AddressTypes.shipping),
        new ChosenOptionModel(AddressTypes.delivery, AddressTypes.delivery),
        new ChosenOptionModel(AddressTypes.other, AddressTypes.other)
    ];

    public accountInfo: AccountInfoModel;
    public addresses: Array<AddressModel> = [new AddressModel("", AddressTypes.billing, "", "", "", "", "", "")];
    public contactPersons: Array<ContactPersonModel> = [new ContactPersonModel("", "", "", "", "", "")];
    public notes: Array<any> = [{ text: "" }];
    public financialstatements: Array<any> = [{ year: "", employees: "", revenue: "" }]

    private orgSub: any;
    private userSub: any;
    private buttonSub: any;
    private importSub: Subscription;
    private personAccounts: boolean = false
    private statementCurrency: string;

    constructor(
        private _importService: ImportService,
        private _usersService: UsersService,
        private _accountsCollection: AccountsCollection,
        private _localStorage: LocalStorage,
        private _translateService: TranslateService,
        private _orgService: OrgService,
        private _button: ButtonPanelService,
        config: Configuration
    ) {
        for (let cur of config.currencies) {
            this.currencies.push(new ChosenOptionModel(cur, cur));
        }
        this.buttonSub = this._button.commandObservable().subscribe(command => {
            if (command == "importpersons") { this.showModal(true) }
            if (command == "importcompanies") { this.showModal(false) }
        });
    }

    private clearData() {
        this.accountInfo = new AccountInfoModel("", "", "", "", "", "", "", "", "", "", this.personAccounts);
        if (this.accountTypes.length > 0)
            this.accountInfo.typeId = this.accountTypes[0].value.toString();
        if (this.users.length > 0)
            this.accountInfo.managerId = this.users[0].value.toString();
    }

    private subscribeToEvents(): void {
        this._crunchingCompletedSubscription = this._importService.importCompleted.subscribe((response: any) => {

            this.processingMessage = typeof (response) == "string" ? response : this._translateService.instant("CRUNCHINGDATACOMPLETE");
            this.processing = false;

            this._accountsCollection.getAccounts();
            if (typeof (response) != "string") {
                setTimeout(() => {
                    this.hideModal();
                }, 2000);
            }
            else {
                this.step = 3;
            }
        });
    }

    private subscribeToObservables(): void {
        this.importSub = this._importService.fileProceed$.subscribe(response => {
            if (response && typeof (response) != "string") {
                if (!this.aborted) {
                    this.processingMessage = this._translateService.instant("UPLOADCOMPLETE");
                    this.step = 2;
                    this.fileName = response.fileName;
                    this.fileHeaders = [];
                    for (var item of response.columns) {
                        this.fileHeaders.push(new ChosenOptionModel(item.value, item.key));
                    }

                    setTimeout(() => {
                        this.processing = false;
                    }, 500);
                } else {
                    this.processing = false;
                    this.aborted = false;
                }
            }
            else if (typeof (response) == "string") {
                if (!this.aborted) {
                    this.processingMessage = response;
                    this.showErrorMessage(response);
                }
                this.processing = false;
                this.aborted = false;
            }
        });

        this.userSub = this._usersService.activeUserListObervable().subscribe(users => {
            if (users) {
                this.users = users;
                if (this.users.length > 0)
                    this.accountInfo.managerId = this.users[0].value.toString();
            }
        });

        this.orgSub = this._orgService.currentOrganisationObservable().subscribe(org => {
            if (org) {
                this.statementCurrency = org.CurrencyName;
                for (let item of org.Tags) {
                    this.accountTags.push(new ChosenOptionModel(item, item))
                }
                for (let item of org.AccountTypes) {
                    if (item != null && typeof item.Id != "undefined")
                        this.accountTypes.push(new ChosenOptionModel(item.Id, item.Name))
                }
                if (this.accountTypes.length > 0)
                    this.accountInfo.typeId = this.accountTypes[0].value.toString();
            }
        })
    }

    private unsubscribeFromEvents() {
        if (this._crunchingCompletedSubscription) this._crunchingCompletedSubscription.unsubscribe();
        if (this.orgSub) this.orgSub.unsubscribe();
        if (this.userSub) this.userSub.unsubscribe();
        if (this.buttonSub) this.buttonSub.unsubscribe();
        if (this.importSub) this.importSub.unsubscribe();
    }

    ngOnInit() {
        this.clearData();
        this.subscribeToEvents();
        this.subscribeToObservables();
    }

    ngOnDestroy() {
        this.unsubscribeFromEvents();
    }

    public hideModal() {
        this.displayModal = false;
        this.step = 1;
        this.fileName = "";
        this.fileHeaders = [];
        this.addresses = [new AddressModel("", "", "", "", "", "", "", "")];
        this.contactPersons = [new ContactPersonModel("", "", "", "", "", "")];
        this.notes = [{ text: "" }];
        //this.tags = [];
        this.clearData();
    }

    public showModal(personAccounts = false) {
        this.personAccounts = personAccounts;
        this.displayModal = true;
    }

    public upload(fileList: Array<any>): void {
        const file = fileList[0];
        if (file.name.substring(file.name.length - 5) === '.xlsx') {
            this.processingMessage = this._translateService.instant("UPLOADING");
            this.processing = true;
            let formData: FormData = new FormData();
            formData.append('file', file);
            this._importService.uploadFile(formData);
        } else {
            this.showErrorMessage('THEFILEMUSTBEXLSX')
        }
    }

    public addNote() {
        this.notes.push({ text: "" });
    }

    public deleteNote(note: any) {
        var itemPosition = this.notes.indexOf(note);
        this.notes.splice(itemPosition, 1);
    }

    public addStatement() {
        this.financialstatements.push({ year: "", employees: "", revenue: "" })
    }

    public deleteStatement(statement: any) {
        var itemPosition = this.financialstatements.indexOf(statement);
        this.financialstatements.splice(itemPosition, 1);
    }

    public addAddress() {
        this.addresses.push(new AddressModel("", "", "", "", "", "", "", ""));
    }

    public deleteAddress(address: AddressModel): void {
        var itemPosition = this.addresses.indexOf(address);
        this.addresses.splice(itemPosition, 1);
    }

    public addContact() {
        this.contactPersons.push(new ContactPersonModel("", "", "", "", "", ""));
    }

    public deleteContact(contact: ContactPersonModel): void {
        var itemPosition = this.contactPersons.indexOf(contact);
        this.contactPersons.splice(itemPosition, 1);
    }

    //public addTag() {
    //    this.tags.push(string);
    //}

    public runImport() {

        this.processingMessage = this._translateService.instant("CRUNCHINGTHEDATA");
        this.processing = true;
        var correspondingCollection = {};
        if (this.accountInfo.fullName)
            (<any>correspondingCollection)[AccountImportSpecification.fullName] = this.accountInfo.fullName;
        if (this.accountInfo.shownName)
            (<any>correspondingCollection)[AccountImportSpecification.shownName] = this.accountInfo.shownName;
        if (this.accountInfo.vat)
            (<any>correspondingCollection)[AccountImportSpecification.vat] = this.accountInfo.vat;
        if (this.accountInfo.accCompanyId)
            (<any>correspondingCollection)[AccountImportSpecification.accCompanyId] = this.accountInfo.accCompanyId;

        if (this.accountInfo.phone)
            (<any>correspondingCollection)[AccountImportSpecification.phone] = this.accountInfo.phone;
        if (this.accountInfo.email)
            (<any>correspondingCollection)[AccountImportSpecification.acountEmail] = this.accountInfo.email;
        if (this.accountInfo.website)
            (<any>correspondingCollection)[AccountImportSpecification.website] = this.accountInfo.website;
        if (this.accountInfo.tags)
            (<any>correspondingCollection)[AccountImportSpecification.tags] = this.accountInfo.tags;


        for (var i = 1; i <= this.addresses.length; i++) {
            if (this.addresses[i - 1].City)
                (<any>correspondingCollection)[AccountImportSpecification.city + i] = this.addresses[i - 1].City;
            if (this.addresses[i - 1].Country)
                (<any>correspondingCollection)[AccountImportSpecification.country + i] = this.addresses[i - 1].Country;
            if (this.addresses[i - 1].Description)
                (<any>correspondingCollection)[AccountImportSpecification.description + i] = this.addresses[i - 1].Description;
            if (this.addresses[i - 1].PostalCode)
                (<any>correspondingCollection)[AccountImportSpecification.postalCode + i] = this.addresses[i - 1].PostalCode;
            if (this.addresses[i - 1].State)
                (<any>correspondingCollection)[AccountImportSpecification.state + i] = this.addresses[i - 1].State;
            if (this.addresses[i - 1].Street)
                (<any>correspondingCollection)[AccountImportSpecification.street + i] = this.addresses[i - 1].Street;
            if (this.addresses[i - 1].Type)
                (<any>correspondingCollection)[AccountImportSpecification.addressType + i] = this.addresses[i - 1].Type;
        }

        for (var i = 1; i <= this.contactPersons.length; i++) {
            if (this.contactPersons[i - 1].email)
                (<any>correspondingCollection)[AccountImportSpecification.contsctEmail + i] = this.contactPersons[i - 1].email;
            if (this.contactPersons[i - 1].firstName)
                (<any>correspondingCollection)[AccountImportSpecification.firstName + i] = this.contactPersons[i - 1].firstName;
            if (this.contactPersons[i - 1].lastName)
                (<any>correspondingCollection)[AccountImportSpecification.lastName + i] = this.contactPersons[i - 1].lastName;
            if (this.contactPersons[i - 1].phoneNumber)
                (<any>correspondingCollection)[AccountImportSpecification.phoneNumber + i] = this.contactPersons[i - 1].phoneNumber;
            if (this.contactPersons[i - 1].title)
                (<any>correspondingCollection)[AccountImportSpecification.title + i] = this.contactPersons[i - 1].title;
            if (this.contactPersons[i - 1].note)
                (<any>correspondingCollection)[AccountImportSpecification.contactNote + i] = this.contactPersons[i - 1].note;
        }

        for (var i = 1; i <= this.notes.length; i++) {
            if (this.notes[i - 1].text != "")
                (<any>correspondingCollection)[AccountImportSpecification.notes + i] = this.notes[i - 1].text;
        }

        for (var i = 1; i <= this.financialstatements.length; i++) {
            if (this.financialstatements[i - 1].year != "")
                (<any>correspondingCollection)[AccountImportSpecification.statementYear + i] = this.financialstatements[i - 1].year;
            if (this.financialstatements[i - 1].employees != "")
                (<any>correspondingCollection)[AccountImportSpecification.statementEmployee + i] = this.financialstatements[i - 1].employees;
            if (this.financialstatements[i - 1].revenue != "")
                (<any>correspondingCollection)[AccountImportSpecification.statementRevenue + i] = this.financialstatements[i - 1].revenue;
        }

        var data = new ImportExcelModel(
            this._localStorage.get('orgId'),
            this.accountInfo.managerId,
            this.accountInfo.typeId,
            this.fileName,
            correspondingCollection,
            this.addresses.length,
            this.contactPersons.length,
            this.notes.length,
            this.financialstatements.length,
            this.personAccounts,
            this.statementCurrency
        );

        this._importService.importUploadedFile(data);
    }

    private showErrorMessage(message: string) {
        this.errorMessage = this._translateService.instant(message);
        setTimeout(() => {
            this.errorMessage = '';
        }, 5000);
    }

    clickToClose(event: any) {
        if (this.step === 1) {
            let target = event.target || event.srcElement;
            if (target.id === 'importPopup') {
                this.hideModal();
            }
        }
    }

    abort() {
        this.aborted = true;
        this.processing = false;
    }
}
