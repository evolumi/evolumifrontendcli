import { Component } from '@angular/core';
import { AccountsCollection } from '../../../../services/accounts.service';
declare var $: JQueryStatic;

@Component({

    templateUrl: 'account.html',
})

export class AccountComponent {
    constructor(public _accountCollection: AccountsCollection) { }
    toggleRightPanel() {
        $('.rightPanel').hasClass('systemActive') ? $('.rightPanel').removeClass('systemActive') : $('.rightPanel').addClass('systemActive');
        $('.rightPanelControl').hasClass('systemActive') ? $('.rightPanelControl').removeClass('systemActive') : $('.rightPanelControl').addClass('systemActive');
    }
}

