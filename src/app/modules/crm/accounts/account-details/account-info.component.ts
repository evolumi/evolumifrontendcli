import { Component, Input, EventEmitter, Output } from '@angular/core';
import { Location } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';

import { AccountsModel } from '../../../../models/account.models';
import { AccountsCollection } from '../../../../services/accounts.service';

import { ModalService } from '../../../../services/modal.service';
import { OrgService } from '../../../../services/organisation.service';


@Component({

    selector: 'account-info',
    templateUrl: './account-info.html',
})

export class AccountInfoComponent {

    @Input() account: AccountsModel;
    @Input() enableEdit: boolean;
    @Output() editAccount = new EventEmitter();

    public accountForm: any;
    public showEditAccountForm: boolean = false;
    public tags: Array<string> = [];

    public customFields: Array<any> = [];

    constructor(private orgService: OrgService, private translate: TranslateService, public _accountsCollection: AccountsCollection, private _loc: Location, public modal: ModalService) {

    }
    ngOnInit() {
        this.customFields = this.account.IsPerson ?
            this.orgService.currentOrganisation().PersonCustomFields :
            this.orgService.currentOrganisation().AccountCustomFields;
    }
    edit() {
        this._accountsCollection.account = new AccountsModel(this.account);
        this.editAccount.next(true);
    }

    delete() {
        var conf = confirm(this.translate.instant('WARNDELACC'));
        if (conf) {
            this._accountsCollection.deleteAccount(this.account.Id);
            this._loc.back();
        }
    }
}
