import { Component, Input } from '@angular/core';
import { AccountsModel } from '../../../../models/account.models';
import { AccountsCollection, FinancialStatement } from '../../../../services/accounts.service';

import { ModalService } from '../../../../services/modal.service';

import { Configuration } from '../../../../services/configuration';
import { ChosenOptionModel } from '../../../../modules/shared/components/chosen/chosen.models';
import { OrgService } from '../../../../services/organisation.service';
import { accountdataSerivce } from '../../../../services/account-data.service';
import { ChosenOption } from "../../../../modules/shared/components/chosen/chosen";

import { ActivatedRoute } from '@angular/router';
import { FilterCollection } from '../../../../services/filterservice';

import { Subscription } from 'rxjs/Subscription';

@Component({

	selector: 'account-data',
	templateUrl: './account-data.html',
	styles: [`
	.col-sm-8 {	
		padding-left: 0px;
	}
	.col-sm-4{
		padding-right: 0px;
	}
	`]
})

export class AccountDataComponent {

	@Input() account: AccountsModel;

	private orgSub: Subscription;

	public displayAddDeclartion: boolean = false;
	public displayUpdateDeclartion: boolean = false;

	public subscriptions: Array<any> = [];
	public isSubscribed: boolean;

	public currency: string;
	public defaultCurrency: string;
	public currencies: Array<any> = [];
	public currencyOptions: Array<ChosenOption> = [];

	public availableYearsOptions: Array<ChosenOption> = [];
	public showAfterDelete: Array<ChosenOption> = [];
	public showAllAvailabelYears: Array<any> = []

	public editYear: boolean = false;

	public accountId: string = '';

	public i: number;
	public e: number;
	public endOfSlice: number;
	public beginOfSlice: number;

	public showingYears: Array<FinancialStatement> = [];

	public selectedYear: string;

	public CurrencyName: any;
	public addFirstNewYear: any;
	public updateNewEmployee: string;
	public updateNewRevunee: string;
	public updateNewCurrency: any;

	public financialStatement: FinancialStatement;
	public ShowArrowLeft: boolean = false;
	public ShowArrowRight: boolean = false;
	public newFnancialStatement: FinancialStatement = new FinancialStatement;
	public showArrowLeftAllYears: boolean = false;
	public showArrowRightAllYears: boolean = false;

	constructor(
		private config: Configuration,
		private orgService: OrgService,
		public _accountsCollection: AccountsCollection,
		public modal: ModalService,
		public _accountData: accountdataSerivce,
		public route: ActivatedRoute,
		public _filter: FilterCollection
	) { }

	ngOnInit() {

		this.currencies = this.config.currencies;
		for (var cur of this.currencies) {
			this.currencyOptions.push(new ChosenOptionModel(cur, cur));
		}

		this.orgSub = this.orgService.currentOrganisationObservable().subscribe(org => {
			if (org) {
				this.currency = org.CurrencyName;
			}
			this.newFnancialStatement.CurrencyName = this.currency;
		});
	}

	ngOnChanges(changes: any) {
		if (this.account) {
			this.i = 0;
			this.e = 1;
			this.showArrowLeftAllYears = true;
			this.newFnancialStatement.Revenue = "";
			this.newFnancialStatement.Employee = "";
			let newAccount = !this.account || (this.account.Id != this.account.Id);

			if (!this.selectedYear || newAccount) {
				if (this.account.FinancialStatements && this.account.FinancialStatements.length) {
					this.financialStatement = this.account.FinancialStatements[this.account.FinancialStatements.length - 1];
					this.selectedYear = this.financialStatement.Year;

					// if (this.account.FinancialStatements.length < 3) {
					// 	this.latestIndex = 3;
					// 	this.newestIndex = 0;
					// }

					// if (this.account.FinancialStatements.length >= 3) {

					// 	this.index = account.FinancialStatements.lastIndexOf(this.financialStatement);
					// 	this.latestIndex = this.index + 1;
					// 	this.newestIndex = this.latestIndex - 3;
					// }

					// 	if(this.account.FinancialStatements[this.account.FinancialStatements.length - 1].Year == this.financialStatement.Year){
					// 	this.ShowArrowRight = false;			
					// }else{this.ShowArrowRight = true}
					// 	var last_element_showSlice = this.account.FinancialStatements[this.account.FinancialStatements.length - this.account.FinancialStatements.length];

					// 	if(last_element_showSlice == this.financialStatement){
					// 		this.ShowArrowLeft = false;	
					// } else{this.ShowArrowLeft = true;}
				}

			}
			if (this.account.FinancialStatements && this.account.FinancialStatements.length) {
				this.setYear(this.selectedYear);
				this.showYears();
			}
		}

		for (let year = new Date().getFullYear(); year > 1989; year--) {
			this.showAllAvailabelYears.push(year);
		}
	}

	clickToClose(event: any) {
		var target = event.target || event.srcElement;
		if (target.id === 'edit-Year') {
			this.hideDeclartion();
		}
	}
	clickToCloseAdd(event: any) {
		var target = event.target || event.srcElement;
		if (target.id === 'add-Year') {
			this.hideDeclartionOnAdd();
		}
	}

	public showYears() {

		let testSlice = this.account.FinancialStatements.slice(this.beginOfSlice, this.endOfSlice)

		if (this.account.FinancialStatements.length > 3) {
			var first_element = this.account.FinancialStatements[0];
			var first_element_of_Slice = testSlice[0];

			this.ShowArrowLeft = !(first_element == first_element_of_Slice || first_element_of_Slice == undefined);


			var last_element = this.account.FinancialStatements[this.account.FinancialStatements.length - 1];
			var last_element_showSlice = testSlice[testSlice.length - 1]

			if (last_element.Year == last_element_showSlice.Year) {
				this.ShowArrowRight = false;
			} else {
				this.ShowArrowRight = true;
			}
		} else {
			this.ShowArrowLeft = false;
			this.ShowArrowRight = false;
		}
		this.showingYears = testSlice;
	}

	public allYears() {

		let showallYears = this.showAllAvailabelYears.slice(this.i, this.e)

		if (this.i == 0) {
			this.showArrowRightAllYears = false;
		} else {
			this.showArrowRightAllYears = true;
		}

		return showallYears;
	}

	public prevAllYears() {
		this.showArrowRightAllYears = true;

		this.i += 1;
		this.e += 1;

		var last_element = this.showAllAvailabelYears[this.showAllAvailabelYears.length - 1];
		var last_element_showallYears = this.allYears()[this.allYears().length - 1]

		if (last_element == last_element_showallYears) {
			this.showArrowLeftAllYears = false;
		}
	}

	public nextAllYears() {
		this.showArrowLeftAllYears = true;
		this.i -= 1;
		this.e -= 1;
		console.log()
	}

	public nextTwoYears() {

		this.beginOfSlice += 1;
		this.endOfSlice += 1;

		this.showYears();
	};

	public setYear(year: any) {
		this.financialStatement = this.account.FinancialStatements.find(x => x.Year == year);

		let wfafaw = this.account.FinancialStatements.findIndex(x => x.Year == year)

		console.log(wfafaw)

		if (wfafaw < 3) {
			this.beginOfSlice = 0;
			this.endOfSlice = 3;
		}

		if (wfafaw >= 3) {
			this.endOfSlice = wfafaw + 1;
			this.beginOfSlice = wfafaw - 2;
		}

	}

	public prevTwoYears() {

		this.beginOfSlice -= 1;
		this.endOfSlice -= 1;

		this.showYears();

	}

	public showDeclartionOnNumber(year: any) {
		this.financialStatement = year;
		this.displayUpdateDeclartion = true;
		this.editYear = true;
		this.updateNewCurrency = this.financialStatement.CurrencyName;
		this.updateNewRevunee = this.financialStatement.Revenue;
		this.updateNewEmployee = this.financialStatement.Employee;
	}

	public showDeclartion() {
		this.financialStatement = new FinancialStatement();
		for (let year = new Date().getFullYear(); year > 1989; year--) {

			if (this.account.FinancialStatements.find(x => x.Year == year.toString())) {
				this.showAfterDelete.push(new ChosenOptionModel(year, year))
			}
			else {
				this.availableYearsOptions.push(new ChosenOptionModel(year, year));
			}
		}
		this.financialStatement.CurrencyName = this.currency;
		this.financialStatement.Year = this.availableYearsOptions[0].value.toString();
		this.displayAddDeclartion = true;
		this.editYear = false;
	}

	public hideDeclartionOnAdd() {
		this.displayAddDeclartion = false;
		this.editYear = false;
		this.financialStatement = this.account.FinancialStatements[this.account.FinancialStatements.length - 1];
		this.selectedYear = this.financialStatement.Year;
		this.availableYearsOptions = [];
	}

	public hideDeclartion() {
		this.displayUpdateDeclartion = false;
		this.editYear = false;
		this.displayAddDeclartion = false;
		this.availableYearsOptions = [];
	}

	public addThisNewYear() {
		this.newFnancialStatement.Year = this.allYears().toString();
		console.log("[Account-Data] Trying to AddThisNewYear!", this.account);
		this._accountData.addYear(this.newFnancialStatement, this.account.Id);
	}

	public addYear() {
		console.log("[Account-Data] Trying to AddYear!", this.account);
		this._accountData.addYear(this.financialStatement, this.account.Id);
		this.selectedYear = this.financialStatement.Year;

		this.hideDeclartion();
	}

	public updateYear() {
		this.financialStatement.Employee = this.updateNewEmployee;
		this.financialStatement.Revenue = this.updateNewRevunee;
		this.financialStatement.CurrencyName = this.updateNewCurrency;
		this._accountData.updateYear(this.financialStatement, this.account.Id);
		this.selectedYear = this.financialStatement.Year;
		this.hideDeclartion();
	}

	public deleteYear() {
		var conf = confirm('Do you want to remove this Year?');
		if (conf) {
			this._accountData.deleteYear(this.financialStatement.Year, this.account.Id);
			this.hideDeclartion();

		}
	}

	ngOnDestroy() {
		if (this.orgSub) {
			this.orgSub.unsubscribe();
		}

	}

}
