import { Component } from '@angular/core';
import { ActionsCollection } from '../../../../services/actionservice';
import { AccountsCollection } from '../../../../services/accounts.service';
import { OrgService } from '../../../../services/organisation.service';
import { AccountsModel, AccountRelation } from '../../../../models/account.models';
import { Subscription } from 'rxjs/Subscription';
import { Router } from '@angular/router';
import { ActionsModel } from "../../../../models/actions.models";
import { ModalService } from "../../../../services/modal.service";

@Component({
    selector: 'account-details-modal',
    templateUrl: 'account-details-modal.html',
    styleUrls: ['account-details-modal.css']
})

export class AccountDetailsModal {
    private editmode: boolean = false;
    private editperson: boolean = false;
    private popupAccSub: Subscription;
    private relation: AccountRelation;
    public relationData: AccountRelation
    private personAccount: boolean;
    private actionGroupId: string;
    private personsRelations: AccountRelation[] = [];
    private timelineCompany: string;
    private actions: ActionsModel[] = [];
    private displayOpenActions: ActionsModel[] = [];
    private displayClosedActions: ActionsModel[] = [];

    private customFields: Array<any> = [];

    public tab = "";
    public account: AccountsModel;
    constructor(private orgService: OrgService, public accSvc: AccountsCollection, private router: Router, private actionSvc: ActionsCollection,
        public modal: ModalService) {

    }

    ngOnInit() {
        this.personAccount = this.router.routerState.snapshot.url.indexOf("PersonAccounts") == -1 ? false : true;
        this.popupAccSub = this.accSvc.popupAccountObservable().subscribe(data => {
            this.timelineCompany = "";
            this.relation = data;
            this.relationData = new AccountRelation(data);
            this.editmode = data.Id == null;
            this.editperson = data.Id == null;
            this.account = data.currentAccount;
            this.actionGroupId = data.actionGroupId;
            this.personsRelations = [];
            if (this.relation.PersonId) {

                this.accSvc.getRelationsForPerson(this.relation.PersonId, (success: boolean, relations: AccountRelation[]) => {
                    if (success) {
                        this.personsRelations = relations.filter(x => x.CompanyId != this.relationData.CompanyId);
                    }
                });
                this.getActions();
            }
        });
        this.customFields = this.orgService.currentOrganisation().RelationCustomFields;
    }

    getActions() {
        this.actionSvc.getActionsForAccountAndRelated(this.relation.PersonId, (success: boolean, data: ActionsModel[]) => {
            if (success) {
                this.actions = data;
                this.displayOpenActions = data.filter(x => x.Status == 'Open');
                this.displayClosedActions = data.filter(x => x.Status != 'Open');
            }
        });
    }

    setCustom(id: string, value: any) {
        if (!this.relationData.CustomData[id] || this.relationData.CustomData[id].toString() != value.toString()) {
            this.relationData.CustomData[id] = value;
        }
    }

    setTimelineFilter(companyId: string) {
        this.displayOpenActions = companyId == this.timelineCompany ? this.actions.filter(x => x.Status == 'Open') : this.actions.filter(x => x.AccountId == companyId && x.Status == 'Open');
        this.displayClosedActions = companyId == this.timelineCompany ? this.actions.filter(x => x.Status != 'Open') : this.actions.filter(x => x.AccountId == companyId && x.Status != 'Open');
        this.timelineCompany = companyId == this.timelineCompany ? null : companyId;
    }

    saveRelation() {
        console.log("Save relation", this.relationData);
        this.accSvc.saveRelation(this.relationData, () => {
            let keys = Object.keys(this.relationData)
            for (let key of keys) {
                this.relation[key] = this.relationData[key];
            }
            this.editmode = false;
            this.editperson = false;
            //this.relationData = null;
        });
    }

    close() {
        this.relationData = null;
    }
    clickToClose(e: any) {
        let target = e.target || e.srcElement;
        if (target.id === "accountdetailspopup") {
            this.close();
        }
    }

    closeModal() {
        this.relationData = null;
    }

    ngOnDestroy() {
        if (this.popupAccSub) this.popupAccSub.unsubscribe();
    }
}



