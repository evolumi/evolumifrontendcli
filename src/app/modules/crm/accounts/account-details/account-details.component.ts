import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ChatService } from '../../../../services/chatService';
import { TranslateService } from '@ngx-translate/core';
import { AccountsCollection } from '../../../../services/accounts.service';
import { NewsfeedService } from '../../../../services/newfeed.service';
import { NewsfeedModel } from '../../../../models/newsfeed.models';
import { ChosenOption } from "../../../shared/components/chosen/chosen";
import { ChosenOptionModel } from '../../../shared/components/chosen/chosen.models';
import { OrgService } from '../../../../services/organisation.service';
import { UsersService } from '../../../../services/users.service';
import { ButtonPanelService } from '../../../../services/button-panel.service';
import { Subscription } from "rxjs/Subscription";
import { AccountsModel } from "../../../../models/account.models";
import { environment } from "../../../../../environments/environment";
import { AuthService } from "../../../../services/auth-service";

@Component({

    templateUrl: 'account-details.html',
    styles: [`
    .title {
        border-bottom: 1px solid #eeeeee;
        display: block;
        padding-bottom: 10px;
        margin-bottom: 10px;
        font-weight: bold;
    }
    .clt, .clt ul, .clt li {
     position: relative;
    }

    .clt ul, ul {
        list-style: none;
        padding-left: 32px;
    }
    .clt li::before, .clt li::after {
        content: "";
        position: absolute;
        left: -12px;
    }
    .clt li::before {
        border-top: 1px solid #000;
        top: 9px;
        width: 8px;
        height: 0;
    }
    .clt li::after {
        border-left: 1px solid #000;
        height: 100%;
        width: 0px;
        top: 2px;
    }
    .clt ul > li:last-child::after {
        height: 8px;
    }

    `]
})

export class AccountDetailsComponent {
    private sub: any;

    public accountWorkGroups: Array<any> = [];
    public accountId: string = '';
    public newsFilterForm: any;
    public showEditAccount: boolean = false;
    public selectedNewsType: any = {};
    public Type: string = 'All Activity';
    public UserId: string = 'All';
    public usersSubscriber: any;
    public chatSubscriber: any;
    private canEdit: boolean;

    private personAccount: boolean;
    private customerSuccess: boolean = false;
    public users: Array<ChosenOption> = [];
    public activityArray: Array<ChosenOption> = [];

    public account: AccountsModel;

    private accSub: Subscription;
    private buttonSub: Subscription;

    constructor(
        public router: Router,
        public _accountCollection: AccountsCollection,
        public _userService: UsersService,
        public route: ActivatedRoute,

        public _newsfeedService: NewsfeedService,
        private _chatService: ChatService,
        private translate: TranslateService,
        public orgService: OrgService,
        private button: ButtonPanelService,
        public auth: AuthService
    ) {
        this.selectedNewsType.Type = 'All Activity';
        this.selectedNewsType.UserId = 'All';
        this.activityArray = [
            new ChosenOptionModel("All Activity", translate.instant("ALLACTIVITY")),
            new ChosenOptionModel("Account", translate.instant("ACCOUNTS")),
            new ChosenOptionModel("Deal", translate.instant("DEALS")),
            new ChosenOptionModel("Contact", translate.instant("CONTACTS")),
            new ChosenOptionModel("Note", translate.instant("NOTES"))
        ];
    }

    ngOnInit() {
        this.canEdit = this.orgService.hasPermission("EditOrganisationNotes");
        this.customerSuccess = this.orgService.currentOrganisation().Id == environment.customerSuccessOrg;

        this.sub = this.route.params.subscribe(params => {
            this.personAccount = this.router.routerState.snapshot.url.indexOf("PersonAccounts") == -1 ? false : true;
            this.accountId = params['accountId'];
            console.log("[Account-Details]" + this.accountId);

            this.button.removeButton('addAccount');
            this.button.addPanelButton(this.personAccount ? 'ADDPERSON' : 'ADDNEWCOMPANY', "addAccount", 'plus');

            if (!this.buttonSub || this.buttonSub.closed) {
                this.buttonSub = this.button.commandObservable().subscribe(command => {
                    if (command == 'addAccount') {
                        this.router.navigate(['CRM', this.personAccount ? 'PersonAccounts' : 'Accounts', 'AddAccount']);
                    }
                })
            }

            this._newsfeedService.reset();
            this._newsfeedService.clear();
            this.selectedNewsType = new NewsfeedModel({ AccountId: this.accountId });

            this.account = null;
            if (!this.accSub || this.accSub.closed) {
                this.accSub = this._accountCollection.currentAccountObserver().subscribe((data: AccountsModel) => {
                    console.log("[Account-Details] Current Account", data);
                    if (data && data.Id == this.accountId) {
                        this.account = new AccountsModel(data);
                        // Swich URL if somehow a person account gets linked to company URL and vice versa. 
                        if (data) {
                            if (!this.personAccount && this.account.IsPerson) {
                                this._accountCollection.searchParams.IsPerson = true;
                                this._accountCollection.getAccounts();
                                this.router.navigate(['CRM/PersonAccounts', this.account.Id]);
                            }
                            else if (this.personAccount && !this.account.IsPerson) {
                                this._accountCollection.searchParams.IsPerson = false;
                                this._accountCollection.getAccounts();
                                this.router.navigate(['CRM/Accounts', this.account.Id]);
                            }
                            this.getlongLat(this.account);
                            this.updateNews();
                        }
                    }
                });
            }

            if (!this.usersSubscriber || this.usersSubscriber.closed) {
                this.usersSubscriber = this._userService.activeUserListObervable().subscribe(res => {
                    if (res) {
                        this.users = [new ChosenOptionModel("All", this.translate.instant("ALLUSERS"))];
                        this.users = this.users.concat(res);
                        this.selectedNewsType.UserId = <string>this.users[0].value;
                    }
                });
            }

            if (!this.chatSubscriber || this.chatSubscriber.closed) {
                this.chatSubscriber = this._chatService.accountChannelsObservable().subscribe(response => {
                    this.accountWorkGroups = response;
                });
            }

            this._chatService.getWorkGroupForAccount(this.accountId);
            this._accountCollection.getAccount(this.accountId);
        });

    }

    deleteWorkGroup(id: string) {
        this._chatService.deleteConvrsation(id, () => {
            this.accountWorkGroups.splice(this.accountWorkGroups.findIndex(x => x.id == id), 1);
        });
    }

    updateNews() {
        this.selectedNewsType = new NewsfeedModel({ UserId: this.UserId, Type: this.Type, AccountId: this.accountId });
    }

    newsScroll($event?: any) {
        this._newsfeedService.makeSroll();
    }

    // Are here untill all old accounts have lat and long
    getlongLat(incomingAccount: any) {
        for (let address of incomingAccount.Address) {
            if (address.Latitude || address.Longitude || address.ShowAddress) {
            } else {
                this._accountCollection.updateAddressWithLongAndLat(address, incomingAccount);
            }
        }
    }

    ngOnDestroy() {
        if (this.usersSubscriber) this.usersSubscriber.unsubscribe();
        if (this.chatSubscriber) this.chatSubscriber.unsubscribe();
        if (this.sub) this.sub.unsubscribe();
        if (this.accSub) this.accSub.unsubscribe();
        if (this.buttonSub) this.buttonSub.unsubscribe();
        this.button.removeButton('addAccount');
    }
}

