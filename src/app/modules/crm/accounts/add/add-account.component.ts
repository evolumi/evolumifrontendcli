import { Component } from '@angular/core';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ValidationService } from '../../../../services/validation.service';
import { AccountsCollection } from '../../../../services/accounts.service';
import { ChosenOptionModel } from '../../../shared/components/chosen/chosen.models';
import { LocalStorage } from '../../../../services/localstorage';
import { OrgService } from '../../../../services/organisation.service';
import { UsersService } from '../../../../services/users.service';

@Component({
    templateUrl: './add-account.html',
    styleUrls: ['./addAccount.css']
})

export class AddAccountComponent {

    private sub: any;
    private userSub: any;
    private orgSub: any;
    public emailValidationPattern = '/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/';


    public accountForm: FormGroup;
    public tags: Array<string> = [];
    public addAcc: boolean = true;
    public takeMeToNewAccount: boolean = true;

    public accountTypes: any;
    public users: Array<ChosenOptionModel> = [];
    public personAccount: boolean;
    public accountList: Array<ChosenOptionModel>;
    public priceLists: Array<ChosenOptionModel> = [];
    private accountListPage: number = 1;
    public customFields: Array<any>;

    public priceListName: string;

    public accountId = '';
    public loggedinUser = '';

    //flags
    public showFullName: boolean;
    public showWebsite: boolean;
    public showEmail: boolean;
    public showPhone: boolean;
    public addParent: boolean;
    public clear: boolean;
    public showCompanyNumber: boolean;
    public showRefered: boolean;
    public editAccount: boolean;

    constructor(
        private location: Location,
        public _router: Router,
        public _accountsCollection: AccountsCollection,
        private userService: UsersService,
        private orgService: OrgService,
        public form: FormBuilder,
        public route: ActivatedRoute,
        public _localStore: LocalStorage) {

        this.personAccount = _router.routerState.snapshot.url.indexOf("PersonAccounts") != -1 || this._accountsCollection.addingContactForAccount != null;

        this._accountsCollection.createAccount();
        if (this.personAccount) {
            this.accountForm = form.group({
                ShownName: [''],
                FirstName: ['', Validators.required],
                LastName: ['', Validators.required],
                FullName: [''],
                CompanyNumber: [''],
                Website: [''],
                Email: ['', ValidationService.emailValidatorIfNotEmpty],
                Phone: [''],
                Vat: [''],
                AccountTypeId: [''],
                ParentAccount: [''],
                AccountManagerId: [''],
            });
        }
        else {
            this.accountForm = form.group({
                ShownName: ['', Validators.required],
                FirstName: [''],
                LastName: [''],
                FullName: [''],
                CompanyNumber: [''],
                Website: [''],
                Email: ['', ValidationService.emailValidatorIfNotEmpty],
                Phone: [''],
                Vat: [''],
                AccountTypeId: [''],
                ParentAccount: [''],
                AccountManagerId: [''],
            });
        }

        if (this._accountsCollection.addingContactForAccount) {
            this.takeMeToNewAccount = false;
        }

        this.sub = this.route.params.subscribe(params => {
            if (params['id'])
                this.accountId = params['id'];
        });
    }
    ngOnInit() {

        this._accountsCollection.account.IsPerson = this.personAccount;
        this.loggedinUser = this._localStore.get('userId');
        this._accountsCollection.account.AccountManagerId = this.loggedinUser;
        this._accountsCollection.account.IsPerson = this.personAccount;
        this.userSub = this.userService.activeUserListObervable().subscribe(users => {
            if (users) {
                this.users = users;
                for (let user of users) {
                    if (user.value == this.loggedinUser) {
                        this._accountsCollection.account.AccountManagerName = user.label.toString();
                    }
                }
            }
        });

        this.orgSub = this.orgService.currentOrganisationObservable().subscribe(org => {
            if (org) {
                this.accountTypes = [];
                this.customFields = this._accountsCollection.account.IsPerson ? org.PersonCustomFields : org.AccountCustomFields;

                this.accountTypes = org.AccountTypes;
                console.log(this.accountTypes);
                this._accountsCollection.account.AccountTypeId = org.AccountTypes[0].Id;
                this._accountsCollection.account.AccountTypeCollor = org.AccountTypes[0].Color;
                this._accountsCollection.account.AccountTypeName = org.AccountTypes[0].Name;


                this.priceLists = org.PriceLists;
                for (let a of org.PriceLists) {
                    if (a.Id == org.DefaultPriceList) {
                        this.selectPriceList(a);
                    }
                }
            }
        });

        this.updateAccountList("");
    }

    setManager(user: ChosenOptionModel) {
        this._accountsCollection.account.AccountManagerId = user.value.toString();
        this._accountsCollection.account.AccountManagerName = user.label.toString();
    }

    selectAccountType(type: any) {
        console.log("Select Type", type);
        this._accountsCollection.account.AccountTypeId = type.Id;
        this._accountsCollection.account.AccountTypeName = type.Name;
        this._accountsCollection.account.AccountTypeCollor = type.Color;
    }

    selectPriceList(pricelist: any) {
        this.priceListName = pricelist.Name;
        this._accountsCollection.account.PriceListId = pricelist.Id;
    }

    setCustom(id: string, value: any) {
        console.log('DATEVAL', value, id);
        if (!this._accountsCollection.account.CustomData[id] || this._accountsCollection.account.CustomData[id].toString() != value.toString()) {
            this._accountsCollection.account.CustomData[id] = value;
            console.log('DATE', value);
        }
    }

    private updateAccountList(search: string, scroll = false) {
        scroll ? this.accountListPage++ : this.accountList = [];
        this._accountsCollection.accountList(search, this.accountListPage, (accounts) => { this.accountList = [...this.accountList].concat(accounts) });
    }

    Create() {
        this.addAccount();
    }

    addAccount() {
        this._accountsCollection.account.Tags = this.tags;
        this._accountsCollection.addAccount(this._accountsCollection.account, this.takeMeToNewAccount).subscribe(res => {
            if (res.IsSuccess) {

                if (this.tags && this.tags.length) {
                    this.orgService.tagsChange(this.tags);
                }

                if (this._accountsCollection.addingContactForAccount) {
                    this._accountsCollection.addAccountAsContact(this._accountsCollection.addingContactForAccount.Id, res.Data, () => {
                        this._accountsCollection.currentAccount.ContactAccounts.push(res.Data);
                        this._accountsCollection.addingContactForAccount = null;
                    });
                }
                if (!this.takeMeToNewAccount)
                    this.back();
            }
        });
    }
    back() {
        this.location.back();
        this._accountsCollection.addingContactForAccount = null;
        //if (this.accountId !== '') {
        //    this._router.navigate(['/CRM/Accounts/', this.accountId]);
        //} else {
        //    this._router.navigate(['/CRM/Accounts']);
        //}
    }
    onitemsChange(event: any) {
        this.tags = event;
    }

    ontextboxChange(event: any) {
        //this.textTag = event.trim();
    }
    clickToClose(event: any) {
        var target = event.target || event.srcElement;
        if (target.id === 'add') {
            this.back();
        }
    }
    ngOnDestroy() {
        if (this.orgSub) this.orgSub.unsubscribe();
        if (this.userSub) this.userSub.unsubscribe();
        if (this.sub) this.sub.unsubscribe();
    }
}
