import { Component } from '@angular/core';
import { Location } from '@angular/common';

import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AccountsCollection } from '../../../services/accounts.service';
import { UsersService } from '../../../services/users.service';
import { ChosenOption } from "../../shared/components/chosen/chosen";
import { ChosenOptionModel } from '../../shared/components/chosen/chosen.models';
import { ModalService } from '../../../services/modal.service';
import { ActionsCollection, ActionGroup, ActionTypes } from '../../../services/actionservice';
import { OrgService } from '../../../services/organisation.service';
import { ButtonPanelService } from '../../../services/button-panel.service';
import { ActionsModel } from "../../../models/actions.models";
import { environment } from "../../../../environments/environment";
import { Subscription } from "rxjs/Subscription";
declare var $: JQueryStatic;

@Component({
    templateUrl: './accounts.html',
})

export class AccountsComponent {

    public accountTypes: Array<ChosenOption> = [];
    public users: Array<ChosenOption> = [];
    public sortOptions: Array<ChosenOption> = [];
    public personAccounts: boolean;

    public apiReady: boolean;
    public apiProgress: boolean;

    public optionsArray: Array<any> = [];

    public tagFilters: Array<ChosenOption> = [];
    public selectedTagFilter: string;

    public showExport: boolean;

    private orgSub: any;
    private userSub: any;
    private buttonSub: any;
    private actionSub: Subscription;

    constructor(
        public _loc: Location,
        public _accountsCollection: AccountsCollection,
        public translate: TranslateService,
        public _router: Router,
        public modal: ModalService,
        private orgService: OrgService,
        private userService: UsersService,
        private button: ButtonPanelService,
        private actionService: ActionsCollection
    ) { 
        let urlstring = this._router.routerState.snapshot.url;
        this.personAccounts = urlstring.indexOf("PersonAccounts") == -1 ? false : true;
        this._accountsCollection.searchParams.IsPerson = this.personAccounts;
    }

    ngOnInit() {
        this.sortOptions = [
            new ChosenOptionModel("", this.translate.instant("NAME")),
            new ChosenOptionModel("DateCreated", this.translate.instant("DATE")),
            new ChosenOptionModel("AccountTypeId", this.translate.instant("ACCTYPE")),
            new ChosenOptionModel("AccountManagerId", this.translate.instant("ACCMAN")),
        ];

        this.actionSub = this.actionService.actionChangeObservable().subscribe(action => {
            if (action) {
                let acc = this._accountsCollection.accounts.find(x => x.Id == action.AccountId);
                if (acc) {
                    if (action.Status == "Open") {
                        if (!acc.NextAction || acc.NextAction.Date > action.StartDate) {
                            acc.NextAction = { Date: action.StartDate, Type: action.Type, Id: action.Id, ActionGroupId: action.ActionGroupId };
                        }
                    }
                    else {
                        if (!acc.LastAction || acc.LastAction.Date < action.StartDate) {
                            acc.LastAction = { Date: action.StartDate, Type: action.Type, Id: action.Id, ActionGroupId: action.ActionGroupId };
                        }
                    }
                }
            }
        })

        this._accountsCollection.accounts = [];
        this._accountsCollection.selectedAccounts = [];

        this.button.addPanelButton("IMPORT", this.personAccounts ? "importpersons" : "importcompanies", "download");
        if (this.orgService.hasPermission("Export"))
            this.button.addPanelButton("EXPORT", "export", "upload");
        this.button.addPanelButton(this.personAccounts ? 'ADDPERSON' : 'ADDNEWCOMPANY', "addaccount", "plus");

        this.buttonSub = this.button.commandObservable().subscribe(command => {
            if (command == "export") { this.runExport() }
            if (command == "addaccount") { this._router.navigate(['CRM', this.personAccounts ? "PersonAccounts" : "Accounts", 'AddAccount']) }
        });

        // Subscribe on org change for AccountTypes and Tags.
        this.orgSub = this.orgService.currentOrganisationObservable().subscribe(org => {
            if (org) {
                for (let a of org.AccountTypes) {
                    this.accountTypes.push(new ChosenOptionModel(a.Id, a.Name));
                }

                // Add sorting on LastActive to Customersuccess org
                if (this.orgService.hasPermission("EvolumiAdmin") && org.Id == environment.customerSuccessOrg) {
                    var field = org.AccountCustomFields.find(x => x.Id == environment.lastActiveCustomField);
                    if (field) {
                        this.sortOptions.push(new ChosenOptionModel("CustomData." + field.Id, field.Name));
                    }
                }
            }
        });

        this.userSub = this.userService.userListObervable().subscribe(users => {
            this.users = this.users.concat(users);
        });
        this.getTagFilters();
    }

    accountIdsChange(account: any) {
        if (this._accountsCollection.selectAll) {
            this.checkAll();
        }
        else {
            console.log("SelectAccount", account);
            this._accountsCollection.selectedAccounts = [];
            for (let a of this._accountsCollection.accounts) {
                if (a.IsChecked)
                    this._accountsCollection.selectedAccounts.push(a.Id);
            }

            console.log("SelectedAccount", this._accountsCollection.selectedAccounts);
        }
    }

    clickAction(action: ActionsModel) {
        this.actionService.showGroup(action.ActionGroupId, action.Id);
    }

    newAction(accountId: string) {
        var group = new ActionGroup();
        group.AccountId = accountId;
        this.actionService.newGroup(group)
    }

    getIcon(type: string) {
        var t = ActionTypes.find(x => x.value == type);
        if (!t) {
            console.log(type);
        }
        return t ? t.icon : "";
    }

    back() {
        this._loc.back();
    };

    delete(id: string) {
        var conf = confirm(this.translate.instant('WARNDELACC'));
        if (conf) {
            this._accountsCollection.deleteAccount(id);
        }
    }

    runExport(): void {
        this.showExport = true; // for new export
        // this._accountsCollection.exportAccounts(); // for old export
    }

    //mobile view
    toggleFiterbar() {
        $('.evo-filter-container').hasClass('visible') ? $('.evo-filter-container').removeClass('visible') : $('.evo-filter-container').addClass('visible');
    }

    public checkAll() {
        if (this._accountsCollection.selectAll) {
            this._accountsCollection.selectedAccounts = [];
            for (let acc of this._accountsCollection.accounts)
                acc.IsChecked = true;
        }
        else {
            this._accountsCollection.selectedAccounts = [];
            for (let acc of this._accountsCollection.accounts) {
                acc.IsChecked = false;
            }
        }
        console.log("[Account-List]" + this._accountsCollection.selectAll ? "Selecting all" : "Deselecting all");
    }

    deleteSelectedAccounts() {
        console.log(this._accountsCollection.selectedOption);
        if (this._accountsCollection.selectedAccounts.length > 0 || this._accountsCollection.selectAll) {
            let conf = confirm(this.translate.instant('WARNDELACCS'))
            if (conf) {
                this._accountsCollection.deleteMany(this._accountsCollection.selectedAccounts);

            }
        }
        else
            alert(this.translate.instant('WARNNOSELECT'));
    }

    accountOptions(event: string) {

        if (event === 'delete')
            this.deleteSelectedAccounts();
        else if (event === 'addtags') {
            this._router.navigateByUrl('CRM/Accounts/Options/AddTags');
        }
        else if (event === 'removetags') {
            this._router.navigateByUrl('CRM/Accounts/Options/RemoveTags');
        }
        else if (event === 'massmail') {
            if (this._accountsCollection.selectAll) {
                this.modal.showMassMailFilterModal(this._accountsCollection.getSearchString());
            }
            else {
                this.modal.showMassMailModal(this._accountsCollection.selectedAccounts);
            }
        }
        else if (event === 'changeacm') {
            this._router.navigateByUrl('CRM/Accounts/Options/ChangeAccountManager');
        }
        else if (event === 'changeactype') {
            this._router.navigateByUrl('CRM/Accounts/Options/ChangeAccountType');
        }
        else if (event === 'addaction') {
            let group = new ActionGroup();
            group.multi = true;
            if (this._accountsCollection.selectAll) {
                group.accountQuery = this._accountsCollection.searchParams;
            }
            else {
                group.accountIds = this._accountsCollection.selectedAccounts;
            }

            this.actionService.newGroup(group);
        }
        else if (event === 'showmap') {
            this._accountsCollection.showMap();
        }
        else
            return false;
        return true;
    }

    changeTagFilter(event: any, noReload?: boolean) {
        this.selectedTagFilter = event.label;
        switch (event.value) {
            case 'TagsAll':
                this._accountsCollection.searchParams.MustHaveAllTags = true;
                break;
            case 'TagsAny':
                this._accountsCollection.searchParams.MustHaveAllTags = false;

        }
        if (!noReload) {
            this._accountsCollection.search();
        }

    }

    private getTagFilters() {
        const allLabel = this.translate.instant('TAGSALL');
        const anyLabel = this.translate.instant('TAGSANY');
        this.tagFilters = [
            { label: allLabel, value: 'TagsAll' },
            { label: anyLabel, value: 'TagsAny' }
        ]
        this.changeTagFilter(this.tagFilters[1], true);
    }

    ngOnDestroy() {
        if (this.orgSub) this.orgSub.unsubscribe();
        if (this.userSub) this.userSub.unsubscribe();
        if (this.buttonSub) this.buttonSub.unsubscribe();
        if (this.actionSub) this.actionSub.unsubscribe();

        this.button.removeButtons(["export", "importpersons", "importcompanies", "addaccount"]);

    }
}
