import { Component, Output, EventEmitter, Input } from '@angular/core';
import { Router } from '@angular/router';
import { ButtonPanelService } from '../../../../services/button-panel.service';
import { TranslateService } from '@ngx-translate/core';
import { GoogleApiService } from "../../../../services/google-api.service";

//declare var MarkerClusterer : any;

@Component({
    selector: 'map-company-selector',
    templateUrl: 'mapCompany.html',
    styleUrls: ['mapCompany.css']
})

export class MapCompanyComponent {
    @Output() close: EventEmitter<any> = new EventEmitter();
    @Input() incomingAddress: Array<any>;
    public currentOrg: any;
    public addressType: any;
    public textContext: any;
    public buttonSub: any;
    public htmlContent: any;

    constructor(
        public _button: ButtonPanelService,
        public route: Router,
        public translate: TranslateService,
        private googleSvc: GoogleApiService
    ) {
        this.htmlContent = this.translate.instant('NOSTREETVIEWAVA');
    };
    ngOnInit() {
        (<any>window).myFunction = (id: any) => {
            this.route.navigateByUrl('/CRM/Accounts/' + id);

            if (this.route.isActive('/CRM/Accounts/' + id, true)) {
                this.closeMap();
            }
        }
        this.googleSvc.initMap().then(() => { console.log('GOOGLESCRIPTLOADED') });
    }

    ngAfterViewInit() {

        var markers: Array<any> = [];
        var bounds = new google.maps.LatLngBounds();
        var showGoogleMapsStreet: boolean = true;
        var sv = new google.maps.StreetViewService();
        let that = this;
        // for (let address of this.incomingAddress) {
        //     let textAdress: string;
        //     textAdress = address.Street;
        //     textAdress += " ";
        //     textAdress += address.State;
        //     textAdress += " ";
        //     textAdress += address.PostalCode;
        //     textAdress += " ";
        //     textAdress += address.City;
        //     textAdress += " ";
        //     textAdress += address.Country;
        //     locations.push(textAdress);
        // }
        var mapOptions = {
            zoom: 4,
            streetViewControl: false,
        };

        var map = new google.maps.Map(document.getElementById("map"), mapOptions);

        if (this.incomingAddress.length) {
            addMarker(this.incomingAddress);
        }

        function addMarker(addresses: Array<any>) {
            var infowindowZindexCounter: number = 1;
            for (let counter = 0; counter < addresses.length; counter++) {
                if (addresses[counter].Latitude && addresses[counter].Longitude) {
                    let addressLat = Number(addresses[counter].Latitude);
                    let addressLong = Number(addresses[counter].Longitude);
                    let addressPosition = { lat: addressLat, lng: addressLong };

                    // If you wanna have another color then google can provide
                    // var pinColor = "#7FFF00";
                    // var pinImage = new google.maps.MarkerImage('http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|' + pinColor,
                    //     new google.maps.Size(21, 34),
                    //     new google.maps.Point(0, 0),
                    //     new google.maps.Point(10, 34));

                    let markerOptions = {
                        position: addressPosition,
                        map: map,
                        animation: google.maps.Animation.DROP,
                        icon: addresses[counter].MarkerCollor,
                        // label: labels[labelIndex++ % labels.length],
                        // icon: {
                        //     path: 'https://maps.gstatic.com/mapfiles/api-3/images/spotlight-poi-dotless.png',
                        //     fillColor: 'blue',
                        //     scale: 3
                        // }
                        // icon: pinImage,
                    };

                    let marker = new google.maps.Marker(markerOptions);

                    markers.push(marker);
                    bounds.extend(marker.getPosition());
                    let showInfoWindow: boolean = false;
                    let infowindow: google.maps.InfoWindow;
                    let accountClickId = addresses[counter].AccountId;
                    let textContent = `<a onclick="myFunction('${accountClickId}')" >` + addresses[counter].AccountName + '</a>' + '<br>' + addresses[counter].Type + '<br>' + addresses[counter].ShowAddress + '<br>' +
                        `<div *ngIf="showGoogleMaps" id="${addresses[counter].ShowAddress}" style="width:100%;height:200px;"></div><div id="${addresses[counter].ShowAddress + addresses[counter].Street}" *ngIf="!showGoogleMaps" style="width:100%;">${that.htmlContent}</div>`;

                    // If you just want an image instead of streetview
                    // <br><p><img src='https://maps.googleapis.com/maps/api/streetview?size=250x200&location=${addressLat},${addressLong}&key=AIzaSyD9--Zwso4bSeABenzM3j2mxEyULh1_h0A'></p>

                    google.maps.event.addListener(marker, 'click', function (event: any) {
                        infowindowZindexCounter++;
                        showInfoWindow = !showInfoWindow;
                        if (showInfoWindow) {
                            infowindow = new google.maps.InfoWindow({
                                content: textContent,
                                zIndex: infowindowZindexCounter,
                            });
                            infowindow.open(map, marker);
                            google.maps.event.addListener(infowindow, 'closeclick', function () {
                                showInfoWindow = !showInfoWindow;
                            })
                        } else {
                            infowindow.close();
                        }
                        if (infowindow) {
                            setTimeout(function () {
                                var panorama = new google.maps.StreetViewPanorama(document.getElementById(`${addresses[counter].ShowAddress}`), {
                                    addressControlOptions: {
                                        position: google.maps.ControlPosition.BOTTOM_CENTER
                                    },
                                    linksControl: false,
                                    panControl: false,
                                    addressControl: false,
                                });
                                sv.getPanorama({ location: addressPosition, radius: 50 }, processSVData);
                                function processSVData(data: any, status: any) {
                                    if (status === 'OK') {
                                        showGoogleMapsStreet = true;
                                        document.getElementById(addresses[counter].ShowAddress + addresses[counter].Street).remove();
                                        let heading = google.maps.geometry.spherical.computeHeading(data.location.latLng, marker.getPosition());
                                        panorama.setPano(data.location.pano);
                                        panorama.setPov({
                                            heading: heading,
                                            pitch: 0
                                        });
                                        panorama.setVisible(true);
                                    } else {
                                        showGoogleMapsStreet = false;
                                        document.getElementById(addresses[counter].ShowAddress).remove();
                                        console.error('Street View data not found for this location.', status);
                                    }
                                }
                            }, 30);
                        }
                    });
                    // google.maps.event.addListener(marker, 'dblclick', function () {
                    //     let getPositionOfMarker = marker.getPosition()
                    //     let currentZoom = map.getZoom();
                    //     currentZoom++;
                    //     map.setCenter(getPositionOfMarker);
                    //     map.setZoom(currentZoom);
                    // });
                    map.fitBounds(bounds);
                }
            }
            // var markerCluster = new MarkerClusterer(map, markers,
            //     {
            //         imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m',
            //         gridSize: 23,
            //         maxZoom: 15,
            //     });
        }

    }

    // scriptMarkerCluster() {
    //     this.clusterScript = document.createElement('script');
    //     this.clusterScript.type = "text/javascript";
    //     this.clusterScript.src = "https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js";
    //     $("head").append(this.clusterScript);
    // }
    closeMap() {
        this.close.next();
    }
    clickToCloseMap(event: any) {
        var target = event.target || event.srcElement;
        if (target.id === 'popupMap') {
            this.closeMap();
        }
    }
    ngOnDestroy() {

    }
}