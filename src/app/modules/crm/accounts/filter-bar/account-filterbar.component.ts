import { Component, OnInit, Output, EventEmitter, Input, SimpleChanges } from '@angular/core';
import { AccountsCollection, filterContainer, SearchParams } from '../../../../services/accounts.service';
import { Router } from "@angular/router";

@Component({
    selector: 'account-filterbar',
    templateUrl: 'account-filterbar.component.html',
    styleUrls: ['account-filterbar.component.css']
})
export class AccountFilterbarComponent {

    public selectedFilter: filterContainer;
    public filterName: string;
    public editMode: boolean = false;
    public changingFilter: boolean = false;
    public organisation: any;
    public showFeature: boolean;
    public showMap: boolean = false;

    @Output() command: EventEmitter<string> = new EventEmitter();
    @Input() enableButtons: boolean;
    @Input() searchFilter: SearchParams;
    private personAccounts: boolean;

    constructor(public accountService: AccountsCollection, private _router : Router) {
        let urlstring = this._router.routerState.snapshot.url;
        this.personAccounts = urlstring.indexOf("PersonAccounts") == -1 ? false : true;
        if (this.accountService.defaultFilter) {
            this.changingFilter = true;
            this.setFilter(this.accountService.defaultFilter);
        }
        else {
            this.setFilter('');
        }        
    }

    ngOnInit(){

    }

    public saveFilter() {
        this.selectedFilter.Filter = this.searchFilter;
        this.selectedFilter.Filter.Page = 1;
        this.selectedFilter.Filter.PageSize = 40;
        this.accountService.addorUpdateFilter(this.selectedFilter);
        this.editMode = false;
    }

    public createFilter() {
        this.selectedFilter = new filterContainer();
        this.editMode = true;
    }

    public deleteFilter() {
        this.accountService.deleteFilter(this.selectedFilter.Id);
        this.editMode = false;
    }

    public setDefaultFilter() {
        this.accountService.updateDefaultFilter(this.selectedFilter.Id);
    }
    removeDefaultFilter() {
        this.accountService.updateDefaultFilter("");
    }

    public setFilter(filterId: string) {

        let filter = this.accountService.filters.find(x => x.Id == filterId);
        if (filter) {
            filter.Filter.Page = 1;
            filter.Filter.PageSize = 40;
            filter.Filter.IsPerson = this.personAccounts;
            this.changingFilter = true;
            this.accountService.setFilter(filter);
            this.filterName = filter.Name;
            this.selectedFilter = filter;
        }
        else {
            this.accountService.reset(this.personAccounts);
            this.accountService.getAccounts();
            this.selectedFilter = new filterContainer();
        }
        this.editMode = false;
    }

    public buttonClick(command: string) {
        if (this.accountService.selectAll || this.accountService.selectedAccounts.length)
            this.command.emit(command);
    }

    ngOnChanges(changes: SimpleChanges) {

        if (changes["searchFilter"] && !this.changingFilter && !this.editMode) {
            this.selectedFilter = new filterContainer();
        }
        if (changes["searchFilter"] && this.changingFilter) {
            this.changingFilter = false;
        }
    }
    ngOnDestroy() {
    }

}