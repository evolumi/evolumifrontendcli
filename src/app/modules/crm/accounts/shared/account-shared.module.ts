import { NgModule } from '@angular/core';
import { SharedModule } from '../../../shared/shared.module';

import { AccountInfoComponent } from '../account-details/account-info.component';
import { AccountEditComponent } from '../edit/account-edit.component';
import { AccountDataComponent } from '../account-details/account-data.component';
import { AddressListComponent } from '../address/address-list.component';
import { DaughterAccountsListComponent } from '../daughter-companies/daughter-accounts.component';
import { ContactBlockComponent } from '../contacts/contactblock/contact-block.component';
import { RelativesBlockComponent } from '../contacts/relativesblock/relatives-block.component';
import { MapCompanyComponent } from '../mapCompany/mapCompany.component';
import { AdminAccountDataComponent } from "../admin-account-data/admin-account-data.component";
import { FileBlockComponent } from "../fileblock/file-block.component";


@NgModule({
    imports: [
        SharedModule
    ],
    declarations: [
        AccountInfoComponent,
        AccountEditComponent,
        AccountDataComponent,
        AddressListComponent,
        DaughterAccountsListComponent,
        ContactBlockComponent,
        RelativesBlockComponent,
        MapCompanyComponent,
        AdminAccountDataComponent,
        FileBlockComponent
    ],
    exports: [
        AccountInfoComponent,
        AccountEditComponent,
        AccountDataComponent,
        AddressListComponent,
        DaughterAccountsListComponent,
        ContactBlockComponent,
        RelativesBlockComponent,
        MapCompanyComponent,
        AdminAccountDataComponent,
        FileBlockComponent
    ]
})
export class AccountSharedModule { }