import { Component, Input } from '@angular/core';
import { AccountsModel } from '../../../../models/account.models';
import { AccountsCollection } from '../../../../services/accounts.service';

@Component({
    selector: 'referred-tree',
    templateUrl: 'referred-tree.html',
    styleUrls: ['referred-tree.css']
})

export class ReferredTreeComponent {

    @Input() account: AccountsModel;
    public referData: ReferTreeModel[] = [];

    constructor(
        private accSvc: AccountsCollection
    ) { }

    ngOnChanges(changes: any) {
        if (changes["account"] && changes["account"].currentValue) {
            this.initViewTree(changes["account"].currentValue.Id);
        }
    }

    initViewTree(accountId: string) {
        this.accSvc.getReferredAccounts(accountId, (referredAccounts: ReferTreeModel[]) => {
            this.referData = referredAccounts;
        });
    }

    getTreeLevel(account: ReferTreeModel, event: any) {
        event.stopPropagation();
        account.Expanded = !account.Expanded;
        if (!account.Referred || account.Referred.length == 0) {
            this.accSvc.getReferredAccounts(account.Id, (referredAccounts: ReferTreeModel[]) => {
                account.Referred = referredAccounts;
            });
        }
    }
}

export class ReferTreeModel {
    public Id: string;
    public Name: string;
    public Color: string;
    public IsPerson: boolean;
    public Expanded: boolean = false;
    public Referred: ReferTreeModel[] = [];
    [key: string]: any;

    constructor(obj: any = null) {
        this.Referred = [];
        for (let key in obj) {
            if (obj.hasOwnProperty(key)) {
                this[key] = obj[key]
            }
        }
    }
}