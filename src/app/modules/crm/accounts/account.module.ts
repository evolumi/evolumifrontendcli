import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { AccountSharedModule } from './shared/account-shared.module';
import { DealsSharedModule } from '../deals/shared/deals-shared.module';
import { ActionsSharedModule } from '../actions/shared/action-shared.module';
import { ChannelsModule } from '../channels/channels.module';

import { AccountsComponent } from './accounts.component';
import { AccountComponent } from './account-details/account.component';
import { AccountDetailsComponent } from './account-details/account-details.component';
import { AddAccountComponent } from './add/add-account.component';
import { ImportComponent } from './import/import.component';
import { LeftPanelComponent } from './left-panel.component';
import { AccountOptionsComponent } from './account-options/account-options.component';
import { AddressAddComponent } from './address/add/address-add.component';
import { ContactModalComponent } from './contacts/contact-modal.component';
import { DocumentBlockComponent } from './documents/documentblock/document-block.component';
import { DocumentModalComponent } from './documents/document-modal.component';
import { AccountFilterbarComponent } from './filter-bar/account-filterbar.component';
import { AccountDetailsModal } from './account-details/account-details-modal.component';
import { ReferredTreeComponent } from './referredtree/referredtree.component';
// import { MapCompanyComponent } from './mapCompany/mapCompany.component';

@NgModule({
    imports: [
        SharedModule,
        DealsSharedModule,
        ActionsSharedModule,
        AccountSharedModule,
        ChannelsModule
    ],
    declarations: [
        AccountsComponent,
        AccountComponent,
        AccountDetailsComponent,
        AddAccountComponent,
        ImportComponent,
        LeftPanelComponent,
        AccountOptionsComponent,
        AddressAddComponent,
        ContactModalComponent,
        DocumentBlockComponent,
        DocumentModalComponent,
        AccountFilterbarComponent,
        AccountDetailsModal,
        ReferredTreeComponent,
        // MapCompanyComponent,
    ],
    exports: [DocumentModalComponent, ContactModalComponent, AccountDetailsModal]
})
export class AccountModule { }