import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AccountsCollection } from '../../../../services/accounts.service';
import { UsersService } from '../../../../services/users.service';
import { OrgService } from '../../../../services/organisation.service';
import { ChosenOptionModel } from '../../../shared/components/chosen/chosen.models';

@Component({
    templateUrl: './account-options.html',
})

export class AccountOptionsComponent {
    private sub: any;
    public accountIds: Array<string>;
    public task: string;

    private userSub: any;
    private orgSub: any;

    public accountTypes: Array<ChosenOptionModel> = [];
    public users: Array<ChosenOptionModel> = [];
    public accType: string;
    public accManager: string;
    public tags: Array<string> = [];
    constructor(
        public route: ActivatedRoute,
        public _router: Router,
        private orgService: OrgService,
        public _accountsCollection: AccountsCollection,
        public userService: UsersService) { }
    ngOnInit() {
        this.accountIds = this._accountsCollection.selectedAccounts;
        this.sub = this.route.params.subscribe(params => {
            this.task = params['task'];
            if (this.task === 'ChangeAccountManager') {
                this.userSub = this.userService.activeUserListObervable().subscribe(res => {
                    this.users = [];
                    if (res.length > 0) {
                        console.log("[Accounts-Options] Loaded active users [" + res.length + "]");
                        console.log(res);
                        for (let user of res) {
                            this.users.push(user);;
                        }
                        this.accManager = res[0].value.toString();
                    }
                })
            }
            if (this.task === 'ChangeAccountType') {
                this.orgSub = this.orgService.currentOrganisationObservable().subscribe(res => {
                    if (res) {
                        this.accountTypes = [];
                        console.log("[Accounts-Options] Loaded account Types", res.AccountTypes);

                        for (let type of res.AccountTypes) {
                            this.accountTypes.push(new ChosenOptionModel(type.Id, type.Name));;
                        }
                        this.accType = res.AccountTypes[0].value;
                    }
                })
            }
        });
    }

    clickToClose(event: any) {
        var target = event.target || event.srcElement;
        if (target.id === 'add') {
            this.back();
        }
    }
    back() {
        this._router.navigate(['/CRM', 'Accounts']);
    }
    onitemsChange(event: any) {
        this.tags = event;
    }
    add() {
        if (this.accountIds.length > 0 || this._accountsCollection.selectAll) {
            let data = {
                'Ids': this.accountIds,
                'Labels': this.tags
            };
            this._accountsCollection.addTagsToMany(data);
            this.back();
        }
    }
    changeAccountManager() {
        if (this.accountIds.length > 0 || this._accountsCollection.selectAll) {

            let data = {
                'AccountIds': this.accountIds,
                'ManagerId': this.accManager
            };
            this._accountsCollection.updateAccountManagerOnMany(data);
            this.back();
        }
    }

    changeAccountType() {
        if (this.accountIds.length > 0 || this._accountsCollection.selectAll) {

            let data = {
                'AccountIds': this.accountIds,
                'ManagerId': this.accType
            };
            this._accountsCollection.updateAccountTypeOnMany(data);
            this.back();
        }
    }

    removeTagsFromMany() {
        if ((this.accountIds.length > 0 || this._accountsCollection.selectAll) && this.tags.length > 0) {
            let data = {
                'Ids': this.accountIds,
                'Labels': this.tags
            };
            console.log(data);
            let conf = confirm('You are going to remove tags from the selected accounts. Are you sure?')
            if (conf) {

                this._accountsCollection.removeTagsFromMany(data);

            }
        }
        else
            alert('Nothing selected');
        this.back();
    }

    ngOnDestroy() {
        if (this.sub) this.sub.unsubscribe();
        if (this.userSub) this.userSub.unsubscribe();
        if (this.orgSub) this.orgSub.unsubscribe();
    }

}