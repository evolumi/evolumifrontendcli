import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AccountsCollection } from '../../../services/accounts.service';

@Component({

  selector: 'left-panel',
  template: `		
<div [class.systemActive]="sideMenuActive" class="leftPanel" scrollTracker (scrollBottom)="_accountCollection.onScrollBottom();">
  <div class="leftPanelFilter">
    <div class="form-group">
        <label for="">&nbsp;</label>
         <select id="LeftPanelFilter" (change)="changeFilter(options.value)" class="form-control" #options [(ngModel)]="selectOption">
             <option *ngFor="let opt of optionsArray" [value]="opt.key">{{opt.value | translate }}</option>
                                  
          </select>
    </div>
   <div class="leftPanelSerach">
    <input id="LeftPanelSearch" type="text" (keyup)="_accountCollection.UpdateSearchFields({Search:name.value, IsPerson: personAccounts })" [(ngModel)]="searchParams.Search" class="form-control" placeholder="{{ 'SEARCH' | translate }}" #name>
   </div>
  </div>

  <ul class="menuItems">
    
    <li *ngIf="_accountCollection.accounts.length==0"> <a>{{'NORECORDSFOUND' | translate }}</a></li>

    <li *ngFor="let acc of _accountCollection.accounts">
      <a (click)="close()" [routerLink]="['../'+acc.Id]" routerLinkActive="active"><div class="accountLabel" [style.background]="acc.AccountTypeCollor"></div>{{ acc.ShownName }}</a>
    </li>

  </ul>
   
  <div class="mobileSideMenu" (click)="sideMenuActive = true;">
    <ul class="dots">
      <li></li>
      <li></li>
      <li></li>
    </ul>
  </div>
</div>
<div [class.systemActive]="sideMenuActive" class="popupOverlay" id="SideMenuOverlayLeft" (click)="close()"></div>
	`
})
export class LeftPanelComponent {
  public optionsArray: Array<any> = [];
  public selectOption: string = 'all';

  public searchParams: any;
  private savePage: any;
  private saveSize: any;
  public sideMenuActive: boolean;

  public personAccounts: boolean;

  constructor(public _accountCollection: AccountsCollection, public router: Router) {
    this.personAccounts = this.router.routerState.snapshot.url.indexOf("PersonAccounts") == -1 ? false : true;
  }

  ngOnInit() {

    this.searchParams = this._accountCollection.searchParams;
    this.searchParams.IsPerson = this.personAccounts;
    this.savePage = this.searchParams.Page;
    this.saveSize = this.searchParams.PageSize;

    this.searchParams.PageSize = this.searchParams.Page * this.searchParams.PageSize;
    this.searchParams.Page = 1;

    // If list is empty or if route is changed between Company and Persons. Tell the service to reset search.
    if (!this._accountCollection.accounts.length || this._accountCollection.accounts[0].IsPerson != this.personAccounts) this.changeFilter('all');

    this.optionsArray = [
      { 'key': 'all', 'value': 'SHOWALL' },
      { 'key': 'selected', 'value': 'SHOWSELECTION' }
    ];
    for (var key in this.searchParams) {
      if (this.searchParams.hasOwnProperty(key)) {
        if (this.searchParams[key] !== '' && key !== 'Page' && key !== 'PageSize')
          this.selectOption = 'selected';
      }
    }
  }

  changeFilter(val: any) {
    if (val === 'all') {
      this._accountCollection.reset(this.personAccounts);
      this._accountCollection.getAccounts();
    }
    else if (val === 'selected') {

      this._accountCollection.UpdateSearchFields(this.searchParams);

      this._accountCollection.searchParams.PageSize = this.saveSize;
      this._accountCollection.searchParams.Page = this.savePage;
    }
  }

  close() {
    this.sideMenuActive = false;
  }

  ngOnDestroy() {
  }
}
