import { Component, Input } from '@angular/core';
import { Api } from '../../../../services/api';

@Component({

    selector: 'admin-account-data',
    styleUrls: ['./admin-accounts.css'],
    templateUrl: './admin-account-data.component.html',
})

export class AdminAccountDataComponent {

    //This will show activity data on a Organisation. 
    //Its only suposed to be used in the CustomerSuccess Org, Where AccountId actualy match a OrganisationId.

    @Input() account: any;

    public summary: any;

    private startDate: Date = undefined;
    private endDate: Date = undefined;
    private userId: string = "";
    private selectedUser: string = "";

    private page: number = 0;

    constructor(private api: Api) {

    }

    clickUser(id: string) {
        if (this.selectedUser == id) {
            this.selectedUser = "";
        }
        else {
            this.selectedUser = id;
        }
    }

    ngOnChanges() {
        this.getActivity();
    }

    getActivity() {

        console.log("Getting activity:", this.startDate, this.endDate, this.userId);

        let query = "?Page=" + this.page + "&PageSize=200"
            + (this.startDate ? "&StartDate=" + this.startDate + "&EndDate=" + this.endDate : "")
            + (this.userId ? "&UserId" + this.userId : "");

        if (this.account) {
            this.api.get("Reports/ActivitySummary/" + this.account.Id + query).map(res => res.json()).subscribe(
                res => {
                    console.log("[Admin] ", res.Data);
                    this.summary = res.Data;
                }
            );
        }
    }

}