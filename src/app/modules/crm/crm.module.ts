import { NgModule } from '@angular/core';
import { SettingsModule } from './settings/settings.module';
import { SettingsSharedModule } from './settings/shared/settings-shared.module';
import { AccountModule } from './accounts/account.module';
import { AccountSharedModule } from './accounts/shared/account-shared.module';
import { ActionsModule } from './actions/actions.module';
import { DealsModule } from './deals/deals.module';
import { DealsSharedModule } from "./deals/shared/deals-shared.module";
import { InsightsModule } from './insights/insights.module';
import { SharedModule } from '../shared/shared.module';
import { HeaderModule } from '../header/header.module';
import { ContractsModule } from './contracts/contracts.module';
import { SalesCompetitionModule } from './sales-competition/sales-competition.module';
import { ChannelsModule } from "./channels/channels.module";
import { DashboardModule } from "./dashboard/dashboard.module";
import { CalendarModule } from './calendar/calendar.module';
import { PackagesModule } from './packages/packages.module';
import { NewslettersModule } from './newsletters/newsletters.module';
import { CRMComponent } from './crm.component';
import { PayComponent } from './pay/pay.component';
// import { CrmRoutingModule } from "./crm-routing.module";

@NgModule({
    imports: [
        SharedModule,
        HeaderModule,
        AccountModule,
        ActionsModule,
        DealsModule,
        DealsSharedModule,
        ContractsModule,
        InsightsModule,
        SettingsModule,
        SalesCompetitionModule,
        ChannelsModule,
        DashboardModule,
        PackagesModule,
        SettingsSharedModule,
        NewslettersModule,
        CalendarModule,
        AccountSharedModule,
        // CrmRoutingModule // for lazy loading
    ],
    declarations: [
        PayComponent,
        CRMComponent
    ]
})
export class CrmModule { }