import { Component, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { Api } from '../../../../services/api';
import { DealService } from '../../../../services/deal.service';
import { DealListing } from '../../../../services/deallisting.service';
import { DragulaService } from '../../../../draggable/providers/dragula.provider';
import { NotificationService } from '../../../../services/notification-service';
import { Subscription } from "rxjs/Subscription";
import { DealAddEdit } from "../../../../services/dealAddEdit.service";
import { AccountsCollection } from '../../../../services/accounts.service';
import { AccountsModel } from "../../../../models/account.models";
import { ActionsCollection, ActionTypes, ActionType } from "../../../../services/actionservice";
declare var $: JQueryStatic;

@Component({
    selector: 'deal-overview',
    templateUrl: './deal-overview.html',
    styleUrls: ["./deal-overview.css"],
    providers: [DragulaService],
})

export class DealOverviewComponent {
    @Input() type: string;
    @Input() trello: any;
    @Input() currency: string;

    @Output() deals = new EventEmitter();
    @Output() value = new EventEmitter();
    @Output() forecast = new EventEmitter();

    public pipeSelector: string;
    public salesId: string;
    public DealStatus: any;
    public Trelloboard: any;
    public prerenderd: any;
    public dataSet: boolean = false;
    public laoded: boolean = false;
    public dragEvent: boolean = false;
    public options: any = {};
    public oldx: any = 0;
    public direction: any = "";
    public stop_timeout: any = false;
    public stop_check_time: any = 150;
    public accountColors: any;
    public actionTypeIcons: any;

    public displayAccount: AccountsModel;
    
    private dropSub: Subscription;
    private overSub: Subscription;

    constructor(public drag: DragulaService, public _http: Api, public _notify: NotificationService, public _dealList: DealListing, public _trello: DealService, public _dealAddEdit: DealAddEdit, private accSvc: AccountsCollection, private actionSvc: ActionsCollection, public dealSvc: DealService) {
        drag.drop.subscribe((value: any) => {
            this.onDrop(value.slice(1));
            this.dragEvent = false;
        });
        this.overSub = drag.over.subscribe((value: any) => {
            this.dragEvent = true;
            $(".cardsWrapper").eq(0).scrollLeft($(value[1]).offset().left);
        });
    }

    ngOnInit() {
        this.accountColors = this.accSvc.accounts.reduce((res: any, cur: AccountsModel) => {
            res[cur.Id] = cur.AccountTypeCollor;
            return res;
        }, {});  
        this.actionTypeIcons = ActionTypes.reduce((res: any, current: ActionType) => {
            res[current.value] = current.icon;
            return res;
        }, {});
        console.log(this.actionTypeIcons);
    }

    showAccountInfo(accountId: string){
        this.accSvc.getAccount(accountId, null, null, (acc) => {
            this.dealSvc.rightPanelAccount = acc;
        });
    }

    onDrop(args: any) {
        var dealId = args[0].getAttribute('class').split(' ');
        var grpId = args[1].getAttribute('class').split(' ');
        this._http.put('Deals/' + dealId[1] + '/UpdateStatus/' + grpId[1], {})
            .map(res => res.json())
            .subscribe(res => {
                if (res.IsSuccess === true) {
                    this._notify.success('Deals status updated');
                    this._dealList.getAlldeals(null);
                    this._trello.getTrelloBoard();
                } else {
                    this._notify.error('Deals status updation failed');
                }
            });

        // Hide stuff dropped into the "Won Deal" area on SalePipe page.
        if (grpId[1] == this.trello['postSales'][0].process.Id && this.pipeSelector == "salePipe") {
            console.log("[Trello-Drop] Hiding deal");
            args[0].hidden = "true";
        }

    }

    openAction(actionId: string){
        this.actionSvc.showGroupByActionId(actionId);
    }

    ngOnChanges(change: SimpleChanges) {
        if (change['type']) {
            if (change['type']['currentValue']) {
                this.pipeSelector = change['type']['currentValue'];
            }
        }
        if (change['trello']) {
            console.log("[Trello] Change", change['trello']['currentValue']);
            if (change['trello']['currentValue']) {
                this.laoded = true;
                this.calculateValues();
            }
        }
    }

    private calculateValues() {

        let value = 0;
        let forecast = 0;
        let deals = 0;

        for (let group of this.trello[this.pipeSelector]) {
            value += Number(group.forcast);
            forecast += Number(group.forcast) * Number(group.process.Percentage) / 100;
            deals += group.deals.length;
        }

        this.value.next(value);
        this.forecast.next(forecast);
        this.deals.next(deals);
    }

    ngOnDestroy() {
        if (this.dropSub) this.dropSub.unsubscribe();
        if (this.overSub) this.overSub.unsubscribe();
    }
}