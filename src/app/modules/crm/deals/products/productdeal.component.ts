import { Component, Input } from '@angular/core';
import { ProductModel, DealsModel } from '../../../../models/deals.models';
import { ProductService, Product } from "../../../../services/products.service";
import { Subscription } from "rxjs/Subscription";
import { Configuration } from "../../../../services/configuration";
import { OrgService } from "../../../../services/organisation.service";
import { AccountsCollection } from "../../../../services/accounts.service";
import { DealAddEdit } from "../../../../services/dealAddEdit.service";

// @Pipe({ name: 'limitToTwo' })
// export class limitToTwoPipe implements PipeTransform {
// 	transform(value: number) {
// 		return Math.round(value * 100) / 100
// 	}
// }

@Component({

	selector: 'product-deal',
	templateUrl: './products.html',
	styleUrls: ["./products.css"]
})

export class ProductDetailsComponent {
	@Input() deal: DealsModel;
	public products: ProductModel[] = [];
	public savedProducts: ProductModel[] = [];
	private pickedProductsSub: Subscription;
	private quickAddProduct: ProductModel;
	private showQuickAdd: boolean;
	private unitTypes: string[] = [];
	private currencies: string[];
	private orgSub: any;
	public allowCurrencyChange: boolean = true;
	private editingProduct: number;
	private pickedProducts: Product[] = [];

	constructor(private productSvc: ProductService, private configuration: Configuration, private orgSvc: OrgService, private accSvc: AccountsCollection, private _deal: DealAddEdit) {

	}

	ngOnInit() {
		this.pickedProducts = [];
		this.orgSub = this.orgSvc.currentOrganisationObservable().subscribe(data => {
			if (data) {
				this.unitTypes = data.Units;
				this.quickAddProduct = this.newQuickAddModel();
				this.quickAddProduct.UnitsType = data.Units[0];
				this.currencies = this.configuration.currencies;
			}
		});
		this.pickedProductsSub = this.productSvc.getPickedProducts().subscribe(data => {
			if (!this.deal.ProductItems) {
				this.deal.ProductItems = [];
			}
			this.pickedProducts = this.pickedProducts.concat(data);
			console.log(data);
			this.deal.ProductItems = this.deal.ProductItems.concat(this.convertProducts(data));
			console.log(this.deal.ProductItems);
			this.allowCurrencyChange = false;
			this._deal.updateDealValue();
		});
	}

	wrongCurrencyCheck(product: ProductModel) {
		if (!this.allowCurrencyChange && product.InitialCurrency && product.InitialCurrency != this.deal.CurrencyName) {
			return true;
		}
		else {
			return false;
		}
	}

	changeVat(value: number) {
		this.deal.Vat = value;
	}

	newQuickAddModel(): ProductModel {
		let initialUnitType = this.unitTypes.length > 0 ? this.unitTypes[0] : "";
		return new ProductModel(0, "", 0, 0, 0, 0, initialUnitType, "");
	}

	openProductPicker() {
		this.productSvc.productPickerDeal = this.deal;
		this.productSvc.showProductPickerModal();
	}
	closeProductPicker() {
		this.productSvc.hideProductPickerModal();
	}

	addQuickProduct() {
		this.getSum(this.quickAddProduct);
		this.quickAddProduct.InitialCurrency = this.deal.CurrencyName;
		this.deal.ProductItems.push(this.quickAddProduct);
		this.quickAddProduct = this.newQuickAddModel();
		this.showQuickAdd = false;
		this._deal.updateDealValue();
	}

	convertProducts(products: Product[]): ProductModel[] {
		return products.reduce(function (res, current) {
			let newprod = new ProductModel(0, current.Name, current.Units, current.Discount, current.Price, current.Sum, current.Unit, current.Description, "Sum");
			newprod.ProductId = current.Id;
			newprod.PricesAsTotal = current.PricesAsTotal;
			res.push(newprod);
			return res;
		}, [])
	}

	removeProductFromDeal(prod: number) {
		this.deal.ProductItems.splice(prod, 1);
		if (this.deal.ProductItems.length == 0) {
			this.allowCurrencyChange = true;
		}
		this._deal.updateDealValue();
	}

	getSum(product: ProductModel) {
		var totalPriceSubPopUP = product.PricesAsTotal && product.ManualPrice == 0 ? Number(product.Price) : Number(product.Price) * Number(product.Units);
		if (product.ManualPrice != 0) {
			product.ManualPrice = product.Price * product.Units;
		}

		// if (totalPriceSubPopUP > 0) {
		// 	if (product.ProductDiscountType == 'Persent') {
		// 		var discount = (totalPriceSubPopUP * product.Discount / 100);
		// 		totalPriceSubPopUP = totalPriceSubPopUP - discount;
		// 	} else {
		// 		totalPriceSubPopUP = totalPriceSubPopUP - product.Discount;
		// 	}
		// }

		product.Sum = Number(totalPriceSubPopUP.toFixed(2));

	}

	updateProductUnits(prod: ProductModel) {
		console.log(prod);
		if (prod.ProductId) {
			let product = this.pickedProducts.find(x => x.Id == prod.ProductId);
			if (!product) {
				this.productSvc.GetProduct(prod.ProductId).subscribe(data => {
					product = data;
					if (product && product.PricesAsTotal && !product.PriceIsManual) {
						let pList = product.PriceLists.find((x: any) => x.Range >= prod.Units);
						if (pList == null) {
							prod.Units = product.PriceLists[product.PriceLists.length - 1].Range;
						}
						let price = product.PriceLists.reduce(function (res: any, current: any) {
							if (!res) {
								if (prod.Units <= current.Range) {
									res = current;
								}
								else {
									if (current.Range === -1) {
										res = current;
									}
								}
							}
							return res;
						}, 0);
						prod.Sum = price.Prices[this.accSvc.currentAccount.PriceListId];
						prod.Price = price.Prices[this.accSvc.currentAccount.PriceListId];
					}
					else {
						this.getSum(prod);
					}
					this._deal.updateDealValue();
				});
			}
			else {
				if (product && product.PricesAsTotal) {

					let pList = product.PriceLists.find((x: any) => x.Range >= prod.Units);
					if (pList == null) {
						prod.Units = product.PriceLists[product.PriceLists.length - 1].Range;
					}
					let price = product.PriceLists.reduce(function (res: any, current: any) {
						if (!res) {
							if (prod.Units <= current.Range) {
								res = current;
							}
							else {
								if (current.Range === -1) {
									res = current;
								}
							}
						}
						return res;
					}, 0);
					prod.Sum = price.Prices[this.accSvc.currentAccount.PriceListId];
					prod.Price = price.Prices[this.accSvc.currentAccount.PriceListId];
				}
				else {
					this.getSum(prod);
				}

				this._deal.updateDealValue();
			}

			this.editingProduct = null;
		}
	}
	ngOnDestroy() {
		if (this.pickedProductsSub) this.pickedProductsSub.unsubscribe();
		if (this.orgSub) this.orgSub.unsubscribe();
	}

}