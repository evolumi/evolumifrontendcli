import { Component } from '@angular/core';
import { DealListing } from '../../../../services/deallisting.service';
import { TranslateService } from '@ngx-translate/core';
import { OrgService } from '../../../../services/organisation.service';
import { FilterCollection } from '../../../../services/filterservice';
import { UsersService } from '../../../../services/users.service';
import { ChosenOptionModel, ChosenOptionGroupModel } from '../../../shared/components/chosen/chosen.models';
import { AccountsCollection } from '../../../../services/accounts.service';
import { Subscription } from 'rxjs/Subscription';
import { DealService } from "../../../../services/deal.service";

@Component({

    selector: 'deal-listing',
    templateUrl: './listing.html',
})

export class DeallistingComponent {

    public columns: any;

    private dealChangeSub: Subscription;
    public usersSubscription: Subscription;
    private dealSub: Subscription;

    public users: any;
    public saleProcess: any;
    public dealStatuses: any;
    public owner: string = '';
    public dealstatus: string = '';
    public account: Array<ChosenOptionModel>;
    public selectedaccount: string = '';
    public search: any;
    public searchString: string;
    public orgSubscription: any;
    public currentOrg: any = '';

    public deals: Array<any> = [];


    constructor(public listing: DealListing, public filter: FilterCollection, public orgService: OrgService, private translate: TranslateService, public accountCollection: AccountsCollection, private userService: UsersService, private dealSvc: DealService) {

        this.columns = [
            { 'name': 'Name', 'label': 'NAME', 'Link': true },
            { 'name': 'AccountName', 'label': 'ACCOUNT', 'AccountLink' : true },
            { 'name': 'OwnerName', 'label': 'DEALOWNERS', 'Owner' : true},
            { 'name': 'DealStatusName', 'label': 'STATUS' },
            { 'name': 'Value', 'label': 'VALUE', 'pipe': true },
            { 'name': 'Actions', 'label': '', 'Act': true }
        ];
    }

    ngOnInit() {

        //this.listing.getAllDetalStatus();
        this.usersSubscription = this.userService.activeUserListObervable().subscribe(res => {
            this.users = [new ChosenOptionModel("", this.translate.instant("SHOWALL"))];
            if (res.length > 0) {
                for (var a of res) {
                    this.users.push(a);
                }
                this.owner = <string>this.users[0].value;
            }
        });

        this.orgSubscription = this.orgService.currentOrganisationObservable().subscribe(res => {
            this.currentOrg = res;
            this.dealStatuses = [new ChosenOptionModel("", this.translate.instant("SHOWALL"), "0")];
            this.saleProcess = [new ChosenOptionGroupModel("0", this.translate.instant("DEFAULT"))]
            if (res) {
                for (var group of res.Settings.SaleProcesses) {
                    this.saleProcess.push(new ChosenOptionGroupModel(group.Id, group.Name));
                    for (var stat of group.DealStatuses) {
                        this.dealStatuses.push(new ChosenOptionModel(stat.Id, stat.Name, group.Id));
                    }
                }
                this.dealstatus = <string>this.dealStatuses[0].value;
            }
        });

        this.accountListSeach("");

        // Todo! Remove filterservice.
        this.dealChangeSub = this.filter.dealchange$.subscribe(res => {
            this.listing.getAlldeals();
        });

        this.dealSub = this.listing.deals$
            .subscribe(deals => {
                if (deals) {
                    this.deals = deals;
                }
            });

        this.updateSearch();
    }

    showAccountInfo(accountId: string) {
        this.accountCollection.getAccount(accountId, null, null, (acc) => {
            this.dealSvc.rightPanelAccount = acc;
        });
    }

    onShow() {
        if (!this.deals.length) {
            this.listing.setSearch({ Page: 1, PageSize: 20 });
        }
    }

    updateSearch() {
        this.search = { searchString: this.searchString, UserId: this.owner, SaleProcessId: '', DealStatusIds: this.dealstatus, AccountId: this.selectedaccount, PageSize: 20, Page: 1 }
        this.listing.setSearch(this.search);
    }

    public accountListSeach(search: string, scroll: boolean = false, page: number = 1) {
        if (!scroll) this.account = [];
        this.accountCollection.accountList(search, 1, (accounts) => {
            this.account = [...this.account].concat(accounts);
        })
    }

    ngOnDestroy() {
        if (this.usersSubscription) this.usersSubscription.unsubscribe();
        if (this.dealChangeSub) this.dealChangeSub.unsubscribe();
        if (this.dealSub) this.dealSub.unsubscribe();
        if (this.orgSubscription) this.orgSubscription.unsubscribe();
    }

    delete(dealId: string, i: number) {
        let conf = confirm(this.translate.instant("WARNDELDEAL"))
        if (conf) {
            this.listing.deals.splice(i, 1);
            this.listing.deleteDeal(dealId);
        }
    }
}