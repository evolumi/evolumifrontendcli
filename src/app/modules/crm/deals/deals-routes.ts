import { Routes } from '@angular/router';

// Components
import { DealsComponent } from './deals.component';
import { VoidComponent } from '../../shared/components/void';

export const DealsRoutes: Routes = [
    {
        path: 'Deals',
        component: DealsComponent,
        children: [
            { path: '', component: VoidComponent }
        ]
    }
];
