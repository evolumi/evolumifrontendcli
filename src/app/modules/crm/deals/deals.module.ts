import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { SharedModule } from '../../shared/shared.module';
import { DealsSharedModule } from './shared/deals-shared.module';

// Components
import { DealsComponent } from './deals.component';
import { DeallistingComponent } from './listing/listing.component';
import { DealOverviewComponent } from './deal-overview/deal-overview.component';
import { ActionsSharedModule } from '../actions/shared/action-shared.module';


@NgModule({
    imports: [
        FormsModule,
        HttpModule,
        CommonModule,
        SharedModule,
        ActionsSharedModule,
        DealsSharedModule
    ],
    declarations: [
        DealsComponent,
        DeallistingComponent,
        DealOverviewComponent,
    ],
    providers: [],
    exports: []
})
export class DealsModule { }