import { Component, Input } from '@angular/core';
import { LocalStorage } from '../../../../../services/localstorage';
import { OwnerPercentage, DealsModel } from '../../../../../models/deals.models';
import { ChosenOptionModel } from '../../../../shared/components/chosen/chosen.models';
import { UsersService } from '../../../../../services/users.service';
import { AccountsCollection } from "../../../../../services/accounts.service";
import { Subscription } from "rxjs/Subscription";
import { AccountRelation } from "../../../../../models/account.models";

@Component({
    selector: 'owner-contact',
    templateUrl: 'ownercontact.html',
    styleUrls: ['ownercontact.css'],
    viewProviders: [LocalStorage],
})

export class OwnerContactComponent {
    @Input() deal: DealsModel;
    @Input() accountId: string;

    public contacts: AccountRelation[] = [];
    private userSub: any;
    public viewOwnerSettings: boolean = false;
    private ownerSettingsWarning: boolean = false;
    public viewContactSettings: boolean = false;
    public userList: OwnerPercentage[] = [];

    private notAddedOwners: OwnerPercentage[] = [];
    private notAddedContacts: AccountRelation[] = [];
    private contacObservable: Subscription;

    constructor(
        private _localStore: LocalStorage,
        private userService: UsersService,
        public accSvc: AccountsCollection
    ) { }

    log() {
        console.log(this.deal.Contacts)
    }

    ngOnInit() {
        let loggedinUser = this._localStore.get('userId');
        this.userSub = this.userService.activeUserListObervable().subscribe(data => {
            this.userList = data.reduce((res: any, current: ChosenOptionModel) => {
                let obj = new OwnerPercentage(current.value, 0, current.label as string);
                res.push(obj);
                return res;
            }, []);
            if (this.deal.Owners.length == 0) {
                let user = this.userList.find(x => x.UserId == loggedinUser);
                user.Percent = 100;
                this.deal.Owners.push(user);
            }
            this.updateNotAddedOwners();
        });

        this.contacObservable = this.accSvc.contactAccountsObservable().subscribe((data) => {
            if (data) {
                this.contacts = data;
                this.updateNotAddedContacts();
            }
        });


    }

    logStuff(input: any) {
        let c = 3;
        if (c == 3) {
            console.log(input);
        }
    }

    getPersonName(id: string): string {
        let contact = this.contacts.find(x => x.PersonId == id);
        if (contact) {
            return contact.FirstName + " " + contact.LastName;
        }
        else return "";
    }

    updateNotAddedOwners() {
        this.notAddedOwners = this.userList.filter((owner) => {
            return this.deal.Owners.find(x => x.UserId == owner.UserId) == null;
        });
    }

    updateNotAddedContacts() {
        this.notAddedContacts = this.contacts.filter((contact) => {
            return (this.deal.ContactAccounts.indexOf(contact.PersonId) == -1);
        });
    }

    getContacts(accountId: string) {
        if (accountId) {
            this.accSvc.getContactsForAccount(this.deal.AccountId, false);
        }
    }

    warningClass() {
        if (this.ownerSettingsWarning) {
            return "badValue";
        }
        return "";
    }

    ngOnChanges(change: any) {
        if (change["accountId"]) {
            if (this.deal.Editmode) {
                this.deal.ContactAccounts = [];
                this.deal.Contacts = [];
            }
            if (change['accountId']['currentValue']) {
                this.accSvc.getAccount(change["accountId"]["currentValue"]);
                this.getContacts(change["accountId"]["currentValue"]);
                this.updateNotAddedContacts();
            }
        }
    }

    toggleOwnerSettings() {
        this.viewOwnerSettings = !this.viewOwnerSettings;
    }

    toggleContactSettings() {
        this.viewContactSettings = !this.viewContactSettings;
    }

    CalcMax($event: number, owner: OwnerPercentage) {
        owner.Percent = Number($event);
        let sum = this.getTotalPercent();
        this.ownerSettingsWarning = sum > 100;
    }

    getTotalPercent() {
        return this.deal.Owners.reduce(function (res, current) {
            res = res + Number(current.Percent);
            return res;
        }, 0);
    }

    addOwner(user: OwnerPercentage) {
        this.deal.Owners.push(user);
        this.updateNotAddedOwners();
    }

    removeOwner(index: number) {
        this.deal.Owners.splice(index, 1);
        this.updateNotAddedOwners();
    }

    removeContact(index: number) {
        if (this.accSvc.account.IsPerson) {
            this.deal.Contacts.splice(index, 1);
        }
        else {
            this.deal.ContactAccounts.splice(index, 1);
        }
        this.updateNotAddedContacts();
    }

    addContact(id: string) {
        if (this.accSvc.account.IsPerson) {
            // this.deal.Contacts.push(contact.Id);
        }
        else {
            if (!this.deal.ContactAccounts) this.deal.ContactAccounts = [];
            this.deal.ContactAccounts.push(id);
        }
        this.updateNotAddedContacts();
    }

    ngOnDestroy() {
        if (this.userSub) this.userSub.unsubscribe();
        if (this.contacObservable) this.contacObservable.unsubscribe();
    }

}