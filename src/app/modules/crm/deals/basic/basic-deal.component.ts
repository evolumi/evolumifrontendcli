import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { LocalStorage } from '../../../../services/localstorage';
import { TranslateService } from '@ngx-translate/core';
import { DealsModel } from '../../../../models/deals.models';
import { ChosenOption } from "../../../shared/components/chosen/chosen";
import { ChosenOptionModel, ChosenOptionGroupModel } from '../../../shared/components/chosen/chosen.models';
import { OwnerContactComponent } from "./owner-contact/ownercontact.component";
import { OrgService } from "../../../../services/organisation.service";
import { Subscription } from "rxjs/Subscription";
import { Router } from "@angular/router";
import { DealAddEdit } from "../../../../services/dealAddEdit.service";
import { OrganisationModel } from "../../../../models/organisation.models";
import { ActionsCollection } from "../../../../services/actionservice";
import { ActionsModel } from "../../../../models/actions.models";
import { DotmenuOption } from "../../../shared/components/dotmenu/DotmenuOption";
import { DealService } from "../../../../services/deal.service";
import { DealListing } from "../../../../services/deallisting.service";
import { ConfirmationService } from "../../../../services/confirmation.service";

declare var $: JQueryStatic;


@Component({
    selector: 'basic-deal',
    templateUrl: './basic.html',
    viewProviders: [LocalStorage],
    styleUrls: ["./basic.css"]
})

export class BasicDealComponent {
    public groups: ChosenOptionGroupModel[] = [];
    public statusOptions: Array<any> = [];
    public dateNames: Array<ChosenOption> = [];
    private org: OrganisationModel;
    private orgSub: Subscription;
    private accounts: ChosenOptionModel[];
    public closedActions: ActionsModel[] = [];
    public openActions: ActionsModel[] = [];
    @Input() salesProcess: any;
    @Input() currencies: any;
    @Input() vat: any;
    @Input() ocr: string;
    @Input() deal: DealsModel;
    @Input() internalId: string;
    @Input() externalId: string;
    @Input() currentStatusDropDown: any;
    @Output() formValidity: EventEmitter<boolean> = new EventEmitter();
    @ViewChild("ownercontact") ownerContact: OwnerContactComponent;

    public companyAccountList: ChosenOptionModel[] = [];
    public personAccountList: ChosenOptionModel[] = [];

    public dotlineOptions: string[] = [];
    public dotlineValue: number;
    public dotlineHeader: string;
    public dealStatuses: any[] = [];

    public dotmenuOptions: DotmenuOption[] = [];
    public changeDealStatusOptions: any[];

    private nocompany = this.translate.instant("NOCOMPANY");

    constructor(private translate: TranslateService, private orgSvc: OrgService, private router: Router, private _deal: DealAddEdit, private actionSvc: ActionsCollection, private dealSvc: DealService, private dealListSvc: DealListing, private confirmSvc: ConfirmationService) {
        this.groups.push(new ChosenOptionGroupModel(1, "SALESPIPE"));
        this.groups.push(new ChosenOptionGroupModel(2, "POSTSALES"));
        this.groups.push(new ChosenOptionGroupModel(3, "LOSTDEAL"));
        this.groups.push(new ChosenOptionGroupModel(4, "CLOSED"));

        this.dateNames = [
            new ChosenOptionModel("DateStart", this.translate.instant("STARTDATE")),
            new ChosenOptionModel("DateEnd", this.translate.instant("ENDDATE")),
            new ChosenOptionModel("DateInvoice", this.translate.instant("INVOICEDATESTART")),
            new ChosenOptionModel("DateInvoiceExpire", this.translate.instant("INVOICEDATEEND"))
        ];
    }
    ngOnInit() {
        this.orgSub = this.orgSvc.currentOrganisationObservable().subscribe(data => {
            this.org = data;
            this.setDotlineValues();
        });
        console.log(this.deal);
        this.changeStatus();
        this.checkFormValidity();
        if (this.deal.Id) {
            this.actionSvc.getActionsForDeal(this.deal.Id, (success, data) => {
                if (success) {
                    this.openActions = data.filter(x => x.Status == "Open");
                    this.closedActions = data.filter(x => x.Status != "Open");
                }
                else {
                    console.log("Failed to get actions for deal");
                }
            });
        }
    }

    setNewDealStatus(dealStatus: any, confirm: boolean = true) {
        this.changeDealStatusOptions = null;
        if (confirm) {
            let confirmTitle = this.translate.instant("CONFIRMDEALSTATUSCHANGETITLE");
            let confirmMessage = this.translate.instant("CONFIRMDEALSTATUSCHANGEMESSAGE", { StatusName: dealStatus.Name });
            this.confirmSvc.showModal(confirmTitle, confirmMessage, () => {
                this.dealSvc.updateDealStatus(this.deal.Id, dealStatus.Id, (success: boolean) => {
                    if (success) {
                        if (this.changeDealStatusOptions) {
                            this.changeDealStatusOptions = null;
                        }
                        this.deal.DealStatusId = dealStatus.Id;
                        this.setDotlineValues();
                        this.dealSvc.getTrelloBoard();
                        this.dealListSvc.getAlldeals();
                    }
                    else {
                        console.log("Status update failed");
                    }
                });
            });
        }
        else {
            this.deal.DealStatusId = dealStatus.Id;
            this.setDotlineValues();
        }
    }

    lineDealStatusChange(e: any) {
        this.setNewDealStatus(this.dealStatuses.find(x => x.Name == this.dotlineOptions[e]), this.deal.Id != null);
    }

    setDotlineValues() {

        let saleProcess = this.org.Settings.SaleProcesses.find((x: any) => x.Id == this.deal.SaleProcessId);
        let dealStatus = saleProcess.DealStatuses.find((x: any) => x.Id == this.deal.DealStatusId);
        this.dotlineHeader = saleProcess.Name + " (" + this.translate.instant(this.groups.find(x => x.value == dealStatus.Type).label as string) + ")";
        this.dealStatuses = saleProcess.DealStatuses;
        let openStatuses = this.dealStatuses.filter((x: any) => x.Type == 1);
        let wonStatus = this.dealStatuses.find((x: any) => x.Type == 2);
        let lostStatuses = this.dealStatuses.filter((x: any) => x.Type == 3);
        let archiveStatuses = this.dealStatuses.filter((x: any) => x.Type == 4);
        this.dotmenuOptions = [
            new DotmenuOption({
                Label: this.translate.instant("SALESPIPE"),
                Function: () => {
                    if (openStatuses.length == 1) {
                        this.setNewDealStatus(openStatuses[0], this.deal.Id != null);
                    }
                    else {
                        this.changeDealStatusOptions = openStatuses;
                    }
                },
                Icon: "times"
            }),
            new DotmenuOption({
                Label: this.translate.instant("WONDEAL"),
                Function: () => {
                    this.setNewDealStatus(wonStatus, this.deal.Id != null);
                },
                Icon: "dollar"
            }),
            new DotmenuOption({
                Label: this.translate.instant("LOST"),
                Function: () => {
                    if (lostStatuses.length == 1) {
                        this.setNewDealStatus(lostStatuses[0], this.deal.Id != null);
                    }
                    else {
                        this.changeDealStatusOptions = lostStatuses;
                    }
                },
                Icon: "times"
            }),
            new DotmenuOption({
                Label: this.translate.instant("ARCHIVE"),
                Function: () => {
                    if (archiveStatuses.length == 1) {
                        this.setNewDealStatus(archiveStatuses[0], this.deal.Id != null);
                    }
                    else {
                        this.changeDealStatusOptions = archiveStatuses;
                    }
                },
                Icon: "folder-o"
            })
        ]
        this.dotmenuOptions.splice(dealStatus.Type - 1, 1);

        this.dotlineOptions = this.dealStatuses.filter((x: any) => x.Type == dealStatus.Type).map((x: any) => x.Name)
        this.dotlineValue = this.dotlineOptions.findIndex((x: any) => x == dealStatus.Name);
    }

    goToAccount() {
        this.router.navigate(['CRM/' + (this.deal.AccountIsPerson ? 'Person' : "") + 'Accounts', this.deal.AccountId]);
        this._deal.deal = null;
    }

    selectPerson(id: string) {
        this.deal.AccountIsPerson = true;
        this.setAccount(id);
    }

    selectCompany(id: string) {
        this.deal.AccountIsPerson = false;
        this.setAccount(id);
    }

    private setAccount(id: string) {
        this.deal.AccountId = id;
        let acc = this.companyAccountList.find(x => x.value == id) || this.personAccountList.find(x => x.value == id);
        this.deal.AccountName = acc ? acc.label.toString() : "";
        this.checkFormValidity();
    }


    private DealValues: any = {
        AsDeal: 0,
        AsProducts: 0
    }
    toggleDealAsProducts() {
        if (this.deal.DealAsProducts) {
            this.DealValues.AsProducts = this.deal.Value;
            this.deal.Value = this.DealValues.AsDeal;
        }
        else {
            this.DealValues.AsDeal = this.deal.Value;
            this.deal.Value = this.DealValues.AsProducts;
        }
        this.deal.DealAsProducts = !this.deal.DealAsProducts;
    }

    getSaleProcessName(id: string) {
        return this.salesProcess.find((x: ChosenOptionModel) => x.value == id).label;
    }

    getDealStatusName(id: string) {

        return this.currentStatusDropDown.salesPipe.find((x: ChosenOptionModel) => x.value == id).label;
    }

    setHeight() {
        let elem = document.getElementById("DealNoteText");
        elem.style.height = elem.scrollHeight + "px";
    }

    updateCloseDate(event: Date) {
        this.deal.ExpectedCloseDate = event ? event.getTime() : null;
    }


    changeStatus() {
        var statusFromProcess: any = {};
        for (var salesstats of this.salesProcess) {
            var salesPipe: any = [];
            for (var status of salesstats.group) {
                salesPipe.push(new ChosenOptionModel(status.Id, status.Name, status.Type));
            }
            statusFromProcess[salesstats.value] = { salesPipe: salesPipe };
        }

        this.currentStatusDropDown = statusFromProcess[this.deal.SaleProcessId];

        // Set first option if Status is not yet set or Statis does not exist in new optionList.
        if (!this.deal.DealStatusId || this.currentStatusDropDown.salesPipe.findIndex((x: any) => x.value == this.deal.DealStatusId) == -1) {
            var status = this.currentStatusDropDown.salesPipe.find((x: any) => x.group == 1);
            if (status) {
                this.deal.DealStatusId = status.value;
            }
            else {
                this.deal.DealStatusId = this.currentStatusDropDown.salesPipe[0].value;
            }
        }
        console.log("[Deal] Setting status", this.deal.DealStatusId);
    }
    checkFormValidity() {
        if (this.deal.AccountId && this.deal.Name) {
            this.formValidity.emit(true);
        }
        else {
            this.formValidity.emit(false);
        }

    }

    UpdateCurrencyName() {
        this.deal.CurrencyName = this.currencies[this.deal.Currency]['id'];
    }

    changeVat(value: number) {
        this.deal.Vat = value;
    }

    clickToClose($event: any) {
        let parents = $($event.target).parents();
        if (parents.hasClass("noClose")) {
            return;
        }

        function hasParent(parentId: string) {
            return $($event.target).parents("#" + parentId).length > 0;
        }

        if (this.ownerContact.viewOwnerSettings && $event.target.id != "ownerSettings" && !hasParent("ownerSettings")) {
            this.ownerContact.viewOwnerSettings = false;
        }
        if (this.ownerContact.viewContactSettings && $event.target.id != "contactSettings" && !hasParent("contactSettings")) {
            this.ownerContact.viewContactSettings = false;
        }
    }

    ngOnChanges(changes: any) {
        if (changes && changes["deal"] && changes["deal"].currentValue && changes["deal"].currentValue.editmode) {
            setTimeout(() => this.setHeight(), 0); //In the making.. needs to expand the notes textarea on edit
        }
        if (changes && changes.deal && changes.deal.currentValue && this.org) {
            this.setDotlineValues();
        }
    }
    ngOnDestroy() {
        if (this.orgSub) {
            this.orgSub.unsubscribe();
        }
    }

}
