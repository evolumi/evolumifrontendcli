import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { LocalStorage } from '../../../../services/localstorage';
import { TranslateService } from '@ngx-translate/core';
import { FilterCollection } from '../../../../services/filterservice';
import { DealAddEdit } from '../../../../services/dealAddEdit.service';
import { DealService } from '../../../../services/deal.service';
import { ChosenOptionModel } from '../../../shared/components/chosen/chosen.models';
import { OrgService } from '../../../../services/organisation.service';
import { Configuration } from '../../../../services/configuration';

@Component({
    selector: "addeditdeal",
    templateUrl: './addedit.html',
    viewProviders: [LocalStorage],
})

export class AddDealsComponent {
    public edit: boolean = false;
    //subscribers
    public orgSubscription: any;

    //dropdowns
    public accounts: any;
    public salesProcess: any;
    public statusFromProcess: any;
    public currentStatusDropDown: any;
    public currencies: any;
    public vat: any;
    public orgSettings: any;
    public ocr: string;
    public internalId: string;
    public externalId: string;

    //preseletions
    public selectedAccount: string;
    public dealprocessid: string;
    public selectedStatus: string;
    public selectedVat: number;

    //formvalid
    public formValid: boolean = true;

    //for loading
    public apiprogress: boolean = false;
    public loaderCount = 0;

    constructor(
        public filter: FilterCollection,
        public _deal: DealAddEdit,
        public _trello: DealService,
        public _router: Router,
        private translate: TranslateService,
        private orgService: OrgService,
        private config: Configuration
    ) { }


    ngOnInit() {

        this.orgSubscription = this.orgService.currentOrganisationObservable().subscribe(org => {
            if (org) {
                this.orgSettings = org;
                this.vat = [];
                for (var a of org.Vats) {
                    this.vat.push(new ChosenOptionModel(a, a));
                }

                this.salesProcess = [];
                for (let a of org.Settings.SaleProcesses) {
                    if (a.DealStatuses.length > 0)
                        this.salesProcess.push(new ChosenOptionModel(a.Id, a.Name, a.DealStatuses));
                }

                if (!this._deal.deal.Id)
                    if (!this._deal.deal.Id) {
                        this._deal.deal.Currency = org.Currency;
                        this._deal.deal.CurrencyName = org.CurrencyName;
                        this._deal.deal.Vat = org.Vats[0];
                        this._deal.deal.SaleProcessId = org.Settings.SaleProcesses[0]['Id'];
                    }

                this.getSalesProcesStatus(org.Settings.SaleProcesses);
            }
        });

        this.currencies = [];
        for (var a of this.config.currencies) {
            this.currencies.push(new ChosenOptionModel(a, a));
        }
    }

    getSalesProcesStatus(res: any) {
        this.statusFromProcess = {};

        for (var salesstats of res) {
            var salesPipe: any = [];
            salesstats.DealStatuses.sort((n1: any, n2: any) => n1.SortOrder - n2.SortOrder);
            for (var status of salesstats.DealStatuses) {
                salesPipe.push(new ChosenOptionModel(status.Id, status.Name, status.Type));
            }
            this.statusFromProcess[salesstats.Id] = { salesPipe: salesPipe };
        }
        this.currentStatusDropDown = this.statusFromProcess[res[0].Id];
        if (!this._deal.deal.Id)
            this.selectDefaultDealStatus(res[0]);
    }

    ngOnDestroy() {
        if (this.orgSubscription) this.orgSubscription.unsubscribe();
    }

    selectDefaultDealStatus(saleProcess: any) {

        console.log("[Deal] Statuses", saleProcess);
        var status = saleProcess.DealStatuses.find((x: any) => x.Type == 1);
        console.log("[Deal] Found default status", status);

        if (status) {
            this._deal.deal.DealStatusId = status.Id;
        }

        //this.deal.DealStatusId = this.currentStatusDropDown.salesPipe[0].value;
    }

    AddOrUpdate() {
        if (!this._deal.deal.DealAsProducts) {
            this._deal.deal.ProductItems = [];
        }

        if (this.apiprogress == true) return;
        this.apiprogress = true;
        if (this._deal.deal.Name == "") {
            this._deal.deal.Name = this.translate.instant("DEAL");
        }
        this._deal.AddOrUpdateDeal(() => {
            this.filter.changeDeal();
            this._trello.getTrelloBoard();
            this.apiprogress = false;
        })
    }

    clickToClose(event: any) {
        if (event.srcElement.id == "addeditdealmodal") {
            this._deal.clear();
        }
    }

    formValidSetter(val: any) {
        console.log("[DealModal] Valid:", val);
        this.formValid = val;
    }
}
