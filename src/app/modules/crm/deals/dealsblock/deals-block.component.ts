import { Component, Input } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { DealsBlockService } from '../../../../services/deals-block.service';
import { FilterCollection } from '../../../../services/filterservice';
import { DealAddEdit } from "../../../../services/dealAddEdit.service";
import { DealsModel, Address } from "../../../../models/deals.models";

@Component({
    selector: 'deals-by-account',
    template: ` 
                    <div class="title">
                        <h4>{{ 'DEALS' | translate }}</h4>
                        <a *ngIf="enableAdd" (click)="createDeal()" class="btn pull-right">+</a>
                    </div>
                    <div class="content">
                        <ul class="iconList" *ngIf="_dealCollection.deals.length!=0">
                            <li *ngFor="let deal of _dealCollection.deals;let i=index">
                                <i class="fa fa-money left icon"></i>
                                <a (click)="_dealAddEdit.showDeal(deal)">                 
                                    {{deal.Name}}
                                </a>
                                <p>{{deal.DealStatusName}}</p>
                                <a class="icon dlt right" *ngIf="enableDelete" (click)="deleteDeal(deal.Id)"><i class="fa fa-trash"></i></a>
                            </li>
                        </ul>
                        <span *ngIf="_dealCollection.deals.length==0">{{'NODEALS' | translate }}</span>
                   </div>
                        
                  `
})

export class DealsByAccountComponent {
    @Input() enableDelete: boolean;
    @Input() enableAdd = true;
    @Input() search: string;
    public dealAccountId: string;
    private changeSub: any;

    constructor(public _dealCollection: DealsBlockService, public _filter: FilterCollection, private translate: TranslateService, private _dealAddEdit: DealAddEdit) { }

    ngOnInit() {
        this.changeSub = this._filter.dealchange$
            .subscribe(change => {
                console.log("[Deals-Block] Deal Update detected. Loading deals for Account[" + this.dealAccountId + "]");
                this.getDealById();
            });
    }

    ngOnChanges(changes: any) {
        if (changes && changes.search) {
            this.dealAccountId = changes.search.currentValue.accountId;
            console.log("[Deals-Block] Change on AccountId[" + this.dealAccountId + "]");
            this.getDealById();
        }
    }

    createDeal() {
        let newdeal = new DealsModel();
        newdeal.ShippingAddress = new Address();
        newdeal.BillingAddress = new Address();
        newdeal.AccountId = this.dealAccountId;
        this._dealAddEdit.showDeal(newdeal);
    }

    getDealById() {
        this._dealCollection.getDealsByAccount(this.dealAccountId)
    }

    deleteDeal(id: string) {
        var conf = confirm(this.translate.instant("WARNDELDEAL"));
        if (conf) {
            this._dealCollection.deleteDeal(id);
        }
    }
    ngOnDestroy() {
        if (this.changeSub) this.changeSub.unsubscribe();
    }
}
