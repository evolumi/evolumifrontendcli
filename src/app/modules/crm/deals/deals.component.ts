import { Component } from '@angular/core';
import { LocalStorage } from '../../../services/localstorage';
import { DealService } from '../../../services/deal.service';
import { SalesProcessModel } from '../../../models/deals.models';
import { DealListing } from '../../../services/deallisting.service';
import { UsersService } from '../../../services/users.service';
import { OrgService } from '../../../services/organisation.service';
import { DealAddEdit } from "../../../services/dealAddEdit.service";
import { DealsModel, Address } from "../../../models/deals.models";
import { Subscription } from 'rxjs/Subscription';
import { ButtonPanelService } from '../../../services/button-panel.service';

@Component({
    templateUrl: './deals.html',
    viewProviders: [LocalStorage],
})

export class DealsComponent {
    public saleProcessArray: Array<SalesProcessModel> = new Array<SalesProcessModel>();
    public users: Array<any> = new Array<any>();
    public userFilter: string = "";
    public salesProcess: string;
    public displayExport: boolean = false;
    public pipe = "salePipe";
    public currency: string = "USD";

    public value: any = { postSales: 0, salePipe: 0 };
    public forecast: any = { postSales: 0, salePipe: 0 };
    public dealCount: any = { postSales: 0, salePipe: 0 };

    public showingDeals: Array<any> = [];

    public rightPanelCloseFunc = () => { this._deal.rightPanelAccount = null };

    //subscribers
    private userSub: Subscription;
    private orgSub: Subscription;
    private buttonSub: Subscription;

    constructor(private button: ButtonPanelService, public _deal: DealService, public _listing: DealListing, private userService: UsersService, private orgService: OrgService, private _dealAddEdit: DealAddEdit) {

        this.button.addPanelButton("ADDDEAL", "add-deal", "plus");
        if (this.orgService.hasPermission("Export"))
            this.button.addPanelButton("EXPORT", "exportdeals", "download");


        this.buttonSub = this.button.commandObservable().subscribe(command => {
            if (command == "exportdeals") { this.runExport() }
            if (command == "add-deal") { this.createDeal() }
        });
    }

    ngOnInit() {

        this.userSub = this.userService.userListObervable().subscribe(users => {
            if (users) {
                for (var a of users) {
                    this.users.push(a);
                }
                console.log(users);
            }
        });

        this.orgSub = this.orgService.currentOrganisationObservable().subscribe(org => {
            if (org) {
                this.saleProcessArray = [];
                for (var status of org.Settings.SaleProcesses) {
                    this.saleProcessArray.push(new SalesProcessModel(status.Id, status.Name));
                }

                var sp = localStorage.getItem("saleProcess");
                if (sp && this.saleProcessArray.findIndex(x => x.Id == sp)) {
                    this.salesProcess = sp;
                }
                else {
                    this.salesProcess = this.saleProcessArray[0].Id;
                }
                this._deal.setSalesProcessId(this.salesProcess);
                this.currency = org.CurrencyName;

                this._deal.getTrelloBoard();
            }
        });
    }

    createDeal() {
        let newdeal = new DealsModel();
        newdeal.ShippingAddress = new Address();
        newdeal.BillingAddress = new Address();
        this._dealAddEdit.showDeal(newdeal);
    }

    changeSale() {
        this._deal.setSalesProcessId(this.salesProcess);
        this._deal.getTrelloBoard();
        localStorage.setItem("saleProcess", this.salesProcess);
    }

    changeUser(userId: string) {
        console.log("[Deals] Setting user filter [" + userId + "]");
        this._deal.setUserId(userId);
        this._deal.getTrelloBoard();
    }

    ngOnDestroy() {
        if (this.orgSub) this.orgSub.unsubscribe();
        if (this.userSub) this.userSub.unsubscribe();
        if (this.buttonSub) this.buttonSub.unsubscribe();
        this.button.removeButtons(["exportdeals", "add-deal"]);
    }

    runExport(): void {
        this._listing.exportDeals();
    }
}