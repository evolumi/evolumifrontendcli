import { Component, Input } from '@angular/core';
import { DealsModel } from '../../../../models/deals.models';
import { Address } from '../../../../models/deals.models';

@Component({
    selector: 'delivery-deal',
    templateUrl: './delivery.html'
})


export class DeliveryDetailsComponent {
    @Input() deal: DealsModel;

    public showPaymentInfo: boolean = false;
    public showDeliveryInfo: boolean = false;

    public now: Date = new Date();
    public startOfDay = new Date(this.now.getFullYear(), this.now.getMonth(), this.now.getDate()).getTime();

    public startDate: number = this.startOfDay;
    public endDate: number = this.startOfDay;
    public invoiceDate: number = this.startOfDay;
    public invoiceExpireDate: number = this.startOfDay;

    constructor() { }

    public copyShippingAddress() {
        this.deal.BillingAddress = new Address(this.deal.ShippingAddress);
    }

    objectHasContent(obj: any) {
        let res = false;
        for (var prop in obj) {
            if (obj.hasOwnProperty(prop)) {
                if (prop != "Id" && prop != "Type" && obj[prop] != null) {
                    res = true;
                }
            }
        }
        return res;
    }
}