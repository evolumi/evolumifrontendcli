import { NgModule } from '@angular/core';

import { SharedModule } from '../../../shared/shared.module';

import { AddDealsComponent } from '../add/add.component';
import { BasicDealComponent } from '../basic/basic-deal.component';
import { OwnerContactComponent } from '../basic/owner-contact/ownercontact.component';
import { DeliveryDetailsComponent } from '../delivery/delivery.component';
import { ProductDetailsComponent } from '../products/productdeal.component';
import { ActionsSharedModule } from '../../actions/shared/action-shared.module';

@NgModule({
    imports: [
        SharedModule,
        ActionsSharedModule
    ],
    declarations: [
        AddDealsComponent,
        BasicDealComponent,
        OwnerContactComponent,
        DeliveryDetailsComponent,
        ProductDetailsComponent
    ],
    exports: [AddDealsComponent]
})
export class DealsSharedModule { }