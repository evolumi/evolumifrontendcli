import { Injectable } from '@angular/core';
import { Api } from '../../../../services/api';
import { TranslateService } from '@ngx-translate/core';
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { Observable } from 'rxjs/Observable';
import { NotificationService } from '../../../../services/notification-service';
import { OrgService } from '../../../../services/organisation.service';

@Injectable()
export class ReportService {

    private $reports: BehaviorSubject<Array<any>> = new BehaviorSubject([]);
    public reportsObserver() {
        return this.$reports.asObservable();
    }

    private $report: BehaviorSubject<any> = new BehaviorSubject({});
    public reportObserver() {
        return this.$report.asObservable();
    }

    public insightTypes: Array<any> = [];
    public groupedInsightTypes: any;

    private orgSub: any;
    private orgId: string;

    private reportsLoaded: boolean = false;
    private reportIdToLoad: string;
    private loadedOrgid: string;

    constructor(
        private api: Api,
        private orgService: OrgService,
        private notify: NotificationService,
        private translate: TranslateService) {

        this.orgSub = this.orgService.currentOrganisationObservable().subscribe(org => {
            if (org) {
                this.orgId = org.Id;
            }
        });
    }

    ngOnInit() {

    }

    ngOnDestroy(){
        if (this.orgSub) this.orgSub.unsubscribe();
    }

    public getInsightTypes(complete: () => any = null) {
        if (this.loadedOrgid != this.orgId) {
            this.api.requestGet(`Organisations/${this.orgId}/Reports/InsightTypes`, res => {
                this.loadedOrgid = this.orgId;
                this.insightTypes = res;
                this.groupedInsightTypes = this.groupInsightTypes();
                if (complete)
                    complete();
                console.log("[Report] Insight-types loaded");
            },
                err => {
                    console.log("[Report] Error loading insight-types");
                });
        } else {
            if (complete) {
                complete();
            }
        }
    }

    private groupInsightTypes() {
        let arrays = this.insightTypes.reduce((res: any, curr: any) => {
            if (!res[curr.DocumentType]) {
                res[curr.DocumentType] = [];
            }
            res[curr.DocumentType].push(curr);
            return res;
        }, []);

        return arrays;
    }

    // Sets a new Report as active
    public setReport(report: any) {
        console.log("[Report] Setting report. ", report);
        this.$report.next(report);
    }

    // Load a report from reportlist and sets as active.
    public loadReport(id: string) {
        console.log("[Reports] Trying to load report [" + id + "]");
        let report = this.$reports.value.find(x => x.Id == id);

        if (!report && !this.reportsLoaded) {
            console.log("[Report] Load report Failed! Report list not populated yet. Saving Id to retry later.");
            this.reportIdToLoad = id;
        }
        else {
            this.setReport(report);
        }
    }

    // Adds or updates a report.
    public addOrUpdate(report: any): Observable<any> {
        if (report.Id) {
            return this.api.put("Reports/" + report.Id, report).map(res => {
                return this.handleUpdateResponse(res);
            });
        }
        else {
            return this.api.post("Organisations/" + this.orgId + "/Reports", report).map(res => {
                return this.handleUpdateResponse(res);
            });
        }
    }

    // Handles response from Add/Update. Displays message and Maps reposne object.
    private handleUpdateResponse(res: any) {
        var response = res.json();
        if (response.IsSuccess) {
            this.notify.success(this.translate.instant("SUCCESS"));
            this.getReports();
        }
        else {
            this.notify.success(this.translate.instant("ERROR"));
            console.log("[Report] Add/Update Failed!", response.Message);
        }

        return response;
    }

    // Loads Reports from API.
    public getReports() {
        if (this.loadedOrgid != this.orgId) {
            this.api.requestGet("Organisations/" + this.orgId + "/Reports", reports => {
                this.$reports.next(reports);
                console.log("[Reports] Reports loaded", reports);
                if (this.reportIdToLoad && reports.length) {
                    console.log("[Reports] Found saved Id[" + this.reportIdToLoad + "]");
                    this.loadReport(this.reportIdToLoad);
                    this.reportIdToLoad = "";
                }
                this.reportsLoaded = true;
            }, err => {
                this.notify.error(this.translate.instant("ERROR"));
            });
        }
    }

    // Deletes Report.
    public delete(id: string): Observable<boolean> {
        return this.api.delete("Reports/" + id).map(res => {
            let response = res.json();
            if (response.IsSuccess) {
                this.notify.success("DELETED");
            }
            else {
                this.notify.error("DELETEFAILED");
            }
            this.getReports();
            return (response.IsSuccess);
        });
    }
}