import { ActivatedRoute, Router } from '@angular/router';
import { Component } from '@angular/core';
import { HighchartService, InsightFilter } from '../../../../services/highchart.service';
import { ReportService } from './report.service';

@Component({
    templateUrl: './reports.html',
    styleUrls: ['../insights.css'],
})

export class ReportComponent {

    public report: any = {};

    public layouts: Array<any> = [];

    private reportSub: any;
    private routeSub: any;

    public edit: boolean;

    public activeChart: any;
    public editFilter: boolean = false;

    constructor(public chartService: HighchartService, private router: Router, private reportService: ReportService, private route: ActivatedRoute) {
        this.reportSub = this.reportService.reportObserver().subscribe(report => {
            if (report) {
                this.report = report;
            }
        });

        this.routeSub = route.params.subscribe(params => {
            if (params["id"]) {
                this.reportService.loadReport(params["id"]);
            }
            else {
                this.newReport();
            }
        });

        this.layouts.push(
            { name: "Single", charts: 1 },
            { name: "Split", charts: 2 },
            { name: "SplitLeft", charts: 3 },
            { name: "SplitRight", charts: 3 },
            { name: "Quarters", charts: 4 });
    }

    ngOnInit() {
        this.edit = false;
    }

    public newReport() {
        console.log("[Report] Creating new Report");
        let rep = {};
        this.reportService.setReport(rep);
    }

    public update() {
        this.reportService.addOrUpdate(this.report).subscribe(res => {
            if (res.IsSuccess) {
                this.router.navigate(["CRM/Insights/Reports", res.Data.Id]);
            }
            else {
                // Error
            }
        });
    }

    public delete() {
        this.reportService.delete(this.report.Id).subscribe(res => {
            if (res) {
                this.router.navigate(["CRM/Insights/Reports"]);
            }
        });
    }

    public newProjetCard() {

        this.edit = true;

        if (!this.report.Cards) {
            this.report.Cards = [];
        }


        this.report.Cards.push({ Description: "", Templates: [], Layout: "" })
    }

    setLayout(card: any, layout: any) {

        console.log("[Report] Setting layout", layout, " On card", card);

        let charts = card.Templates;

        card.Templates = [];
        for (let i = 0; i < layout.charts; i++) {
            if (charts[i])
                card.Templates.push(charts[i]);
            else {
                card.Templates.push({});
            }
        }

        card.Layout = layout.name;
    }

    setActiveChartType(chartType: any, insight: any) {
        console.log("Setting active chartType", chartType);
        this.activeChart.InsightType = insight.Type;
        this.activeChart.DocumentType = insight.DocumentType;
        this.activeChart.ChartId = chartType.Id;
        this.activeChart.Filter = new InsightFilter({ OnlyOneChartId: chartType.Id });
        console.log("[Report] Creating new Filter", this.activeChart.Filter);
        this.editFilter = true;
    }

    setActiveChart(chart: any) {
        console.log("Setting active chart", chart);
        this.activeChart = chart;
        this.editFilter = true;
    }

    setFilter(filter: InsightFilter) {
        this.activeChart.Filter = filter;
        this.editFilter = false;
    }

    ngOnDestroy() {
        if (this.reportSub) this.reportSub.unsubscribe();
        if (this.routeSub) this.routeSub.unsubscribe();
    }
}
