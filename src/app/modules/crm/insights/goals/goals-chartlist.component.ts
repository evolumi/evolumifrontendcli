import { Component } from '@angular/core';
import { GoalsService } from '../../../../services/goals.service';
import { Subscription } from "rxjs/Subscription";
import { GoalsModel } from "../../settings/goals/goals.models";
import { HighchartService } from "../../../../services/highchart.service";
import { Router } from "@angular/router";
import { ButtonPanelService } from "../../../../services/button-panel.service";

@Component({
    templateUrl: './goals-chartlist.component.html',
    selector: 'goals-chartlist'
})

export class GoalsChartlistComponent {
    //html
    public goalList: GoalsModel[] = [];

    //misc
    private goalListSubscription: Subscription;
    private buttonSub: Subscription;

    constructor(public goalsSvc: GoalsService,
        private highchart: HighchartService,
        private router: Router,
        private button: ButtonPanelService) {
    }

    ngOnInit() {
        this.goalsSvc.getGoals();
        this.goalListSubscription = this.goalsSvc.goalListObservable()
            .subscribe((data: any) => {
                if (data) {
                    this.goalList = data
                }
            });
        this.button.addPanelButton("GOALCREATE", "add-goal", "plus");
        this.buttonSub = this.button.commandObservable().subscribe(command => {
            if (command == "add-goal") this.goalsSvc.openAddModal();
        });
    }

    ngOnDestroy() {
        if (this.goalListSubscription) this.goalListSubscription.unsubscribe();
        if (this.buttonSub) this.buttonSub.unsubscribe();
        this.button.clearPanel();
    }

    getInsight(id: string) {
        this.highchart.filter.goalsId = id;
        this.router.navigateByUrl('/CRM/Insights/Standard/Goals/GoalsAll/' + id);
    }
}