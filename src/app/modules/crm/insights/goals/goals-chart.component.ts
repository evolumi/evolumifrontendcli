import { Component, OnInit } from '@angular/core';
import { HighchartService, InsightFilter } from '../../../../services/highchart.service';
import { DateService } from '../../../../services/date.service';
import { LocalStorage } from '../../../../services/localstorage';
import { GoalsService } from "../../../../services/goals.service";
import { ButtonPanelService } from "../../../../services/button-panel.service";
import { Subscription } from "rxjs/Subscription";
import { ActivatedRoute } from "@angular/router";
import { GoalsModel } from "../../settings/goals/goals.models";

@Component({
    selector: 'goals-chart',
    templateUrl: 'goals-chart.component.html',
    styleUrls: ['goals-chart.component.css']
})
export class GoalsChartComponent implements OnInit {
    private orgId: string;

    private currentDate: number;
    public showNext: boolean;
    public startDate: number;
    public endDate: number;
    public insightType = 'Goals';
    public goal: GoalsModel;
    // public goalId = 'f156343b-2c8f-4317-b103-eb31c990c036';

    private routeSub: Subscription;
    private buttonSub: Subscription;

    constructor(public highchart: HighchartService,
        private dateService: DateService,
        public goalsSvc: GoalsService,
        private button: ButtonPanelService,
        private route: ActivatedRoute) { }

    ngOnInit() {
        const localStorage = new LocalStorage();
        this.orgId = localStorage.get('orgId')

        this.routeSub = this.route.params
            .subscribe(params => {
                this.highchart.filter.goalsId = params.id;
                this.highchart.clear();
                this.highchart.update(this.insightType, 'GoalsAll', () => {
                    this.initDates();
                });
            })

        this.button.addPanelButton("GOALCREATE", "add-goal", "plus");
        this.buttonSub = this.button.commandObservable().subscribe(command => {
            if (command == "add-goal") {
                this.goalsSvc.openAddModal();
            }
        });
    }

    ngOnDestroy() {
        if (this.buttonSub) this.buttonSub.unsubscribe();
        if (this.routeSub) this.routeSub.unsubscribe();
        this.button.clearPanel();
    }

    initDates() {
        this.currentDate = new Date().getTime();
        this.startDate = +this.dateService.getFirstDateOfPeriod(this.highchart.goalInsight.Interval);
        this.endDate = +this.dateService.getLastDateOfPeriod(this.highchart.goalInsight.Interval);
    }

    public update() {
        this.highchart.update(this.insightType, 'GoalsAll', () => {
            this.initDates();
        });
    }

    public next(id: string, interval: string) {
        this.startDate = +this.dateService.getNextDateOfPeriod(this.startDate, interval);
        this.endDate = +this.dateService.getLastDateOfPeriod(interval, 'x', this.startDate);
        this.showNext = this.currentDate > this.endDate;
        this.highchart.filter.goalsStartDate = this.startDate;
        this.highchart.filter.goalsEndDate = this.endDate;
        this.highchart.updatePersonalGoals(`Reports/Goals/Update`);
    }

    public prev(id: string, interval: string) {
        this.showNext = true;
        this.startDate = +this.dateService.getPrevDateOfPeriod(this.startDate, interval);
        this.endDate = +this.dateService.getLastDateOfPeriod(interval, 'x', this.startDate);
        this.highchart.filter.goalsStartDate = this.startDate;
        this.highchart.filter.goalsEndDate = this.endDate;
        this.highchart.updatePersonalGoals(`Reports/Goals/Update`);
    }

    pin(id: string) {
        this.highchart.insightType = "Goals";
        this.highchart.insisghtId = id;
        this.highchart.filter.goalsStartDate = null;
        this.highchart.filter.goalsEndDate = null;
        this.highchart.pin(id);
    }
}