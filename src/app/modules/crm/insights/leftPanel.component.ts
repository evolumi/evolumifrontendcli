import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ReportService } from './reports/report.service';
import { Subscription } from 'rxjs/Subscription';
import { OrgService } from '../../../services/organisation.service';
import { GoalsService } from "../../../services/goals.service";
import { SalesCommissionService } from "../../../services/sales-commission.service";
import { HighchartService } from "../../../services/highchart.service";

@Component({
    selector: 'left-panel-insights',
    templateUrl: './leftPanel.html'
})
export class LeftPanelInsightsComponent implements OnInit {
    @Output() changeHeader = new EventEmitter();

    public reports: Array<any> = [];
    public openLeftPanel: boolean;

    private reportsSub: Subscription;
    private orgSub: Subscription;

    constructor(private translate: TranslateService,
        private router: Router,
        public reportService: ReportService,
        private orgSvc: OrgService,
        public goalsSvc: GoalsService,
        public salesCommSvc: SalesCommissionService,
        private highchart: HighchartService,
        private route: ActivatedRoute) { }

    ngOnInit() {
        this.reportsSub = this.reportService.reportsObserver().subscribe(reports => {
            if (reports) {
                this.reports = reports;
            }
        });

        this.orgSub = this.orgSvc.currentOrganisationObservable()
            .subscribe(org => {
                if (org) {
                    this.reportService.getInsightTypes();
                }
            })

        this.reportService.getReports();
        this.goalsSvc.getGoals();
        //this.salesCommSvc.getManySalesCommission();
    }

    ngOnDestroy() {
        if (this.reportsSub) this.reportsSub.unsubscribe();
        if (this.orgSub) this.orgSub.unsubscribe();
    }

    public setHeader(name: string) {
        this.changeHeader.next(this.translate.instant(name));
    }

    public routeGoal(id?: string) {
        console.log('ROUTE', this.route)
        if (id) {
            this.highchart.filter.goalsId = id;
            this.router.navigateByUrl('/CRM/Insights/Standard/Goals/GoalsAll/' + id);
        } else {
            this.router.navigateByUrl('/CRM/Insights/Standard/Goals/GoalsList')
        }
    }

    // openLeftPanel() {
    //     $(".leftPanel, #SideMenuOverlayLeft").addClass("systemActive");
    // }
    // removeOpenLeftPanel() {
    //     $(".leftPanel, #SideMenuOverlayLeft").removeClass("systemActive");
    // }
}
