import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { HighchartService, InsightFilter } from '../../../../services/highchart.service';
import { LocalStorage } from "../../../../services/localstorage";

@Component({
    templateUrl: './standard.html',
})

export class StandardChartComponent {

    private sub: any;

    private orgId: string;
    public insightType: string;
    public insightId: string;

    constructor(public route: ActivatedRoute, public higchart: HighchartService, private local: LocalStorage) {

    }

    ngOnInit() {
        this.orgId = this.local.get('orgId');

        this.higchart.clear();

        this.sub = this.route.params.subscribe(params => {
            this.insightType = params['type'];
            this.insightId = params['chartId'];
            if (this.higchart.insightType != this.insightId) {
                this.higchart.update(this.insightType, this.insightId);
            }
        });
    }

    ngOnDestroy(){
        if (this.sub) this.sub.unsubscribe();
    }

    public update(filter: InsightFilter) {
        console.log("Updating filter", filter);
        this.higchart.filter = filter;
        this.higchart.update(this.insightType, this.insightId);
    }
}
