import { Component } from '@angular/core';

@Component({
    template: `
            <div class="contentPanel accountsPage reports">
                <div class="container-fluid">
                    <div class="projectBox">
                        <h3>Insights intro</h3>
                        <p>Introduction text</p>
                     </div>
                </div>
            </div>
`
})

export class InsightsIntroComponent {

}

