import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { OrgService } from '../../../services/organisation.service';
import { ReportService } from './reports/report.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
    templateUrl: './insights.html',
})

export class InsightsComponent implements OnInit, OnDestroy {

    private orgSub: Subscription;

    constructor(private router: Router,
        private orgSvc: OrgService,
        private reportSvc: ReportService) { }

    ngOnInit() {
        this.orgSub = this.orgSvc.currentOrganisationObservable()
            .subscribe(org => {
                if (org) {
                    this.reportSvc.getInsightTypes(() => {
                        if (this.router.url.indexOf('Standard') == -1 && this.router.url.indexOf('Reports') == -1) {
                            if (this.reportSvc.insightTypes && this.reportSvc.insightTypes.length) {
                                this.router.navigate(['CRM/Insights/Standard', this.reportSvc.insightTypes[0].DocumentType, this.reportSvc.insightTypes[0].Type])
                            }
                        }
                    })
                }
            })
    }

    ngOnDestroy() {
        if (this.orgSub) this.orgSub.unsubscribe();
    }
}
