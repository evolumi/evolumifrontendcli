import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { SharedModule } from '../../shared/shared.module';

// Components
import { InsightsComponent } from './insights.component';
import { LeftPanelInsightsComponent } from './leftPanel.component';
import { InsightsIntroComponent } from './intro/intro.component';
import { ReportComponent } from './reports/reports.component';
import { StandardChartComponent } from './standard/standard.component';
import { GoalsChartComponent } from './goals/goals-chart.component';
import { GoalsChartlistComponent } from "./goals/goals-chartlist.component";

// Services
import { ReportService } from './reports/report.service';
import { SettingsSharedModule } from "../settings/shared/settings-shared.module";

@NgModule({
    imports: [
        FormsModule,
        HttpModule,
        CommonModule,
        SharedModule,
        SettingsSharedModule
    ],
    declarations: [
        InsightsComponent,
        LeftPanelInsightsComponent,
        ReportComponent,
        StandardChartComponent,
        InsightsIntroComponent,
        GoalsChartComponent,
        GoalsChartlistComponent
    ],
    providers: [
        ReportService,
    ]
})
export class InsightsModule { }