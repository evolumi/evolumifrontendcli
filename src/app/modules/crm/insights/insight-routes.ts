import { Routes } from '@angular/router';

// Components
import { InsightsComponent } from './insights.component';
import { InsightsIntroComponent } from './intro/intro.component';
import { StandardChartComponent } from './standard/standard.component';
import { ReportComponent } from './reports/reports.component';
import { GoalsChartComponent } from './goals/goals-chart.component';
import { GoalsChartlistComponent } from "./goals/goals-chartlist.component";

export const InsightRoutes: Routes = [
    {
        path: 'Insights',
        component: InsightsComponent,
        children: [
            { path: '', redirectTo: 'Intro', pathMatch: 'full' },
            { path: 'Intro', component: InsightsIntroComponent },
            { path: 'Standard/Goals/GoalsAll/:id', component: GoalsChartComponent },
            { path: 'Standard/Goals/GoalsAll', component: GoalsChartlistComponent },
            { path: 'Standard/Goals/GoalsList', component: GoalsChartlistComponent },
            { path: 'Standard/:type/:chartId', component: StandardChartComponent },
            { path: 'Reports', component: ReportComponent },
            { path: 'Reports/:id', component: ReportComponent },
        ]
    }
];