import { Component, OnInit } from '@angular/core';
import { OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { ButtonPanelService } from "../../../services/button-panel.service";
import { NewsletterService } from "../../../services/newsletter.service";
import { Router, ActivatedRoute } from "@angular/router";
import { NewsletterModel } from "../../../models/newsletter.models";
import { TemplateBuilderService } from "../../../services/template-builder.service";

@Component({
    selector: 'newsletter-details',
    templateUrl: 'newsletter-details.component.html'
})
export class NewsletterDetailsComponent implements OnInit, OnDestroy {

    public newsletter: NewsletterModel;

    private newsSub: Subscription;
    private routeSub: Subscription;
    private buttonSub: Subscription;

    constructor(private buttonSvc: ButtonPanelService,
        public newsSvc: NewsletterService,
        private router: Router,
        private route: ActivatedRoute,
        private templateSvc: TemplateBuilderService) { }

    ngOnInit() {
        this.routeSub = this.route.params
            .subscribe(params => {
                if (params && params['newsletterId']) {
                    console.log(params['newsletterId']);
                    this.newsSvc.getNewsletter(params['newsletterId']);
                }
            });

        this.newsSub = this.newsSvc.currentNewsletterObs()
            .subscribe(news => {
                if (news) {
                    this.newsletter = news;
                }
            });

        this.buttonSvc.addPanelButton('CREATEMAILLIST', 'add-maillist', 'plus');
        this.buttonSvc.addPanelButton('CREATENEWSLETTER', 'add-template', 'plus')
        this.buttonSub = this.buttonSvc.commandObservable().subscribe(command => {
            if (command === 'add-maillist') this.newsSvc.openAddEditMailList();
            if (command === 'add-template') this.router.navigateByUrl('CRM/Newsletters/Edit');
        });
    }

    ngOnDestroy() {
        if (this.buttonSub) this.buttonSub.unsubscribe();
        if (this.routeSub) this.routeSub.unsubscribe();
        if (this.newsSub) this.newsSub.unsubscribe();
        this.buttonSvc.clearPanel();
    }

    copy() {
        this.templateSvc.setCurrentTemplate(this.newsletter.Template);
        this.router.navigateByUrl('CRM/Newsletters/Edit');
    }
}