import { Routes } from '@angular/router';
import { Permissions } from '../../../services/organisation.service';
import { EvoRequireGuard } from '../../../services/guards/evo-require-guard';

import { NewslettersComponent } from './newsletters.component';
import { NewsletterTemplateComponent } from './newsletter-template.component';
import { NewsletterDetailsComponent } from "./newsletter-details.component";
import { TemplateSaveGuard } from "../../../services/guards/template-save.guard";

export const NewslettersRoutes: Routes = [
    {
        path: 'Newsletters',
        component: NewslettersComponent,
        canActivate: [EvoRequireGuard],
        data: { permissions: [Permissions.EvolumiAdmin] }
    },
    {
        path: 'Newsletters/Edit/:newsletterId',
        component: NewsletterTemplateComponent,
        canDeactivate: [TemplateSaveGuard],
        canActivate: [EvoRequireGuard],
        data: { permissions: [Permissions.EvolumiAdmin] }
    },
    {
        path: 'Newsletters/Edit',
        component: NewsletterTemplateComponent,
        canDeactivate: [TemplateSaveGuard],
        canActivate: [EvoRequireGuard],
        data: { permissions: [Permissions.EvolumiAdmin] },
    },
    {
        path: 'Newsletters/Details/:newsletterId',
        component: NewsletterDetailsComponent,
        canActivate: [EvoRequireGuard],
        data: { permissions: [Permissions.EvolumiAdmin] }
    }
];