import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from "rxjs/Subscription";
import { MailListModel } from "../../../../models/newsletter.models";
import { NewsletterService } from "../../../../services/newsletter.service";
import { ConfirmationService } from "../../../../services/confirmation.service";
import { TranslateService } from "@ngx-translate/core";

@Component({
    selector: 'maillist-list',
    templateUrl: 'maillist-list.component.html'
})
export class MaillistListComponent implements OnInit, OnDestroy {

    public mailLists: Array<MailListModel> = [];

    private mailSub: Subscription;

    constructor(private newsSvc: NewsletterService,
        private confSvc: ConfirmationService,
        private translate: TranslateService) { }

    ngOnInit() {
        this.newsSvc.getMailLists();
        this.mailSub = this.newsSvc.maillistListObs()
            .subscribe(lists => {
                if (lists) {
                    this.mailLists = lists;
                }
            });
    }

    ngOnDestroy() {
        if (this.mailSub) this.mailSub.unsubscribe();
    }

    edit(model: MailListModel) {
        this.newsSvc.openAddEditMailList(model);
    }

    delete(id: string) {
        this.confSvc.showModal(this.translate.instant('DELETEMAILLISTHEADER'), this.translate.instant('DELETEMAILLISTTEXT'), () => {
            this.newsSvc.deleteMailList(id);
        });
    }
}