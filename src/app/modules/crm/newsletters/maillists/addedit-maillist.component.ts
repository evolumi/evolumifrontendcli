import { Component, OnInit, OnDestroy, Output, EventEmitter, ViewChild, ElementRef, Renderer } from '@angular/core';
import { NewsletterService } from '../../../../services/newsletter.service';
import { OrgService } from '../../../../services/organisation.service';
import { AccountsCollection, SearchParams } from '../../../../services/accounts.service';
import { MailListModel, NonAccount } from '../../../../models/newsletter.models';
import { ChosenOptionModel } from '../../../shared/components/chosen/chosen.models';
import { Subscription } from 'rxjs/Subscription';
import { TranslateService } from '@ngx-translate/core';
import { OrganisationModel } from "../../../../models/organisation.models";

@Component({
    selector: 'addedit-maillist',
    templateUrl: 'addedit-maillist.component.html',
    styleUrls: ['addedit-maillist.css'],
    host: { '(document:click)': 'onClick($event)' }
})
export class AddEditMailListComponent implements OnInit, OnDestroy {

    @Output() mailListChange = new EventEmitter<MailListModel>();
    @ViewChild('warningLabel') warningLabel: ElementRef;
    @ViewChild('warningCb') warningCb: ElementRef;

    public currentMailList: MailListModel;
    public nonAccount = new NonAccount();

    private currentOrg: OrganisationModel;
    private currentVolumeLimit: number;
    private totalRecipientCount: number;

    public companyTagFilters: Array<{ label: string, value: any }>;
    public personTagFilters: Array<{ label: string, value: any }>;
    public accountTypes: Array<ChosenOptionModel> = [];

    public companyAccounts: Array<ChosenOptionModel> = [];
    public personAccounts: Array<ChosenOptionModel> = [];
    public selectedCompanyAccounts: Array<{ label: string, value: any }> = [];
    public selectedPersonAccounts: Array<{ label: string, value: any }> = [];
    public selectedCompanyAccountTypes: Array<{ label: string | number, value: string | number }> = [];
    public selectedPersonAccountTypes: Array<{ label: string | number, value: string | number }> = [];

    public companyNonAccounts: Array<NonAccount> = [];
    public personNonAccounts: Array<NonAccount> = [];

    private personFilter: SearchParams = new SearchParams({ IsPerson: true });
    private companyFilter: SearchParams = new SearchParams({ IsPerson: false });

    public allCompanies: boolean;
    public allPersons: boolean;

    public activeTab: string;
    public showAdds: boolean = true;
    public showButton: boolean;
    public countInProgress: boolean;
    public saveInProgress: boolean;

    public recipientCount: number = 0;
    public showWarning: boolean;
    public checkWarning: boolean;

    private mailListSub: Subscription;
    private allMailListSub: Subscription;
    private orgSub: Subscription;

    constructor(private newsSvc: NewsletterService,
        private accountSvc: AccountsCollection,
        private orgSvc: OrgService,
        private translate: TranslateService,
        private renderer: Renderer) {
    }

    ngOnInit() {
        this.accountSvc.companyList('', 1, (accounts: any) => {
            this.companyAccounts = accounts;
            this.initAccounts();
        });
        this.accountSvc.personList('', 1, (accounts) => {
            this.personAccounts = accounts;
            this.initAccounts();
        });

        this.mailListSub = this.newsSvc.currentMailListObs()
            .subscribe(mailList => {
                if (mailList) {
                    this.currentMailList = JSON.parse(JSON.stringify(mailList));

                    this.companyFilter = new SearchParams(this.currentMailList.CompanyFilter);
                    this.allCompanies = Boolean(this.currentMailList.CompanyFilter && (!this.currentMailList.CompanyFilter.Tags || !this.currentMailList.CompanyFilter.Tags.length) && (!this.currentMailList.CompanyFilter.AccountTypeId || !this.currentMailList.CompanyFilter.AccountTypeId.length));
                    this.personFilter = new SearchParams(this.currentMailList.PersonFilter);
                    this.allPersons = Boolean(this.currentMailList.PersonFilter && (!this.currentMailList.PersonFilter.Tags || !this.currentMailList.PersonFilter.Tags.length) && (!this.currentMailList.PersonFilter.AccountTypeId || !this.currentMailList.PersonFilter.AccountTypeId.length));
                    this.companyNonAccounts = this.currentMailList.NonAccounts.filter(x => x.OrganisationName);
                    this.personNonAccounts = this.currentMailList.NonAccounts.filter(x => x.FirstName);
                    this.initAccounts();
                    this.initAccountTypes();
                    this.initActiveTab();
                    if (mailList.Id) {
                        this.recipientCount = mailList.Recipients;
                        this.totalRecipientCount -= mailList.TotalRecipients;
                    }
                }
            });
        this.newsSvc.getMailLists();
        this.allMailListSub = this.newsSvc.maillistListObs()
            .subscribe(lists => {
                if (lists) {
                    this.totalRecipientCount += lists.reduce((a, b) => a + b.TotalRecipients, 0);
                }
            });
        this.orgSub = this.orgSvc.currentOrganisationObservable()
            .subscribe(org => {
                if (org) {
                    this.currentOrg = org;
                    this.accountTypes = org.AccountTypes.map(x => new ChosenOptionModel(x.Id, x.Name));
                    this.initAccountTypes();
                    let volumeObj = this.currentOrg.Packages.map(x => x.Volumes).find(x => x.Newsletters);
                    if (volumeObj) {
                        this.currentVolumeLimit = volumeObj.Newsletters
                    }
                }
            });

        this.getQuickFilters();
    }

    ngOnDestroy() {
        if (this.mailListSub) this.mailListSub.unsubscribe();
        if (this.orgSub) this.orgSub.unsubscribe();
        if (this.allMailListSub) this.allMailListSub.unsubscribe();
    }

    private initAccounts() {
        if (this.companyAccounts.length && this.currentMailList && this.currentMailList.CompanyIds && !this.selectedCompanyAccounts.length) {
            this.selectedCompanyAccounts = this.companyAccounts.filter(x => this.currentMailList.CompanyIds.indexOf(x.value.toString()) > -1).map(x => { return { label: x.label.toString(), value: x.value } });
        }
        if (this.personAccounts.length && this.currentMailList && this.currentMailList.PersonIds && !this.selectedPersonAccounts.length) {
            this.selectedPersonAccounts = this.personAccounts.filter(x => this.currentMailList.PersonIds.indexOf(x.value.toString()) > -1).map(x => { return { label: x.label.toString(), value: x.value } });
        }
    }

    private initAccountTypes() {
        if (this.currentOrg && this.companyFilter && this.companyFilter.AccountTypeId) {
            this.selectedCompanyAccountTypes = this.accountTypes.filter(x => this.companyFilter.AccountTypeId.indexOf(x.value.toString()) > -1);
        }
        if (this.currentOrg && this.personFilter && this.personFilter.AccountTypeId) {
            this.selectedCompanyAccountTypes = this.accountTypes.filter(x => this.personFilter.AccountTypeId.indexOf(x.value.toString()) > -1);
        }
    }

    private initActiveTab() {
        if (this.allCompanies && this.allPersons) {
            this.activeTab = '';
        } else if (this.allCompanies) {
            this.activeTab = this.currentMailList.PersonFilter || this.currentMailList.PersonIds.length || this.companyNonAccounts.length ? 'Person' : '';
        } else if (this.allPersons) {
            this.activeTab = this.currentMailList.CompanyFilter || this.currentMailList.CompanyIds.length || this.personNonAccounts.length ? 'Company' : '';
        } else {
            this.activeTab = this.currentMailList.CompanyFilter || this.currentMailList.CompanyIds.length || this.companyNonAccounts.length ? 'Company' : (this.currentMailList.PersonFilter || this.currentMailList.PersonIds.length || this.personNonAccounts.length ? 'Person' : '');
        }
    }

    onSave() {
        if (this.showWarning && !this.checkWarning) {
            this.validateWarning();
        } else {
            this.setCurrentMailList();
            this.saveInProgress = true;
            if (this.currentMailList.Id) {
                // Update
                this.newsSvc.updateMailList(this.currentMailList, (mailList: MailListModel) => {
                    this.saveInProgress = false;
                    this.currentOrg.MailLists = this.currentOrg.MailLists.map(x => x.Id === mailList.Id ? mailList : x);
                    this.mailListChange.emit(mailList);
                    this.back();
                }, () => this.saveInProgress = false);
            } else {
                // Add
                this.newsSvc.createMailList(this.currentMailList, (mailList: MailListModel) => {
                    this.saveInProgress = false;
                    if (!this.currentOrg.MailLists) this.currentOrg.MailLists = [];
                    this.currentOrg.MailLists.push(mailList);
                    this.currentOrg.MailLists = this.currentOrg.MailLists.slice();
                    this.mailListChange.emit(mailList);
                    this.back();
                }, () => this.saveInProgress = false);
            }

        }
    }

    private setCurrentMailList() {
        if ((this.companyFilter.Tags && this.companyFilter.Tags.length) || (this.companyFilter.AccountTypeId && this.companyFilter.AccountTypeId.length) || this.companyFilter.IsPerson === false) {
            this.currentMailList.CompanyFilter = this.companyFilter;
        } else {
            this.currentMailList.CompanyFilter = null;
        }
        if ((this.personFilter.Tags && this.personFilter.Tags.length) || (this.personFilter.AccountTypeId && this.personFilter.AccountTypeId.length) || this.personFilter.IsPerson === true) {
            this.currentMailList.PersonFilter = this.personFilter;
        } else {
            this.currentMailList.PersonFilter = null;
        }

        this.currentMailList.NonAccounts = this.companyNonAccounts.concat(this.personNonAccounts);
    }

    getRecipientCount() {
        this.setCurrentMailList();
        this.countInProgress = true;
        this.newsSvc.getMailListCount(this.currentMailList)
            .subscribe(res => {
                if (res != null) {
                    this.recipientCount = res;
                    this.checkVolume();
                }
                this.countInProgress = false;
            });
    }

    onTagsChange(value: any, type: string) {
        switch (type) {
            case 'Company':
                this.companyFilter.Tags = value;
                this.allCompanies = false;
                break;
            case 'Person':
                this.personFilter.Tags = value;
                this.allPersons = false;
        }
        this.getRecipientCount();
    }

    onAccountChange(event: Array<string>, type: string) {
        switch (type) {
            case 'Company':
                this.currentMailList.CompanyIds = event;
                this.selectedCompanyAccounts = this.companyAccounts.filter(x => this.currentMailList.CompanyIds.indexOf(x.value.toString()) > -1).map(x => { return { label: x.label.toString(), value: x.value } });
                this.allCompanies = false;
                break;
            case 'Person':
                this.currentMailList.PersonIds = event;
                this.selectedPersonAccounts = this.personAccounts.filter(x => this.currentMailList.PersonIds.indexOf(x.value.toString()) > -1).map(x => { return { label: x.label.toString(), value: x.value } });
                this.allPersons = false;
                break;
        }
        this.getRecipientCount();
    }

    onAccountTypeChange(event: Array<string>, type: string) {
        switch (type) {
            case 'Company':
                this.companyFilter.AccountTypeId = event;
                this.selectedCompanyAccountTypes = this.accountTypes.filter(x => this.companyFilter.AccountTypeId.indexOf(x.value.toString()) > -1);
                this.allCompanies = false;
                break;
            case 'Person':
                this.personFilter.AccountTypeId = event;
                this.selectedPersonAccountTypes = this.accountTypes.filter(x => this.personFilter.AccountTypeId.indexOf(x.value.toString()) > -1);
                this.allPersons = false;
                break;
        }
        this.getRecipientCount();
    }

    onAllChange(type: string, value: boolean) {
        switch (type) {
            case 'Company':
                this.allCompanies = value;
                if (this.allCompanies) this.companyFilter = new SearchParams({ IsPerson: false });
                else this.companyFilter = new SearchParams();
                this.currentMailList.CompanyIds = [];
                break;
            case 'Person':
                this.allPersons = value;
                if (this.allPersons) this.personFilter = new SearchParams({ IsPerson: true });
                else this.personFilter = new SearchParams();
                this.currentMailList.PersonIds = [];
                break;
        }
        this.getRecipientCount();
    }

    setTagFilter(filter: SearchParams, value: string) {
        filter.MustHaveAllTags = value;
    }

    deleteFilter(obj: any, prop?: string) {
        if (prop) obj[prop] = [];
        else obj.splice(0, obj.length);
        this.getRecipientCount();
    }

    deleteSelected(type: string, id: string, index: number) {
        switch (type) {
            case 'Company':
                this.selectedCompanyAccounts.splice(index, 1);
                this.currentMailList.CompanyIds = this.currentMailList.CompanyIds.filter(x => x !== id);
                break;
            case 'Person':
                this.selectedPersonAccounts.splice(index, 1);
                this.currentMailList.PersonIds = this.currentMailList.PersonIds.filter(x => x !== id);
                break;
        }
    }

    deleteAccountType(type: string, id: string, index: number) {
        switch (type) {
            case 'Company':
                this.selectedCompanyAccountTypes.splice(index, 1);
                this.companyFilter.AccountTypeId = this.companyFilter.AccountTypeId.filter(x => x !== id);
                break;
            case 'Person':
                this.selectedPersonAccountTypes.splice(index, 1);
                this.companyFilter.AccountTypeId = this.companyFilter.AccountTypeId.filter(x => x !== id);
                break;
        }
    }

    private getQuickFilters() {
        this.companyTagFilters = [
            { label: this.translate.instant('COMPANYTAGSALL'), value: true },
            { label: this.translate.instant('COMPANYTAGSANY'), value: false }
        ];
        this.personTagFilters = [
            { label: this.translate.instant('PERSONTAGSALL'), value: true },
            { label: this.translate.instant('PERSONTAGSANY'), value: false }
        ]
    }

    private validateWarning() {
        let colors = ['#a94442', '#727272'];
        let counter = 0;
        this.renderer.setElementStyle(this.warningLabel.nativeElement, 'font-weight', '500')
        let interval = setInterval(() => {
            this.renderer.setElementStyle(this.warningCb.nativeElement, 'border-color', colors[counter % 2]);
            this.renderer.setElementStyle(this.warningLabel.nativeElement, 'color', colors[counter % 2]);
            counter++;
            if (counter === 7) clearInterval(interval);
        }, 400);
    }

    addNonAccount(form: any, type: string) {
        switch (type) {
            case 'Company':
                this.companyNonAccounts.push(Object.assign({}, this.nonAccount));
                this.allCompanies = false;
                break;
            case 'Person':
                this.personNonAccounts.push(Object.assign({}, this.nonAccount));
                this.allPersons = false;
                break;
        }
        form.form.reset();
        this.nonAccount = new NonAccount();
        this.getRecipientCount();
    }

    private checkVolume() {
        if (this.totalRecipientCount + this.recipientCount > this.currentVolumeLimit) {
            this.showWarning = true;
        } else {
            this.showWarning = false;
        }
    }

    onClickTab(type: string) {
        switch (type) {
            case 'Company':
                this.activeTab = type;
                this.allCompanies = false;
                break;
            case 'Person':
                this.activeTab = type;
                this.allPersons = false;
                break;
        }
        this.nonAccount = new NonAccount();
        this.showAdds = false;
    }

    onClick(event: any) {
        let target = event.target || event.srcElement;
        console.log('TARGET', target);
        if (!this.showButton && target.className.indexOf('form-input') > -1) {
            this.showButton = true;
        } else if (this.showButton) {
            let form = document.getElementById('nonForms');
            if (form && !form.contains(target)) {
                this.showButton = false;
            }
        }
    }

    clickToClose(event: any) {
        let target = event.target || event.srcElement;
        if (target.id === 'addMaillist') {
            this.back();
        }
    }

    back() {
        this.newsSvc.closeAddEditMailList();
    }
}