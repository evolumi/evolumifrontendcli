import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { TemplateModel } from '../../../models/template.models';
import { MailSettings } from '../templates/template-settings'
import { Subscription } from 'rxjs/Subscription';
import { TemplateBuilderService } from '../../../services/template-builder.service';
import { NewsletterService } from "../../../services/newsletter.service";
import { NewsletterModel } from "../../../models/newsletter.models";
import { Router } from "@angular/router";

export class KeyupModel {
    value: string;
    max: number;
    min: number;
    obj: any;
    prop: any;
    element: HTMLInputElement;
    suffix?: string;

    constructor(value: string, max: number, min: number, obj: any, prop: any, element: HTMLInputElement, suffix?: string) {
        this.value = value;
        this.max = max;
        this.min = min;
        this.obj = obj;
        this.prop = prop;
        this.element = element;
        this.suffix = suffix;
    }
}

@Component({
    selector: 'newsletter-styling',
    templateUrl: './newsletter-styling.component.html',
    styleUrls: ['./newsletters.css']
})

export class NewsletterStylingComponent implements OnInit, OnDestroy {

    // Models
    @Input() template: TemplateModel
    @Input() newsletter: NewsletterModel;
    @Output() isUnsaved = new EventEmitter<boolean>();
    @Output() saveTemplate = new EventEmitter();

    public settings: any

    public showStyle: boolean;
    public showSend: boolean;
    public activeTab: string;

    // Subs
    private rowSub: Subscription;
    private blockSub: Subscription;

    [key: string]: any;

    constructor(private templateSvc: TemplateBuilderService,
        private newsletterSvc: NewsletterService,
        private router: Router) { }

    ngOnInit() {
        this.settings = MailSettings;

        this.rowSub = this.templateSvc.currentTemplateRowObs()
            .subscribe(row => {
                if (row) {
                    this.activeTab = 'Row';
                }
            });

        this.blockSub = this.templateSvc.currentTemplateBlockObs()
            .subscribe(block => {
                if (block) {
                    this.activeTab = 'Block';
                }
            });

        this.isUnsaved.emit(true);
    }

    ngOnDestroy() {
        if (this.rowSub) this.rowSub.unsubscribe();
        if (this.blockSub) this.blockSub.unsubscribe();
        this.resetTemplateBuilder();
    }

    create() {
        if (this.newsletter.SendNow) this.newsletter.SendDate = Date.now();
        this.newsletterSvc.addNewsletter(this.newsletter, () => {
            this.isUnsaved.emit(false);
            this.router.navigateByUrl('CRM/Newsletters');
        });
    }

    update() {
        if (this.newsletter.SendNow) this.newsletter.SendDate = Date.now();
        this.newsletterSvc.updateNewsletter(this.newsletter, () => {
            this.isUnsaved.emit(false);
            this.router.navigateByUrl('/CRM/Newsletters');
        });
    }

    resetTemplateBuilder() {
        this.templateSvc.setCurrentTemplate(null);
        this.templateSvc.setCurrentTemplateBlock(null);
        this.templateSvc.setCurrentTemplateColumn(null);
        this.templateSvc.setCurrentTemplateRow(null);

        this.isUnsaved.emit(false);
    }

    onClickTab(tab: string) {
        this.activeTab = tab;
    }
}