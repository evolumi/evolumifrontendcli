import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { NewsletterService } from '../../../services/newsletter.service';
import { MailListModel } from '../../../models/newsletter.models';
import { ButtonPanelService } from '../../../services/button-panel.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
    selector: 'newsletters',
    templateUrl: 'newsletters.component.html'
})
export class NewslettersComponent implements OnInit {

    @ViewChild('tabset') tabset: ElementRef;

    private buttonSub: Subscription;

    constructor(public newsSvc: NewsletterService,
        private buttonSvc: ButtonPanelService,
        private router: Router) { }

    ngOnInit() {
        this.buttonSvc.addPanelButton('CREATEMAILLIST', 'add-maillist', 'plus');
        this.buttonSvc.addPanelButton('CREATENEWSLETTER', 'add-template', 'plus')
        this.buttonSub = this.buttonSvc.commandObservable().subscribe(command => {
            if (command === 'add-maillist') this.openAddEditMailList();
            if (command === 'add-template') this.router.navigateByUrl('CRM/Newsletters/Edit');
        });
    }

    ngOnDestroy() {
        if (this.buttonSub) this.buttonSub.unsubscribe();
        this.buttonSvc.clearPanel();
    }

    editNewsletter(id: string) {
        this.router.navigateByUrl(`CRM/Newsletters/Edit/${id}`);
    }

    openAddEditMailList(mailList?: MailListModel) {
        this.newsSvc.openAddEditMailList(mailList);
    }
}