import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NewsletterModel } from "../../../models/newsletter.models";
import { Router } from "@angular/router";

export interface Percentages {
    Delivered: number;
    Opened: number;
    Unsubscribed: number;
    Spam: number;
    Clicked: number;
    Bounced: number;
    Dropped: number;
}

@Component({
    selector: 'newsletter-details-box',
    templateUrl: 'newsletter-details-box.component.html'
})
export class NewsletterDetailsBoxComponent implements OnInit {

    @Input() newsletter: NewsletterModel;
    @Output() copy = new EventEmitter<NewsletterModel>();

    public percent: Percentages;

    constructor(private router: Router) { }

    ngOnInit() {
        if (this.newsletter) {
            this.setPercentages();
        }
    }

    private setPercentages() {
        let news = this.newsletter;
        this.percent = { Delivered: 0, Opened: 0, Unsubscribed: 0, Spam: 0, Clicked: 0, Bounced: 0, Dropped: 0 };
        if (news.Sent) {
            this.percent.Delivered = Math.floor((news.Delivered / news.Sent) * 100);
            this.percent.Opened = Math.floor((news.Opened / news.Sent) * 100);
            this.percent.Unsubscribed = Math.floor((news.Unsubscribed / news.Sent) * 100);
            this.percent.Clicked = Math.floor((news.Clicked / news.Sent) * 100);
            this.percent.Spam = Math.floor((news.Spam / news.Sent) * 100);
            this.percent.Bounced = Math.floor((news.Bounced / news.Sent) * 100);
            this.percent.Dropped = Math.floor((news.Dropped / news.Sent) * 100);
        }
    }

    back() {
        this.router.navigateByUrl('CRM/Newsletters');
    }
}