import { Component, OnInit } from '@angular/core';
import { TemplateModel } from '../../../models/template.models';
import { TemplateBuilderService } from '../../../services/template-builder.service';
import { Subscription } from 'rxjs/Subscription';
import { NewsletterService } from '../../../services/newsletter.service';
import { NewsletterModel, MailListModel } from '../../../models/newsletter.models';
import { ActivatedRoute } from '@angular/router';
import { DefaultMailTemplate } from '../templates/template-settings';

@Component({
    selector: 'newsletter-template',
    templateUrl: './newsletter-template.component.html',
    styleUrls: ['./newsletters.css']
})

export class NewsletterTemplateComponent implements OnInit {

    public currentNewsletter: NewsletterModel;
    public selectedTemplate: TemplateModel;

    public goDirectToBuilder: boolean;
    public showSaveTemplate: boolean;
    // for the deactivated guard
    public unsaved: boolean;

    private routeSub: Subscription;
    private templateSub: Subscription;
    private newsletterSub: Subscription;

    constructor(private templateSvc: TemplateBuilderService,
        public newsletterSvc: NewsletterService,
        private route: ActivatedRoute) { }

    ngOnInit() {
        // this.unsaved = false;
        this.routeSub = this.route.params.subscribe(params => {
            if (params && params.newsletterId) {
                this.goDirectToBuilder = true;
                this.newsletterSvc.getNewsletter(params.newsletterId, (newsletter: NewsletterModel) => {
                    this.templateSvc.setCurrentTemplate(newsletter.Template);
                    this.goDirectToBuilder = false;
                });
            } else {
                this.newsletterSvc.setCurrentNewsletter(new NewsletterModel({}));
            }
        });

        this.templateSub = this.templateSvc.currentTemplateObs()
            .subscribe(template => {
                this.selectedTemplate = JSON.parse(JSON.stringify(template));
                if (this.currentNewsletter) {
                    this.currentNewsletter.Template = this.selectedTemplate;
                }
            });
        this.newsletterSub = this.newsletterSvc.currentNewsletterObs()
            .subscribe(news => {
                this.currentNewsletter = news;
                if (this.selectedTemplate) {
                    this.currentNewsletter.Template = this.selectedTemplate;
                }
            });
    }

    ngOnDestroy() {
        if (this.routeSub) this.routeSub.unsubscribe();
        if (this.templateSub) this.templateSub.unsubscribe();
        if (this.newsletterSub) this.newsletterSub.unsubscribe();
        this.newsletterSvc.setCurrentNewsletter(null);
    }

    mailListAdd(mailList: MailListModel) {
        if (this.currentNewsletter) {
            if (!this.currentNewsletter.MailLists.find(x => x === mailList.Id)) {
                this.currentNewsletter.MailLists.push(mailList.Id);
                this.currentNewsletter.MailLists = this.currentNewsletter.MailLists.slice();
            }
        }
    }

    isUnsaved(){
        return this.unsaved;
    }
}