import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges, OnDestroy, OnChanges, ViewChild } from '@angular/core';
import { NewsletterModel } from '../../../../models/newsletter.models';
import { NewsletterService } from '../../../../services/newsletter.service';
import { Subscription } from 'rxjs/Subscription';
import { OrgService } from '../../../../services/organisation.service';
import { ChosenOptionModel } from '../../../shared/components/chosen/chosen.models';
import { OrganisationModel } from "../../../../models/organisation.models";

@Component({
    selector: 'addedit-newsletter',
    templateUrl: './addedit-newsletter.component.html',
    styleUrls: ['../newsletters.css']
})

export class AddEditNewsletterComponent implements OnInit, OnChanges, OnDestroy {

    @ViewChild('sendform') form: any;
    @Input() newsletter: NewsletterModel;
    @Output() close = new EventEmitter();

    public recipientCount: number = 0;

    public edit: boolean;
    public sendNow: boolean = true;

    public mailListsChosen: Array<ChosenOptionModel> = [];

    private currentOrg: OrganisationModel;
    
    private orgSub: Subscription;

    constructor(private newsletterSvc: NewsletterService,
        private orgSvc: OrgService) { }

    ngOnInit() {
        this.orgSub = this.orgSvc.currentOrganisationObservable()
            .subscribe(org => {
                if (org && org.MailLists && org.MailLists.length) {
                    this.currentOrg = org;
                    this.mailListsChosen = org.MailLists.map(x => new ChosenOptionModel(x.Id, x.Name));
                    let mailLists = org.MailLists.filter(x => this.newsletter.MailLists.indexOf(x.Id) > -1).map(x => x.Id)
                    this.maillistChange(mailLists);
                }
            });
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes && changes['newsletter']) {
            let news = changes['newsletter'].currentValue;
            this.edit = Boolean(news && news.Id);
            this.newsletter.SendNow = Boolean(news && !news.SendDate);
        }
    }

    ngOnDestroy() {
        if (this.orgSub) this.orgSub.unsubscribe();
    }

    maillistChange(maillists: Array<string>) {
        this.newsletter.MailLists = maillists;
        this.recipientCount = this.currentOrg.MailLists.filter(x => maillists.indexOf(x.Id) > -1).reduce((a, b) => a + b.Recipients, 0);
    }

    openAddMailList() {
        this.newsletterSvc.openAddEditMailList();
    }
}