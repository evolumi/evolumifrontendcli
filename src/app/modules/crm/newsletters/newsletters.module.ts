import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { TemplateModule } from "../templates/template.module";

import { NewslettersComponent } from './newsletters.component';
import { NewsletterTemplateComponent } from './newsletter-template.component';
import { AddEditMailListComponent } from './maillists/addedit-maillist.component';
import { AddEditNewsletterComponent } from './add-edit/addedit-newsletter.component';
import { NewsletterListComponent } from "./newsletter-list.component";
import { MaillistListComponent } from "./maillists/maillist-list.component";
import { NewsletterDetailsComponent } from "./newsletter-details.component";
import { NewsletterDetailsBoxComponent } from "./newsletter-details-box.component";
import { NewsletterStylingComponent } from './newsletter-styling.component'

@NgModule({
    imports: [
        SharedModule,
        TemplateModule
    ],
    exports: [],
    declarations: [
        NewslettersComponent,
        NewsletterTemplateComponent,
        AddEditMailListComponent,
        AddEditNewsletterComponent,
        NewsletterListComponent,
        MaillistListComponent,
        NewsletterDetailsComponent,
        NewsletterDetailsBoxComponent,
        NewsletterStylingComponent,
    ],
})
export class NewslettersModule { }
