import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from "rxjs/Subscription";
import { NewsletterModel } from "../../../models/newsletter.models";
import { NewsletterService } from "../../../services/newsletter.service";
import { ConfirmationService } from "../../../services/confirmation.service";
import { TranslateService } from "@ngx-translate/core";
import { Router } from "@angular/router";

@Component({
    selector: 'newsletter-list',
    templateUrl: 'newsletter-list.component.html',
    styleUrls: ['newsletters.css']
})
export class NewsletterListComponent implements OnInit, OnDestroy {

    public scheduledNewsletters: Array<NewsletterModel> = [];
    public sentNewsletters: Array<NewsletterModel> = [];

    private newsSub: Subscription;

    constructor(public newsSvc: NewsletterService,
        private confSvc: ConfirmationService,
        private translate: TranslateService,
        private router: Router) { }

    ngOnInit() {
        this.newsSvc.getNewsletters();
        this.newsSub = this.newsSvc.newslettersListObs()
            .subscribe(news => {
                if (news) {
                    this.separateNewsletters(news);
                }
            })
    }

    ngOnDestroy() {
        if (this.newsSub) this.newsSub.unsubscribe();
    }

    edit(id: string) {
        this.router.navigateByUrl('CRM/Newsletters/Edit/' + id);
    }

    delete(id: string) {
        this.confSvc.showModal(this.translate.instant('DELETENEWSLETTERHEADER'), this.translate.instant('DELETENEWSLETTERTEXT'), () => {
            this.newsSvc.deleteNewsletter(id);
        });
    }

    show(id: string) {
        this.router.navigateByUrl('CRM/Newsletters/Details/' + id);
    }

    onScroll() {
        this.newsSvc.onScroll();
    }

    private separateNewsletters(newsletters: Array<NewsletterModel>) {
        let now = new Date().getTime();
        this.scheduledNewsletters = [];
        this.sentNewsletters = [];
        for (let i = 0; i < newsletters.length; i++) {
            if (newsletters[i].SendDate >= now) {
                this.scheduledNewsletters.push(newsletters[i]);
            } else {
                this.sentNewsletters.push(newsletters[i]);
            }
        }
    }
}