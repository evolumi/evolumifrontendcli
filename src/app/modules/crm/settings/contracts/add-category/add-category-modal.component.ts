import { Component, OnInit, OnDestroy } from '@angular/core';
import { ContractsModel } from '../../../../../models/contracts.models';
import { CategoryGroup } from '../../../../../models/category.models';
import { ContractsService } from '../../../../../services/contracts.service'
import { Subscription } from 'rxjs/Subscription';
import { CategoryService } from '../../../../../services/category.service';

@Component({

    selector: 'add-category-modal',
    templateUrl: 'add-category-modal.component.html',
    styleUrls: ['../contracts.css']
})
export class AddCategoryComponent implements OnInit, OnDestroy {

    private untouchedCategoryGroup: CategoryGroup;
    public categoryGroup: CategoryGroup;
    public showCategoryValid: boolean;
    public isEdit: boolean;

    private contracts: Array<ContractsModel> = [];

    private contractsSub: Subscription;
    private currentCategorySub: Subscription;

    constructor(private contractsService: ContractsService,
        private categorySvc: CategoryService) { }

    ngOnInit() {
        this.currentCategorySub = this.categorySvc.currentCategoryGroupObs()
            .subscribe((cat: any) => {
                if (cat && cat.Id) {
                    this.isEdit = true;
                }
                this.categoryGroup = JSON.parse(JSON.stringify(cat));
                this.untouchedCategoryGroup = JSON.parse(JSON.stringify(cat));
            })

        if (this.isEdit) {
            this.contractsSub = this.contractsService.contractsListObs()
                .subscribe(contracts => {
                    this.contracts = contracts;
                });
        }
    }

    ngOnDestroy() {
        if (this.contractsSub) this.contractsSub.unsubscribe();
        if (this.currentCategorySub) this.currentCategorySub.unsubscribe();
    }

    back() {
        this.categorySvc.closeEditCreateCategoryGroup();
    }

    createCategory() {
        if (this.validation()) {
            this.categoryGroup.Type = 'Contracts';
            this.categorySvc.addCategoryGroup(this.categoryGroup, this.back.bind(this));
        }
    }

    updateCategory() {
        if (this.validation()) {
            this.categorySvc.updateCategoryGroup(this.categoryGroup, this.back.bind(this));
        }
    }

    validation(): boolean {
        if (!this.categoryGroup.IsEditable && this.categoryGroup.Categories.length < 1) {
            this.showCategoryValid = true;
            return false;
        }
        return true;
    }

    clickToClose(event: any) {
        let target = event.target || event.srcElement;
        if (target.id === 'addCategoryGroup') {
            this.back();
        }
    }
}