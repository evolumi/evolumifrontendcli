import { Component, OnInit } from '@angular/core';
import { CategoryGroup } from '../../../../models/category.models';
import { CategoryService } from '../../../../services/category.service';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs/Subscription';
import { ButtonPanelService } from '../../../../services/button-panel.service';
import { ConfirmationService } from '../../../../services/confirmation.service';

@Component({
    selector: 'contracts-settings',
    templateUrl: 'contracts.component.html'
})
export class ContractSettingsComponent implements OnInit {

    public categoryGroups: Array<CategoryGroup> = [];

    private buttonSub: Subscription;
    private catSub: Subscription;

    constructor(private button: ButtonPanelService,
        public catSvc: CategoryService,
        private translate: TranslateService,
        private confSvc: ConfirmationService) {
    }

    ngOnInit() {
        this.catSub = this.catSvc.contractsCategoryListObs()
            .subscribe(list => this.categoryGroups = list);
        this.button.addPanelButton("CREATECATEGORY", "add-category", "plus");
        this.buttonSub = this.button.commandObservable().subscribe(command => {
            if (command == "add-category") this.openCreate();
        })
    }

    ngOnDestroy() {
        if (this.buttonSub) this.buttonSub.unsubscribe();
        if (this.catSub) this.catSub.unsubscribe();
        this.button.clearPanel();
    }

    openCreate() {
        this.catSvc.openEditCreateCategoryGroup();
    }

    openEdit(id: string) {
        this.catSvc.openEditCreateCategoryGroup(id);
    }

    openDelete(id: string) {
        this.confSvc.showModal(this.translate.instant('DELETECATEGORYGROUP'), this.translate.instant('WARNDELETECATEGORYGROUP'), () => {
            this.catSvc.deleteCategoryGroup(id);
        })
    }
}