import { NgModule } from '@angular/core';
import { SharedModule } from '../../../shared/shared.module';

// Components
import { UpdateCardComponent } from '../payment/update-card.component';
import { AddGoalModalComponent } from "../goals/add-goal-modal.component";
import { EditGoalModalComponent, GoalDataActivePipe } from "../goals/edit-goal-modal.component";
import { AddCategoryComponent } from "../contracts/add-category/add-category-modal.component";
import { GoalDataListComponent } from "../goals/goaldata-list.component";

@NgModule({
    imports: [
        SharedModule,
    ],
    declarations: [
        UpdateCardComponent,
        AddGoalModalComponent,
        EditGoalModalComponent,
        AddCategoryComponent,
        GoalDataListComponent,
        GoalDataActivePipe
    ],
    exports: [
        UpdateCardComponent,
        AddGoalModalComponent,
        EditGoalModalComponent,
        AddCategoryComponent,
        GoalDataListComponent,
        GoalDataActivePipe
    ]
})
export class SettingsSharedModule { }