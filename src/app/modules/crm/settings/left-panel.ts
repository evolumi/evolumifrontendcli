import { Component } from '@angular/core';
import { OrgService, Features, Permissions } from '../../../services/organisation.service';
import { Subscription } from 'rxjs/Subscription';
import { OrganisationModel } from "../../../models/organisation.models";

@Component({
  selector: 'left-panel',
  templateUrl: 'left-panel.html'
})
export class LeftPanel {

  public settingsLinks: Array<any> = [];
  public customizeLinks: Array<any> = [];

  private orgSub: Subscription;

  constructor(orgService: OrgService) {
    this.orgSub = orgService.currentOrganisationObservable().subscribe(org => {

      if (org) {
        this.buildLinks(org);
      }
    });
  }

  ngOnDestroy() {
    if (this.orgSub) this.orgSub.unsubscribe();
  }

  private buildLinks(org: OrganisationModel) {
    this.settingsLinks = [];
    this.customizeLinks = [];

    this.settingsLinks.push(
      { id: "GeneralSettings", link: "./General", label: "GENERALSETTINGS" },
      { id: "SMTPSettings", link: "./SMTP", label: "SMTPSETTINGS" },
      { id: "PhoneSettings", link: "./Phone", label: "PHONESETTINGS", features: [Features.Click2Call], permissions: [Permissions.EvolumiAdmin] },
      { id: "BillingSettings", link: "./Payment", label: "BILLING", permissions: [Permissions.HandlePayment] },
      { id: "UserSettings", link: "./Users", label: "USERS", permissions: [Permissions.HandleUsers] },
      { id: "RolesSettings", link: "./Roles", label: "USERROLES", permissions: [Permissions.HandleUserRoles] },
      { id: "ProductSettings", link: "./Products", label: "PRODUCTS", features: [Features.Products], permissions: [Permissions.HandleProducts] },
      { id: "GoalsSettings", link: "./Goals", label: "GOALS", features: [Features.Goals], permissions: [Permissions.HandleGoals] },
      { id: "Contracts", link: "./Contracts", label: "CONTRACTS", features: [Features.Contracts], permissions: [Permissions.HandleContracts, Permissions.EvolumiAdmin] },
      { id: "ComissionSettings", link: "./SalesCommissions", label: "SALESCOMMISSIONS", features: [Features.SalesCommisions], permissions: [Permissions.HandleSalesCommissions, Permissions.EvolumiAdmin] },
      { id: "Calendar", link: "./Calendar", label: "CALENDAR", features: [Features.CalendarSync] },
      { id: "Documents", link: "./Documents", label: "DOCUMENTTEMPLATES", features: [Features.DocumentTemplates] }
    );
    this.customizeLinks.push(
      { id: "DealSettings", link: "./Deals", label: "DEALS", permissions: [Permissions.HandleDealStatus] },
      { id: "AccountSettings", link: "./Accounts", label: "ACCOUNTS", permissions: [Permissions.EditAccounts] },
      { id: "VatSettings", link: "./Vat", label: "VAT", permissions: [Permissions.HandlePayment] },
      { id: "UnitSettings", link: "./Units", label: "UNITS", permissions: [Permissions.HandleProducts] }
    );
  }

  fail(key: string) {
    if (key == 'Goals') {
      console.log(key)
      this.settingsLinks.push({ id: "GoalsFeature", link: "/CRM/Features/Goals", label: "GOALS" })
    } else if (key == 'Products') {
      console.log(key)
      this.settingsLinks.push({ id: "ProductFeature", link: "/CRM/Features/Products", label: "PRODUCTS" })
    }
  }

  openLeftPanel() {
    let leftEl = document.getElementById('SettingsLeftPanel');
    let sideEl = document.getElementById('SideMenuOverlayLeft');
    if (leftEl) leftEl.className += ' systemActive';
    if (sideEl) sideEl.className += ' systemActive';
    // $(".leftPanel, #SideMenuOverlayLeft").addClass("systemActive");
  }
  removeOpenLeftPanel() {
    let leftEl = document.getElementById('SettingsLeftPanel');
    let sideEl = document.getElementById('SideMenuOverlayLeft');
    if (leftEl) leftEl.className = leftEl.className.replace('systemActive', '');
    if (sideEl) sideEl.className = sideEl.className.replace('systemActive', '');
    // $(".leftPanel, #SideMenuOverlayLeft").removeClass("systemActive");
  }
}
