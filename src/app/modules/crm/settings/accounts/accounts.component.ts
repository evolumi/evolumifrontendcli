import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Api } from '../../../../services/api';
import { Subscription } from 'rxjs/Subscription';
import { DragulaService } from '../../../../draggable/providers/dragula.provider';
import { TranslateService } from '@ngx-translate/core';
import { LocalStorage } from '../../../../services/localstorage';
import { NotificationService } from '../../../../services/notification-service';
import { ValidationService } from '../../../../services/validation.service';
import { OrgService } from '../../../../services/organisation.service';
import { PermissionService } from '../../../../services/permissionService';

export class Account {
    Name: string;
    Color: string;
    Id: string;

    constructor(name: string, color: string) {
        this.Name = name;
        this.Color = color;
    }
}

@Component({
    templateUrl: './accounts.html',
    viewProviders: [DragulaService,],
})

export class CustomizeAccountsComponent {

    public colorpickercolor: string;
    public accounts: Array<Account>;
    public accountTypes: Array<Account> = [];
    public addAccountPopUp: boolean = false;
    public editAccountPopup: boolean = false;
    public deleteAccountPopup: boolean = false;
    public items: any;
    public accountEditIndex: number;
    public accountDeleteIndex: number;
    public http: any;

    public typeValue: any = '';
    public colorValue: string;
    public newAccount: Account;
    public AccountForm: any;
    private defaultColor: string = '#333';

    public DBid: any;
    public process = false;
    public disableColorBox = true;
    private form: any;
    public isValidColor = true;

    public companyFields: Array<any> = [];
    public personFields: Array<any> = [];
    public relationFields: Array<any> = [];

    public customFieldsInfo: boolean;

    private accountTypeSub: Subscription;
    private dragSub: Subscription;
    private permSub: Subscription;

    constructor(private permissions: PermissionService,
        private _http: Api,
        public dragulaService: DragulaService,
        private translate: TranslateService,
        public _localstore: LocalStorage,
        form: FormBuilder,
        private _notify: NotificationService,
        private orgService: OrgService
    ) {
        this.companyFields = [...this.orgService.currentOrganisation().AccountCustomFields];
        this.personFields = [...this.orgService.currentOrganisation().PersonCustomFields];
        this.relationFields = [...this.orgService.currentOrganisation().RelationCustomFields];



        this.colorpickercolor = this.defaultColor;
        this.form = form;
        this.AccountForm = form.group({
            type: ['', Validators.required],
            color: ['', Validators.compose([ValidationService.hexvalidator])]
        });

        dragulaService.setOptions('nested-bag', {
            revertOnSpill: true
        });

        this.dragSub = dragulaService.dropModel.subscribe((value: any) => {
            this.updateAccount();
        });
    }

    ngOnInit() {
        this.permSub = this.permissions.IsLoaded().subscribe(res => {
            if (res) {
                if (!this.permissions.handleAccountTypes) {
                    this.permissions.RedirectForbidden();
                }
            }
        });

        this.accountTypeSub = this.orgService.currentOrganisationObservable().subscribe(org => {
            this.accountTypes = org.AccountTypes;
        });

    }

    ngOnDestroy() {
        if (this.accountTypeSub) this.accountTypeSub.unsubscribe();
        if (this.permSub) this.permSub.unsubscribe();
        if (this.dragSub) this.dragSub.unsubscribe();
    }

    updateAccount() {
        var orgId = this._localstore.get('orgId');

        this._http.put('Organisations/' + orgId + '/UpdateAccountTypes', this.accountTypes)
            .map((res: any) => res.json())
            .subscribe((res: any) => {
                if (res.IsSuccess === true) {
                    if (this.deleteAccountPopup) {
                        this._notify.success(this.translate.instant('DELETESUCCESSFUL'));
                    }
                    this.orgService.accountTypesChange(this.accountTypes);
                }
            }, (err: any) => {
                this.process = false;
                this.addAccountPopUp = false;
                this.addAccountPopUp = false;
                this.editAccountPopup = false;
                this.deleteAccountPopup = false;
                this._notify.error(this.translate.instant('SOMETHINGWRONG'));
            }, () => {
                this.addAccountPopUp = false;
                this.editAccountPopup = false;
                this.process = false;
                this.deleteAccountPopup = false;
            });
    }

    toggleAccountPopupDisplay() {
        this.addAccountPopUp = false;
        this.editAccountPopup = false;
    }

    toggleDeleteAccountPopupDisplay() {
        this.deleteAccountPopup = false;
    }

    //delete an item
    delete() {
        this.process = true;
        var orgId = this._localstore.get('orgId');
        this._http.delete('Organisations/' + orgId + '/AccountTypes/' + this.DBid)
            .map((res: any) => res.json())
            .subscribe((res: any) => {
                if (res.IsSuccess === true) {
                    this.accountTypes.splice(this.accountTypes.findIndex(x => x.Id == this.DBid));
                    this.orgService.accountTypesChange(this.accountTypes);
                    this.deleteAccountPopup = false;
                    this._notify.error(this.translate.instant('DELETED'));
                }
                else {
                    this.process = false;
                    this.deleteAccountPopup = false;
                    this._notify.error(res.Message);
                }
            }, (err: any) => {
                this.process = false;
                this.deleteAccountPopup = false;
                this._notify.error(this.translate.instant('SOMETHINGWRONG'));
            });
    }

    //Edit or Create account details
    editAccount(editedAccount: string, editedColor: string, dbId: string) {

        this.process = true;
        this.newAccount = new Account(editedAccount, editedColor);
        var orgId = this._localstore.get('orgId');

        if (this.addAccountPopUp) {
            if (this.IsDuplicate(editedAccount, this.accountEditIndex)) {
                this._notify.error(this.translate.instant('ACCEXISTS'));
                return;
            } else {
                this._http.post('Organisations/' + orgId + '/AccountTypes/', this.newAccount)
                    .map((res: any) => res.json())
                    .subscribe((res: any) => {
                        if (res.IsSuccess === true) {
                            this.newAccount.Id = res.Data;
                            this.accountTypes.push(this.newAccount);
                            this.orgService.accountTypesChange(this.accountTypes);
                            this.toggleAccountPopupDisplay();
                        }
                    });
            }
        }
        else {
            this._http.put('Organisations/' + orgId + '/AccountTypes/' + dbId, this.newAccount)
                .map((res: any) => res.json())
                .subscribe((res: any) => {
                    if (res.IsSuccess === true) {
                        this.orgService.accountTypesChange(this.accountTypes);
                        this.toggleAccountPopupDisplay();
                    }
                });
        }
    }

    //Add an account
    IsDuplicate(newAccountName: string, index: number): boolean {
        return this.accountTypes.find(x => x.Name == newAccountName) != undefined;
    }

    //Show Edit popup
    showEditAccountPopup(accountIndex: number, DBid: string) {
        this.isValidColor = true;
        this.process = false;
        this.accountEditIndex = accountIndex;
        this.typeValue = this.accountTypes[accountIndex].Name;
        this.colorValue = this.accountTypes[accountIndex].Color;
        this.colorpickercolor = this.accountTypes[accountIndex].Color;
        this.editAccountPopup = true;
        this.addAccountPopUp = false;
        this.DBid = DBid;
    }

    //Show add popup
    showAddAccountPopup() {
        this.newAccount = new Account("", this.defaultColor);
        this.AccountForm.type = "";
        this.AccountForm.color = this.defaultColor;
        this.isValidColor = true;
        this.process = false;
        this.typeValue = '';
        this.colorpickercolor = this.defaultColor;
        this.addAccountPopUp = true;
        this.editAccountPopup = false;

    }

    checkIsBright(c: any) {
        var c = c.substring(1);      // strip #
        var rgb = parseInt(c, 16);   // convert rrggbb to decimal
        var r = (rgb >> 16) & 0xff;  // extract red
        var g = (rgb >> 8) & 0xff;  // extract green
        var b = (rgb >> 0) & 0xff;  // extract blue
        var luma = 0.2126 * r + 0.7152 * g + 0.0722 * b; // per ITU-R BT.709
        if (luma < 90) {
            return "#FFFFFF";
        }
        else {
            return "#000000";
        }
    }

    showDeleteAccountPopup(accountIndex: number, Dbid: string) {
        this.process = false;
        this.DBid = Dbid;
        this.deleteAccountPopup = true;
        this.accountDeleteIndex = accountIndex;
        this.typeValue = this.accountTypes[accountIndex].Name;
    }

    //Cancel popup
    cancel(from: any) {
        this.editAccountPopup = false;
        this.addAccountPopUp = false;
        this.deleteAccountPopup = false;
    }

    clickToClose(event: any, type: any) {
        var target = event.target || event.srcElement;

        if (type === 'add' && target.id === 'addAccount') {
            this.editAccountPopup = false;
            this.addAccountPopUp = false;
        } else if (type === 'delete' && event.srcElement.id === 'deleteaccount') {
            this.deleteAccountPopup = false;
        }
    }

    onColorPickerChange(event: any) {
        var hexformat = /(^#[0-9A-Fa-f]{6}$)|(^#[0-9A-Fa-f]{3}$)/;
        if (event.match(hexformat)) {
            this.isValidColor = true;
        }
        else {
            this.isValidColor = false;
        }
    }

    saveFields() {
        let org = this.orgService.currentOrganisation();
        org.AccountCustomFields = this.companyFields;
        org.PersonCustomFields = this.personFields;
        org.RelationCustomFields = this.relationFields;
        this.orgService.updateOrganisation(org);
    }
}
