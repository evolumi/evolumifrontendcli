import { Component } from '@angular/core';
import { Api } from "../../../../services/api";
import { OrgService } from "../../../../services/organisation.service";
import { Subscription } from "rxjs/Subscription";
import { Visitor } from '../../../../models/visitor.models';
import { OrganisationModel } from "../../../../models/organisation.models";

@Component({
    selector: 'visitor',
    templateUrl: './visitors.component.html',
    styleUrls: ['./visitors.css']
})

export class VisitorComponent {
    constructor(private api: Api, private orgSvc: OrgService) { }

    private orgSub: Subscription;
    private org: OrganisationModel;

    private visitors: Visitor[];
    ngOnInit() {
        this.orgSub = this.orgSvc.currentOrganisationObservable().subscribe(data => this.org = data);
    }

    ngOnDestroy(){
        if (this.orgSub) this.orgSub.unsubscribe();
    }

    getVisitors() {
        this.api.get(`Organisations/${this.org.Id}/Sitevisitors`).map(data => data.json()).subscribe(data => {
            if (data.IsSuccess) {
                this.visitors = data
            }
        });
    }
    clickFunc() {
        console.log("now what?");
    }
}