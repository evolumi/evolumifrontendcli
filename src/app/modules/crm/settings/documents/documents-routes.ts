import { Routes } from '@angular/router';
import { Permissions } from '../../../../services/organisation.service';
import { EvoRequireGuard } from '../../../../services/guards/evo-require-guard';
import { DocumentsComponent } from "./documents.component";
import { DocumentsTemplateComponent } from "./templates/documents-template.component";
import { TemplateSaveGuard } from "../../../../services/guards/template-save.guard";

export const DocumentRoutes: Routes = [
    {
        path: 'Documents',
        component: DocumentsComponent,
        canActivate: [EvoRequireGuard],
        data: { permissions: [Permissions.EvolumiAdmin] }
    },
    {
        path: 'Documents/Templates/:newsletterId',
        component: DocumentsTemplateComponent,
        canDeactivate: [TemplateSaveGuard],
        canActivate: [EvoRequireGuard],
        data: { permissions: [Permissions.EvolumiAdmin] }
    },
    {
        path: 'Documents/Templates',
        component: DocumentsTemplateComponent,
        canDeactivate: [TemplateSaveGuard],
        canActivate: [EvoRequireGuard],
        data: { permissions: [Permissions.EvolumiAdmin] },
    },
];