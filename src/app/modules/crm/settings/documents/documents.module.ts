import { NgModule } from '@angular/core';
import { SharedModule } from "../../../shared/shared.module";
import { TemplateModule } from "../../templates/template.module";
import { DocumentsComponent } from './documents.component';
import { DocumentsTemplateComponent } from "./templates/documents-template.component";
import { DocumentsStylingComponent } from './templates/documents-styling.component';

@NgModule({
  imports: [
    SharedModule,
    TemplateModule
  ],
  declarations: [
    DocumentsComponent,
    DocumentsTemplateComponent,
    DocumentsStylingComponent
  ]
})
export class DocumentsModule { }
