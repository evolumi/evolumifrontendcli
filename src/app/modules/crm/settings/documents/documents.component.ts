import { Component, OnInit } from '@angular/core';
import { ButtonPanelService } from "../../../../services/button-panel.service";
import { Subscription } from "rxjs/Subscription";
import { Router } from "@angular/router";

@Component({
  selector: 'documents',
  templateUrl: './documents.component.html',
  styleUrls: ['./documents.component.css']
})
export class DocumentsComponent implements OnInit {

  private buttonSub: Subscription;

  constructor(private buttonSvc: ButtonPanelService,
    private router: Router) { }

  ngOnInit() {
    this.buttonSvc.addPanelButton('CREATETEMPLATE', 'template', 'plus');
    this.buttonSub = this.buttonSvc.commandObservable().subscribe(command => {
      if (command === 'template') this.router.navigateByUrl('CRM/Settings/Documents/Templates')
    });
  }

  ngOnDestroy(){
    if (this.buttonSub) this.buttonSub.unsubscribe();
    this.buttonSvc.clearPanel();
  }

}
