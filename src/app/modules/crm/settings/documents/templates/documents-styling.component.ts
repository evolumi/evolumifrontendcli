import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { TemplateModel } from "../../../../../models/template.models";
import { TemplateBuilderService } from "../../../../../services/template-builder.service";
import { Router } from "@angular/router";
import { DocumentSettings } from "../../../templates/template-settings";
import { Subscription } from "rxjs/Subscription";

@Component({
  selector: 'documents-styling',
  templateUrl: './documents-styling.component.html',
  styleUrls: ['./documents-styling.component.css']
})
export class DocumentsStylingComponent implements OnInit {

  @Input() template: TemplateModel;
  @Output() isUnsaved = new EventEmitter<boolean>();

  public settings: any;
  public activeTab: string;

  public showSave: boolean;
  public showSend: boolean;

  private blockSub: Subscription;
  private rowSub: Subscription;

  constructor(private templateSvc: TemplateBuilderService,
    private router: Router) { }

  ngOnInit() {
    this.settings = DocumentSettings;

    this.rowSub = this.templateSvc.currentTemplateRowObs()
      .subscribe(row => {
        if (row) {
          this.activeTab = 'Row';
        }
      });

    this.blockSub = this.templateSvc.currentTemplateBlockObs()
      .subscribe(block => {
        if (block) {
          this.activeTab = 'Block';
        }
      });

    this.isUnsaved.emit(true);
  }

  ngOnDestroy() {
    if (this.rowSub) this.rowSub.unsubscribe();
    if (this.blockSub) this.blockSub.unsubscribe();
    this.resetTemplateBuilder();
  }

  create(){
    console.log('CREATE NOT IMPLEMENTED');
  }

  update(){
    console.log('UPDATE NOT IMPLEMENTED');
  }

  save() {
    if (this.template.Id) {
      this.templateSvc.updateTemplate(this.template, () => {
        this.isUnsaved.emit(false);
        this.router.navigateByUrl('CRM/Settings/Documents');
      });
    } else {
      this.templateSvc.addTemplate(this.template, () => {
        this.isUnsaved.emit(false);
        this.router.navigateByUrl('CRM/Settings/Documents');
      });
    }
  }

  resetTemplateBuilder() {
    this.templateSvc.setCurrentTemplate(null);
    // this.templateSvc.setCurrentTemplateBlock(null);
    // this.templateSvc.setCurrentTemplateColumn(null);
    // this.templateSvc.setCurrentTemplateRow(null);

    this.isUnsaved.emit(false);
  }
}
