import { Component, OnInit, OnDestroy } from '@angular/core';
import { TemplateDocumentModel } from "../../../../../models/template.models";
import { DefaultMailTemplate } from "../../../templates/template-settings";
import { TemplateBuilderService } from "../../../../../services/template-builder.service";
import { Subscription } from "rxjs/Subscription";

@Component({
  selector: 'documents-template',
  templateUrl: './documents-template.component.html',
  styleUrls: ['./documents-template.component.css']
})
export class DocumentsTemplateComponent implements OnInit {

  public selectedTemplate: TemplateDocumentModel;

  public showSaveTemplate: boolean;
  public unsaved: boolean;

  private templateSub: Subscription;

  constructor(private templateSvc: TemplateBuilderService) { }

  ngOnInit() {
    this.templateSub = this.templateSvc.currentTemplateObs()
      .subscribe(temp => {
        this.selectedTemplate = JSON.parse(JSON.stringify(temp));
      })
  }

  ngOnDestroy() {
    if (this.templateSub) this.templateSub.unsubscribe();
  }

  isUnsaved(){
    return this.unsaved;
  }

}
