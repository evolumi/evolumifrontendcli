import { Component, OnInit } from '@angular/core';
import { LocalStorage } from '../../../../services/localstorage';
import { Api } from '../../../../services/api';
import { FormBuilder, Validators } from '@angular/forms';
import { NotificationService } from '../../../../services/notification-service';
import { TranslateService } from '@ngx-translate/core';
import { DragulaService } from '../../../../draggable/providers/dragula.provider';
import { FilterCollection } from '../../../../services/filterservice';
import { PermissionService } from '../../../../services/permissionService';
import { Subscription } from "rxjs/Subscription";

@Component({

	templateUrl: './vat.html',
	providers: [LocalStorage],
	viewProviders: [DragulaService]
})
export class VatComponent implements OnInit {
	public test1: string;
	public orgId: string;
	public vats: Array<any>;
	public addVatPopUp: boolean = false;
	public editVatPopup: boolean = false;
	public deleteVatPopup: boolean = false;
	public items: any;
	public vatEditIndex: number;
	public vatDeleteIndex: number;
	public _http: any;
	public VatForm: any;
	public vatValue: any;
	public isAddpopup: string = 'add';

	private dragSub: Subscription;
	private permSub: Subscription;

	constructor(private permissions: PermissionService, private filter: FilterCollection, _localstore: LocalStorage, _http: Api, public form: FormBuilder, private _notify: NotificationService, private translate: TranslateService, public dragulaService: DragulaService) {
		this.orgId = _localstore.get('orgId');
		this.vatValue = '';
		this.VatForm = form.group({
			vat: ['', Validators.required],
		});
		dragulaService.setOptions('nested-bag', {
			revertOnSpill: true
		});
		this.dragSub = dragulaService.dropModel.subscribe((value: any) => {
			this.onDrop(value.slice(1));
		});
		this._http = _http;
	}
	ngOnInit() {
		this.permSub = this.permissions.IsLoaded().subscribe(res => {
			if (res) {
				if (!this.permissions.handleVat) {
					this.permissions.RedirectForbidden();
				}
			}
		});
		this.getVatDetails();
	}

	ngOnDestroy() {
		if (this.permSub) this.permSub.unsubscribe();
		if (this.dragSub) this.dragSub.unsubscribe();
	}

	getVatDetails() {
		this._http.get('Organisations/' + this.orgId + '/Vats')
			.map((res: any) => res.json())
			.subscribe((res: any) => {
				this.vats = res.Data;
			}), (err: any) => {
				console.log('error 77');
			};
	}
	updateVat() {
		this.editVatPopup = false;
		this.addVatPopUp = false;
		this._http.put('Organisations/' + this.orgId + '/Vats', this.vats)
			.map((res: any) => res.json())
			.subscribe((res: any) => {
				console.log('vat updated');
				if (this.isAddpopup === 'add') {
					this._notify.success(this.translate.instant('VATADDED'));
				}
				else if (this.isAddpopup === 'delete') {
					this._notify.success(this.translate.instant('VATDELETED'));
				}
				else if (this.isAddpopup === 'edit') {
					this._notify.success(this.translate.instant('VATUPDATED'));
				} else {
					this._notify.success(this.translate.instant('VATUPDATED'));
				}
				this.filter.getVat(true);
			})
	}
	toggleVatPopupDisplay() {
		this.addVatPopUp = false;
		this.editVatPopup = false;
	}
	toggleDeleteVatPopupDisplay() {
		this.deleteVatPopup = false;
	}
	//delete an item
	/*delete(vatIndex) {
		this.vats.splice(vatIndex, 1);*/
	delete() {
		this.isAddpopup = 'delete';
		this.vats.splice(this.vatDeleteIndex, 1);
		console.log(this.vats);
		this.updateVat();
		this.deleteVatPopup = false;
	}
	editVAT(editedVAT: any) {
		this.isAddpopup = 'edit';
		//to avoid duplication
		if (!isNaN(this.vatValue)) {
			if (this.vats.indexOf(editedVAT) != -1) {
				this._notify.error(this.translate.instant('VATALREADYEXIST'));
			}
			else {
				this.editVatPopup = !this.editVatPopup;
				this.vats[this.vatEditIndex] = editedVAT;
				this.updateVat();
			}
		}
		else {
			this._notify.error(this.translate.instant('VATVALUEREQUIRED'));
		}
	}
	addVAT(newVat: any) {
		this.isAddpopup = 'add';
		if (this.VatForm.dirty && this.VatForm.valid) {
			if (!(this.vatValue === 'undefined' || this.vatValue === '')) {
				if (!isNaN(this.vatValue)) {
					//to avoid duplication
					if (this.vats.indexOf(newVat) !== -1) {
						this._notify.error(this.translate.instant('VATALREADYEXIST'));
					}
					else {
						this.addVatPopUp = false;
						this.vats.push(parseInt(newVat));
						this.updateVat();
					}

				}
				else {
					this._notify.error(this.translate.instant('VATVALUEREQUIRED'));
				}
			}
			else {
				this._notify.error(this.translate.instant('VATVALUEREQUIRED'));
			}
		}
		else {
			this._notify.error(this.translate.instant('VATVALUEREQUIRED'));
		}
	}
	showEditVatPopup(vatIndex: any) {
		this.vatEditIndex = vatIndex;
		this.vatValue = this.vats[vatIndex];
		this.editVatPopup = true;
	}
	showDeleteVatPopup(vatIndex: any) {
		this.vatDeleteIndex = vatIndex;
		this.vatValue = this.vats[vatIndex];
		this.deleteVatPopup = true;
	}
	showAddVatPopup() {
		this.vatValue = 0;
		this.addVatPopUp = !this.addVatPopUp;
	}
	cancel(from: any) {
		this.editVatPopup = false;
		this.addVatPopUp = false;
		this.deleteVatPopup = false;
	}
	clickToClose(event: any, type: string) {
		var target = event.target || event.srcElement;

		if (type === 'add' && target.id === 'addContact') {
			this.editVatPopup = false;
			this.addVatPopUp = false;
			this.deleteVatPopup = false;
		}
	}
	onDrop(args: any) {
		this.isAddpopup = 'update';
		this.updateVat();
	}
}
