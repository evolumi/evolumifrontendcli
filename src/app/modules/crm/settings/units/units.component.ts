import { Component, OnInit } from '@angular/core';
import { LocalStorage } from '../../../../services/localstorage';
import { Api } from '../../../../services/api';
import { FormBuilder, Validators } from '@angular/forms';
import { NotificationService } from '../../../../services/notification-service';
import { TranslateService } from '@ngx-translate/core';

import { DragulaService } from '../../../../draggable/providers/dragula.provider';
import { FilterCollection } from '../../../../services/filterservice';
import { PermissionService } from '../../../../services/permissionService';
import { Subscription } from "rxjs/Subscription";

@Component({
	templateUrl: './units.html',
	providers: [LocalStorage],
	viewProviders: [DragulaService]
})
export class UnitsComponent implements OnInit {
	public test1: string;
	public orgId: string;
	public units: Array<any>;
	public addUnitPopUp: boolean = false;
	public editUnitPopup: boolean = false;
	public deleteUnitPopup: boolean = false;
	public items: any;
	public unitEditIndex: number;
	public unitDeleteIndex: number;
	public _http: any;
	public UnitForm: any;
	public unitValue: string;
	public isAddpopup: string = 'add';

	private dragSub: Subscription;
	private permSub: Subscription;

	public vats: any; // Just added for compilation errors. Someone should check what it does and if it acts correct.

	constructor(private permissions: PermissionService, _localstore: LocalStorage, private filter: FilterCollection, _http: Api, public form: FormBuilder, private _notify: NotificationService, private translate: TranslateService, public dragulaService: DragulaService) {
		this.orgId = _localstore.get('orgId');
		this.unitValue = '';
		this.UnitForm = form.group({
			unit: ['', Validators.required],
		});
		dragulaService.setOptions('nested-bag', {
			revertOnSpill: true
		});
		this.dragSub = dragulaService.drop.subscribe((value: any) => {
			console.log(`drop: ${value[0]}`);
			this.onDrop(value.slice(1));
		});
		this._http = _http;
	}
	ngOnInit() {
		this.permSub = this.permissions.IsLoaded().subscribe(res => {
			if (res) {
				if (!this.permissions.handleProducts) {
					this.permissions.RedirectForbidden();
				}
			}
		});
		this.getUnits();
	}

	ngOnDestroy(){
		if (this.dragSub) this.dragSub.unsubscribe();
		if (this.permSub) this.permSub.unsubscribe();
	}

	getUnits() {
		this._http.get('Organisations/' + this.orgId + '/Units')
			.map((res: any) => res.json())
			.subscribe((res: any) => {
				this.units = res.Data;
			}), (err: any) => {
				console.log('error 77');
			};
	}
	updateUnit() {
		this.editUnitPopup = false;
		this.addUnitPopUp = false;
		this._http.put('Organisations/' + this.orgId + '/Units', this.units)
			.map((res: any) => res.json())
			.subscribe((res: any) => {
				console.log('units updated');
				if (this.isAddpopup === 'add') {
					this._notify.success(this.translate.instant('UNITADDED'));
				}
				else if (this.isAddpopup === 'delete') {
					this._notify.success(this.translate.instant('UNITDELETED'));
				}
				else {
					this._notify.success(this.translate.instant('UNITUPDATED'));
				}
				this.filter.getUnit(true);
			})
	}
	toggleUnitPopupDisplay() {
		this.addUnitPopUp = false;
		this.editUnitPopup = false;
	}
	toggleDeleteVatPopupDisplay() {
		this.deleteUnitPopup = false;
	}
	//delete an item
	/*delete(vatIndex) {
		this.vats.splice(vatIndex, 1);*/
	delete() {
		this.isAddpopup = 'delete';
		this.units.splice(this.unitDeleteIndex, 1);
		console.log(this.units);
		this.updateUnit();
		this.deleteUnitPopup = false;
	}
	editUnit(editedUnit: any) {
		this.isAddpopup = 'edit';
		//to avoid duplication
		if (this.unitValue) {
			if (this.units.indexOf(editedUnit) != -1) {
				this._notify.error(this.translate.instant('UNITALREADYEXIST'));
			}
			else {
				this.editUnitPopup = !this.editUnitPopup;
				this.units[this.unitEditIndex] = editedUnit;
				this.updateUnit();
			}
		}
		else {
			this._notify.error(this.translate.instant('UNITVALUEREQUIRED'));
		}
	}
	addUnit(newUnit: any) {
		this.isAddpopup = 'add';
		if (this.UnitForm.dirty && this.UnitForm.valid) {
			if (!(this.unitValue === 'undefined' || this.unitValue === '')) {
				if (this.unitValue) {
					//to avoid duplication
					if (this.units.indexOf(newUnit) !== -1) {
						this._notify.error(this.translate.instant('UNITALREADYEXIST'));
					}
					else {
						this.addUnitPopUp = false;
						this.units.push(newUnit);
						this.updateUnit();
					}

				}
				else {
					this._notify.error(this.translate.instant('UNITVALUEREQUIRED'));
				}
			}
			else {
				this._notify.error(this.translate.instant('UNITVALUEREQUIRED'));
			}
		}
		else {
			this._notify.error(this.translate.instant('UNITVALUEREQUIRED'));
		}
	}
	showEditUnitPopup(unitIndex: any) {
		this.unitEditIndex = unitIndex;
		this.unitValue = this.units[unitIndex];
		this.editUnitPopup = true;
	}
	showDeleteUnitPopup(unitIndex: any) {
		this.unitDeleteIndex = unitIndex;
		this.unitValue = this.units[unitIndex];
		this.deleteUnitPopup = true;
	}
	showAddUnitPopup() {
		this.addUnitPopUp = !this.addUnitPopUp;
	}
	cancel(from: any) {
		this.editUnitPopup = false;
		this.addUnitPopUp = false;
		this.deleteUnitPopup = false;
	}
	clickToClose(event: any, type: any) {
		var target = event.target || event.srcElement;

		if (type === 'add' && target.id === 'addContact') {
			this.editUnitPopup = false;
			this.addUnitPopUp = false;
			this.deleteUnitPopup = false;
		}
	}
	onDrop(args: any) {
		this.isAddpopup = 'update';
		console.log('onDrop');
		this.updateUnit();
	}
}
