import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { SalesCommissionRule } from '../../../../models/sales-commission.models';
import { ChosenOption } from '../../../shared/components/chosen/chosen';
import { ChosenOptionModel } from '../../../shared/components/chosen/chosen.models';
import { SalesCommissionService } from '../../../../services/sales-commission.service';
import { ActionsCollection } from '../../../../services/actionservice';
import { GoalsService } from '../../../../services/goals.service';
import { Subscription } from 'rxjs/Subscription';
import { OrgService } from '../../../../services/organisation.service';
import { Configuration } from '../../../../services/configuration';
import { OrganisationModel } from "../../../../models/organisation.models";

@Component({
    selector: 'addedit-commrule',
    templateUrl: 'addedit-rule.component.html',
    styleUrls: ['./sales-commission.css']
})
export class AddEditSalesCommissionRuleComponent implements OnInit, OnDestroy {

    @Input() currency: string;
    @Output() ruleAdded = new EventEmitter<SalesCommissionRule>();

    //flags
    public isEdit: boolean;

    // view objects
    public commRule: SalesCommissionRule;
    public ruleTypeList: Array<ChosenOption> = [];
    public actionTypeList: Array<ChosenOption> = [];
    public actionStatusList: Array<ChosenOption> = [];
    public dealStatusList: Array<ChosenOption> = [];
    public labelList: Array<ChosenOption> = [];
    public salesProcessList: Array<ChosenOption> = [];
    public currencies: Array<ChosenOption> = [];

    // placeholders
    private salesProcessId: string; // for race conditions

    // privates
    private currentOrg: OrganisationModel;

    //subscriptions
    private orgSub: Subscription;
    private ruleSub: Subscription;

    constructor(private salesCommSvc: SalesCommissionService,
        private goalSvc: GoalsService,
        private actionSvc: ActionsCollection,
        private orgSvc: OrgService,
        private config: Configuration) { }

    ngOnInit() {
        this.ruleTypeList = this.goalSvc.getGoalTypes();
        this.actionTypeList = this.actionSvc.getActionTypes();
        this.orgSub = this.orgSvc.currentOrganisationObservable()
            .subscribe(org => {
                if (org) {
                    this.currentOrg = org;
                    if (org.Settings.SaleProcesses) {
                        this.salesProcessList = org.Settings.SaleProcesses
                            .map((x: any) => new ChosenOptionModel(x.Id, x.Name));
                        if (this.salesProcessList.length === 1) {
                            if (this.commRule) {
                                this.commRule.SalesProcessId = this.salesProcessList[0].value.toString();
                            } else {
                                this.salesProcessId = this.salesProcessList[0].value.toString();
                            }
                            this.dealStatusList = org.Settings.SaleProcesses[0].DealStatuses
                                .map((x: any) => new ChosenOptionModel(x.Id, x.Name));
                        }
                    }
                    if (org.Labels) {
                        this.labelList = org.Labels.map(x => new ChosenOptionModel(x, x));
                    }
                }
            });
        this.ruleSub = this.salesCommSvc.currentRuleObs()
            .subscribe(rule => {
                if (rule) {
                    this.commRule = JSON.parse(JSON.stringify(rule));
                    if (this.salesProcessId) {
                        this.commRule.SalesProcessId = this.salesProcessId;
                        this.salesProcessId = '';
                    }
                } else {
                    this.commRule = new SalesCommissionRule({});
                    if (this.salesProcessId) {
                        this.commRule.SalesProcessId = this.salesProcessId;
                        this.salesProcessId = '';
                    }
                }
            });
        this.currencies.push(...this.config.currencies.map(x => new ChosenOptionModel(x, x)));
    }

    ngOnDestroy() {
        if (this.orgSub) this.orgSub.unsubscribe();
        if (this.ruleSub) this.ruleSub.unsubscribe();
    }

    addRule() {
        this.ruleAdded.emit(this.commRule);
        this.back();
    }

    onTypeChange(event: string) {
        if (this.commRule.SalesGoalType && this.commRule.SalesGoalType.length > 0) {
            this.commRule = new SalesCommissionRule({ SalesGoalType: event });
            if (this.commRule.SalesGoalType === 'DealsValueOf') {
                this.commRule.Currency = this.currentOrg ? this.currentOrg.CurrencyName : 'USD';
            }
        }
    }

    onActionTypeChanged(actionTypes: Array<string>) {
        this.commRule.ActionTypes = actionTypes;
        this.actionStatusList = [];
        this.actionSvc.getActionTypesAndStatuses(this.commRule.SalesGoalType === 'ClosedActions').reduce((res, curr) => {
            if (this.commRule.ActionTypes.indexOf(curr.value) > -1) {
                this.actionStatusList.push(...curr.statuses);
            }
            return res;
        }, []);
        this.actionStatusList = this.actionStatusList.filter((x, i, arr) => arr.indexOf(x) === i);
        // to make the chosen bind properly
        this.commRule.ActionStatuses = this.actionStatusList.filter(x => this.commRule.ActionStatuses.indexOf(x.value.toString()) > -1).map(x => x.value.toString());
    }

    onSaleProcessChange(value: any) {
        const saleProcess = this.currentOrg.Settings.SaleProcesses.find((x: any) => x.Id === value)
        this.dealStatusList = saleProcess.DealStatuses
            .map((x: any) => new ChosenOptionModel(x.Id, x.Name));
    }

    clickToClose(event: any) {
        let target = event.target || event.srcElement;
        if (target.id === 'addSalesCommissionRule') {
            this.back();
        }
    }

    back() {
        this.salesCommSvc.closeAddRule();
    }
}