import { Component, OnInit } from '@angular/core';
import { SalesCommissionService } from '../../../../services/sales-commission.service';
import { SalesCommission } from '../../../../models/sales-commission.models';
import { Subscription } from 'rxjs/Subscription';

@Component({
    selector: 'sales-commission',
    templateUrl: 'sales-commission.component.html'
})
export class SalesCommissionComponent implements OnInit {

    public salesCommList: Array<SalesCommission> = [];

    private salesCommSub: Subscription;

    constructor(public salesCommSvc: SalesCommissionService) { }

    ngOnInit() {
        this.salesCommSvc.getManySalesCommission();
        this.salesCommSub = this.salesCommSvc.salesCommissionListObs()
            .subscribe(salesComm => {
                if (salesComm) {
                    this.salesCommList = salesComm;
                }
            });
    }

    ngOnDestroy(){
        if (this.salesCommSub) this.salesCommSub.unsubscribe();
    }

    openCreate() {
        this.salesCommSvc.openAddEdit();
    }

    openEdit(salesComm: SalesCommission) {
        this.salesCommSvc.openAddEdit(salesComm);
    }

    delete(id: string) {
        this.salesCommSvc.deleteSalesCommission(id);
    }
}