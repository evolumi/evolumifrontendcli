import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SalesCommissionRule } from '../../../../models/sales-commission.models';
import { TranslateService } from '@ngx-translate/core';
import { OrgService } from '../../../../services/organisation.service';
import { Subscription } from 'rxjs/Subscription';
import { OrganisationModel } from "../../../../models/organisation.models";

@Component({
    selector: 'rules-display',
    template: `
        <div class="rules-display-group">
            <p>{{description}}</p>
            <i class="fa fa-trash rules-display-icon clickable" (click)="onRemove()"></i>
        </div>
    `,
    styleUrls: ['./sales-commission.css']
})
export class RulesDisplayComponent implements OnInit {

    @Input() rule: SalesCommissionRule;
    @Input() currency: string;
    @Output() removeRule = new EventEmitter<number>();

    public description: string;
    private amount: string;
    private currentOrg: OrganisationModel;

    private orgSub: Subscription;

    constructor(private translate: TranslateService,
        private orgSvc: OrgService) { }

    ngOnInit() {
        this.orgSub = this.orgSvc.currentOrganisationObservable()
            .subscribe(org => {
                if (org) this.currentOrg = org;
            });

        switch (this.rule.SalesGoalType) {
            case 'ClosedActions':
                this.getClosedActionData();
                break;
            case 'CreatedActions':
                this.getCreatedActionData();
                break;
            case 'DealsValueOf':
                this.getDealsValueOfData();
                break;
            case 'CreatedDeals':
                this.getCreatedDealsData();
                break;
            case 'NumberOfDeals':
                this.getNumberOfDealsData();
                break;
            default:
                break;
        }
    }

    ngOnDestroy() {
        if (this.orgSub) this.orgSub.unsubscribe();
    }

    onRemove() {
        this.removeRule.emit(0);
    }

    getClosedActionData() {
        let labels = '';
        let actionType = this.rule.ActionTypes.map(x => this.translate.instant(x.toUpperCase())).join(', ');
        if (this.rule.Labels.length > 0) {
            for (let i = 0; i < this.rule.Labels.length; i++) {
                labels += `${this.rule.Labels[i]}, `;
            }
            labels = labels.substring(0, labels.length - 2);
        }
        if (labels && actionType) {
            this.description = this.translate.instant('SALESCOMMRULECLOSEDACTIONFULL', { labels, actionType });
        } else if (labels) {
            this.description = this.translate.instant('SALESCOMMRULECLOSEDACTIONLABEL', { labels })
        } else if (actionType) {
            this.description = this.translate.instant('SALESCOMMRULECLOSEDACTIONTYPE', { actionType })
        }
        this.amount = this.translate.instant('SALESCOMMRULEAMOUNTPERCLOSEDACTION', { points: this.rule.Value, quantity: this.rule.Quantity, currency: this.currency })
    }

    getCreatedActionData() {
        let labels = '';
        let actionType = this.rule.ActionTypes.map(x => this.translate.instant(x.toUpperCase())).join(', ');
        if (this.rule.Labels.length > 0) {
            for (let i = 0; i < this.rule.Labels.length; i++) {
                labels += `${this.rule.Labels[i]}, `;
            }
            labels = labels.substring(0, labels.length - 2);
        }
        if (labels && actionType) {
            this.description = this.translate.instant('SALESCOMMRULECREATEDACTIONFULL', { labels, actionType });
        } else if (labels) {
            this.description = this.translate.instant('SALESCOMMRULECREATEDACTIONLABEL', { labels })
        } else if (actionType) {
            this.description = this.translate.instant('SALESCOMMRULECREATEDACTIONTYPE', { actionType })
        }
        this.amount = this.translate.instant('SALESCOMMRULEAMOUNTPERCREATEDACTION', { points: this.rule.Value, quantity: this.rule.Quantity, currency: this.currency })
    }

    getDealsValueOfData() {
        const saleProcess = this.currentOrg.Settings.SaleProcesses.find((x: any) => x.Id === this.rule.SalesProcessId);
        const dealStatus = saleProcess.DealStatuses.find((x: any) => x.Id === this.rule.DealStatusId);

        this.description = this.translate.instant('SALESCOMMRULEVALUEOFDEALSFULL', { saleProcess, dealStatus });
        this.amount = this.translate.instant('SALESCOMMRULEAMOUNTPERVALUEOFDEALS', { points: this.rule.Value, quantity: this.rule.Quantity, currency: this.currency })
    }

    getCreatedDealsData() {
        this.description = this.translate.instant('SALESCOMMRULECREATEDDEALSFULL');
        this.amount = this.translate.instant('SALESCOMMRULEAMOUNTPERCREATEDDEALS', { points: this.rule.Value, quantity: this.rule.Quantity, currency: this.currency })
    }

    getNumberOfDealsData() {
        const saleProcess = this.currentOrg.Settings.SaleProcesses.find((x: any) => x.Id === this.rule.SalesProcessId);
        const dealStatus = saleProcess.DealStatuses.find((x: any) => x.Id === this.rule.DealStatusId);
        this.description = this.translate.instant('SALESCOMMRULENUMBEROFDEALSFULL', { saleProcess, dealStatus });
        this.amount = this.translate.instant('SALESCOMMRULEAMOUNTPERNUMBEROFDEALS', { points: this.rule.Value, quantity: this.rule.Quantity, currency: this.currency })
    }

}