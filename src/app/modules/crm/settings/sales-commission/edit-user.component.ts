import { Component, Input, Output, EventEmitter } from '@angular/core';
import { SalesCommissionService } from '../../../../services/sales-commission.service';
import { SalesCommissionData, SalesCommissionUserModel, SalesCommissionUserEditModel } from '../../../../models/sales-commission.models';
import { ChosenOptionModel } from '../../../shared/components/chosen/chosen.models';
import { Subscription } from 'rxjs/Subscription';
import { UsersService } from '../../../../services/users.service';
import { DateService } from '../../../../services/date.service';

@Component({
    selector: 'edit-user',
    templateUrl: './edit-user.component.html',
    styleUrls: ['./sales-commission.css']
})

export class EditUserComponent {

    @Input() id: string;
    @Input('edit') isEdit: boolean;
    @Output() add = new EventEmitter<Array<SalesCommissionData>>();
    @Output() remove = new EventEmitter<string>();

    public editModel: SalesCommissionUserEditModel = new SalesCommissionUserEditModel({});
    public userModel: SalesCommissionUserModel;
    public originalUserModel: SalesCommissionUserModel;

    public fromDate: number;
    public endDate: number;
    public isTemporary: boolean;
    public originalUserList: Array<ChosenOptionModel> = [];
    public addedUsers: Array<ChosenOptionModel> = [];
    public userList: Array<ChosenOptionModel> = [];

    private userModelLoaded: boolean;
    private userListLoaded: boolean;

    private userSub: Subscription;

    constructor(private salesCommSvc: SalesCommissionService,
        private usersSvc: UsersService,
        private dateSvc: DateService) { }

    ngOnInit() {
        if (this.isEdit) {
            this.editModel = new SalesCommissionUserEditModel({});
        }
        if (this.id) {
            this.salesCommSvc.getUsersBySalesCommission(this.id)
                .subscribe(res => {
                    if (res.IsSuccess) {
                        this.userModel = res.Data;
                        this.originalUserModel = JSON.parse(JSON.stringify(res.Data));
                        this.userModelLoaded = true;
                        if (this.userListLoaded) {
                            this.loadUserList();
                            this.userListLoaded = false;
                        }
                    }
                });
        } else {
            this.userModel = new SalesCommissionUserModel({});
            this.userModelLoaded = true;
            if (this.userListLoaded) {
                this.loadUserList();
                this.userListLoaded = false;
            }
        }
        this.userSub = this.usersSvc.activeUserListObervable()
            .subscribe(users => {
                if (users) {
                    this.originalUserList = JSON.parse(JSON.stringify(users));;
                    this.userListLoaded = true;
                    if (this.userModelLoaded) {
                        this.loadUserList();
                        this.userModelLoaded = false;
                    }
                }
            });
    }

    ngOnDestroy(){
        if (this.userSub) this.userSub.unsubscribe();
    }

    onSave() {
        this.setDateHours();
        let data = this.addedUsers.map(x => new SalesCommissionData({ UserId: x.value, FromDate: this.editModel.FromDate, EndDate: this.editModel.EndDate, IsTemporary: this.editModel.IsTemporary, RelatedSalesCommissionId: this.id || null }))
        this.add.emit(data);
        this.updateLists();
        this.loadUserList();
        this.addedUsers = [];
        this.editModel = new SalesCommissionUserEditModel({});
    }

    private updateLists() {
        const dateNow = new Date().getTime();
        if (this.editModel.IsTemporary) {
            if (this.editModel.FromDate < dateNow) {
                this.userModel.TemporaryUsers.push(...this.addedUsers.map(x => new SalesCommissionData({ UserId: x.value, FromDate: this.editModel.FromDate, EndDate: this.editModel.EndDate, IsTemporary: this.editModel.IsTemporary, RelatedSalesCommissionId: this.id || null })));
                this.userModel.TemporaryUsers = this.userModel.TemporaryUsers.slice();
            } else {
                this.userModel.FutureTemporaryUsers.push(...this.addedUsers.map(x => new SalesCommissionData({ UserId: x.value, FromDate: this.editModel.FromDate, EndDate: this.editModel.EndDate, IsTemporary: this.editModel.IsTemporary, RelatedSalesCommissionId: this.id || null })));
                this.userModel.FutureTemporaryUsers = this.userModel.FutureTemporaryUsers.slice();
            }
        } else {
            if (this.editModel.FromDate < dateNow) {
                this.userModel.BaseUsers.push(...this.addedUsers.map(x => new SalesCommissionData({ UserId: x.value, FromDate: this.editModel.FromDate, IsTemporary: this.editModel.IsTemporary, RelatedSalesCommissionId: this.id || null })));
                this.userModel.BaseUsers = this.userModel.BaseUsers.slice();
            } else {
                this.userModel.FutureBaseUsers.push(...this.addedUsers.map(x => new SalesCommissionData({ UserId: x.value, FromDate: this.editModel.FromDate, IsTemporary: this.editModel.IsTemporary, RelatedSalesCommissionId: this.id || null })));
                this.userModel.FutureBaseUsers = this.userModel.FutureBaseUsers.slice();
            }
        }
    }

    private loadUserList() {
        let users = this.userModel.BaseUsers.map(x => x.UserId)
            .concat(...this.userModel.TemporaryUsers.map(x => x.UserId))
            .concat(...this.userModel.FutureBaseUsers.map(x => x.UserId))
            .concat(...this.userModel.FutureTemporaryUsers.map(x => x.UserId));

        this.userList = this.originalUserList.filter(x => users.indexOf(x.value.toString()) === -1);
    }

    onAddUser(user: ChosenOptionModel, index: number) {
        this.userList.splice(index, 1);
        this.addedUsers.push(user);
    }

    onAddAllUsers() {
        this.addedUsers.push(...this.userList);
        this.userList = [];
    }

    onRemoveUser(user: ChosenOptionModel, index: number) {
        this.addedUsers.splice(index, 1);
        this.userList.push(user);
    }

    onRemoveAllUsers() {
        this.addedUsers = [];
        this.userList = Object.assign([], this.originalUserList);
    }

    onDeleteUser(user: SalesCommissionData, index: number, type: string) {
        if (this.isEdit) {
            if (user.Id) {
                this.remove.emit(user.UserId);
            }
            this.userModel[type].splice(index, 1);
            this.loadUserList();
        }
    }

    cancelChanges() {
        this.userModel = JSON.parse(JSON.stringify(this.originalUserModel));
        this.loadUserList();
    }

    private setDateHours() {
        this.editModel.FromDate = this.dateSvc.changeTimeOfDate(this.editModel.FromDate, false);
        if (this.editModel.EndDate) {
            this.editModel.EndDate = this.dateSvc.changeTimeOfDate(this.editModel.EndDate, true);
        }
    }
}