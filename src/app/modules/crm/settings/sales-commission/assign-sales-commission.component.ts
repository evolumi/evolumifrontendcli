import { Component, OnInit, Input, OnDestroy, Output, EventEmitter } from '@angular/core';
import { SalesCommissionService } from '../../../../services/sales-commission.service';
import { SalesCommissionData, SalesCommission } from '../../../../models/sales-commission.models';
import { Subscription } from 'rxjs/Subscription';
import { ChosenOptionModel } from '../../../shared/components/chosen/chosen.models';
import { DateService } from '../../../../services/date.service';

@Component({
    selector: 'assign-sales-comm',
    templateUrl: './assign-sales-comm.component.html',
    styleUrls: ['./sales-commission.css']
})

export class AssignSalesCommissionComponent implements OnInit, OnDestroy {

    @Input() users: Array<string>;
    @Output() close = new EventEmitter<string>();

    public salesCommId: string;
    public fromDate: number = new Date().getTime();
    public endDate: number;
    public isTemporary: boolean;
    public salesCommChosen: Array<ChosenOptionModel> = [];
    private salesCommList: Array<SalesCommission> = [];

    private salesCommListSub: Subscription;

    constructor(private salesCommSvc: SalesCommissionService,
        private dateSvc: DateService) { }

    ngOnInit() {
        this.salesCommListSub = this.salesCommSvc.salesCommissionListObs()
            .subscribe(list => {
                console.log(list);
                if (list) {
                    this.salesCommChosen = list.map(x => new ChosenOptionModel(x.Id, x.Name));
                    this.salesCommList = list;
                } else {
                    this.salesCommSvc.getManySalesCommission();
                }
            });
    }

    ngOnDestroy() {
        if (this.salesCommListSub) this.salesCommListSub.unsubscribe();
    }

    addData() {
        this.setDateHours();
        let salesComm = this.salesCommList.find(x => x.Id === this.salesCommId);
        salesComm.Data = this.users.map(x => new SalesCommissionData({
            RelatedSalesCommissionId: this.salesCommId,
            UserId: x,
            FromDate: this.fromDate,
            EndDate: this.endDate,
            IsTemporary: this.isTemporary,
        }));
        console.log('SALESCOMM', salesComm);
        this.salesCommSvc.updateSalesCommission(salesComm, this.back.bind(this));
    }

    clickToClose(event: any) {
        let target = event.target || event.srcElement;
        if (target.id === 'assignSalesComm') {
            this.back();
        }
    }

    back() {
        this.close.emit('');
    }

    private setDateHours() {
        this.fromDate = this.dateSvc.getFirstDateOfPeriod('day', 'x', this.fromDate);
        if (this.endDate) this.endDate = this.dateSvc.getLastDateOfPeriod('day', 'x', this.endDate);
    }
}