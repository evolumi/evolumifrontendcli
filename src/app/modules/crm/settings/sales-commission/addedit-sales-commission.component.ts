import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ChosenOptionModel } from '../../../shared/components/chosen/chosen.models';
import { SalesCommissionService } from '../../../../services/sales-commission.service';
import { SalesCommission, SalesCommissionData, SalesCommissionRule } from '../../../../models/sales-commission.models';
import { Subscription } from 'rxjs/Subscription';
import { OrgService } from '../../../../services/organisation.service';
import { EditUserComponent } from './edit-user.component';

@Component({
    selector: 'addedit-sales-commission',
    templateUrl: 'addedit-sales-commission.html'
})
export class AddEditSalesCommissionComponent implements OnInit, OnDestroy {

    @ViewChild(EditUserComponent) editUserComp: EditUserComponent;

    public salesCommission: SalesCommission;
    public originalSalesCommission: SalesCommission;

    public roleList: Array<ChosenOptionModel> = [];

    public isDisplay: boolean;
    public isEditMode: boolean;
    public showAddUsers: boolean;
    public createNew: boolean;

    private salesCommSub: Subscription;
    private orgSub: Subscription;

    constructor(public salesCommSvc: SalesCommissionService,
        private orgSvc: OrgService) { }

    ngOnInit() {
        this.salesCommSub = this.salesCommSvc.currentSalesCommissionObs()
            .subscribe(salesComm => {
                if (salesComm) {
                    console.log(salesComm);
                    this.salesCommission = JSON.parse(JSON.stringify(salesComm));
                    this.originalSalesCommission = JSON.parse(JSON.stringify(salesComm));
                    if (salesComm.Id) {
                        this.isDisplay = true;
                    }
                }
            });
        this.orgSub = this.orgSvc.currentOrganisationObservable()
            .subscribe(org => {
                if (org) {
                    this.roleList = [];
                    this.roleList.push(...org.Roles.map(x => new ChosenOptionModel(x.Id, x.Name)));
                }
            });
    }

    ngOnDestroy() {
        if (this.salesCommSub) this.salesCommSub.unsubscribe();
        if (this.orgSub) this.orgSub.unsubscribe();
    }

    create() {
        // this.salesCommission.Users = this.addedUsers.map(x => x.value.toString());
        this.salesCommSvc.addSalesCommission(this.salesCommission, this.back.bind(this));
    }

    update() {
        if (this.needToCreateNew()) {
            this.originalSalesCommission.IsActive = false;
            this.salesCommSvc.updateSalesCommission(this.originalSalesCommission, this.back.bind(this));
            this.salesCommSvc.addSalesCommission(this.salesCommission, this.back.bind(this));
        } else {
            this.salesCommSvc.updateSalesCommission(this.salesCommission, this.back.bind(this));
        }
    }

    onOpenAddRule() {
        this.salesCommSvc.openAddRule();
    }

    onAddRule(rule: SalesCommissionRule) {
        this.salesCommission.Rules.push(rule);
    }

    onRemoveRule(index: number) {
        this.salesCommission.Rules = this.salesCommission.Rules.splice(index, 1);
    }

    onEdit() {
        this.isDisplay = false
        this.isEditMode = true;
    }

    onAddUsers(data: Array<SalesCommissionData>) {
        console.log('DATA', data);
        this.initializeArrays();
        for (let i = 0; i < data.length; i++) {
            let index = this.salesCommission.RemoveData.findIndex(x => x === data[i].UserId);
            if (index > -1) {
                this.salesCommission.RemoveData.splice(index, 1);
            }
        }
        this.salesCommission.Data.push(...data);
    }

    onRemoveUser(userId: string) {
        console.log('REMOVEUSER');
        this.initializeArrays();
        let index = this.salesCommission.Data.findIndex(x => x.UserId == userId)
        if (index > -1) {
            this.salesCommission.Data.splice(index, 1);
        }
        this.salesCommission.RemoveData.push(userId);
    }

    //Need to check if we must create a new one
    private needToCreateNew(): boolean {
        if (this.salesCommission.Rules.some(x => !x.Id) || this.salesCommission.Rules.length !== this.originalSalesCommission.Rules.length) {
            return true;
        }
        return false;
    }

    private initializeArrays() {
        if (!this.salesCommission.Data) {
            this.salesCommission.Data = [];
        }
        if (!this.salesCommission.RemoveData) {
            this.salesCommission.RemoveData = [];
        }
    }

    // private loadLists() {
    //     let arrays = this.originalUserList.reduce((res: any, curr: any) => {
    //         if (!res['added']) {
    //             res['added'] = [];
    //             res['notadded'] = [];
    //         }
    //         if (this.salesCommission.Users.find(x => x === curr.value)) {
    //             res['added'].push(curr);
    //         } else {
    //             res['notadded'].push(curr);
    //         }
    //         return res;
    //     }, []);

    //     this.addedUsers = arrays['added'];
    //     this.userList = arrays['notadded'];
    // }

    cancelChanges() {
        this.salesCommission = JSON.parse(JSON.stringify(this.originalSalesCommission));
        this.editUserComp.cancelChanges();
        this.isDisplay = true;
        this.isEditMode = false;
    }

    clickToClose(event: any) {
        let target = event.target || event.srcElement;
        if (target.id === 'addSalesCommission') {
            this.back();
        }
    }

    back() {
        this.salesCommSvc.closeAddEdit()
    }
}