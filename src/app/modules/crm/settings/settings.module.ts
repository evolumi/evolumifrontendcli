import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { SettingsSharedModule } from './shared/settings-shared.module';
import { DocumentsModule } from "./documents/documents.module";

// Components
import { SettingsComponent } from './settings.component';
import { CustomizeAccountsComponent } from './accounts/accounts.component';
import { LeftPanel } from './left-panel';
import { CustomizeDealsComponent } from './deals/deals.component';
import { DealStatusComponent } from './deals/dealstatus/dealstatus.component';
import { SalesProcessComponent } from './deals/salesprocess/salesprocess.component';
import { EditSalesProcessComponent } from './deals/salesprocess/editsaleprocess.component';
import { GeneralSettingsComponent } from './general/general.component';
import { PaymentComponent } from './payment/payment.component';
import { PhoneList } from './phone/phonelist.component';
import { PhoneNumbersComponent } from './phone/phonenumbers.component';
import { PlanComponent } from './plan/plan.component';
import { RolesComponent } from './roles/roles.component';
import { SMTPComponent } from './smtp/smtp.component';
import { UsersComponent } from './users/users.component';
import { VatComponent } from './vat/vat.component';
import { SMTPSettingsFormComponent } from './smtp/smtpsettingsform.component';
import { ProductModule } from "./Product/products.module";
import { GoalsComponent } from './goals/goals.component';
import { UnitsComponent } from './units/units.component';
import { ContractSettingsComponent } from './contracts/contracts.component';
import { AddEditSalesCommissionComponent } from './sales-commission/addedit-sales-commission.component';
import { SalesCommissionComponent } from './sales-commission/sales-commission.component';
import { AddEditSalesCommissionRuleComponent } from './sales-commission/addedit-rule.component';
import { RulesDisplayComponent } from './sales-commission/rules-display.component';
import { CalendarSettingsComponent } from './calendar/calendar-settings.component';
import { EditUserComponent } from './sales-commission/edit-user.component';
import { AssignSalesCommissionComponent } from './sales-commission/assign-sales-commission.component';
import { SyncRulesComponent } from "./calendar/syncrules.component";
import { VisitorComponent } from "./visitors/visitors.component";

// Services/
import { RolesService } from './roles/roles.service';
import { EditUserRoleComponent } from "./users/add/edit-user-role.component";

@NgModule({
    imports: [
        SharedModule,
        SettingsSharedModule,
        ProductModule,
        DocumentsModule
    ],
    declarations: [
        SettingsComponent,
        LeftPanel,
        CustomizeAccountsComponent,
        CustomizeDealsComponent,
        DealStatusComponent,
        SalesProcessComponent,
        EditSalesProcessComponent,
        GeneralSettingsComponent,
        PaymentComponent,
        PhoneList,
        PhoneNumbersComponent,
        PlanComponent,
        RolesComponent,
        SMTPComponent,
        SMTPSettingsFormComponent,
        UsersComponent,
        VatComponent,
        GoalsComponent,
        UnitsComponent,
        ContractSettingsComponent,
        AddEditSalesCommissionComponent,
        SalesCommissionComponent,
        AddEditSalesCommissionRuleComponent,
        RulesDisplayComponent,
        CalendarSettingsComponent,
        EditUserComponent,
        AssignSalesCommissionComponent,
        SyncRulesComponent,
        EditUserRoleComponent,
        VisitorComponent
    ],
    providers: [
        RolesService,
    ],
    exports: [
        SettingsSharedModule
    ]
})
export class SettingsModule { }