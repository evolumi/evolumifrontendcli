import { Component } from '@angular/core';
import { CalendarService, AuthCodeObject } from '../../../../services/calendar.service';
import { ActionsCollection, ActionType } from '../../../../services/actionservice';
import { Subscription } from 'rxjs/Subscription';
import { ChosenOptionModel } from "../../../shared/components/chosen/chosen.models";
import { NotificationService } from '../../../../services/notification-service';
import { CalendarSettings } from '../../../../models/calendar.models';
import { OrgService } from '../../../../services/organisation.service';
import { TranslateService } from "@ngx-translate/core";
import { OrganisationModel } from "../../../../models/organisation.models";

@Component({
    selector: 'calendar-settings',
    templateUrl: './calendar-settings.html',
    styleUrls: ['./calendar-settings.css']
})

export class CalendarSettingsComponent {

    private actionTypes: ActionType[];
    private popupWindow: Window;
    public settings: CalendarSettings;
    private providers: ChosenOptionModel[];
    private showAuthWindow: boolean;
    public showAuthCompletePopup: boolean;
    public waitingForAuthResult: boolean;

    public microsoftCalendar: string;
    public googleCalendar: string;

    private orgSub: Subscription;
    private currentOrg: OrganisationModel

    constructor(
        private actionSvc: ActionsCollection,
        public calendarSvc: CalendarService,
        private notificationSvc: NotificationService,
        private orgSvc: OrgService,
        private translate: TranslateService
    ) { }

    ngOnInit() {

        this.orgSub = this.orgSvc.currentOrganisationObservable().subscribe(data => this.currentOrg = data);
        this.actionTypes = this.actionSvc.getActionTypes();
        this.calendarSvc.getSettings((success, result) => {
            if (success) {
                this.settings = new CalendarSettings(result);
                if (result.ProvidersToUse.indexOf("Microsoft") != -1) {
                    this.microsoftCalendar = result.Office365Email;
                }
                if (result.ProvidersToUse.indexOf("Google") != -1) {
                    this.googleCalendar = result.GoogleEmail;
                }
            }
        });
        this.providers = [
            new ChosenOptionModel("Microsoft", "Microsoft"),
            new ChosenOptionModel("Google", "Google")
        ];
    }

    ngOnDestroy() {
        if (this.orgSub) this.orgSub.unsubscribe();
    }

    showAuthPopup(provider: string) {
        let userid = localStorage.getItem("userId");
        let orgId = localStorage.getItem("orgId");

        let userObj = JSON.stringify({ UserId: userid, OrgId: orgId });
        let url = "";
        switch (provider) {
            case "Microsoft": {
                url = "https://login.microsoftonline.com/common/oauth2/v2.0/authorize?client_id=73ede299-a12f-457f-b050-46053d561329&scope=https://outlook.office.com/calendars.readwrite%20openid%20offline_access&response_type=code&redirect_uri=https://app.evolumi.se/AuthMicrosoft&state=" + userObj;
                break;
            }
            case "Google": {
                url = "https://accounts.google.com/o/oauth2/v2/auth?scope=https://www.googleapis.com/auth/calendar%20https://www.googleapis.com/auth/userinfo.email&access_type=offline&prompt=consent&include_granted_scopes=true&redirect_uri=https://app.evolumi.se/AuthGoogle&response_type=code&client_id=162216705453-oqja1577b5c9u91tpf9n7iagrfv0fmbj.apps.googleusercontent.com&state=" + userObj;
                break;
            }
        }
        this.popupWindow = window.open(url, "authWindow", "width=350, height=450", true);
        this.waitForAuthResult(provider);
        this.showAuthWindow = false;
        this.waitingForAuthResult = true;
    }

    private tries = 0;
    waitForAuthResult(provider: string) {
        this.calendarSvc.waitForAuthResult(provider, (done: boolean, status: boolean) => {
            if (done) {
                this.popupWindow.close();
                console.log("auth done. Tokentest: ", status);
                if (status) {
                    this.notificationSvc.success(this.translate.instant("AUTHCOMPLETED"));
                    this.calendarSvc.getSettings((success, result) => {
                        this.waitingForAuthResult = false;
                        this.settings = result;
                        this.microsoftCalendar = (result.ProvidersToUse.indexOf("Microsoft") != -1) ? result.Office365Email : "";
                        this.googleCalendar = (result.ProvidersToUse.indexOf("Google") != -1) ? result.GoogleEmail : "";
                    });
                }
                this.showAuthCompletePopup = true;
            }
            else {
                if (this.tries < 120) {
                    this.tries++;
                    console.log("auth not done, checking again");
                    setTimeout(() => {
                        this.waitForAuthResult(provider);
                    }, 2000);
                }
                else {
                    this.tries = 0;
                    console.log("Auth failed or timed out");
                    this.waitingForAuthResult = false;
                }
            }
        });
    }

    insertCode(provider: string) {
        let userId = localStorage.getItem("userId");
        let code = prompt("koden");
        let orgId = JSON.parse(decodeURIComponent(prompt("orgid"))).OrgId;
        let model: AuthCodeObject = {
            AuthCode: code,
            UserId: userId,
            OrganisationId: orgId
        }
        console.log(model);
        this.calendarSvc.authorize(model, provider);
    }

    saveClicked() {
        this.settings.SyncRules = this.settings.SyncRules.filter((x) => x.ActionTypes.length != 0);
        this.calendarSvc.saveSettings(this.settings, (success: boolean) => {
            success ? this.notificationSvc.success(this.translate.instant("SAVECALENDARSETTINGSSUCCESS")) : this.notificationSvc.error(this.translate.instant("SAVECALENDARSETTINGSFAILED"));
        });
        this.showAuthCompletePopup = false;
    }
    closeAuthCompletePopup(ev: any, force?: boolean) {
        if (force || ev.target.id == "authCompletePopup") {
            this.showAuthCompletePopup = false;
        }
    }
    closeAuthSelect(ev: any, force?: boolean) {
        if (force || ev.target.id == "authSelectWindow") {
            this.showAuthWindow = false;
        }
    }
}