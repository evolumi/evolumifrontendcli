import { Component, Input, Output, EventEmitter } from '@angular/core';
import { CalendarSettings } from "../../../../models/calendar.models";
import { ActionType } from "../../../../services/actionservice";
import { OrganisationModel } from "../../../../models/organisation.models";

@Component({
    selector: 'syncrules',
    templateUrl: './syncrules.html',
    styleUrls: ['./syncrules.css']
})

export class SyncRulesComponent {
    @Input() settings: CalendarSettings;
    @Input() actionTypes: ActionType[];
    @Input() currentOrg: OrganisationModel
    @Input() popup: boolean;
    @Output() save = new EventEmitter();

    public editmode: boolean;

    constructor() { }

    addRule() {
        this.settings.SyncRules.push({ ActionTypes: [], Tags: [] });
    }

    removeRule(index: number) {
        this.settings.SyncRules.splice(index, 1);
    }

    saveClicked() {
        this.editmode = false;
        this.save.emit();
    }
}