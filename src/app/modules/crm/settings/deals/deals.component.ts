import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorage } from '../../../../services/localstorage';
import { TranslateService } from '@ngx-translate/core';
import { SettingsDealsOperations } from '../../../../services/settings-deals.service';
import { PermissionService } from '../../../../services/permissionService';

import { DragulaService } from '../../../../draggable/providers/dragula.provider';
import { Subscription } from 'rxjs/Subscription';
import { ButtonPanelService } from '../../../../services/button-panel.service';

@Component({
	templateUrl: './deals.html',
	providers: [LocalStorage],
	viewProviders: [DragulaService]
})

export class CustomizeDealsComponent {

	private buttonSub: Subscription;
	private dragSub: Subscription;
	private permSub: Subscription;

	constructor(private permissions: PermissionService, private router: Router,
		private translate: TranslateService,
		public _salesService: SettingsDealsOperations,
		public _dragulaService: DragulaService,
		private button: ButtonPanelService
	) {
		_dragulaService.setOptions('nested-bag', {
			revertOnSpill: true
		});

		_dragulaService.setOptions('postsales-bag', {
			revertOnSpill: true
		});

		_dragulaService.setOptions('lostdeals-bag', {
			revertOnSpill: true
		});

		_dragulaService.setOptions('closed-bag', {
			revertOnSpill: true
		});
		this.dragSub = _dragulaService.dropModel.subscribe((value: any) => {
			this.onDrop(value[0], value.slice(1));
		});

	}
	ngOnInit() {
		this.button.addPanelButton("ADDSALESPROCESS", "add-process", "plus");
		this.buttonSub = this.button.commandObservable().subscribe(command => {
			if (command == "add-process") this.router.navigate(["CRM/Settings/Deals/AddSalesProcess"]);
		})

		this.permissions.IsLoaded().subscribe(res => {
			if (res) {
				if (!this.permissions.handleSalesProcess) {
					this.permissions.RedirectForbidden();
				}
			}
		});
	}

	ngOnDestroy() {
		this.button.clearPanel();
		this.buttonSub.unsubscribe();
		if (this.dragSub) this.dragSub.unsubscribe();
		if (this.permSub) this.permSub.unsubscribe();
	}

	deleteSalesProcess(id: any) {
		var conf = confirm(this.translate.instant('WARNDELETE'));
		if (conf) {
			this._salesService.deleteSalesProcess(id);
		}
	}

	onDrop(type: string, args: any) {
		var dealOld = args[0].getAttribute('class').split(',');
		var salesprocessindex = dealOld[3];

		var processId = this._salesService.salesProcessList[salesprocessindex].Id;
		var items: Array<any>;
		switch (type) {
			case 'nested-bag':
				items = this._salesService.salesProcessList[salesprocessindex].SalesStatus.SalesPipe;
				break;
			case 'postsales-bag':
				items = this._salesService.salesProcessList[salesprocessindex].SalesStatus.PostSalesPipe;
				break;
			case 'lostdeals-bag':
				items = this._salesService.salesProcessList[salesprocessindex].SalesStatus.LostSalesPipe;
				break;
			case 'closed-bag':
				items = this._salesService.salesProcessList[salesprocessindex].SalesStatus.ClosedSalesPipe;
				break;
			default: break;
		}

		this.onSortOrderChange(processId, items);
	}

	onSortOrderChange(processId: string, item: any) {
		this._salesService.updateSortOrder(processId, item);
	}

	deleteField(dealType: any, salesId: string, fieldid: string) {
		let conf = confirm(this.translate.instant('WARNDELETE'));
		if (conf) {
			this._salesService.deleteDealStatus(dealType, salesId, fieldid);
		};

	}
}
