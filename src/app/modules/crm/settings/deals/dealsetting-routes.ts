import { Routes } from '@angular/router';

// Components
import { CustomizeDealsComponent } from './deals.component';
import { SalesProcessComponent } from './salesprocess/salesprocess.component';
import { VoidComponent } from '../../../shared/components/void';
import { DealStatusComponent } from './dealstatus/dealstatus.component';


export const DealsettingsRoutes: Routes = [
    {
        path: 'Deals',
        component: CustomizeDealsComponent,
        children: [
            { path: '', component: VoidComponent },
            { path: 'AddSalesProcess', component: SalesProcessComponent },
            { path: 'AddDealStatus/:salesProcessId/:dealStatusId', component: DealStatusComponent },
            { path: 'EditDealStatus/:salesProcessId/:dealStatusId/:dealId', component: DealStatusComponent }
        ]
    }
];