import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { NotificationService } from '../../../../../services/notification-service';
import { SettingsDealsOperations } from '../../../../../services/settings-deals.service';
import { Api } from '../../../../../services/api';
import { LocalStorage } from '../../../../../services/localstorage';
import { ChosenOptionModel } from '../../../../../modules/shared/components/chosen/chosen.models';

@Component({
    templateUrl: './dealstatus.html',
})

export class DealStatusComponent {
    private sub: any;

    public SaleProcessForm: any;
    public saleProcessId: string = '';
    public dealStatusId: string = '1';
    public dealId: string = '';
    public salesName: string = '';
    public probability: number = 0;
    public dealStatuses: ChosenOptionModel[] = [];
    constructor(
        public _form: FormBuilder,
        public _router: Router,
        public _notify: NotificationService,
        public _salesService: SettingsDealsOperations,
        public route: ActivatedRoute,
        public _localstore: LocalStorage,
        public _http: Api
    ) {
        this.sub = this.route.params.subscribe(params => {
            this.saleProcessId = params['salesProcessId'];
            this.dealStatusId = params['dealStatusId'];

            this.SaleProcessForm = this._form.group({
                Name: ['', Validators.required],
                Percentage: ['0'],
                Type: ['', Validators.required],
                SaleId: [this.saleProcessId]
            });

            if (params['dealId']) {
                this.dealId = params['dealId'];
                var orgId = this._localstore.get('orgId');
                this._http.get('Organisations/' + orgId + '/SalesProcess/' + this.saleProcessId + '/DealStatus/' + this.dealId)
                    .map(res => res.json())
                    .subscribe(res => {
                        if (res.IsSuccess === true) {
                            this.salesName = res.Data.Name;
                            this.probability = res.Data.Percentage;
                        }
                    });
            }
        });
        this.dealStatuses.push(new ChosenOptionModel(1, "Sales Pipe"));
        this.dealStatuses.push(new ChosenOptionModel(2, "Post Salespipe"));
        this.dealStatuses.push(new ChosenOptionModel(3, "Lost Deals"));
        this.dealStatuses.push(new ChosenOptionModel(4, "Closed"));
    }

    addSalesfieldPost() {
        if (isNaN(this.SaleProcessForm.value.Percentage)) {
            this._notify.error('Please provide a valid probability');
        }
        else {

            this._salesService.addDealStatus(this.saleProcessId, this.SaleProcessForm.value);
            this.back();

        }

    }

    update() {
        this.SaleProcessForm.value.Percentage = this.SaleProcessForm.value.Type === '1' ? this.SaleProcessForm.value.Percentage : '0';
        if (isNaN(this.SaleProcessForm.value.Percentage)) {
            this._notify.error('Please provide a valid probability');
        } else {
            if (this.SaleProcessForm.valid) {
                this._salesService.updateDealStatus(this.saleProcessId, this.dealId, this.SaleProcessForm.value);
                this.back();
            }
        }
    }

    clickToClose(event: any) {
        var target = event.target || event.srcElement;
        if (target.id === 'newSalesStatus') {
            this.back();
        }
    }

    back() {
        this._router.navigate(['CRM', 'Settings', 'Deals']);
    }

    ngOnDestroy() {
        if (this.sub) this.sub.unsubscribe();
    }

}
