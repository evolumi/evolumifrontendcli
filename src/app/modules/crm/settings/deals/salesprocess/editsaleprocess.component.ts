import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NotificationService } from '../../../../../services/notification-service';
import { SettingsDealsOperations } from '../../../../../services/settings-deals.service';

@Component({
    selector: 'edit-sales-process',
    templateUrl: './editsaleprocess.html',
})

export class EditSalesProcessComponent {

    private _salesProcessId: string;
    public saleProcessName: string;
    public startApi: boolean = false;
    public salesProcessForm: any;
    public displayModal: boolean = false;

    constructor(
        _form: FormBuilder,
        public _notify: NotificationService,
        private _salesService: SettingsDealsOperations
    ) {
        this.salesProcessForm = _form.group({
            saleProcessName: ['', Validators.required]
        });
    }

    public save() {
        this.startApi = true;
        if (this.salesProcessForm.valid) {
            this._salesService.updateSalesProcess(this._salesProcessId, this.salesProcessForm.value.saleProcessName);
            this.hideModal();
        } else {
            this._notify.error('Invalid salesprocess name');
        }
    }

    public hideModal() {
        this.displayModal = false;
    }

    public clickToClose(event: any) {
        var target = event.target || event.srcElement;
        if (target.id === 'editSales') {
            this.hideModal();
        }
    }

    public showModal(salesProcessId: string, saleProcessName: string) {
        this.displayModal = true;
        this._salesProcessId = salesProcessId;
        this.saleProcessName = saleProcessName;
    }
}
