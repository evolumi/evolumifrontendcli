import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NotificationService } from '../../../../../services/notification-service';
import { SettingsDealsOperations } from '../../../../../services/settings-deals.service';

@Component({
	templateUrl: './salesprocess.html',
})

export class SalesProcessComponent {
	public SalesProcessForm: any;
	public startApi: boolean = false;
	constructor(
		public _form: FormBuilder,
		public _router: Router,
		public _notify: NotificationService,
		public _salesService: SettingsDealsOperations
	) {
		this.SalesProcessForm = _form.group({
			saleProcessName: ['', Validators.required]
		});
	}

	createSalesProcess() {
		this.startApi = true;
		if (this.SalesProcessForm.valid) {
			this._salesService.addSalesProcess(this.SalesProcessForm.value.saleProcessName);
			this.back();
		} else {
			this._notify.error('Invalid salesprocess name');
		}
	}
	clickToClose(event: any) {
		var target = event.target || event.srcElement;
		if (target.id === 'addSales') {
			this.back();
		}
	}
	back() {
		this._router.navigate(['/CRM', 'Settings', 'Deals']);
	}

}
