import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Api } from '../../../../services/api';
import { NotificationService } from '../../../../services/notification-service';
import { ValidationService } from '../../../../services/validation.service';
import { ChosenOptionModel } from '../../../../modules/shared/components/chosen/chosen.models';
import { PermissionService } from '../../../../services/permissionService';
import { Subscription } from "rxjs/Subscription";
declare var $: JQueryStatic;

@Component({
    templateUrl: './update-card.html',
    selector: 'credit-card-select',
})

export class UpdateCardComponent {
    @Input() orgId: string;
    @Output() cardSelected: EventEmitter<any>;

    public chosenCurrencyName: string;

    public cardTypes: any = [];
    public month: any = [];
    public year: any = [];
    public cvv: any = [];
    public currency: string;
    public selectedMonth: number;
    public selectedYear: number;

    public paymentBasic: any = {};

    public creditCard: any;
    public errorMsg = false;
    public erroMonth = false;
    public currentYear = new Date().getFullYear();
    public currentMonth = new Date().getMonth() + 1;
    public pay: boolean = false;
    public editCard: boolean = false;

    private permSub: Subscription;

    constructor(private permissions: PermissionService,
        public _http: Api,
        private _notify: NotificationService,
        private form: FormBuilder) {
        this._http = _http;
        this.cardSelected = new EventEmitter();
        this.paymentBasic = this.form.group({
            cardholdername: ['', Validators.required],
            cardnumber: ['', Validators.compose([Validators.required, ValidationService.creditCardValidator])],
            cvv: ['', Validators.compose([Validators.required, ValidationService.cvvValidator])]
        });
    }
    ngOnInit() {
        this.permSub = this.permissions.IsLoaded().subscribe(res => {
            if (res) {
                if (!this.permissions.handleBilling) {
                    this.permissions.RedirectForbidden();
                }
            }
        });
        this.cardTypes = ['Visa', 'Mestro', 'American Express'];
        for (var i = 1; i <= 12; i++) {
            this.month.push(new ChosenOptionModel(i, i));
        }
        for (var i = this.currentYear; i <= this.currentYear + 20; i++) {
            this.year.push(new ChosenOptionModel(i, i));
        }
        this.selectedMonth = this.currentMonth;
        this.selectedYear = this.currentYear;

        this.getCredicardDetails();
    }

    ngOnDestroy(){
        if (this.permSub) this.permSub.unsubscribe();
    }

    getCredicardDetails() {
        this._http.get('Organisations/' + this.orgId + '/CreditCardToken')
            .map(res => res.json())
            .subscribe(res => {
                this.creditCard = res.Data;
                this.cardSelected.emit(res.Data);
            });
    }

    updatePayment() {
        if (this.paymentBasic.valid) {
            var expDate = this.selectedYear + '-' + this.selectedMonth;
            console.log(expDate);
            var d = {
                'CardNumber': this.paymentBasic._value.cardnumber,
                'ExpireDate': expDate,
                'Cvc': this.paymentBasic._value.cvv
            };
            this._http.post('Organisations/' + this.orgId + '/RegisterCreditCard', d)
                .map(res => res.json())
                .subscribe(res => {
                    //console.log(res);
                    if (res.IsSuccess == true) {
                        this._notify.success('card registerd');
                        $('#pay').removeClass('systemActive');
                        this.getCredicardDetails();
                        this.pay = true;
                        this.editCard = false;

                    } else {
                        this._notify.error(res.Message);
                    }
                });
        }

    }
    edit() {
        this.editCard = true;
        this.cardSelected.emit(null);
    }

    cancleUpdateCard() {
        this.editCard = false;
        if (this.creditCard)
            this.cardSelected.emit(this.creditCard);
    }

    ngOnChanges(changes: any) {
        if (changes["orgId"]) {
            console.log("[Update-Card] Organisation changed -> " + this.orgId);
            this.getCredicardDetails();
        }
    }

    onChange(key: string, value: any) {
        switch (key) {
            case 'selectedYear':
                this.selectedYear = value;
                break;
            case 'selectedMonth':
                this.selectedMonth = value;
                console.log('inside selcted month');
                break;
            default:
                break;
        }
    }

}