import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { FormBuilder } from '@angular/forms';
import { Api } from '../../../../services/api';
import { NotificationService } from '../../../../services/notification-service';
import { LocalStorage } from '../../../../services/localstorage';

import { PermissionService } from '../../../../services/permissionService';
import { FilterCollection } from '../../../../services/filterservice';
import { Subscription } from "rxjs/Subscription";

@Component({
	templateUrl: './payment.html',
})

export class PaymentComponent {
	public creditAmout: string;
	public cretidToBuy: string;
	public chosenCurrencyName: string;

	public paymentBasic: any = {};
	public orgId: string;

	public paymentHistory: Array<any> = [];
	public pay: boolean = false;
	public form: any;
	public creditCard: any;

	private orgSub: Subscription;
	private permSub: Subscription;

	constructor(private permissions: PermissionService, private filterService: FilterCollection, public _http: Api, public Http: Http, form: FormBuilder, public _local: LocalStorage, private _notify: NotificationService) {
		this._http = _http;
		this.form = form;

		this.orgId = this._local.get('orgId');

		this.paymentHistory = [];
		this.orgSub = this.filterService.currentOrganisation$.subscribe(res => {
			console.log("PaymentData fetched", res.PaymentData.Credits);
			if (res) {
				this.creditAmout = res.PaymentData.Credits;
				this.chosenCurrencyName = res.PaymentData.CurrencyName;
			}
		});
		this.filterService.getCurrentOrganisation();
	}
	ngOnInit() {
		this.permSub = this.permissions.IsLoaded().subscribe(res => {
			if (res) {
				if (!this.permissions.handleBilling) {
					this.permissions.RedirectForbidden();
				}
			}
		});
		this.getPaymentHistory();
	}

	ngOnDestroy(){
		if (this.permSub) this.permSub.unsubscribe();
		if (this.orgSub) this.orgSub.unsubscribe();
	}

	buyCredits() {

		var d = {
			'Amount': this.cretidToBuy
		};

		this._http.post('Organisations/' + this.orgId + '/AddCredits', d)
			.map(res => res.json())
			.subscribe(res => {
				if (res.IsSuccess == true) {
					this._notify.success(this.cretidToBuy + ' Credits Added!');

					this.getPaymentHistory();
					this.pay = true;
					this.creditAmout = res.Data.Credits;

				} else {
					this._notify.error(res.Message);
				}
			});
	}



	getPaymentHistory() {

		this._http.get('Organisations/' + this.orgId + '/PaymentHistory')
			.map(res => res.json())
			.subscribe(res => {
				if (res.IsSuccess === true) {
					this.paymentHistory = res.Data;
				}

			});
	}
	// clickToClose(event) {
	// 	var target = event.target || event.srcElement;

	// 	if (target.id === 'pay') {
	// 		this.close();
	// 	}
	// }

	// onChange(key,value){
	// 	switch (key){
	// 		case 'selectedCurrency':
	// 			this.selectedCurrency = value;
	// 			console.log('inside selcted currency');
	// 		break;
	// 		case 'selectedYear':
	// 			this.selectedYear = value;
	// 		 break;
	// 		case 'selectedMonth':
	// 			this.selectedMonth = value;
	// 			console.log('inside selcted month');
	// 		break;
	// 		default: 
	// 		break;
	// 	}
	// }

}
