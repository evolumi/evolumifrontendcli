import { Component } from '@angular/core';
import { PhoneNumber } from '../../../../services/callservice';
import { AuthService } from '../../../../services/auth-service';
import { OrgService } from '../../../../services/organisation.service';

@Component({
    templateUrl: './phonenumbers.html',
})

export class PhoneNumbersComponent {

    public orgNumbers: Array<PhoneNumber>;
    public userNumbers: Array<PhoneNumber>;
    public selectedNumber: PhoneNumber;
    public defaultOrgNumber: PhoneNumber;

    private orgSub: any;
    private userSub: any;
    private orgId: string;

    constructor(
        private auth: AuthService, private orgService: OrgService) {
        this.orgSub = this.orgService.currentOrganisationObservable().subscribe(org => {
            if (org) {
                console.log("[Settings] Loaded Phonenumbers for [" + org.OrganisationName + "] -> [" + org.PhoneNumbers.length + "]");
                this.orgId = org.Id;
                this.defaultOrgNumber = org.DefaultPhoneNumber;
                this.orgNumbers = org.PhoneNumbers;
                this.selectedNumber = org.UserPhoneNumber;
            }
        });

        this.userSub = this.auth.currentUserObservable().subscribe(user => {
            if (user) {
                console.log("[Settings] Loaded Phonenumbers for [" + user.Email + "] -> [" + user.PhoneNumbers.length + "]");
                this.userNumbers = user.PhoneNumbers;
            }
        });
    }

    public addNewOrgNumber(number: PhoneNumber) {
        this.orgService.addPhonenumber(number.Number, number.Description);
    }

    public addNewUserNumber(number: PhoneNumber) {
        this.auth.addPhonenumber(number.Number, number.Description);
    }

    public deleteOrgNumber(id: string) {
        this.orgService.deletePhonenumber(id);
    }

    public deleteUserNumber(id: string) {
        this.auth.deletePhonenumber(id);
    }

    public refresh() {
        this.orgService.getOrganisation(this.orgId);
        this.auth.GetCurrentUser();
    }

    public verify(event: any) {
        // Not implemented, just added for compilation error. Don't know for what it is. Have 1000 errors to take care of so not too interested.
    }

    ngOnDestroy() {
        if (this.userSub) this.userSub.unsubscribe();
        if (this.orgSub) this.orgSub.unsubscribe();
    }
}


