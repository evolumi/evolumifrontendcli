import { Component, Input, Output, EventEmitter } from '@angular/core';
import { OrgService } from '../../../../services/organisation.service';
import { CallService, PhoneNumber } from '../../../../services/callService';

@Component({
    selector: 'phonelist',
    templateUrl: './phonelist.html',
})
export class PhoneList {

    public showNewNumber: boolean = false;
    public newNumber: any;

    @Input() enableSetDefault: boolean;
    @Input() numbers: Array<PhoneNumber>;
    @Input() selectedNumber: PhoneNumber;
    @Output() add = new EventEmitter();
    @Output() delete = new EventEmitter();

    constructor(
        public orgService: OrgService,
        public sinchService: CallService) {
        console.log("[Settings-Phone] Constructor");
        this.newNumber = {};
    }

    public setNewDefaultNumber(number: PhoneNumber) {
        this.orgService.setDefaultPhonenumber(number);
    }

    public selectNumber(number: PhoneNumber) {
        this.orgService.selectPhonenumber(number);
    }

    public verify(number: PhoneNumber) {
        this.sinchService.validateNumber(number);
    }
}