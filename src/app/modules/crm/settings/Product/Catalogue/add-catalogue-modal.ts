import { Component, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'add-catalogue-modal',
    templateUrl: "add-catalogue-modal.html"
})
export class AddCatalogueModal {

    public displayModal: boolean;
    @Output() catalogueSaved = new EventEmitter();

    constructor() {
    }

    public showModal(): void {
        this.displayModal = true;
    }

    public hideModal(): void {
        this.displayModal = false;
    }


    saved($event: any) {
        this.catalogueSaved.emit({});
        this.hideModal();
    }

    clickToClose(event: any) {
        var target = event.target || event.srcElement;
        if (target.id === 'addcatalogue-modal') {
            this.hideModal();
        }
    }
}