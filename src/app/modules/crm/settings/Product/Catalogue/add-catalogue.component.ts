import { Component, Output, EventEmitter, ViewChild } from '@angular/core';
import { ProductService } from "../../../../../services/products.service";
import { CropModal } from "../../../../shared/components/cropmodal/cropmodal"

@Component({
    selector: 'add-catalogue-component',
    templateUrl: "add-catalogue.component.html",
    styleUrls: ["add-catalogue.component.css"]
})
export class AddCatalogueComponent {

    @Output() OnSave = new EventEmitter();
    public catalogue = {
        Id: "",
        ProductsCount: 0,
        IsActive: true,
        Description: "",
        Name: "",
        Image: "",
        Products: new Array<any>()
    }

    @ViewChild('cropmodal') cropmodal: CropModal;
    public data: any;
    public image: any;
    public cropping: boolean;

    constructor(private api: ProductService) { }

    saveCatalogue() {
        this.api.AddOrUpdateCatalogue(this.catalogue).subscribe((data: any) => {
            this.OnSave.emit({});
            this.catalogue.Description = "";
            this.catalogue.Name = "";
            this.catalogue.Image = "";
            this.image = null;
        }, (err: any) => console.error(err));
    }

    cropped($event: any) {
        this.image = $event.image;
        this.catalogue.Image = $event.image.src.toString();
        this.cropping = false;
    }

    imageChanged($event: any) {
        this.data = $event.srcElement.files;
        this.cropmodal.imgCropper.fileChangeListener($event);
        this.cropping = true;
    }

    ngOnDestroy() {
    }
}