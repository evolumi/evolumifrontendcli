import { Component, Output, EventEmitter, ViewChild } from '@angular/core';
import { CatalogueComponent } from "./catalogue.component";

@Component({
    selector: 'catalogue-modal',
    templateUrl: "catalogue-modal.html"
})
export class CatalogueModal {

    public displayModal: boolean;
    public catalogue: any;
    @Output() catalogueUpdated = new EventEmitter();
    @ViewChild("cataloguecomponent") catalogueComponent: CatalogueComponent;

    constructor() {
    }

    emitCatalogueChanges($event: boolean) {
        this.catalogueUpdated.emit();
        if ($event) {
            this.hideModal();
        }
    }
    public showModal(catalogue: any): void {
        this.catalogue = catalogue;
        this.displayModal = true;
    }

    public hideModal(): void {
        this.catalogueComponent.collapseProducts();
        this.displayModal = false;
    }

    clickToClose(event: any) {
        var target = event.target || event.srcElement;
        if (target.id === 'catalogue-modal') {
            this.hideModal();
        }
    }
}