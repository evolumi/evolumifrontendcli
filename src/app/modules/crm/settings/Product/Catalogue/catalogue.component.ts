import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { ProductService, Catalogue, ProductFilterParameters } from "../../../../../services/products.service";
import { CropModal } from "../../../../shared/components/cropmodal/cropmodal";
import { TranslateService } from "@ngx-translate/core";
import { ConfirmationService } from "../../../../../services/confirmation.service";

@Component({
    selector: 'catalogue-component',
    templateUrl: "catalogue.component.html",
    styleUrls: ["catalogue.component.css"]
})
export class CatalogueComponent {

    @Input() catalogue: Catalogue;
    @Output() catalogueUpdated = new EventEmitter();
    public products: any = {};
    private page: any = {};
    private pricelists: any[];
    private editmode: boolean;
    public searchobj = new ProductFilterParameters();
    public data: any;
    public cropping: boolean;
    private image: any;
    @ViewChild("cropmodal") cropmodal: CropModal;

    constructor(private api: ProductService, private translator: TranslateService, private confirmationService: ConfirmationService) {

    }
    private searchTimer: any;
    productSearch() {
        clearTimeout(this.searchTimer);
        this.searchobj.CatalogueId = this.catalogue.Id;
        this.page[this.catalogue.Id] = 0;
        var x = this;
        this.searchTimer = setTimeout(function () {
            x.getProductsForCatalogue(x.catalogue.Id, false);
        }, 150);
    }

    imageChanged($event: any) {
        this.data = $event.srcElement.files;
        this.cropmodal.imgCropper.fileChangeListener($event);
        this.cropping = true;
    }
    cropped($event: any) {
        this.image = $event.image;
        this.catalogue.Image = $event.image.src.toString();
        this.cropping = false;
    }
    confirmDeleteCatalogue() {
        let boxTitle = this.translator.instant("DELETE") + " " + this.translator.instant("CATALOGUE");
        let boxMessage = this.translator.instant("DELETECATALOGUECONFIRMTEXT")
        this.confirmationService.showModal(boxTitle, boxMessage, () => {
            this.deleteCatalogue();
        });
    }

    deleteCatalogue() {
        this.api.DeleteCatalogue(this.catalogue.Id).subscribe(data => {
            this.catalogueUpdated.emit(true);
        });
    }
    productDeleted($event: any) {
        this.products[this.catalogue.Id] = this.products[this.catalogue.Id].filter(function (prod: any) {
            return prod.Id != $event;
        });
        this.catalogueUpdated.emit(false);
    }

    getNextPage() {
        this.page[this.catalogue.Id]++;
        this.getProductsForCatalogue(this.catalogue.Id, true);
    }
    getProductsForCatalogue(catalogueId: string, concat: boolean) {
        if (catalogueId != "ALL") {
            this.api.GetProductsForCatalogue(catalogueId, this.page[catalogueId], this.searchobj).subscribe(data => {
                if (concat) {
                    this.products[catalogueId] = this.products[catalogueId].concat(data);
                }
                else {
                    this.products[catalogueId] = data;
                }
            });
        }
        else {
            if (!this.products["ALL"]) {
                this.products["ALL"] = [];
            }
            this.api.Products(this.page['ALL'], this.searchobj).subscribe(data => {
                if (concat) {
                    this.products["ALL"] = this.products["ALL"].concat(data);
                }
                else {
                    this.products["ALL"] = data;
                }
            })
        }



        this.api.PriceLists().subscribe(data => {
            this.pricelists = data;
        });
    }

    collapseProducts() {
        this.products[this.catalogue.Id].map(function (prod: any) {
            prod.Expand = false;
        });
    }

    toggleEditMode() {
        if (this.editmode) {
            this.api.AddOrUpdateCatalogue(this.catalogue).subscribe(data => { });
        }
        this.editmode = !this.editmode;
    }
    activated() {
        if (this.catalogue && this.catalogue.IsActive) {
            return "fa fa-check";
        }
        else {
            return "fa fa-times";
        }
    }
    ngOnChanges(changes: any) {
        if (changes.catalogue && changes.catalogue.currentValue) {
            if (!this.products[changes.catalogue.currentValue.Id]) {
                this.products[changes.catalogue.currentValue.Id] = [];
            }
            if (!this.page[changes.catalogue.currentValue.Id]) {
                this.page[changes.catalogue.currentValue.Id] = 0;
            }
            if (this.products[changes.catalogue.currentValue.Id].length == 0) {
                this.getProductsForCatalogue(changes.catalogue.currentValue.Id, false);
            }
            this.searchobj = new ProductFilterParameters();
        }
    }
}