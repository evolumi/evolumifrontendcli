import { Component, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'add-pricelist-modal',
    templateUrl: "add-pricelist-modal.html"
})
export class AddPriceListModal {

    public displayModal: boolean;
    @Output() priceListSaved = new EventEmitter();

    constructor() {
    }

    public showModal(): void {
        this.displayModal = true;
    }

    public hideModal(): void {
        this.displayModal = false;
    }
    saved() {
        this.priceListSaved.emit({});
        this.hideModal();
    }
    clickToClose(event: any) {
        var target = event.target || event.srcElement;
        if (target.id === 'addpricelist-modal') {
            this.hideModal();
        }
    }
}