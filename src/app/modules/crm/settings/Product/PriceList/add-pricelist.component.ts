import { Component, EventEmitter, Output } from '@angular/core';
import { ProductService, PriceList } from "../../../../../services/products.service";
import { Configuration } from "../../../../../services/configuration";
import { OrgService } from "../../../../../services/organisation.service";
import { Subscription } from "rxjs/Subscription";

@Component({
    selector: 'add-pricelist-component',
    templateUrl: "add-pricelist.component.html"
})
export class AddPriceListComponent {

    @Output() OnSave = new EventEmitter();
    public pricelist: PriceList = new PriceList();
    private selectedCurrencies: string[];
    private orgSub: Subscription;
    private defaultUnit: string;

    constructor(private api: ProductService, cfg: Configuration, orgSvc: OrgService) {

        this.selectedCurrencies = cfg.currencies;
        this.orgSub = orgSvc.currentOrganisationObservable().subscribe(data => {
            if (data) {
                this.pricelist.Currency = data.CurrencyName;
                this.defaultUnit = data.CurrencyName;
            }
        });
    }

    savePriceList(f: any) {
        this.api.AddOrUpdatePriceList(this.pricelist).subscribe((data) => {
            this.OnSave.emit({});
            this.pricelist = new PriceList();
            this.pricelist.Currency = this.defaultUnit;
        }, err => console.error(err), () => {
        });
    }

    ngOnDestroy() {
        if (this.orgSub) this.orgSub.unsubscribe();
    }
}