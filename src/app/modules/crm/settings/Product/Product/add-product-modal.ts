import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { AddProductComponent } from "./add-product.component";

@Component({
    selector: 'add-product-modal',
    templateUrl: "add-product-modal.html"
})
export class AddProductModal {

    @Input() catalogues: Array<any> = [];
    @Input() pricelists: Array<any> = [];
    @Output() productSaved = new EventEmitter();
    @Output() test = new EventEmitter();
    public displayModal: boolean;
    @ViewChild("addproductcomponent") addProductComponent: AddProductComponent;

    constructor() {
    }

    public showModal(): void {
        this.displayModal = true;
    }

    public hideModal(): void {
        this.addProductComponent.clearForm();
        this.displayModal = false;
    }

    saved() {
        this.productSaved.emit()
        this.hideModal();
    }

    clickToClose(event: any) {
        var target = event.target || event.srcElement;
        if (target.id === 'addproduct-modal') {
            this.hideModal();
        }
    }
}