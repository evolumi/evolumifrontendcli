import { Component, Output, EventEmitter, Input, ViewChild } from '@angular/core';
import { ProductService } from "../../../../../services/products.service";
import { CropModal } from "../../../../shared/components/cropmodal/cropmodal";
import { ConfirmationService } from "../../../../../services/confirmation.service";
import { TranslateService } from "@ngx-translate/core";
import { ChosenOptionModel } from "../../../../shared/components/chosen/chosen.models";

@Component({
    selector: 'product-component',
    templateUrl: "product.component.html",
    styleUrls: ["product.component.css"]
})
export class EditProductComponent {

    constructor(private api: ProductService, private translator: TranslateService, private confirmationService: ConfirmationService) {
        this.api.PriceLists().subscribe(data => this.pricelists = data);
    }
    @Input() product: any;
    @Output() productDeleted = new EventEmitter();
    public pricelists: any[];
    private editmode: boolean = false;
    public data: any;
    public cropping: boolean;
    private image: any;
    @ViewChild("cropmodal") cropmodal: CropModal;
    private inheritedPrices: boolean;
    private catalogueOptions: ChosenOptionModel[];

    ngOnInit() {
        this.createOptions();
    }

    getStandardPrice(range: number) {
        this.inheritedPrices = true;
        return this.product.PriceLists.find(function (pricelist: any) {
            return pricelist.Range == range;
        }).Prices[this.getStandardPriceList().Id];
    }

    createOptions() {
        this.catalogueOptions = this.api.catalogues.map(catalogue => new ChosenOptionModel(catalogue.Id, catalogue.Name));
    }

    getStandardPriceList() {
        return this.pricelists.find(function (item) {
            return item.Editable == false;
        });
    }

    imageChanged($event: any) {
        this.data = $event.srcElement.files;
        this.cropmodal.imgCropper.fileChangeListener($event);
        this.cropping = true;
    }
    cropped($event: any) {
        this.image = $event.image;
        this.product.Image = $event.image.src.toString();
        this.cropping = false;
    }

    updateAndSetFocus($event: any, item: any) {
        item.Range = $event;
        setTimeout(function () {
            var elem = <any>document.getElementById(item.code);
            elem.focus();
        }, 10);
    }

    multiOnes() {
        var pricesForOne = this.product.PriceLists.filter(function (list: any) {
            return list.Range == -1;
        });
        return pricesForOne.length > 1;
    }

    addRange() {
        var range = this.product.PriceLists.reduce(function (res: any, current: any) {
            current.Range > res ? res = current.Range : null;;
            return res;
        }, 0);
        var newrange = {
            Range: Number(range) + 1,
            code: this.api.getCode(),
            Prices: this.pricelists.reduce(function (res: any, current: any) {
                res[current.Id] = 0;
                return res;
            }, {})
        }
        this.product.PriceLists.push(newrange);
    }

    deleteRange(item: any) {
        this.product.PriceLists = this.product.PriceLists.filter((list: any) => list.code !== item.code);
    }

    confirmDeleteProduct() {
        let boxTitle = this.translator.instant("DELETE") + " " + this.translator.instant("PRODUCT");
        let boxMessage = this.translator.instant("DELETEPRODUCTCONFIRMTEXT")
        this.confirmationService.showModal(boxTitle, boxMessage, () => {
            this.deleteProduct();
        });
    }

    deleteProduct() {
        this.api.DeleteProduct(this.product.Id).subscribe(data => {
            this.productDeleted.emit(this.product.Id);
        })
    }
    // toggleExpand(product: any) {
    //     product.Expand = !product.Expand;
    // }
    editClass() {
        if (this.editmode) {
            return "fa fa-floppy-o";
        }
        else {
            return "fa fa-wrench";
        }
    }


    toggleEditMode() {
        this.editmode = !this.editmode;
        if (!this.editmode) {
            this.api.AddOrUpdateProduct(this.product).subscribe(data => {
                console.log(data);
                this.inheritedPrices = false;
            });
            this.api.ConvertProductToFrontend(this.product);
        }
    }


}