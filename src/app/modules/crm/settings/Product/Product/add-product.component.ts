import { Component, Output, EventEmitter, Input, SimpleChanges, ViewChild } from '@angular/core';
import { ProductService } from "../../../../../services/products.service";
import { ChosenOptionModel } from "../../../../../modules/shared/components/chosen/chosen.models";
import { CropModal } from "../../../../../modules/shared/components/cropmodal/cropmodal";

@Component({
    selector: 'add-product-component',
    templateUrl: "add-product.component.html",
    styleUrls: ["add-product.component.css"]
})
export class AddProductComponent {

    @Input() catalogues: any[] = [];
    @Input() pricelists: any[] = [];
    @Output() OnSave = new EventEmitter<boolean>();

    @ViewChild('cropmodal') cropmodal: CropModal;
    public catalogueOptions: any;
    public product: any;
    public data: any;
    public image: any;
    public cropping: boolean;
    public selectedUnits: string[] = [];
    public priceType: string = "Unit";

    constructor(private api: ProductService) {
        this.createOptions();
        this.getUnits();
    }

    clearForm() {
        var prices = this.getNewPrices();
        this.product = {
            Name: "",
            Description: "",
            ArticleNumber: "",
            PriceLists: [{
                Range: -1,
                Prices: prices,
                Disabled: true
            }],
            Image: ""
        }
        this.image = null;
    }

    getNewPrices() {
        return this.pricelists.reduce(function (res, current) {
            res[current.Id] = 0;
            return res;
        }, {})
    }

    togglePricesAsTotal() {
        if (this.product.PricesAsTotal) {
            this.product.PriceLists.splice(this.product.PriceLists.indexOf((x: any) => x.Range == -1));
        }
        else {
            let prices = this.pricelists.reduce(function (res, current) {
                res[current.Id] = 0;
                return res;
            }, {})
            this.product.PriceLists.push({ Range: -1, Prices: prices, Disabled: true });
        }
    }

    getUnits() {
        this.api.Units().subscribe(data => {
            this.selectedUnits = data.Data;
            this.product.Unit = data.Data[0];
        });
    }

    cropped($event: any) {
        this.image = $event.image;
        this.product.Image = $event.image.src.toString();
        this.cropping = false;
    }

    setPriceType() {
        switch (this.priceType) {
            case "Unit": {
                this.product.PricesAsTotal = false;
                this.product.ManualPrices = false;
                break;
            }
            case "Total": {
                this.product.PricesAsTotal = true;
                this.product.ManualPrices = false;
                break;
            }
            case "Manual": {
                this.product.PricesAsTotal = true;
                this.product.ManualPrices = true;
                break;
            }
        }
        this.togglePricesAsTotal();
    }

    deleteRange(item: any) {
        this.product.PriceLists = this.product.PriceLists.filter(function (plist: any) {
            return plist.Range != item.Range;
        });
    }

    imageChanged($event: any) {
        this.data = $event.srcElement.files;
        this.cropmodal.imgCropper.fileChangeListener($event);
        this.cropping = true;
    }

    validatePrices() {
        let obj: any = {};
        obj.listsCount = this.pricelists.length;
        obj.rangesCount = this.product.PriceLists.length;
        obj.prices = [];
        obj.valsegs = {
            ranges: false,
            prices: false,
            calc: false
        };
        let rangeCheck = true;
        for (let i = 0; i < obj.rangesCount; i++) {
            rangeCheck = Boolean(Number(this.product.PriceLists[i].Range)) && rangeCheck;
            let x = this.product.PriceLists[i].Prices;
            obj.prices = obj.prices.concat(Object.keys(x).map(function (key) { return x[key]; }));
        }
        obj.valsegs.ranges = rangeCheck;
        obj.valsegs.calc = obj.prices.length == (obj.rangesCount * obj.listsCount);

        let priceCheck = true;
        if (this.priceType == "Manual") {
            priceCheck = true;
        }
        else if (obj.prices.length > 0) {
            for (let i = 0; i < obj.prices.length; i++) {
                priceCheck = Boolean(Number(obj.prices[i])) && priceCheck;
            }
        } else {
            priceCheck = false;
        }
        obj.valsegs.prices = priceCheck;

        return obj.valsegs.ranges && obj.valsegs.prices && obj.valsegs.calc;
    }

    updateAndSetFocus($event: any, item: any) {
        item.Range = $event;
        setTimeout(function () {
            var elem = <any>document.getElementById(item.code);
            elem.focus();
        }, 10);
    }

    addQuantity() {
        var range = this.product.PriceLists.reduce(function (res: Number, current: any) {
            Number(current.Range) > res ? res = Number(current.Range) : null;
            return res;
        }, 0);
        var newrange = {
            Range: Number(range) + 1,
            code: this.api.getCode(),
            Prices: this.pricelists.reduce(function (res: any, current: any) {
                res[current.Id] = 0;
                return res;
            }, {})
        }
        this.product.PriceLists.push(newrange)
    }
    createOptions() {
        this.catalogueOptions = this.catalogues.filter(function (cat) { return cat.Id != "ALL" }).map(catalogue => new ChosenOptionModel(catalogue.Id, catalogue.Name));
    }

    getPriceLists() {
        this.api.PriceLists().subscribe(data => {
            this.pricelists = data
        });
    }

    saveProduct() {
        if (this.priceType == "Manual") {
            let prices = this.getNewPrices();
            this.product.PriceLists = [{
                Range: -1,
                Prices: prices,
                Disabled: true
            }];
            this.product.PricesAsTotal = true;
            this.product.PriceIsManual = true;
        }
        console.log(this.product);
        this.api.AddOrUpdateProduct(this.product).subscribe(data => {
            this.OnSave.emit(true);
            this.clearForm();
        }, err => console.log(err));
    }

    ngOnChanges(change: SimpleChanges) {
        if (change['catalogues']) {
            this.createOptions();
        }
        if (change['pricelists'] && change["pricelists"].currentValue) {
            this.clearForm();
        }
    }
}