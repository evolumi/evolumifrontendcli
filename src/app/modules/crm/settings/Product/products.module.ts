import { NgModule } from '@angular/core';
import { SharedModule } from "../../../shared/shared.module";
// Components
import { ProductsComponent } from './products.component';
import { AddProductComponent } from "./Product/add-product.component";
import { AddProductModal } from "./Product/add-product-modal";
import { EditProductComponent } from "./Product/product.component";

import { CatalogueModal } from "./Catalogue/catalogue-modal";
import { AddCatalogueModal } from "./Catalogue/add-catalogue-modal";
import { AddCatalogueComponent } from "./Catalogue/add-catalogue.component";
import { CatalogueComponent } from "./Catalogue/catalogue.component";

import { AddPriceListModal } from "./PriceList/add-pricelist-modal";
import { AddPriceListComponent } from "./PriceList/add-pricelist.component";

//Services
import { ProductService } from '../../../../services/products.service';

@NgModule({
    imports: [
        SharedModule,

    ],
    declarations: [
        ProductsComponent,
        AddProductModal,
        AddProductComponent,
        EditProductComponent,
        CatalogueModal,
        AddCatalogueModal,
        AddCatalogueComponent,
        CatalogueComponent,
        AddPriceListModal,
        AddPriceListComponent
    ],

    providers: [
        ProductService
    ],
    exports: []
})
export class ProductModule { }