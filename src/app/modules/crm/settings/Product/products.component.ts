import { Component } from '@angular/core';
import { ProductService, Catalogue, PriceList } from "../../../../services/products.service";
import { Configuration } from "../../../../services/configuration";
import { ConfirmationService } from "../../../../services/confirmation.service";
import { TranslateService } from "@ngx-translate/core";

@Component({
    selector: 'products-component',
    templateUrl: "products.component.html",
    styleUrls: ["products.component.css", "fireworks.css"]

})
export class ProductsComponent {

    public selectedCatalogue: string;
    private selectedCurrencies: string[];
    public catalogues: Catalogue[] = [];
    public pricelists: PriceList[] = [];
    private editingpricelist: string;

    constructor(private api: ProductService, cfg: Configuration, private translator: TranslateService, private confirmationService: ConfirmationService) {
        this.getCatalogues();
        this.getPriceLists();
        this.selectedCurrencies = cfg.currencies;
    }

    confirmDeletePriceList(id: string) {
        let boxTitle = this.translator.instant("DELETE") + " " + this.translator.instant("PRICELIST");
        let boxMessage = this.translator.instant("DELETEPRICELISTCONFIRMTEXT")
        this.confirmationService.showModal(boxTitle, boxMessage, () => {
            this.deletePriceList(id);
        });
    }

    editPriceList(pricelist: any) {
        if (this.editingpricelist == pricelist.Id) {
            this.api.AddOrUpdatePriceList(pricelist).subscribe(data => {

            });
            this.editingpricelist = null;
        }
        else {
            this.editingpricelist = pricelist.Id;
        }

    }

    getTotalProductsCount() {
        return this.api.productsCount;
    }
    updateCatalogues() {
        this.getCatalogues();
    }

    deletePriceList(priceListId: string) {
        this.api.DeletePriceList(priceListId).subscribe(data => {
            if (data.IsSuccess) {
                this.pricelists = this.pricelists.filter(function (list) {
                    return list.Id != priceListId;
                })
            }
        });
    }
    getCatalogues() {
        let allproductstrans = this.translator.instant("ALLPRODUCTS");
        this.catalogues = [new Catalogue({
            Id: "ALL",
            Name: allproductstrans,
            Description: allproductstrans,
            Image: "/assets/images/barcode.png",
            IsActive: true,
            ProductsCount: 0
        })]
        this.api.Catalogues().subscribe(data => {
            this.catalogues[0].ProductsCount = this.api.productsCount || 0;
            this.catalogues = this.catalogues.concat(data.sort((a: Catalogue, b: Catalogue) => { return b.ProductsCount - a.ProductsCount }));
        }, err => console.log(err));
    }

    getPriceLists() {
        this.api.PriceLists().subscribe(data => {
            this.pricelists = data;
        }, err => console.log(err));
    }

    catalogueSaved($event: any) {
        this.getCatalogues();
    }

    priceListSaved($event: any) {
        this.getPriceLists();
    }

    productSaved($event: any) {
        this.api.productsCount++;
        this.getCatalogues();
    }

    ngOnDestroy() {
    }

    ngAfterViewInit() {
        this.catalogues[0].ProductsCount = this.api.productsCount;
    }


}