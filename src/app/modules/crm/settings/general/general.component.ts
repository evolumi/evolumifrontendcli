import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Api } from '../../../../services/api';
import { NotificationService } from '../../../../services/notification-service';
import { LocalStorage } from '../../../../services/localstorage';
import { ChosenOptionModel } from '../../../../modules/shared/components/chosen/chosen.models';
import { OrgService } from '../../../../services/organisation.service';
import { Configuration } from '../../../../services/configuration';
import { ExportService } from '../../../../services/export.service';
import { OrganisationUpdateModel } from "../../../../models/organisation.models";
import { Subscription } from "rxjs/Subscription";
declare var $: JQueryStatic;

@Component({
    templateUrl: './general.html',
})
export class GeneralSettingsComponent {
    public allcurrency: any = [];
    public organisation: OrganisationUpdateModel = { Id: "", OrganisationName: "", Currency: "", Email: "", PhoneNumber: "", CustomTitles: {}, PersonCustomFields: [], AccountCustomFields: [], ActionCustomFields: [], DealCustomFields: [], RelationCustomFields: [] };
    public timestamp: any;
    public imageUrl: string;

    public plan: string;

    private orgSub: any;
    private transSub: Subscription;

    constructor(
        private orgService: OrgService,
        private _api: Api,
        private _notify: NotificationService,
        private translate: TranslateService,
        private _localstore: LocalStorage,
        private config: Configuration,
        private exporter: ExportService
    ) {

    }

    ngOnInit() {

        this.orgSub = this.orgService.currentOrganisationObservable().subscribe(res => {
            if (res) {
                this.organisation.Id = res.Id;
                this.organisation.Currency = res.CurrencyName;
                this.organisation.Email = res.Email;
                this.organisation.OrganisationName = res.OrganisationName;
                this.organisation.PhoneNumber = res.PhoneNumber;
                this.imageUrl = res.ImageUrl;
                this.organisation.CustomTitles = res.CustomTitles;
                this.organisation.AccountCustomFields = res.AccountCustomFields
                this.organisation.PersonCustomFields = res.PersonCustomFields
                this.organisation.ActionCustomFields = res.ActionCustomFields
                this.organisation.DealCustomFields = res.DealCustomFields
                this.organisation.RelationCustomFields = res.RelationCustomFields

                this.plan = res.Plan;
                console.log("[Settings-General] Plan: " + this.plan + " -> " + (this.plan == 'Free' ? "Disabeling custom titles" : " Enabeling custom titles"));
            }
            this.timestamp = new Date().getTime();
        })

        for (let cur of this.config.currencies) {
            this.allcurrency.push(new ChosenOptionModel(cur, cur));
        }
    }

    ngOnDestroy() {
        if (this.orgSub) this.orgSub.unsubscribe();
        if (this.transSub) this.transSub.unsubscribe();
    }

    update() {
        this.orgService.updateOrganisation(this.organisation);

        var lang = localStorage.getItem("lan");
        var languageVersion = localStorage.getItem("lanVersion");
        this.transSub = this.translate.getTranslation(lang + languageVersion).subscribe(obj => {
            for (let key in this.organisation.CustomTitles) {
                if (this.organisation.CustomTitles[key]) {
                    console.log("[Org] Replacing tag [" + key + "] with custom value [" + this.organisation.CustomTitles[key] + "] in language file : " + lang + ".i18");
                    obj[key] = this.organisation.CustomTitles[key];
                }
            }
        });
    }

    updateCurval(val: any) {
        this.organisation.Currency = val;
    }

    uploadOrgImage(orgId: string) {
        let formData: FormData = new FormData();
        formData.append('file', (<any>$('#file-input-org'))[0].files[0]);
        this.imageUrl = "assets/images/preloader.gif";
        this.orgService.uploadImage(orgId, formData);
    }

    submitDisabled(): boolean {
        return this.organisation.OrganisationName.length == 0;
    }

    cleanTags() {
        var orgId = localStorage.getItem("orgId");
        this._api.put("Organisations/" + orgId + "/CleanUpTagList", null).map(res => res.json())
            .subscribe(response => {
                if (response.IsSuccess) {
                    console.log('[Org-Settings] Tag Cleanup: Success!');
                    this._notify.success(this.translate.instant("TAGCLEANSUCCESS"));
                }
                else {
                    console.log('[Org-Settings] Tag Cleanup: Failed!');
                    this._notify.success(this.translate.instant("TAGCLEANERROR"));
                }
            });
        this.orgService.getOrganisation(orgId);
    }

    runExport(): void {
        const orgId = this._localstore.get('orgId');
        const path = `Organisations/${orgId}/ExportAll`;
        const date = new Date();
        const fileName = `Export-Organisation-${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}.xlsx`;
        const contentType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
        this.exporter.downloadExport(path, fileName, contentType);
        //     this._api.get(
        // `Organisations/${this.organisation.Id}/ExportAll`).map(res => res.json())
        // .subscribe(response => {
        //     console.log('[Export-Accounts] File download: Success!');
        //     let url = response.Data;
        //     window.open(this._api.apiUrl + "/Files/AccountExport/" + url, "_blank");
        // },
        // error => console.log('[Export-Accounts] File download: Failed!'));
    }
}


