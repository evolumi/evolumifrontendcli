import { Observable } from 'rxjs/Observable';
import { Api } from '../../../../services/api';
import { Injectable } from "@angular/core";

@Injectable()
export class RolesService {

    public _http: any;
    public statisticsUsers$: Observable<Array<any>>;

    public rolesData$: Observable<Array<any>>;
    private _rolesDataObserver: any;
    private _roles = {};


    constructor(_http: Api) {
        this._http = _http;

        this.rolesData$ = new Observable<Array<any>>((observer: any) => {
            this._rolesDataObserver = observer;
        }).share();

    }


    getRoles(orgId: any) {

        this._http.get('Organisations/' + orgId + '/Roles')
            .map((res: any) => res.json())
            .subscribe((res: any) => {
                this._roles = res;
                this._rolesDataObserver.next(this._roles);
            }, (err: any) => {
                console.log('error 77');
            });
    }
}
