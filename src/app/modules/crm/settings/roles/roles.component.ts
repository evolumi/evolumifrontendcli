import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Api } from '../../../../services/api';
import { NotificationService } from '../../../../services/notification-service';
import { LocalStorage } from '../../../../services/localstorage';
import { RolesService } from './roles.service';
import { ChosenOptionModel } from '../../../../modules/shared/components/chosen/chosen.models';
import { PermissionService } from '../../../../services/permissionService';
import { OrgService } from '../../../../services/organisation.service';
import { Subscription } from 'rxjs/Subscription';
import { ContractCategory } from '../../../../models/contracts.models';
import { CategoryGroup } from '../../../../models/category.models';

@Component({
    templateUrl: './roles.html',
    //providers: [RolesService]
})

export class RolesComponent {

    public popUpVisible = false;
    public edit: boolean = true;
    public editable: boolean = true;

    public columns: any = [];
    public rows: any = [];
    public rolesArray: any = [];
    public name: string = '';
    public permission: any = {};
    public IncludedTags: Array<string> = [];
    public ExcludedTags: Array<string> = [];
    public roleID: string = '';
    // public permissionsArray: any = ['ViewAccounts', 'AddAccounts', 'ImportAccounts', 'EditAccounts', 'ChangeOwner', 'DeleteAccounts', 'DeleteDeals', 'DeleteActions', 'DeleteContacts', 'HandleUsers', 'HandleUserRoles', 'HandleDealStatus', 'HandlePayment', 'HandleContracts', 'DeleteContracts', 'CreateContracts', 'ViewContracts'];
    public valueArray: any = [];
    public canViewAccounts: boolean;

    public roleOptions: ChosenOptionModel[] = [];
    public defaultRole: string;

    private orgSub: Subscription;
    private permSub: Subscription;
    private roleSub: Subscription;
    private categoryGroups: Array<CategoryGroup> = [];
    private categoryPermissions: Array<ContractCategory> = [];

    constructor(private permissions: PermissionService,
        public _http: Api,
        public _notify: NotificationService,
        private translate: TranslateService,
        public _localstore: LocalStorage,
        public _roles: RolesService,
        private orgService: OrgService) {

        this.columns = [
            { 'name': 'UserRole', 'label': 'ROLE', 'Link': true },
            { 'name': 'Employees', 'label': 'EMPLOYEES' },
            { 'name': 'Actions', 'label': '', 'Act': true }
        ];
        this.rows = [];
    }
    ngOnInit() {
        this.permSub = this.permissions.IsLoaded().subscribe(res => {
            if (res) {
                if (!this.permissions.handleRoles) {
                    this.permissions.RedirectForbidden();
                }
            }
        });
        this.permission = {
            ViewAccounts: '',
            AddAccounts: '',
            ImportAccounts: '',
            EditAccounts: '',
            ChangeOwner: '',
            DeleteAccounts: '',
            DeleteDeals: '',
            DeleteActions: '',
            DeleteContacts: '',
            HandleUsers: '',
            HandleGoals: '',
            HandleProducts: '',
            HandleUserRoles: '',
            HandleDealStatus: '',
            HandlePayment: '',
            HandleContracts: '',
            ViewContracts: '',
            DeleteContracts: '',
            CreateContracts: '',
            ViewSalesCompetitions: '',
            CreateSalesCompetitions: '',
            ViewGoals: '',
            HandleSalesCommissions: '',
            ViewSalesCommissions: '',
            Export: '',
            EditOrganisationNotes: ''
        };
        this.IncludedTags = [];
        this.ExcludedTags = [];
        this.name = '';
        this.getRoles();

        this.roleSub = this._roles.rolesData$.subscribe((latestCollection: any) => {

            this.rows = latestCollection.Data;

        });

        this.orgSub = this.orgService.currentOrganisationObservable()
            .subscribe(org => {
                this.categoryGroups = org.CategoryGroups || [];
                this.roleOptions = org.Roles.map(x => new ChosenOptionModel(x.Id, x.Name));
                this.defaultRole = org.DefaultRole;
            });

    }

    ngOnDestroy(){
        if (this.orgSub) this.orgSub.unsubscribe();
        if (this.roleSub) this.roleSub.unsubscribe();
        if (this.permSub) this.permSub.unsubscribe();
    }

    setDefaultRole(e: any) {
        this.defaultRole = e;
        this.orgService.setDefaultRole(e, (res: boolean) => {
            console.log(res);
        });
    }

    changed(event: any, pro: string) {
        this.permission[pro] = event;
    }

    changedMany(event: any, pro: string[]) {
        for (let prop in pro) {
            this.permission[prop] = event;
        }
    }

    getRoles() {
        var orgId = this._localstore.get('orgId');
        this._roles.getRoles(orgId);
    }

    delete(id: any) {

        var conf = confirm(this.translate.instant("WARNDELETEROLE"));
        if (conf) {
            var orgId = this._localstore.get('orgId');
            this._http.delete('Organisations/' + orgId + '/Roles/' + id)
                .map(res => res.json())
                .subscribe(res => {
                    if (res.IsSuccess === true) {
                        console.log("[Settings-Roles] Delete Role: Success!");
                        this._notify.success(this.translate.instant("ROLEDELETED"));
                        this.getRoles();
                        this.orgService.rolesChange(null);
                    } else {
                        this._notify.error(res.Message);
                        console.log("[Settings-Roles] Delete Role: Failed! -> " + res.Message);
                    }
                });
        }
    }

    newRole() {
        this.edit = false;
        this.popUpVisible = true;
        // TweenMax.set('#editRole', { className: '+=systemActive' });
        this.permission.ViewAccounts = 'ViewAccounts';
        this.permission.AddAccounts = 'AddAccounts';
        this.permission.ImportAccounts = 'ImportAccounts';
        this.permission.EditAccounts = 'EditAccounts';
        this.permission.ChangeOwner = 'ChangeOwner';
        this.permission.DeleteAccounts = 'DeleteAccounts';
        this.permission.DeleteDeals = 'DeleteDeals';
        this.permission.DeleteActions = 'DeleteActions';
        this.permission.DeleteContacts = 'DeleteContacts';
        this.permission.HandleUsers = 'HandleUsers';
        this.permission.HandleUserRoles = 'HandleUserRoles';
        this.permission.HandleDealStatus = 'HandleDealStatus';
        this.permission.HandlePayment = 'HandlePayment';
        this.permission.HandleContracts = 'HandleContracts';
        this.permission.ViewContracts = 'ViewContracts';
        this.permission.DeleteContracts = 'DeleteContracts';
        this.permission.CreateContracts = 'CreateContracts';
        this.permission.ViewSalesCompetitions = 'ViewSalesCompetitions'
        this.permission.CreateSalesCompetitions = 'CreateSalesCompetitions';
        this.permission.HandleGoals = 'HandleGoals';
        this.permission.HandleProducts = 'HandleProducts';
        this.permission.ViewGoals = 'ViewGoals';
        this.permission.ViewSalesCommissions = 'ViewSalesCommissions';
        this.permission.HandleSalesCommissions = 'HandleSalesCommissions';
        this.permission.Export = "Export";
        this.permission.EditOrganisationNotes = "EditOrganisationNotes";

        this.IncludedTags = [];
        this.ExcludedTags = [];
        this.name = '';
        this.editable = true;
    }

    getRole(id: any) {
        this.edit = true;
        this.popUpVisible = true;
        // TweenMax.set('#editRole', { className: '+=systemActive' });
        this.roleID = id;
        var orgId = this._localstore.get('orgId');
        this._http.get('Organisations/' + orgId + '/Roles/' + id)
            .map(res => res.json())
            .subscribe(res => {
                if (res.IsSuccess === true) {
                    this.name = res.Data.Name;
                    console.log("[Settings-Roles] Loaded " + (res.Data.Editable ? "" : "Admin ") + "Role [" + res.Data.Id + "]: " + res.Data.Name);
                    this.editable = res.Data.Editable;
                    for (var i in this.permission) {
                        this.permission[i] = 'No';
                    }

                    for (let i of res.Data.Permissions) {
                        let prop = i.replace("Own", "");
                        this.permission[prop] = i;

                        // if (i == "ViewAccounts" || i == "ViewOwnAccounts")
                        //     this.permission.ViewAccounts = i;
                        // if (i == "AddAccounts")
                        //     this.permission.AddAccounts = i;
                        // if (i == "ImportAccounts")
                        //     this.permission.ImportAccounts = i;
                        // if (i == "EditAccounts" || i == "EditOwnAccounts")
                        //     this.permission.EditAccounts = i;
                        // if (i == "ChangeOwner")
                        //     this.permission.ChangeOwner = i;
                        // if (i == "DeleteAccounts" || i == "DeleteOwnAccount")
                        //     this.permission.DeleteAccounts = i;
                        // if (i == "DeleteDeals" || i == "DeleteOwnDeals")
                        //     this.permission.DeleteDeals = i;
                        // if (i == "DeleteActions" || i == "DeleteOwnActions")
                        //     this.permission.DeleteActions = i;
                        // if (i == "DeleteContacts" || i == "DeleteOwnContacts")
                        //     this.permission.DeleteContacts = i;
                        // if (i == "HandleUsers")
                        //     this.permission.HandleUsers = i;
                        // if (i == "HandleUserRoles")
                        //     this.permission.HandleUserRoles = i;
                        // if (i == "HandleDealStatus")
                        //     this.permission.HandleDealStatus = i;
                        // if (i == "HandlePayment")
                        //     this.permission.HandlePayment = i;
                        // if (i == 'HandleContracts')
                        //     this.permission.HandleContracts = i;
                        // if (i == 'ViewContracts')
                        //     this.permission.ViewContracts = i;
                        // if (i == 'DeleteContracts')
                        //     this.permission.DeleteContracts = i;
                        // if (i == 'CreateContracts')
                        //     this.permission.CreateContracts = i;
                        // if (i == 'ViewSalesCompetitions')
                        //     this.permission.ViewSalesCompetitions = i;
                        // if (i == 'CreateSalesCompetitions')
                        //     this.permission.CreateSalesCompetitions = i;
                        // if (i == 'ViewGoals' || i == 'ViewOwnGoals')
                        //     this.permission.ViewGoals = i;
                        // if (i === 'HandleGoals')
                        //     this.permission.HandleGoals = i;
                        // if (i === 'HandleProducts')
                        //     this.permission.HandleProducts = i;
                        // if (i == 'ViewSalesCommissions' || i == 'ViewOwnSalesCommissions')
                        //     this.permission.ViewSalesCommissions = i;
                        // if (i === 'HandleSalesCommissions')
                        //     this.permission.HandleSalesCommissions = i;
                        // if (i === 'Export')
                        //     this.permission.Export = i;
                        // if (i === 'EditOrganisationNotes')
                        //     this.permission.EditOrganisationNotes = i;
                    }

                    this.IncludedTags = res.Data.IncludedTags.filter(function (n: any) { return n != undefined && n != "" });
                    this.ExcludedTags = res.Data.ExcludedTags.filter(function (n: any) { return n != undefined && n != "" });
                    this.categoryPermissions = res.Data.CategoryPermissions || [];

                    //for (let i = 0; i < this.IncludedTags.length; i++) {
                    //    if (!this.IncludedTags[i]) {
                    //        this.IncludedTags.splice(i, 1);
                    //        i--;
                    //    }
                    //}

                    //for (let i = 0; i < this.ExcludedTags.length; i++) {
                    //    if (!this.ExcludedTags[i]) {
                    //        this.ExcludedTags.splice(i, 1);
                    //        i--;
                    //    }
                    //}
                }
                else {
                    console.log("[Settings-Roles] Loaded Role: Failed! -> " + res.message);
                    this._notify.error(res.message);
                }
            }, err => {
                console.log("[Settings-Roles] Loaded Role: Failed!");
                console.log(err);
            }
            );
    }

    Create() {

        var orgId = this._localstore.get('orgId');
        if (this.name !== null || this.name !== '') {
            let permis: any[] = [];
            for (var i in this.permission) {
                permis.push(this.permission[i]);
            }

            let inTags: any[] = [];
            inTags = this.IncludedTags;
            let exTags: any[] = [];
            exTags = this.ExcludedTags;
            let catPerms: any[] = [];
            catPerms = this.categoryPermissions;
            let obj = {
                'Name': this.name,
                'Permissions': permis,
                'IncludedTags': inTags,
                'ExcludedTags': exTags,
                'CategoryPermissions': catPerms
            };

            if (this.edit) {
                this._http.put('Organisations/' + orgId + '/Roles/' + this.roleID, obj)
                    .map(res => res.json())
                    .subscribe(res => {
                        if (res.IsSuccess === true) {
                            console.log("[Settings-Roles] Update Role: Success!");
                            this._notify.success(this.translate.instant("ROLEUPDATED"));
                            this.getRoles();
                            this.orgService.rolesChange(null);
                            this.back();
                        } else {
                            console.log("[Settings-Roles] Update Role: Failed! -> " + res.message);
                            this._notify.error(res.message);;
                        }
                    }, err => {
                        console.log("[Settings-Roles] Update Role: Failed!");
                        console.log(err);
                    });
            }
            else {
                this._http.post('Organisations/' + orgId + '/Roles', obj)
                    .map(res => res.json())
                    .subscribe(res => {
                        if (res.IsSuccess === true) {
                            console.log("[Settings-Roles] Add Role: Success!");
                            this._notify.success(this.translate.instant("ROLECREATED"));
                            this.getRoles();
                            this.orgService.rolesChange(null);
                            this.back();
                        } else {
                            console.log("[Settings-Roles] Add Role: Failed! -> " + res.message);
                            this._notify.error(res.message);
                        }
                    }, err => {
                        console.log("[Settings-Roles] Add Role: Failed!");
                        console.log(err);
                    });
            }
        } else {
            console.log("[Settings-Roles] Validation Failed: Name is needed!");
        }
    }

    includeChange(event: any) {
        this.IncludedTags = event;
    }

    excludeChange(event: any) {
        this.ExcludedTags = event;
    }

    getCategoryPermissions(id: string) {
        return this.categoryPermissions.filter(x => x.Id === id).map(x => x.Name);
    }

    onCategoryChange(id: string, items: Array<string>) {
        this.categoryPermissions = this.categoryPermissions.filter(x => x.Id !== id)
        items.map(x => {
            if (!this.categoryPermissions.find(y => y.Id === id && y.Name === x)) {
                this.categoryPermissions.push(new ContractCategory({ Id: id, Name: x }))
            }
        });
    }

    // selectAllCategories(id: string, viewAll: boolean) {
    //     console.log('SELECT', viewAll);
    //     if (viewAll) {
    //         const catGroup = this.categoryGroups.find(x => x.Id === id);
    //         this.categoryPermissions = this.categoryPermissions.filter(x => x.Id !== id)
    //         catGroup.Categories.map(x => {
    //             this.categoryPermissions.push(new ContractCategory({ Id: id, Name: x }));
    //         });
    //     } else {
    //         console.log('NOTVIEWALL', this.categoryPermissions);
    //         this.categoryPermissions = this.categoryPermissions.filter(x => x.Id !== id)
    //     }
    // }

    back() {
        this.popUpVisible = false;
    }
    clickToClose(event: any, type: any) {
        if (type === 'edt' && event.srcElement.id === 'editRole') {
            this.back();
        }
    }
}