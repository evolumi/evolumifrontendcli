import { Component, Input, Output, EventEmitter, ElementRef } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs/Subscription';
import { SMTPSettingsModel } from '../../../../models/smtp-settings.models';
import { Api } from '../../../../services/api';
import { NotificationService } from '../../../../services/notification-service';
import { AuthService } from '../../../../services/auth-service';
declare var $: JQueryStatic;

@Component({

    selector: 'smtp-settings-form',
    templateUrl: './smtpsettingsform.html',
})

export class SMTPSettingsFormComponent {

    @Input() smtpSettings: SMTPSettingsModel;
    @Output() sendForm: EventEmitter<boolean>;

    public isValid: boolean;
    private validEmails: Array<string> = [];
    public userSubscription: Subscription;

    private _$element: any;
    public showESignFeature: boolean;

    constructor(private _el: ElementRef, private api: Api, private notify: NotificationService, private translate: TranslateService, private auth: AuthService
    ) {
        this.sendForm = new EventEmitter();
        this.userSubscription = this.auth.currentUserObservable().subscribe(user => {
            if (user) {
                this.validEmails = user.ValidEmails;
                this.checkValidity();
            }
        });
    }

    ngOnInit() {
        this._$element = (<any>$(this._el.nativeElement)).find('.redactor');
        var self = this;
        this._$element.redactor({
            focus: true,
            buttons: ['format', 'bold', 'italic', 'deleted',
                'lists', 'image', 'link'],
            imageResizable: true,
            imagePositioning: true,
            callbacks: {
                change: function () {
                    self.smtpSettings.signature = this.code.get();
                },
                imageUpload: (image: any, json: any) => {
                    // This is to be able to upload Blob since the built-in callback returns only JSON
                    self.api.postDownloadFile('Users/SMTP/GetTempFile', json.Data)
                        .subscribe(res => {
                            const blob = new Blob([res], { type: 'application/octet-stream' });
                            const url = window.URL.createObjectURL(blob);
                            $(image).attr('src', url);
                            json.Data.BlobLocation = url;
                            self.addImage(json.Data);
                        });
                },
            },
            imageUpload: `${this.api.apiUrl}Users/SMTP/UploadTempFile`
        });

        if (this.smtpSettings) {
            this._$element.redactor('code.set', this.smtpSettings.signature);
        }
    }

    ngOnChanges(changes: any) {
        if (changes.smtpSettings && changes.smtpSettings.currentValue && this._$element) {
            this._$element.redactor('code.set', this.smtpSettings.signature);
        }
        this.checkValidity();

    }

    ngOnDestroy() {
        if (this.userSubscription) this.userSubscription.unsubscribe();
        this._$element.redactor('core.destroy');
    }

    update() {
        this.sendForm.emit(true);
    }

    checkValidity() {
        if (this.smtpSettings) {
            this.isValid = Boolean(this.validEmails.indexOf(this.smtpSettings.sender) != -1);
        }
    }

    validate() {
        this.api.post("Users/SendEmailValidate", this.smtpSettings.sender).subscribe(res => {
            this.notify.success(this.translate.instant("VALIDATESENT"));
        },
            err => {
                this.notify.error(this.translate.instant("SENDERROR"));
            });
    }

    addImage(file: any) {
        this.smtpSettings.images.push(file);
    }
}
