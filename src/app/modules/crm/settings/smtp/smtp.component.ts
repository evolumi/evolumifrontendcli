import { TranslateService } from '@ngx-translate/core';
import { Component } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { NotificationService } from '../../../../services/notification-service';
import { Api } from '../../../../services/api';
import { SMTPSettingsModel } from '../../../../models/smtp-settings.models';
import { OrgService } from '../../../../services/organisation.service';


@Component({
    templateUrl: './smtp.html',
})

export class SMTPComponent {

    public orgSubscription: Subscription;

    public smtpSettings: SMTPSettingsModel = new SMTPSettingsModel(null);
    public loaded: boolean = false;
    private orgId: string;

    constructor(
        private _api: Api,
        private _notify: NotificationService,
        private _translate: TranslateService,
        private orgService: OrgService,
    ) {

    }

    ngOnInit() {
        this.orgSubscription = this.orgService.currentOrganisationObservable().subscribe(org => {
            if (org) {
                console.log("[Settings] Loaded SMTP-Settings for [" + org.OrganisationName + "]");
                this.orgId = org.Id;
                this.smtpSettings = new SMTPSettingsModel(org.SMTPSettings);
                this.loaded = true;
            }
        });

    }

    update(isValid: boolean) {
        if (isValid) {
            this.smtpSettings.organisationId = this.orgId;
            this.removeFigureTag();
            this._api.put(`Users/ChangeSMTPSettings`, this.smtpSettings)
                .map(res => res.json())
                .subscribe(res => {
                    if (res.IsSuccess) {
                        this._notify.success(this._translate.instant("SMTPSETTINGSUPDATED"));
                        this.orgService.getOrganisation(this.orgId);
                        console.log("[Settings] STMP-Settings Update: Success!");
                    }
                    else {
                        this._notify.error(this._translate.instant('SMTPSETTINGSUPDATEFAIL'))
                        console.log("[Settings] STMP-Settings Update: Failed! -> " + res.Message);
                    }
                }, err => {
                    this._notify.error(this._translate.instant('SMTPSETTINGSUPDATEFAIL'))
                });
        }
    }

    // Used to strip the figure tag and <br> thar comes from redactor but doesnt work good with email clients
    removeFigureTag() {
        if (this.smtpSettings.signature)
            this.smtpSettings.signature = this.smtpSettings.signature.replace('<figure>', '').replace('</figure>', '<br/>');
    }

    ngOnDestroy() {
        if (this.orgSubscription) this.orgSubscription.unsubscribe();
    }
}
