import { Routes } from '@angular/router';

// Components
import { SettingsComponent } from './settings.component';
import { GeneralSettingsComponent } from './general/general.component';
import { RolesComponent } from './roles/roles.component';
import { PaymentComponent } from './payment/payment.component';
import { PlanComponent } from './plan/plan.component';
import { CustomizeAccountsComponent } from './accounts/accounts.component';
import { VatComponent } from './vat/vat.component';
import { SMTPComponent } from './smtp/smtp.component';
import { PhoneNumbersComponent } from './phone/phonenumbers.component';
import { ProductsComponent } from "./Product/products.component"
import { GoalsComponent } from './goals/goals.component';
import { UnitsComponent } from './units/units.component';
import { ContractSettingsComponent } from './contracts/contracts.component';
import { SalesCommissionComponent } from './sales-commission/sales-commission.component';
import { CalendarSettingsComponent } from './calendar/calendar-settings.component';
import { DealsettingsRoutes } from './deals/dealsetting-routes';
import { UserRoutes } from './users/users-routes';
import { DocumentRoutes } from "./documents/documents-routes";

export const SettingsRoutes: Routes = [
    {
        path: 'Settings',
        component: SettingsComponent,
        children: [
            ...UserRoutes,
            ...DealsettingsRoutes,
            ...DocumentRoutes,
            { path: '', redirectTo: "General", pathMatch: "full" },
            { path: 'SMTP', component: SMTPComponent },
            { path: 'General', component: GeneralSettingsComponent },
            { path: 'Roles', component: RolesComponent },
            { path: 'Accounts', component: CustomizeAccountsComponent },
            { path: 'Vat', component: VatComponent },
            { path: 'Payment', component: PaymentComponent },
            { path: 'Products', component: ProductsComponent },
            { path: 'Plan', component: PlanComponent },
            { path: 'Phone', component: PhoneNumbersComponent },
            { path: 'Units', component: UnitsComponent },
            { path: 'Goals', component: GoalsComponent },
            { path: 'Contracts', component: ContractSettingsComponent },
            { path: 'SalesCommissions', component: SalesCommissionComponent },
            { path: 'Calendar', component: CalendarSettingsComponent },
            { path: 'Goals', component: GoalsComponent },
        ]
    }
];