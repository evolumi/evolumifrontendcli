import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, Validators } from '@angular/forms';

import { Api } from '../../../../services/api';
import { LocalStorage } from '../../../../services/localstorage';

import { ValidationService } from '../../../../services/validation.service';
import { PermissionService } from '../../../../services/permissionService';
import { NotificationService } from '../../../../services/notification-service';
import { ChosenOption } from "../../../../modules/shared/components/chosen/chosen";
import { ChosenOptionModel } from '../../../../modules/shared/components/chosen/chosen.models';
import { OrgService } from '../../../../services/organisation.service';
import { Configuration } from '../../../../services/configuration';
import { Subscription } from "rxjs/Subscription";

@Component({
    templateUrl: './plan.html',
    styleUrls: ['plan.css']
})

export class PlanComponent {

    private orgSub: any;

    public subscriptions: Array<any> = [];
    public paymentBasic: any;
    public cardTypes: Array<any> = [];
    public cardType: any;
    public month: any = [];
    public year: any = [];
    public cvv: any = [];
    public selectedMonth: any;
    public selectedYear: any;

    public currency: string;
    public defaultCurrency: string;
    public currencies: Array<any> = [];
    public currencyOptions: Array<ChosenOption> = [];

    public isSubscribed: boolean;

    public orgId: string;
    public creditCard: any;
    public plan: any;
    public subScribedPlan: any = undefined;
    public activeUsers: number;

    public subModel: any = { Standard: undefined, Free: undefined, Plus: undefined };
    public creidtCard = false;
    public errorMsg = false;
    public erroMonth = false;
    public currentYear = new Date().getFullYear();
    public currentMonth = new Date().getMonth() + 1;
    public loggedInUserCurrency: string = '';

    private permSub: Subscription;

    constructor(private config: Configuration, private orgService: OrgService, private permissions: PermissionService, private translate: TranslateService, public _notify: NotificationService, public _local: LocalStorage, public _http: Api, form: FormBuilder) {
        this.paymentBasic = form.group({
            //cardType: ['', Validators.required],
            cardholdername: ['', Validators.required],
            cardnumber: ['', Validators.compose([Validators.required, ValidationService.creditCardValidator])],
            month: ['', Validators.required],
            year: ['', Validators.required],
            cvv: ['', Validators.compose([Validators.required, ValidationService.cvvValidator])],
        });
    }
    ngOnInit() {
        this.permSub = this.permissions.IsLoaded().subscribe(res => {
            if (res) {
                if (!this.permissions.handleBilling) {
                    this.permissions.RedirectForbidden();
                }
            }
        });
        this.orgId = this._local.get('orgId');
        this.cardTypes = ['Visa', 'Mestro', 'American Express'];
        this.cardType = 'Visa';

        this.currencies = this.config.currencies;
        for (var cur of this.currencies) {
            this.currencyOptions.push(new ChosenOptionModel(cur, this.translate.instant(cur)));
        }

        for (var i = 1; i <= 12; i++) {
            this.month.push(i);
        }
        for (var i = this.currentYear; i <= this.currentYear + 20; i++) {
            this.year.push(i);
        }
        this.selectedMonth = this.currentMonth;
        this.selectedYear = this.currentYear;

        this.defaultCurrency = 'USD';

        // Get current subscription. If none just set currency to Organisation currency.
        this.orgSub = this.orgService.currentOrganisationObservable().subscribe(org => {
            if (org) {
                this.isSubscribed = org.PaymentData.SubscriptionPerformed;
                this.subScribedPlan = org.PaymentData.SubscriptionModel;
                this.activeUsers = org.PaymentData.ActiveUsers;
                this.currency = this.isSubscribed ? org.PaymentData.CurrencyName : org.CurrencyName;

                if (this.subscriptions) {
                    this.setCurrency(this.currency);
                }
            }
        });

        // Load all the subscription models. And show the correct subscription cards based on current (or default) currency.
        this._http.get('Organisations/SubscriptionModels')
            .map(res => res.json())
            .subscribe(res => {
                if (res.IsSuccess === true) {
                    console.log("[Settings-Plan] Loaded subscription models [" + res.Data.length + "]");
                    this.subscriptions = res.Data;
                    this.setCurrency(this.currency ? this.currency : this.defaultCurrency);
                }
            });

        this._http.get('Organisations/' + this.orgId + '/CreditCardToken')
            .map(res => res.json())
            .subscribe(res => {
                if (res.IsSuccess === true && res.Data != null) {
                    this.creditCard = res.Data;
                } else {
                    this.creditCard = false;
                }
            });
    }


    active(type: any) {
        this.plan = type;
        let payEl = document.getElementById('pay');
        if (payEl) payEl.className += ' systemActive';
    }

    setCurrency(currency: string) {
        console.log("[Settings-Plan] Setting currency: " + currency);

        this.subModel = { Standard: undefined, Plus: undefined, Free: undefined };
        for (var i of this.subscriptions) {
            if (i.Plan === 'Standard' && i.Currency === currency) {
                this.subModel.Standard = i;
            } else if (i.Plan === 'Plus' && i.Currency === currency) {
                this.subModel.Plus = i;
            } else if (i.Plan === 'Free') {
                this.subModel.Free = i;
            }
        }
    }

    updatePayment() {
        if (this.paymentBasic.valid && this.currency) {
            var expDate = this.paymentBasic._value.year + '-' + this.paymentBasic._value.month;
            var d = {
                'CardNumber': this.paymentBasic._value.cardnumber,
                'ExpireDate': expDate,
                'Cvc': this.paymentBasic._value.cvv,
                'Currency': this.currency
            };
            this.currency = this.paymentBasic._value.currency;
            this._http.post('Organisations/' + this.orgId + '/RegisterCreditCard', d)
                .map(res => res.json())
                .subscribe(res => {

                    if (res.IsSuccess === true) {
                        this._notify.success('card registration success');
                        let payEl = document.getElementById('pay');
                        if (payEl) payEl.className = payEl.className.replace('systemActive', '');
                        // $('#pay').removeClass('systemActive');
                    } else {
                        this._notify.error(res.Message);
                    }
                },
                res => {

                    var res = JSON.parse(res._body);
                    this._notify.error('Something went wrong');
                });
        } else {
            this._notify.error('Wrong informations provided');
        }
    }
    close() {
        let payEl = document.getElementById('pay');
        if (payEl) payEl.className = payEl.className.replace('systemActive', '');
        // $('#pay').removeClass('systemActive');
    }
    subscribe() {
        var subscriptopnModel = {
            'Plan': this.plan,
            'Currency': this.currency
        };
        this._http.post('Organisations/' + this.orgId + '/Subscription', subscriptopnModel)
            .map(res => res.json())
            .subscribe(res => {
                this._notify.success('plan activation success');
                let payEl = document.getElementById('pay');
                if (payEl) payEl.className = payEl.className.replace('systemActive', '');
                // $('#pay').removeClass('systemActive');
                this.orgService.getOrganisation(this.orgId);
            });
    }
    edit() {
        this.creditCard = false;

    }
    clickToClose(event: any) {
        var target = event.target || event.srcElement;

        if (target.id === 'pay') {
            this.close();
        }
    }

    ngOnDestroy() {
        if (this.orgSub) this.orgSub.unsubscribe();
        if (this.permSub) this.permSub.unsubscribe();
    }
}
