export class GoalsModel {
    public Id: string;
    public Name: string;
    public Description: string;
    public SalesGoalType: string = ''; //Enum backend
    public ActionTypes: Array<string> = []; //Enum backend
    public ActionStatuses: Array<string> = []; // Enum backend
    public DealStatusId: string;
    public Interval: string; //Enum backend
    public Labels: string[] = [];
    public UserCount: number;
    public SalesProcessId: string;
    public UseActualCloseDate: boolean;
    public Currency: string;

    [key: string]: any;

    constructor(obj: any) {
        for (var key in obj) {
            this[key] = (<any>obj)[key];
        }
    }
}

export class GoalData {
    constructor(public Id: string,
        public UserId: string,
        public RelatedGoalId: string,
        public GoalValue: number,
        public Timestamp: number,
        public IsActive: boolean,
        public Name: string
    ) { }
}