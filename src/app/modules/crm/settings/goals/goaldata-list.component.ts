import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Subject } from 'rxjs/Subject';

import { GoalsService } from '../../../../services/goals.service';
import { GoalData } from './goals.models';
import { Subscription } from "rxjs/Subscription";

@Component({

    selector: '[goalData]',
    template: `
            <td><img [profilepicture]="goalData.UserId" style="min-width: 40px; width: 40px;"/></td>
            <td style="vertical-align: middle;">{{ goalData.UserId | username }}</td>
            <td style="vertical-align: middle;"><input class="goals-edit-input form-control" type="number" [(ngModel)]="goalData.GoalValue" (keyup)="quantityChanged($event.target.value)"/></td>
            <td style="vertical-align: middle;"><i (click)="remove()" class="fa fa-trash goals-pointer"></i></td>
    `,
    styleUrls: ['./goals.component.css']
})

export class GoalDataListComponent {
    @Input() goalData: GoalData;
    @Input() index: number;
    @Input() timestamp: number;
    @Output() onRemove = new EventEmitter<{ goalData: GoalData, index: number }>();
    @Output() onSave = new EventEmitter<{}>();
    private quantityChangedSource = new Subject<GoalData>();

    private quantitySub: Subscription;

    constructor(private goalsService: GoalsService) { }

    ngOnInit() {
        this.quantitySub = this.quantityChangedSource
            .debounceTime(800)
            .distinctUntilChanged()
            .map((value: any) => value)
            .subscribe((value: any) => {
                if (this.timestamp === this.goalData.Timestamp) {
                    this.goalsService.updateGoalData(this.goalData)
                        .subscribe((res: any) => {
                            if (res.IsSuccess) {
                                this.onSave.emit();
                                console.log("[Settings-Goals] Update GoalData: Success!");
                            } else {
                                console.log("[Settings-Goals] Update GoalData: Failed! -> " + res.message);
                            }
                        }, (err: any) => {
                            console.log("[Settings-Goals] Update GoalData: Failed!", err);
                        });
                } else {
                    this.goalData.Timestamp = this.timestamp;
                    this.goalsService.createGoalData(this.goalData)
                        .subscribe((res: any) => {
                            if (res.IsSuccess) {
                                this.onSave.emit();
                                console.log("[Settings-Goals] Create GoalData: Success!");
                            } else {
                                console.log("[Settings-Goals] Create GoalData: Failed! -> " + res.message);
                            }
                        }, (err: any) => {
                            console.log("[Settings-Goals] Create GoalData: Failed!", err);
                        });
                }
            })
    }

    ngOnDestroy(){
        if (this.quantitySub) this.quantitySub.unsubscribe();
    }

    remove() {
        this.onRemove.emit({ goalData: this.goalData, index: this.index })
    }

    quantityChanged(value: any) {
        this.quantityChangedSource.next(value);
    }
}