import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { TranslateService } from '@ngx-translate/core';

import { UsersService } from '../../../../services/users.service';
import { DateService } from '../../../../services/date.service';
import { OrgService } from '../../../../services/organisation.service';

import { ChosenOption } from "../../../shared/components/chosen/chosen";
import { GoalsService } from '../../../../services/goals.service';
import { GoalData, GoalsModel } from './goals.models';
import { ActionStatuses, ActionTypes } from "../../../../services/actionservice";

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'goalDataActive'
})
export class GoalDataActivePipe implements PipeTransform {
    transform(items: GoalData[]): any {
        return items.filter(item => item.IsActive === true);
    }
}

@Component({
    templateUrl: './edit-goal-modal.html',
    styleUrls: ['./goals.component.css'],
    selector: 'edit-goal-modal'
})

export class EditGoalModalComponent implements OnInit, OnDestroy {
    public showPrev = false;

    public userList: ChosenOption[];
    public firstDayOfPeriod: number = 0;
    public lastDayOfPeriod: number = 0;
    public goalDataList: GoalData[] = [];
    public userListData: ChosenOption[];
    private todayDate: number;
    private currentDate: number;
    private goalId: string;
    public goal: GoalsModel;
    public totalSum: number;
    public showSaved: boolean = false;

    //Details view
    public salesProcessName: string;
    public dealStatusName: string;
    public actionStatuses: string;
    public actionTypes: string;

    //subscriptions
    private userListSubscription: Subscription;
    private goalSubscription: Subscription;

    constructor(public goalsService: GoalsService,
        private usersService: UsersService,
        private dateService: DateService,
        private orgSvc: OrgService,
        private translate: TranslateService) { }

    ngOnInit() {

        this.userListSubscription = this.usersService.activeUserListObervable()
            .subscribe(data => this.userListData = data);

        this.goalSubscription = this.goalsService.goalObservable()
            .subscribe(data => {
                if (data) {
                    this.goal = data;
                    if (this.goal.ActionStatuses && this.goal.ActionStatuses.length) this.actionStatuses = this.goal.ActionStatuses.map(x => this.translate.instant(ActionStatuses.find(y => y.value === x).label)).join(', ');
                    if (this.goal.ActionTypes && this.goal.ActionTypes.length) this.actionTypes = this.goal.ActionTypes.map(x => this.translate.instant(ActionTypes.find(y => y.value === x).label)).join(', ');
                    if (this.goal.SalesProcessId) {
                        let salesProcess = this.orgSvc.currentOrganisation().Settings.SaleProcesses.find((x: any) => x.Id === this.goal.SalesProcessId);
                        if (salesProcess) {
                            this.salesProcessName === salesProcess.Name;
                            let dealStatus = salesProcess.DealStatuses.find((x: any) => x.Id === this.goal.DealStatusId);
                            if (dealStatus) this.dealStatusName = dealStatus.Name;
                        }
                    }
                    this.goalId = this.goal.Id;
                    if (this.goal && this.goal.Interval) {
                        this.setDates(this.goal.Interval);
                    }
                    this.goalsService.getGoalDataLatest(this.goal.Id, this.currentDate)
                        .subscribe((data: GoalData[]) => {
                            this.goalDataList = data.map((goalData: GoalData) => { goalData.RelatedGoalId = this.goalId; return goalData });
                            this.updateUserList();
                            this.sumValue();
                        });
                }
            });
    }

    ngOnDestroy() {
        if (this.userListSubscription) this.userListSubscription.unsubscribe();
        if (this.goalSubscription) this.goalSubscription.unsubscribe();
        this.goalsService.changeUserCount(this.goal.Id, this.goalDataList.filter(x => x.IsActive).length);
    }

    createGoalData(goalData: GoalData) {
        this.goalsService.createGoalData(goalData)
            .subscribe((res: any) => {
                if (res.IsSuccess) {
                    this.goalDataList.push(res.Data as GoalData);
                    this.goalDataList = this.goalDataList.slice();
                } else {
                    console.log("[Settings-Goals] Create GoalData: Failed! -> " + res.message);
                }
            }, (err: any) => {
                console.log("[Settings-Goals] Create GoalData: Failed!", err);
            });
    }

    updateGoalData(goalData: GoalData, succFunc?: Function) {
        this.goalsService.updateGoalData(goalData)
            .subscribe((res: any) => {
                if (res.IsSuccess) {
                    succFunc();
                } else {
                    console.log("[Settings-Goals] Update GoalData: Failed! -> " + res.message);
                }
            }, (err: any) => {
                console.log("[Settings-Goals] update GoalData: Failed!", err);
            })
    }

    removeUser(data: any) {
        let goalData = data.goalData;
        let index = data.index;
        goalData.IsActive = false;
        if (this.goalDataList[index].Timestamp == this.currentDate) {
            this.updateGoalData(goalData, () => { this.goalDataList.splice(index, 1); this.goalDataList = this.goalDataList.slice() })
        } else {
            goalData.Timestamp = this.currentDate;
            this.createGoalData(goalData);
        }
        this.updateUserList();
    }

    onAddUser(userId: string, index: number, splice?: boolean) {
        let updateGoalData = this.goalDataList.filter((item: GoalData) => item.UserId == userId && item.Timestamp == this.currentDate);
        if (updateGoalData.length > 0) {
            let goalDataToUpdate = updateGoalData[0]
            goalDataToUpdate.IsActive = true;
            this.updateGoalData(goalDataToUpdate, () => this.goalDataList = this.goalDataList.slice());
        } else {
            const newGoal = new GoalData('', userId, this.goalId, 0, this.currentDate, true, '');
            this.createGoalData(newGoal);
        }
        if (!splice) {
            this.userList.splice(index, 1);
            this.userList = this.userList.slice();
        }
    }

    onAddAllUsers() {
        this.userList.forEach(user => {
            this.onAddUser(user.value.toString(), 0, true);
        });
        this.userList = [];
    }

    onPrevPeriod() {
        this.setDatePrev();
        this.goalsService.getGoalDataLatest(this.goal.Id, this.currentDate)
            .subscribe((data: GoalData[]) => {
                this.goalDataList = data;
                this.updateUserList();
            });
    }

    onNextPeriod() {
        this.setDateNext();
        this.goalsService.getGoalDataLatest(this.goal.Id, this.currentDate)
            .subscribe((data: GoalData[]) => {
                this.goalDataList = data;
                this.updateUserList();
            });
    }

    back() {
        this.goalsService.closeModal();
    }

    saved() {
        this.showSaved = true;
        setTimeout(function () {
            this.showSaved = false;
        }.bind(this), 2500);
        this.sumValue();
    }

    private sumValue() {
        this.totalSum = this.goalDataList.filter(x => x.IsActive).reduce((a, b) => a + b.GoalValue, 0)
    }

    private updateUserList() {
        this.userList = [];
        let goalDataUsers = this.goalDataList.filter((m: GoalData) => m.IsActive === true);
        let add = true;
        for (let i = 0; i < this.userListData.length; i++) {
            add = true;
            for (let j = 0; j < goalDataUsers.length; j++) {
                if (this.userListData[i].value === goalDataUsers[j].UserId) {
                    goalDataUsers.splice(j, 1);
                    add = false;
                    break;
                }
            }
            if (add) {
                this.userList.push(this.userListData[i]);
            }
        }
        this.userList = this.userList.slice();
    }

    private setDates(interval: string) {
        this.firstDayOfPeriod = Number(this.dateService.getFirstDateOfPeriod(interval));
        this.lastDayOfPeriod = Number(this.dateService.getLastDateOfPeriod(interval));
        this.currentDate = this.firstDayOfPeriod;
        this.todayDate = this.firstDayOfPeriod;
    }

    private setDateNext() {
        this.showPrev = true;
        this.firstDayOfPeriod = Number(this.dateService.getNextDateOfPeriod(this.firstDayOfPeriod, this.goal.Interval));
        this.lastDayOfPeriod = Number(this.dateService.getLastDateOfPeriod(this.goal.Interval, 'x', this.firstDayOfPeriod));
        this.currentDate = this.firstDayOfPeriod;
    }

    private setDatePrev() {
        this.firstDayOfPeriod = Number(this.dateService.getPrevDateOfPeriod(this.firstDayOfPeriod, this.goal.Interval));
        this.lastDayOfPeriod = Number(this.dateService.getLastDateOfPeriod(this.goal.Interval, 'x', this.firstDayOfPeriod));
        this.currentDate = this.firstDayOfPeriod;
        if (this.currentDate === this.todayDate) {
            this.showPrev = false;
        }
    }

    clickToClose(event: any) {
        var target = event.target || event.srcElement;

        if (target.id === 'editGoals') {
            this.back();
        }
    }
}