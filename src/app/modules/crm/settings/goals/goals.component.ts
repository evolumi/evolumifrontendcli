import { Component } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';
import { GoalsService } from '../../../../services/goals.service';
import { GoalsModel } from './goals.models';

import { ButtonPanelService } from "../../../../services/button-panel.service";
import { Subscription } from "rxjs/Subscription";

@Component({
    templateUrl: './goals.component.html',
    styleUrls: ['./goals.component.css'],
    selector: 'goals'
})

export class GoalsComponent {
    //html
    public goalList: GoalsModel[] = [];

    //misc
    private goalListSubscription: Subscription;
    private buttonSub: Subscription;

    constructor(public goalsService: GoalsService,
        private translateService: TranslateService,
        private button: ButtonPanelService) { }

    ngOnInit() {
        this.goalsService.getGoals();
        this.goalListSubscription = this.goalsService.goalListObservable()
            .subscribe(goalsList => {
                if (goalsList) {
                    this.goalList = goalsList;
                }
            });
        this.button.addPanelButton("GOALCREATE", "add-goal", "plus");
        this.buttonSub = this.button.commandObservable().subscribe(command => {
            if (command == "add-goal") this.goalsService.openAddModal();
        });
    }

    ngOnDestroy() {
        if (this.goalListSubscription) this.goalListSubscription.unsubscribe();
        if (this.buttonSub) this.buttonSub.unsubscribe();
        this.button.clearPanel();
    }

    openEditGoal(id: string) {
        this.goalsService.getGoal(id);
    }

    delete(id: string) {
        var conf = confirm(this.translateService.instant('WARNDELETEGOAL'));
        if (conf) {
            this.goalsService.deleteGoal(id);
        }
    }
}