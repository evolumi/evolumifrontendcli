import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription'

import { TranslateService } from '@ngx-translate/core';
import { NotificationService } from '../../../../services/notification-service';
import { OrgService } from '../../../../services/organisation.service';

import { ChosenOption } from "../../../shared/components/chosen/chosen";
import { GoalsService } from '../../../../services/goals.service';
import { GoalsModel } from './goals.models';
import { ActionsCollection } from '../../../../services/actionservice';
import { ChosenOptionModel, ChosenOptionGroupModel } from '../../../shared/components/chosen/chosen.models';
import { Configuration } from '../../../../services/configuration';

@Component({
    templateUrl: './add-goal-modal.html',
    styleUrls: ['./goals.component.css'],
    selector: 'add-goal-modal'
})

export class AddGoalModalComponent implements OnInit, OnDestroy {
    //flags
    public typeChosen: boolean;
    public actionChosen: boolean;
    public dealsWithStatusChosen: boolean;
    public dealsWithValue: boolean;

    //savemodel
    public goal: GoalsModel;

    //html
    public columns: Array<any>;

    //dropdowns
    public goalsTypes: ChosenOption[] = [];
    public actionTypes: ChosenOption[] = [];
    public dealsStatuses: ChosenOption[] = [];
    public timeIntervals: ChosenOption[] = [];
    public labels: ChosenOption[] = [];
    public saleProcesses: ChosenOption[] = [];
    public actionStatusList: ChosenOption[] = [];
    public currencies: ChosenOption[] = [];
    public statusGroups: ChosenOptionGroupModel[] = [];

    //subs
    private orgSub: Subscription;
    private goalSub: Subscription;
    private labelSub: Subscription;

    private currentOrg: any;
    private salesProcessId: string; // placeholder if only one salesProcess

    constructor(public goalsService: GoalsService,
        private translateService: TranslateService,
        private notify: NotificationService,
        private actionSvc: ActionsCollection,
        private orgSvc: OrgService,
        private config: Configuration) {
    }

    ngOnInit() {
        this.goalSub = this.goalsService.goalObservable()
            .subscribe(goal => {
                this.goal = goal;
            });

        this.goalsTypes = this.goalsService.getGoalTypes();

        this.actionTypes = this.actionSvc.getActionTypes();

        this.orgSub = this.orgSvc.currentOrganisationObservable()
            .subscribe(org => {
                if (org) {
                    this.currentOrg = org;
                    if (org.Settings.SaleProcesses) {
                        this.saleProcesses = org.Settings.SaleProcesses
                            .map((x: any) => new ChosenOptionModel(x.Id, x.Name));
                        if (this.saleProcesses.length === 1) {
                            this.goal.SalesProcessId = this.saleProcesses[0].value.toString();
                            this.populateDealStatusChosen(org.Settings.SaleProcesses[0]);
                        }
                    }
                };
            });

        this.timeIntervals = this.goalsService.getTimeIntervals();

        this.labelSub = this.goalsService.getLabels()
            .subscribe((data: ChosenOption[]) => this.labels = data);

        this.currencies.push(...this.config.currencies.map(x => new ChosenOptionModel(x, x)));

        this.statusGroups.push(new ChosenOptionGroupModel(1, "SALESPIPE"));
        this.statusGroups.push(new ChosenOptionGroupModel(2, "POSTSALES"));
        this.statusGroups.push(new ChosenOptionGroupModel(3, "LOSTDEALS"));
        this.statusGroups.push(new ChosenOptionGroupModel(4, "CLOSED"));
    }

    ngOnDestroy() {
        if (this.orgSub) this.orgSub.unsubscribe();
        if (this.labelSub) this.labelSub.unsubscribe();
        if (this.goalSub) this.goalSub.unsubscribe();
    }

    createGoal() {
        this.goalsService.createGoal(this.goal)
            .subscribe((res: boolean) => {
                if (res) {
                    this.notify.success(this.translateService.instant('CREATEGOALSUCCESS'));
                } else {
                    this.notify.error(this.translateService.instant('CREATEGOALFAILED'));
                }
            }, (err: any) => {
                this.notify.error(this.translateService.instant('CREATEGOALFAILED'))
            });
    }

    onTypeChanged(value: any) {
        if (value) {
            this.typeChosen = true;
            this.dealsWithValue = false;
            this.resetGoal();
            switch (this.goal.SalesGoalType.toLowerCase()) {
                case 'createdactions':
                case 'closedactions':
                    this.actionChosen = true;
                    this.dealsWithStatusChosen = false;
                    break;
                case 'numberofdeals':
                    this.dealsWithStatusChosen = true;
                    this.actionChosen = false;
                    break;
                case 'dealsvalueof':
                    this.dealsWithStatusChosen = true;
                    this.actionChosen = false;
                    this.dealsWithValue = true;
                    this.goal.Currency = this.currentOrg.CurrencyName;
                    break;
                case 'createddeals':
                default:
                    this.dealsWithStatusChosen = false;
                    this.actionChosen = false;
            }
        } else {
            this.typeChosen = false;
        }
    }

    private resetGoal() {
        this.goal = new GoalsModel({ Name: this.goal.Name, Description: this.goal.Description, Interval: this.goal.Interval, SalesGoalType: this.goal.SalesGoalType })
        if (this.salesProcessId && (this.goal.SalesGoalType === 'DealsValueOf' || this.goal.SalesGoalType === 'NumberOfDeals')) {
            this.goal.SalesProcessId = this.salesProcessId;
        }
    }

    onActionTypeChanged(actionTypes: Array<string>) {
        this.goal.ActionTypes = actionTypes;
        this.actionStatusList = [];
        this.actionSvc.getActionTypesAndStatuses(this.goal.SalesGoalType === 'ClosedActions').reduce((res, curr) => {
            if (this.goal.ActionTypes.indexOf(curr.value) > -1) {
                this.actionStatusList.push(...curr.statuses);
            }
            return res;
        }, []);
        this.actionStatusList = this.actionStatusList.filter((x, i, arr) => arr.indexOf(x) === i);
        // to make the chosen bind properly
        this.goal.ActionStatuses = this.actionStatusList.filter(x => this.goal.ActionStatuses.indexOf(x.value.toString()) > -1).map(x => x.value.toString());
        this.goal.ActionStatuses = this.goal.ActionStatuses.slice();
    }

    onSaleProcessChange(value: any) {
        const saleProcess = this.currentOrg.Settings.SaleProcesses.find((x: any) => x.Id === value)
        this.populateDealStatusChosen(saleProcess);
    }

    chooseAll(type: string) {
        switch (type) {
            case 'ActionTypes':
                this.goal.ActionTypes = this.actionTypes.map(x => x.value.toString());
                if (this.goal.SalesGoalType === 'ClosedActions') {
                    this.onActionTypeChanged(this.goal.ActionTypes)
                }
                break;
            case 'ActionStatuses':
                this.goal.ActionStatuses = this.actionStatusList.map(x => x.value.toString());
                break;
        }
    }

    private populateDealStatusChosen(saleProcess: any) {
        this.dealsStatuses = saleProcess.DealStatuses.sort((x: any, y: any) => x.SortOrder - y.SortOrder).map((x: any) => new ChosenOptionModel(x.Id, x.Name, x.Type));
    }

    back() {
        this.goalsService.closeModal();
    }

    clickToClose(event: any) {
        var target = event.target || event.srcElement;

        if (target.id === 'addGoals') {
            this.back();
        }
    }
}