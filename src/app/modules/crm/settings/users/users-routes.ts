import { Routes } from '@angular/router';

// Components
import { UsersComponent } from './users.component';
import { VoidComponent } from '../../../shared/components/void';
import { AddUserComponent } from './add/add-user.component';

export const UserRoutes: Routes = [
    {
        path: 'Users',
        component: UsersComponent,
        children: [
            { path: '', component: VoidComponent },
            { path: 'Add', component: AddUserComponent },
            { path: 'Edit/:userId', component: AddUserComponent },
            { path: 'EditInvited/:inviteId', component: AddUserComponent }
        ]
    }
];