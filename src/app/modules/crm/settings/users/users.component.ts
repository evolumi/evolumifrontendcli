import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { UsersService } from '../../../../services/users.service';
import { AllUsersModel, ChangeManyUserSettingsModel } from '../../../../models/user.models';
import { PermissionService } from '../../../../services/permissionService';
import { LocalStorage } from '../../../../services/localstorage';
import { ConfirmationService } from '../../../../services/confirmation.service';
import { Subscription } from 'rxjs/Subscription';
import { OrgService } from "../../../../services/organisation.service";
import { EditUserRoleModel } from "./add/edit-user-role.component";

export class UserSettingsModel {
    public Active: boolean = false;
    public Inactive: boolean = false;
    public Invited: boolean = false;

    [key: string]: any;
}
@Component({

    selector: 'usercomponent',
    templateUrl: './users.html',
})
export class UsersComponent {
    public columns: any;
    public loggedInUserId: string = '';
    public users: Array<AllUsersModel> = [];
    public invitedUsers: Array<AllUsersModel> = [];
    public activeUsers: Array<AllUsersModel> = [];
    public inactiveUsers: Array<AllUsersModel> = [];
    public salesCommissionUsers: Array<string> = [];

    public anyUserChecked: UserSettingsModel = new UserSettingsModel();
    public checkAll: UserSettingsModel = new UserSettingsModel();
    // public checkAllInvite: boolean;
    public openAssignSalesCommission: boolean;
    public openDelete: boolean;
    private userSub: Subscription;
    private invitedSub: Subscription;
    private permissionSub: Subscription;

    public editUser: EditUserRoleModel;

    constructor(public userService: UsersService,
        public _local: LocalStorage,
        private permissions: PermissionService,
        private translate: TranslateService,
        private confSvc: ConfirmationService,
        public orgSvc: OrgService
    ) {

        this.columns = [
            { 'name': 'FullName', 'label': 'NAME', 'Link': true },
            { 'name': 'Email', 'label': 'EMAIL' },
            { 'name': 'OrganisationRole', 'label': 'ROLE' },
            { 'name': 'Actions', 'label': '', 'Act': true },
        ];
        this.userSub = this.userService.usersObservable().subscribe(users => {
            this.users = users;
            this.separateUsers();
        });
        this.invitedSub = this.userService.invitedUsersObservable().subscribe(users => {
            this.invitedUsers = [];
            for (let u of users) {
                this.invitedUsers.push(u);
            }
        });
    }
    ngOnInit() {
        this.permissionSub = this.permissions.IsLoaded().subscribe(res => {
            if (res) {
                if (!this.permissions.handleUsers) {
                    this.permissions.RedirectForbidden();
                }
            }
        });
        this.loggedInUserId = this._local.get('userId');
        this.userService.getInvitedUsers();
        this.userService.getAllUsers();
    }

    ngOnDestroy() {
        if (this.userSub) this.userSub.unsubscribe();
        if (this.invitedSub) this.invitedSub.unsubscribe();
        if (this.permissionSub) this.permissionSub.unsubscribe();
    }

    separateUsers() {
        let arrays = this.users.reduce((res: any, curr: AllUsersModel) => {
            if (!res['Active']) {
                res['Active'] = [];
                res['Inactive'] = [];
            }
            if (curr.OrganisationStatus == 'Active' || curr.OrganisationStatus === 'Inactive') {
                res[curr.OrganisationStatus].push(curr);
            }
            return res;
            // res[curr.OrganisationStatus].push(curr);
            // return res;
        }, {});
        this.activeUsers = arrays['Active'];
        this.inactiveUsers = arrays['Inactive'];
    }

    delete(userId: any) {
        var conf = confirm(this.translate.instant("WARNDELETEUSER"));
        if (conf) {
            this.userService.deleteUser(userId);
        }
    }

    changeStatusUser(userId: any, status: any) {
        const boxTitle = status === 'Active' ? this.translate.instant('ACTIVATEUSER') : this.translate.instant('DEACTIVATEUSER');
        const boxMessage = status === 'Active' ? this.translate.instant('WARNACTIVATEUSER') : this.translate.instant('WARNDEACTIVATEUSER');
        this.confSvc.showModal(boxTitle, boxMessage, () => {
            this.userService.changeStatus(status, userId);
        });
        this.setCheckAll(false);
    }

    private changeStatusMany(status: string) {
        const orgId = this._local.get('orgId');
        let boxTitle: string, boxMessage: string;
        let obj = new ChangeManyUserSettingsModel({ OrganisationId: orgId, SelectAll: this.checkAll[status], Status: status });

        switch (status) {
            case 'Active':
                boxTitle = this.translate.instant('DEACTIVATEUSERS');
                obj.ChangeToStatus = 'Inactive';
                obj.UserIds = this.activeUsers.filter(x => x.IsChecked && x.Id !== this.loggedInUserId).map(x => x.Id);
                boxMessage = this.checkAll[status] ? this.translate.instant('WARNDEACTIVATEALLUSERS') : this.translate.instant('WARNDEACTIVATEUSERS', { userCount: obj.UserIds.length });
                break;
            case 'Inactive':
                boxTitle = this.translate.instant('ACTIVATEUSERS');
                obj.ChangeToStatus = 'Active';
                obj.UserIds = this.inactiveUsers.filter(x => x.IsChecked).map(x => x.Id);
                boxMessage = this.checkAll[status] ? this.translate.instant('WARNACTIVATEALLUSERS') : this.translate.instant('WARNACTIVATEUSERS', { userCount: obj.UserIds.length });
                break;
        }

        this.confSvc.showModal(boxTitle, boxMessage, () => {
            this.userService.changeStatusMany(obj);
        });

        this.setCheckAll(false);
    }

    deleteInvitedUser(userId: any) {
        var conf = confirm(this.translate.instant("WARNDELETEUSER"));
        if (conf) {
            this.userService.deleteInvitedUser(userId);
        }
    }

    onCheckAll(event: boolean, type: string) {
        this.checkAll[type] = event;
        switch (type) {
            case 'Active':
                this.activeUsers = this.activeUsers.map(x => {
                    x.IsChecked = event;
                    return x;
                });
                break;
            case 'Inactive':
                this.inactiveUsers = this.inactiveUsers.map(x => {
                    x.IsChecked = event;
                    return x;
                });
                break;
            case 'Invited':
                this.invitedUsers = this.invitedUsers.map(x => {
                    x.IsChecked = event;
                    return x;
                });
                break;
        }
        this.anyUserChecked[type] = event;
    }

    onCheck(check: boolean, id: string, type: string) {
        let user: AllUsersModel;
        switch (type) {
            case 'Active':
                user = this.activeUsers.find(x => x.Id === id);
                user.IsChecked = check;
                this.anyUserChecked[type] = this.activeUsers.some(x => x.IsChecked);
                break;
            case 'Inactive':
                user = this.inactiveUsers.find(x => x.Id === id);
                user.IsChecked = check;
                this.anyUserChecked[type] = this.inactiveUsers.some(x => x.IsChecked);
                break;
            case 'Invited':
                user = this.invitedUsers.find(x => x.Id === id);
                user.IsChecked = check;
                this.anyUserChecked[type] = this.invitedUsers.some(x => x.IsChecked);
                break;
        }
        if (!check) {
            this.checkAll[type] = check;
        }
    }

    onOpenModal(type: string, status: string) {
        if (this.anyUserChecked[status]) {
            switch (type) {
                case 'SalesCommission':
                    this.salesCommissionUsers = this.activeUsers.filter(x => x.IsChecked).map(x => x.Id);
                    this.openAssignSalesCommission = true;
                    break;
                case 'Delete':
                    this.deleteMany(status);
                    break;
                case 'Deactivate':
                    this.changeStatusMany(status);
                    break;
                case 'Reinvite':
                    this.reinviteMany();
                    break;
            }
        }
    }

    editClosed(save: boolean) {
        if (save) {
            this.userService.updateUserRole(this.editUser);
        }
        this.editUser = null;
    }
    openEdit(user: AllUsersModel) {
        this.editUser = new EditUserRoleModel(user);
    }

    onCloseModal(type: string) {
        switch (type) {
            case 'SalesCommission':
                this.openAssignSalesCommission = false;
                break;
        }
        this.setCheckAll(false);
    }

    private deleteMany(userStatus: string) {
        const orgId = this._local.get('orgId');
        const boxTitle = this.translate.instant('DELETEUSERS');
        let boxMessage = this.translate.instant('CONFIRMDELETEALLUSERS');
        let obj = new ChangeManyUserSettingsModel({ OrganisationId: orgId, SelectAll: this.checkAll[userStatus] });
        if (!this.checkAll[userStatus]) {
            switch (userStatus) {
                case 'Active':
                    obj.Status = 'Active';
                    obj.UserIds = this.activeUsers.filter(x => x.IsChecked).map(x => x.Id);
                    break;
                case 'Inactive':
                    obj.Status = 'Inactive';
                    obj.UserIds = this.inactiveUsers.filter(x => x.IsChecked).map(x => x.Id);
                    break;
                case 'Invited':
                    obj.UserIds = this.invitedUsers.filter(x => x.IsChecked).map(x => x.Id);
                    break;
            }
            boxMessage = this.translate.instant('CONFIRMDELETEUSERS', { userCount: obj.UserIds.length });
        }

        this.confSvc.showModal(boxTitle, boxMessage, () => {
            if (userStatus === 'Active' || userStatus === 'Inactive') {
                this.userService.deleteMany(obj);
            } else if (userStatus === 'Invited') {
                this.userService.deleteInvitedUserMany(obj)
            }
        });
        this.setCheckAll(false);
    }

    private setCheckAll(value: boolean) {
        for (let key in this.checkAll) {
            this.checkAll[key] = value;
        }
        for (let key in this.anyUserChecked) {
            this.anyUserChecked[key] = value;
        }
        this.activeUsers = this.activeUsers.map(x => {
            x.IsChecked = value;
            return x;
        });
        this.inactiveUsers = this.inactiveUsers.map(x => {
            x.IsChecked = value;
            return x;
        });
        this.invitedUsers = this.invitedUsers.map(x => {
            x.IsChecked = value;
            return x;
        });
    }

    public reinviteUser(id: string) {
        this.userService.reInviteUser(id);
    }

    private reinviteMany() {
        const orgId = this._local.get('orgId');
        let obj = new ChangeManyUserSettingsModel({ OrganisationId: orgId, SelectAll: this.checkAll['Invited'] });
        obj.UserIds = this.invitedUsers.filter(x => x.IsChecked).map(x => x.Id);
        this.userService.reinviteMany(obj);
        this.setCheckAll(false);
    }
}