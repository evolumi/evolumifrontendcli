import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

import { NotificationService } from '../../../../../services/notification-service';
import { LocalStorage } from '../../../../../services/localstorage';
import { UsersService } from '../../../../../services/users.service';
import { OrgService } from '../../../../../services/organisation.service';
import { ChosenOptionModel } from '../../../../shared/components/chosen/chosen.models';

@Component({
    templateUrl: './add-user.html',
    selector: "invite-user-modal"
})
export class AddUserComponent {
    private sub: any;
    private orgSub: any;

    public invite: boolean = false;
    public step1Form: FormGroup;
    public orgId: string;
    public type: string;
    public form: any;
    public Email = '';
    public rolesArray: Array<any> = [];
    public selectedRole: string;
    public userId: string = '';

    public usersToInvite: any[] = [];
    private defaultRole: string;
    private OrgName: string;
    constructor(
        public _router: Router,
        form: FormBuilder,
        public _notify: NotificationService,
        private translate: TranslateService,
        public _localStorage: LocalStorage,
        public route: ActivatedRoute,
        public _userService: UsersService,
        public orgService: OrgService) {


        this.sub = route.params.subscribe(params => {
            if (params['userId']) {
                this.userId = params['userId'];
                this._userService.getUserById(this.userId);
            } else if (params['inviteId']) {
                this.invite = true;
                this.userId = params['inviteId'];
                console.log("Invite", this.userId);
                this._userService.getInvitedUser(this.userId);
            } else {
                this._userService.createUserModel();
            }
        });
    }

    ngOnInit() {
        this.orgSub = this.orgService.currentOrganisationObservable().subscribe(org => {
            if (org) {
                this.OrgName = org.OrganisationName;
                this.rolesArray = [];
                this.defaultRole = org.DefaultRole;
                console.log(org);

                for (var index of org.Roles) {
                    let role = new ChosenOptionModel(index.Id, index.Name);
                    this.rolesArray.push(role);
                }
                if (!this._userService.user.RoleId && org.Roles.length > 0) {
                    this._userService.user.RoleId = this.rolesArray[0].value;
                }
                this.usersToInvite = [];
                this.addRow();
            }
        });
    }

    addRow() {
        this.usersToInvite.push({ FirstName: "", LastName: "", Email: "", RoleId: this.defaultRole })
    }

    deleteRow(i: number) {
        this.usersToInvite.splice(i, 1);
    }

    inviteUsers() {
        for (let i = 0; i < this.usersToInvite.length; i++) {
            let user = this.usersToInvite[i];
            if (!user.FirstName || !user.LastName || !user.RoleId) {
                console.log("nameorroleerrer");
                continue;
            }
            if (!user.Email.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)) {
                console.log("useremailerror");
                continue;
            }
            this._userService.inviteUser(user);
            this.usersToInvite.splice(i, 1);
        }
        this.orgService.inviteToOrganisation = null;
    }

    Create() {
        this.orgId = this._localStorage.get('orgId');
        if (this.step1Form.valid && this._userService.user.RoleId) {
            var data = { 'RoleId': this._userService.user.RoleId, 'FirstName': this._userService.user.FirstName, 'LastName': this._userService.user.LastName, 'Email': this._userService.user.Email, };
            this._userService.inviteUser(data);
            this.back();
        } else {
            this._notify.error(this.translate.instant("WARNCOMPLETEFORM"));
        }
    }

    Update() {
        if (this.invite) {
            let data = { 'RoleId': this._userService.user.RoleId, 'FirstName': this._userService.user.FirstName, 'LastName': this._userService.user.LastName, 'Email': this._userService.user.Email };
            this._userService.updateInvite(this.userId, data);
        }
        else {
            let data = { 'UserId': this._userService.user.Id, 'RoleId': this._userService.user.RoleId };
            this._userService.updateUserRole(data);
        }
        this.back();
    }

    back() {
        this._router.navigate(['/CRM/Settings/Users']);
    }

    clickToClose(event: any) {
        var target = event.target || event.srcElement;
        if (target.id === 'addUser') {
            this.close();
        }
    }

    close() {
        this.usersToInvite = [];
        this.addRow();
        this.orgService.toggleInviteUserPopup();
    }

    ngOnDestroy() {
        if (this.sub) this.sub.unsubscribe();
        if (this.orgSub) this.orgSub.unsubscribe();
    }

}
