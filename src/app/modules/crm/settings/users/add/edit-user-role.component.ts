import { Component, Input, Output, EventEmitter } from '@angular/core';
import { OrgService } from "../../../../../services/organisation.service";
import { Subscription } from "rxjs/Subscription";
import { ChosenOptionModel } from "../../../../shared/components/chosen/chosen.models";

@Component({
    selector: 'edit-user-role',
    templateUrl: './edit-user-role.html'
})

export class EditUserRoleComponent {
    @Input() user: EditUserRoleModel;
    @Output() close = new EventEmitter<boolean>();

    private orgSub: Subscription;
    private roles: ChosenOptionModel[] = [];

    constructor(
        private orgSvc: OrgService
    ) { }

    ngOnInit() {
        this.orgSub = this.orgSvc.currentOrganisationObservable().subscribe(res => {
            this.roles = res.Roles.map(x => new ChosenOptionModel(x.Id, x.Name));
        });
    }

    ngOnDestroy(){
        if (this.orgSub) this.orgSub.unsubscribe();
    }

    changeRole(roleId: any) {
        let role = this.roles.find(x => x.value === roleId);
        this.user.RoleId = role.value as string;
    }

    closeModal(save: boolean) {
        this.close.emit(save);
    }
    clickToClose(e: any) {
        if (e.target.id == "editUserRole") {
            this.closeModal(false);
        }
    }
}

export class EditUserRoleModel {
    public UserId: string;
    public RoleId: string;


    constructor(obj: any) {
        this.UserId = obj.Id;
        this.RoleId = obj.RoleId;
    }
}