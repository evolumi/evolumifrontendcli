import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { OrgService } from '../../../services/organisation.service';
import { NotificationService } from '../../../services/notification-service';
import { Api } from '../../../services/api';

import { ChosenOption } from "../../shared/components/chosen/chosen";
import { ChosenOptionModel } from '../../shared/components/chosen/chosen.models';

@Component({
    templateUrl: './pay.html',
})

export class PayComponent {

    private sub: any;
    private orgsSub: any;
    public orgId: string;
    public amount: string;
    public currency: string;

    public cardSelected: boolean;

    public organisations: Array<ChosenOption> = [];

    constructor(public orgService: OrgService, private router: Router, private route: ActivatedRoute, private api: Api, private notify: NotificationService
    ) {
        this.sub = this.route
            .params
            .subscribe(params => {
                this.amount = params['amount'];
                this.currency = params['currency'];
            });

        this.orgsSub = orgService.organisationsObservable().subscribe(orgs => {
            if (orgs && orgs.length) {
                this.organisations = [];
                console.log("[Pay] Loaded Orgs -> " + orgs.length);
                for (let org of orgs) {
                    this.organisations.push(new ChosenOptionModel(org.Id, org.OrganisationName));
                }
                this.orgId = orgs[0].Id;
            }
        })

        this.orgService.getOrganisations();
    }

    public getPaymentData(orgId: string) {
        this.orgId = orgId;
    }


    public pay() {
        var data = { amount: this.amount, currency: this.currency };
        this.api.requestPost("Organisations/{" + this.orgId + "}/Pay", data, res => {

            this.notify.success("Payment successfull");
            this.router.navigate(["CRM/Overview"]);

        }, err => {
            this.notify.error("Payment failed" + err);
        })
    }

    ngOnInit() {
    }

    ngOnDestroy() {
        if (this.sub) this.sub.unsubscribe();
        if (this.orgsSub) this.orgsSub.unsubscribe();
    }

}
