import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { SignalRService } from '../../services/signalRService';
import { DealAddEdit } from "../../services/dealAddEdit.service";
import { ChatService } from "../../services/chatService";
import { MapService } from "../../services/map.service";

@Component({
    templateUrl: './crm.html',
})

export class CRMComponent {
    constructor(public translate: TranslateService,
        public _signalRService: SignalRService,
        public _deal: DealAddEdit,
        public chat: ChatService, // Loading chat-service just to get the constructor going. (shows new messages)
        public _map: MapService) { }

    ngOnInit() {

    }
}
