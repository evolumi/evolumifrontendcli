import { Api } from '../../../services/api';
import { Observable } from 'rxjs/Observable';
import { NotificationService } from '../../../services/notification-service';
import { LocalStorage } from '../../../services/localstorage';
import { Injectable } from "@angular/core";

// This dont seeems to be used!
@Injectable()
export class DashboardService {
	public orgId: string;
	public news$: Observable<Array<string>>;
	private _newsObserver: any;
	private _news: Array<string>;
	constructor(public _http: Api,
		public _notify: NotificationService,
		public _localstore: LocalStorage
	) {
		this._news = new Array;
		this.news$ = new Observable<Array<string>>((observer: any) => {
			this._newsObserver = observer;
		}).share();
		this.orgId = _localstore.get('orgId');
	}
	getDashboard() {
		this._http.get(this.orgId + '/News')
			.map(res => res.json())
			.subscribe(res => {
				this._news.push(res.Data);
				this._newsObserver.next(this._news);
			}, err => {
				this._notify.error('error updating');
			});
	}
}


