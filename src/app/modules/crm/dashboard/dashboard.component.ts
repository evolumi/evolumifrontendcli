import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { LoaderService } from '../../../services/loader.service';
import { UsersService } from '../../../services/users.service';
import { OrgService } from '../../../services/organisation.service';
import { NewsfeedModel } from '../../../models/newsfeed.models';
import { ChosenOption } from "../../shared/components/chosen/chosen";
import { ChosenOptionModel } from '../../shared/components/chosen/chosen.models';
import { PermissionService } from '../../../services/permissionService';
import { HighchartService } from '../../../services/highchart.service';

declare var $: JQueryStatic;

@Component({
    templateUrl: './dashboard.html',
    styleUrls: ["dashboard.component.css"],
})
export class DashboardComponent {
    public newsFilterForm: any;
    public Type: string = 'All Activity';
    public UserId: string = 'All';
    public organisationName: string;

    public selectedNewsType: NewsfeedModel = new NewsfeedModel({ Type: 'All Activity', UserId: '' });
    public activityArray: Array<ChosenOption> = [];
    public accOwner: string = 'All';
    public list: Array<any> = [];
    public insights: Array<any> = [];
    public triggerSroll: number;
    public users: Array<ChosenOption> = [];
    public isGuestUser: boolean = false;
    private hasLoaded: boolean;

    private userSub: any;
    private orgSub: any;

    constructor(private chartService: HighchartService, private orgService: OrgService, public _load: LoaderService, private translate: TranslateService, private userService: UsersService, public permissions: PermissionService) {
        this.activityArray = [
            new ChosenOptionModel("All Activity", this.translate.instant("ALLACTIVITY")),
            new ChosenOptionModel("Account", this.translate.instant("ACCOUNT")),
            new ChosenOptionModel("Deal", this.translate.instant("DEALS")),
            new ChosenOptionModel("Contact", this.translate.instant("CONTACT")),
            new ChosenOptionModel("Note", this.translate.instant("NOTE"))
        ];
    }
    ngOnInit() {

        this.userSub = this.userService.activeUserListObervable().subscribe(users => {
            this.users = [new ChosenOptionModel("All", this.translate.instant("ALLUSERS"))];
            if (users) {
                for (var u of users)
                    this.users.push(u);
            }
        });

        this.orgSub = this.orgService.currentOrganisationObservable().subscribe(org => {
            if (org) {
                if (!this.hasLoaded) {
                    this.getInsights(org.FavoriteInsights);
                    this.hasLoaded = true;
                }
                this.organisationName = org.OrganisationName;
            }
        });
    }

    private getInsights(insights: Array<any>) {
        this.insights = [];
        if (insights) {
            for (let insight of insights) {
                this.chartService.getSingleChart(insight.DocumentType, insight.InsightId, insight.Filter).subscribe((res: any) => {
                    if (res.IsSuccess) {
                        if (res.Data.Charts[0].chart.type === 'solidgauge') {
                            this.chartService.positionGaugeLabel(res.Data.Charts);
                        }
                        insight.options = res.Data.Charts[0];
                        this.insights.push(insight);
                    }
                });
            }
        }
    }

    public deleteInsight(id: string) {
        if (confirm(this.translate.instant("CONFIRMUNPIN"))) {
            this.chartService.unPin(id).subscribe(res => {
                if (res.IsSuccess) {
                    this.insights.splice(this.insights.findIndex(x => x.Id == id), 1);
                }
            });
        }
    }

    visitingSite() {
        var data = {
            Ip: "",
            Url: window.location.href,
            OrgKey: "aac51aee-678d-41e9-bfb8-59776bdcf79a"
        };

        var ipRequest = new XMLHttpRequest();
        ipRequest.onload = function () {
            data.Ip = ipRequest.responseText;
            sendData();
        }
        ipRequest.open("GET", "https://api.ipify.org/?format=string", true)
        ipRequest.setRequestHeader("Content-Type", "application/json;charest=UTF-8");
        ipRequest.send();

        var sendData = function () {
            var postData = JSON.stringify(data);
            var request = new XMLHttpRequest();
            request.open("POST", "http://evolumidev.azurewebsites.net/api/Sitevisitor/Visiting", true);
            request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            request.send(postData);
        }
    }

    updateNews() {
        this.selectedNewsType = new NewsfeedModel({ UserId: this.UserId, Type: this.Type });
    }
    ngOnDestroy() {
        this._load.clear();
        if (this.orgSub) this.orgSub.unsubscribe();
        if (this.userSub) this.userSub.unsubscribe();
    }
    newsScroll() {
        this.triggerSroll = new Date().getTime();
    }
    toggleFiterbar() {
        $('.filterBox').hasClass('visible') ? $('.filterBox').removeClass('visible') : $('.filterBox').addClass('visible');
    }
    toggleRightPanel() {
        $('.rightPanel').hasClass('systemActive') ? $('.rightPanel').removeClass('systemActive') : $('.rightPanel').addClass('systemActive');
        $('.rightPanelControl').hasClass('systemActive') ? $('.rightPanelControl').removeClass('systemActive') : $('.rightPanelControl').addClass('systemActive');
    }
}
