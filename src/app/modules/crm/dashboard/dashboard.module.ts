import { NgModule } from '@angular/core';

import { SharedModule } from '../../shared/shared.module';
import { ActionsSharedModule } from '../actions/shared/action-shared.module';
import { DashboardRoutingModule } from './dashboard-routing.module';

// Components
import { DashboardComponent } from './dashboard.component';

// Services
import { DashboardService } from './dashboard.service';

@NgModule({
    imports: [
        SharedModule,
        ActionsSharedModule,
        DashboardRoutingModule
    ],
    declarations: [
        DashboardComponent
    ],
    providers: [
        DashboardService
    ],
    exports: [
        DashboardComponent
    ]
})
export class DashboardModule { }