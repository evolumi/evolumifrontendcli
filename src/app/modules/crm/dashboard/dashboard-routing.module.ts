import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { OrgGuard } from '../../../services/guards/org-guard';

// Components
import { DashboardComponent } from './dashboard.component';
import { VoidComponent } from "../../shared/components/void";

export const DashboardRoutes: Routes = [
    {
        path: 'Dashboard',
        component: DashboardComponent,
        canActivate: [OrgGuard],
        children: [
            { path: '', component: VoidComponent }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(DashboardRoutes)
    ],
    exports: [RouterModule]
})
export class DashboardRoutingModule { }