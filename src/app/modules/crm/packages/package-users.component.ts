import { Component } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { Package, PackageSummary, PackagesCollection } from '../../../services/packages.service';
import { UsersService } from '../../../services/users.service';
import { OrgService } from '../../../services/organisation.service';

@Component({
    selector: 'package-users-selector',
    templateUrl: 'package-users.html',
    styleUrls: ['packages.css']
})
export class PackageUsersComponent {

    private avtivePackage: any;
    public choosenPackageWithUser: Package;
    public packageArray: Array<Package> = [];;
    public packageSub: Subscription;
    public activatePackage: boolean;
    public userList: Array<any> = [];
    public notAddedUsers: Array<any> = [];
    private userSub: Subscription;
    public selectedUsersArray: Array<any> = [];
    public activatedShowAvaUsers: boolean = false;
    public showButton: boolean = true;
    public packageUsers: Subscription;
    public orgSub: Subscription;
    public alreadyChoosenPackage: Array<any> = [];
    public orgPackages: Array<any>;
    public orgHasPackage: boolean = false;
    public orgPayment: any;
    public packageLicences: number;
    public showNewMontlyPayment: number;
    public showNewYearlyPayment: number;
    public showOldMontlyPayment: number;
    public showOldYearlyPayment: number;

    private full: boolean;
    private summary: PackageSummary;

    constructor(
        public _packageService: PackagesCollection,
        private userService: UsersService,
        public orgService: OrgService) {
    }

    ngOnInit() {
        this.userSub = this.userService.activeUserListObervable().subscribe(data => {

            this.userList = data;            
            this.updateUsers();
        });

        this.packageSub = this._packageService.packages().subscribe(result => {
            this.packageArray = result;
        })

        this.packageUsers = this._packageService.selectedPackageObservable().subscribe(data => {
            this.choosenPackageWithUser = data;
            this.ifPackageHaveUsers();
            this.checkIfOrgHasPackage();

            if (this.orgHasPackage) {
                this.getUpdateCosts();
            }
            else {
                // Skip this step if no licences and no updatePrice information is avalible.
                if (this._packageService.buildingPackage.Licences == 0) {
                    this.sendPackageToCart(this.choosenPackageWithUser);
                }
            }
        })

        this.orgSub = this.orgService.currentOrganisationObservable().subscribe(org => {
            if (org) {
                this.orgPackages = org.Packages;
                this.orgPayment = org.PaymentData;
            }
        })

    }
    public showAvaUsers() {
        this.activatedShowAvaUsers = !this.activatedShowAvaUsers;
    }

    public showAvaUsersClose(event: any) {
        var target = event.target || event.srcElement;
        if (target.id === 'packageSettings') {
            this.activatedShowAvaUsers = false;
        }
    }

    public getUpdateCosts() {

        let montlyPrice = this.choosenPackageWithUser.MonthlyPrices[this._packageService.Currency] || 0;
        let yearlyPrice = this.choosenPackageWithUser.YearlyPrices[this._packageService.Currency] || 0;

        var volumePrice = this._packageService.calcVolumeCost(this._packageService.buildingPackage);

        this._packageService.getUpdatePrice().subscribe(summary => {
            if (summary) {
                this.summary = summary;
            }
        });

        this.showNewMontlyPayment = Math.round(montlyPrice * this._packageService.buildingPackage.Licences) || volumePrice;

        this.showNewYearlyPayment = Math.round(yearlyPrice * this._packageService.buildingPackage.Licences);

        this.showOldMontlyPayment = this._packageService.buildingPackage.Trial ? 0 : Math.round(montlyPrice * this.packageLicences);

        this.showOldYearlyPayment = this._packageService.buildingPackage.Trial ? 0 : Math.round(yearlyPrice * this.packageLicences);

    }

    public removeUser(index: number) {
        this.selectedUsersArray.splice(index, 1);
        this.checkLicenceCount();
        this.updateUsers();
    }

    public updateUsers() {
        this.selectedUsersArray = this.selectedUsersArray.filter(x => x != undefined);

        console.log("userlist", this.userList);        
        
        this.notAddedUsers = this.userList.filter((owner) => {
            var found = this.selectedUsersArray.findIndex(x => x.value == owner.value) != -1;
            console.log("found: " + found);
            return !found;
        })
        console.log("userlist avalibe", this.notAddedUsers);
        console.log("userlist selected", this.selectedUsersArray);
    }

    public addUser(user: any) {
        if (!this.full) {
            this.selectedUsersArray.push(user);
            this.checkLicenceCount();
            this.updateUsers();
        }
    }

    private checkLicenceCount() {
        this.full = this.selectedUsersArray.length >= this._packageService.buildingPackage.Licences;
    }

    public ifPackageHaveUsers() {
        if (this._packageService.cart && this._packageService.cart.find(x => x.choosenPackage.NameLabel == this.choosenPackageWithUser.NameLabel)) {
            let editingPackage = this._packageService.cart.find(x => x.choosenPackage.NameLabel == this.choosenPackageWithUser.NameLabel)
            for (let user of editingPackage.UserLicenses) {
                let findUsersInfo = this.userList.find(x => x.value == user);
                this.selectedUsersArray.push(findUsersInfo);
            }
            if (this.selectedUsersArray.length >= this._packageService.buildingPackage.Licences) {
                this.showButton = false;
            }
        }
        let editingCurrentOwnPackage = this._packageService.orgPackages.find(x => x.PackageId == this.choosenPackageWithUser.Id)
        if (editingCurrentOwnPackage) {
            this.packageLicences = editingCurrentOwnPackage.Licences;
            for (let user of editingCurrentOwnPackage.UserLicenses) {
                let findUsersInfo = this.userList.find(x => x.value == user);
                this.selectedUsersArray.push(findUsersInfo);
            }
            if (this.selectedUsersArray.length >= this._packageService.buildingPackage.Licences) {
                this.showButton = false;
            }
        }
        this.updateUsers();
    }

    public removePackgeFromList(item: Package) {
        if (this._packageService.packagesToShow.find(x => x.NameLabel == item.NameLabel)) {
            let index = this._packageService.packagesToShow.findIndex(x => x == item);
            let test = this._packageService.packagesToShow.splice(index, 1);
            this.alreadyChoosenPackage.push(test);
        }
    }
    public updateAPackage(item: Package) {
        this._packageService.buildingPackage.choosenPackage = item;
        this._packageService.buildingPackage.PackageId = item.Id;
        console.log("userlist adding selected users to package", this.selectedUsersArray);
        this._packageService.buildingPackage.UserLicenses = [];
        for(let user of this.selectedUsersArray){
            this._packageService.buildingPackage.UserLicenses.push(user.value);
        }
        this._packageService.buildingPackage.Currency = this._packageService.Currency;
        this._packageService.updatePackagesBackend();
        this._packageService.updatePackages(item.Id);
        this.closeSelectedPackageWithUser();
    }

    public checkIfOrgHasPackage() {
        let foundOrgPackages = this._packageService.orgPackages.find(x => x.PackageId == this.choosenPackageWithUser.Id);
        if (foundOrgPackages) {
            this.avtivePackage = foundOrgPackages;
            this.orgHasPackage = true;
        }
        else {
            this.orgHasPackage = false;
        }
    }

    public sendPackageToCart(choosenPackage: Package) {
        this._packageService.removingAlreadyExsistingPackages();
        this._packageService.buildingPackage.Currency = this._packageService.Currency;
        for (let users of this.selectedUsersArray) {
            this._packageService.buildingPackage.UserLicenses.push(users.value)
        }
        if (this._packageService.cart.find(x => x.choosenPackage.NameLabel == choosenPackage.NameLabel)) {
            let slice = this._packageService.cart.findIndex(x => x.choosenPackage == choosenPackage)
            this._packageService.cart.splice(slice, 1);
        }
        this._packageService.buildingPackage.choosenPackage = choosenPackage;
        this._packageService.sendPackagesBackend();
        this._packageService.checkingTotalSum();
        this.removePackgeFromList(choosenPackage);
        this.closeSelectedPackageWithUser();
    }

    public clickToCloseSelectedPackageWithUser(event: any) {
        var target = event.target || event.srcElement;
        if (target.id === 'popupActivatePackage') {
            this.closeSelectedPackageWithUser();
        }
    }

    public closeSelectedPackageWithUser() {
        this.activatePackage = false;
        this.selectedUsersArray = [];
        this.showButton = true;
        this.activatedShowAvaUsers = false;;
        this.choosenPackageWithUser = null;
    }

    ngOnDestroy() {
        if (this.userSub) this.userSub.unsubscribe();
        if (this.packageSub) this.packageSub.unsubscribe();
        if (this.packageUsers) this.packageUsers.unsubscribe();
        if (this.orgSub) this.orgSub.unsubscribe();
    }

}