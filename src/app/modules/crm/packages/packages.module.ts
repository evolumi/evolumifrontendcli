import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';

import { PackagesComponent } from './packages.component';
import { FeaturePackagesComponent } from './feature-packages.component';
import { ShowPackagesComponent, PriceListPipe } from './show-packages.component';
import { FeatureStandaloneComponent } from './feature-standalone.component';
import { ShowImageComponent } from './showImage.component';
import { PackageUsersComponent } from './package-users.component';
import { SettingsSharedModule } from '../settings/shared/settings-shared.module';
import { PackageCartComponent } from './package-cart.component';
import { PackageCardComponent } from './package-card.component';


@NgModule({
    imports: [
        SharedModule,
        SettingsSharedModule
    ],
    exports: [FeatureStandaloneComponent, ShowImageComponent, PackageUsersComponent],
    declarations: [
        PackagesComponent,
        FeaturePackagesComponent,
        ShowPackagesComponent,
        FeatureStandaloneComponent,
        ShowImageComponent,
        PackageUsersComponent,
        PackageCartComponent,
        PackageCardComponent,
        PriceListPipe
    ]
})
export class PackagesModule { }