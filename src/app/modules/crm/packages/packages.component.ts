import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Package, PackagesCollection, AddPackageModel } from '../../../services/packages.service';
import { OrgService } from '../../../services/organisation.service';
import { TranslateService } from '@ngx-translate/core';
import { ChosenOptionModel } from '../../../modules/shared/components/chosen/chosen.models';
import { Subscription } from 'rxjs/Subscription';

@Component({
    selector: 'packages-selector',
    templateUrl: 'packages.html',
    styleUrls: ['packages.css']
})
export class PackagesComponent {
    [x: string]: any;
    public selectedPackage: Package;
    public editingThisPackage: Package;

    public showingTitel: boolean;
    public currency: string;
    public orgSub: Subscription;
    public selectedCurrentOwnPackage: Package;
    public allcurrency: ChosenOptionModel[] = [];

    public featureArray: Array<any> = [];
    public orgPackages: Array<AddPackageModel>;
    public addPackageToCart: boolean = false;
    private routeSub: Subscription;
    private featureSub: Subscription;

    constructor(
        public _packageService: PackagesCollection,
        private orgService: OrgService,
        private translate: TranslateService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe(params => {
            if (params["name"]) {
                this._packageService.loadFeature(params["name"]);
            }
        })
        this._packageService.settingCurrencyIfYouhavePackage();

        this.orgSub = this.orgService.currentOrganisationObservable().subscribe(org => {
            if (org) {
                this.currency = org.CurrencyName;
                this.orgPackages = org.Packages;
                this._packageService.getPackages(true);
            }
        })

        this.featureSub = this._packageService.features().subscribe(result => {
            this.featureArray = result;
        })

        this.allcurrency.push(
            new ChosenOptionModel('SEK', 'SEK'),
            new ChosenOptionModel('USD', 'USD'),
            new ChosenOptionModel('EUR', 'EUR'),
        )
    }


    public selectPackage(item: Package) {
        if (this._packageService.packagesToShow.find(x => x.Id == item.Id)) {
            this.selectedPackage = item;
            this.showingTitel = true;
            this.addPackageToCart = true;
        }
        else if (this._packageService.ownedPackages.find(x => x.Id == item.Id)) {
            let pack = this._packageService.orgPackages.find(x => x.PackageId == item.Id)
            if (pack) {
                this._packageService.buildingPackage = JSON.parse(JSON.stringify(pack));
            }
            this.selectedPackage = item;
            this.showingTitel = true;
        }
    }

    public deleteCurrentPackage(item: any) {
        var conf = confirm(this.translate.instant("WARNDELPACKAGE"));
        if (conf) {
            this._packageService.packagesToShow.push(item.choosenPackage)
            let slice = this._packageService.cart.findIndex(x => x == item);
            this._packageService.cart.splice(slice, 1);
        }

    }

    public selectCurrentOwnPackage(item: Package) {

    }

    cancelSubscription(item: any) {
        console.log(item)
        var conf = confirm(this.translate.instant("WARNCANCELPACKAGE"));
        if (conf) {
            this._packageService.cancelPackage(item.Id);
            this.closeCurrentlyOwnPackages();
        }
    }

    clickToCloseCurrentOwnPackages(event: any) {
        var target = event.target || event.srcElement;
        if (target.id === 'showCurrentOwnPackages') {
            this.closeCurrentlyOwnPackages();
        }
    }
    public closeCurrentlyOwnPackages() {
        this.selectedCurrentOwnPackage = null;
        document.getElementById('showCurrentOwnPackages').scrollTop = 0;
    }

    public removeCart() {
        for (let item of this._packageService.cart) {
            this._packageService.packagesToShow.push(item.choosenPackage);
        }
        this._packageService.cart = [];

    }

    clickToClosePackages(event: any) {
        var target = event.target || event.srcElement;
        if (target.id === 'showPackages') {
            this.closePackages();
        }
    }

    public editCurrenctPackage(item: any) {
        this._packageService.buildingPackage.Interval = item.Interval;
        this._packageService.buildingPackage.Licences = item.Licences;
        this.editingThisPackage = item.choosenPackage;
        this.showingTitel = true;
        this.addPackageToCart = false;
    }

    public updateUsersWithPackage(item: Package) {
        this._packageService.showSelectedPackageWithUsers(item);
        this._packageService.buildingPackage.PackageId = item.Id;
        this.closeCurrentlyOwnPackages();
    }

    public clickToCloseEdit(event: any) {
        var target = event.target || event.srcElement;
        if (target.id === 'showPackageEdit') {
            this.closeEdit();
        }
    }

    public editingPackage(item: Package) {
        this._packageService.showSelectedPackageWithUsers(item);
        this._packageService.buildingPackage.PackageId = item.Id;
        this.closeEdit();
    }

    public closeEdit() {
        this.editingThisPackage = null;
        document.getElementById('showPackageEdit').scrollTop = 0;
    }

    public chooseUsersWithPackage(packageUsers: Package) {
        this._packageService.buildingPackage.PackageId = packageUsers.Id;
        this._packageService.showSelectedPackageWithUsers(packageUsers);
        this.closePackages();
    }

    public closePackages() {
        this.selectedPackage = undefined;
        document.getElementById('showPackages').scrollTop = 0;
    }

    ngOnDestroy() {
        if (this._packageService.cart) {
            this.removeCart();
        }
        if (this.orgSub) this.orgSub.unsubscribe();
        if (this.routeSub) this.routeSub.unsubscribe();
        if (this.featureSub) this.featureSub.unsubscribe();
    }
}