import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Package, PackagesCollection } from '../../../services/packages.service';
import { Subscription } from 'rxjs/Subscription';

@Component({

    selector: 'package-card',
    templateUrl: 'package-card.component.html',
    styleUrls: ['packages.css']
})

export class PackageCardComponent {

    @Input() package: Package;
    @Input() currency: string;
    @Output() select: EventEmitter<Package> = new EventEmitter();

    private volumePrice: number;
    private priceSub: Subscription;

    constructor(
        public _packageService: PackagesCollection) {
    }

    ngOnChanges(changes: any) {
        if (this.package.HasVolumePricing) {

            this.priceSub = this._packageService.getPackageLowestVolumePrice(this.package, this.currency).subscribe(res => {
                this.volumePrice = res;
            });
        }
    }

    selectPackage(pack: Package) {
        this.select.next(pack);
    }

    displayFeatureWithPackage(popupFeature: any, popupPackage: any) {
        this._packageService.packagesWithFeature.push(popupPackage);
        this._packageService.loadFeature(popupFeature);
    }

    ngOnDestroy() {
        if (this.priceSub) this.priceSub.unsubscribe();
    }
}
