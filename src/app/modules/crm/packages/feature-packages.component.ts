import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Package, Feature, PackagesCollection } from '../../../services/packages.service';
import { MediaService } from '../../../services/media.service';
import { Subscription } from "rxjs/Subscription";

@Component({
    selector: 'feature-packages-selector',
    templateUrl: 'feature-packages.html',
    styleUrls: ['packages.css']
})

export class FeaturePackagesComponent {

    public package: Package;
    public feature: Feature;
    public featureArray: Array<Feature> = [];
    @Input() name: string;
    @Input() clickabelName: boolean;
    @Input() displayOnlyName: boolean;
    @Input() showPrices: boolean = false;
    @Input() compact: boolean = false;
    @Output() selectPrice: EventEmitter<number> = new EventEmitter();
    public showMore: boolean = false;

    public showFeatures: any;
    public pushingFeautre: Array<string> = [];
    public imageArray: Array<any> = [];
    public allVideoId: Array<string> = [];

    public packageArray: Array<Package>;

    private featureSub: Subscription;
    private packageSub: Subscription;

    constructor(
        private _packageService: PackagesCollection,
        private media: MediaService) {
    }

    ngOnChanges(changes: any) {
        this.loadFeatures();
    }

    ngOnInit() {
        this.featureSub = this._packageService.features().subscribe(result => {
            this.featureArray = result;
            this.loadFeatures();
        })
        this.packageSub = this._packageService.packages().subscribe(result => {
            this.packageArray = result;
        })
        this.removeIfNoImage();
        this.showWistia();
    }

    ngOnDestroy() {
        if (this.featureSub) this.featureSub.unsubscribe();
        if (this.packageSub) this.packageSub.unsubscribe();
    }

    public loadFeatures() {
        this.feature = this.featureArray.find(x => x.Name == this.name);
    }
    displayAllFeatures() {
        this.showMore = !this.showMore;
    }

    public removeIfNoImage() {
        if (this.feature && this.feature.Information) {
            for (let images of this.feature.Information) {
                if (images.ImageUrl == "") {
                } else {
                    this.imageArray.push(images.ImageUrl)
                }
            }
        }
    }

    public showWistia() {
        this.allVideoId = [];
        if (this.feature) {
            for (let video of this.feature.Information) {

                if (video.VideoUrl.includes('https')) {

                    let videoId = video.VideoUrl.substring(video.VideoUrl.lastIndexOf('/') + 1, video.VideoUrl.lastIndexOf('.'))
                    this.allVideoId.push(videoId);
                }
            }
        }
    }

    public selectImage(image: string) {
        this.media.showImage(image);
    }
}
