import { Component } from '@angular/core';
import { Package, PackagesCollection } from '../../../services/packages.service';
import { OrgService } from '../../../services/organisation.service';
import { Subscription } from "rxjs/Subscription";

@Component({
    selector: 'feature-standalone-selector',
    templateUrl: 'feature-standalone.html',
    styleUrls: ['packages.css']
})

export class FeatureStandaloneComponent {

    public packageArray: Array<Package>;
    //public hidingTitel: boolean;
    public featureName: string;
    public thisChoosenPackage: any;
    public choosenTabset: any;
    public selectedUsers: any;
    public activatePackage: boolean;
    public userList: Array<any> = [];
    public selectedUsersArray: Array<any> = [];
    public activatedShowAvaUsers: boolean = false;
    public showButton: boolean = true;
    public sendPackageToGetUsers: Package;
    public clickabelName: boolean = false;
    public currency: any;
    public displayOnlyName: boolean = false;
    public addPackageToCart: boolean = true;
    public orgPackages: Array<any>;

    private orgSub: Subscription;
    private featureSub: Subscription;
    private packageSub: Subscription;

    constructor(
        public _packageService: PackagesCollection,
        private orgService: OrgService,
    ) {
    }


    ngOnInit() {

        this.featureSub = this._packageService.selectedFeature().subscribe(result => {
            this.featureName = result;
            this.matchFeatureWithPackages();
            this.settingLicenscesOnFirstPackage();
            // this.settingLicenscesOnFirstPackage();
            // setTimeout(function () {
            //     var tabContent = document.getElementById('packageTabsetId').lastElementChild;
            //     tabContent.setAttribute('style', 'height: inherit;');
            //     console.log(tabContent);
            // }, 100);
        })

        this.packageSub = this._packageService.packages().subscribe(result => {
            this.packageArray = result;
        })

        this.orgSub = this.orgService.currentOrganisationObservable().subscribe(org => {
            if (org) {
                this.currency = org.CurrencyName;
                this.orgPackages = org.Packages;
            }
        });
        this._packageService.settingCurrencyIfYouhavePackage();
    }

    public matchFeatureWithPackages() {
        if (this._packageService.packagesWithFeature.length) {
            for (let packageMatch of this.packageArray) {
                let matched = this._packageService.packagesWithFeature.find(x => x.NameLabel == packageMatch.NameLabel)
                if (matched) {
                } else {
                    if (packageMatch.Features.findIndex(x => x == this.featureName) >= 0) {
                        this._packageService.packagesWithFeature.push(packageMatch);
                    }
                }
                this.choosenTabset = this._packageService.packagesWithFeature[0];
            }
        } else {
            for (let packageMatch of this.packageArray) {
                if (packageMatch.Features.findIndex(x => x == this.featureName) >= 0) {
                    this._packageService.packagesWithFeature.push(packageMatch);
                }
            }
            this.choosenTabset = this._packageService.packagesWithFeature[0];
        }
    }

    public settingLicenscesOnFirstPackage() {
        if (this._packageService.packagesToShow.find(x => x.Id == this.choosenTabset.Id)) {
            this._packageService.settingValueOnBuildingPackage();
        } else if (this.orgPackages.find(x => x.PackageId == this.choosenTabset.Id)) {
            let packageWithLicensces = this.orgPackages.find(x => x.PackageId == this.choosenTabset.Id)
            this._packageService.buildingPackage.Licences = packageWithLicensces.Licences;
        }
    }

    public choosenTab(tab: any) {
        this.choosenTabset = tab;
        if (this._packageService.packagesToShow.find(x => x == tab)) {
            this._packageService.settingValueOnBuildingPackage();
        } else if (this._packageService.ownedPackages.find(x => x == tab)) {
            let packageWithLicensces = this.orgPackages.find(x => x.PackageId == tab.Id)
            this._packageService.buildingPackage.Licences = packageWithLicensces.Licences;
        }



        // var tabsetId = document.getElementById('packageTabsetId').lastElementChild;
        // tabsetId.scrollTop = 0;
    }

    public chooseUsersWithPackage(packageUsers: Package) {
        this._packageService.buildingPackage.PackageId = packageUsers.Id;
        this._packageService.showSelectedPackageWithUsers(packageUsers);
        this.closeSingelFeature();
    }

    public selectedPackageWithUser() {
        this.activatePackage = true;
        this.closeSingelFeature();
    }

    public sendPackageToUsers(packageWithoutUsers: Package) {
        this.sendPackageToGetUsers = packageWithoutUsers;
        this.closeSingelFeature();
    }

    public showAmmountOfUser(user: any) {
        this.selectedUsers = user;
    }

    clickToCloseSingelFeature(event: any) {
        var target = event.target || event.srcElement;
        if (target.id === 'showOneFeature') {
            this.closeSingelFeature();
        }
    }

    public closeSingelFeature() {
        this.featureName = null;
        this._packageService.packagesWithFeature = [];
        // var tabsetId = document.getElementById('packageTabsetId').lastElementChild;
        // tabsetId.scrollTop = 0;
    }
    ngOnDestroy() {
        this._packageService.packagesWithFeature = [];
        if (this.featureSub) this.featureSub.unsubscribe();
        if (this.packageSub) this.packageSub.unsubscribe();
        if (this.orgSub) this.orgSub.unsubscribe();
    }
}