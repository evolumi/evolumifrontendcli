import { Component, Output, EventEmitter } from '@angular/core';
import { PackagesCollection, AddPackageModel } from '../../../services/packages.service';
import { ChosenOptionModel } from "../../shared/components/chosen/chosen.models";

@Component({

    selector: 'package-cart',
    templateUrl: 'package-cart.component.html',
    styleUrls: ['packages.css']
})

export class PackageCartComponent {

    public orgId: string;
    public showCard: boolean = false;
    public lock: boolean = false;
    public allcurrency: ChosenOptionModel[] = [];

    @Output() edit: EventEmitter<AddPackageModel> = new EventEmitter();
    @Output() delete: EventEmitter<AddPackageModel> = new EventEmitter();

    constructor(
        public _packageService: PackagesCollection) {
        this.orgId = localStorage.getItem("orgId");

        this.allcurrency.push(
            new ChosenOptionModel('SEK', 'SEK'),
            new ChosenOptionModel('USD', 'USD'),
            new ChosenOptionModel('EUR', 'EUR'),
        )
    }

    reCalculate() {
        this._packageService.checkingTotalSum();
    }

    editCurrenctPackage(pack: AddPackageModel) {
        this.edit.next(pack);
    }

    deleteCurrentPackage(pack: AddPackageModel) {
        this.delete.next(pack);
    }

    private hideCardModal() {
        this.showCard = false;
    }

    showCardModal() {
        this.showCard = true;
    }

    public cardSelected(card: any) {
        console.log("Selected card:", card);
        this._packageService.creditCard = card;
        this.hideCardModal();
    }

    public activatePackage() {
        this.lock = true;
        
        this._packageService.addPackages(() => {
            this.lock = false;
        });
    }

    public clicktoCloseCreditCard(event: any) {
        var target = event.target || event.srcElement;
        if (target.id === 'showCreditCard') {
            this.hideCardModal();
        }
    }
}