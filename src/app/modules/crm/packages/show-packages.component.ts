import { Component, Input, Output, EventEmitter, Pipe, PipeTransform } from '@angular/core';
import { Package, Feature, VolumePrice, PackagesCollection } from '../../../services/packages.service';
import { OrgService } from '../../../services/organisation.service';
import { MediaService } from '../../../services/media.service';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from "rxjs/Subscription";

@Component({
    selector: 'show-packages-selector',
    templateUrl: 'show-packages.html',
    styleUrls: ['packages.css']
})
export class ShowPackagesComponent {

    @Input() chosenPackage: Package;
    @Input() newPackage: boolean;
    @Output() closePopup: EventEmitter<boolean> = new EventEmitter();

    public packageArray: Array<Package>;
    public allVideoId: Array<string> = [];
    public orgSub: Subscription;
    private packageSub: Subscription;
    private featureSub: Subscription;
    public imageArray: Array<any> = [];
    public showYearlyPrice: boolean = true;
    public orgPackages: Array<any>;
    public foundPackageInterval: string = "";
    public foundPackge: any;
    public orgHasPackage: boolean = true;
    public canceldPackage: boolean = false;
    public currentUsers: number;
    public orgPackageActive: any;

    private features: Array<Feature> = [];
    private showCard = false;
    private orgId: string;

    private volumePricedFeatures: Array<Feature> = [];

    private limitExceeded: boolean = false;
    private monthlyPaymentDay: Date;
    private yearlyPaymentDay: Date;

    constructor(
        public _packageService: PackagesCollection,
        private orgService: OrgService,
        private media: MediaService,
        private translate: TranslateService) {
    }

    ngOnChanges(changes: any) {
        this.showYearlyPrice = (this._packageService.buildingPackage && this._packageService.buildingPackage.Interval == "Yearly");
        if (this.chosenPackage) {
            this.showWistia();
            console.log("[Package-Modal]", this.chosenPackage);
        }
    }

    ngOnInit() {

        this.packageSub = this._packageService.packages().subscribe(result => {
            this.packageArray = result;
        })

        this.featureSub = this._packageService.features().subscribe(features => {
            this.features = features;
        });

        this.orgSub = this.orgService.currentOrganisationObservable().subscribe(org => {
            if (org) {
                this.orgId = org.Id;
                this.orgPackages = org.Packages;
                this.monthlyPaymentDay = org.PaymentData.MonthlyPaymentDate;
                this.yearlyPaymentDay = org.PaymentData.YearlyPaymentDate;
            }
        })

        this.checkingIfPackageIsInArray();
        this.checkingValue();
        this.removeIfNoImage();
        this.checkingIfPackageIsCanceld();
        this.getVolumePrices();
    }

    public selectImage(image: string) {
        this.media.showImage(image);
    }

    checkingIfPackageIsCanceld() {
        if (this.orgPackages.find(x => x.PackageId == this.chosenPackage.Id)) {
            this.orgPackageActive = this.orgPackages.find(x => x.PackageId == this.chosenPackage.Id);
            if (!this.orgPackageActive.Active) {
                this.canceldPackage = true;
                this.currentUsers = this.orgPackageActive.Licences;
            } else {
                this.canceldPackage = false;
            }
        } else {
            this.canceldPackage = false;
        }
    }

    public checkingIfPackageIsInArray() {

        if (this._packageService.ownedPackages.find(x => x.NameLabel == this.chosenPackage.NameLabel)) {
            this.orgHasPackage = true;
            this.foundPackge = this.orgPackages.find(x => x.PackageId == this.chosenPackage.Id);
            this.foundPackageInterval = this.foundPackge.Interval;
            if (this.foundPackageInterval.includes('Yearly')) {
                this.showYearlyPrice = false;
            }
            else if (this.foundPackageInterval.includes('Monthly')) {
                this.showYearlyPrice = true;
            }
        }

        else if (this._packageService.packagesToShow.find(x => x.NameLabel == this.chosenPackage.NameLabel)) {
            this.orgHasPackage = false;
            if (!this._packageService.packagesWithFeature.find(x => x.NameLabel == this.chosenPackage.NameLabel)) {
                this._packageService.settingValueOnBuildingPackage();
            }
            this.showYearlyPrice = true;
        }
        else if (this._packageService.cart.find(x => x.choosenPackage.NameLabel == this.chosenPackage.NameLabel)) {
            this.orgHasPackage = false;
            if (this._packageService.buildingPackage.Interval.includes('Yearly')) {
                this.showYearlyPrice = false;
            }
            else if (this._packageService.buildingPackage.Interval.includes('Monthly')) {
                this.showYearlyPrice = true;
            }
        }
        else {
            this.orgHasPackage = false;
            this.showYearlyPrice = true;
        }
    }

    public checkingValue() {
        this.showYearlyPrice = !this.showYearlyPrice;
        console.log("Click. Yearly payment = " + this.showYearlyPrice);
        this._packageService.buildingPackage.Interval = this.showYearlyPrice ? "Yearly" : "Monthly";
    }

    public packagePriceYearly: any;
    public packagePriceMontly: any;
    public packagePrice() {
        this.packagePriceMontly = this.packageArray.reduce((result: any, current: any) => {
            result[current.Id] = current.MonthlyPrices;
            return result;
        }, {});

        this.packagePriceYearly = this.packageArray.reduce((result: any, current: any) => {
            result[current.Id] = current.YearlyPrices;
            return result;
        }, {});

    }

    public removeIfNoImage() {
        for (let images of this.chosenPackage.Information) {
            if (images.ImageUrl == "") {
            } else {
                this.imageArray.push(images.ImageUrl)
            }
        }
    }

    public showWistia() {
        this.allVideoId = [];

        for (let video of this.chosenPackage.Information) {
            if (video.VideoUrl.includes('https')) {
                let videoId = video.VideoUrl.substring(video.VideoUrl.lastIndexOf('/') + 1, video.VideoUrl.lastIndexOf('.'))
                this.allVideoId.push(videoId);
            }
        }
    }

    public clickRow(feature: Feature, value: number) {
        feature.selectedValue = value;
        this.setPrice(feature);
    }

    public setPrice(feature: Feature) {

        let priceList = feature.VolumePricing[this._packageService.Currency];
        if (priceList && priceList.length) {
            let volumeLimit = priceList.slice(-1)[0];
            let price = priceList.find(x => x.Max >= feature.selectedValue) || volumeLimit;
            this._packageService.buildingPackage.Volumes[feature.Name] = price.Max;
            feature.selectedPrice = price.Price;

            if (feature.selectedValue > volumeLimit.Max) {
                this.limitExceeded = true;
            }
        }
    }

    public chooseUsersWithPackage(packageUsers: Package) {
        this._packageService.buildingPackage.PackageId = packageUsers.Id;
        this._packageService.showSelectedPackageWithUsers(packageUsers);
        this.closePackages();
    }
    public chooseUsersWithPackageToUpdate(updatePackage: Package) {
        this._packageService.buildingPackage.PackageId = updatePackage.Id;
        this._packageService.buildingPackage.ReActivate = false;
        this._packageService.showSelectedPackageWithUsers(updatePackage);
        this.closePackages();
    }

    public reActivatePackage(activatePackage: Package) {
        this._packageService.buildingPackage.PackageId = activatePackage.Id;
        this._packageService.buildingPackage.ReActivate = true;
        this._packageService.showSelectedPackageWithUsers(activatePackage);
        this.closePackages();
    }

    public closePackages() {
        this.chosenPackage = undefined;
        // var resetScroll = document.getElementById('choosenScrollPackage');
        // resetScroll.scrollTop = 0;
        this.closePopup.next(true);
    }

    public clicktoCloseCreditCard(event: any) {
        var target = event.target || event.srcElement;
        if (target.id === 'showCreditCard') {
            this.hideCardModal();
        }
    }

    public cardSelected(card: any) {
        console.log("Selected card:", card);
        this._packageService.creditCard = card;
        this.hideCardModal();
    }

    public showCardModal() {
        this.showCard = true;
    }

    public hideCardModal() {
        this.showCard = false;
    }

    private getVolumePrices() {
        this.volumePricedFeatures = this.features.filter(x =>
            x.VolumePricing &&
            Object.keys(x.VolumePricing).length &&
            this.chosenPackage.Features.findIndex(y => y == x.Name) != -1);

        for (var feature of this.volumePricedFeatures) {

            let price: VolumePrice;

            if (this._packageService.buildingPackage.Volumes[feature.Name]) {
                price = feature.VolumePricing[this._packageService.Currency].find(x => x.Max == this._packageService.buildingPackage.Volumes[feature.Name]);
            }
            else {
                price = feature.VolumePricing[this._packageService.Currency][0];
                this._packageService.buildingPackage.Volumes[feature.Name] = price.Max;
            }
            feature.selectedPrice = price.Price;
            feature.selectedValue = price.Max;
        }
        if (this.volumePricedFeatures.length) {
            this._packageService.buildingPackage.Licences = 0;
        }
    }

    cancelSubscription(item: any) {
        var conf = confirm(this.translate.instant("WARNCANCELPACKAGE"));
        if (conf) {
            this._packageService.cancelPackage(item.Id);
            this.closePackages();
        }
    }

    ngOnDestroy() {
        if (this.orgSub) this.orgSub.unsubscribe();
        if (this.featureSub) this.featureSub.unsubscribe();
        if (this.packageSub) this.packageSub.unsubscribe();
    }
}

@Pipe({ name: 'filterPriceList' })
export class PriceListPipe implements PipeTransform {
    transform(value: Array<VolumePrice>, quantity: number) {

        if (!value.length || value.length < 3) return value;

        if (quantity > value[value.length - 1].Max) quantity = value[value.length - 1].Max;

        value = value.sort((x1, x2) => x1.Max > x2.Max ? 1 : x1.Max < x2.Max ? -1 : 0);
        console.log("Filtering on Quantity: " + quantity, value);
        let index = value.findIndex(x => quantity == undefined || quantity <= x.Max) - 1;
        if (index < 0) index = 0;
        let end = index + 3 > value.length - 1 ? value.length : index + 4;

        console.log("Found Index:" + index + " - " + end);
        return value.slice(index, end);
    }
}
