import { Component } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { MediaService } from '../../../services/media.service';

@Component({
    selector: 'showImage-selector',
    templateUrl: 'showImage.html',
    styleUrls: ['packages.css']
})
export class ShowImageComponent {

    public choosenImage: string;
    private imageSub: Subscription;

    constructor(private media: MediaService) {
    }

    ngOnInit() {
        this.imageSub = this.media.seletedImageObservable().subscribe(data => {
            this.choosenImage = data;
        })
    }

    public clickToCloseImage(event: any) {
        var target = event.target || event.srcElement;
        if (target.id === 'popupImage') {
            this.closeImage();
        }
    }

    public closeImage() {
        this.choosenImage = null;
    }

    ngOnDestroy() {
        if (this.imageSub) this.imageSub.unsubscribe();
    }
}