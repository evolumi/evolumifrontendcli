import { Routes } from '@angular/router';

import { PackagesComponent } from './packages.component';

export const PackagesRoutes: Routes = [
    { path: 'Packages', component: PackagesComponent },
    { path: "Features/:name", component: PackagesComponent },
];