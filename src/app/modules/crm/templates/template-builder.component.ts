import { Component, Input } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { TemplateModel, TemplateRowModel, TemplateColumnModel, TemplateBlockModel, TemplateBlockText } from '../../../models/template.models';
import { TemplateBuilderService } from '../../../services/template-builder.service';
import { DragAndDropService } from '../../../services/drag-and-drop.service';

@Component({
    selector: 'template-builder',
    templateUrl: './template-builder.component.html',
    styleUrls: ['./template-builder.css']
})
export class TemplateBuilderComponent {

    @Input() template: TemplateModel;
    @Input() onlyShow: boolean = false;

    public currentTemplateBlock: TemplateBlockModel;
    private currentTemplateRow: TemplateRowModel;

    public redactorEdit: boolean;

    private blockSub: Subscription;
    private rowSub: Subscription;
    private dropSub: Subscription;

    [key: string]: any;

    constructor(private templateSvc: TemplateBuilderService,
        private dragSvc: DragAndDropService) { }

    ngOnInit() {
        // if (!this.onlyShow) {
        //     this.blockSub = this.templateSvc.currentTemplateBlockObs()
        //         .subscribe(block => this.currentTemplateBlock = block);
        //     this.rowSub = this.templateSvc.currentTemplateRowObs()
        //         .subscribe(row => this.currentTemplateRow = row);
        //     this.dropSub = this.dragSvc.onDropObs()
        //         .subscribe((x: any) => {
        //             if (!x.TemplateId) {
        //                 this.setNewId(x);
        //                 if (x.Columns) {
        //                     x.Columns.map((x: any) => this.setNewId(x));
        //                 }
        //             }
        //         });
        // }

        // IF IT
        this.initLinkStyles();
    }

    ngOnDestroy() {
        // if (this.blockSub) this.blockSub.unsubscribe();
        // if (this.dropSub) this.dropSub.unsubscribe();
        // if (this.rowSub) this.rowSub.unsubscribe();
        // this.templateSvc.clearStyleSheets();
    }

    // onClickRow(event: any, row: TemplateRowModel) {
    //     if (!this.onlyShow) {
    //         event.stopPropagation();
    //         this.templateSvc.setCurrentTemplateRow(row);

    //         Array.from(document.getElementsByClassName('template-selected'))
    //             .map(el => el.className = el.className.replace(' template-selected', ''));

    //         if (event.target.className.indexOf('template-row') > -1) {
    //             event.target.className += ' template-selected'
    //         } else {
    //             let parent = event.target.closest('.template-row');
    //             parent.className += ' template-selected';
    //         }
    //     }
    // }

    // onEnterBlock(event: Event) {
    //     if (!this.onlyShow) {
    //         event.stopPropagation();
    //         let rowElements = Array.from(document.querySelectorAll('.template-row-hover'));
    //         rowElements = rowElements.map(el => {
    //             el.className = el.className.replace(' template-row-hover', '');
    //             return el;
    //         });
    //     }
    // }

    // onLeaveBlock(event: Event) {
    //     if (!this.onlyShow) {
    //         event.stopPropagation();
    //         let rowElements = Array.from(document.querySelectorAll('.template-row'));
    //         rowElements = rowElements.map(el => {
    //             el.className += ' template-row-hover';
    //             return el;
    //         })
    //     }
    // }

    // onClickBlock(event: any, block: TemplateBlockModel) {
    //     if (!this.onlyShow) {
    //         event.stopPropagation();
    //         this.templateSvc.setCurrentTemplateBlock(block);
    //         Array.from(document.getElementsByClassName('template-selected'))
    //             .map(el => el.className = el.className.replace(' template-selected', ''));

    //         if (event.target.className.indexOf('template-block') > -1) {
    //             event.target.className += ' template-selected'
    //         } else {
    //             let parent = event.target.closest('.template-block');
    //             parent.className += ' template-selected';
    //         }

    //         if (block.Type === 'TextBlock' || block.Type === 'Button' || block.Type === 'Unsubscribe') {
    //             this.redactorEdit = true;
    //         }
    //     }
    // }

    // deleteRow(event: Event, row: TemplateRowModel) {
    //     if (!this.onlyShow) {
    //         event.stopPropagation();
    //         this.template.Rows = this.template.Rows.filter(x => x.TemplateId !== row.TemplateId);
    //         this.templateSvc.setCurrentTemplateRow(null);
    //     }
    // }

    // deleteBlock(event: Event, column: TemplateColumnModel, block: TemplateBlockModel) {
    //     if (!this.onlyShow) {
    //         event.stopPropagation();
    //         column.Blocks = column.Blocks.filter(x => x.TemplateId !== block.TemplateId);
    //         this.templateSvc.setCurrentTemplateBlock(null);
    //     }
    // }

    // duplicateRow(event: Event, row: TemplateRowModel, index: number) {
    //     if (!this.onlyShow) {
    //         event.stopPropagation();
    //         let newRow = JSON.parse(JSON.stringify(row)) as TemplateRowModel;
    //         this.setNewId(newRow);
    //         newRow.Columns.map(x => {
    //             this.setNewId(x);
    //             x.Blocks.map(y => this.setNewId(y))
    //         });
    //         this.template.Rows.splice(index, 0, newRow);
    //     }
    // }

    // duplicateBlock(event: Event, column: TemplateColumnModel, block: TemplateBlockModel, index: number) {
    //     if (!this.onlyShow) {
    //         event.stopPropagation();
    //         let newBlock = JSON.parse(JSON.stringify(block));
    //         this.setNewId(newBlock);
    //         column.Blocks.splice(index, 0, newBlock);
    //     }
    // }

    // deselect(event: any) {
    //     if (!this.templateSvc.redactorModalActive && (this.currentTemplateBlock || this.currentTemplateRow)) {
    //         let noDeselectEl = document.getElementsByClassName('no-deselect-template');
    //         let contains = false;
    //         for (let i = 0; i < noDeselectEl.length; i++) {
    //             contains = noDeselectEl[i].contains(event.target);
    //             if (contains) i = noDeselectEl.length;
    //         }
    //         if (!contains) {
    //             Array.from(document.getElementsByClassName('template-selected'))
    //                 .map(el => el.className = el.className.replace(' template-selected', ''));
    //             if (this.currentTemplateBlock) this.templateSvc.setCurrentTemplateBlock(null);
    //             if (this.currentTemplateRow) this.templateSvc.setCurrentTemplateRow(null);
    //         }
    //     }
    // }

    // onRedactorDestroy(block: TemplateBlockText) {
    //     this.redactorEdit = false;
    //     setTimeout(() => {
    //         let el = document.getElementById(block.TemplateId);
    //         if (el) {
    //             let links = Array.from(el.querySelectorAll('a'));
    //             this.templateSvc.adjustLinkElements(links, this.template.LinkStyles);
    //         }
    //     }, 0);
    // }

    // private setNewId(obj: any) {
    //     const id = Math.random().toString(36).slice(-8);
    //     obj.TemplateId = id;
    // }

    private initLinkStyles() {
        let bodyStyle = this.template.LinkStyles;
        let blockStyles = this.template.Rows.map(x => x.Columns).reduce((a, b) => a.concat(b), []).map(x => x.Blocks).reduce((a, b) => a.concat(b), []).filter(x => (<any>x).LinkStyles);

        if (this.template.HeaderRows && this.template.HeaderRows.length) {
            blockStyles.push(...this.template.HeaderRows.map(x => x.Columns).reduce((a, b) => a.concat(b), []).map(x => x.Blocks).reduce((a, b) => a.concat(b), []).filter(x => (<any>x).LinkStyles));
        }
        if (this.template.FooterRows && this.template.FooterRows.length) {
            blockStyles.push(...this.template.FooterRows.map(x => x.Columns).reduce((a, b) => a.concat(b), []).map(x => x.Blocks).reduce((a, b) => a.concat(b), []).filter(x => (<any>x).LinkStyles));
        }

        let css = '';
        let linkCss = '';
        let hoverCss = '';

        for (let prop in bodyStyle) {
            if (bodyStyle.hasOwnProperty(prop)) {
                if (prop.indexOf('hover') > -1) {
                    if (!hoverCss) {
                        hoverCss += '#template-body a:hover {'
                    }
                    hoverCss += `${prop.replace('hover-', '')}: ${bodyStyle[prop]};`;
                } else {
                    if (!linkCss) {
                        linkCss += '#template-body a {'
                    }
                    linkCss += `${prop}: ${bodyStyle[prop]};`;
                }
            }
        }
        if (hoverCss) hoverCss += '}';
        if (linkCss) linkCss += '}';
        css = linkCss + hoverCss;

        for (let block of blockStyles) {
            let blockLinkCss = '';
            let blockHoverCss = '';
            for (let prop in <any>(block).LinkStyles) {
                if (block.hasOwnProperty(prop)) {
                    if (prop.indexOf('hover') > -1) {
                        if (!blockHoverCss) {
                            blockHoverCss += `#${block.TemplateId} a:hover {`
                        }
                        blockHoverCss += `${prop.replace('hover-', '')}: ${bodyStyle[prop]};`;
                    } else {
                        if (!blockLinkCss) {
                            blockLinkCss += `#${block.TemplateId} a {`
                        }
                        blockLinkCss += `${prop}: ${bodyStyle[prop]};`;
                    }
                }
            }
            if (blockHoverCss) blockHoverCss += '}';
            if (blockLinkCss) blockLinkCss += '}';
            css += blockLinkCss + blockHoverCss
        }

        if (css) {
            let styleElement = document.createElement('style');
            this.templateSvc.styleElements.push(styleElement);
            styleElement.appendChild(document.createTextNode(css));
            document.getElementsByTagName('head')[0].appendChild(styleElement);
        }
    }
}