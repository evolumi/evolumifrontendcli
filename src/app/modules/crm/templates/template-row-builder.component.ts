import { Component, OnInit, Input } from '@angular/core';
import { TemplateRowModel, TemplateModel, TemplateBlockModel, TemplateBlockText, TemplateColumnModel, TemplateProductValues, TemplateBlockProduct } from "../../../models/template.models";
import { Subscription } from "rxjs/Subscription";
import { TemplateBuilderService } from "../../../services/template-builder.service";
import { DragAndDropService } from "../../../services/drag-and-drop.service";

@Component({
    selector: 'template-row-builder',
    templateUrl: './template-row-builder.component.html',
    styleUrls: ['./template-builder.css'],
    host: {
        '(document:click)': 'deselect($event)',
    }
})
export class TemplateRowBuilderComponent implements OnInit {

    @Input() template: TemplateModel
    @Input() rowArrProp: string;
    @Input() dragName: string;
    @Input() editable: boolean;

    public currentTemplateBlock: TemplateBlockModel;
    private currentTemplateRow: TemplateRowModel;

    public redactorEdit: boolean;
    public redactorEditCell: {row: number, col: number} = {row: -1, col:-1};

    private blockSub: Subscription;
    private rowSub: Subscription;
    private dropSub: Subscription;

    constructor(private templateSvc: TemplateBuilderService,
        private dragSvc: DragAndDropService) { }

    ngOnInit() {
        if (this.editable) {
            this.blockSub = this.templateSvc.currentTemplateBlockObs()
                .subscribe(block => this.currentTemplateBlock = block);
            this.rowSub = this.templateSvc.currentTemplateRowObs()
                .subscribe(row => this.currentTemplateRow = row);
            this.dropSub = this.dragSvc.onDropObs()
                .subscribe((x: any) => {
                    if (!x.TemplateId) {
                        this.setNewId(x);
                        if (x.Columns) {
                            x.Columns.map((x: any) => this.setNewId(x));
                        }
                    }
                });
        }

        // this.initLinkStyles();
    }

    ngOnDestroy() {
        if (this.blockSub) this.blockSub.unsubscribe();
        if (this.dropSub) this.dropSub.unsubscribe();
        if (this.rowSub) this.rowSub.unsubscribe();
        this.templateSvc.clearStyleSheets();
    }

    onClickRow(event: any, row: TemplateRowModel) {
        if (this.editable) {
            event.stopPropagation();
            this.templateSvc.setCurrentTemplateRow(row);

            Array.from(document.getElementsByClassName('template-selected'))
                .map(el => el.className = el.className.replace(' template-selected', ''));

            if (event.target.className.indexOf('template-row') > -1) {
                event.target.className += ' template-selected'
            } else {
                let parent = event.target.closest('.template-row');
                parent.className += ' template-selected';
            }
        }
    }

    onEnterBlock(event: Event) {
        if (this.editable) {
            event.stopPropagation();
            let rowElements = Array.from(document.querySelectorAll('.template-row-hover'));
            rowElements = rowElements.map(el => {
                el.className = el.className.replace(' template-row-hover', '');
                return el;
            });
        }
    }

    onLeaveBlock(event: Event) {
        if (this.editable) {
            event.stopPropagation();
            let rowElements = Array.from(document.querySelectorAll('.template-row'));
            rowElements = rowElements.map(el => {
                el.className += ' template-row-hover';
                return el;
            })
        }
    }

    onClickBlock(event: any, block: TemplateBlockModel) {
        if (this.editable) {
            event.stopPropagation();
            this.templateSvc.setCurrentTemplateBlock(block);
            Array.from(document.getElementsByClassName('template-selected'))
                .map(el => el.className = el.className.replace(' template-selected', ''));

            if (event.target.className.indexOf('template-block') > -1) {
                event.target.className += ' template-selected'
            } else {
                let parent = event.target.closest('.template-block');
                if (parent) {
                    parent.className += ' template-selected';
                }
            }

            if (block.Type === 'TextBlock' || block.Type === 'Button' || block.Type === 'Unsubscribe') {
                this.redactorEdit = true;
            }
        }
    }

    onClickCell(event: Event, rowIndex: number, colIndex: number){
        if (this.editable){
            event.stopPropagation();
            this.redactorEditCell.row = rowIndex;
            this.redactorEditCell.col = colIndex;
        }
    }

    deleteRow(event: Event, row: TemplateRowModel) {
        if (this.editable) {
            event.stopPropagation();
            this.template.Rows = this.template.Rows.filter(x => x.TemplateId !== row.TemplateId);
            this.templateSvc.setCurrentTemplateRow(null);
        }
    }

    deleteBlock(event: Event, column: TemplateColumnModel, block: TemplateBlockModel) {
        if (this.editable) {
            event.stopPropagation();
            column.Blocks = column.Blocks.filter(x => x.TemplateId !== block.TemplateId);
            this.templateSvc.setCurrentTemplateBlock(null);
        }
    }

    duplicateRow(event: Event, row: TemplateRowModel, index: number) {
        if (this.editable) {
            event.stopPropagation();
            let newRow = JSON.parse(JSON.stringify(row)) as TemplateRowModel;
            this.setNewId(newRow);
            newRow.Columns.map(x => {
                this.setNewId(x);
                x.Blocks.map(y => this.setNewId(y))
            });
            this.template.Rows.splice(index, 0, newRow);
        }
    }

    duplicateBlock(event: Event, column: TemplateColumnModel, block: TemplateBlockModel, index: number) {
        if (this.editable) {
            event.stopPropagation();
            let newBlock = JSON.parse(JSON.stringify(block));
            this.setNewId(newBlock);
            column.Blocks.splice(index, 0, newBlock);
        }
    }

    deselect(event: any) {
        console.log('DESELECTCLICK');
        if (!this.templateSvc.redactorModalActive && (this.currentTemplateBlock || this.currentTemplateRow)) {
            let noDeselectEl = document.getElementsByClassName('no-deselect-template');
            let contains = false;
            for (let i = 0; i < noDeselectEl.length; i++) {
                contains = noDeselectEl[i].contains(event.target);
                if (contains) i = noDeselectEl.length;
            }
            if (!contains) {
                Array.from(document.getElementsByClassName('template-selected'))
                    .map(el => el.className = el.className.replace(' template-selected', ''));
                if (this.currentTemplateBlock) this.templateSvc.setCurrentTemplateBlock(null);
                if (this.currentTemplateRow) this.templateSvc.setCurrentTemplateRow(null);
            }
        }
    }

    onRedactorDestroy(block: TemplateBlockText) {
        this.redactorEdit = false;
        setTimeout(() => {
            let el = document.getElementById(block.TemplateId);
            if (el) {
                let links = Array.from(el.querySelectorAll('a'));
                this.templateSvc.adjustLinkElements(links, this.template.LinkStyles);
            }
        }, 0);
    }

    onRedactorCellDestroy(){
        this.redactorEditCell = {row: -1, col: -1};
    }

    private setNewId(obj: any) {
        const id = Math.random().toString(36).slice(-8);
        obj.TemplateId = id;
    }
}
