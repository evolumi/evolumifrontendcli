import { Component, OnInit, Input, EventEmitter, Output, OnDestroy } from '@angular/core';
import { TemplateModel } from "../../../../../models/template.models";
import { DefaultMailTemplate, DefaultDocumentTemplate } from "../../template-settings";
import { TemplateBuilderService } from "../../../../../services/template-builder.service";
import { Subscription } from "rxjs/Subscription";

@Component({
  selector: 'template-main',
  templateUrl: 'template-main.component.html',
  styleUrls: ['template-main.component.css']
})
export class TemplateMainComponent implements OnInit, OnDestroy {

  @Input() type: string;
  @Input() goToBuilder: boolean;
  @Input() leftFree: boolean = true;
  @Input() showSave: boolean;
  @Output() showSaveChange = new EventEmitter<boolean>();
  @Output() templateSelected = new EventEmitter<TemplateModel>();

  public selectedTemplate: TemplateModel;
  public defaultTemplate: TemplateModel;
  public templateList: Array<TemplateModel> = [];

  private templateListSub: Subscription;
  private templateSub: Subscription;

  constructor(private templateSvc: TemplateBuilderService) { }

  ngOnInit() {
    this.templateSvc.getTemplates(this.type);
    this.templateListSub = this.templateSvc.templateListObs()
      .subscribe(list => {
        if (list) {
          this.templateList = list;
        }
      });
    this.templateSub = this.templateSvc.currentTemplateObs()
      .subscribe(temp => {
        this.selectedTemplate = temp;
      })
    this.getDefaultTemplate();
  }

  ngOnDestroy(): void {
    if (this.templateListSub) this.templateListSub.unsubscribe();
    if (this.templateSub) this.templateSub.unsubscribe();
    this.templateSvc.resetService();
  }

  getDefaultTemplate() {
    switch (this.type) {
      case 'Newsletter':
        this.defaultTemplate = JSON.parse(JSON.stringify(DefaultMailTemplate));
        break;
      case 'Document':
        this.defaultTemplate = JSON.parse(JSON.stringify(DefaultDocumentTemplate));
        break;
    }
  }

  selectTemplate(template: TemplateModel) {
    this.templateSvc.setCurrentTemplate(template);
    this.templateSelected.emit(template);
  }

  deleteTemplate(id: string, event: Event) {
    event.stopPropagation();
    this.templateSvc.deleteTemplate(id);
  }
}
