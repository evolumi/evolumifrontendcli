import { TemplateModel, TemplateRowModel, TemplateColumnModel, TemplateBlockImage, TemplateBlockButton, TemplateBlockDivider, TemplateBlockSocial, TemplateBlockText, TemplateSocialMediaIcon, TemplateBlockUnsubscribe, TemplateBlockProduct, TemplateDocumentModel, TemplateBlockAddress, TemplateBlockTable } from '../../../models/template.models';

export interface IconCollection {
    id: string;
    icons: Array<Icon>;
}

export interface Icon {
    type: string;
    src: string;
}

const Fonts = [
    { value: 'Arial', label: 'Arial', styles: { 'font-family': 'Arial' } }, { value: 'Arial Black', label: 'Arial Black', styles: { 'font-family': 'Arial Black' } }, { value: 'Courier New', label: 'Courier New', styles: { 'font-family': 'Courier New' } }, { value: 'Georgia', label: 'Georgia', styles: { 'font-family': 'Georgia' } }, { value: 'Helvetica', label: 'Helvetica', styles: { 'font-family': 'Helvetica' } }, { value: 'Impact', label: 'Impact', styles: { 'font-family': 'Impact' } }, { value: 'Lucida Console', label: 'Lucida Console', styles: { 'font-family': 'Lucida Console' } }, { value: 'Lucida Sans Unicode', label: 'Lucida Sans Unicode', styles: { 'font-family': 'Lucida Sans Unicode' } }, { value: 'Palatino Linotype', label: 'Palatino Linotype', styles: { 'font-family': 'Palatino Linotype' } }, { value: 'Tahoma', label: 'Tahoma', styles: { 'font-family': 'Tahoma' } }, { value: 'Times New Roman', label: 'Times New Roman', styles: { 'font-family': 'Times New Roman' } }, { value: 'Trebuchet MS', label: 'Trebuchet MS', styles: { 'font-family': 'Trebuchet MS' } }, { value: 'Verdana', label: 'Verdana', styles: { 'font-family': 'Verdana' } }
];

const FontSizes = [
    { value: '10px', label: '10px' }, { value: '12px', label: '12px' }, { value: '14px', label: '14px' }, { value: '16px', label: '16px' }, { value: '18px', label: '18px' }, { value: '20px', label: '20px' }, { value: '22px', label: '22px' }, { value: '24px', label: '24px' }, { value: '28px', label: '28px' }, { value: '32px', label: '32px' }, { value: '40px', label: '40px' }, { value: '48px', label: '48px' }, { value: '64px', label: '64px' }, { value: '72px', label: '72px' }
];

const BorderStyles = [
    { value: 'dashed', label: 'Dashed', styles: { 'border-bottom': 'dashed 1px #CCC' } }, { value: 'dotted', label: 'Dotted', styles: { 'border-bottom': 'dotted 1px #CCC' } }, { value: 'double', label: 'Double', styles: { 'border-bottom': 'double 1px #CCC' } }, { value: 'groove', label: 'Groove', styles: { 'border-bottom': 'groove 1px #CCC' } }, { value: 'inset', label: 'Inset', styles: { 'border-bottom': 'inset 1px #CCC' } }, { value: 'none', label: 'None', styles: { 'border-bottom': 'none' } }, { value: 'outset', label: 'Outset', styles: { 'border-bottom': 'outset 1px #CCC' } }, { value: 'ridge', label: 'Ridge', styles: { 'border-bottom': 'ridge 1px #CCC' } }, { value: 'solid', label: 'Solid', styles: { 'border-bottom': 'solid 1px #CCC' } }
];

export const MailSettings = {
    PaddingMax: 40,
    PaddingMin: 0,
    PaddingStep: 5,
    ContentWidthMax: 900,
    ContentWidthMin: 480,
    ContentWidthStep: 10,
    BorderMax: 40,
    BorderMin: 0,
    BorderStep: 1,
    BorderRadiusMax: 200,
    BorderRadiusMin: 0,
    BorderRadiusStep: 1,
    LineHeightMax: 72,
    LineHeightMin: 10,
    LineHeightStep: 2,
    ImageWidthMax: 900,
    ImageWidthMin: 1,
    ImageWidthStep: 5,
    ImageHeightMax: 1200,
    ImageHeightMin: 1,
    ImageHeightStep: 5,
    IconSpacingMax: 30,
    IconSpacingMin: 0,
    IconSpacingStep: 5,
    WidthPercentMax: 100,
    WidthPercentMin: 0,
    WidthPercentStep: 10,
    ButtonWidthMax: 100,
    ButtonWidthMin: 10,
    ButtonWidthStep: 10,
    DividerWidthMax: 100,
    DividerWidthMin: 10,
    DividerWidthStep: 10,

    Fonts: Fonts,
    FontSizes: FontSizes,
    BorderStyles: BorderStyles
};

export const DocumentSettings = {
    WidthPercentMax: 100,
    WidthPercentMin: 10,
    WidthPercentStep: 10,

    PaddingMax: 20,
    PaddingMin: 0,
    PaddingStep: 1,

    BorderMax: 40,
    BorderMin: 0,
    BorderStep: 1,

    ColSpanMax: 10,
    ColSpanMin: 1,
    ColSpanStep: 1,

    MarginTopMax: 500,
    MarginTopMin: 0,
    MarginTopStep: 2,

    BorderRadiusMax: 200,
    BorderRadiusMin: 0,
    BorderRadiusStep: 1,

    Fonts: Fonts,
    FontSizes: FontSizes,
    BorderStyles: BorderStyles
}

const SocialIconsBasePath = '/assets/social-media/';

export const SocialMediaIconCollections = [
    { id: '001', icons: [{ type: 'Facebook', src: SocialIconsBasePath + 'facebook_001.png', url: 'www.facebook.com' }, { type: 'Instagram', src: SocialIconsBasePath + 'instagram_001.png', url: 'www.instagram.com' }, { type: 'GooglePlus', src: SocialIconsBasePath + 'gplus_001.png', url: 'plus.google.com' }, { type: 'LinkedIn', src: SocialIconsBasePath + 'linkedin_001.png', url: 'www.linkedin.com' }, { type: 'Pinterest', src: SocialIconsBasePath + 'pinterest_001.png', url: 'www.pinterest.com' }, { type: 'Tumblr', src: SocialIconsBasePath + 'tumblr_001.png', url: 'www.tumblr.com' }, { type: 'Twitter', src: SocialIconsBasePath + 'twitter_001.png', url: 'www.twitter.com' }, { type: 'YouTube', src: SocialIconsBasePath + 'youtube_001.png', url: 'www.youtube.com' }] },
    { id: '003', icons: [{ type: 'Facebook', src: SocialIconsBasePath + 'facebook_003.png', url: 'www.facebook.com' }, { type: 'Instagram', src: SocialIconsBasePath + 'instagram_003.png', url: 'www.instagram.com' }, { type: 'GooglePlus', src: SocialIconsBasePath + 'gplus_003.png', url: 'plus.google.com' }, { type: 'LinkedIn', src: SocialIconsBasePath + 'linkedin_003.png', url: 'www.linkedin.com' }, { type: 'Pinterest', src: SocialIconsBasePath + 'pinterest_003.png', url: 'www.pinterest.com' }, { type: 'Tumblr', src: SocialIconsBasePath + 'tumblr_003.png', url: 'www.tumblr.com' }, { type: 'Twitter', src: SocialIconsBasePath + 'twitter_003.png', url: 'www.twitter.com' }, { type: 'YouTube', src: SocialIconsBasePath + 'youtube_003.png', url: 'www.youtube.com' }] },
    { id: '004', icons: [{ type: 'Facebook', src: SocialIconsBasePath + 'facebook_004.png', url: 'www.facebook.com' }, { type: 'Instagram', src: SocialIconsBasePath + 'instagram_004.png', url: 'www.instagram.com' }, { type: 'GooglePlus', src: SocialIconsBasePath + 'gplus_004.png', url: 'plus.google.com' }, { type: 'LinkedIn', src: SocialIconsBasePath + 'linkedin_004.png', url: 'www.linkedin.com' }, { type: 'Pinterest', src: SocialIconsBasePath + 'pinterest_004.png', url: 'www.pinterest.com' }, { type: 'Tumblr', src: SocialIconsBasePath + 'tumblr_004.png', url: 'www.tumblr.com' }, { type: 'Twitter', src: SocialIconsBasePath + 'twitter_004.png', url: 'www.twitter.com' }, { type: 'YouTube', src: SocialIconsBasePath + 'youtube_004.png', url: 'www.youtube.com' }] },
]

const DefaultTemplateStylesheet = {
    'background-color': 'transparent',
    'font-family': 'Trebuchet MS',
    'color': '#000',
    'font-size': '14px',
}

const DefaultLinkStylesheet = {
    'color': '#337ab7',
    'text-decoration': 'none',
    'hover-color': '#23527c',
    'hover-text-decoration': 'underline'
}

const DefaultRowStylesheet = {
    'background-color': 'transparent'
}

export const DefaultColumnStylesheet = {
    'background-color': 'transparent',
    'padding-top': '10px',
    'padding-bottom': '10px',
    'padding-left': '10px',
    'padding-right': '10px',
    'border-radius': '0px',
    'border-top-style': 'None',
    'border-top-width': '0px',
    'border-top-color': 'transparent',
    'border-bottom-style': 'None',
    'border-bottom-width': '0px',
    'border-bottom-color': 'transparent',
    'border-left-style': 'None',
    'border-left-width': '0px',
    'border-left-color': 'transparent',
    'border-right-style': 'None',
    'border-right-width': '0px',
    'border-right-color': 'transparent',
}

const DefaultBlockStylesheet = {
    'background-color': 'transparent',
    'padding-top': '10px',
    'padding-bottom': '10px',
    'padding-left': '10px',
    'padding-right': '10px',
    'border-radius': '0px',
    'border-top-style': 'None',
    'border-top-width': '0px',
    'border-top-color': 'transparent',
    'border-bottom-style': 'None',
    'border-bottom-width': '0px',
    'border-bottom-color': 'transparent',
    'border-left-style': 'None',
    'border-left-width': '0px',
    'border-left-color': 'transparent',
    'border-right-style': 'None',
    'border-right-width': '0px',
    'border-right-color': 'transparent',
    'line-height': 'normal',
}

const DefaultButtonContentStylesheet = {
    'background-color': '#CCC',
    'padding-top': '10px',
    'padding-bottom': '10px',
    'padding-left': '10px',
    'padding-right': '10px',
    'border-radius': '1000px',
    'border-top-style': 'Solid',
    'border-top-width': '1px',
    'border-top-color': '#aaa',
    'border-bottom-style': 'Solid',
    'border-bottom-width': '1px',
    'border-bottom-color': '#aaa',
    'border-left-style': 'Solid',
    'border-left-width': '1px',
    'border-left-color': '#aaa',
    'border-right-style': 'Solid',
    'border-right-width': '1px',
    'border-right-color': '#aaa',
    'line-height': 'normal',
}

const DefaultButtonStylesheet = {
    'text-align': 'center',
    'cursor': 'pointer',
    'padding-top': '5px',
    'padding-bottom': '5px',
    'padding-left': '5px',
    'padding-right': '5px',
}

const DefaultDividerStylesheet = {
    'border-width': '1px',
    'border-color': '#000',
    'border-style': 'solid',
    'width': '100%'
}

const DefaultProductStylesheet = {

}

const DefaultAddressStylesheet = {

}

const DefaultTableStylesheet = {

}

export const Blocks = [
    { name: 'TextBlock', obj: new TemplateBlockText({ Styles: Object.assign({}, DefaultBlockStylesheet), Type: 'TextBlock', Content: '<p>Click here to edit text block</p>' }) },
    { name: 'Button', obj: new TemplateBlockButton({ Styles: Object.assign({}, DefaultButtonStylesheet), Type: 'Button', ContentStyles: Object.assign({}, DefaultButtonContentStylesheet), Content: '<p>Click me</p>' }) },
    { name: 'Divider', obj: new TemplateBlockDivider({ Styles: Object.assign({}, DefaultBlockStylesheet), ContentStyles: Object.assign({}, DefaultDividerStylesheet), Type: 'Divider' }) },
    { name: 'Image', obj: new TemplateBlockImage({ Styles: Object.assign({}, DefaultBlockStylesheet), BlobUrl: '/assets/images/placeholder.jpg', AltText: 'Placeholder', Type: 'Image' }) },
    {
        name: 'Social', obj: new TemplateBlockSocial({
            Styles: Object.assign({}, DefaultBlockStylesheet), Spacing: '5px', Type: 'Social', IconCollectionId: '004', Icons: [
                { Type: 'Facebook', LinkUrl: 'www.facebook.com', AltText: 'Facebook', ImgSrc: '/assets/social-media/facebook_004.png' },
                { Type: 'Instagram', LinkUrl: 'www.instagram.com', AltText: 'Instagram', ImgSrc: '/assets/social-media/instagram_004.png' },
                { Type: 'LinkedIn', LinkUrl: 'www.linkedin.com', AltText: 'LinkedIn', ImgSrc: '/assets/social-media/linkedin_004.png' },
            ]
        })
    },
    { name: 'Unsubscribe', obj: new TemplateBlockUnsubscribe({ Styles: Object.assign({}, DefaultBlockStylesheet), Type: 'Unsubscribe', Content: '<a onclick href="unsubscribe">Click here to unsubscribe</a>' }) },
    { name: 'Product', obj: new TemplateBlockProduct({ Styles: Object.assign({}, DefaultProductStylesheet), ProductStyles: { TableStyles: { 'width': '100%' }, HeaderRowStyles: { 'font-weight': 'bold' }, RowStyles: {}, TotalTableStyles: { 'margin-top': '10px' }, TotalTableAlign: 'right', LeftColumnStyles: {}, RightColumnStyles: {} }, Type: 'Product', Content: 'Hej', TotalStyles: { 'width': '30%' }, Columns: [{ Title: 'NAME', Fields: ['[Product]'], WidthRatio: 4, CellAlign: 'left', Styles: {} }, { Title: 'QUANTITY', Fields: ['[Units]'], WidthRatio: 1, CellAlign: 'right', Styles: {} }, { Title: 'PRICE', Fields: ['[Price]'], WidthRatio: 1, CellAlign: 'right', Styles: {} }, { Title: 'SUMAMOUNT', Fields: ['[Sum]'], WidthRatio: 1, CellAlign: 'right', Styles: {} }], SumRows: [{ Title: 'AMOUNTBEFOREVAT', Fields: ['[AmountBeforeVat]'], Styles: {} }, { Title: 'VAT', Fields: ['[Vat]'], Styles: {} }, { Title: 'TOTALAMOUNT', Fields: ['[TotalAmount]'], Styles: {} }] }) },
    { name: 'Address', obj: new TemplateBlockAddress({ Styles: Object.assign({}, DefaultAddressStylesheet), Type: 'Address', AddressBlock: { Title: 'ADDRESSTITLE', BindTo: 'Our', AddressType: 'Billing', ContentStyles: {}, Rows: [{ Field: '[Name]', Styles: {} }, { Field: '[StreetAddress]', Styles: {} }, { Field: '[ZipCode] [City]', Styles: {} }, { Field: '[Country]', Styles: {} }] } }) },
    { name: 'Table', obj: new TemplateBlockTable({ Styles: Object.assign({}, DefaultTableStylesheet), Type: 'Table', TableBlock: { Columns: 2, Rows: 2, Cells: [[{ Content: 'Row1Col1', Styles: {} }, { Content: 'Row1Col2', Styles: {} }], [{ Content: 'Row2Col1', Styles: {} }, { Content: 'Row2Col2', Styles: {} }]] } }) }
]

export const DefaultRows = [
    {
        colArray: [12], obj: new TemplateRowModel({
            TemplateId: '', ContentBackground: 'transparent', Styles: Object.assign({}, DefaultRowStylesheet), Columns: [
                new TemplateColumnModel({
                    TemplateId: '', Styles: Object.assign({}, DefaultColumnStylesheet), WidthRatio: 12, Blocks: []
                })
            ]
        })
    },
    {
        colArray: [6, 6], obj: new TemplateRowModel({
            TemplateId: '', ContentBackground: 'transparent', Styles: Object.assign({}, DefaultRowStylesheet), Columns: [
                new TemplateColumnModel({
                    TemplateId: '', Styles: Object.assign({}, DefaultColumnStylesheet), WidthRatio: 6, Blocks: []
                }),
                new TemplateColumnModel({
                    TemplateId: '', Styles: Object.assign({}, DefaultColumnStylesheet), WidthRatio: 6, Blocks: []
                })
            ]
        })
    },
    {
        colArray: [4, 4, 4], obj: new TemplateRowModel({
            TemplateId: '', ContentBackground: 'transparent', Styles: Object.assign({}, DefaultRowStylesheet), Columns: [
                new TemplateColumnModel({
                    TemplateId: '', Styles: Object.assign({}, DefaultColumnStylesheet), WidthRatio: 4, Blocks: []
                }),
                new TemplateColumnModel({
                    TemplateId: '', Styles: Object.assign({}, DefaultColumnStylesheet), WidthRatio: 4, Blocks: []
                }),
                new TemplateColumnModel({
                    TemplateId: '', Styles: Object.assign({}, DefaultColumnStylesheet), WidthRatio: 4, Blocks: []
                })
            ]
        })
    },
    {
        colArray: [3, 3, 3, 3], obj: new TemplateRowModel({
            TemplateId: '', ContentBackground: 'transparent', Styles: Object.assign({}, DefaultRowStylesheet), Columns: [
                new TemplateColumnModel({
                    TemplateId: '', Styles: Object.assign({}, DefaultColumnStylesheet), WidthRatio: 3, Blocks: []
                }),
                new TemplateColumnModel({
                    TemplateId: '', Styles: Object.assign({}, DefaultColumnStylesheet), WidthRatio: 3, Blocks: []
                }),
                new TemplateColumnModel({
                    TemplateId: '', Styles: Object.assign({}, DefaultColumnStylesheet), WidthRatio: 3, Blocks: []
                }),
                new TemplateColumnModel({
                    TemplateId: '', Styles: Object.assign({}, DefaultColumnStylesheet), WidthRatio: 3, Blocks: []
                })
            ]
        })
    },
    {
        colArray: [8, 4], obj: new TemplateRowModel({
            TemplateId: '', ContentBackground: 'transparent', Styles: Object.assign({}, DefaultRowStylesheet), Columns: [
                new TemplateColumnModel({
                    TemplateId: '', Styles: Object.assign({}, DefaultColumnStylesheet), WidthRatio: 8, Blocks: []
                }),
                new TemplateColumnModel({
                    TemplateId: '', Styles: Object.assign({}, DefaultColumnStylesheet), WidthRatio: 4, Blocks: []
                })
            ]
        })
    },
    {
        colArray: [4, 8], obj: new TemplateRowModel({
            TemplateId: '', ContentBackground: 'transparent', Styles: Object.assign({}, DefaultRowStylesheet), Columns: [
                new TemplateColumnModel({
                    TemplateId: '', Styles: Object.assign({}, DefaultColumnStylesheet), WidthRatio: 4, Blocks: []
                }),
                new TemplateColumnModel({
                    TemplateId: '', Styles: Object.assign({}, DefaultColumnStylesheet), WidthRatio: 8, Blocks: []
                })
            ]
        })
    },
    {
        colArray: [6, 3, 3], obj: new TemplateRowModel({
            TemplateId: '', ContentBackground: 'transparent', Styles: Object.assign({}, DefaultRowStylesheet), Columns: [
                new TemplateColumnModel({
                    TemplateId: '', Styles: Object.assign({}, DefaultColumnStylesheet), WidthRatio: 6, Blocks: []
                }),
                new TemplateColumnModel({
                    TemplateId: '', Styles: Object.assign({}, DefaultColumnStylesheet), WidthRatio: 3, Blocks: []
                }),
                new TemplateColumnModel({
                    TemplateId: '', Styles: Object.assign({}, DefaultColumnStylesheet), WidthRatio: 3, Blocks: []
                })
            ]
        })
    },
    {
        colArray: [3, 6, 3], obj: new TemplateRowModel({
            TemplateId: '', ContentBackground: 'transparent', Styles: Object.assign({}, DefaultRowStylesheet), Columns: [
                new TemplateColumnModel({
                    TemplateId: '', Styles: Object.assign({}, DefaultColumnStylesheet), WidthRatio: 3, Blocks: []
                }),
                new TemplateColumnModel({
                    TemplateId: '', Styles: Object.assign({}, DefaultColumnStylesheet), WidthRatio: 6, Blocks: []
                }),
                new TemplateColumnModel({
                    TemplateId: '', Styles: Object.assign({}, DefaultColumnStylesheet), WidthRatio: 3, Blocks: []
                })
            ]
        })
    },
    {
        colArray: [3, 3, 6], obj: new TemplateRowModel({
            TemplateId: '', ContentBackground: 'transparent', Styles: Object.assign({}, DefaultRowStylesheet), Columns: [
                new TemplateColumnModel({
                    TemplateId: '', Styles: Object.assign({}, DefaultColumnStylesheet), WidthRatio: 3, Blocks: []
                }),
                new TemplateColumnModel({
                    TemplateId: '', Styles: Object.assign({}, DefaultColumnStylesheet), WidthRatio: 3, Blocks: []
                }),
                new TemplateColumnModel({
                    TemplateId: '', Styles: Object.assign({}, DefaultColumnStylesheet), WidthRatio: 6, Blocks: []
                })
            ]
        })
    },
    {
        colArray: [5, 2, 5], obj: new TemplateRowModel({
            TemplateId: '', ContentBackground: 'transparent', Styles: Object.assign({}, DefaultRowStylesheet), Columns: [
                new TemplateColumnModel({
                    TemplateId: '', Styles: Object.assign({}, DefaultColumnStylesheet), WidthRatio: 5, Blocks: []
                }),
                new TemplateColumnModel({
                    TemplateId: '', Styles: Object.assign({}, DefaultColumnStylesheet), WidthRatio: 2, Blocks: []
                }),
                new TemplateColumnModel({
                    TemplateId: '', Styles: Object.assign({}, DefaultColumnStylesheet), WidthRatio: 5, Blocks: []
                })
            ]
        })
    },
]

export const BodyStyleInputs = {
    Newsletter: {
        mobile: { text: true, h1: true, h2: true, h3: true, h4: true, h5: true, h6: true },
        body: { width: true, bgcolor: true, cbgcolor: true, font: true, fontsize: true, fontcolor: true, link: true }
    },
    Document: {
        body: { font: true, fontsize: true, fontcolor: true }
    }
};

export const RowStyleInputs = {
    Newsletter: { bgcolor: true, cbgcolor: true, colbgcolor: true, padding: true, borderradius: true, border: true },
    Document: { padding: true, borderradius: true, border: true }
};

export const BlockTypes = {
    Newsletter: { TextBlock: true, Button: true, Divider: true, Social: true, Image: true, Unsubscribe: true },
    Document: { TextBlock: true, Image: true, Product: true, Address: true, Table: true }
}

export const BlockStyleInputs = {
    TextBlock: {
        Newsletter: { bgcolor: true, align: true, font: true, fontsize: true, fontcolor: true, link: true, padding: true, border: true },
        Document: { align: true, font: true, fontsize: true, fontcolor: true, padding: true, border: true }
    },
    Button: {
        Newsletter: { font: true, fontsize: true, fontcolor: true, align: true, url: true, cbgcolor: true, width: true, cpadding: true, style: true, cborder: true, bgcolor: true, padding: true, border: true },
    },
    Divider: {
        Newsletter: { border: true, bradius: true, bgcolor: true, width: true, align: true, padding: true },
    },
    Image: {
        Newsletter: { bgcolor: true, align: true, alt: true, url: true, width: true, height: true, padding: true, border: true },
        Document: { align: true, width: true, height: true, padding: true, border: true }
    },
    Social: {
        Newsletter: { bgcolor: true, align: true, spacing: true, url: true, alt: true, padding: true, border: true },
    },
    Unsubscribe: {
        Newsletter: { bgcolor: true, font: true, fontsize: true, fontcolor: true, align: true, padding: true, border: true },
    },
    Product: {
        Document: { width: true, tabalign: true, tabfont: true, tabfontsize: true, tabfontcolor: true, tabborder: true, rowfont: true, rowfontsize: true, rowfontcolor: true, rowtextstyle: true, rowheadfont: true, rowheadfontsize: true, rowheadfontcolor: true, rowheadtextstyle: true, colpadding: true }
    }
}

export const DefaultMailTemplate = new TemplateModel({
    ContentBackground: 'transparent', Styles: DefaultTemplateStylesheet, LinkStyles: DefaultLinkStylesheet, ContentWidth: 800, Type: 'Newsletter', Rows: []
});

export const DefaultDocumentTemplate = new TemplateDocumentModel({
    ContentBackground: 'transparent', Styles: DefaultTemplateStylesheet, LinkStyles: DefaultLinkStylesheet, ContentWidth: 800, Type: 'Document', Rows: []
});