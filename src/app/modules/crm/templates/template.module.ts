import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';

import { TemplateBuilderComponent } from './template-builder.component';
import { TemplateSaveComponent } from './save/template-save.component';
import { TemplateRedactorComponent } from './styling/styling-helpers/template-redactor.component';
import { TemplateColorpickerComponent } from './styling/styling-helpers/template-colorpicker.component';
import { TemplatePlusMinusComponent } from './styling/styling-helpers/template-plusminus.component';
import { TemplateBorderComponent } from './styling/styling-helpers/template-border.component';
import { TemplateAlignComponent } from './styling/styling-helpers/template-align.component';
import { TemplateBorderStylingComponent } from './styling/styling-helpers/template-border-styling.component';
import { TemplatePaddingStylingComponent } from './styling/styling-helpers/template-padding-styling.component';
import { TemplateBodyStylingComponent } from './styling/main-blocks/template-body-styling.component';
import { TemplateButtonStylingComponent } from './styling/block-styling/template-button-styling.component';
import { TemplateDividerStylingComponent } from './styling/block-styling/template-divider-styling.component';
import { TemplateImageStylingComponent } from './styling/block-styling/template-image-styling.component';
import { TemplateRowStylingComponent } from './styling/main-blocks/template-row-styling.component';
import { TemplateSocialStylingComponent } from './styling/block-styling/template-social-styling.component';
import { TemplateTextblockStylingComponent } from './styling/block-styling/template-textblock-styling.component';
import { TemplateUnsubscribeStylingComponent } from "./styling/block-styling/template-unsubscribe-styling.component";
import { TemplateQuickSelectComponent } from "./styling/styling-helpers/template-quickselect.component";
import { TemplateBlockStylingComponent } from "./styling/main-blocks/template-block-styling.component";
import { TemplateLinkComponent } from "./styling/styling-helpers/template-link.component";
import { TemplateLinkHoverComponent } from "./styling/styling-helpers/template-link-hover.component";
import { TemplateMainComponent } from './scaffolding/template-main/template-main.component';
import { TemplateProductStylingComponent } from './styling/block-styling/template-product-styling.component';
import { TemplateTextStyleComponent } from './styling/styling-helpers/template-text-style.component';
import { TemplateRowBuilderComponent } from './template-row-builder.component';
import { TemplateTableBorderComponent } from './styling/styling-helpers/template-table-border.component';
import { TemplateTableBorderStyleComponent } from './styling/styling-helpers/template-table-border-style.component';
import { TemplateAddressStylingComponent } from './styling/block-styling/template-address-styling.component';
import { TemplateTableStylingComponent } from './styling/block-styling/template-table-styling.component';

@NgModule({
    imports: [
        SharedModule
    ],
    exports: [
        TemplateBuilderComponent,
        TemplateSaveComponent,
        TemplateRedactorComponent,
        TemplateColorpickerComponent,
        TemplatePlusMinusComponent,
        TemplateBorderComponent,
        TemplateAlignComponent,
        TemplateBorderStylingComponent,
        TemplatePaddingStylingComponent,
        TemplateBodyStylingComponent,
        TemplateButtonStylingComponent,
        TemplateDividerStylingComponent,
        TemplateImageStylingComponent,
        TemplateRowStylingComponent,
        TemplateSocialStylingComponent,
        TemplateTextblockStylingComponent,
        TemplateUnsubscribeStylingComponent,
        TemplateQuickSelectComponent,
        TemplateBlockStylingComponent,
        TemplateLinkComponent,
        TemplateLinkHoverComponent,
        TemplateMainComponent
    ],
    declarations: [
        TemplateBuilderComponent,
        TemplateSaveComponent,
        TemplateRedactorComponent,
        TemplateColorpickerComponent,
        TemplatePlusMinusComponent,
        TemplateBorderComponent,
        TemplateAlignComponent,
        TemplateBorderStylingComponent,
        TemplatePaddingStylingComponent,
        TemplateBodyStylingComponent,
        TemplateButtonStylingComponent,
        TemplateDividerStylingComponent,
        TemplateImageStylingComponent,
        TemplateRowStylingComponent,
        TemplateSocialStylingComponent,
        TemplateTextblockStylingComponent,
        TemplateUnsubscribeStylingComponent,
        TemplateQuickSelectComponent,
        TemplateBlockStylingComponent,
        TemplateLinkComponent,
        TemplateLinkHoverComponent,
        TemplateMainComponent,
        TemplateProductStylingComponent,
        TemplateTextStyleComponent,
        TemplateRowBuilderComponent,
        TemplateTableBorderComponent,
        TemplateTableBorderStyleComponent,
        TemplateAddressStylingComponent,
        TemplateTableStylingComponent
    ],
})
export class TemplateModule { }