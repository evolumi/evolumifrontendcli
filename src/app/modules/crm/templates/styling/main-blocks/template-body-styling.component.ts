import { Component, OnInit, Input } from '@angular/core';
import { TemplateModel } from "../../../../../models/template.models";
import { TemplateBuilderService } from "../../../../../services/template-builder.service";
import { BodyStyleInputs } from "../../template-settings";

@Component({
    selector: 'template-body-styling',
    templateUrl: './template-body-styling.component.html',
    styleUrls: ['../template-styling.css']
})

export class TemplateBodyStylingComponent implements OnInit {

    @Input() template: TemplateModel
    @Input() settings: any;
    @Input() bodyId: string = 'template-body'
    @Input() type: string;

    private styleElement: any;
    public inputs: any = {};

    private linkHoverCss: string = '';
    private linkCss: string = '';

    public show: string;
    public showTabs: boolean;

    constructor(private templateSvc: TemplateBuilderService) { }

    ngOnInit() {
        this.getShownInputs();
        this.show = 'body';
    }

    getShownInputs(){
        switch (this.type){
            case 'Newsletter':
                this.inputs = BodyStyleInputs.Newsletter;
                this.showTabs = true;
                break;
            case 'Document':
                this.inputs = BodyStyleInputs.Document;
                this.showTabs = false;
                break;
        }
    }

    setShowStyle(event: Event, show: string) {
        event.stopPropagation();
        if (this.show === show) this.show = '';
        else this.show = show;
    }

    linkChange(type: string, css: string) {
        switch (type) {
            case 'hover':
                this.linkHoverCss = `#template-body a:hover {${css}}`;
                break;
            case 'normal':
            default:
                this.linkCss = `#template-body a {${css}}`;
                break;
        }
        if (!this.styleElement) {
            this.styleElement = document.createElement('style');
            this.templateSvc.styleElements.push(this.styleElement);
            document.getElementsByTagName('head')[0].appendChild(this.styleElement);
        }
        if (this.styleElement.styleSheet) {
            this.styleElement.styleSheet.cssText = this.linkCss + this.linkHoverCss;
        } else {
            this.styleElement.innerHTML = '';
            this.styleElement.appendChild(document.createTextNode(this.linkCss + this.linkHoverCss));
        }
    }
}