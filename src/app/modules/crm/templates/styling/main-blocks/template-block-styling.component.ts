import { Component, OnInit, Input } from '@angular/core';
import { Subscription } from "rxjs/Subscription";
import { TemplateBuilderService } from "../../../../../services/template-builder.service";
import { TemplateBlockModel, TemplateModel } from "../../../../../models/template.models";
import { Blocks, BlockTypes } from "../../template-settings";

@Component({
    selector: 'template-block-styling',
    templateUrl: './template-block-styling.component.html',
    styleUrls: ['../template-styling.css']
})

export class TemplateBlockStylingComponent implements OnInit {

    @Input() template: TemplateModel;
    @Input() settings: any;
    @Input() type: string;

    public block: TemplateBlockModel;

    public show: string;
    public blockTypes: any;

    public defaultBlocks: Array<TemplateBlockModel>;
    public blocks: Array<any>;

    private blockSub: Subscription;

    constructor(private templateSvc: TemplateBuilderService) { }

    ngOnInit() {
        this.getShownBlocks();
        this.blockSub = this.templateSvc.currentTemplateBlockObs()
            .subscribe(block => {
                this.block = block;
                if (block) {
                    this.show = 'style';
                }
            });
    }

    ngOnDestroy() {
        if (this.blockSub) this.blockSub.unsubscribe();
    }
    
    setShowStyle(event: Event, show: string) {
        event.stopPropagation();
        if (this.show === show) this.show = '';
        else this.show = show;
    }

    changeBlockType(event: Event, type: string) {
        event.stopPropagation();
        if (this.block && this.block.Type !== type) {
            let block = this.defaultBlocks.find(x => x.Type === type);
            this.setNewBlock(block, this.block.TemplateId);
            this.templateSvc.setCurrentTemplateBlock(block);
        }
    }

    getShownBlocks() {
        switch (this.type) {
            case 'Newsletter':
                this.blockTypes = BlockTypes.Newsletter;
                break;
            case 'Document':
                this.blockTypes = BlockTypes.Document;
                break;
        }
        this.blocks = Blocks.filter(x => this.blockTypes[x.name]);
        this.defaultBlocks = this.blocks.map(x => x.obj);
    }

    private setNewBlock(block: TemplateBlockModel, id: string) {
        let found = false;
        for (let i = 0; i < this.template.Rows.length; i++) {
            for (let j = 0; j < this.template.Rows[i].Columns.length; j++) {
                for (let k = 0; k < this.template.Rows[i].Columns[j].Blocks.length; k++) {
                    if (this.template.Rows[i].Columns[j].Blocks[k].TemplateId === id) {
                        found = true;
                        this.template.Rows[i].Columns[j].Blocks[k] = block;
                    }
                    if (found) break;
                }
                if (found) break;
            }
            if (found) break;
        }
    }
}