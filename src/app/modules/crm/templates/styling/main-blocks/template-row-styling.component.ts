import { Component, OnInit, Input } from '@angular/core';
import { TemplateRowModel, TemplateColumnModel } from "../../../../../models/template.models";
import { TemplateBuilderService } from "../../../../../services/template-builder.service";
import { DefaultRows, DefaultColumnStylesheet, RowStyleInputs } from "../../template-settings";
import { Subscription } from "rxjs/Subscription";

@Component({
    selector: 'template-row-styling',
    templateUrl: './template-row-styling.component.html',
    styleUrls: ['../template-styling.css']
})

export class TemplateRowStylingComponent implements OnInit {

    @Input() settings: any;
    @Input() type: string;

    public column: TemplateColumnModel;
    public row: TemplateRowModel;

    public show: string;
    public rows: Array<any>;
    public defaultRows: Array<TemplateRowModel>;
    public inputs: any = {};

    private rowSub: Subscription;
    private colSub: Subscription;

    constructor(private templateSvc: TemplateBuilderService) { }

    ngOnInit() {
        this.defaultRows = DefaultRows.map((x: any) => x.obj);
        this.rows = DefaultRows;
        this.getShownInputs();
        this.rowSub = this.templateSvc.currentTemplateRowObs()
            .subscribe(row => {
                this.row = row;
                if (row) {
                    this.show = 'style';
                }
            });
        this.colSub = this.templateSvc.currentTemplateColObs()
            .subscribe(col => {
                this.column = col;
            });
    }

    ngOnDestroy(){
        if (this.rowSub) this.rowSub.unsubscribe();
        if (this.colSub) this.colSub.unsubscribe();
    }

    changeRowLayout(event: any, index: number) {
        event.stopPropagation();
        if (this.row) {
            let newCols = this.defaultRows[index].Columns;
            let cols = this.row.Columns;
            // get the blocks that are in the deleted column and put in the last one
            if (newCols.length < cols.length) {
                let blocks = cols.filter((x, index) => index >= newCols.length).map(x => x.Blocks.map(y => y)).reduce((curr, prev) => prev.concat(curr));
                cols = cols.slice(0, newCols.length);
                cols[newCols.length - 1].Blocks.push(...blocks);
            }

            for (let i = 0; i < newCols.length; i++) {
                if (cols[i]) {
                    cols[i].WidthRatio = newCols[i].WidthRatio;
                } else {
                    cols.push(new TemplateColumnModel({ TemplateId: this.getNewId(), Styles: DefaultColumnStylesheet, WidthRatio: newCols[i].WidthRatio }));
                }
            }
            this.row.Columns = cols;
            console.log('COLS', this.row.Columns);
        }
    }

    setShowStyle(event: Event, show: string) {
        event.stopPropagation();
        if (this.show === show) this.show = '';
        else this.show = show;
    }

    getShownInputs(){
        switch (this.type){
            case 'Newsletter':
                this.inputs = RowStyleInputs.Newsletter;
                break;
            case 'Document':
                this.inputs = RowStyleInputs.Document;
                break;
        }
    }

    private getNewId() {
        return Math.random().toString(36).slice(-8);
    }

    onClickColumn(index: number) {
        this.templateSvc.setCurrentTemplateColumn(this.row.Columns[index]);
    }
}