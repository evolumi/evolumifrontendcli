import { Component, OnInit, Input, Output, ViewChild, ElementRef, Renderer, EventEmitter, OnDestroy } from '@angular/core';
import { TemplateBlockText, TemplateBlockButton, TableCell } from '../../../../../models/template.models';
import { TemplateBuilderService } from "../../../../../services/template-builder.service";
declare let $: any

@Component({
    selector: 'template-redactor',
    templateUrl: './template-redactor.component.html',
    styleUrls: ['../../template-builder.css']
})

export class TemplateRedactorComponent implements OnInit, OnDestroy {

    @Input() obj: TemplateBlockText | TemplateBlockButton | TableCell;
    @Input() linkStyles: any;
    @Output() destroy = new EventEmitter();
    @ViewChild('redEl') redEl: ElementRef;
    @ViewChild('toolbar') toolbarEl: ElementRef

    public topPos: number = 0;
    public leftPos: number = 0;
    private blurDestroy: boolean;

    constructor(private renderer: Renderer,
        private templateSvc: TemplateBuilderService) { }

    ngOnInit() {
    }

    ngAfterViewInit() {
        let element = this.redEl.nativeElement;
        let rect = element.getBoundingClientRect();
        this.renderer.setElementStyle(this.toolbarEl.nativeElement, 'top', rect.top - 60 + 'px');
        this.renderer.setElementStyle(this.toolbarEl.nativeElement, 'left', rect.left + 'px');
        this.renderer.setElementStyle(this.toolbarEl.nativeElement, 'z-index', '1');
        this.topPos = rect.top + 48;
        this.leftPos = rect.left
        let contentElement = $(element);
        if (contentElement) {
            var self = this;
            contentElement.redactor({
                plugins: ['fontsize', 'fontfamily', 'fontcolor', 'alignment'],
                // focus: true,
                focusEnd: true,
                callbacks: {
                    change: function () {
                        self.obj.Content = this.code.get();
                    },
                    blur: function () {
                        this.core.destroy();
                        self.blurDestroy = true;
                        self.destroy.emit();
                    },
                    insertedLink: function (el: any) {
                        el.css(self.linkStyles);
                    },
                    modalOpened: function () {
                        self.templateSvc.redactorModalActive = true;
                    },
                    modalClosed: function () {
                        self.templateSvc.redactorModalActive = false;
                    }
                },
                toolbarExternal: '#toolbar'
            });
            contentElement.redactor('code.set', this.obj.Content);
        }
    }

    ngOnDestroy() {
        if (!this.blurDestroy) {
            this.destroy.emit();
        }
    }

    stop(event: Event) {
        event.stopPropagation();
    }
}