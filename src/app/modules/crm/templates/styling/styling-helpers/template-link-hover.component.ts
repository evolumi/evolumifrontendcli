import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'template-link-hover',
    templateUrl: './template-link-hover.component.html',
    styleUrls: ['../template-styling.css']
})

export class TemplateLinkHoverComponent implements OnInit {

    @Input() styleObj: any;
    @Output() linkChange = new EventEmitter<string>();

    constructor() { }

    ngOnInit() {

    }

    linkHoverChange() {
        let hoverCss = '';
        for (let prop in this.styleObj) {
            if (this.styleObj.hasOwnProperty(prop) && prop.indexOf('hover') > -1) {
                hoverCss += `${prop.replace('hover-', '')}: ${this.styleObj[prop]};`;
            }
        }
        this.linkChange.emit(hoverCss);
    }

    setLinkHoverStyle() {
        this.styleObj['hover-text-decoration'] = this.styleObj['hover-text-decoration'] === 'underline' ? 'none' : 'underline';
        this.linkHoverChange();
    }
}