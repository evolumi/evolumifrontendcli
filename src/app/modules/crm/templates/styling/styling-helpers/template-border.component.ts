import { Component, OnInit, Input } from '@angular/core';
import { ChosenOptionModel } from '../../../../shared/components/chosen/chosen.models';
import { Subject } from 'rxjs/Subject';
import { Subscription } from "rxjs/Subscription";

@Component({
    selector: 'template-border',
    templateUrl: './template-border.component.html',
    styleUrls: ['../template-styling.css']
})

export class TemplateBorderComponent implements OnInit {

    @Input() styleObj: any;
    @Input() direction: string; // 'left', 'top'
    @Input() max: number = 100;
    @Input() min: number = 0;
    @Input() step: number = 1;
    @Input() unit: string;
    @Input() borderStyles: Array<ChosenOptionModel>;
    @Input() cpPosition: string = 'bottom' // for colorpickerposition
    @Input() containerClass: string = 'border-container';
    @Input() isAll: boolean; // if change all sides of property
    @Input() onlyBorder: boolean; // if only use 'border' not 'border-top'...

    public unitLength: number; // chars of unit, for substringing

    // private keyup$ = new Subject<HTMLInputElement>();
    private keyupAll$ = new Subject<HTMLInputElement>();

    private keySub: Subscription;

    constructor() { }

    ngOnInit() {
        // this.keyup$.debounceTime(500).distinctUntilChanged((next, prev) => next.value === prev.value)
        //     .subscribe(el => {
        //         let propName = this.onlyBorder ? 'border-width' : `border-${this.direction}-width`;
        //         if (!this.styleObj[propName] || this.styleObj[propName].toString().replace(this.unit, '') !== el.value) {
        //             this.setAndValidateStyle(el.value);
        //             el.value = this.styleObj[propName].toString().replace(this.unit, '');
        //         }
        //     });

        this.keySub = this.keyupAll$.debounceTime(500).distinctUntilChanged((next, prev) => next.value === prev.value)
            .subscribe(el => {
                if (this.styleObj['border-top-width'] || this.styleObj['border-top-width'].toString().replace(this.unit, '') !== el.value) {
                    this.setAndValidateAllStyles(el.value);
                    el.value = this.styleObj['border-top-width'].toString().replace(this.unit, '');
                }
            });

        this.unitLength = this.unit ? this.unit.length : 0;
    }

    ngOnDestroy(){
        if (this.keySub) this.keySub.unsubscribe();
    }

    onKeyupAll(element: HTMLInputElement) {
        this.keyupAll$.next(element);
    }

    setAndValidateAllStyles(value: string) {
        let numValue = Number(value);
        if (numValue > this.max) numValue = this.max;
        else if (numValue < this.min) numValue = this.min;
        let propValue = numValue + (this.unit || '');
        this.styleObj['border-top-width'] = propValue;
        this.styleObj['border-bottom-width'] = propValue;
        this.styleObj['border-left-width'] = propValue;
        this.styleObj['border-right-width'] = propValue;
    }

    setAllProps(prop: string, value: any) {
        this.styleObj['border-top-' + prop] = value;
        this.styleObj['border-bottom-' + prop] = value;
        this.styleObj['border-left-' + prop] = value;
        this.styleObj['border-right-' + prop] = value;
    }

    addToAllStyles(add: boolean) {
        let value = Number(add ? this.step : -this.step);
        let styleValue = this.styleObj['border-top-width'] ? Number(this.styleObj['border-top-width'].toString().replace(this.unit, '')) + value : value;

        if (styleValue > this.max) styleValue = this.max;
        else if (styleValue < this.min) styleValue = this.min;

        let propValue = styleValue + (this.unit || '');
        this.styleObj['border-top-width'] = propValue;
        this.styleObj['border-bottom-width'] = propValue;
        this.styleObj['border-left-width'] = propValue;
        this.styleObj['border-right-width'] = propValue;
    }
}