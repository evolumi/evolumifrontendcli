import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'template-quickselect',
    templateUrl: './template-quickselect.component.html',
    styleUrls: ['../template-styling.css']
})

export class TemplateQuickSelectComponent implements OnInit {

    @Input() styleObj: any;
    @Input() prop: string;
    @Input() items: Array<any>;
    @Input() placeholderLabel: string = '';
    @Output() select = new EventEmitter<string>();

    constructor() { }

    ngOnInit() {

    }

    onSelect(value: string){
        this.styleObj[this.prop] = value;
        this.select.emit(value);
    }
}