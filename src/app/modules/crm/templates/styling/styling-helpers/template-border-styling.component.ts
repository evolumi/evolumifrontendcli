import { Component, OnInit, Input, SimpleChanges } from '@angular/core';

@Component({
    selector: 'template-border-styling',
    templateUrl: './template-border-styling.component.html',
    styleUrls: ['../template-styling.css']
})

export class TemplateBorderStylingComponent implements OnInit {

    @Input() styleObj: any;
    @Input() settings: any;
    @Input() onlyAll: boolean;
    @Input() prefixLabel: string = '';

    public showAll: boolean;

    constructor() { }

    ngOnInit() {
        if (!this.onlyAll) {
            this.showAll = this.checkIfAllSidesAreSame();
        }
    }

    ngOnChanges(changes: SimpleChanges) {
        if (!this.onlyAll && changes && changes['styleObj']) {
            this.showAll = this.checkIfAllSidesAreSame();
        }
    }

    showAllSides(event: Event) {
        event.stopPropagation();

        let style = this.styleObj['border-top-style'];
        let width = this.styleObj['border-top-width'];
        let color = this.styleObj['border-top-color'];

        this.styleObj['border-bottom-style'] = style;
        this.styleObj['border-right-style'] = style;
        this.styleObj['border-left-style'] = style;
        this.styleObj['border-bottom-width'] = width;
        this.styleObj['border-right-width'] = width;
        this.styleObj['border-left-width'] = width;
        this.styleObj['border-bottom-color'] = color;
        this.styleObj['border-right-color'] = color;
        this.styleObj['border-left-color'] = color;

        this.showAll = !this.showAll
    }

    private checkIfAllSidesAreSame() {
        const dirArr = ['-top-', '-bottom-', '-left-', '-right-'];
        let isDifferent = false;
        let width = this.styleObj['border-top-width'];
        let style = this.styleObj['border-top-style'];
        let color = this.styleObj['border-top-color'];
        for (let i = 1; i < dirArr.length; i++) {
            if (width !== this.styleObj['border' + dirArr[i] + 'width'] || style !== this.styleObj['border' + dirArr[i] + 'style'] || color !== this.styleObj['border' + dirArr[i] + 'color']) {
                isDifferent = true;
                i = 5;
            }
        }

        return isDifferent;
    }
}