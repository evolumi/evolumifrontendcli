import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-template-table-border',
  templateUrl: './template-table-border.component.html',
  styleUrls: ['./template-table-border.component.css']
})
export class TemplateTableBorderComponent implements OnInit {

  @Input() type: string;
  @Input() selected: string;
  @Output() selectedChange = new EventEmitter<string>();

  public borderArray: Array<string> = []; // containg the types wanted from templatetableborderstylingcomonent

  constructor() { }

  ngOnInit() {
    switch (this.type){
      case 'Product':
        this.borderArray = ['full', 'fullNoCont', 'onlyHeader', 'onlyHeaderNoCont', 'headerColumns', 'headerColumnsNoCont', 'onlyRows', 'onlyRowsNoCont', 'onlyColumns', 'onlyColumnsNoCont'];
        break;
    }
  }

  onSelected(value: string){
    this.selected = value;
    this.selectedChange.emit(value);
  }
}
