import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Subscription } from "rxjs/Subscription";

@Component({
    selector: 'template-plusminus',
    templateUrl: './template-plusminus.component.html',
    styleUrls: ['../template-styling.css']
})

export class TemplatePlusMinusComponent implements OnInit {

    @Input() styleObj: any;
    @Input() prop: string; // type of property ('padding', 'padding-top')
    @Input() max: number = 100;
    @Input() min: number = 0;
    @Input() step: number = 1;
    @Input() unit: string;
    @Input() isAll: boolean; // if change all sides of property
    @Output() valueChange = new EventEmitter<{value: any, prop: string}>();

    public unitLength: number; // chars of unit, for substringing

    private keyupAll$ = new Subject<HTMLInputElement>();
    private keyup$ = new Subject<HTMLInputElement>();

    private keyAllSub: Subscription;
    private keySub: Subscription;

    constructor() { }

    ngOnInit() {
        this.keySub = this.keyup$.debounceTime(500).distinctUntilChanged((next, prev) => next.value !== prev.value)
            .subscribe(el => {
                if (!this.styleObj[this.prop] || this.styleObj[this.prop].toString().replace(this.unit, '') !== el.value) {
                    this.setAndValidateStyle(el.value);
                    el.value = this.styleObj[this.prop].toString().replace(this.unit, '');
                }
            });

        this.keyAllSub = this.keyupAll$
            .debounceTime(500)
            .distinctUntilChanged((next, prev) => next.value !== prev.value)
            .subscribe(el => {
                if (!this.styleObj[this.prop + '-top'] || this.styleObj[this.prop + '-top'].toString().replace(this.unit, '') !== el.value) {
                    this.setAndValidateAllStyles(el.value);
                    el.value = this.styleObj[this.prop + '-top'].toString().replace(this.unit, '');
                }
            });

        this.unitLength = this.unit ? this.unit.length : 0;
    }

    ngOnDestroy() {
        if (this.keySub) this.keySub.unsubscribe();
        if (this.keyAllSub) this.keyAllSub.unsubscribe();
    }

    onKeyup(element: HTMLInputElement) {
        this.keyup$.next(element);
    }

    onKeyupAll(el: HTMLInputElement) {
        this.keyupAll$.next(el);
    }

    setAndValidateStyle(value: string) {
        let numValue = Number(value);
        if (numValue > this.max) numValue = this.max;
        else if (numValue < this.min) numValue = this.min;
        this.styleObj[this.prop] = numValue + (this.unit || '');
        this.valueChange.emit({value: numValue + (this.unit || ''), prop: this.prop});
    }

    setAndValidateAllStyles(value: string) {
        let numValue = Number(value);
        if (numValue > this.max) numValue = this.max;
        else if (numValue < this.min) numValue = this.min;
        let propValue = numValue + (this.unit || '');
        this.styleObj[this.prop + '-top'] = propValue;
        this.styleObj[this.prop + '-bottom'] = propValue;
        this.styleObj[this.prop + '-left'] = propValue;
        this.styleObj[this.prop + '-right'] = propValue;
        this.valueChange.emit({value: propValue, prop: 'all'});
    }

    addToStyle(add: boolean) {
        let value = Number(add ? this.step : -this.step)
        let styleValue = this.styleObj[this.prop] && !isNaN(this.styleObj[this.prop].toString().replace(this.unit, '')) ? Number(this.styleObj[this.prop].toString().replace(this.unit, '')) + value : value;
        if (styleValue > this.max) styleValue = this.max;
        else if (styleValue < this.min) styleValue = this.min;
        this.styleObj[this.prop] = styleValue + (this.unit || '');
        this.valueChange.emit({value: styleValue + (this.unit || ''), prop: this.prop})
    }

    addToAllStyles(add: boolean) {
        let value = Number(add ? this.step : -this.step)
        let styleValue = this.styleObj[this.prop + '-top'] && !isNaN(this.styleObj[this.prop + '-top'].toString().replace(this.unit, '')) ? Number(this.styleObj[this.prop + '-top'].toString().replace(this.unit, '')) + value : value;

        if (styleValue > this.max) styleValue = this.max;
        else if (styleValue < this.min) styleValue = this.min;

        let propValue = styleValue + (this.unit || '');
        this.styleObj[this.prop + '-top'] = propValue;
        this.styleObj[this.prop + '-bottom'] = propValue;
        this.styleObj[this.prop + '-left'] = propValue;
        this.styleObj[this.prop + '-right'] = propValue;
        this.valueChange.emit({value: propValue, prop: 'all'});
    }
}