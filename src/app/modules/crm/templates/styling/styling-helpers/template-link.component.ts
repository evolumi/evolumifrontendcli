import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    
    selector: 'template-link',
    templateUrl: './template-link.component.html',
    styleUrls: ['../template-styling.css']
})

export class TemplateLinkComponent implements OnInit {

    @Input() styleObj: any;
    @Output() linkChange = new EventEmitter<string>();

    constructor() { }

    ngOnInit() {

    }

    setLinkStyle() {
        this.styleObj['text-decoration'] = this.styleObj['text-decoration'] === 'underline' ? 'none' : 'underline';
        this.linkStyleChange();
    }

    linkStyleChange() {
        let css = '';
        for (let prop in this.styleObj) {
            if (this.styleObj.hasOwnProperty(prop) && prop.indexOf('hover') === -1) {
                css += `${prop}: ${this.styleObj[prop]};`;
            }
        }
        this.linkChange.emit(css);
    }
}