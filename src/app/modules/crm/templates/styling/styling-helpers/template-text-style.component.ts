import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'template-text-style',
  templateUrl: './template-text-style.component.html',
  styleUrls: ['../template-styling.css']
})
export class TemplateTextStyleComponent implements OnInit {

  @Input() styleObj: any;
  @Input() isBold: boolean;

  constructor() { }

  ngOnInit() {
    console.log('STYLES', this.styleObj)
  }

  setStyleProp(prop: string, value: any) {
    if (this.styleObj[prop] === value) {
      if (this.isBold){
        this.styleObj[prop] = 'normal'
      }
      delete this.styleObj[prop];
    } else {
      this.styleObj[prop] = value;
    }
  }
}
