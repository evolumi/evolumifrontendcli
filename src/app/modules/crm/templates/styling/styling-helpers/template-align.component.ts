import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'template-align',
    templateUrl: './template-align.component.html',
    styleUrls: ['../template-styling.css']
})

export class TemplateAlignComponent implements OnInit {

    @Input() styleObj: any;
    @Input() prop: string = 'text-align';
    @Input() showJustify = true;
    @Input() isHr = false;

    constructor() { }

    ngOnInit() {

    }

    setStyleProp(value: any) {
        this.styleObj[this.prop] = this.styleObj[this.prop] === value ? '' : value;
        if (this.isHr) {
            this.setHrStyle(value);
        }
    }

    private setHrStyle(value: any) {
        switch (value) {
            case 'left':
                if (this.styleObj[this.prop] === value) {
                    this.styleObj['margin-left'] = 0;
                    delete this.styleObj['margin-right'];
                } else {
                    delete this.styleObj['margin-left'];
                }
                break;
            case 'right':
                if (this.styleObj[this.prop] === value) {
                    this.styleObj['margin-right'] = 0;
                    delete this.styleObj['margin-left'];
                } else {
                    delete this.styleObj['margin-right'];
                }
                break;
            case 'center':
                if (this.styleObj[this.prop] === value) {
                    delete this.styleObj['margin-right'];
                    delete this.styleObj['margin-left'];
                }
                break;
        }

    }
}