import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'template-table-border-style',
  templateUrl: './template-table-border-style.component.html',
  styleUrls: ['./template-table-border-style.component.css']
})
export class TemplateTableBorderStyleComponent implements OnInit {

  @Input() type: string
  @Output() selected = new EventEmitter<string>();

  public classNames: string;

  constructor() { }

  ngOnInit() {
    switch (this.type) {
      case 'full':
        this.classNames = 'full-border';
        break;
      case 'fullNoCont':
        this.classNames = 'full-border no-container';
        break;
      case 'onlyHeader':
        this.classNames = 'only-header';
        break;
      case 'onlyHeaderNoCont':
        this.classNames = 'only-header no-container';
        break;
      case 'headerColumns':
        this.classNames = 'header-with-columns';
        break;
      case 'headerColumnsNoCont':
        this.classNames = 'header-with-columns no-container';
        break;
      case 'onlyRows':
        this.classNames = 'only-rows';
        break;
      case 'onlyRowsNoCont':
        this.classNames = 'only-rows no-container';
        break;
      case 'onlyColumns':
        this.classNames = 'only-columns';
        break;
      case 'onlyColumnsNoCont':
        this.classNames = 'only-columns no-container';
        break;
    }
  }

}
