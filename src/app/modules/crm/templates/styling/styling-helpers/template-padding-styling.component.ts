import { Component, OnInit, Input, SimpleChanges, EventEmitter, Output } from '@angular/core';

@Component({

    selector: 'template-padding-styling',
    templateUrl: './template-padding-styling.component.html',
    styleUrls: ['../template-styling.css']
})

export class TemplatePaddingStylingComponent implements OnInit {

    @Input() styleObj: any;
    @Input() settings: any;
    @Input() onlyAll: boolean; // if only want 'padding' and not 'padding-top'...
    @Output() valueChange = new EventEmitter<{value: any, prop: string}>();
    public showAll: boolean;

    constructor() { }

    ngOnInit() {
        if (!this.onlyAll) {
            this.showAll = this.checkIfAllSidesAreSame();
        }
    }

    ngOnChanges(changes: SimpleChanges) {
        if (!this.onlyAll && changes && changes['styleObj']) {
            this.showAll = this.checkIfAllSidesAreSame();
        }
    }

    showAllSides(event: Event) {
        event.stopPropagation();

        let value = this.styleObj['padding-top'];
        this.styleObj['padding-bottom'] = value;
        this.styleObj['padding-left'] = value;
        this.styleObj['padding-right'] = value;
        this.showAll = !this.showAll;
        this.valueChange.emit({value: value, prop: 'all'});
    }

    private checkIfAllSidesAreSame(): boolean {
        const dirArr = ['padding-top', 'padding-bottom', 'padding-left', 'padding-right'];
        let isDifferent = false;
        let value = this.styleObj['padding-top'];
        for (let i = 1; i < dirArr.length; i++) {
            if (value !== this.styleObj[dirArr[i]]) {
                isDifferent = true;
                i = 5;
            }
        }

        return isDifferent;
    }
}