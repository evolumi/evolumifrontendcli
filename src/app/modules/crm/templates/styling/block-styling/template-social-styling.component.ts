import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { TemplateBlockSocial, TemplateSocialMediaIcon } from '../../../../../models/template.models';
import { SocialMediaIconCollections, IconCollection, Icon, BlockStyleInputs } from '../../template-settings';

@Component({
    selector: 'template-social-styling',
    templateUrl: './template-social-styling.component.html',
    styleUrls: ['../template-styling.css'],
    host: {
        '(document:click)': 'closeIcons($event)',
    }
})

export class TemplateSocialStylingComponent implements OnInit {

    @Input() block: TemplateBlockSocial;
    @Input() settings: any;
    @Input() type: string;

    public inputs: any;

    public currentCollection: IconCollection;
    public iconCollections: Array<IconCollection> = SocialMediaIconCollections;
    public nonSelectedCollections: Array<IconCollection> = [];
    public nonSelectedIcons: Array<Icon>;

    public showIcons: boolean;

    constructor() { }

    ngOnInit() {
        this.getShownInputs();
    }

    getShownInputs() {
        switch (this.type) {
            case 'Newsletter':
                this.inputs = BlockStyleInputs.Social.Newsletter;
                break;
        }
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes && changes['block']) {
            this.setCurrentCollection();
        }
    }

    setCurrentCollection() {
        this.currentCollection = this.iconCollections.find(x => x.id === this.block.IconCollectionId);
        this.nonSelectedCollections = this.iconCollections.filter(x => x.id !== this.currentCollection.id);
        this.setNonSelectedIcons();
    }

    setNonSelectedIcons() {
        this.nonSelectedIcons = this.currentCollection.icons.filter(x => !this.block.Icons.some(y => y.Type === x.type));
    }

    selectCollection(event: Event, collection: IconCollection) {
        event.stopPropagation();

        this.block.Icons = this.block.Icons.map(x => {
            x.ImgSrc = collection.icons.find(y => y.type === x.Type).src;
            return x;
        })
        this.block.IconCollectionId = collection.id;
        this.setCurrentCollection();
        this.showIcons = false;
    }

    addIcon(event: Event, icon: Icon) {
        event.stopPropagation();
        this.block.Icons.push({ Type: icon.type, AltText: icon.type, ImgSrc: icon.src, LinkUrl: '' });
        this.setNonSelectedIcons();
    }

    deleteIcon(event: Event, icon: TemplateSocialMediaIcon) {
        event.stopPropagation();
        this.block.Icons = this.block.Icons.filter(x => x.Type !== icon.Type);
        this.setNonSelectedIcons();
    }

    closeIcons() {
        if (this.showIcons) this.showIcons = false;
    }
}