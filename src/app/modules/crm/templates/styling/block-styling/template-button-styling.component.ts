import { Component, OnInit, Input } from '@angular/core';
import { TemplateBlockButton } from '../../../../../models/template.models';
import { BlockStyleInputs } from "../../template-settings";

@Component({
    selector: 'template-button-styling',
    templateUrl: './template-button-styling.component.html',
    styleUrls: ['../template-styling.css']
})

export class TemplateButtonStylingComponent implements OnInit {

    @Input() block: TemplateBlockButton;
    @Input() settings: any;
    @Input() type: string;

    public activeButtonTab: string;
    public showContent: boolean;
    public inputs: any

    constructor() { }

    ngOnInit() {
        this.activeButtonTab = 'Content';
        this.showContent = true;
        this.getShownInputs();
    }

    getShownInputs() {
        switch (this.type) {
            case 'Newsletter':
                this.inputs = BlockStyleInputs.Button.Newsletter;
                break;
        }
    }

    onClickButtonTab(tab: string) {
        this.activeButtonTab = tab;
        this.showContent = tab === 'Content';
    }

    setButtonRadius(type: string) {
        switch (type) {
            case 'Straight':
                this.block.ContentStyles['border-radius'] = '0px';
                this.block.ContentStyles['-webkit-border-radius'] = '0px';
                this.block.ContentStyles['-moz-border-radius'] = '0px';
                break;
            case 'Rounded':
                this.block.ContentStyles['border-radius'] = '1000px';
                this.block.ContentStyles['-webkit-border-radius'] = '1000px';
                this.block.ContentStyles['-moz-border-radius'] = '1000px';
                break;
        }
    }
}