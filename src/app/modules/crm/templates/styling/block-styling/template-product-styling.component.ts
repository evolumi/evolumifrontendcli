import { Component, OnInit, Input } from '@angular/core';
import { TemplateBlockProduct, TemplateProductValues } from "../../../../../models/template.models";
import { BlockStyleInputs } from "../../template-settings";
import { ChosenOptionModel } from "../../../../shared/components/chosen/chosen.models";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: 'template-product-styling',
  templateUrl: './template-product-styling.component.html',
  styleUrls: ['../template-styling.css', 'template-product-styling.component.css']
})
export class TemplateProductStylingComponent implements OnInit {

  @Input() block: TemplateBlockProduct;
  @Input() settings: any;
  @Input() type: string;

  public activeTab: string;
  public productShow: string = 'table';
  public totalShow: string = 'table';
  public showMoreColumns: Array<boolean> = [];
  public showMoreRows: Array<boolean> = [];

  public inputs: any;

  public productFields: Array<ChosenOptionModel>;
  public totalFields: Array<ChosenOptionModel>;

  constructor(private translate: TranslateService) { }

  ngOnInit() {
    this.initChosens();
    this.getShownInputs();
  }

  getShownInputs() {
    switch (this.type) {
      case 'Document':
        this.inputs = BlockStyleInputs.Product.Document;
        break;
    }
  }

  setShowStyle(event: Event, show: string, type: string) {
    event.stopPropagation();

    switch (type) {
      case 'product':
        if (this.productShow === show) this.productShow = '';
        else this.productShow = show;
        break;
      case 'column':
        // if (this.showBlock === show) this.showBlock = '';
        // else this.showBlock = show;
        break;
      case 'total':
        if (this.totalShow === show) this.totalShow = '';
        else this.totalShow = show;
        break;
    }
  }

  initChosens() {
    this.productFields = [
      new ChosenOptionModel('[Name]', this.translate.instant('NAME'))
    ];
    this.totalFields = [
      new ChosenOptionModel('[TotalSum]', this.translate.instant('TOTALSUM'))
    ];
  }

  setTableHeight(type: string) {
    console.log('CHANGEHEIGHT NOTIMPLEMENTED', type);
  }

  add(event: Event, index: number, type: string) {
    event.stopPropagation();
    switch (type) {
      case 'Column':
        const newCol: TemplateProductValues = { Title: 'Title', Fields: [], CellAlign: 'left', Styles: {} };
        this.block.Columns.splice(index + 1, 0, newCol);
        break;
      case 'SumRow':
        const newRow: TemplateProductValues = {Title: 'Title', Fields: [], Styles: {}};
        this.block.SumRows.splice(index + 1, 0, newRow);
        break;
    }
  }

  move(event: Event, up: boolean, index: number, type: string) {
    event.stopPropagation();
    switch (type) {
      case 'Columns':
        if (up) {
          let temp = this.block.Columns[index - 1];
          this.block.Columns[index - 1] = this.block.Columns[index];
          this.block.Columns[index] = temp;
        } else {
          let temp = this.block.Columns[index + 1];
          this.block.Columns[index + 1] = this.block.Columns[index];
          this.block.Columns[index] = temp;
        }
        break;
      case 'SumRows':
        if (up) {
          let temp = this.block.SumRows[index - 1];
          this.block.SumRows[index - 1] = this.block.SumRows[index];
          this.block.SumRows[index] = temp;
        } else {
          let temp = this.block.SumRows[index + 1];
          this.block.SumRows[index + 1] = this.block.SumRows[index];
          this.block.SumRows[index] = temp;
        }
        break;
    }
  }
}
