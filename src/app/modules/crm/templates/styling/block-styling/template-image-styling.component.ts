import { Component, OnInit, Input } from '@angular/core';
import { TemplateBlockImage } from '../../../../../models/template.models';
import { TemplateBuilderService } from '../../../../../services/template-builder.service';
import { FileModel } from '../../../../../models/file.models';
import { NotificationService } from '../../../../../services/notification-service';
import { BlockStyleInputs } from "../../template-settings";

@Component({
    selector: 'template-image-styling',
    templateUrl: './template-image-styling.component.html',
    styleUrls: ['../template-styling.css']
})

export class TemplateImageStylingComponent implements OnInit {

    @Input() block: TemplateBlockImage;
    @Input() settings: any;
    @Input() type: string;

    public inputs: any;

    public fileProcessing: boolean;

    constructor(private templateSvc: TemplateBuilderService,
        private notify: NotificationService) { }

    ngOnInit() {
        this.getShownInputs();
    }

    getShownInputs() {
        switch (this.type) {
            case 'Newsletter':
                this.inputs = BlockStyleInputs.Image.Newsletter;
                break;
            case 'Document':
                this.inputs = BlockStyleInputs.Image.Document;
                break;
        }
    }

    addFile(fileList: Array<any>) {
        let oldFileId: string;
        let isTemp: boolean;
        if (this.block.ImageFile && this.block.ImageFile.IsTemp) {
            oldFileId = this.block.ImageFile.Id;
            isTemp = this.block.ImageFile.IsTemp;
        }
        this.fileProcessing = true;
        this.templateSvc.addTempFile(fileList[0]).then(res => {
            const resObj = JSON.parse(res);
            const file = new FileModel(resObj.Data);
            file.IsTemp = true;
            this.block.ImageFile = file;
            this.fileProcessing = false;
            this.templateSvc.downloadTempFile(file)
                .subscribe(res => {
                    const blob = new Blob([res], { type: 'application/octet-stream' });
                    const url = window.URL.createObjectURL(blob);
                    this.block.BlobUrl = url;
                    this.block.Height = null;
                    this.block.Width = null;
                })
            if (oldFileId) {
                this.deleteFile(oldFileId, isTemp, true);
            }
        }, (err) => {
            this.notify.error('UPLOADFILEFAIL');
            this.fileProcessing = false;
        }).catch(res => {
            this.notify.error('UPLOADFILEFAIL');
            this.fileProcessing = false;
        });
    }

    deleteFile(id: string, isTemp: boolean, keepFile?: boolean) {
        if (isTemp) {
            this.templateSvc.deleteTempFile(id)
                .subscribe((res) => {
                    if (res.IsSuccess) {
                        if (!keepFile) {
                            this.block.ImageFile = null;
                        }
                    }
                }, (err) => {
                    this.notify.error('DELETEFILEFAIL');
                });
        } else {
            this.templateSvc.deleteFile(id)
                .subscribe(res => {
                    if (res.IsSuccess) {
                        if (!keepFile) {
                            this.block.ImageFile = null;
                        }
                    }
                })
        }
    }
}