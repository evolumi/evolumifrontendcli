import { Component, OnInit, Input } from '@angular/core';
import { TemplateBlockText } from "../../../../../models/template.models";
import { TemplateBuilderService } from "../../../../../services/template-builder.service";
import { BlockStyleInputs } from "../../template-settings";

@Component({
    selector: 'template-textblock-styling',
    templateUrl: './template-textblock-styling.component.html',
    styleUrls: ['../template-styling.css']
})

export class TemplateTextblockStylingComponent implements OnInit {

    @Input() block: TemplateBlockText;
    @Input() settings: any;
    @Input() type: string;

    public inputs: any;

    private linkCss: string;
    private linkHoverCss: string;
    private styleElement: any;

    constructor(private templateSvc: TemplateBuilderService) { }

    ngOnInit() {
        this.getShownInputs();
    }

    getShownInputs() {
        switch (this.type) {
            case 'Newsletter':
                this.inputs = BlockStyleInputs.TextBlock.Newsletter;
                break;
            case 'Document':
                this.inputs = BlockStyleInputs.TextBlock.Document;
                break;
        }
    }

    linkChange(type: string, css: string) {
        switch (type) {
            case 'hover':
                this.linkHoverCss = `#${this.block.TemplateId} a:hover {${css}}`;
                break;
            case 'normal':
            default:
                this.linkCss = `#${this.block.TemplateId} a {${css}}`;
                break;
        }
        if (!this.styleElement) {
            this.styleElement = document.createElement('style');
            this.templateSvc.styleElements.push(this.styleElement);
            document.getElementsByTagName('head')[0].appendChild(this.styleElement);
        }
        if (this.styleElement.styleSheet) {
            this.styleElement.styleSheet.cssText = this.linkCss + this.linkHoverCss;
        } else {
            this.styleElement.innerHTML = '';
            this.styleElement.appendChild(document.createTextNode(this.linkCss + this.linkHoverCss));
        }
    }
}