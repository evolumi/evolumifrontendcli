import { Component, OnInit, Input } from '@angular/core';
import { TemplateBlockAddress, AddressRow } from "../../../../../models/template.models";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: 'template-address-styling',
  templateUrl: './template-address-styling.component.html',
  styleUrls: ['../template-styling.css', './template-address-styling.component.css']
})
export class TemplateAddressStylingComponent implements OnInit {

  @Input() block: TemplateBlockAddress;
  @Input() settings: any;
  @Input() type: string;

  public activeTab: string = 'Styles';
  public showMoreRows: Array<boolean> = [];

  public rowInputTypes: Array<{ label: string, value: string }> = [];

  constructor(private translateSvc: TranslateService) { }

  ngOnInit() {
    this.rowInputTypes = [{ label: this.translateSvc.instant('MANUALFIELD'), value: 'manual' }, { label: this.translateSvc.instant('DATAFIELD'), value: 'data' }];
  }

  changeRowPadding(obj: { value: any, prop: string }) {
    if (obj.prop === 'all') {
      this.block.AddressBlock.Rows.map(x => {
        x.Styles['padding-top'] = obj.value;
        x.Styles['padding-bottom'] = obj.value;
        x.Styles['padding-left'] = obj.value;
        x.Styles['padding-right'] = obj.value;
      });
    } else {
      this.block.AddressBlock.Rows.map(x => {
        x.Styles[obj.prop] = obj.value;
      });
    }
  }

  changeInputType(obj: any, index: number) {
    let isManual = this.block.AddressBlock.Rows[index].IsManual;
    this.block.AddressBlock.Rows[index].IsManual = obj.value === 'manual';
    if (isManual !== this.block.AddressBlock.Rows[index].IsManual) {
      if (isManual) {
        this.block.AddressBlock.Rows[index].Field = '';
      } else {
        this.block.AddressBlock.Rows[index].Fields = [];
      }
    }
  }

  add(event: Event, index: number, type: string) {
    event.stopPropagation();
    const newRow: AddressRow = { Fields: [], Styles: {}, IsManual: false };
    this.block.AddressBlock.Rows.splice(index + 1, 0, newRow);
  }

  move(event: Event, up: boolean, index: number, type: string) {
    event.stopPropagation();
    if (up) {
      let temp = this.block.Columns[index - 1];
      this.block.AddressBlock.Rows[index - 1] = this.block.AddressBlock.Rows[index];
      this.block.AddressBlock.Rows[index] = temp;
    } else {
      let temp = this.block.Columns[index + 1];
      this.block.AddressBlock.Rows[index + 1] = this.block.AddressBlock.Rows[index];
      this.block.AddressBlock.Rows[index] = temp;
    }
  }
}
