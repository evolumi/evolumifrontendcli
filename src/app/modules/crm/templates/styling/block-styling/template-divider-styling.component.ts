import { Component, OnInit, Input } from '@angular/core';
import { TemplateBlockDivider } from '../../../../../models/template.models';
import { BlockStyleInputs } from "../../template-settings";

@Component({
    selector: 'template-divider-styling',
    templateUrl: './template-divider-styling.component.html',
    styleUrls: ['../template-styling.css']
})

export class TemplateDividerStylingComponent implements OnInit {

    @Input() block: TemplateBlockDivider;
    @Input() settings: any;
    @Input() type: string;

    public inputs: any;

    constructor() { }

    ngOnInit() {
        this.getShownInputs();
    }

    getShownInputs() {
        switch (this.type) {
            case 'Newsletter':
                this.inputs = BlockStyleInputs.Divider.Newsletter;
                break;
        }
    }
}