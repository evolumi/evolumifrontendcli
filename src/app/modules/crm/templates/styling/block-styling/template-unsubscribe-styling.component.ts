import { Component, OnInit, Input } from '@angular/core';
import { TemplateBlockUnsubscribe } from "../../../../../models/template.models";
import { TemplateBuilderService } from "../../../../../services/template-builder.service";
import { BlockStyleInputs } from "../../template-settings";

@Component({
    selector: 'template-unsubscribe-styling',
    templateUrl: './template-unsubscribe-styling.component.html',
    styleUrls: ['../template-styling.css']
})

export class TemplateUnsubscribeStylingComponent implements OnInit {

    @Input() block: TemplateBlockUnsubscribe;
    @Input() settings: any;
    @Input() type: string;

    public inputs: any;

    private linkCss: string;
    private linkHoverCss: string;
    private styleElement: any;

    constructor(private templateSvc: TemplateBuilderService) { }

    ngOnInit() {
        this.getShownInputs();
    }

    getShownInputs() {
        switch (this.type) {
            case 'Newsletter':
                this.inputs = BlockStyleInputs.Social.Newsletter;
                break;
        }
    }

    linkChange(type: string, css: string) {
        switch (type) {
            case 'hover':
                this.linkHoverCss = `#${this.block.TemplateId} a:hover {${css}}`;
                break;
            case 'normal':
            default:
                this.linkCss = `#${this.block.TemplateId} a {${css}}`;
                break;
        }
        if (!this.styleElement) {
            this.styleElement = document.createElement('style');
            this.templateSvc.styleElements.push(this.styleElement);
            document.getElementsByTagName('head')[0].appendChild(this.styleElement);
        }
        if (this.styleElement.styleSheet) {
            this.styleElement.styleSheet.cssText = this.linkCss + this.linkHoverCss;
        } else {
            this.styleElement.innerHTML = '';
            this.styleElement.appendChild(document.createTextNode(this.linkCss + this.linkHoverCss));
        }
    }
}