import { Component, OnInit, Input } from '@angular/core';
import { TemplateBlockTable, TableCell } from "../../../../../models/template.models";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: 'template-table-styling',
  templateUrl: './template-table-styling.component.html',
  styleUrls: ['./template-table-styling.component.css']
})
export class TemplateTableStylingComponent implements OnInit {

  @Input() block: TemplateBlockTable;
  @Input() settings: any;
  @Input() type: string;

  public activeTab: string;

  constructor(private translate: TranslateService) { }

  ngOnInit() {
  }

  add(event: Event, index: number, type: string) {
    event.stopPropagation();
    switch (type) {
      case 'Column':
        // const newCol: TemplateProductValues = { Title: 'Title', Fields: [], CellAlign: 'left', Styles: {} };
        // this.block.Columns.splice(index + 1, 0, newCol);
        break;
      case 'row':
        let cellLength = this.block.Cells[0].length;
        const newRow: Array<TableCell> = Array(cellLength).fill({Content: this.translate.instant('CLICKTOEDIT')});
        this.block.TableBlock.Cells.splice(index + 1, 0, newRow);
        break;
    }
  }

  move(event: Event, up: boolean, index: number, type: string) {
    event.stopPropagation();
    switch (type) {
      case 'Columns':
        if (up) {
          let temp = this.block.Columns[index - 1];
          this.block.Columns[index - 1] = this.block.Columns[index];
          this.block.Columns[index] = temp;
        } else {
          let temp = this.block.Columns[index + 1];
          this.block.Columns[index + 1] = this.block.Columns[index];
          this.block.Columns[index] = temp;
        }
        break;
      case 'row':
        if (up) {
          let temp = this.block.TableBlock.Cells[index - 1];
          this.block.TableBlock.Cells[index - 1] = this.block.TableBlock.Cells[index];
          this.block.TableBlock.Cells[index] = temp;
        } else {
          let temp = this.block.TableBlock.Cells[index + 1];
          this.block.TableBlock.Cells[index + 1] = this.block.TableBlock.Cells[index];
          this.block.TableBlock.Cells[index] = temp;
        }
        break;
    }
  }

}
