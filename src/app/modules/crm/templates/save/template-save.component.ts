import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { TemplateModel } from '../../../../models/template.models';
import { TemplateBuilderService } from '../../../../services/template-builder.service';

@Component({
    selector: 'template-save',
    templateUrl: './template-save.component.html',
    styleUrls: ['../template-builder.css']
})

export class TemplateSaveComponent implements OnInit {

    @Input() template: TemplateModel;
    @Output() close = new EventEmitter<boolean>();

    public edit: boolean;

    constructor(private templateSvc: TemplateBuilderService) { }

    ngOnInit() {

    }

    save(isNew: boolean) {
        if (isNew) {
            this.templateSvc.addTemplate(this.template, this.back.bind(this));
        } else {
            this.templateSvc.updateTemplate(this.template, this.back.bind(this));
        }
    }

    clickToClose(event: any) {
        let target = event.target || event.srcElement;
        if (target.id === 'saveTemplate') {
            this.back();
        }
    }

    back() {
        this.close.emit(false);
    }
}