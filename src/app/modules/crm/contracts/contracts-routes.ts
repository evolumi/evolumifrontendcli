import { Routes } from '@angular/router';
import { Permissions, Features } from '../../../services/organisation.service';
import { EvoRequireGuard } from '../../../services/guards/evo-require-guard';

import { ContractsComponent } from './contracts.component';
import { ContractsDetailsComponent } from './contracts-details.component';

export const ContractsRoutes: Routes = [
  {
    path: 'Contracts',
    component: ContractsComponent,
    canActivate: [EvoRequireGuard],
    data: { features: [Features.Contracts], permissions: [Permissions.ViewContracts] }
  },
  {
    path: 'Contracts/:contractId',
    component: ContractsDetailsComponent
  }
];
