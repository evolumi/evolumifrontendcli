import { Component, OnInit, OnDestroy, ViewChild, AfterViewInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ContractsService } from '../../../services/contracts.service';
import { ContractsModel } from '../../../models/contracts.models';
import { FileModel } from '../../../models/file.models';
import { Subscription } from 'rxJs/Subscription';
import { LocalStorage } from '../../../services/localstorage';
import { Api } from '../../../services/api';

@Component({
    selector: 'contracts-details',
    templateUrl: 'contracts-details.component.html',
    styleUrls: ['contracts.css']
})
export class ContractsDetailsComponent implements OnInit, OnDestroy, AfterViewInit {

    @ViewChild('pdfViewer') pdfViewer: any;
    public pdfSrc: any;
    public pdfZoom = 1;
    public noPdf: boolean;

    public contract: ContractsModel;

    private routeSub: Subscription;
    private contractSub: Subscription;

    private contractId: string;

    constructor(public contractsService: ContractsService,
        private route: ActivatedRoute,
        private api: Api,
        private localStorage: LocalStorage) { }

    ngOnInit() {
        this.routeSub = this.route.params
            .subscribe(res => {
                this.pdfSrc = '';
                this.contractId = res['contractId'];
                this.noPdf = false;
                this.contractsService.getContract(this.contractId);
            });
        this.contractSub = this.contractsService.contractObs()
            .subscribe(contract => {
                if (contract && contract.Id) {
                    this.contract = contract;
                    if (this.pdfViewer) {
                        this.loadFile();
                    }
                }
            });
    }

    ngOnDestroy() {
        if (this.routeSub) this.routeSub.unsubscribe();
        if (this.contractSub) this.contractSub.unsubscribe();
        this.contractsService.setContract(null);
    }

    ngAfterViewInit() {
        if (this.contract) {
            this.loadFile();
        }
    }

    onFileChange(file: FileModel) {
        this.loadFile(file);
    }

    private loadFile(file?: FileModel) {
        if (file) {
            this.showPdf(file);
            this.noPdf = false;
        } else {
            if (this.contract.Files.length > 0) {
                this.showPdf(this.contract.Files[0]);
                this.noPdf = false;
            } else {
                this.noPdf = true;
            }
        }
    }

    openCreate() {
        this.contractsService.createNew();
    }

    zoom(zoomIn: boolean) {
        if (zoomIn) {
            this.pdfZoom -= 0.1;
        } else {
            this.pdfZoom += 0.1;
        }
    }

    showPdf(file: FileModel) {
        const orgId = this.localStorage.get('orgId');

        this.api.postDownloadFile(`Organisations/${orgId}/DownloadFile`, file)
            .subscribe(data => {
                    const blob = new Blob([data], { type: 'application/octet-stream' });
                    this.pdfSrc = window.URL.createObjectURL(blob);
            });
    }
}