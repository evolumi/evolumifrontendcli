import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { ContractsService } from '../../../services/contracts.service';
import { UsersService } from '../../../services/users.service';
import { TranslateService } from '@ngx-translate/core';
import { ContractsModel, NextDateModel } from '../../../models/contracts.models';
import { OrgService } from '../../../services/organisation.service';
import { ChosenOptionModel } from '../../shared/components/chosen/chosen.models';
import { Subscription } from 'rxjs/Subscription';
import { ButtonPanelService } from '../../../services/button-panel.service';
import { CategoryGroup } from '../../../models/category.models';
import { CategoryService } from '../../../services/category.service';
import { OrganisationModel } from "../../../models/organisation.models";

@Component({

    selector: 'contracts',
    templateUrl: 'contracts.component.html',
    styleUrls: ['contracts.css']
})
export class ContractsComponent implements OnInit {

    public sortOptions: Array<ChosenOptionModel>;
    public categories: Array<ChosenOptionModel>;
    public categoryGroups: Array<ChosenOptionModel>;
    private chosenCategories: Array<ChosenOptionModel> = [];

    public contracts: Array<ContractsModel> = [];
    private contractsSub: Subscription;

    public users: Array<ChosenOptionModel>;
    private usersSub: Subscription;

    private org: OrganisationModel;
    private orgSub: Subscription;
    private buttonSub: Subscription;

    public categoryFilters: Array<any>;
    public selectedCategoryFilter: string;

    public dateFilters: Array<any>;
    public selectedDateType: string;

    public selectedCategories: Array<any> = [];

    private update: boolean;

    private categoryGroupsList: Array<CategoryGroup> = [];
    private catSub: Subscription;

    constructor(public contractsService: ContractsService,
        private translateService: TranslateService,
        private usersService: UsersService,
        private orgService: OrgService,
        private router: Router,
        private button: ButtonPanelService,
        private catSvc: CategoryService) { }

    ngOnInit() {
        this.button.addPanelButton("ADDCONTRACT", "add-contract", "plus");
        this.buttonSub = this.button.commandObservable().subscribe(command => {
            if (command == "add-contract") this.openCreate();
        })

        this.usersSub = this.usersService.activeUserListObervable()
            .subscribe(users => {
                if (users && users.length > 0) {
                    this.users = users;
                }
            })
        this.orgSub = this.orgService.currentOrganisationObservable()
            .subscribe(org => {
                if (org) {
                    this.org = org;
                }
            });
        this.contractsSub = this.contractsService.contractsListObs()
            .subscribe(res => {
                this.contracts = [];
                this.contracts.push(...res);
                for (let i = 0; i < this.contracts.length; i++) {
                    let contract = this.contracts[i];
                    contract.NextDate = this.getNextDate(contract.Id)
                    for (let j = 0; j < contract.Categories.length; j++) {
                        contract.Categories[j].Color = this.getCategoryColor(contract.Categories[j].Id)
                    }
                }
            });
        this.catSub = this.catSvc.contractsCategoryListObs()
            .subscribe((groups: any) => {
                if (groups != null) {
                    this.categoryGroupsList = groups;
                    this.categories = this.getCategories();
                    this.categoryGroups = this.getCategoryGroups();
                }
            });
        this.contractsService.reset();
        this.contractsService.getContracts();
        this.sortOptions = this.getSortOptions();
        this.getCategoryFilters();
        this.getDateFilters();
    }

    ngOnDestroy() {
        if (this.contractsSub) this.contractsSub.unsubscribe();
        if (this.usersSub) this.usersSub.unsubscribe();
        if (this.orgSub) this.orgSub.unsubscribe();
        if (this.buttonSub) this.buttonSub.unsubscribe();
        if (this.catSub) this.catSub.unsubscribe();
        this.button.clearPanel();
    }

    selectAll(all: boolean) {
        this.contractsService.selectAll = all;
        this.update = !this.update;
    }

    delete(id: string) {
        let conf = confirm(this.translateService.instant('CONFIRMDELETECONTRACT'))
        if (conf) {
            this.contractsService.deleteContract(id);
        }
    }

    openCreate() {
        this.contractsService.createNew();
    }

    openDetails(id: string) {
        this.router.navigate(['/CRM/Contracts', id]);
    }

    onSearch(val: string) {
        this.contractsService.searchContracts(val);
    }

    onCategoryFilter(event: any) {
        const options = event.currentTarget.options;
        let chosenArr: Array<ChosenOptionModel> = [];
        let index = 0;
        for (let i = 0; i < options.length; i++) {
            let option = options[i];
            if (option.selected) {
                chosenArr.push(new ChosenOptionModel(option.value, option.text));
                if (!this.chosenCategories.find(x => x.value === option.value && x.label === option.text)) {
                    this.setColorOfCategoryChosen(option.value);
                }
                index++;
            }
        }
        this.contractsService.searchParams.Categories = chosenArr.map(x => x.label.toString());
        this.contractsService.searchParams.CategoryGroups = chosenArr.map(x => x.value.toString());
        this.contractsService.getContracts();
        this.chosenCategories = chosenArr.slice();
    }

    changeCategoryFilter(event: any, noReload?: boolean) {
        this.selectedCategoryFilter = event.label;
        switch (event.value) {
            case 'CategoryAll':
                this.contractsService.searchParams.CategoryAll = true;
                this.contractsService.searchParams.CategoryAny = false;
                break;
            case 'CategoryAny':
                this.contractsService.searchParams.CategoryAll = false;
                this.contractsService.searchParams.CategoryAny = true;

        }
        if (!noReload) {
            this.contractsService.getContracts();
        }
    }

    private getCategoryFilters() {
        const allLabel = this.translateService.instant('FILTERCATEGORYALL');
        const anyLabel = this.translateService.instant('FILTERCATEGORYANY');
        this.categoryFilters = [
            { label: allLabel, value: 'CategoryAll' },
            { label: anyLabel, value: 'CategoryAny' }
        ]
        this.changeCategoryFilter(this.categoryFilters[1], true);
    }

    private setColorOfCategoryChosen(categoryId: string) {
        const catGroup = this.org.CategoryGroups.find(x => x.Id === categoryId);
        const liArr = Array.from(document.querySelectorAll('#categoryChosen>.chosen-container>.chosen-choices>.search-choice'));
        let element = liArr[liArr.length - 1] as HTMLElement;
        element.className += ' category-chosen-label label';
        let closeEl = element.getElementsByClassName('search-choice-close')[0] as HTMLElement;
        element.style.backgroundColor = catGroup.Color;
        element.style.backgroundImage = 'none';
        element.style.color = catGroup.FontColor || '#FFF';
        element.style.border = 'none';
        element.style.fontSize = '16px';
        closeEl.style.color = catGroup.FontColor || '#FFF';
        closeEl.innerHTML = '<i class="fa fa-times" style="pointer-events: none;"></i>';
        closeEl.style.background = 'transparent';
    }

    private getDateFilters() {
        const startLabel = this.translateService.instant('FILTERSTARTDATE');
        const mileLabel = this.translateService.instant('FILTERMILESTONEDATE');
        const endLabel = this.translateService.instant('FILTERENDDATE');

        this.dateFilters = [
            { label: startLabel, value: 'startdate' },
            { label: mileLabel, value: 'milestonedate' },
            { label: endLabel, value: 'enddate' },
        ]
        this.changeDateFilter(this.dateFilters[1], true);
    }

    changeDateFilter(event: any, noReload?: boolean) {
        this.selectedDateType = event.label;
        this.contractsService.searchParams.DateType = event.value;
        if (!noReload) {
            this.contractsService.getContracts();
        }
    }

    onDateChange(value: any, date: string) {
        switch (date) {
            case 'startdate':
                this.contractsService.searchParams.StartDate = value;
                break;
            case 'enddate':
            default:
                this.contractsService.searchParams.EndDate = value;
                break;
        }
        this.contractsService.getContracts();
    }

    private getNextDate(id: string) {
        let retModel = new NextDateModel({});
        let contract = this.contracts.find(x => x.Id === id);
        const dateNow = new Date().getTime();
        let dates = contract.MilestoneDates.map(x => x.Date).filter(x => x > dateNow);
        if (dates.length > 0) {
            let minDate = Math.min(...dates);
            retModel.MilestoneDate = contract.MilestoneDates.find(x => x.Date === minDate)
            return retModel;
        } else if (contract.EndDate > dateNow) {
            retModel.EndDate = +contract.EndDate;
            return retModel;
        }
        return null;
    }

    private getCategoryColor(id: string): string {
        const obj = this.org.CategoryGroups.find(x => x.Id === id);
        return obj.Color;
    }

    private getSortOptions() {
        return [
            new ChosenOptionModel('Name', this.translateService.instant('NAME')),
            new ChosenOptionModel('NextDate', this.translateService.instant('NEXTIMPORTANTDATE')),
            new ChosenOptionModel('EndDate', this.translateService.instant('ENDDATE')),
        ];
    }

    private getCategories() {
        let returnArray: Array<ChosenOptionModel> = [];
        this.categoryGroupsList.map(x => x.Categories.map(y => returnArray.push(new ChosenOptionModel(x.Id, y, x.Id))));
        return returnArray;
    }

    private getCategoryGroups() {
        let returnArray: Array<ChosenOptionModel> = [];
        this.categoryGroupsList.map(x => returnArray.push(new ChosenOptionModel(x.Id, x.Name)));
        return returnArray;
    }

    onScroll() {
        this.contractsService.onScroll();
    }
}