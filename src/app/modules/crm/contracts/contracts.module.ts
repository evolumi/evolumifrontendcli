import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { SettingsSharedModule } from "../settings/shared/settings-shared.module";

import { ContractsComponent } from './contracts.component';
import { ContractsDetailsComponent } from './contracts-details.component'
import { AddContractComponent } from './add/add-contract.component';
import { LeftPanelComponent } from './left-panel.component';

@NgModule({
    imports: [
        SharedModule,
        SettingsSharedModule
    ],
    exports: [],
    declarations: [
        ContractsComponent,
        ContractsDetailsComponent,
        AddContractComponent,
        LeftPanelComponent
    ]
})
export class ContractsModule { }
