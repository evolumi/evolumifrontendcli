import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { UsersService } from '../../../../services/users.service';
import { AccountsCollection } from '../../../../services/accounts.service';
import { ContractsModel, PartyModel, MilestoneDate, ContractCategory } from '../../../../models/contracts.models';
import { CategoryGroup } from '../../../../models/category.models';
import { ContractsService } from '../../../../services/contracts.service';
import { NotificationService } from '../../../../services/notification-service';
import { Subscription } from 'rxjs/Subscription';
import { AllUsersModel } from '../../../../models/user.models';
import { ChosenOptionModel } from "../../../shared/components/chosen/chosen.models";
import { FileModel } from '../../../../models/file.models';
import { TranslateService } from '@ngx-translate/core';
import { CategoryService } from '../../../../services/category.service';
import { PermissionService } from "../../../../services/permissionService";

export enum SelectedUser {
    User = 1,
    Nonuser = 2,
    Company = 3,
    Person = 4,
    Noncompany = 5,
    Nonperson = 6
}

@Component({
    selector: 'add-contract',
    templateUrl: 'add-contract.component.html',
    styleUrls: ['../contracts.css'],
    host: {
        '(document:click)': 'clickToClose($event)',
    }
})
export class AddContractComponent implements OnInit, OnDestroy {

    @Input('edit') edit: boolean = false;
    @Output() file: EventEmitter<FileModel> = new EventEmitter<FileModel>();

    public editMode: boolean;

    public contract: ContractsModel = new ContractsModel({});
    //To be able to cancel changes in editmode
    private contractUntouched: ContractsModel;
    public currentMilestone: MilestoneDate;

    private userList: Array<AllUsersModel> = [];
    private allUserList: Array<AllUsersModel> = [];
    public accountList: Array<ChosenOptionModel> = [];
    public personAccountList: Array<ChosenOptionModel> = [];
    public contactsList: Array<ChosenOptionModel> = [];

    private milestoneEdit: boolean
    private milestoneIndex: number;
    public nonUserParty: PartyModel = new PartyModel({});
    public nonTheirUserParty: PartyModel = new PartyModel({});

    public contactsToAdd: Array<string> = []; //placeholder
    public companyAccount: ChosenOptionModel = new ChosenOptionModel(null, null); //placeholder
    public companyAccountId: string;
    public personAccountId: string; //placeholder
    public selectedAddOur: SelectedUser;
    public selectedAddTheir: SelectedUser;
    public fileProcessing: boolean;

    public SelectedUser = SelectedUser;

    //Categories
    private allCategoryGroups: Array<CategoryGroup> = [];
    public selectedCategoryGroups: Array<CategoryGroup> = [];
    public notSelectedCategoryGroups: Array<CategoryGroup> = [];

    //Subscriptions
    private contractSub: Subscription;
    private userSub: Subscription;
    private categoryGroupsSub: Subscription;
    private contactSub: Subscription;

    constructor(public contractsService: ContractsService,
        private userService: UsersService,
        private notify: NotificationService,
        private accountService: AccountsCollection,
        private translate: TranslateService,
        public catSvc: CategoryService,
        public permission: PermissionService) { }

    ngOnInit() {
        this.editMode = !this.edit;
        this.userSub = this.userService.usersObservable()
            .subscribe(users => {
                if (users && users.length > 0) {
                    Object.assign(this.allUserList, users)
                    Object.assign(this.userList, users);
                    this.populateUserList();
                }
            });
        this.contractSub = this.contractsService.contractObs()
            .subscribe(contract => {
                if (contract) {
                    //This is to be able to see the edit contract even when you open add new contract popup
                    if (this.edit && contract.Id) {
                        this.editMode = false;
                        this.contractUntouched = JSON.parse(JSON.stringify(contract));
                        this.contract = JSON.parse(JSON.stringify(contract));
                        if (this.allCategoryGroups.length > 1) this.arrangeCategoryGroups();
                        if (contract.Id) this.populateUserList();
                    } else if (!this.edit) {
                        this.contractUntouched = JSON.parse(JSON.stringify(contract));
                        this.contract = JSON.parse(JSON.stringify(contract));
                        if (this.allCategoryGroups.length > 1) this.arrangeCategoryGroups();
                    }
                }
            });

        this.companyListSearch("");
        this.personListSearch("");

        this.categoryGroupsSub = this.catSvc.contractsCategoryListObs()
            .subscribe((groups: any) => {
                this.allCategoryGroups = groups;
                if (Object.keys(this.contract).length !== 0 && this.contract.constructor === Object) {
                    this.arrangeCategoryGroups();
                }
            });
        this.contactSub = this.accountService.contactAccountsObservable()
            .subscribe(res => {
                if (res) {
                    this.contactsList = [];
                    this.contactsList.push(...res.map(x => new ChosenOptionModel(x.PersonId, `${x.FirstName} ${x.LastName}`)))
                }
            });
    }

    public companyListSearch(search: string, scroll: boolean = false, page: number = 1) {
        if (!scroll) this.personAccountList = [];
        this.accountService.personList(search, page, (accounts) => {
            this.personAccountList = accounts;
        })
    }
    public personListSearch(search: string, scroll: boolean = false, page: number = 1) {
        if (!scroll) {
            let nocompany = this.translate.instant("SELECTCOMPANY");
            this.accountList = [new ChosenOptionModel("", nocompany)];
        }
        this.accountService.companyList(search, page, (accounts) => {
            this.accountList = [...this.accountList].concat(accounts);
        })
    }

    ngOnDestroy() {
        if (this.contractSub) this.contractSub.unsubscribe();
        if (this.userSub) this.userSub.unsubscribe();
        if (this.categoryGroupsSub) this.categoryGroupsSub.unsubscribe();
        if (this.contactSub) this.contactSub.unsubscribe();
    }

    addContract() {
        if (!this.hasRequiredCategories()) {
            const categories = this.allCategoryGroups.filter(x => x.IsRequired).map(x => x.Name);
            this.notify.error(this.translate.instant('YOUMUSTCHOOSECATEGORYOF', { categories: categories.join(', ') }))
        } else {
            this.saveNewCategories();
            this.contractsService.addContract(this.contract, this.back.bind(this))
        }
    }

    editContract() {
        this.saveNewCategories();
        this.contractsService.updateContract(this.contract, (() => this.editMode = false).bind(this));
    }

    // MILESTONES
    openMilestone(open?: boolean) {
        this.currentMilestone = new MilestoneDate({ Date: this.contract.EndDate || this.contract.StartDate });
    }

    closeMilestone() {
        this.currentMilestone = null;
    }

    addMilestone() {
        this.contract.MilestoneDates.push(this.currentMilestone);
        this.closeMilestone();
    }

    editMilestone() {
        this.milestoneEdit = false;
        this.contract.MilestoneDates.splice(this.milestoneIndex, 1, this.currentMilestone);
        this.closeMilestone();
    }

    openEditMilestone(index: number) {
        if (this.editMode) {
            this.milestoneIndex = index;
            this.milestoneEdit = true;
            this.currentMilestone = Object.assign({}, this.contract.MilestoneDates[index]);
        }
    }

    deleteMilestone(index: number) {
        this.contract.MilestoneDates.splice(index, 1);
    }
    //

    //OURPARTY
    closeAddOur() {
        this.selectedAddOur = null;
        this.nonUserParty = new PartyModel({});
    }

    onAddUser(id: string, index: number) {
        const user = this.allUserList.find(x => x.Id === id);
        this.contract.OurParties.push(new PartyModel({ UserId: id, ProfilePictureUrl: user.ProfilePictureUrl }));
        this.userList.splice(index, 1);
        this.contract.OurParties = this.contract.OurParties.slice();
        this.userList = this.userList.slice();
    }

    onRemoveUser(id: string) {
        const user = this.allUserList.find(x => x.Id === id);
        this.contract.OurParties = this.contract.OurParties.filter(x => x.UserId !== id);
        this.userList.push(user);
        this.userList = this.userList.slice();
        this.contract.OurParties = this.contract.OurParties.slice();
    }

    populateUserList() {
        if (this.contract.Id) {
            this.userList = [];
            this.userList = this.allUserList.reduce((res, curr) => {
                if (this.contract.OurParties.map(x => x.UserId).indexOf(curr.Id) === -1) {
                    res.push(curr);
                }
                return res;
            }, []);
        }
    }

    onAddNonUser() {
        this.contract.OurParties.push(this.nonUserParty);
        this.contract.OurParties = this.contract.OurParties.slice();
        this.selectedAddOur = null;
    }

    onRemoveNonUser(party: PartyModel) {
        this.contract.OurParties = this.contract.OurParties.filter(x =>
            x.UserId || (x.FirstName !== party.FirstName && x.LastName !== party.LastName));
    }
    //

    //THEIRPARTY
    getAccountName() {
        let obj = this.accountList.find(x => x.value === this.contract.AccountId);
        if (!obj) obj = this.personAccountList.find(x => x.value === this.contract.AccountId);
        if (obj) return obj.label;
        return '';
    }

    onAccountChosen(account: ChosenOptionModel, isPerson: boolean) {
        if (account.value && !this.contract.TheirParties.find(x => x.AccountId === account.value)) {
            this.accountService.getContactsForAccount(account.value.toString());
            this.contract.TheirParties.push(new PartyModel({ AccountId: account.value, DisplayName: account.label, IsPerson: isPerson }));
            this.contract.TheirParties = this.contract.TheirParties.slice();
            this.companyAccountId = account.value.toString();
        }
    }

    onContactChosen(contacts: Array<ChosenOptionModel>) {
        let index = this.contract.TheirParties.findIndex(x => x.AccountId === this.companyAccountId)
        if (index > -1) {
            this.contract.TheirParties[index].Contacts = [];
            this.contract.TheirParties[index].Contacts.push(...contacts.map(x => { return { AccountId: x.value.toString(), DisplayName: x.label.toString() }; }))
            this.contract.TheirParties[index].Contacts = this.contract.TheirParties[index].Contacts.slice();
            this.contract.TheirParties = this.contract.TheirParties.slice();
        }
    }

    onRemoveTheirAccount(accountId: string) {
        this.contract.TheirParties = this.contract.TheirParties.filter(x => x.AccountId !== accountId);
    }

    onAddNonUserTheir() {
        this.contract.TheirParties.push(this.nonTheirUserParty);
        this.contract.TheirParties = this.contract.TheirParties.slice();
        this.nonTheirUserParty = null;
    }

    onRemoveNonUserTheir(party: PartyModel) {
        this.contract.TheirParties = this.contract.TheirParties.filter(x =>
            x.AccountId || (x.FirstName !== party.FirstName && x.LastName !== party.LastName && x.Email !== party.Email));
    }

    onOpenTheirPopup() {
        this.nonTheirUserParty = new PartyModel({});
    }

    onTheirPopupClose() {
        this.selectedAddTheir = null;
    }
    //

    //CATEGORIES
    arrangeCategoryGroups() {
        let arrayObj = this.allCategoryGroups.reduce((res, curr) => {
            if (this.contract.Categories.map(x => x.Id).indexOf(curr.Id) === -1 && !curr.IsRequired) {
                res.notSelected.push(curr);
            } else {
                res.selected.push(curr);
            }
            return res;
        }, { selected: [], notSelected: [] });
        this.selectedCategoryGroups = arrayObj.selected;
        this.notSelectedCategoryGroups = arrayObj.notSelected;
    }

    selectCategoryGroup(catGroup: CategoryGroup, index: number) {
        this.selectedCategoryGroups.push(catGroup);
        this.notSelectedCategoryGroups.splice(index, 1);
    }

    deselectCategoryGroup(catGroup: CategoryGroup, index: number) {
        this.contract.Categories = this.contract.Categories.filter(x => x.Id !== catGroup.Id);
        this.selectedCategoryGroups.splice(index, 1);
        this.notSelectedCategoryGroups.push(catGroup);
    }

    onCategoryChange(catGroupId: string, event: Array<string>) {
        this.contract.Categories = this.contract.Categories.filter(x => x.Id !== catGroupId)
        event.map(x => this.contract.Categories.push(new ContractCategory({ Id: catGroupId, Name: x })));
    }

    getCategoryItems(id: string) {
        return this.contract.Categories.filter(x => x.Id == id).map(x => x.Name);
    }

    getCatColor(id: string) {
        return this.allCategoryGroups.find(x => x.Id === id).Color;
    }

    getCatFontColor(id: string) {
        return this.allCategoryGroups.find(x => x.Id === id).FontColor || '#FFF';
    }

    saveNewCategories() {
        let editableGroups = this.allCategoryGroups.filter(x => x.IsEditable);

        for (let i = 0; i < editableGroups.length; i++) {
            let group = editableGroups[i];
            let length = group.Categories.length;
            let contractCategories = this.contract.Categories.filter(x => x.Id === group.Id);
            for (let j = 0; j < contractCategories.length; j++) {
                let category = contractCategories[j].Name;
                if (group.Categories.indexOf(category) === -1) {
                    group.Categories.push(category);
                }
            }
            if (group.Categories.length > length) {
                this.catSvc.updateCategoryGroup(group)
            }
        }
    }

    hasRequiredCategories() {
        const reqCats = this.allCategoryGroups.filter(x => x.IsRequired).map(x => x.Id);
        if (!reqCats.length) return true;
        const categoryIds = this.contract.Categories.map(x => x.Id);
        return reqCats.every(x => categoryIds.indexOf(x) > -1);
    }

    openAddCategoryGroup() {
        this.catSvc.openEditCreateCategoryGroup();
    }

    //

    //FILES
    onFileClick(file: FileModel) {
        this.file.emit(file);
    }

    addFile(fileList: Array<any>, fromDetailsComp?: boolean) {
        this.fileProcessing = true;
        if (this.edit) {
            this.contractsService.addFile(fileList[0]).then(res => {
                this.contractsService.updateContract(this.contract, () => {
                    const resObj = JSON.parse(res);
                    const file = new FileModel(resObj.Data);
                    this.contract.Files.push(file);
                    this.fileProcessing = false;
                    if (fromDetailsComp) { // To get ContractsDetails to get updated
                        this.file.emit(file);
                    }
                }, () => this.fileProcessing = false, true);
            }, (err) => {
                this.notify.error('UPLOADFILEFAIL');
                this.fileProcessing = false;
            }).catch(res => {
                this.notify.error('UPLOADFILEFAIL');
                this.fileProcessing = false;
            });
        } else {
            this.contractsService.addTempFile(fileList[0]).then(res => {
                const resObj = JSON.parse(res);
                const file = new FileModel(resObj.Data);
                this.contract.Files.push(file);
                this.fileProcessing = false;
            }, (err) => {
                this.notify.error('UPLOADFILEFAIL');
                this.fileProcessing = false;
            }).catch(res => {
                this.notify.error('UPLOADFILEFAIL');
                this.fileProcessing = false;
            });
        }
    }

    deleteFile(id: string) {
        if (this.edit) {
            this.contractsService.deleteFile(id)
                .subscribe((res) => {
                    if (res.IsSuccess) {
                        const index = this.contract.Files.map(x => { return x.Id; }).indexOf(id);
                        this.contract.Files.splice(index, 1);
                    }
                }, (err) => {
                    this.notify.error('DELETEFILEFAIL');
                });;
        } else {
            this.contractsService.deleteTempFile(id)
                .subscribe((res) => {
                    if (res.IsSuccess) {
                        const index = this.contract.Files.map(x => { return x.Id; }).indexOf(id);
                        this.contract.Files.splice(index, 1);
                    }
                }, (err) => {
                    this.notify.error('DELETEFILEFAIL');
                });
        }
    }
    //

    cancelChanges() {
        this.contract = JSON.parse(JSON.stringify(this.contractUntouched));
        this.editMode = false;
        this.populateUserList();
    }

    clickToClose(event: any) {
        let target = event.target || event.srcElement;
        if (target.id === 'add') {
            this.back();
        }
    }

    back() {
        this.contractsService.showModal(false);
    }
}