import { Component, EventEmitter, Output } from '@angular/core';
import { ContractsService } from '../../../services/contracts.service';

@Component({
    selector: 'left-panel',
    template: `		
    <div [class.systemActive]="sideMenuActive" class="leftPanel" (scroll)="contractsService.onScroll();">
        <div class="leftPanelFilter">
            <div class="form-group">
                <label for="">&nbsp;</label>
                <select id="LeftPanelFilter" (ngModelChange)="changeFilter($event.value)" class="form-control" [ngModel]="selectOption">
                    <option *ngFor="let opt of optionsArray" [value]="opt.key">{{opt.value | translate }}</option>                                  
                </select>
            </div>
            <div class="leftPanelSerach">
                <input id="LeftPanelSearch" #name type="text" (keyup)="onSearch(name.value)" [(ngModel)]="contractsService.searchParams.Name" class="form-control" placeholder="{{ 'SEARCH' | translate }}">
            </div>
        </div>
        <ul class="menuItems">    
            <li *ngIf="(contractsService.contractsListObs() | async)?.length < 1">
                <a>{{'NORECORDSFOUND' | translate }}</a>
            </li>
            <li *ngFor="let con of (contractsService.contractsListObs() | async)">
                <a [routerLink]="['../' + con.Id]" routerLinkActive="active">{{ con.Name }}</a>
            </li>
        </ul>
        <div class="mobileSideMenu" (click)="sideMenuActive = true;">
            <ul class="dots">
                <li></li>
                <li></li>
                <li></li>
            </ul>
        </div>
    </div>
    <div [class.systemActive]="sideMenuActive" class="popupOverlay" id="SideMenuOverlayLeft" (click)="sideMenuActive = false;">
    </div>
	`
})
export class LeftPanelComponent {

    @Output() search = new EventEmitter<string>();
    public optionsArray: Array<any> = [];
    public selectOption: string = 'selected';

    private searchParams: any;
    public sideMenuActive: boolean;

    constructor(public contractsService: ContractsService) { }

    ngOnInit() {
        this.getOptionsArray();
        this.searchParams = this.contractsService.searchParams;
        this.contractsService.getContracts();
    }

    onSearch(val: string) {
        this.contractsService.searchContracts(val);
    }

    changeFilter(val: any) {
        this.selectOption = val;
        if (val === 'all') {
            this.contractsService.reset();
            this.contractsService.getContracts();
        }
        else if (val === 'selected') {
            this.contractsService.searchParams = this.searchParams;
            this.contractsService.getContracts();
        }
    }

    getOptionsArray() {
        this.optionsArray = [
            { 'key': 'all', 'value': 'SHOWALL' },
            { 'key': 'selected', 'value': 'SHOWSELECTION' }
        ];
    }
}