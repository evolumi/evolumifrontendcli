import { NgModule } from '@angular/core';

import { SharedModule } from '../../shared/shared.module';
import { ActionsSharedModule } from '../actions/shared/action-shared.module';

// Components
import { ChannelsComponent } from './channels.component';
import { ConversationComponent } from './workgroups/conversation/conversation.component';
import { FilesComponent } from './workgroups/conversation/files.component';
import { UsersComponent } from './workgroups/conversation/users.component';
import { AddPublicWorkGroupComponent } from './workgroups/modals/add-public-workgroup.component';
import { AddUsersComponent } from './workgroups/modals/add-users.component';
import { ChannelSettingsComponent } from './workgroups/modals/channel-settings.component';
import { ChatComponent } from './workgroups/rightPanelChat/chat.component';
import { WorkGroupsComponent } from './workgroups/workgroups.component';
import { LeftPanelComponent } from './left-panel.component';

@NgModule({
    imports: [
        SharedModule,
        ActionsSharedModule
    ],
    declarations: [
        ConversationComponent,
        FilesComponent,
        UsersComponent,
        AddPublicWorkGroupComponent,
        AddUsersComponent,
        ChannelSettingsComponent,
        ChatComponent,
        WorkGroupsComponent,
        LeftPanelComponent,
        ChannelsComponent
    ],
    providers: [
    ],
    exports: [
        AddPublicWorkGroupComponent,
        ChatComponent,
        ChannelsComponent
    ]
})
export class ChannelsModule { }