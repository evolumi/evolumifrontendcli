import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { OrgGuard } from '../../../services/guards/org-guard';

// Components
import { WorkGroupsComponent } from './workgroups/workgroups.component';
import { ChannelsComponent } from './channels.component';

export const ChannelsRoutes: Routes = [
    {
        path: 'Chat',
        component: ChannelsComponent,
        canActivate: [OrgGuard],
        children: [
            { path: '', redirectTo: 'Dashboard', pathMatch: 'full' },
            { path: 'WorkGroups/:id', component: WorkGroupsComponent },
            { path: 'Chat', component: ChannelsComponent }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(ChannelsRoutes)
    ],
    exports: [RouterModule]
})
export class ChannelsRoutesModule { }