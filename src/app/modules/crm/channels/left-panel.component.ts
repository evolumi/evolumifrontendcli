import { Component, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorage } from '../../../services/localstorage';
import { ConversationModel } from '../../../models/conversation.models';
import { ChannelTypes } from '../../../models/channel.models';
import { SignalRService } from '../../../services/signalRService';
import { ChatService } from '../../../services/chatService';
import { ChosenOptionModel } from '../../shared/components/chosen/chosen.models';
import { ChosenOption } from "../../shared/components/chosen/chosen";
import { ContactsCollection } from '../../../services/contacts.service';
import { AccountsCollection } from '../../../services/accounts.service';
import { UsersService } from '../../../services/users.service';
import { Subscription } from "rxjs/Subscription";
import { OrgService } from "../../../services/organisation.service";
declare var $: JQueryStatic;

@Component({

    selector: 'left-panel',
    templateUrl: './leftPanel.component.html'
})

export class LeftPanelComponent {

    private _conversationReceivedSubscription: Subscription;
    private _conversationCreatedSubscription: Subscription;

    public channels: Array<ConversationModel> = [];
    public privateChannels: Array<ConversationModel> = [];
    public workGroups: Array<ConversationModel> = [];

    public displayAddChannel: boolean = false;
    public displayAddWorkGroup: boolean = false;

    public accounts: Array<ChosenOption> = [];
    public availableUsers: Array<ChosenOption> = null;
    public availableGuestsUsers: Array<ChosenOption> = null;
    public userIds: Array<string> = []; // selected availableUser ids in workgroup Created form
    public guestUserIds: Array<string> = []; // selected contacts User ids in workgroup Created form
    public conversationName: string = "";
    public accountId: string;

    private userUsb: any;
    private chanelSub: any;

    constructor(
        private _localStore: LocalStorage,
        private _router: Router,
        private _signalRService: SignalRService,
        private _chatService: ChatService,
        private _contactService: ContactsCollection,
        private _accountService: AccountsCollection,
        private _userService: UsersService,
        public _orgService: OrgService
    ) { }

    private clearChannels() {
        this.channels = [];
        this.privateChannels = [];
        this.workGroups = [];
    }

    loadInitialChannel() {
        console.log(this.channels.length, this.privateChannels.length, this.workGroups.length);
        if (this.channels.length > 0) {
            this._router.navigateByUrl("/CRM/Chat/WorkGroups/" + this.channels[0].id);
        }
        else if (this.privateChannels.length > 0) {
            this._router.navigateByUrl("/CRM/Chat/WorkGroups/" + this.privateChannels[0].id);
        }
        else if (this.workGroups.length > 0) {
            this._router.navigateByUrl("CRM/Chat/WorkGroups/" + this.workGroups[0].id);
        }
        else {
            this.showAddChannel();
        }
        this.initialLoad = false;
    }

    private initialLoad: boolean = true;

    private subscribeToObservables(): void {
        this.chanelSub = this._chatService.channelsObservable().subscribe(channels => {

            this.clearChannels();
            if (channels) {

                for (var channel of channels) {
                    if (channel.type == "PrivateWorkGroup") {
                        this.privateChannels.push(channel);
                    }
                    else if (channel.type == "Channel") {
                        this.channels.push(channel);
                    }
                    else if (!channel.hidden) {
                        this.workGroups.push(channel);
                    }
                }
                if (this.initialLoad) {
                    this.loadInitialChannel();
                }
            }
        });

        var userId = this._localStore.get('userId');
        this.userUsb = this._userService.activeUserListObervable().subscribe(users => {
            this.availableUsers = [];
            for (var user of users) {
                if (user.value != userId)
                    this.availableUsers.push(new ChosenOptionModel(user.value, user.label));
            }
        })

        this.accountListSeach("");
    }

    private accountListSeach(search: string, scroll: boolean = false, page: number = 1) {
        if (!scroll) this.accounts = [];
        this._accountService.accountList(search, page, (accounts) => {
            this.accounts = [...this.accounts].concat(accounts);
        })
    }

    private subscribeToEvents() {
        this._conversationReceivedSubscription = this._signalRService.conversationReceived.subscribe((converation: any) => {
            this._chatService.getConversations();
        });

        this._conversationCreatedSubscription = this._chatService.conversationCreated.subscribe((conversationId: string) => {
            this._router.navigate(['/CRM/WorkGroups', { id: conversationId }]);
        });
    }

    private unsubscribeFromEvents() {
        if (this._conversationReceivedSubscription) this._conversationReceivedSubscription.unsubscribe();
        if (this._conversationCreatedSubscription) this._conversationCreatedSubscription.unsubscribe();
    }

    public ngOnInit() {
        this.subscribeToObservables();
        this.subscribeToEvents();
    }

    public ngOnDestroy() {
        this.unsubscribeFromEvents();
        if (this.userUsb) this.userUsb.unsubscribe();
        if (this.chanelSub) this.chanelSub.unsubscribe();
    }

    public addChannel() {
        var data = {
            "name": this.conversationName,
            "type": ChannelTypes.channel
        };
        this._chatService.addConversation(data);
        this.hideAddChannel();
    }

    public addPublicWorkGroup() {
        if (this.accountId) {
            var data: any = {};
            data.name = this.conversationName;
            data.type = ChannelTypes.publicWorkGroup;
            data.userIds = this.userIds;
            data.guestUserIds = this.guestUserIds;
            data.accountId = this.accountId;
            this._chatService.addConversation(data);
            this.hideAddWorkGroup();
        }

    }

    public addPrivateWorkGroup() {
        var data: any = {};
        data.name = this.conversationName;
        data.type = ChannelTypes.privateWorkGroup;
        data.userIds = this.userIds;
        this._chatService.addConversation(data);
        this.hideAddWorkGroup();
    }

    public showAddChannel() {
        this.displayAddChannel = true;
    }

    public hideAddChannel() {
        this.displayAddChannel = false;
        this.conversationName = "";
    }

    public showAddWorkGroup() {
        this.displayAddWorkGroup = true;
    }

    public hideAddWorkGroup() {
        this.displayAddWorkGroup = false;
        this.conversationName = "";
        this.userIds = null;
        this.guestUserIds = null;
    }

    public openLeftPanel() {
        $(".leftPanel, #SideMenuOverlayLeft").addClass("systemActive");
    }

    public removeOpenLeftPanel() {
        $(".leftPanel, #SideMenuOverlayLeft").removeClass("systemActive");
    }

    public changeAccount(accountId: string) {
        this.accountId = accountId;
        this.getContacts(accountId);
    }

    private getContacts(accountId: string) {
        this._contactService.getContactForAccountId(accountId).subscribe(response => {
            let contacts: Array<any> = [];
            for (var item of response) {
                contacts.push(new ChosenOptionModel(item.Id, item.FullName));
            }
            this.availableGuestsUsers = contacts;
        })
    }
}