import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { LocalStorage } from '../../../../../services/localstorage';
import { ChatService } from '../../../../../services/chatService';
import { UserDetailModel } from '../../../../../models/user.models';
import { ConversationModel } from '../../../../../models/conversation.models';
import { Subscription } from 'rxjs/Subscription';
import { ButtonPanelService } from '../../../../../services/button-panel.service';

@Component({

    selector: 'channel-settings',
    templateUrl: './channelSettings.html',
})

export class ChannelSettingsComponent {

    @Input() conversation: ConversationModel;
    private _userdetails: UserDetailModel;
    public showSettings: boolean = false;
    private buttonSub: Subscription;

    constructor(
        private _localStore: LocalStorage,
        private _chatService: ChatService,
        private _router: Router,
        private translate: TranslateService,
        private button: ButtonPanelService
    ) {
        this.buttonSub = this.button.commandObservable().subscribe(command => {
            if (command == "channel-settings") { this.show() }
        });


        this._userdetails = <UserDetailModel>JSON.parse(this._localStore.get('userDetails'));
    }

    public delete() {
        var conf = confirm(this.translate.instant("WARNDELCHANNEL"));
        if (conf) {
            this._chatService.deleteConvrsation(this.conversation.id);
            this._router.navigate(["CRM/Chat"]);
        }
    };

    public leave() {
        var conf = confirm(this.translate.instant("WARNLEAVECHANNEL"));
        if (conf) {
            this._chatService.leaveChannel(this.conversation.id, this._userdetails.UserId, res => {
                if (res) this._router.navigate(["CRM/Chat"]);
            });
        }
    }

    ngOnInit() {
    }

    public show() {
        this.showSettings = true;
    }

    public hide() {
        this.showSettings = false;
    }

    ngOnDestroy() {
        if (this.buttonSub) this.buttonSub.unsubscribe();
    }

}
