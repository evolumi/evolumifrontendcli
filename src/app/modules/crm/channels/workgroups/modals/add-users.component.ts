import { Component, Input, SimpleChange } from '@angular/core';
import { LocalStorage } from '../../../../../services/localstorage';
import { ChatService } from '../../../../../services/chatService';
import { ContactsCollection } from '../../../../../services/contacts.service';
import { UserDetailModel } from '../../../../../models/user.models';
import { ChosenOptionModel } from '../../../../shared/components/chosen/chosen.models';
import { ConversationModel } from '../../../../../models/conversation.models';
import { ChannelTypes } from '../../../../../models/channel.models';
import { ChosenOption } from "../../../../shared/components/chosen/chosen";
import { UsersService } from '../../../../../services/users.service';
import { Subscription } from 'rxjs/Subscription';
import { ButtonPanelService } from '../../../../../services/button-panel.service';

@Component({
    selector: 'add-users-to-conversation',
    templateUrl: './addUsers.html',
})

export class AddUsersComponent {

    @Input() conversation: ConversationModel;
    private _userdetails: UserDetailModel;

    public availableUsers: Array<ChosenOption> = null;
    public userIds: Array<string> = []; // selectedUser ids in AddUser form
    public availableGuestsUsers: Array<ChosenOption> = null;
    public guestUserIds: Array<string> = []; // selectedContacts ids in AddUser form
    public displayAddUser: boolean = false;

    private userSub: Subscription;
    private buttonSub: Subscription;

    constructor(
        private _userService: UsersService,
        private _localStore: LocalStorage,
        private _contactService: ContactsCollection,
        private _chatService: ChatService,
        private _button: ButtonPanelService
    ) {
        this._userdetails = <UserDetailModel>JSON.parse(this._localStore.get('userDetails'));

        this.buttonSub = this._button.commandObservable().subscribe(command => {
            if (command == "channel-add-user") { this.showAddUsers() }
        });
    }

    private subscribeToObservables(): void {

        this.userSub = this._userService.activeUserListObervable().subscribe(response => {
            // from _userService data comes in PascalCase
            let users: Array<any> = [];
            if (this.conversation) {
                for (var item of response) {
                    // Remove users already in the chat.
                    if (!this.conversation.users.find(x => x.id == item.value)) {
                        users.push(item);
                    }
                }
            }
            this.availableUsers = users;
        });

        if (this.conversation.accountId) {

            this._contactService.getContactForAccountId(this.conversation.accountId).subscribe(response => {
                var contacts: Array<any> = [];
                if (this.conversation) {
                    for (var item of response) {
                        if (!this.conversation.contacts.find(x => x.id == item.Id)) {
                            contacts.push(new ChosenOptionModel(item.Id, item.FullName));
                        }
                    }
                }
                this.availableGuestsUsers = contacts;
            });
        }
    }


    private getUsers() {
        if (this.conversation != null) {
            for (var contact of this.conversation.contacts) {
                this.guestUserIds.push(contact.id);
            }
            for (var user of this.conversation.users) {
                var isContact = false;
                for (var contact of this.conversation.contacts) {
                    if (contact.Email == user.email) {
                        isContact = true
                    }
                }

                if (!isContact)
                    this.userIds.push(user.id);
            }
            this.subscribeToObservables();
        }
    }

    ngOnInit() {
    }

    ngOnDestroy() {
        if (this.userSub) this.userSub.unsubscribe();
        if (this.buttonSub) this.buttonSub.unsubscribe();
    }

    ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
        for (let propName in changes) {
            if (propName == 'conversation') {
                this.getUsers();
            }
        }
    }

    public showAddUsers() {
        this.displayAddUser = true;
    }

    public hideAddUsers() {
        this.userIds == [];
        this.guestUserIds = [];
        this.displayAddUser = false;
    }

    public addUsersToConversation() {
        this._chatService.addUsersToConversation(this.conversation.id, this.userIds, this.guestUserIds);
        this.hideAddUsers();
    }

    public disabledAddUsersSubmit(): boolean {
        return this.userIds.length == 0 && this.guestUserIds.length == 0;
    }

    public isPublicWorkGroup() {
        return this.conversation != null && this.conversation.type == ChannelTypes.publicWorkGroup
    }
}
