import { Component, Input, EventEmitter, Inject } from '@angular/core';
import { ContactsCollection } from '../../../../../services/contacts.service';
import { UsersService } from '../../../../../services/users.service';
import { ChatService } from '../../../../../services/chatService';
import { ChannelTypes } from '../../../../../models/channel.models';
import { ChosenOptionModel } from '../../../../shared/components/chosen/chosen.models';
import { LocalStorage } from '../../../../../services/localstorage';
import { ChosenOption } from "../../../../shared/components/chosen/chosen";
import { IntercomService } from '../../../../../services/interecomService';
import { Subscription } from "rxjs/Subscription";

@Component({
    selector: 'add-work-group',
    templateUrl: './addPublicWorkGroup.html',
    providers: [LocalStorage]
})

export class AddPublicWorkGroupComponent {

    @Input() accountId: string;
    public availableUsers: Array<ChosenOption> = null;
    public userIds: Array<string> = [];//selectedUser ids in workgroup Created form
    public availableGuestsUsers: Array<ChosenOption> = null;
    public guestUserIds: Array<string> = [];
    public conversationName: string = "";
    public displayAddWorkGroup: boolean = false;
    private _conversationCreatedSubscription: Subscription;
    private redirectAfterAdd: boolean = true;
    private userSub: any;
    public upgrade: boolean;

    constructor(
        @Inject(IntercomService) public intercomService: IntercomService,
        private _localStore: LocalStorage,
        private _userService: UsersService,
        private _contactService: ContactsCollection,
        private _chatService: ChatService
    ) {

    }

    private subscribeToObservables(): void {
        var userdetails = JSON.parse(this._localStore.get('userDetails'));
        this.userSub = this._userService.activeUserListObervable().subscribe(response => {
            var users: Array<any> = [];
            for (var item of response) {
                if (item.value != userdetails.UserId)
                    // from _userService data comes in PascalCase
                    users.push(new ChosenOptionModel(item.value, item.label));
            }
            this.availableUsers = users;
        });
    }
    private subscribeToEvents() {
        this._conversationCreatedSubscription = this._chatService.conversationCreated.subscribe((conversationId: any) => {
            this._chatService.getWorkGroupForAccount(this.accountId);
        });
    }

    private unsubscribeFromEvents() {
       if(this._conversationCreatedSubscription) this._conversationCreatedSubscription.unsubscribe();
    }

    public ngOnInit() {
        this.subscribeToObservables();
        this.subscribeToEvents();
        if (this.accountId != null)
            this.getContacts(this.accountId);
    }

    ngOnDestroy(){
        this.unsubscribeFromEvents();
        if (this.userSub) this.userSub.unsubscribe();
    }

    public addPublicWorkGroup() {

        var data: any = {};
        data.name = this.conversationName;
        data.type = ChannelTypes.publicWorkGroup;
        data.userIds = this.userIds;
        data.guestUserIds = this.guestUserIds;
        data.accountId = this.accountId;
        this._chatService.addConversation(data, this.redirectAfterAdd);
        this.intercomService.trackEvent('workgroup-created');
        this.hideAddWorkGroup();
    }

    public hideAddWorkGroup() {
        this.displayAddWorkGroup = false;
        this.conversationName = "";
        this.userIds = null;
    }

    public showAddWorkGroup(redirect: boolean = true) {
        this.displayAddWorkGroup = true;
        this.redirectAfterAdd = redirect;
        console.log("trying to open", this.displayAddWorkGroup);
    }

    private getContacts(accountId: string) {
        this._contactService.getContactForAccountId(accountId).subscribe(response => {
            let contacts: Array<any> = [];
            for (var item of response) {
                contacts.push(new ChosenOptionModel(item.Id, item.FullName));
            }
            this.availableGuestsUsers = contacts;
        })
    }
}
