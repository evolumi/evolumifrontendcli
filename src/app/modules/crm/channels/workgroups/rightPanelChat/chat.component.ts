import { Component } from '@angular/core';
import { ConversationModel } from '../../../../../models/conversation.models';
import { ChatService } from '../../../../../services/chatService';
import { LocalStorage } from '../../../../../services/localstorage';
import { UserIconListModel } from '../../../../../models/user.models';
import { UserDetailModel } from '../../../../../models/user.models';
import { ChannelTypes } from '../../../../../models/channel.models';

@Component({
    selector: 'chat',
    templateUrl: './chat.html',
})

export class ChatComponent {

    private _userdetails: UserDetailModel;
    public displayChat: boolean;
    public conversation: ConversationModel = null;
    public users: Array<UserIconListModel> = [];
    public conversationName: string = "";

    private conversationSub: any;

    constructor(
        private _localStore: LocalStorage,
        private _chatService: ChatService
    ) {
        this._userdetails = <UserDetailModel>JSON.parse(this._localStore.get('userDetails'));
    };

    public ngOnInit() {
        this.subscribeToObservables();
    }

    public ngOnDestroy() {
        if (this.conversationSub) this.conversationSub.unsubscribe();
    }

    public routerOnActivate() {
        return new Promise((res, rej) => setTimeout(() => res(1), 500));
    };

    public routerOnDeactivate() {
        return new Promise((res, rej) => setTimeout(() => res(1), 500));
    };

    private subscribeToObservables() {
        this.conversationSub = this._chatService.conversation$.subscribe(response => {
            console.log(response);
            if (response) {
                this.conversation = response;
                this.users = response.users;
                this.conversationName = this.conversation.name;
            }
        });
    }

    public back() {
        this.displayChat = false;
        this.conversation = null;
        this.conversationName = "";
    };

    public showChat(conversationId: string) {
        this._chatService.getConversation(conversationId);
        this.displayChat = true;
    }

    public isNotChanel() {
        return this.conversation != null && this.conversation.type != ChannelTypes.channel
    }

    public showChannel() {
        this.conversation.hidden = false;
        this._chatService.toggleHidden(this.conversation.id);
    }

    public hideChannel() {
        this.conversation.hidden = true;
        this._chatService.toggleHidden(this.conversation.id);
    }

    public leaveChannel() {
        var conf = confirm('Are you sure to leave this conversation?');
        if (conf && this.conversation != null) {
            this._chatService.leaveChannel(this.conversation.id, this._userdetails.UserId, (response) => {
                if (response == true) {
                    // refresh public group list
                    this._chatService.getWorkGroupForAccount(this.conversation.accountId);
                    this.back();
                }
            });
        }
    }
}