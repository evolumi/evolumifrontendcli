import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LocalStorage } from '../../../../services/localstorage';
import { ChatService } from '../../../../services/chatService';
import { ConversationModel } from '../../../../models/conversation.models';
import { UserDetailModel } from '../../../../models/user.models';
import { ChannelTypes } from '../../../../models/channel.models';
import { Subscription } from 'rxjs/Subscription';
import { ButtonPanelService } from '../../../../services/button-panel.service';
import { OrgService } from "../../../../services/organisation.service";

@Component({
    templateUrl: './workgroups.html',
})

export class WorkGroupsComponent {
    private sub: Subscription;
    private buttonSub: Subscription;
    private conversationSub: Subscription;

    private _userdetails: UserDetailModel;
    public conversationId: string = '';
    public conversationName: string = '';
    public accountId: string = '';
    public accountName: string = '';
    public accountIsPerson: boolean;
    public conversation: ConversationModel = null;
    public showUsers: boolean = false;
    public rightpanel: boolean = false;

    constructor(
        public _orgService: OrgService,
        private _localStore: LocalStorage,
        route: ActivatedRoute,
        private _router: Router,
        private _chatService: ChatService,
        private button: ButtonPanelService
    ) {

        this.button.addPanelButton("", "channel-toggle-right", "columns");
        this.button.addPanelButton("SETTINGS", "channel-settings", "briefcase");

        this.buttonSub = this.button.commandObservable().subscribe(command => {
            if (command == "channel-toggle-right") { this.cahtrightAccess() }
        });
        this.rightpanel = window.innerWidth > 767;


        this.subscribeToObservables();

        this._userdetails = <UserDetailModel>JSON.parse(this._localStore.get('userDetails'));
        this.sub = route.params.subscribe(params => {
            this.conversationId = params['id'];
            this._chatService.getConversation(this.conversationId);
        });
    }

    private subscribeToObservables(): void {
        this.conversationSub = this._chatService.conversation$.subscribe(response => {
            this.conversation = response;
            this.conversationName = response.name;
            this.accountId = response.accountId;
            this.accountName = response.accountName;
            this.accountIsPerson = response.accountIsPerson;
            this.showUsers = response.type != "Channel";
            if (this.showUsers)
                this.button.addPanelButton("USERADD", "channel-add-user", "plus");
            else
                this.button.removeButton("channel-add-user");
        });
    }

    public cahtrightAccess() {
        this.rightpanel = !this.rightpanel;
    }

    public leaveChannel() {
        var conf = confirm('Are you sure to leave this conversation?');
        if (conf) {
            this._chatService.leaveChannel(this.conversationId, this._userdetails.UserId, (response) => {
                this._chatService.getConversations();
                this._router.navigate(['/CRM', 'Chatt']);
            });
        }
    }

    public isNotChannel() {
        return this.conversation != null && this.conversation.type != ChannelTypes.channel
    }

    public isPublicWorkGroup() {
        return this.conversation != null && this.conversation.type == ChannelTypes.publicWorkGroup
    }

    ngOnDestroy() {
        if (this.sub) this.sub.unsubscribe();
        if (this.conversationSub) this.conversationSub.unsubscribe();
        if (this.buttonSub) this.buttonSub.unsubscribe();

        this.button.removeButtons(["channel-settings", "channel-add-user", "channel-toggle-right"]);

        // this is set just to let Chatservice know we are not on this page anymore. So Notifications will show.
        this._chatService.conversationId = "";
    }
}
