import { Component, Input, EventEmitter, AfterViewChecked, OnChanges, AfterViewInit, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { LocalStorage } from '../../../../../services/localstorage';
import { ChatService } from '../../../../../services/chatService';
import { SignalRService } from '../../../../../services/signalRService';
import { EventTypes } from '../../../../../models/event.models';
import { MessageModel } from '../../../../../models/message.models';
import { UserDetailModel } from '../../../../../models/user.models';
import { ConversationModel } from '../../../../../models/conversation.models';
import { Subscription } from "rxjs/Subscription";

declare var $: JQueryStatic;

@Component({
    selector: 'conversation-chat',
    templateUrl: './conversation.html',
})

export class ConversationComponent implements AfterViewChecked, AfterViewInit, OnChanges {

    @Input() conversation: ConversationModel;

    private _prevChatHeight: number = 0;
    private _currentPage: number = 1;
    private _pageSize: number = 20;
    private _events: string;
    private _userdetails: UserDetailModel;
    private _elChatList: Element;
    private _currentScrollPosition: number = 0;
    private _canLoadMore: boolean = true;
    private _newDataLoaded: boolean = false;

    private _connectionEstablishedSubscription: Subscription;
    private _connectionStatusSubscription: Subscription;
    private _messageReceivedSubscription: Subscription;

    private messageSub: any;
    private messageSendSub: any;

    public newMessage: string = '';
    public canSendMessage: Boolean = false;
    public messages: Array<MessageModel> = [];
    public isConnected: Boolean = false;
    public status: string;
    //private chatInput: HTMLElement;

    @ViewChild("chatinput") chatInput: HTMLElement;
    constructor(
        private _localStore: LocalStorage,
        private _signalRService: SignalRService,
        private _chatService: ChatService,
        private translate: TranslateService
    ) {
        this.messages = new Array<MessageModel>();
        this.status = _signalRService.connectionExists ? "" : this.translate.instant("CONNECTING");
        this.isConnected = _signalRService.connectionExists;
        console.log("[Conversation] Connection exists: " + _signalRService.connectionExists);
        this._userdetails = <UserDetailModel>JSON.parse(this._localStore.get('userDetails'));
    }

    setFocus() {
        setTimeout(() => {
            console.log("trying to focus input", this.chatInput);
            this.chatInput.focus();
        }, 20);
    }

    ngAfterViewInit() {
        (<any>window).x = function () {
            console.log("testar");
            this.setFocus();
        };
    }

    private subscribeToEvents(): void {

        this._connectionEstablishedSubscription = this._signalRService.connectionEstablished.subscribe((response: any) => {
            this.isConnected = response;
            if (this.canSendMessage) {
                this.setFocus();
            }
        });

        this._connectionStatusSubscription = this._signalRService.connectionStatus.subscribe((response: any) => {
            this.status = response;
        });

        this._messageReceivedSubscription = this._signalRService.messageReceived.subscribe((response: any) => {
            this._events = EventTypes.filter;
            this.newMessage = '';
            var mappedMessage = <MessageModel>response.message;
            mappedMessage.isMe = mappedMessage.userCreatedId === this._userdetails.UserId;

            if (this.conversation != null && this.conversation.id === response.conversationId) {
                this.messages.push(mappedMessage);
            }
        });
    }

    private subscribeToObservables(): void {

        this.messageSendSub = this._chatService.messageSend$.subscribe(response => {
            console.log("[Conversation] MessageSend");
            this.canSendMessage = response;
            this.setFocus();
        });

        this.messageSub = this._chatService.messages$.subscribe(response => {
            this._canLoadMore = response.length == this._pageSize;
            if (this._events !== EventTypes.scroll)
                this.messages = [];
            for (var i = 0; i < response.length; i++) {
                this.messages.unshift(response[i]);
            }
            this._newDataLoaded = true;
        });
    }

    private unsubscribeFromEvents() {
        if (this._connectionEstablishedSubscription) this._connectionEstablishedSubscription.unsubscribe();
        if (this._connectionStatusSubscription) this._connectionStatusSubscription.unsubscribe();
        if (this._messageReceivedSubscription) this._messageReceivedSubscription.unsubscribe();
    }

    private unsubscribeFromObservables() {
        if (this.messageSendSub) this.messageSendSub.unsubscribe();
        if (this.messageSub) this.messageSub.unsubscribe();
    }

    private canScrollDown(): boolean {
        /* compares prev and current scrollHeight */

        var can = (this._prevChatHeight !== this._elChatList.scrollHeight && this._events !== EventTypes.scroll);

        this._prevChatHeight = this._elChatList.scrollHeight;

        return can;
    }

    private scrollDown(): void {
        this._elChatList.scrollTop = this._elChatList.scrollHeight;
    }

    private getMessages() {
        if (this.conversation != null) {
            if (this._events === EventTypes.filter)
                this._currentPage = 1;
            if (this._canLoadMore)
                this._chatService.getMessages(this.conversation.id, this._currentPage, this._pageSize);
        } else {
            this._canLoadMore = true;
            this._currentPage = 1;
            this._currentScrollPosition = 0;
            this.messages = new Array<MessageModel>();
        }
    };

    ngOnInit() {
        this.subscribeToEvents();
        this.subscribeToObservables();
        this._events = EventTypes.filter;
        this.chatInput = document.getElementById("chatmessageinput");

    }

    ngOnDestroy() {
        console.log("[Conversaiton] Unsubscribing");
        this.unsubscribeFromEvents();
        this.unsubscribeFromObservables();
    }

    ngAfterViewChecked(): void {

        this._elChatList = document.querySelector('.messageBox');
        /* need canScrollDown because it triggers even if you enter text in the textarea */

        if (this.canScrollDown()) {
            this.scrollDown();
        }

        if (this._newDataLoaded) {
            this._newDataLoaded = false;
            var scrollHeight = document.getElementsByClassName("messageBox")[0].scrollHeight;
            $('.messageBox').scrollTop(scrollHeight - this._currentScrollPosition);
        }
    }

    ngOnChanges(changes: any) {
        if (changes && changes["conversation"] && changes["conversation"].currentValue) {
            this._canLoadMore = true;
            this._currentPage = 1;
            this._currentScrollPosition = 0;
            this.messages = new Array<MessageModel>();

            this.canSendMessage = true;
            this.getMessages();
            this.newMessage = "";
        }
    }

    public sendMessage = (event: JQueryKeyEventObject) => {
        if (event.keyCode === 13 && this.newMessage !== '' && this.canSendMessage) {
            this.canSendMessage = false;
            let formData: FormData = new FormData();
            formData.append('Text', this.newMessage);
            this._chatService.sendMessage(this.conversation.id, formData);
        }
    }

    public upload(fileList: Array<any>) {
        let formData: FormData = new FormData();
        console.log('UPLOADFILES', fileList);
        if (fileList.length > 0) {
            this.canSendMessage = false;
            this.newMessage = 'Sending file(s)...';
            for (var i = 0; i < fileList.length; i++) {
                formData.append('files' + i, fileList[i]);
            }
            console.log('UPLOADFILES2', formData);
            this._chatService.sendMessage(this.conversation.id, formData);
        }
    }

    public onScroll = (event: JQueryEventObject) => {
        if (event.target.scrollTop == 0) {
            this._currentPage++;
            this._events = EventTypes.scroll;
            this.getMessages();
            this._currentScrollPosition = event.target.scrollHeight;
        }
    }
}
