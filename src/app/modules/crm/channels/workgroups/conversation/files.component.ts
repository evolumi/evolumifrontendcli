import { Component, EventEmitter, Input, SimpleChange } from '@angular/core';
import { LocalStorage } from '../../../../../services/localstorage';
import { ChatService } from '../../../../../services/chatService';
import { SignalRService } from '../../../../../services/signalRService';
import { MessageModel } from '../../../../../models/message.models';
import { UserDetailModel } from '../../../../../models/user.models';
import { ConversationModel } from '../../../../../models/conversation.models';
import { Subscription } from "rxjs/Subscription";

@Component({
    selector: 'conversation-files',
    templateUrl: './files.html',
})

export class FilesComponent {

    @Input() conversation: ConversationModel;
    private _messageReceivedSubscription: Subscription;
    private _userdetails: UserDetailModel;
    public files: Array<MessageModel> = [];

    private chatSub: Subscription;

    constructor(
        private _localStore: LocalStorage,
        private _signalRService: SignalRService,
        private _chatService: ChatService,
    ) {
        this._userdetails = <UserDetailModel>JSON.parse(this._localStore.get('userDetails'));
    }

    private subscribeToEvents(): void {
        this._messageReceivedSubscription = this._signalRService.messageReceived.subscribe((response: any) => {
            var mappedMessage = <MessageModel>response.message;
            mappedMessage.isMe = mappedMessage.userCreatedId === this._userdetails.UserId;
            if (mappedMessage.isFile && this.conversation != null && this.conversation.id === response.conversationId) {
                this.files.push(mappedMessage);
            }
        });
    }

    private subscribeToObservables(): void {
        this.chatSub = this._chatService.files$.subscribe(response => {
            this.files = response
        });
    }

    private unsubscribeFromEvents() {
        if (this._messageReceivedSubscription) this._messageReceivedSubscription.unsubscribe();
    }

    private getFiles() {
        if (this.conversation != null) {
            this._chatService.getFiles(this.conversation.id);
        } else {
            this.files = new Array<MessageModel>();
        }
    };

    ngOnInit() {
        this.subscribeToEvents();
        this.subscribeToObservables();
        this.getFiles();
    }

    ngOnDestroy() {
        this.unsubscribeFromEvents();
        if (this.chatSub) this.chatSub.unsubscribe();
    }

    ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
        for (let propName in changes) {
            if (propName == 'conversation') {
                this.getFiles();
            }
        }
    }
}
