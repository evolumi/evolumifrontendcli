import { Component, Input, EventEmitter, SimpleChange } from '@angular/core';
import { LocalStorage } from '../../../../../services/localstorage';
import { SignalRService } from '../../../../../services/signalRService';
import { UserIconListModel } from '../../../../../models/user.models';
import { UserDetailModel } from '../../../../../models/user.models';
import { ConversationModel } from '../../../../../models/conversation.models';
import { Subscription } from "rxjs/Subscription";
//import {AddUsers} from '../modals/addUsers';

@Component({
    selector: 'conversation-users',
    templateUrl: './users.html',
})

export class UsersComponent {

    @Input() conversation: ConversationModel;
    private _userdetails: UserDetailModel;
    private usersListReceivedSubscription: Subscription;
    public users: Array<UserIconListModel> = [];

    constructor(
        private _localStore: LocalStorage,
        private _signalRService: SignalRService
    ) {
        this._userdetails = <UserDetailModel>JSON.parse(this._localStore.get('userDetails'));
    }

    private subscribeToEvents(): void {
        this.usersListReceivedSubscription = this._signalRService.usersListReceived.subscribe((response: any) => {
            console.log("[SignalR] Recived new user list. for conversation: " + response.conversationId);
            if (this.conversation != null && this.conversation.id === response.conversationId) {
                this.users = response.users;
            }
        });
    }

    private unsubscribeFromEvents() {
        if (this.usersListReceivedSubscription) this.usersListReceivedSubscription.unsubscribe();
    }

    private getUsers() {
        if (this.conversation != null) {
            this.users = this.conversation.users;
        }
    }

    ngOnInit() {
        this.subscribeToEvents();
    }

    ngOnDestroy() {
        this.unsubscribeFromEvents();
    }

    ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
        for (let propName in changes) {
            if (propName == 'conversation') {
                this.getUsers();
            }
        }
    }
}
