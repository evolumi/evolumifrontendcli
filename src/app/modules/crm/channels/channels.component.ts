import { Component, ViewChild } from '@angular/core';
import { LeftPanelComponent } from './left-panel.component';

@Component({
    templateUrl: './channels.html',
})
export class ChannelsComponent {

    @ViewChild("leftpanel") leftPanel: LeftPanelComponent;
    constructor() {
    }

}
