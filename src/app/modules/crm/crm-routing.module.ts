import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { Permissions } from '../../services/organisation.service';

// Guards
import { OrgGuard } from '../../services/guards/org-guard';
import { AuthGuard } from '../../services/guards/auth-guard';
import { EvoRequireGuard } from '../../services/guards/evo-require-guard';

// Components
import { CRMComponent } from './crm.component';
import { PayComponent } from './pay/pay.component';

import { AccountsRoutes } from './accounts/account-routes';
import { ActionsRoutes } from './actions/actions-routes';
import { DealsRoutes } from './deals/deals-routes';
import { InsightRoutes } from './insights/insight-routes';
import { SettingsRoutes } from './settings/settings-routes';
import { ContractsRoutes } from './contracts/contracts-routes';
import { ChannelsRoutes } from "./channels/channels-routes";
import { DashboardRoutes } from "./dashboard/dashboard-routing.module";
import { SalesCompetitionRoutes } from './sales-competition/sales-competition.routes';
import { PackagesRoutes } from './packages/packages.routes';
import { NewslettersRoutes } from './newsletters/newsletters.routes';
import { CalendarComponent } from './calendar/calendar.component';

export const CrmRoutes: Routes = [
  {
    path: 'CRM',
    component: CRMComponent,
    canActivate: [AuthGuard, OrgGuard],
    children: [
      { path: '', redirectTo: 'Dashboard', pathMatch: 'full' },
      { path: 'Boards/Dashboard', redirectTo: 'Dashboard', pathMatch: 'full' },
      { path: 'Pay/:currency/:amount', component: PayComponent, canActivate: [EvoRequireGuard], data: { permissions: [Permissions.HandlePayment] } },
      ...DashboardRoutes,
      ...AccountsRoutes,
      ...ActionsRoutes,
      ...DealsRoutes,
      ...InsightRoutes,
      ...SettingsRoutes,
      ...ContractsRoutes,
      ...ChannelsRoutes,
      ...SalesCompetitionRoutes,
      ...PackagesRoutes,
      ...NewslettersRoutes,
      { path: 'Calendar', component: CalendarComponent }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(CrmRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class CrmRoutingModule { }





