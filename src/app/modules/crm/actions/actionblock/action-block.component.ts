import { TranslateService } from '@ngx-translate/core';
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ActionsModel } from '../../../../models/actions.models';
import { ActionsCollection, ActionGroup } from '../../../../services/actionservice';
import { ActionslistCollection } from '../../../../services/ActionList.service';
import { ActionsflowCollection } from '../../../../services/Actionsflow.service';

@Component({

    selector: 'ActionBlockByAccount',
    template: ` 
                <div class="title">
                    <h4>{{ 'ACTIONS' | translate }}</h4>

                    <a *ngIf="enableAdd" (click)="newAction()" class="btn pull-right">+</a>

                </div>
                <div class="content">
                <ul>
                    <li *ngFor="let action of _action.actionsByAccountId">
                        <checkboxes (updateFromCheckBox)="updateleftPannel()" [action]='action'></checkboxes>
                         <a class="link" (click)="_action.showGroupByActionId(action.Id)"> {{action.Name}}</a>
                         
                         <a *ngIf="enableDelete" class="dlt pull-right" (click)="deleteAction(action.Id)"><i class="fa fa-trash"></i></a>
                         <br>
                         <font>{{action.StartDate | datetime}}</font>
                         <font class="pull-right">{{action.Type}}</font>
                       <br>
                    </li>
                </ul>
                <span *ngIf="_action.actionsByAccountId.length==0">{{'NOACTIONS' | translate }}</span> 
                </div>`
})

export class ActionBlockByAccountComponent {

    @Input() enableDelete: boolean;
    @Input() enableAdd = true;
    @Input() accountId: string;
    @Output() updateFromCheckBox = new EventEmitter();

    private sub: any;

    constructor(public _action: ActionsCollection, public _actionlist: ActionslistCollection, private translate: TranslateService) { }

    ngOnInit() {
        this.sub = this._action.actionChangeObservable().subscribe(res => {
            if (this.accountId)
                this._action.getActionByAccountId(this.accountId);
        });
    }

    deleteAction(id: string) {
        var conf = confirm(this.translate.instant("WARNDELACTION"));
        if (conf) {
            this._action.deleteAction(id);
        }
    }

    updateleftPannel() {
        this.updateFromCheckBox.next(1);
        this._actionlist.getActions();

    }

    updateActionStatus(actionId: string, event: any) {
        this._action.updateActionStatus(actionId, event.srcElement.checked);
    }

    ngOnChanges(changes: any) {
        if (changes.accountId.currentValue !== '' && typeof changes.accountId.currentValue !== 'undefined') {
            this._action.getActionByAccountId(changes.accountId.currentValue);
            this.accountId = changes.accountId.currentValue;
        }

    }

    newAction() {
        let group = new ActionGroup();
        group.AccountId = this.accountId;
        this._action.selectedGroup = group;
    }

    reload() {
        this._action.getActionByAccountId(this.accountId);
    }

    ngOnDestroy() {
        if (this.sub) this.sub.unsubscribe();
    }
}