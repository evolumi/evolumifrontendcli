import { Component, Input, EventEmitter, Output } from '@angular/core';
import { ActionsModel } from '../../../../models/actions.models';
import { ActionsCollection } from '../../../../services/actionservice';
import { NotificationService } from '../../../../services/notification-service';

@Component({

	selector: 'checkboxes',
	template: `<label class="customCheckBox">
					<input type="checkbox" [ngModel]="(action.Status === 'Open')"  (change)="updateActionStatus(action)">
					<div class="custonCkeck"></div>
				</label>`

})

export class CheckboxComponent {
	@Input() action: any;
	@Output() updateFromCheckBox = new EventEmitter;
	constructor(public _action: ActionsCollection, public _notify: NotificationService) { }

	updateActionStatus(action: ActionsModel) {
		action.Status = (action.Status == 'Open') ? 'Closed' : 'Open';
		this._action.updateStatusForFlow(action.Id, action.Status)

			.subscribe((res: any) => {
				this.updateFromCheckBox.next(1);
				this._notify.success('Action status updated');

				if (this._action.getActionByAccountId.length > 0) {
					let ind = this.action.actionsByAccountId.findIndex((x: any) => x.Id === action.Id);
					if (ind != -1) this._action.actionsByAccountId.splice(ind, 1);
				}

			})


	}
}