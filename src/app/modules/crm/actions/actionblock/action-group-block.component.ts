import { Component, Input } from '@angular/core';
import { ActionsCollection, ActionGroup, GroupFilter } from '../../../../services/actionservice';
import { TranslateService } from '@ngx-translate/core';
import { NotificationService } from '../../../../services/notification-service';
import { Subscription } from "rxjs/Subscription";

@Component({
    selector: 'action-group-block',
    templateUrl: './action-group-block.component.html'
})

export class ActionGroupBlockComponent {

    @Input() enableDelete: boolean;
    @Input() enableAdd = true;
    @Input() accountId: string;

    public groups: Array<ActionGroup> = [];
    private filter: GroupFilter;
    private groupChangeSub: Subscription;
    private actionChangeSub: Subscription;

    constructor(
        private notify: NotificationService,
        public _action: ActionsCollection,
        private translate: TranslateService) {
        this.filter = { Page: 0, PageSize: 100 }
        this.groupChangeSub = this._action.actionGroupChangeObservable().subscribe(res => {
            if (res) {
                let index = this.groups.findIndex(x => x.Id == res.Id)
                if (res.AccountId == this.accountId) {
                    if (index == -1) {
                        this.groups.push(res);
                    }
                    else {
                        this.groups.splice(index, 1, res);
                    }
                }
                else {
                    if (index != -1) {
                        this.groups.splice(index, 1);
                    }
                }
            }
            else {
                let filter: GroupFilter = { AccountId: this.accountId, Page: 0, PageSize: 100 };
                this._action.getGroups(filter).subscribe(data => {
                    if (data.IsSuccess) {
                        this.groups = data.Data;
                    }
                });
            }
        });

        this.actionChangeSub = this._action.actionChangeObservable().subscribe(res => {
            this.getGroups();
        });
    }


    ngOnInit() {

    }

    newGroup() {
        let group = new ActionGroup();
        group.AccountId = this.accountId;
        this._action.selectedGroup = group;
    }

    scroll() {
        this.filter.Page++;
        this.getGroups();
    }


    ngOnChanges(changes: any) {
        if (changes['accountId']) {
            this.filter.AccountId = this.accountId;
            this.filter.Page = 0;
            this.getGroups();
        }
    }

    show(group: ActionGroup) {
        this._action.selectedGroup = group;
    }

    delete(id: string) {
        console.log("Deleting! (TODO)");
        if (confirm(this.translate.instant("WARNDELACTION"))) {
            this._action.deleteGroup(id).subscribe(res => {
                if (res.IsSuccess) {
                    this.groups.splice(this.groups.findIndex(x => x.Id == id), 1);
                    this.notify.success(this.translate.instant("ACTIONDELETED"));
                }
                else {
                    this.notify.success(this.translate.instant("ERROR"));
                }
            });
        }
    }

    getGroups() {
        console.log("[ActionGroup-Block] Loading groups");
        this._action.getGroups(this.filter).subscribe(res => {
            res.Data = res.Data.map((x: any) => new ActionGroup(x));
            this.groups = res.Data;
            console.log("[ActionGroup-Block] Load finished", res);
        });
    }

    ngOnDestroy() {
        if (this.groupChangeSub) this.groupChangeSub.unsubscribe();
        if (this.actionChangeSub) this.actionChangeSub.unsubscribe();
    }
}