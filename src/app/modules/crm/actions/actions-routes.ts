import { Routes } from '@angular/router';

// Components
import { ActionsComponent } from './actions.component';
import { VoidComponent } from '../../shared/components/void';

export const ActionsRoutes: Routes = [
    {
        path: 'Actions',
        component: ActionsComponent,
        children: [
            { path: '', component: VoidComponent },
        ]
    }
];