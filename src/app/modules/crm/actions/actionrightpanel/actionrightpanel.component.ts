import { Component, Input } from '@angular/core';
import { AccountsCollection } from '../../../../services/accounts.service';
import { NewsfeedModel } from '../../../../models/newsfeed.models';
import { ActionsflowCollection } from '../../../../services/Actionsflow.service';
import { NewsfeedService } from '../../../../services/newfeed.service';
import { OrgService } from "../../../../services/organisation.service";
import { AuthService } from "../../../../services/auth-service";
import { environment } from '../../../../../environments/environment';

@Component({
	selector: 'actionright',
	templateUrl: './actionrightpanel.html',
	styles: [`
		.closeButton {
			position: absolute;
			top: 25px;
			left: 10px;
		}

		.title {
			text-align: center;
		}
	`]
})

export class ActionRightPanelComponent {
	@Input() display: any;
	@Input() account: any;
	@Input() closeFunc: () => any;

	public showEditAccount: boolean = false;
	public NewsfeedsSearch: NewsfeedModel;
	public canEdit: boolean;
	public customerSuccess: boolean;
	public tab = "";
	constructor(public _accountCollection: AccountsCollection, public _actionFlow: ActionsflowCollection, private news: NewsfeedService, private orgSvc: OrgService, public auth:AuthService) {

	}

	ngOnInit() {
		this.canEdit = this.orgSvc.hasPermission("EditOrganisationNotes");
		this.customerSuccess = this.orgSvc.currentOrganisation().Id == environment.customerSuccessOrg;
	}
	updateleftPannel() {
		this._actionFlow.getActions();
	}

	newsScroll() {
		this.news.makeSroll();
	}
}



