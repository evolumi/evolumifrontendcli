import { NgModule } from '@angular/core';

import { SharedModule } from '../../../shared/shared.module';

import { CheckboxComponent } from '../actionblock/checkbox.component';
import { ActionGroupBlockComponent } from '../actionblock/action-group-block.component';
import { ActionBlockByAccountComponent } from '../actionblock/action-block.component';
import { ActionRightPanelComponent } from "../actionrightpanel/actionrightpanel.component";
import { AccountSharedModule } from "../../accounts/shared/account-shared.module";

@NgModule({
    imports: [
        SharedModule,
        AccountSharedModule
    ],
    declarations: [
        ActionBlockByAccountComponent,
        CheckboxComponent,
        ActionGroupBlockComponent,
        ActionRightPanelComponent
    ],
    exports: [
        ActionBlockByAccountComponent,
        ActionGroupBlockComponent,
        ActionRightPanelComponent,
    ]
})
export class ActionsSharedModule { }