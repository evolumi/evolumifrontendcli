import { Component } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { ActionsModel } from '../../../models/actions.models';
import { AccountsCollection } from '../../../services/accounts.service';
import { ActionslistCollection } from '../../../services/ActionList.service';
import { ActionsCollection } from '../../../services/actionservice';
import { ActionsflowCollection } from '../../../services/Actionsflow.service';
import { NotificationService } from '../../../services/notification-service';
import { DealListing } from '../../../services/deallisting.service';
import { ChosenOption } from "../../shared/components/chosen/chosen";
import { ChosenOptionModel } from '../../shared/components/chosen/chosen.models';
import { IntercomService } from '../../../services/interecomService';
import { AccountRelation } from "../../../models/account.models";
import { Subscription } from "rxjs/Subscription";

import { OrgService } from '../../../services/organisation.service';
import { UsersService } from '../../../services/users.service';
import { ModalService } from '../../../services/modal.service';
import { OrganisationModel } from "../../../models/organisation.models";

@Component({
    selector: 'action-modal',
    templateUrl: './action-modal.html',
    styleUrls: ['actions.css']
})
export class ActionModalComponent {

    public showing: boolean = false;
    public simpleMode: boolean = true;
    public multiMode: boolean = false;
    public multiSource: string = "";
    public syncAll: boolean; //Sync all created actions to calendar, not recommended

    public now: Date = new Date();
    public startOfDay = new Date(this.now.getFullYear(), this.now.getMonth(), this.now.getDate()).getTime();
    public action: ActionsModel;
    public edittype: boolean = true;
    public datetime = new Date().getTime();
    public startHour: number = 0;
    public endHour: number = 0;
    public actionForm: FormGroup;
    public account$: any;
    public accounts: Array<ChosenOption> = [];
    public actionStatus: Array<ChosenOption> = [];
    public contacts: Array<AccountRelation> = [];
    public priority: Array<ChosenOption> = [];
    public users: Array<ChosenOption> = [];
    public deals: Array<ChosenOption> = [];
    public actionTypes: Array<ChosenOption> = [];
    public Typeid: string;
    public labels: Array<string> = [];
    public process = false;

    public dateNames: Array<ChosenOption> = [];

    //all subscribers
    private orgSub: Subscription;
    private userSub: Subscription;
    private modalSub: Subscription;

    //actionid
    public actionId: string;
    public accountId: string = '';

    public startDate: number;
    public endDate: number;

    private contacObservable: Subscription;

    private currentOrg: OrganisationModel;

    constructor(
        public _notify: NotificationService,
        public translate: TranslateService,

        // ToDo !Merge these when time...
        private actionFlow: ActionsflowCollection,
        private actionList: ActionslistCollection,
        private actionService: ActionsCollection,

        public dealService: DealListing,
        public accountService: AccountsCollection,
        private orgService: OrgService,
        private userService: UsersService,
        private modal: ModalService,
        private intercom: IntercomService
    ) { }

    ngOnInit() {

        this.action = new ActionsModel({ Status: 'Open', Priority: 'Normal', Type: 'Meeting', Comments: '', RelatedPersons: [], RelatedDealId: '', EndDate: this.startOfDay, StartDate: this.startOfDay, Labels: [], Location: '' });

        this.userSub = this.userService.activeUserListObervable().subscribe(users => {
            if (users) {
                this.users = users;
            }
        });

        this.modalSub = this.modal.actionTrigger().subscribe((res: any) => {
            if (res) {
                if (res.actionId) {
                    this.showModalFromId(res.actionId);
                }
                else {
                    this.showModal(res.action, res.accountId);
                }
                this.simpleMode = res.simpleMode;
                this.multiMode = res.multiMode;
                this.multiSource = res.source;
                this.process = false;
            }
        });

        this.actionStatus = this.actionService.getActionStatuses();
        this.actionTypes = this.actionService.getActionTypes();

        this.contacObservable = this.accountService.contactAccountsObservable().subscribe((data) => {
            if (data) {
                console.log("[Action-Modal] Found contacts", data);
                this.contacts = data;
                this.updateNotAddedContacts();
            }
        });

        this.dateNames = [
            new ChosenOptionModel("StartDate", this.translate.instant("STARTDATE")),
            new ChosenOptionModel("EndDate", this.translate.instant("ENDDATE"))
        ];

        this.orgSub = this.orgService.currentOrganisationObservable()
            .subscribe(org => {
                if (org) {
                    this.currentOrg = org;
                }
            })
    }

    public selectCompany(accountId: string) {
        this.action.AccountIsPerson = !accountId;
        this.setAccount(accountId);
    }

    public selectPerson(accountId: string) {
        this.action.AccountIsPerson = true;
        this.setAccount(accountId);
    }

    private setAccount(accountId: string) {
        this.action.AccountId = accountId;
        this.getContacts(accountId);
        this.getDeals(accountId);
    }

    private notAddedContacts: AccountRelation[] = [];
    updateNotAddedContacts() {
        this.notAddedContacts = this.contacts.filter(contact =>
            this.action.RelatedPersons.indexOf(contact.PersonId) == -1
        );
        this.notAddedContacts = this.notAddedContacts.slice();
    }

    addContact(id: string) {
        this.action.RelatedPersons.push(id);
        this.updateNotAddedContacts();
    }
    removeContact(id: string) {
        this.action.RelatedPersons.splice(this.action.RelatedPersons.indexOf(id), 1);
        this.updateNotAddedContacts();
    }

    getPersonName(id: string): string {
        let contact = this.contacts.find(x => x.PersonId == id);
        if (contact) {
            return contact.FirstName + " " + (contact.LastName ? contact.LastName : "");
        }
        else return "";
    }

    public showModalFromId(actionId: string) {
        console.log("[Action-Modal] Getting action from Id");
        this.actionService.getActionById(actionId).subscribe(res => {
            let action = res.Data
            if (action) {
                console.log("[Action-Modal] Found Action", action);
                this.action = new ActionsModel(action);
                this.startDate = this.action.StartDate;
                this.endDate = this.action.EndDate;
                this.getContacts(this.action.AccountId);
                this.getDeals(this.action.AccountId);
                this.showing = true;
                if (!action.accountId) this.action.AccountIsPerson = true;
            }
        });
    }

    public showModal(action: ActionsModel, accountId: string = null) {
        if (action) {
            this.action = new ActionsModel(action);
            this.startDate = this.action.StartDate;
            this.endDate = this.action.EndDate;
        }
        else {
            let end = new Date(this.now.getFullYear(), this.now.getMonth(), this.now.getDate(), this.now.getHours() + 2);
            let start = new Date(this.now.getFullYear(), this.now.getMonth(), this.now.getDate(), this.now.getHours() + 1);
            this.action = new ActionsModel({ Status: 'Open', Priority: 'Normal', Type: 'Meeting', Comments: '', RelatedPersons: [], RelatedDealId: '', EndDate: end.getTime(), StartDate: start.getTime(), Labels: [], Location: '' });
            this.endDate = end.getTime();
            this.startDate = start.getTime();
            console.log("datetest 2: ", end, start);
            if (accountId)
                this.action.AccountId = accountId;
            var userid = localStorage.getItem("userId");
            console.log("[Action-Modal] Setting Assigned user [" + userid + "]");
            this.action.UserAssignedId = userid;
        }

        if (!this.action.AccountId && this.accounts.length) {
            this.action.AccountId = this.accounts[0].value.toString();
        }

        if (this.action.AccountId) {
            this.getContacts(this.action.AccountId);
            this.getDeals(this.action.AccountId);
        }
        else {
            this.action.AccountIsPerson = true;
        }

        this.showing = true;
    }



    getContacts(accountId: string) {
        if (this.showing) {
            this.action.RelatedPersons = [];
        }
        if (accountId) {
            this.accountService.getContactsForAccount(accountId, false);
        }
    }

    getDeals(accountId: string) {
        if (accountId) {
            this.dealService.getDealsForAccountId(accountId).subscribe(deals => {
                this.deals = [new ChosenOptionModel("", this.translate.instant("SELECTDEAL"))];
                for (var deal of deals) {
                    this.deals.push(new ChosenOptionModel(deal.Id, deal.Name));
                }
            });
        }
    }

    clickToClose(event: any) {
        var target = event.target || event.srcElement;
        if (target.id === 'editAction') {
            this.close();
        }
    }

    close(saved: boolean = false) {
        if (saved) {
            this.actionService.triggerActionUpdate(this.action.Id);
        }
        this.action = new ActionsModel({});
        this.showing = false;
    }

    editAction() {
        if (this.process === false) {
            this.process = true;
            if (this.action['EndDate'] < this.action['StartDate']) {
                alert(this.translate.instant("WARNENDDATELARGER"));
                this.process = false;
                return false;
            }
            else {
                this.actionService.updateAction(this.action)
                    .subscribe(res => {
                        if (res.IsSuccess == true) {
                            this._notify.success(this.translate.instant("ACTIONUPDATED"));
                            if (this.accountId !== '') {
                                this.actionService.getActionByAccountId(this.accountId);
                            } else {
                                this.actionService.getActionByAccountId(this.action.AccountId);
                                this.setScrollHeights();
                                this.actionFlow.getActions();
                                this.actionList.getActions();
                            }
                            this.process = false;
                            this.close(true);
                        }
                    })
            }
        }
        this.orgService.labelsChange(this.labels);
        return null;
    }

    addAction() {
        if (this.process == false) {
            this.process = true;
            if (this.action['EndDate'] < this.action['StartDate']) {
                alert(this.translate.instant("WARNENDDATELARGER"));
                this.process = false;
                return false;
            } else {
                if (this.multiMode) {
                    let data =
                        {
                            'ActionFilter': this.multiSource == "action" && this.actionList.selectAll ? this.actionList.searchParams : "",
                            'AccountFilter': this.multiSource == "account" && this.accountService.selectAll ? this.accountService.searchParams : "",
                            'ActionIds': this.multiSource == "action" && !this.actionList.selectAll ? this.actionList.selectedActions : [],
                            'AccountIds': this.multiSource == "account" && !this.accountService.selectAll ? this.accountService.selectedAccounts : [],
                            'Name': this.action.Name,
                            'StartDate': this.action.StartDate,
                            'EndDate': this.action.EndDate,
                            'UserAssignedId': this.action.UserAssignedId,
                            'Type': this.action.Type,
                            'Priority': this.action.Priority,
                            'Status': this.action.Status,
                            'Location': this.action.Location,
                            'Comments': this.action.Comments,
                            'Labels': this.action.Labels,
                            'SyncAll': this.syncAll
                        };

                    this.actionService.createManyAction(data)
                        .subscribe((res: any) => {
                            this.accountService.selectedOption = 'selected';
                            if (res.IsSuccess == true) {
                                this.intercom.trackEvent('action-created');
                                this._notify.success("Action Created");
                                this.process = false;
                                this.close(true);
                                this.actionFlow.getActions();
                                this.actionList.getActions();
                            } else {
                                this._notify.error(res.Message);
                            }
                        });
                } else {
                    console.log(this.action);
                    this.actionService.createAction(this.action)
                        .subscribe(res => {
                            if (res.IsSuccess == true) {
                                this.intercom.trackEvent('action-created');
                                this._notify.success("Action Created");
                                if (this.accountId !== '') {
                                    this.actionService.getActionByAccountId(this.accountId);
                                } else {
                                    this.actionService.getActionByAccountId(this.action.AccountId);
                                    this.actionFlow.getActions();
                                    this.actionList.getActions();
                                }
                                this.process = false;
                                this.close(true);
                            } else {
                                this._notify.error(res.Message);
                            }
                        });
                }
            }
            this.orgService.labelsChange(this.labels);
        }
        return null;
    }

    setScrollHeights() {
        this.actionFlow.searchParams.PageSize = this.actionFlow.searchParams.PageSize * this.actionFlow.searchParams.Page;
        this.actionFlow.searchParams.Page = 1;
        this.actionList.searchParams.PageSize = this.actionList.searchParams.PageSize * this.actionList.searchParams.Page;
        this.actionList.searchParams.Page = 1;
    }

    onitemsChange(event: any) {
        this.labels = event;
    }

    ngOnDestroy() {
        if (this.userSub) this.userSub.unsubscribe();
        if (this.orgSub) this.orgSub.unsubscribe();
        if (this.modalSub) this.modalSub.unsubscribe();
        if (this.contacObservable) this.contacObservable.unsubscribe();
    }
}
