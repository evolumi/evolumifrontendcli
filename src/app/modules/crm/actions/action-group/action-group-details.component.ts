import { Component, Output, Input, EventEmitter, SimpleChange } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ActionsModel } from '../../../../models/actions.models';
import { AccountsCollection } from '../../../../services/accounts.service';
import { ActionsCollection, ActionGroup, ActionTypes, ActionStatus, ActionType } from '../../../../services/actionservice';
import { NotificationService } from '../../../../services/notification-service';
import { DealListing } from '../../../../services/deallisting.service';
import { ChosenOptionModel } from '../../../shared/components/chosen/chosen.models';
import { Subscription } from "rxjs/Subscription";
import { OrgService } from '../../../../services/organisation.service';
import { UsersService } from '../../../../services/users.service';
import { ConfirmationService } from "../../../../services/confirmation.service";

@Component({
    selector: 'action-group',
    templateUrl: './action-group-details.component.html',
    styleUrls: ['./action-group.css', '../actions.css']
})
export class ActionGroupComponent {

    @Input() group: ActionGroup;
    @Input() edit: ActionGroup;
    @Output() onSaved: EventEmitter<ActionGroup> = new EventEmitter();

    public editAction: ActionsModel;

    private deals: Array<ChosenOptionModel> = [];
    private contacts: Array<ChosenOptionModel> = [];
    private users: Array<ChosenOptionModel> = [];
    private labels: Array<string> = [];
    private statuses: Array<ActionStatus> = [];
    private types: Array<ActionType> = [];
    private dateNames: Array<ChosenOptionModel> = [];

    private selectedType: ActionType;

    private openActions: Array<ActionsModel> = [];
    private closedActions: Array<ActionsModel> = [];

    private contactSub: Subscription;
    private userSub: Subscription;

    private now: Date = new Date();
    public startOfDay = new Date(this.now.getFullYear(), this.now.getMonth(), this.now.getDate(), 8).getTime();
    private editAccountIsPerson: boolean;

    public showTypes: boolean = false;
    private actionTemplate: ActionsModel;

    constructor(
        public _notify: NotificationService,
        public translate: TranslateService,
        public actionService: ActionsCollection,
        public dealService: DealListing,
        public accountService: AccountsCollection,
        private orgService: OrgService,
        private userService: UsersService,
        private confirmSvc: ConfirmationService
    ) {

        this.types = actionService.getActionTypesAndStatuses();

        this.contactSub = this.accountService.contactAccountsObservable().subscribe(relations => {
            this.contacts = [];
            console.log("[Group-Details] Loaded Contacts!", relations);
            for (let relation of relations) {
                this.contacts.push(new ChosenOptionModel(relation.PersonId, relation.FirstName + " " + relation.LastName));
            }
        })

        this.userSub = this.userService.activeUserListObervable().subscribe(users => {
            if (users) {
                this.users = users;
            }
        });

        this.labels = this.orgService.currentOrganisation().Labels;
        this.dateNames = [
            new ChosenOptionModel("StartDate", this.translate.instant("STARTDATE")),
            new ChosenOptionModel("EndDate", this.translate.instant("ENDDATE"))
        ];
    }

    ngOnChanges(change: SimpleChange) {

        this.sortActions();

        if (!this.group.Id) {
            if (!this.edit) {
                this.edit = new ActionGroup();
                Object.assign(this.edit, this.group);
            }
        }

        if (this.group.AccountId) {
            this.selectAccount(this.group.AccountId);
        }
    }

    private sortActions() {
        for (let i = 0; i < this.group.Actions.length; i++) {
            let a = this.group.Actions[i];
            if (!a.Type) {

                this.actionTemplate = a;
                this.group.Actions.splice(i, 1);
                // this.startDate = a.StartDate;
                // this.endDate = a.EndDate;
                continue;
            }
            a.icon = this.getIcon(a.Type);
            if (!a.Id || a.Id == this.group.editActionId) {
                this.group.editActionId = null;
                this.selectedType = ActionTypes.find(x => x.value == a.Type);
                this.getStatuses(this.selectedType);
                // this.startDate = a.StartDate;
                // this.endDate = a.EndDate;
                this.editAction = a;
                this.editAction.StartDate = a.StartDate;
                this.editAction.EndDate = a.EndDate;
                if (!a.Id) {
                    this.group.Actions.splice(this.group.Actions.indexOf(a), 1);
                }
            }
        }
        this.group.Actions.sort(function (a, b) {
            return b.StartDate - a.StartDate;
        });

        this.openActions = [];
        this.closedActions = [];

        this.openActions.push(...this.group.Actions.filter(x => x.Status == "Open").reverse());
        this.closedActions.push(...this.group.Actions.filter(x => x.Status != "Open"));
    }

    clearType() {
        this.selectedType = undefined;
        this.statuses = [];
        this.editAction.Type = "";
        this.showTypes = true;
    }

    setType(type: ActionType) {
        this.selectedType = type;

        if(this.actionTemplate){
            this.actionTemplate.Type = type.value;
            this.editAction = this.actionTemplate;
            this.actionTemplate = null;
            return;
        }

        if (this.editAction) {
            this.editAction.Type = type.value;
            this.editAction.Status = "Open";
        }
        else {
            let user = this.users.find(x => x.value == localStorage.getItem('userId')) || this.users[0];
            this.editAction = new ActionsModel({
                OrganisationId: this.group.Organisation,
                AccountId: this.group.AccountId,
                ActionGroupId: this.group.Id,
                DealId: this.group.DealId,
                Name: this.group.Name,
                StartDate: this.startOfDay,
                EndDate: this.startOfDay,
                Type: type.value,
                RelatedPersons: [],
                Labels: [],
                Reminder: null,
                UserAssignedId: user.value,
                UserAssignedName: user.label,
                Status: type.statuses[0].value
            });
        }
        this.getStatuses(type);
        this.showTypes = false;
    }

    private getStatuses(type: ActionType) {
        this.statuses = type.statuses;
    }

    setAssigned(user: ChosenOptionModel) {
        this.editAction.UserAssignedId = user.value.toString();
        this.editAction.UserAssignedName = user.label.toString();
    }

    selectAction(action: ActionsModel) {
        this.selectedType = ActionTypes.find(x => x.value == action.Type);
        this.getStatuses(this.selectedType);
        this.editAction = JSON.parse(JSON.stringify(action));
    }

    selectAccount(id: string) {
        if (this.edit) {
            this.edit.AccountId = id;
        }
        else {
            this.group.AccountId = id;
        }

        if (id) {
            console.log("[Group-Details] Loading stuff for Account:", id);
            this.dealService.getDealsForAccountId(id).subscribe(deals => {
                this.deals = [];
                for (let deal of deals) {
                    this.deals.push(new ChosenOptionModel(deal.Id, deal.Name));
                }
            });
            this.accountService.getContactsForAccount(id, false);
            this.accountService.accountIsPerson(id, (isPerson) => {
                this.editAccountIsPerson = isPerson;
            });
        }
        else {
            this.deals = [];
            this.contacts = [];
        }
    }

    save() {
        // Multi Action Create.
        if (this.group.multi) {
            console.log("[ActionGroupModal] Multicreate: Started");
            let data: any = this.editAction;
            data.Name = this.edit.Name;
            data.ActionFilter = this.group.actionQuery;
            data.AccountFilter = this.group.accountQuery;
            data.ActionIds = this.group.actionIds;
            data.AccountIds = this.group.accountIds;
            
            if(this.group.actionQuery || this.group.actionIds){
                data.Name = this.translate.instant("FOLLOWUP");
            }

            this.actionService.createManyAction(data).subscribe(res => {
                if (res.IsSuccess) {
                    this.editAction = null;
                    this._notify.success(this.translate.instant("ACTIONCREATED"));
                    this.cancle();
                }
            });
            return;
        }

        // New Group
        if (this.edit && !this.edit.Id && this.editAction) {
            console.log("[ActionGroupModal] New Group: Started");
            this.editAction.Name = this.edit.Name;
            this.editAction.AccountId = this.edit.AccountId;
            this.editAction.RelatedDealId = this.edit.DealId;
            this.actionService.createAction(this.editAction).subscribe(res => {
                if (res.IsSuccess) {
                    this.edit = null;
                    this.editAction = null;
                }
                else {
                    this._notify.error(this.translate.instant("ERROR"));
                }
            })
        }
        else {
            // Edit Group
            if (this.edit && this.edit.Id) {
                console.log("[ActionGroupModal] Edit Group: Started");
                this.actionService.updateGroup(this.edit).subscribe(res => {
                    if (res.IsSuccess) {
                        this.edit = null;
                        this.editAction = null;
                    }
                })
            }

            // Actions
            if (this.editAction) {
                this.editAction.Name = this.group.Name;
                this.editAction.AccountId = this.group.AccountId;
                this.editAction.RelatedDealId = this.group.DealId;
                if (!this.editAction.Id) {
                    console.log("[ActionGroupModal] New Action: Started");
                    this.actionService.createAction(this.editAction).subscribe(res => {
                        if (res.IsSuccess) {
                            this.editAction = null;
                            this.updateActions();
                        }
                    })
                }
                else {
                    console.log("[ActionGroupModal] Edit Action: Started");
                    this.actionService.updateAction(this.editAction).subscribe(res => {
                        if (res.IsSuccess) {
                            let index = this.group.Actions.findIndex(x => x.Id == this.editAction.Id);
                            this.group.Actions.splice(index, 1, res.Data);
                            this.editAction = null;
                            this.updateActions()
                        }
                    })
                }
            }
        }
    }

    private updateActions() {
        this.sortActions();
        if (this.group.Open && !this.openActions.length) {
            this.group.Open = false;
            this.actionService.updateGroupLocaly(this.group);
        }
        else if (!this.group.Open && this.closedActions.length) {
            this.group.Open = true;
            this.actionService.updateGroupLocaly(this.group);
        }
    }

    cancle() {
        if (this.editAction)
            this.editAction = null;
        else if (this.edit) {
            this.onSaved.emit(null);
        }
    }

    onLabelsChange(event: any) {
        this.editAction.Labels = event;
    }

    newReminder() {
        this.editAction.Reminder = { TimeSpan: 900000, DateFieldName: this.dateNames[0].value };
    }

    getIcon(type: string) {
        return ActionTypes.find(x => x.value == type).icon;
    }

    deleteAction() {
        this.confirmSvc.showModal(this.translate.instant("CONFIRMDELETEACTIONTITLE"), this.translate.instant("CONFIRMDELETEACTIONTEXT"), () => {
            if (this.group.Actions.length < 2) {
                this.actionService.deleteGroup(this.group.Id).subscribe(res => {
                    if (res.IsSuccess) {
                        this.actionService.selectedGroup = null;
                        this.actionService.actionsChange();
                        this.actionService.actionGroupChanged();
                    }
                });
            }
            else {
                this.actionService.deleteAction(this.editAction.Id, (success: boolean) => {
                    if (success) {
                        this.actionService.actionsChange(this.editAction);
                        this.group.Actions.splice(this.group.Actions.findIndex(x => x.Id == this.editAction.Id), 1);
                        this.editAction = new ActionsModel({});
                    }
                });
            }
        });
    }

    ngOnDestroy() {
        if (this.contactSub) this.contactSub.unsubscribe();
        if (this.userSub) this.userSub.unsubscribe();
    }
}
