import { Component } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { ActionsCollection, ActionGroup } from '../../../../services/actionservice';

@Component({

    selector: 'action-group-modal',
    templateUrl: './action-group-modal.html',
    styleUrls: ['./action-group.css']
})
export class ActionGroupModalComponent {

    private edit: ActionGroup;
    private groupSub: Subscription;

    constructor(public service: ActionsCollection) {
        this.groupSub = this.service.actionGroupChangeObservable().subscribe(res => {
            if ((this.service.selectedGroup && !this.service.selectedGroup.Id) || this.service.selectedGroup && res.Id == this.service.selectedGroup.Id) {
                var group = new ActionGroup();
                Object.assign(group, res);
                this.service.selectedGroup = group;
            }
        });
    }

    ngOnDestroy(){
        if (this.groupSub) this.groupSub.unsubscribe();
    }

    clickToClose(event: any) {
        var target = event.target || event.srcElement;
        if (target.id === 'editActionGroup') {
            this.close();
        }
    }

    editGroup() {
        console.log(this.service.selectedGroup);
        this.edit = new ActionGroup();
        Object.assign(this.edit, this.service.selectedGroup);
    }

    onSaved(group: ActionGroup) {

        if (group == null) {
            if (this.edit && this.edit.Id) {
                this.edit = null;
            } else {
                this.close();
            }
        }
        else {
            this.service.selectedGroup = group;
            this.edit = null;
        }
    }

    close() {
        this.service.selectedGroup = null;
        this.edit = null;
    }
}