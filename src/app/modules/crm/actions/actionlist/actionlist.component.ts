import { TranslateService } from '@ngx-translate/core';
import { Component, Input, EventEmitter, Output } from '@angular/core';
import { ActionslistCollection } from '../../../../services/ActionList.service';
import { ActionsCollection, ActionGroup, ActionType, ActionStatus } from '../../../../services/actionservice';
import { ChosenOption } from "../../../shared/components/chosen/chosen";
import { ChosenOptionModel } from '../../../shared/components/chosen/chosen.models';
import { UsersService } from '../../../../services/users.service';
import { OrgService } from '../../../../services/organisation.service';
import { Subscription } from 'rxjs/Subscription';
import { ActionsModel } from "../../../../models/actions.models";
import { AccountsModel } from "../../../../models/account.models";

@Component({
	selector: 'actionlist',
	templateUrl: './actionlist.html',
	styleUrls: ['../actions.css']
})

export class ActionListComponent {
	@Input() action: ActionsModel;
	public columns: Array<any> = [];
	private usersSubscription: any;
	public actionTypes: Array<ActionType> = [];
	public accountTypes: Array<any> = [];
	//private prioritySubscription: any;
	public priority: Array<ChosenOption> = [];
	public users: Array<ChosenOption> = [];
	public actionpriority: string = "";
	public accountowner: Array<string> = [];
	public actiontype: Array<string> = ['Email', 'Phone', 'Meeting', 'Events', 'Other'];
	public actionstatus: Array<string> = [];
	public name: any;
	public actionChangeSub: any;
	public filteredUsers: ChosenOption[] = [];
	public notFilteredUsers: ChosenOption[] = [];
	public chosenLabels: ChosenOption[] = [];
	public labels: string[] = [];
	public labelFilters: ChosenOption[] = [];
	public selectedLabelFilter: string;
	public editDates: boolean;
	public filterDateType: string = "StartDate";
	public filterDateTypes: string[] = ["StartDate", "EndDate", "DateClosed"];
	public startPickerVisible: boolean;
	public endPickerVisible: boolean;
	@Output() openAccountInfo = new EventEmitter<AccountsModel>();

	public filterStartDate: number;
	public filterEndDate: number;
	public sortingField: string = "StartDate";
	public sortingFields: ChosenOptionModel[] = [
		new ChosenOptionModel("StartDate", this.translate.instant("STARTDATE")),
		new ChosenOptionModel("EndDate", this.translate.instant("ENDDATE")),
		new ChosenOptionModel("DateClosed", this.translate.instant("DATECLOSED")),
		new ChosenOptionModel("Name", this.translate.instant("NAME"))
	]

	public actionStatuses: Array<ActionStatus> = [];

	private orgSub: Subscription;
	constructor(public actionList: ActionslistCollection, private userService: UsersService, public _actionservice: ActionsCollection, private translate: TranslateService,
		private orgSvc: OrgService
	) {
		this.columns = [
			{ 'name': 'Account', 'label': 'ACCOUNT', 'class': 'smallHidden' },
			{ 'name': 'UserAssignedName', 'label': 'ASSIGNEDTO', 'class': 'smallHidden' },
			{ 'name': 'Name', 'label': 'NAME', 'Link': true, 'class': 'accountname' },
			{ 'name': 'Status', 'label': 'STATUS', 'class': 'smallHidden' },
			//{ 'name': 'Type', 'label': 'ACTIONTYPE', 'class': 'smallHidden' },
			//{ 'name': 'Priority', 'label': 'PRIORITY', 'class': 'smallHidden' },
			{ name: 'Start', label: 'START', class: 'smallHidden' },
			{ 'name': 'Lists', 'label': 'LISTS', 'class': 'smallHidden' },
			{ 'name': '', 'label': '', 'Act': true, 'class': 'actionTd' }
		];
		this.actionChangeSub = this._actionservice.actionChangeObservable()
			.subscribe(res => {
				this.getactions();
			});
	}

	ngOnInit() {
		this.actionList.reset();
		this.actionList.getActions();
		this.getLabelFilters();
		this.orgSub = this.orgSvc.currentOrganisationObservable().subscribe(res => this.chosenLabels = res.Labels.map(x => new ChosenOptionModel(x, x)));

		// Get Users
		this.usersSubscription = this.userService.userListObervable().subscribe(res => {
			if (res.length > 0) {
				this.users = [];
				this.users.push(...res);
				this.notFilteredUsers = this.users;
				let me = this.notFilteredUsers.findIndex(x => x.value == localStorage.getItem("userId"));
				// for (var index = 0; index < 10; index++){
				// 	let u = this.filteredUsers[0];
				// 	u.value += ("hej" + index);
				// 	this.notFilteredUsers.push(u);
				// }
				if (me != -1) {
					this.addUserToFilter(this.notFilteredUsers[me], me);
				}
			}
		});

		this.actionTypes = this._actionservice.getActionTypes().filter(x => x.active);
		this.actionStatuses = this._actionservice.getActionStatuses();
	}

	showAccountInfo(account: AccountsModel){
		this.openAccountInfo.emit(account);
	}	

	addUserToFilter(user: ChosenOption, notFilteredIndex: number) {
		this.filteredUsers.push(user);
		this.notFilteredUsers.splice(notFilteredIndex, 1);
		this.getactions();
	}

	removeUserFromFilter(user: ChosenOption, filteredIndex: number) {
		this.notFilteredUsers.push(user);
		this.filteredUsers.splice(filteredIndex, 1);
		this.getactions();
	}

	changeLabelFilter(e: ChosenOption, noReload?: boolean) {
		this.selectedLabelFilter = e.label as string;
	}

	hideDatepickers(value: boolean, type: string) {
		if (!this.startPickerVisible && !this.endPickerVisible) {
			this.editDates = false;
		}
	}

	private getLabelFilters() {
		const allLabel = this.translate.instant('ACTIVITYLISTSALL');
		const anyLabel = this.translate.instant('ACTIVITYLISTSANY');
		this.labelFilters = [
			{ label: allLabel, value: 'LabelsAll' },
			{ label: anyLabel, value: 'LabelsAny' }
		]
		this.changeLabelFilter(this.labelFilters[1], true);
	}

	checkAll() {
		if (this.actionList.selectAll) {
			this.actionList.selectedActions = [];
			for (let action of this.actionList.Actionslist)
				action.IsChecked = true;
		}
		else {
			this.actionList.selectedActions = [];
			for (let action of this.actionList.Actionslist) {
				action.IsChecked = false;
			}
		}
	}

	actionIdChange(action: any) {
		if (this.actionList.selectAll) {
			this.checkAll();
			return;
		}
		this.actionList.selectedActions = [];
		for (let a of this.actionList.Actionslist) {
			if (a.IsChecked)
				this.actionList.selectedActions.push(a.Id);
		}
	}

	filterDateTypeChanged(type: string) {
		this.filterDateType = type;
		this.getactions();
	}

	getactions() {
		this.actionList.reset();
		let assignees = this.filteredUsers.reduce((res: any[], curr: ChosenOption) => {
			res.push(curr.value);
			return res;
		}, []);
		let show = { SearchString: this.name, AssignedId: assignees, Types: this.actiontype, Status: this.actionstatus, Priority: this.actionpriority, SortingFieldName: this.sortingField, Labels: this.labels, LabelsAll: this.selectedLabelFilter === "LabelsAll" }
		this.setDateFilter(show);
		this.actionList.UpdateSearchFields(show);
	}

	setDateFilter(filter: any) {
		switch (this.filterDateType) {
			case "StartDate": {
				if (this.filterStartDate) {
					filter.StartDateRangeStart = this.filterStartDate;
				}
				if (this.filterEndDate) {
					filter.StartDateRangeEnd = this.filterEndDate;
				}
				break;
			}
			case "EndDate": {
				if (this.filterStartDate) {
					filter.EndDateRangeStart = this.filterStartDate;
				}
				if (this.filterEndDate) {
					filter.EndDateRangeEnd = this.filterEndDate;
				}
				break;
			}
			case "DateClosed": {
				if (this.filterStartDate) {
					filter.CloseStartDate = this.filterStartDate;
				}
				if (this.filterEndDate) {
					filter.CloseEndDate = this.filterEndDate;
				}
				break;
			}
		}
	}

	showGroup(groupId: string, actionId: string = null) {
		this._actionservice.showGroup(groupId, actionId);
	}

	ngOnDestroy() {
		if (this.usersSubscription) this.usersSubscription.unsubscribe();
		if (this.actionChangeSub) this.actionChangeSub.unsubscribe();
		if (this.orgSub) this.orgSub.unsubscribe();
	}
	delete(accid: any) {
		var conf = confirm(this.translate.instant("WARNDELACTION"));
		if (conf) {
			this._actionservice.deleteAction(accid);
		}
	}

	dateChange(date: string, value: number) {
		if (date === 'startDate') {
			this.filterStartDate = value;
		} else if (date === 'endDate') {
			this.filterEndDate = value;
		}
		this.getactions();
		this.editDates = false;
	}

	addActions() {
		let group = new ActionGroup();
		group.multi = true;
		if (this.actionList.selectAll) {
			group.actionQuery = this.actionList.searchParams;
		}
		else {
			group.actionIds = this.actionList.selectedActions;
		}

		this._actionservice.newGroup(group);
	}

	deleteMany() {
		let conf = confirm(this.translate.instant("WARNDELACTION"));
		if (conf)
			this.actionList.deleteMany();
	}

	toggleFiterbar() {
		$('.filterBox').hasClass('visible') ? $('.filterBox').removeClass('visible') : $('.filterBox').addClass('visible');
	}
}
