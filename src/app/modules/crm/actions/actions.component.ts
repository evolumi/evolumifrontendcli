import { Component } from '@angular/core';
import { ActionsflowCollection } from '../../../services/Actionsflow.service';
import { ActionslistCollection } from '../../../services/ActionList.service';
import { ActionsCollection, ActionGroup } from '../../../services/actionservice';
import { AccountsCollection } from '../../../services/accounts.service';
import { Subscription } from 'rxjs/Subscription';
import { ButtonPanelService } from '../../../services/button-panel.service';
import { OrgService } from "../../../services/organisation.service";
import { AccountsModel } from "../../../models/account.models";

declare var $: JQueryStatic;

@Component({
    templateUrl: './actions.html'
})

export class ActionsComponent {
    public display: boolean = true;
    public showExport: boolean;
    public account: any;
    public activeTab: string = 'ActionFlow';
    private buttonSub: Subscription;
    public closeRightPanel = () => { this.account = null; this.display = false; };
    constructor(
        private _actionFlowSer: ActionsflowCollection,
        private _actionListCollection: ActionslistCollection,
        private _actionService: ActionsCollection,
        private _accService: AccountsCollection,
        private button: ButtonPanelService,
        private orgService: OrgService
    ) {
        this.button.addPanelButton("ADDACTION", "addaction", "plus");
        if (this.orgService.hasPermission("Export"))
            this.button.addPanelButton("EXPORT", "exportactions", "download");

        this.buttonSub = this.button.commandObservable().subscribe(command => {
            if (command == "exportactions") {
                this.showExport = true; // for new Export
                // this._actionListCollection.exportActions(); // for old export
            }
            if (command == "addaction") {
                let group = new ActionGroup()
                this._actionService.selectedGroup = group;
            }
        });
    }

    getAccount(account: any) {
        if (typeof (account) === 'string') {
            this._accService.getAccount(account, "", true, acc => {
                this.account = acc;
            });
        }
        else {
            this._accService.getAccount(account.Id);
            this.account = account;
        }

    }

    showAccountInfo(account: AccountsModel) {
        this.account = account;
        this.display = true;
    }

    onTabSelect(tab: string) {
        switch (tab) {
            case 'ActionFlow':
                this.display = true;
                break;
            case 'ActionList':
                this.account = null;
                this.display = false;
                break;
        }
        this.activeTab = tab;
    }

    removeClass() {
        $(".contentPanel.leftFree.actionsPage").removeClass('accountsPage');
    }

    addClass() {
        $(".contentPanel.leftFree.actionsPage").addClass('accountsPage');
    }

    ngOnDestroy() {
        if (this.buttonSub) this.buttonSub.unsubscribe();
        this.button.removeButtons(["addaction", "exportactions"]);
    }
    public scroll() {
        this.display ? this._actionFlowSer.onScroll() : this._actionListCollection.onScroll();
    }
}
