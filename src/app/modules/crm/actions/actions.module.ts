import { NgModule } from '@angular/core';

import { SharedModule } from '../../shared/shared.module';
import { AccountSharedModule } from '../accounts/shared/account-shared.module';
import { ActionsSharedModule } from './shared/action-shared.module';
import { DealsSharedModule } from '../deals/shared/deals-shared.module';

import { ActionGroupModalComponent } from './action-group/action-group-modal';
import { ActionsComponent } from './actions.component';
import { ActionModalComponent } from './action-modal.component';
import { ActionCardComponent } from './actionflow/action-card.component';
import { ActionListComponent } from './actionlist/actionlist.component';
import { ActionFlowComponent } from './actionflow/actionflow.component';
import { ActionGroupComponent } from "./action-group/action-group-details.component";

@NgModule({
    imports: [
        SharedModule,
        AccountSharedModule,
        ActionsSharedModule,
        DealsSharedModule
    ],
    declarations: [
        ActionsComponent,
        ActionModalComponent,
        ActionCardComponent,
        ActionListComponent,
        ActionFlowComponent,
        ActionGroupModalComponent,
        ActionGroupComponent
    ],
    exports: [ActionModalComponent, ActionGroupModalComponent]
})
export class ActionsModule { }