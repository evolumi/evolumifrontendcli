import { Component, Output, EventEmitter } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ActionStatus, ActionStatuses } from '../../../../services/actionservice';
import { ActionsModel } from '../../../../models/actions.models';
import { ActionsflowCollection } from '../../../../services/Actionsflow.service';
import { ChosenOptionModel } from '../../../shared/components/chosen/chosen.models';
import { ChosenOption } from "../../../shared/components/chosen/chosen";
import { UsersService } from '../../../../services/users.service';
import { OrgService } from '../../../../services/organisation.service';

declare var $: JQueryStatic;

@Component({

    selector: 'actionflow',
    templateUrl: './actionflow.html',
    styleUrls: ['../actions.css']
})

export class ActionFlowComponent {
    @Output() out = new EventEmitter();

    public initalActionModel: ActionsModel;
    public users: Array<ChosenOption> = [];

    private userSub: any;
    private orgSub: any;

    public statuses: Array<string> = [];
    public filterStartDate: number;
    public filterEndDate: number;

    public types: Array<string> = ['Email', 'Phone', 'Meeting', 'Events', 'Other'];

    public typestring: string = "";
    public ownerIds: Array<string> = [];

    public labels: Array<string> = [];
    private labelsAll: boolean;
    public labelFilters: Array<ChosenOption> = [];

    public actionChangeSub: any;
    public actionFlowChangeSub: any;
    public chosenStatuses: Array<ActionStatus> = [];
    public chosenLabels: Array<any> = [];

    public selectedLabelFilter: string;
    private loaded: boolean = false;

    private usersLoaded: boolean = false;
    private labelsLoaded: boolean = false;
    private datesLoaded: boolean = false;

    constructor(private userService: UsersService,
        private orgService: OrgService,
        public translate: TranslateService,
        public _actionFlowSer: ActionsflowCollection) {

        this.chosenStatuses = ActionStatuses;
        this.chosenStatuses.forEach(x => x.label = this.translate.instant(x.label));


        this.ownerIds.push(localStorage.getItem("userId"));
        this.statuses.push("Open");
    }

    ngOnInit() {

        this.usersLoaded = false;
        this.labelsLoaded = false;
        this.datesLoaded = false;

        let today = new Date();
        let lastWeek = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 5, 0, 0, 0, 0);

        this.filterStartDate = lastWeek.getTime();
        this.filterEndDate = today.getTime();
        this.datesLoaded = true;

        this.actionFlowChangeSub = this._actionFlowSer.actionFlowChange$
            .subscribe(res => {
                // Only load Account details the first time Action list gets updated.
                if (!this.loaded && this._actionFlowSer.Actionsflow.length > 0) {
                    this.out.next(this._actionFlowSer.Actionsflow[0].Account);
                    this.loaded = true;
                }
            });

        // Get users
        this.userSub = this.userService.userListObervable().subscribe(users => {
            this.users = [new ChosenOptionModel("", this.translate.instant("SHOWALL"))];
            if (users.length) {
                for (var u of users)
                    this.users.push(u);
                this.usersLoaded = true;
                console.log("[Action-Flow] usersLoaded");
                this.getActions();
            }

        })

        // Get Labels
        this.orgSub = this.orgService.currentOrganisationObservable().subscribe(org => {
            this.chosenLabels = [new ChosenOptionModel("", this.translate.instant("SHOWALL"))];
            if (org) {
                for (var a of org.Labels) {
                    this.chosenLabels.push(new ChosenOptionModel(a, a));
                }
                this.labelsLoaded = true;
                console.log("[Action-Flow] LabelsLoaded");
                this.getActions();

            }
        });

        this.getLabelFilters();
    }

    public filterChange(event: any) {
        console.log("[ActionFlow] Filter Change" + event);
        this.getActions();
    }

    showAccountDetails(account: any) {
        this.out.next(account);
    }

    dateChange(date: string, value: number) {
        if (date === 'startDate') {
            this.filterStartDate = value;
        } else {
            this.filterEndDate = value;
        }
        this.getActions();
    }

    updateDateRange(date: any, type: string) {
        if (date != '') {
            if (type === 'startDate') {
                this.filterStartDate = date.getTime();
            } else if (type === 'endDate') {
                this.filterEndDate = date.getTime();
            }
            console.log("[Action-Flow] DateRangeChanged");
            this.getActions();
        }
    }

    getActions() {
        console.log("[Action-Flow] GetActions", this.usersLoaded, this.labelsLoaded, this.datesLoaded);
        if (this.usersLoaded && this.labelsLoaded && this.datesLoaded) {

            this._actionFlowSer.reset();
            var type = this.gettypestring();
            var show = { AssignedId: this.ownerIds, Status: this.statuses, StartDate: this.filterStartDate, EndDate: this.filterEndDate, Types: type, Labels: this.labels, LabelsAll: this.labelsAll };
            this._actionFlowSer.UpdateSearchFields(show);
        }
    }

    gettypestring() {
        this.typestring = "";
        for (let type of this.types) {
            this.typestring = this.typestring + type + ',';
        }
        this.typestring = this.typestring.slice(0, this.typestring.length - 1);
        return this.typestring;

    }

    // Localy update a specific action. Or else reload all actions.
    update(action: ActionsModel) {
        if (action) {
            console.log("[Action-Card] Updating action localy");
            var index = this._actionFlowSer.Actionsflow.findIndex(x => x.Id === action.Id)
            this._actionFlowSer.Actionsflow[index].Comments = action.Comments;
        }
        else {
            console.log("[Action-Card] Updating Actions");
            this.getActions();
        }
    }

    ngOnDestroy() {
        if (this.orgSub) this.orgSub.unsubscribe();
        if (this.userSub) this.userSub.unsubscribe();
        if (this.actionFlowChangeSub) this.actionFlowChangeSub.unsubscribe();
    }

    public changeLabelFilter(event: any, noReload?: boolean) {
        this.selectedLabelFilter = event.label;
        switch (event.value) {
            case 'LabelsAll':
                this.labelsAll = true;
                break;
            case 'LabelsAny':
                this.labelsAll = false;
        }
        if (!noReload) {
            this.getActions();
        }
    }

    private getLabelFilters() {
        const allLabel = this.translate.instant('ACTIVITYLISTSALL');
        const anyLabel = this.translate.instant('ACTIVITYLISTSANY');
        this.labelFilters = [
            { label: allLabel, value: 'LabelsAll' },
            { label: anyLabel, value: 'LabelsAny' }
        ]
        this.changeLabelFilter(this.labelFilters[1], true);
    }

    //mobile view
    toggleFiterbar() {
        $('.filterBox').hasClass('visible') ? $('.filterBox').removeClass('visible') : $('.filterBox').addClass('visible');
    }
}