import { Component, Output, Input, EventEmitter, SimpleChanges } from '@angular/core';
import { FormControl } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { ActionsModel } from '../../../../models/actions.models';
import { ActionsCollection, ActionStatus, ActionTypes, ActionType } from '../../../../services/actionservice';
import { AccountsCollection } from '../../../../services/accounts.service';
import { DealAddEdit } from '../../../../services/dealAddEdit.service';

// import 'rxjs/add/operator/debounceTime';
declare var $: JQueryStatic;

@Component({
    
    selector: 'action-card',
    templateUrl: './action-card.html',
    styleUrls: ['../actions.css']
})

export class ActionCardComponent {
    @Input() action: ActionsModel;
    @Output() account = new EventEmitter();
    @Output() update = new EventEmitter();
    @Output() deal = new EventEmitter();

    public statuses : Array<ActionStatus> = [];
    public type: ActionType;

    public pickToolTip = false;
    public isSaved = false;

    public showMenu: boolean;
    public textArea = new FormControl();
    public sub: any;
    public showSave: boolean;

    constructor(public translate: TranslateService,
        public actionSer: ActionsCollection,
        private accountService: AccountsCollection,
        private dealSvc: DealAddEdit) {

        this.sub = this.textArea.valueChanges.debounceTime(1200).distinctUntilChanged().subscribe(text => {
            this.updateComment(this.action, text);
        });
    }

    syncActionToCalendar(action: ActionsModel){
        this.actionSer.syncActionToCalendar(action);
    }

    ngOnChanges(changes: SimpleChanges) {
        console.log("{[Flow] Changes }", changes);
        this.textArea.setValue(changes['action'].currentValue.Comments);
        this.type = ActionTypes.find(x => x.value == this.action.Type);
        this.statuses = this.type.statuses;
    }

    ngOnInit() {
    }

    showGroup(groupId: string, actionId: string = null){
        this.actionSer.showGroup(groupId, actionId);
    }

    showAccountDetails(account: any) {
        this.account.next(account);
    }

    showContact(personId: string) {
        this.accountService.getRelationFromAccountIds(this.action.AccountId, personId).subscribe(res => {
            if (res.IsSuccess) {
                res.Data.actionId = this.action.Id;
                res.Data.actionGroupId = this.action.ActionGroupId;
                res.Data.currentAccount = this.action.Account;
                this.accountService.showAccountRelationPopup(res.Data);
            }
        })

    }

    updateComment(action: ActionsModel, comment: string) {
        if (action.Comments != comment) {
            action.Comments = comment;
            this.update.next(action);
            this.actionSer.updateComment(action, comment).subscribe(res => {
                this.isSaved = res;
                if (this.isSaved){
                    this.saved();
                }
            });
        }
    }

    ngOnDestroy() {
        if(this.sub) this.sub.unsubscribe();
    }

    public saveAndCreateNew() {

        let action= this.action;
        var newAction = new ActionsModel({});

        newAction.AccountId = action.AccountId;
        newAction.ActionGroupId = action.ActionGroupId;
        newAction.RelatedPersons = action.RelatedPersons;
        newAction.RelatedDealId = action.RelatedDealId;
        newAction.StartDate = Date.now();
        newAction.EndDate = Date.now();
        newAction.Labels = [];
        newAction.History = [];
        newAction.UserAssignedId = action.UserAssignedId;
        newAction.Type = action.Type;
        newAction.Status = "Open";
        newAction.Priority = action.Priority;

        this.actionSer.showGroup(action.ActionGroupId, null, newAction);

        this.save();
    }

    public save() {
        console.log("[Action-flow] Setting new status[" + this.action.Status + "] on action. [" + this.action.Id + "]");
        this.setStatus();
    } 

    public delete(id: string) {
        var conf = confirm(this.translate.instant("WARNDELACTION"));
        if (conf) {
            this.actionSer.deleteAction(id);
        }
    }

    private setStatus() {
        this.actionSer.updateStatusForFlow(this.action.Id, this.action.Status).subscribe(res => {
            if (res.IsSuccess) {
                console.log("[Action-flow] Update status [" + status + "] on action [" + this.action.Id + "] Success!");
                this.update.next(false);
            }
            else {
                console.log("[Action-flow] Update status [" + status + "] on action [" + this.action.Id + "] Failed!");
            }
        });
    }

    openDealModal(deal: any) {
        this.dealSvc.showDeal(deal);
    }

    saved() {
        setTimeout(function () {
            this.isSaved = false;
        }.bind(this), 2000);
    }

    // //mobile view
    toggleFiterbar() {
        $('.filterBox').hasClass('visible') ? $('.filterBox').removeClass('visible') : $('.filterBox').addClass('visible');
    }
}