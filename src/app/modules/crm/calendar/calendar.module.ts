import { NgModule } from '@angular/core';
import { CalendarComponent } from './calendar.component';
import { SharedModule } from '../../shared/shared.module';
@NgModule({
    imports: [
        SharedModule
    ],
    exports: [
        CalendarComponent
    ],
    declarations: [
        CalendarComponent
    ]
})

export class CalendarModule { }