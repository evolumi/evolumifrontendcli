import { Component } from '@angular/core';
import { CalendarService } from '../../../services/calendar.service';
import { ActionsCollection, ActionGroup } from '../../../services/actionservice';

import { Subscription } from 'rxjs/Subscription';
import { CalendarDay } from '../../../models/calendar.models';
import { ActionsModel } from '../../../models/actions.models';


@Component({
    templateUrl: './calendar.html',
    styleUrls: ['./calendar.css']
})

export class CalendarComponent {

    public weekDays = [
        { Num: 0, Name: "MONDAY" },
        { Num: 1, Name: "TUESDAY" },
        { Num: 2, Name: "WEDNESDAY" },
        { Num: 3, Name: "THURSDAY" },
        { Num: 4, Name: "FRIDAY" },
        { Num: 5, Name: "SATURDAY" },
        { Num: 6, Name: "SUNDAY" }
    ]

    private months = ["JANUARY", "FEBRUARY", "MARCH", "APRIL", "MAY", "JUNE", "JULY", "AUGUST", "SEPTEMBER", "OCTOBER", "NOVEMBER", "DECEMBER"]
    private days: CalendarDay[][] = [];
    private weekDay: number;
    private todaysDate: number;
    public lastMonth: string;
    public thisMonth: string;
    public nextMonth: string;
    private showToolTip: boolean;
    private displayDay: CalendarDay;

    private actionSub: Subscription;

    public year: number;
    private month: number;

    constructor(
        private calendarSvc: CalendarService,
        private actionSvc: ActionsCollection,
    ) { }

    ngOnInit() {
        let now = new Date();
        this.year = now.getUTCFullYear();
        this.month = now.getUTCMonth();
        this.generateCalendar();
        this.actionSub = this.actionSvc.actionChangeObservable().subscribe(data => {
            this.getMonth(0);
        });
        //this.cookieTest();
    }

    ngOnDestroy() {
        if (this.actionSub) this.actionSub.unsubscribe();
    }

    //     <script charset="UTF-8">
    //     const cookieName = "nav";
    //         function setCookie(cvalue, exdays) {
    //     let d = new Date();
    //     d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    //     let expires = "expires=" + d.toUTCString();
    //     document.cookie = cookieName + "=" + cvalue + ";" + expires + ";path=/";
    // }

    // function getCookie() {
    //     let name = cookieName + "=";
    //     let ca = document.cookie.split(";");
    //     for (let i = 0; i < ca.length; i++) {
    //         let c = ca[i];
    //         while (c.charAt(0) == " ") {
    //             c = c.substring(1);
    //         }
    //         if (c.indexOf(name) == 0) {
    //             return c.substring(name.length, c.length);
    //         }
    //     }
    //     return "";
    // }

    // let obj = getCookie();
    // if (!obj) {
    // let queryRef = decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent("ref").replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
    //     obj = {
    //         LastUrl: document.referrer,
    //         FirstUrl: window.location.href,
    // ReferCode: [],
    //         Navigations: []
    //     };
    // if(queryRef){
    //     obj.ReferCode.push(queryRef);
    // }
    //     let json = JSON.stringify(obj);
    //     setCookie(json, 30)
    // }
    // else {
    //     let newObj = JSON.parse(obj);
    //     newObj.Navigations.push(window.location.href);
    //     let queryRef = decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent("ref").replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
    //      if(queryRef){
    //      newObj.ReferCode.push(queryRef);
    //      }
    //     let json = JSON.stringify(newObj);
    //     setCookie(json, 30);
    // }
    // </script>
    //{custom-field-pmm2}{custom-field-pYEm}{custom-field-snmy}
    getMonth(modifier: number) {
        let newMonth = this.month + modifier;
        if (newMonth == -1) {
            this.year--;
            newMonth = 11;
        }
        else if (newMonth == 12) {
            this.year++;
            newMonth = 0;
        }
        this.month = newMonth;
        this.generateCalendar();
    }

    createAction(day: any) {
        let now = new Date();
        let startDate = new Date(this.year, this.month, day, day == now.getDate() ? now.getHours() + 1 : 8).getTime();
        let endDate = new Date(this.year, this.month, day, day == now.getDate() ? now.getHours() + 2 : 9).getTime();
        let group = new ActionGroup();
        let action = new ActionsModel({ StartDate: startDate, EndDate: endDate });
        action.UserAssignedId = localStorage.getItem("userId");
        action.Status = "Open";
        action.Priority = "Normal";
        group.Actions = [action];
        this.actionSvc.newGroup(group);
    }

    generateCalendar() {
        let now = new Date();
        this.weekDay = now.getDay();
        this.todaysDate = now.getDate();

        this.lastMonth = this.months[this.month == 0 ? 11 : this.month - 1];
        this.thisMonth = this.months[this.month];
        this.nextMonth = this.months[this.month == 11 ? 0 : this.month + 1];


        let days = new Date(this.year, this.month + 1, 0).getDate();
        //console.log("Days this month: ", days);

        let fictNow = new Date(this.year, this.month);
        let weekDay = fictNow.getDay() == 0 ? 7 : fictNow.getDay();
        //console.log("fictnow:", fictNow, "weekday: " + weekDay);

        let lastDate = new Date(this.year, this.month, 0);
        //console.log("lastdaylastmonth", lastDate);

        let lastDayLastMonth = lastDate.getDate();

        let before = lastDayLastMonth - weekDay + 2;
        //console.log("weekday: ", weekDay, "before: ", before, lastDayLastMonth);

        this.days = [];
        for (let i = 0; i < weekDay - 1; i++) {
            if (!this.days[i]) this.days[i] = [];
            this.days[i].push(new CalendarDay({ Date: before, ThisMonth: false, Weekend: weekDay > 5 }));
            before++;
        }

        for (let i = 1; i <= days; i++) {
            if (weekDay > 7) weekDay = 1;
            if (!this.days[weekDay - 1]) this.days[weekDay - 1] = [];
            this.days[weekDay - 1].push(new CalendarDay({ Date: i, ThisMonth: true, Weekend: weekDay > 5, Today: i == this.todaysDate && this.month == now.getMonth(), Events: [] }));
            weekDay++;
        }
        let rest = 7 - weekDay;
        for (let i = 0; i <= rest; i++) {
            this.days[weekDay - 1].push(new CalendarDay({ Date: i + 1, ThisMonth: false, Weekend: weekDay > 5 }));
            weekDay++
        }
        this.getCalendarEvents(this.year, this.month);
    }

    getCalendarEvents(year: number, month: number) {
        this.calendarSvc.getCalendarEvents(year, month, (data: any) => {
            if (data.IsSuccess) {
                for (let ev of data.Data) {
                    let date = new Date(ev.Start).getDate();
                    let weekDay = new Date(ev.Start).getDay() - 1;
                    //console.log(this.days, weekDay, date, ev);
                    let day = this.days[weekDay == -1 ? 6 : weekDay].find(x => x.Date == date);
                    day.Events.push(ev);
                }
                console.log("EVENTS: ", data.Data);
            }
            else {
                console.log("[Calendar] Could not get any Events");
            }
        })
    }

    checkTrunc(e: any) {
        this.showToolTip = e.target.offsetWidth < e.target.scrollWidth;
    }

    openDay(day: CalendarDay) {
        if (day.Events.length) {
            day.Expanded = true;
        }
    }
    closeDay(e: any) {
        if (e.target.id == "dayModal") {
            this.displayDay = null;
        }
    }

    openActionModal(actionId: string) {
        this.actionSvc.showGroupByActionId(actionId);
    }
}