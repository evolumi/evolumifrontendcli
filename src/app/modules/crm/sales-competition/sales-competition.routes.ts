import { Routes } from '@angular/router';
import { Features } from '../../../services/organisation.service';
import { EvoRequireGuard } from '../../../services/guards/evo-require-guard';

import { SalesCompetitionComponent } from "./sales-competition.component";
import { SalesCompetitionLeaderboardComponent } from './sales-competition-leaderboard.component';

export const SalesCompetitionRoutes: Routes = [
    {
        path: 'SalesCompetition/:salesCompId',
        component: SalesCompetitionLeaderboardComponent
    },
    {
        path: 'SalesCompetition',
        component: SalesCompetitionComponent,
        canActivate: [EvoRequireGuard],
        data: { features: [Features.SalesCompetition] }
    }
];