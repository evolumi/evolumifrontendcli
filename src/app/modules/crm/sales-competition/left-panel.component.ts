import { Component, EventEmitter, Output, OnInit, OnDestroy } from '@angular/core';
import { SalesCompetitionService } from '../../../services/sales-competition.service';
import { Subscription } from 'rxjs/Subscription';
import { SalesCompetitionModel } from '../../../models/sales-competition.models';

@Component({
    selector: 'left-panel-sales-comp',
    templateUrl: 'left-panel.component.html',
    styleUrls: ['sales-competition.css']
})
export class LeftPanelComponent implements OnInit, OnDestroy {

    @Output() search = new EventEmitter<string>();

    public activeSalesComps: Array<SalesCompetitionModel> = [];
    public inactiveSalesComps: Array<SalesCompetitionModel> = [];

    public optionsArray: Array<any> = [];
    public selectOption: string = 'selected';

    private searchParams: any;
    public sideMenuActive: boolean;

    private salesCompSub: Subscription;

    constructor(public salesSvc: SalesCompetitionService) { }

    ngOnInit() {
        // this.getOptionsArray();
        this.searchParams = this.salesSvc.searchParams;
        this.salesCompSub = this.salesSvc.salesCompListObs()
            .subscribe(salesComps => {
                if (salesComps) {
                    this.activeSalesComps = [];
                    this.inactiveSalesComps = [];
                    const timestamp = new Date().getTime();
                    for (let i = 0; i < salesComps.length; i++) {
                        if (salesComps[i].EndDate >= timestamp) {
                            this.activeSalesComps.push(salesComps[i]);
                        } else {
                            this.inactiveSalesComps.push(salesComps[i]);
                        }
                    }
                }
            })
    }

    ngOnDestroy() {
        if (this.salesCompSub) this.salesCompSub.unsubscribe();
        this.salesSvc.reset();
    }

    onSearch(val: string) {
        this.salesSvc.resetPaging();
        this.salesSvc.searchSalesComps(val);
    }

    onScroll() {
        this.salesSvc.onScroll();
    }

    onActiveChange() {
        this.salesSvc.searchParams.Active = !this.salesSvc.searchParams.Active;
        this.salesSvc.resetPaging();
        this.salesSvc.getSalesComps();
    }

    onInactiveChange() {
        this.salesSvc.searchParams.Inactive = !this.salesSvc.searchParams.Inactive;
        this.salesSvc.resetPaging();
        this.salesSvc.getSalesComps();
    }
}