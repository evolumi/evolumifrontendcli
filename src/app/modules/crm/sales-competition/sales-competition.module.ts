import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';

import { SalesCompetitionLeaderboardComponent } from './sales-competition-leaderboard.component';
import { LeftPanelComponent } from './left-panel.component';
import { AddEditSalesCompComponent } from './add-edit/add-edit-sales-comp.component';
import { AddEditCompRuleComponent } from './add-edit/add-edit-comp-rule.component';
import { RulesDisplayComponent } from './add-edit/rules-display.component';
import { SalesCompetitionComponent } from "./sales-competition.component";

@NgModule({
    imports: [
        SharedModule
    ],
    exports: [],
    declarations: [
        SalesCompetitionComponent,
        SalesCompetitionLeaderboardComponent,
        LeftPanelComponent,
        AddEditSalesCompComponent,
        AddEditCompRuleComponent,
        RulesDisplayComponent
    ],
})
export class SalesCompetitionModule { }
