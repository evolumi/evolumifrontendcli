import { Component, OnInit, OnDestroy, ViewChild, ViewContainerRef, ComponentRef, ComponentFactoryResolver } from '@angular/core';
import { SalesCompetitionService } from '../../../services/sales-competition.service';
import { Subscription } from 'rxjs/Subscription';
import { ButtonPanelService } from '../../../services/button-panel.service';
import { SalesCompLeaderboard } from '../../../models/sales-competition.models';
import { TranslateService } from '@ngx-translate/core';
import { HighchartService } from '../../../services/highchart.service';
import { HighchartMultiComponent } from '../../shared/components/highchart/highchart.component'
import { ConfirmationService } from '../../../services/confirmation.service';
import { ActivatedRoute, Router } from "@angular/router";

@Component({

    selector: 'sales-competition-leaderboard',
    templateUrl: 'sales-competition-leaderboard.component.html',
    styleUrls: ['sales-competition.css']
})
export class SalesCompetitionLeaderboardComponent implements OnInit, OnDestroy {

    @ViewChild('highchartCont', { read: ViewContainerRef }) highchartCont: ViewContainerRef;
    private highchartRef: ComponentRef<HighchartMultiComponent>;

    private salesCompId: string;
    public salesCompData: SalesCompLeaderboard;

    // subs
    private routeSub: Subscription;
    private buttonSub: Subscription;
    private salesCompSub: Subscription;
    // private salesCompListSub: Subscription;

    constructor(public salesSvc: SalesCompetitionService,
        private translate: TranslateService,
        public highchartSvc: HighchartService,
        private cfr: ComponentFactoryResolver,
        private confirmationSvc: ConfirmationService,
        private button: ButtonPanelService,
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() {

        this.button.addPanelButton("ADDSALESCOMP", "add-salescomp", "plus");
        this.buttonSub = this.button.commandObservable().subscribe(command => {
            if (command == "add-salescomp") this.openCreate();
        });

        this.routeSub = this.route.params
            .subscribe((res: any) => {
                this.salesCompId = res['salesCompId'];
                if (this.salesCompId) {
                    this.salesSvc.getSalesCompLeaderboard(this.salesCompId);
                }
            });

        this.salesCompSub = this.salesSvc.currentSalesCompLeaderboardObs()
            .subscribe(salesComp => {
                if (salesComp) {
                    this.resetHighchart();
                    this.salesCompData = salesComp;
                    this.highchartSvc.options = salesComp.Charts;
                    this.highchartSvc.options = this.highchartSvc.options.slice();
                }
            });
        // this.salesCompListSub = this.salesSvc.salesCompListObs()
        //     .subscribe(list => {
        //         if (list) {
        //             this.salesSvc.reroute();
        //         }
        //     });
    }

    ngOnDestroy() {
        if (this.routeSub) this.routeSub.unsubscribe();
        if (this.salesCompSub) this.salesCompSub.unsubscribe();
        // if (this.salesCompListSub) this.salesCompListSub.unsubscribe();
        if (this.buttonSub) this.buttonSub.unsubscribe();
        this.button.clearPanel();
    }

    openCreate() {
        this.salesSvc.openAddSalesComp();
    }

    // openEdit() {
    //     this.salesSvc.openAddSalesComp(this.salesCompData.SalesCompetition.Id);
    // }

    resetHighchart() {
        this.highchartSvc.clear();
        if (this.highchartRef) {
            this.highchartRef.destroy();
        }
        let factory = this.cfr.resolveComponentFactory(HighchartMultiComponent);
        this.highchartRef = this.highchartCont.createComponent(factory);
        this.highchartRef.instance.minHeight = '470px';
        // this.cdr.detectChanges();
    }

    delete() {
        let boxTitle = this.translate.instant("DELETESALESCOMP");
        let boxMessage = this.translate.instant("CONFIRMDELETESALESCOMP")
        this.confirmationSvc.showModal(boxTitle, boxMessage, () => {
            this.salesSvc.deleteSalesComp(this.salesCompData.SalesCompetition.Id, (success) => {
                if (success) {
                    this.router.navigate(['/CRM', 'SalesCompetition']);
                }
            });
        });
    }
}