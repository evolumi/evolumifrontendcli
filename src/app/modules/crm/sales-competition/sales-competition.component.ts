import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { SalesCompetitionModel } from "../../../models/sales-competition.models";
import { SalesCompetitionService } from "../../../services/sales-competition.service";
import { ButtonPanelService } from "../../../services/button-panel.service";
import { Router } from "@angular/router";

@Component({

    selector: 'sales-competition',
    templateUrl: './sales-competition.component.html',
    styleUrls: ['./sales-competition.css']
})

export class SalesCompetitionComponent implements OnInit, OnDestroy {

    public activeSalesComps: Array<SalesCompetitionModel> = [];
    public inactiveSalesComps: Array<SalesCompetitionModel> = [];

    private buttonSub: Subscription;
    private salesCompListSub: Subscription;

    constructor(public salesSvc: SalesCompetitionService,
        private button: ButtonPanelService,
        private router: Router) { }

    ngOnInit() {
        this.salesSvc.getSalesComps();
        this.salesCompListSub = this.salesSvc.salesCompListObs()
            .subscribe(list => {
                if (list) {
                    this.activeSalesComps = [];
                    this.inactiveSalesComps = [];
                    const timestamp = new Date().getTime();
                    for (let i = 0; i < list.length; i++) {
                        if (list[i].EndDate >= timestamp) {
                            this.activeSalesComps.push(list[i]);
                        } else {
                            this.inactiveSalesComps.push(list[i]);
                        }
                    }
                }
            });

        this.button.addPanelButton("ADDSALESCOMP", "add-salescomp", "plus");
        this.buttonSub = this.button.commandObservable().subscribe(command => {
            if (command == "add-salescomp") this.openCreate();
        });
    }

    ngOnDestroy() {
        if (this.salesCompListSub) this.salesCompListSub.unsubscribe();
        if (this.buttonSub) this.buttonSub.unsubscribe();
        this.button.clearPanel();
    }

    openCreate() {
        this.salesSvc.openAddSalesComp();
    }

    delete(id: string) {
        this.salesSvc.deleteSalesComp(id);
    }

    leaderboard(id: string) {
        this.router.navigateByUrl('/CRM/SalesCompetition/' + id);
    }
}