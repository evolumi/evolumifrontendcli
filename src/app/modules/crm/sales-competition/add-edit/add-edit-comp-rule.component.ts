import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { SalesCompRuleModel } from '../../../../models/sales-competition.models';
import { ChosenOption } from '../../../shared/components/chosen/chosen';
import { ChosenOptionModel, ChosenOptionGroupModel } from '../../../shared/components/chosen/chosen.models';
import { SalesCompetitionService } from '../../../../services/sales-competition.service';
import { ActionsCollection } from '../../../../services/actionservice';
import { GoalsService } from '../../../../services/goals.service';
import { Subscription } from 'rxjs/Subscription';
import { OrgService } from '../../../../services/organisation.service';
import { Configuration } from '../../../../services/configuration';
import { OrganisationModel } from "../../../../models/organisation.models";

@Component({

    selector: 'addedit-comprule',
    templateUrl: 'add-edit-comp-rule.html',
    styleUrls: ['../sales-competition.css']
})
export class AddEditCompRuleComponent implements OnInit, OnDestroy {

    @Output() ruleAdded = new EventEmitter<SalesCompRuleModel>();

    //flags
    public isEdit: boolean;

    // view objects
    public compRule: SalesCompRuleModel;
    public compTypeList: Array<ChosenOption> = [];
    public actionTypeList: Array<ChosenOption> = [];
    public actionStatusList: Array<ChosenOption> = [];
    public dealStatusList: Array<ChosenOption> = [];
    public labelList: Array<ChosenOption> = [];
    public salesProcessList: Array<ChosenOption> = [];
    public currencies: Array<ChosenOption> = [];
    public statusGroups: Array<ChosenOptionGroupModel> = [];

    // privates
    private currentOrg: OrganisationModel;

    //subscriptions
    private orgSub: Subscription;
    private ruleSub: Subscription;

    constructor(private compSvc: SalesCompetitionService,
        private goalSvc: GoalsService,
        private actionSvc: ActionsCollection,
        private orgSvc: OrgService,
        private config: Configuration) { }

    ngOnInit() {
        this.ruleSub = this.compSvc.currentRuleObs()
            .subscribe(rule => {
                if (rule) {
                    this.compRule = JSON.parse(JSON.stringify(rule));
                } else {
                    this.compRule = new SalesCompRuleModel({});
                }
            });
        this.compTypeList = this.goalSvc.getGoalTypes();
        this.actionTypeList = this.actionSvc.getActionTypes();
        this.orgSub = this.orgSvc.currentOrganisationObservable()
            .subscribe(org => {
                if (org) {
                    this.currentOrg = org;
                    if (org.Settings.SaleProcesses) {
                        this.salesProcessList = org.Settings.SaleProcesses
                            .map((x: any) => new ChosenOptionModel(x.Id, x.Name));
                        if (this.salesProcessList.length === 1) {
                            this.compRule.SalesProcessId = this.salesProcessList[0].value.toString();
                            this.populateDealStatusChosen(org.Settings.SaleProcesses[0]);

                            // this.dealStatusList = org.Settings.SaleProcesses[0].DealStatuses
                            //     .map((x: any) => new ChosenOptionModel(x.Id, x.Name));
                        }
                    }
                    if (org.Labels) {
                        this.labelList = org.Labels.map(x => new ChosenOptionModel(x, x));
                    }
                }
            });
        this.currencies.push(...this.config.currencies.map(x => new ChosenOptionModel(x, x)));
        this.statusGroups.push(new ChosenOptionGroupModel(1, "SALESPIPE"));
        this.statusGroups.push(new ChosenOptionGroupModel(2, "POSTSALES"));
        this.statusGroups.push(new ChosenOptionGroupModel(3, "LOSTDEALS"));
        this.statusGroups.push(new ChosenOptionGroupModel(4, "CLOSED"));
    }

    ngOnDestroy() {
        if (this.orgSub) this.orgSub.unsubscribe();
        if (this.ruleSub) this.ruleSub.unsubscribe();
    }

    addRule() {
        this.ruleAdded.emit(this.compRule);
        this.back();
    }

    onTypeChange(event: string) {
        if (this.compRule.SalesGoalType && this.compRule.SalesGoalType.length > 0) {
            this.compRule = new SalesCompRuleModel({ SalesGoalType: event });
            if (this.compRule.SalesGoalType === 'DealsValueOf') {
                this.compRule.Currency = this.currentOrg ? this.currentOrg.CurrencyName : 'USD';
            }
        }
    }

    onActionTypeChanged(actionTypes: Array<string>) {
        this.compRule.ActionTypes = actionTypes;
        this.actionStatusList = [];
        this.actionSvc.getActionTypesAndStatuses(this.compRule.SalesGoalType === 'ClosedActions').reduce((res, curr) => {
            if (this.compRule.ActionTypes.indexOf(curr.value) > -1) {
                this.actionStatusList.push(...curr.statuses);
            }
            return res;
        }, []);
        this.actionStatusList = this.actionStatusList.filter((x, i, arr) => arr.indexOf(x) === i);
        // to make the chosen bind properly
        this.compRule.ActionStatuses = this.actionStatusList.filter(x => this.compRule.ActionStatuses.indexOf(x.value.toString()) > -1).map(x => x.value.toString());
    }

    chooseAll(type: string) {
        switch (type) {
            case 'ActionTypes':
                this.compRule.ActionTypes = this.actionTypeList.map(x => x.value.toString());
                if (this.compRule.SalesGoalType === 'ClosedActions') {
                    this.onActionTypeChanged(this.compRule.ActionTypes)
                }
                break;
            case 'ActionStatuses':
                this.compRule.ActionStatuses = this.actionStatusList.map(x => x.value.toString());
                break;
        }
    }

    onSaleProcessChange(value: any) {
        const saleProcess = this.currentOrg.Settings.SaleProcesses.find((x: any) => x.Id === value)
        this.populateDealStatusChosen(saleProcess);
        // this.dealStatusList = saleProcess.DealStatuses
        //     .map((x: any) => new ChosenOptionModel(x.Id, x.Name));
    }

    private populateDealStatusChosen(saleProcess: any) {
        this.dealStatusList = saleProcess.DealStatuses
            .sort((x: any, y: any) => x.SortOrder - y.SortOrder)
            .map((x: any) => new ChosenOptionModel(x.Id, x.Name, x.Type));

        // this.dealStatusList = [];
        // let sortedStatuses = saleProcess.DealStatuses.sort((x: any, y: any) => x.SortOrder - y.SortOrder);
        // for (let status of saleProcess.Dealstatuses) {
        //     this.dealStatusList.push(new ChosenOptionModel(status.Id, status.Name, status.Type));
        // }
    }

    clickToClose(event: any) {
        let target = event.target || event.srcElement;
        if (target.id === 'addCompRule') {
            this.back();
        }
    }

    back() {
        this.compSvc.closeAddRule();
    }
}