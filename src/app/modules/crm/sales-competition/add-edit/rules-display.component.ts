import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { SalesCompRuleModel } from '../../../../models/sales-competition.models';
import { OrgService } from '../../../../services/organisation.service';
import { ActionsCollection } from '../../../../services/actionservice';
import { Subscription } from 'rxjs/Subscription';
import { OrganisationModel } from "../../../../models/organisation.models";

@Component({
    selector: 'rules-display',
    template: `
        <div class="rules-display-group">
            <p>{{description}}</p>
            <p>{{points}}</p>
            <!--<i class="fa fa-trash rules-display-icon clickable" (click)="onRemove()"></i>-->
        </div>
    `,
    styleUrls: ['../sales-competition.css']
})
export class RulesDisplayComponent implements OnInit, OnDestroy {

    @Input() compRule: SalesCompRuleModel;
    @Input() index: number;
    @Output() removeRule = new EventEmitter<number>();

    public points: string;
    public description: string;
    private currentOrg: OrganisationModel;

    private orgSub: Subscription;

    constructor(private translate: TranslateService, private orgSvc: OrgService, private actionService: ActionsCollection) { }

    ngOnInit() {
        this.orgSub = this.orgSvc.currentOrganisationObservable()
            .subscribe(org => {
                if (org) this.currentOrg = org;
            });

        switch (this.compRule.SalesGoalType) {
            case 'ClosedActions':
                this.getClosedActionData();
                break;
            case 'CreatedActions':
                this.getCreatedActionData();
                break;
            case 'DealsValueOf':
                this.getDealsValueOfData();
                break;
            case 'CreatedDeals':
                this.getCreatedDealsData();
                break;
            case 'NumberOfDeals':
                this.getNumberOfDealsData();
                break;
            default:
                break;
        }
    }

    ngOnDestroy() {
        if (this.orgSub) this.orgSub.unsubscribe();
    }

    onRemove() {
        this.removeRule.emit(this.index);
    }

    getClosedActionData() {
        let labels = '';
        let actionType = this.compRule.ActionTypes.map(x => this.actionService.getActionTypelabel(x)).join(', ');
        if (this.compRule.Labels.length > 0) {
            for (let i = 0; i < this.compRule.Labels.length; i++) {
                labels += `${this.compRule.Labels[i]}, `;
            }
            labels = labels.substring(0, labels.length - 2);
        }
        if (labels && actionType) {
            this.description = this.translate.instant('SALESCOMPRULECLOSEDACTIONFULL', { labels, actionType });
        } else if (labels) {
            this.description = this.translate.instant('SALESCOMPRULECLOSEDACTIONLABEL', { labels })
        } else if (actionType) {
            this.description = this.translate.instant('SALESCOMPRULECLOSEDACTIONTYPE', { actionType })
        }
        this.points = this.translate.instant('SALESCOMPRULEPOINTSPERCLOSEDACTION', { points: this.compRule.Value, quantity: this.compRule.Quantity })
    }

    getCreatedActionData() {
        let labels = '';
        let actionType = this.compRule.ActionTypes.map(x => this.actionService.getActionTypelabel(x)).join(', ');
        if (this.compRule.Labels.length > 0) {
            for (let i = 0; i < this.compRule.Labels.length; i++) {
                labels += `${this.compRule.Labels[i]}, `;
            }
            labels = labels.substring(0, labels.length - 2);
        }
        if (labels && actionType) {
            this.description = this.translate.instant('SALESCOMPRULECREATEDACTIONFULL', { labels, actionType });
        } else if (labels) {
            this.description = this.translate.instant('SALESCOMPRULECREATEDACTIONLABEL', { labels })
        } else if (actionType) {
            this.description = this.translate.instant('SALESCOMPRULECREATEDACTIONTYPE', { actionType })
        }
        this.points = this.translate.instant('SALESCOMPRULEPOINTSPERCREATEDACTION', { points: this.compRule.Value, quantity: this.compRule.Quantity })
    }

    getDealsValueOfData() {
        if (!this.compRule.SalesProcessId) this.compRule.SalesProcessId = this.currentOrg.Settings.SaleProcesses[0].Id;
        let saleProcess = this.currentOrg.Settings.SaleProcesses.find((x: any) => x.Id === this.compRule.SalesProcessId);// || this.currentOrg.Settings.SaleProcesses[0];
        let dealStatus = saleProcess.DealStatuses.find((x: any) => x.Id === this.compRule.DealStatusId);
        const currency = this.currentOrg.CurrencyName;

        this.description = this.translate.instant('SALESCOMPRULEVALUEOFDEALSFULL', { saleProcess: saleProcess ? saleProcess.Name : '', dealStatus: dealStatus ? dealStatus.Name : '' });
        this.points = this.translate.instant('SALESCOMPRULEPOINTSPERVALUEOFDEALS', { points: this.compRule.Value, quantity: this.compRule.Quantity, currency: currency })
    }

    getCreatedDealsData() {
        this.description = this.translate.instant('SALESCOMPRULECREATEDDEALSFULL');
        this.points = this.translate.instant('SALESCOMPRULEPOINTSPERCREATEDDEALS', { points: this.compRule.Value, quantity: this.compRule.Quantity })
    }

    getNumberOfDealsData() {
        if (!this.compRule.SalesProcessId) this.compRule.SalesProcessId = this.currentOrg.Settings.SaleProcesses[0].Id;
        let saleProcess = this.currentOrg.Settings.SaleProcesses.find((x: any) => x.Id === this.compRule.SalesProcessId);
        let dealStatus = saleProcess.DealStatuses.find((x: any) => x.Id === this.compRule.DealStatusId);
        const currency = this.currentOrg.CurrencyName;
        this.description = this.translate.instant('SALESCOMPRULENUMBEROFDEALSFULL', { saleProcess: saleProcess ? saleProcess.Name : '', dealStatus: dealStatus ? dealStatus.Name : '' });
        this.points = this.translate.instant('SALESCOMPRULEPOINTSPERNUMBEROFDEALS', { points: this.compRule.Value, quantity: this.compRule.Quantity, currency: currency })
    }
}