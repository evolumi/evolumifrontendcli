import { Component, OnInit, OnDestroy } from '@angular/core';
import { SalesCompetitionModel, SalesCompRuleModel } from '../../../../models/sales-competition.models';
import { SalesCompetitionService } from '../../../../services/sales-competition.service';
import { Subscription } from 'rxjs/Subscription';
import { UsersService } from '../../../../services/users.service';
import { ChosenOptionModel } from '../../../shared/components/chosen/chosen.models';
import { DateService } from "../../../../services/date.service";

@Component({
    selector: 'addedit-sales-comp',
    templateUrl: 'add-edit-sales-comp.html',
    styleUrls: ['../sales-competition.css']
})
export class AddEditSalesCompComponent implements OnInit, OnDestroy {

    public salesComp: SalesCompetitionModel;
    public salesCompUntouched: SalesCompetitionModel;
    public originalUserList: Array<ChosenOptionModel> = [];
    public participantList: Array<ChosenOptionModel> = [];
    public addedParticipants: Array<ChosenOptionModel> = [];
    public observerList: Array<ChosenOptionModel> = [];
    public addedObservers: Array<ChosenOptionModel> = [];

    //flags
    public isViewMode: boolean;
    public isEditMode: boolean;
    public dateOnly: boolean = true;

    //subs
    private salesSub: Subscription;
    private userSub: Subscription;

    constructor(public salesSvc: SalesCompetitionService,
        private usersSvc: UsersService,
        private dateSvc: DateService) { }

    ngOnInit() {
        this.salesSub = this.salesSvc.addEditSalesCompObs()
            .subscribe(comp => {
                if (comp) {
                    this.salesComp = JSON.parse(JSON.stringify(comp));
                    this.salesCompUntouched = JSON.parse(JSON.stringify(comp));
                    if (comp.Id && comp.Id.length > 0) {
                        this.isEditMode = true;
                        this.loadLists();
                    }
                } else {
                    this.salesComp = new SalesCompetitionModel({});
                }
            });
        this.userSub = this.usersSvc.activeUserListObervable()
            .subscribe(users => {
                if (users) {
                    this.originalUserList = JSON.parse(JSON.stringify(users));
                    this.participantList = JSON.parse(JSON.stringify(users));
                    this.observerList = JSON.parse(JSON.stringify(users));
                    this.loadLists();
                }
            })
    }

    ngOnDestroy() {
        if (this.salesSub) this.salesSub.unsubscribe();
        if (this.userSub) this.userSub.unsubscribe();
    }

    createSalesComp() {
        if (this.dateOnly) {
            this.salesComp.StartDate = this.dateSvc.getDateStartOfDay(this.salesComp.StartDate);
            this.salesComp.EndDate = this.dateSvc.getDateEndOfDay(this.salesComp.EndDate);
        }
        this.salesComp.Participants = this.addedParticipants.map(x => x.value.toString());
        this.salesComp.Observers = this.addedObservers.map(x => x.value.toString());
        //this.salesSvc.addSalesComp(this.salesComp, this.back.bind(this));
        this.salesSvc.addSalesComp(this.salesComp);
    }

    updateSalesComp() {
        this.salesComp.Participants = this.addedParticipants.map(x => x.value.toString());
        this.salesComp.Observers = this.addedObservers.map(x => x.value.toString());
        this.salesSvc.updateSalesComp(this.salesComp);
    }

    onAddParticipant(user: ChosenOptionModel, index: number) {
        this.participantList.splice(index, 1);
        this.addedParticipants.push(user);
    }

    onRemoveParticipant(user: ChosenOptionModel, index: number) {
        this.addedParticipants.splice(index, 1);
        this.participantList.push(user);
    }

    onAddAllParticipants() {
        this.addedParticipants.push(...this.participantList);
        this.participantList = [];
    }

    onRemoveAllParticipants() {
        this.addedParticipants = [];
        this.participantList = this.originalUserList.reduce((res, curr) => {
            if (!this.addedObservers.find(x => x.value == curr.value)) {
                res.push(curr);
            }
            return res;
        }, [])
    }

    onAddObserver(user: ChosenOptionModel, index: number) {
        this.observerList.splice(index, 1);
        this.addedObservers.push(user);
        this.removeParticipantIfObserver(user);
    }

    onRemoveObserver(user: ChosenOptionModel, index: number) {
        this.addedObservers.splice(index, 1);
        this.observerList.push(user);
        this.participantList.push(user);
    }

    private removeParticipantIfObserver(user: ChosenOptionModel) {
        const addIndex = this.addedParticipants.findIndex(x => x.value === user.value);
        const allIndex = this.participantList.findIndex(x => x.value === user.value);

        if (addIndex > -1) this.addedParticipants.splice(addIndex, 1);
        if (allIndex > -1) this.participantList.splice(allIndex, 1);
    }

    private loadLists() {
        let arrays = this.originalUserList.reduce((res: any, curr: any) => {
            if (!res['addp']) {
                res['addp'] = [];
                res['listp'] = [];
                res['addo'] = [];
                res['listo'] = [];
            }
            if (this.salesComp.Observers.find(x => x === curr.value)) {
                res['addo'].push(curr);
            } else if (this.salesComp.Participants.find(x => x === curr.value)) {
                res['addp'].push(curr);
                res['listo'].push(curr);
            } else {
                res['listo'].push(curr);
                res['listp'].push(curr);
            }
            return res;
        }, []);

        this.addedParticipants = arrays['addp'];
        this.participantList = arrays['listp'];
        this.addedObservers = arrays['addo'];
        this.observerList = arrays['listo'];
    }

    onAddRule(compRule: SalesCompRuleModel) {
        this.salesComp.Rules.push(compRule);
    }

    onOpenAddRule() {
        this.salesSvc.openAddRule();
    }

    removeRule(index: number) {
        this.salesComp.Rules.splice(index, 1);
    }

    cancelChanges() {
        this.salesComp = JSON.parse(JSON.stringify(this.salesCompUntouched));
        this.back();
    }

    clickToClose(event: any) {
        let target = event.target || event.srcElement;
        if (target.id === 'addSalesComp') {
            this.back();
        } else {
            // const element = document.getElementById('userAdd');
            // const element2 = document.getElementById('categoryAdd');
            // if (target !== element && !element.contains(target) && target.className.indexOf('prevent-close') === -1
            //     && target !== element2 && !element2.contains(target)) {
            //     this.addUser = false;
            //     this.addCategory = false;
            // }
        }
    }

    back() {
        this.salesSvc.closeAddSalesComp();
        this.isEditMode = false;
    }
}