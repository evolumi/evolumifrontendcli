import { Component } from '@angular/core';

@Component({
  selector: 'header-bar',
  template: `<header class="header white admin">
    <div class="container-fluid">
      <div class="row"> 
        <div class="col-sm-12">
          <a class="logo">
            <img alt="" class="hidden-xs" src="assets/images/evolumi_logo.png">
          </a>
        </div>
      </div>
    </div>
   </header>
<div class="primaryMenu whiteHeader">

    <a *ngFor="let link of links" [routerLink]="[link.path]" [routerLinkActive]="['evo-nav-active']" class="evo-nav-section-link white-menu">
      {{link.text}}
    </a>

</div>
  `,
  styleUrls: ['../../header/header.css']
})
export class Headerbar {

  public links: Array<any> = [];

  constructor() {
    this.links.push(
      { path: 'Users', text: 'Customers', icon: '' },
      { path: 'Sponsors', text: 'Daily Sponsors', icon: '' },
      { path: 'Activity', text: 'Activity', icon: '' },
      { path: 'Packages', text: 'Packages', icon: '' },
      { path: '/Overview', text: 'Crm', icon: '' },
      { path: 'ReferCode', text: 'ReferCode', icon: '' }
    )
  }

}
