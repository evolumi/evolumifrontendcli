import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { StatisticsService } from './statistics.service';
import { Subscription } from "rxjs/Subscription";

@Component({
    templateUrl: 'statistics.html',
    providers: [StatisticsService]
})
export class StatisticsComponent {

    public statisticsData: any;
    public year: any;
    public statisticsArray: Array<any>[];
    public userYearArray: Array<any> = [];
    public orgYearArray: Array<any> = [];
    public monthArray: Array<any> = [];
    public monthLabelArray: Array<any> = [];
    public dayArray: Array<any> = [];
    public labelArray: Array<any> = [];
    public labelUsersArray: Array<any> = [];
    public dataUsersArray: Array<any> = [];
    public labelOrgArray: Array<any> = [];
    public dataOrgArray: Array<any> = [];
    public _userCount: any = '';
    public _orgCount: any;
    public userChart: any;
    public orgChart: any;
    public clientsChart: any;
    public url: string;
    public usersForm: FormGroup;
    public orgForm: any;
    public dataArray: Array<any> = [];
    public canvas: any;
    public category: any;
    public userArray: Array<any> = [];
    public orgsArray: Array<any> = [];
    public userYear: any;
    public userMonth: any;
    public userDay: any;
    public orgYear: any;
    public orgMonth: any;
    public orgDay: any;
    public filterMonth: Array<any> = []
    public filterDay: Array<any> = []

    private userSub: Subscription;
    private orgSub: Subscription;
    private userDaySub: Subscription;
    private orgDaySub: Subscription;

    constructor(public _statistics: StatisticsService, public _form: FormBuilder) {

        this.userYearArray = ['Year'];
        this.orgYearArray = ['Year'];
        this.monthArray = ['Month', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        this.monthLabelArray = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        this.dayArray = ['Day', 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31];
        this.usersForm = this._form.group({
            user_year: [''],
            user_month: [''],
            user_day: [''],
            org_year: [''],
            org_month: [''],
            org_day: ['']
        });

        this.userYear = 'Year';
        this.userMonth = 0;
        this.userDay = 0;
        this.orgYear = 'Year';
        this.orgMonth = 0;
        this.orgDay = 0;

    }

    stringAsDate(dateStr: string) {
        var date = new Date(dateStr);
        return date;
    }

    showUsers(category: any, canvas: any) {

        this.category = category;
        this.canvas = canvas;
        // TweenMax.set('#clients', { className: '-=hideCanvas' });
        // TweenMax.set('#userInfo', { className: '+=hideUserInfo' });
        // TweenMax.set('#users_title', { className: '-=hideCanvas' });
        this._statistics.getAllUsers();
    }

    showOrg(category: any, canvas: any) {

        this.category = category;
        this.canvas = canvas;
        // TweenMax.set('#oragization', { className: '-=hideCanvas' });
        // TweenMax.set('#orgInfo', { className: '+=hideUserInfo' });
        // TweenMax.set('#org_title', { className: '-=hideCanvas' });
        this._statistics.getAllOrganizations();
    }

    showUsersByFilter(category: any, canvas: any, event: any) {
        this.category = category;
        this.canvas = canvas;

        if (category == 'Day' && event != 0) {
            // TweenMax.set('#clients', { className: '+=hideCanvas' });
            // TweenMax.set('#userInfo', { className: '-=hideUserInfo' });
            // TweenMax.set('#users_title', { className: '+=hideCanvas' });

            this.usersForm.value.user_day = event;
            this._statistics.getUsersByDayFilter(this.usersForm.value);

        }
        else if (category == 'Year' && event != 'Year') {
            this.usersForm.value.user_year = event;
            this._statistics.getUsersByFilter(this.usersForm.value);
        }
        else if (category == 'Month' && event != 0) {
            this.usersForm.value.user_month = event;
            this._statistics.getUsersByFilter(this.usersForm.value);

        }
        else {
            if (event == 0 && this.category == 'Month') {
                this.category = 'Year';
                this.usersForm.value.user_month = event;
            }
            else if (event == 'Year' && this.category == 'Year') {
                this.category = '';
                this.usersForm.value.user_year = event;
            }
            else if (event == 0 && this.category == 'Day') {

                this.category = 'Day';
                this.usersForm.value.user_day = event;
            }

            // TweenMax.set('#clients', { className: '-=hideCanvas' });
            // TweenMax.set('#userInfo', { className: '+=hideUserInfo' });
            // TweenMax.set('#users_title', { className: '-=hideCanvas' });
            this._statistics.getUsersByFilter(this.usersForm.value);
        }

    }

    showOrgByFilter(category: any, canvas: any, event: any) {

        this.category = category;
        this.canvas = canvas;

        if (category == 'Day' && event != 0) {

            // TweenMax.set('#oragization', { className: '+=hideCanvas' });
            // TweenMax.set('#orgInfo', { className: '-=hideUserInfo' });
            // TweenMax.set('#org_title', { className: '+=hideCanvas' });
            this.usersForm.value.org_day = event;
            this._statistics.getOrgByDayFilter(this.usersForm.value);
        }
        else if (category == 'Year' && event != 'Year') {
            this.usersForm.value.org_year = event;
            this._statistics.getOrgByFilter(this.usersForm.value);
        }
        else if (category == 'Month' && event != 0) {
            this.usersForm.value.org_month = event;
            this._statistics.getOrgByFilter(this.usersForm.value);

        }

        else {
            if (event == 0 && this.category == 'Month') {
                this.category = 'Year';
                this.usersForm.value.org_month = event;
            }
            else if (event == 'Year' && this.category == 'Year') {
                this.category = '';
                this.usersForm.value.org_year = event;
            }
            else if (event == 0 && this.category == 'Day') {

                this.category = 'Day';
                this.usersForm.value.org_day = event;
            }

            // TweenMax.set('#oragization', { className: '-=hideCanvas' });
            // TweenMax.set('#orgInfo', { className: '+=hideUserInfo' });
            // TweenMax.set('#org_title', { className: '-=hideCanvas' });
            this._statistics.getOrgByFilter(this.usersForm.value);
        }

    }

    ngOnInit() {
        this.year = '';

        for (var i = 0; i < this.monthArray.length; i++) {

            this.filterMonth.push({
                value: i,
                label: this.monthArray[i]
            })

        }

        for (var i = 0; i < this.dayArray.length; i++) {

            this.filterDay.push({
                value: i,
                label: this.dayArray[i]
            })

        }

        this.userSub = this._statistics.statisticsUsers$.subscribe(latestCollection => {

            this.statisticsData = latestCollection;
            var data = this.statisticsData.Data;
            this.labelArray = [];
            this.dataArray = [];

            var userSum = 0;

            for (var i in data) {

                this.labelArray.push(i);
                this.dataArray.push(data[i]);
                userSum = userSum + data[i];
                if (this.category == 'All')
                    this.userYearArray.push(i);

            }

            // if (this.category == 'Year') {

            //     let barDataUsers = {

            //         labels: this.monthLabelArray,
            //         datasets: [
            //             {
            //                 label: '2010 customers #',
            //                 fillColor: '#1173cd',
            //                 data: this.dataArray
            //             }

            //         ]

            //     };


            // }
            // else {

            //     let barDataUsers = {

            //         labels: this.labelArray,
            //         datasets: [
            //             {
            //                 label: '2010 customers #',
            //                 fillColor: '#1173cd',
            //                 data: this.dataArray
            //             }

            //         ]

            //     };

            // }


            // [ERROR] charts.ts(22,28): error TS2304: Cannot find name 'Chart'. 

            // this._userCount = userSum;
            // var context = document.getElementById('clients').getContext('2d');
            // if (this.userChart)
            //     this.userChart.destroy();
            // this.userChart = new Chart(context).Bar(barDataUsers);


        });

        this.orgSub = this._statistics.statisticsOrgs$.subscribe(latestCollection => {

            this.statisticsData = latestCollection;
            var data = this.statisticsData.Data;
            this.labelArray = [];
            this.dataArray = [];
            var userSum = 0;

            for (var i in data) {

                this.labelArray.push(i);
                this.dataArray.push(data[i]);
                userSum = userSum + data[i];
                if (this.category == 'All')
                    this.orgYearArray.push(i);


            }

            // if (this.category == 'Year') {

            //     var barDataUsers = {

            //         labels: this.monthLabelArray,
            //         datasets: [
            //             {
            //                 label: '2010 customers #',
            //                 fillColor: '#5fc764',
            //                 data: this.dataArray
            //             }

            //         ]

            //     };

            // }
            // else {

            //     var barDataUsers = {

            //         labels: this.labelArray,
            //         datasets: [
            //             {
            //                 label: '2010 customers #',
            //                 fillColor: '#5fc764',
            //                 data: this.dataArray
            //             }

            //         ]

            //     };

            // }

            // [ERROR] TS2304: Cannot find name 'Chart'. 
            // this._orgCount = userSum;
            // var context = document.getElementById('oragization').getContext('2d');
            // if (this.orgChart)
            //     this.orgChart.destroy();
            // this.orgChart = new Chart(context).Bar(barDataUsers);


        });

        this.userDaySub = this._statistics.statisticsUsersByDay$.subscribe(latestCollection => {

            // this.userArray = latestCollection.Data;


        });
        this.orgDaySub = this._statistics.statisticsOrgByDay$.subscribe(latestCollection => {

            // this.orgsArray = latestCollection.Data;


        });

        this.showOrg('All', 'oragization');
        this.showUsers('All', 'clients');


    }

    ngOnDestroy(){
        if (this.orgSub) this.orgSub.unsubscribe();
        if (this.userSub) this.userSub.unsubscribe();
        if (this.userDaySub) this.userDaySub.unsubscribe();
        if (this.orgDaySub) this.orgDaySub.unsubscribe();
    }

}


