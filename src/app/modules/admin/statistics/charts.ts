import { Directive, ElementRef } from '@angular/core';
declare var Chart: any;

@Directive({
    selector: '[chart]'
})
export class ChartDirective {
    constructor(el: ElementRef) {
        //el.nativeElement.style.backgroundColor = 'yellow';
        var barData = {
            labels: ['2010', '2011', '2012', '2013', '2014', '2015', '2016'],
            datasets: [
                {
                    label: '2010 customers #',
                    fillColor: '#1173cd',
                    data: [100, 300, 500, 200, 100, 250, 700]
                }

            ]

        };

        var ctx: any = el.nativeElement.getContext("2d");
        var BarChart = new Chart(ctx);
        BarChart.Bar(barData);

    }

}