import { Inject } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Api } from '../../../services/api';

export class StatisticsService {

    public _http: any;
    public statisticsUsers$: Observable<Array<any>>;

    public statisticsUsersByDay$: Observable<Array<any>>;
    public statisticsOrg$: Observable<Array<any>>;
    public statisticsOrgs$: Observable<Array<any>>;
    public statisticsOrgByDay$: Observable<Array<any>>;
    private _statisticsUsersObserver: any;
    private _statisticsOrgObserver: any;
    private _statisticsOrgsObserver: any;
    private _statisticsUsersByDayObserver: any;
    private _statisticsOrgByDayObserver: any;
    private _statistics = {};


    constructor( @Inject(Api) _http: Api) {
        this._http = _http;

        this.statisticsUsers$ = new Observable<Array<any>>((observer: any) => {
            this._statisticsUsersObserver = observer;
        }).share();

        this.statisticsOrgs$ = new Observable<Array<any>>((observer: any) => {
            this._statisticsOrgsObserver = observer;
        }).share();

        this.statisticsOrg$ = new Observable<Array<any>>((observer: any) => {
            this._statisticsOrgObserver = observer;
        }).share();

        this.statisticsUsersByDay$ = new Observable<Array<any>>((observer: any) => {
            this._statisticsUsersByDayObserver = observer;
        }).share();

        this.statisticsOrgByDay$ = new Observable<Array<any>>((observer: any) => {
            this._statisticsOrgByDayObserver = observer;
        }).share();


    }


    getAllUsers() {

        this._http.get('SuperAdmin/Activity/RegisteredUsers')
            .map((res: any) => res.json())
            .subscribe((res: any) => {
                this._statistics = res;
                this._statisticsUsersObserver.next(this._statistics);
            }, (err: any) => {
                console.log('error 77');
            });
    }

    getUsersByFilter(filter: any) {

        var url = '';

        if (filter.user_year != 'Year') {
            url = '/' + filter.user_year;
        }
        if (filter.user_month != 'Month' && filter.user_month != 0) {

            url = url + '/' + filter.user_month;
        }

        this._http.get('SuperAdmin/Activity/RegisteredUsers' + url)
            .map((res: any) => res.json())
            .subscribe((res: any) => {
                this._statistics = res;
                this._statisticsUsersObserver.next(this._statistics);
            }, (err: any) => {
                console.log('error 77');
            });
    }

    getUsersByDayFilter(filter: any) {

        var url = '';

        if (filter.user_year != 'Year') {
            url = '/' + filter.user_year;
        }
        if (filter.user_month != 'Month' && filter.user_month != 0) {
            url = url + '/' + filter.user_month;
        }
        if (filter.user_day != 'Day' && filter.user_day != 0) {
            url = url + '/' + filter.user_day;
        }

        this._http.get('SuperAdmin/Activity/RegisteredUsers' + url)
            .map((res: any) => res.json())
            .subscribe((res: any) => {
                this._statistics = res;
                this._statisticsUsersByDayObserver.next(this._statistics);
            }, (err: any) => {
                console.log('error 77');
            });

    }

    getAllOrganizations() {
        this._http.get('SuperAdmin/Activity/RegisteredOrganisations')
            .map((res: any) => res.json())
            .subscribe((res: any) => {
                this._statistics = res;
                this._statisticsOrgsObserver.next(this._statistics);
            }, (err: any) => {
                console.log('error 77');
            });
    }

    getOrgByFilter(filter: any) {

        var url = '';

        if (filter.org_year != 'Year') {
            url = '/' + filter.org_year;
        }
        if (filter.org_month != 'Month' && filter.org_month != 0) {
            url = url + '/' + filter.org_month;
        }
        if (filter.org_day != 'Day' && filter.org_day != 0) {
            url = url + '/' + filter.org_day;
        }

        this._http.get('SuperAdmin/Activity/RegisteredOrganisations' + url)
            .map((res: any) => res.json())
            .subscribe((res: any) => {
                this._statistics = res;
                this._statisticsOrgsObserver.next(this._statistics);
            }, (err: any) => {
                console.log('error 77');
            });
    }

    getOrgByDayFilter(filter: any) {

        var url = '';

        if (filter.org_year != 'Year') {
            url = '/' + filter.org_year;
        }
        if (filter.org_month != 'Month' && filter.org_month != 0) {
            url = url + '/' + filter.org_month;
        }
        if (filter.org_day != 'Day') {
            url = url + '/' + filter.org_day;
        }



        this._http.get('SuperAdmin/Activity/RegisteredOrganisations' + url)
            .map((res: any) => res.json())
            .subscribe((res: any) => {
                this._statistics = res;
                this._statisticsOrgByDayObserver.next(this._statistics);
            }, (err: any) => {
                console.log('error 77');
            });

    }


}
