import { Component } from '@angular/core'
import { Api } from '../../../services/api';

@Component({

    styleUrls: ['./admin-packages.component.css'],
    templateUrl: 'admin-packages.component.html'
})

export class AdminPackagesComponent {

    public packages: Array<any> = [];
    public features: Array<any> = [];

    constructor(private api: Api) {
        this.api.get("Packages").map(res => res.json()).subscribe(res => {
            this.packages = res.Data;
        })
        this.api.get("Features").map(res => res.json()).subscribe(res => {
            this.features = res.Data;
        })
    }

    public newPackage() {
        this.packages.push({ NameLabel: "NEW" })
    }

}