import { Component, Input } from '@angular/core'
import { Api } from '../../../services/api';

@Component({
    selector: 'edit-package',
    styleUrls: ['./admin-packages.component.css'],
    templateUrl: 'edit-package.component.html'
})

export class EditPackageComponent {

    @Input() package: any;
    @Input() features: any;

    public editInfo: boolean = false;
    public editPrice: boolean = false;
    public editFeatures: boolean = false;

    constructor(private api: Api) {

    }

    public save() {
        if (this.package.Id) {
            this.api.put("Packages/" + this.package.Id, this.package).map(res => res.json()).subscribe(res => {
                this.package = res.Data;
                close();
            });
        }
        else {
            this.api.post("Packages", this.package).map(res => res.json()).subscribe(res => {
                this.package = res.Data;
                close();
            });
        }
    }
}