import { Component, Input } from '@angular/core'
import { Api } from '../../../../services/api';

@Component({
    selector: 'edit-feature',
    styleUrls: ['../admin-packages.component.css'],
    templateUrl: './edit-feature.coponent.html'
})

export class EditFeatureComponent {

    @Input() feature: any;

    public editInfo: boolean = false;
    public editPrice: boolean = false;

    constructor(private api: Api) {

    }

    public save() {
        this.api.put("Features/" + this.feature.Id, this.feature).map(res => res.json()).subscribe(res => {
            this.feature = res.Data;
            this.editInfo = false;
            this.editPrice = false;
        })
    }

}