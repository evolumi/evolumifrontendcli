import { Component, Input } from '@angular/core'

@Component({

    selector: 'feature-list',
    styleUrls: ['../admin-packages.component.css'],
    templateUrl: './admin-features.component.html'
})

export class AdminFeaturesComponent {

    @Input() features: Array<any>;

    constructor() {

    }
}