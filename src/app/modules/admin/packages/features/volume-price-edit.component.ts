import { Component, Input, Output, EventEmitter } from '@angular/core'
import { Configuration } from '../../../../services/configuration';
import { Feature, VolumePrice } from "../../../../services/packages.service";

@Component({
    selector: 'volume-price-edit',

    styleUrls: ['../admin-packages.component.css'],
    templateUrl: './volume-price-edit.component.html'
})
export class EditVolumePricesComponent {

    @Input() feature: Feature;
    @Output() update: EventEmitter<Feature> = new EventEmitter();

    public currencies: Array<string> = [];
    public selectedList: Array<VolumePrice>;
    public currency: string = "";

    constructor(private config: Configuration) { }

    ngOnInit() {
        this.currencies = this.config.currencies;
        this.currency = "SEK";
    }

    ngOnChanges(changes: any) {
        this.changeCurrency();
    }

    public changeCurrency() {
        if (this.feature) {
            if (!this.feature.VolumePricing[this.currency]) this.feature.VolumePricing[this.currency] = [];
            this.selectedList = this.feature.VolumePricing[this.currency];
        }
    }

    public newPrice() {
        this.selectedList.push(new VolumePrice());
    }

    public save() {
        this.update.next(this.feature);
    }
}