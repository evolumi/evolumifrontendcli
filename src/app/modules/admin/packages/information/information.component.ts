import { Component, Input, Output, EventEmitter } from '@angular/core'

@Component({

    selector: 'edit-info',
    styleUrls: ['../admin-packages.component.css'],
    templateUrl: 'information.component.html'
})

export class EditInformationComponent {

    @Input() information: Array<any> = [];
    @Output() update: EventEmitter<any> = new EventEmitter();

    public save() {
        this.update.next(this.information);
    }

    public add() {
        this.information.push({ Label: "", ImageUrl: "", VideoUrl: "", Link: "" });
    }

    public remove(index: number) {
        this.information.splice(index, 1);
    }

}