import { Component, Input, Output, EventEmitter } from '@angular/core'
import { Configuration } from '../../../../services/configuration';

@Component({

    selector: 'edit-price',
    styleUrls: ['../admin-packages.component.css'],
    templateUrl: 'price.component.html'
})

export class PriceComponent {

    @Input() package: any;
    @Output() update: EventEmitter<any> = new EventEmitter();

    public currencies: Array<string> = [];

    constructor(private config: Configuration) {
    }

    ngOnInit() {
        this.currencies = this.config.currencies;
    }

    public save() {
        this.update.next(this.package);
    }
}