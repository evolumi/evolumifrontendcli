import { Component, Input, Output, EventEmitter } from '@angular/core'
import { ChosenOption } from "../../../shared/components/chosen/chosen";

@Component({

    selector: 'feature-select',
    styleUrls: ['../admin-packages.component.css'],
    templateUrl: 'feature-select.component.html'
})

export class FeatureSelectComponent {

    @Input() package: any;
    @Input() features: Array<any> = [];
    @Output() update: EventEmitter<any> = new EventEmitter();

    public featureList: Array<ChosenOption> = [];

    ngOnChanges(changes: any) {
        this.featureList = [];
        for (let f of this.features) {
            this.featureList.push({ value: f.Name, label: f.Name })
        }
    }

    public save() {
        this.update.next(this.package);
    }
}