import { Component } from '@angular/core'
import { Api } from '../../../services/api';
import { Observable } from 'rxjs/Observable';

@Component({
    templateUrl: 'activity.component.html'
})
export class ActivityComponent {

    private timestamp: number;
    public activities: Array<any>;

    public summary: any;
    private obs: any;

    constructor(private api: Api) {

        this.timestamp = 0;
        this.activities = [];

        this.api.get("Reports/ActivitySummary").map(res => res.json()).subscribe(res => {
            if(res.IsSuccess){
                this.summary = res.Data;
            }
        })

        this.api.get("Reports/Activity?Timestamp=" + this.timestamp + "&PageSize=200").map(res => res.json()).subscribe(res => {
            if (res.IsSuccess) {
                res.Data.reverse()
                for (let act of res.Data) {
                    this.activities.unshift(act);
                }
                this.timestamp = this.activities[0].Date;
                console.log("Setting Timestap:" + this.timestamp)

                this.obs = Observable.interval(5000).subscribe(x => {
                    this.api.get("Reports/Activity?Timestamp=" + this.timestamp + "&PageSize=100").map(res => res.json()).subscribe(res => {
                        if (res.IsSuccess) {
                            for (let act of res.Data) {
                                this.activities.unshift(act);
                            }
                            if(this.activities.length > 200)
                            this.activities.splice(200, this.activities.length);
                            this.timestamp = this.activities[0].Date;
                        }
                    });
                });
            }
        });
    }

    ngOnDestroy() {
        if(this.obs) this.obs.unsubscribe();
    }

}