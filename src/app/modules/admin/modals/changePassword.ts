import { Component, ElementRef } from '@angular/core';
import { AuthService } from '../../../services/auth-service';
import { NotificationService } from '../../../services/notification-service';
import { Api } from '../../../services/api';

@Component({
    
    selector: 'change-password',
    template: ` 
<div class="customPopup systemActive" id="changePassword" (click)="clickToClose($event)">
    <div class="popupBox"> 
        <div class="title">{{userName}}<a class="close pull-right" (click)="cancel()"><i class="fa fa-times"></i></a></div>
        <div class="form-group">
            <label>Change password for this user</label>
            <input id="NewPassword" type="text" class="form-control" placeholder="Password" [(ngModel)]="password">
        </div>
        <button id="ChangePasswordButton" (click)="updatePassword()" class="btn pull-right" [disabled]="!password">Change</button>

        <div class="form-group">
            <label>Or loggin as this user</label>
            <button id="LoginAsButton" (click)="loginUnderUser(userId)" class="btn pull-right">Login</button>            
        </div>
    </div>
</div>
                `,
})

export class AdminChangePasswordModal {

    el: HTMLElement;
    public userId: string;
    public userName: string;
    public password: string;

    constructor(
        private api: Api,
        public element: ElementRef,
        private notify: NotificationService,
        private auth: AuthService
    ) {

    }

    ngOnInit() {
        console.log('EL', this.el);
        this.el = this.element.nativeElement;
        this.el.style.display = 'none';
        // $(this.el).hide();
    }

    public updatePassword() {

        this.api.get("SuperAdmin/ChangePasswordOnUser/" + this.userId + "/" + this.password).map(res => res.json()).subscribe(res => {
            if (res.IsSuccess) {
                this.notify.success("Updated Password");
            }
            else {
                this.notify.error("Error uppdateing password");
            }
        });

        this.cancel();
    }

    loginUnderUser(id: string) {
        console.log("[Admin] Loggin in as User: " + this.userName);
        this.auth.AdminLogin(id);
    }

    public showModal(userId: string = '', userName: string) {
        this.userId = userId;
        this.userName = userName;
        Array.from(document.querySelectorAll('sidePopup'), (x: HTMLElement) => x.style.overflow = 'visible');
        Array.from(document.querySelectorAll('contentPanel'), (x: HTMLElement) => x.style.overflow = 'visible');

        // $(".sidePopup").css('overflow', 'visible');
        // $(".contentPanel").css('overflow', 'visible');
        this.el.style.display = 'block';
        // $(this.el).show();
    }



    clickToClose(event: any) {
        var target = event.target || event.srcElement;
        if (target.id === 'changePassword') {
            this.cancel();
        }
    }

    public cancel() {

        this.password = "";

        this.el.style.display = 'none';
        // $(this.el).hide();
        Array.from(document.querySelectorAll('sidePopup'), (x: HTMLElement) => x.style.overflow = 'auto');
        Array.from(document.querySelectorAll('contentPanel'), (x: HTMLElement) => x.style.overflow = 'auto');
        // $(".sidePopup").css('overflow', 'auto');
        // $(".contentPanel").css('overflow', 'auto');

    }

    ngOnDestroy() {

    }
}