import { Inject } from '@angular/core';
import { Api } from '../../../services/api';
import { Observable } from 'rxjs/Observable';
import { LoaderService } from '../../../services/loader.service';
export class SponsorsService {
    public _http: any;
    public sponsors$: Observable<Array<string>>;
    public activeAgenda$: Observable<Array<string>>;
    public todaysAgenda$: Observable<Array<string>>;
    private _sponsorsObserver: any;
    private _sponsors: Array<string>;
    private _activeAgendaObserver: any;
    //private _activeAgenda: Array<string>;
    private _todaysAgendaObserver: any;
    private _sponsorss = {};
    private _activeAgenda = {};



    constructor( @Inject(Api) _http: Api, @Inject(LoaderService) public _load: LoaderService) {
        this._http = _http;
        this._sponsors = new Array;
        this.sponsors$ = new Observable<Array<string>>((observer: any) => {
            this._sponsorsObserver = observer;
        }).share();
        this._activeAgenda = new Array;
        this.activeAgenda$ = new Observable<Array<string>>((observer: any) => {
            this._activeAgendaObserver = observer;
        }).share();
        this.todaysAgenda$ = new Observable<Array<string>>((observer: any) => {
            this._todaysAgendaObserver = observer;
        }).share();
    }

    getActiveAgenda() {
        this._http.get('Sponsors/ActiveAgenda')
            .map((res: any) => res.json())
            .subscribe((res: any) => {
                this._activeAgenda = res;
                this._activeAgendaObserver.next(this._activeAgenda);


            }, (err: any) => {
                console.log('error 77');
            });
    }
    addSponsor(sponsordata: any) {
        this._http.post('Sponsors', sponsordata)
            .map((res: any) => res.json())
            .subscribe((res: any) => {
                if (res.IsSuccess) {
                    return true;
                } else {
                    alert('error');
                    return false;
                }
            });
    }

    getSponsors() {

        this._http.get('Sponsors')
            .map((res: any) => res.json())
            .subscribe((res: any) => {
                if (res.IsSuccess === true) {
                    this._sponsors = res.Data;
                    this._sponsorsObserver.next(this._sponsors);
                    return true;
                } else { return false; }
            }, (err: any) => {
                console.log('error 77');
            },
            (com: any) => {
            });


    }
    addDays(daysdata: any) {
        return this._http.post('Sponsors/AddMoreDays', daysdata)
            .map((res: any) => res.json())
            .subscribe((res: any) => {
                if (res.IsSuccess) {
                    this.getSponsors();
                    return true;
                } else {
                    console.log('error');
                    return false;
                }
            });
    }
    deleteAd(Id: string) {

        return this._http.delete('Sponsors/' + Id)
            .map(function (res: any) { return res.json(); })
            .subscribe((res: any) => {
                if (res.IsSuccess) {

                    this.getSponsors();
                    return true;
                } else {
                    console.log('error');
                    return false;
                }
            });
    }

    deleteAgenda(Id: string) {

        this._http.delete('Sponsors/Agenda/' + Id)
            .map(function (res: any) { return res.json(); })
            .subscribe((res: any) => {
                if (res.IsSuccess) {
                    this.getActiveAgenda();
                } else {
                    console.log('error');
                }
            });

    }
    getActiveAgenda1() {
        this._http.get('Sponsors/ActiveAgenda')
            .map((res: any) => res.json())
            .subscribe((res: any) => {
                let data: Array<any> = [];
                if (res.IsSuccess === true) {
                    for (var a of res.Data) {
                        data.push(a);
                    }
                    this._activeAgenda = data;
                    this._activeAgendaObserver.next(this._activeAgenda);
                    return true;
                } else { return false; }
            });
    }


    addSchedule(daysdata: any) {
        return this._http.post('Sponsors/Agenda', daysdata)
            .map((res: any) => res.json())
            .subscribe((res: any) => {
                if (res.IsSuccess) {
                    this.getActiveAgenda();
                } else {
                    console.log('error');
                }
            });
    }
    reSchedule(daysdata: any) {
        this._http.put('Sponsors/Agenda', daysdata)
            .map((res: any) => res.json())
            .subscribe((res: any) => {
                if (res.IsSuccess) {
                    this.getActiveAgenda();
                } else {
                    console.log('error');
                }
            });
    }

    todaysAgenda() {

        this._http.get('Sponsors/TodayAgenda')
            .map((res: any) => res.json())
            .subscribe((res: any) => {
                this._sponsorss = res;
                this._activeAgendaObserver.next(this._sponsorss);
            }, (err: any) => {
                console.log('error 77');
            });



    }
}
