import { Component, Input, OnInit } from '@angular/core';
import { SponsorsService } from './sponsors.service';
import { Subscription } from "rxjs/Subscription";

@Component({
	selector: 'ad-list',
	templateUrl: './adList.html'
})
export class AdListComponent implements OnInit {
	@Input() adInfo: any;
	@Input() mainpage: any;
	public sponsors: any;
	public sponsorsArray: Array<any> = [];
	/*public activeAgenda;
    public activeAgendaArray: Array<any> = [];*/
	public activeArray: Array<any> = [];
	public dateList: any;
	public agendaDateList: Array<any> = [];
	public sponsorItem: any = {};
	public days = [1, 2, 3, 4, 5, 6];

	private sponsorSub: Subscription;
	private agendaSub: Subscription;

	constructor(public _sponsors: SponsorsService) {
		this._sponsors = _sponsors;
	}
	ngOnInit() {

		var startDate = new Date();
		var endDate = new Date();
		endDate.setDate(endDate.getDate() + 45);
		this.dateList = this.betweenDate(startDate, endDate);
		this.sponsorSub = this._sponsors.sponsors$.subscribe(latest => {
			this.sponsorsArray = latest;
		});
		this.agendaSub = this._sponsors.activeAgenda$.subscribe((latest: any) => {
			this.activeArray = latest.Data;

			for (var i = 0; i < this.dateList.length; i++) {

				this.agendaDateList[i] = [];
				this.agendaDateList[i].objIndex = -1;
				this.agendaDateList[i].objId = 0;
				this.agendaDateList[i].date = this.dateList[i];
				this.agendaDateList[i].showEditAgenda = false;
				for (var j = 0; j < this.activeArray.length; j++) {

					if (this.agendaDateList[i].objIndex === -1) {

						if (this.stringAsDate(this.activeArray[j].Date).toDateString() === this.dateList[i].toDateString()) {

							this.agendaDateList[i].objIndex = j;
							this.agendaDateList[i].objId = this.activeArray[j].Id;
						}
					}
				}
			}
		});

		this._sponsors.getActiveAgenda();
		this.getSponsors();
	}

	ngOnDestroy(){
		if (this.sponsorSub) this.sponsorSub.unsubscribe();
		if (this.agendaSub) this.agendaSub.unsubscribe();
	}

	getSponsors() {
		this._sponsors.getSponsors();
	}
	addDays(index: any) {
		this.sponsorItem.SponsorId = this.sponsorsArray[index].Id;
		var added = this._sponsors.addDays(this.sponsorItem);
		if (added) {
			this._sponsors.getSponsors();
			this.disablePop(index, 'days');
		}
	}
	deleteAd(Id: any) {

		var conf = confirm('Do you want to remove this Ad?');

		if (conf) {
			this._sponsors.deleteAd(Id);
		}


	}
	deleteAgenda(Id: any) {

		var conf = confirm('Do you want to remove this Agenda?');
		if (conf) {
			this._sponsors.deleteAgenda(Id);
			this._sponsors.getActiveAgenda();
		}

	}

	enablePop(index: any, showElement: any) {
		if (showElement === 'days') {
			this.sponsorsArray[index].showDays = true;
		} else if (showElement === 'schedule') {
			this.sponsorsArray[index].showSchedule = true;
		}
	}
	disablePop(index: any, showElement: any) {
		if (showElement === 'days') {
			this.sponsorsArray[index].showDays = false;
		} else if (showElement === 'schedule') {
			this.sponsorsArray[index].showSchedule = false;
		}
	}
	stringAsDate(dateStr: any) {
		return new Date(dateStr);
	}
	isDate(dateArg: any) {
		var t = (dateArg instanceof Date) ? dateArg : (new Date(dateArg));
		return !isNaN(t.valueOf());
	}
	isValidRange(minDate: any, maxDate: any) {
		return (new Date(minDate) <= new Date(maxDate));
	}
	betweenDate(startDt: any, endDt: any) {
		var error = ((this.isDate(endDt)) && (this.isDate(startDt)) && this.isValidRange(startDt, endDt)) ? false : true;
		var between: Array<any> = [];
		if (error) {
			console.log('error occured!!!... Please Enter Valid Dates');
		} else {
			var currentDate = new Date(startDt),
				end = new Date(endDt);
			while (currentDate <= end) {
				between.push(new Date(currentDate));
				currentDate.setDate(currentDate.getDate() + 1);
			}
		}
		return between;
	}

	addSchedule(Id: any, agenda_date: any) {
		this.sponsorItem = {};
		this.sponsorItem.SponsorId = Id;
		this.sponsorItem.Date = agenda_date;
		this._sponsors.addSchedule(this.sponsorItem);
	}
	reSchedule(oldId: any, Id: any, agenda_date: any) {
		this.sponsorItem = {};
		this.sponsorItem.OldSponsorId = oldId;
		this.sponsorItem.SponsorId = Id;
		this.sponsorItem.Date = 'agenda_date';
		this._sponsors.reSchedule(this.sponsorItem);
	}
}
