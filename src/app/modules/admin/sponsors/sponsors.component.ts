import { Component } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Api } from '../../../services/api';
import { SponsorsService } from './sponsors.service';
import { NotificationService } from '../../../services/notification-service';
import { Subscription } from "rxjs/Subscription";
declare var $: JQueryStatic

@Component({
	templateUrl: 'sponsors.html'
})
export class SponsorsComponent {
	public src = 'assets/images/avatar.png';
	public title = 'Sponsor Form';
	public activeAgenda: any;
	public activeAgendaArray: {};
	public sponsor = { NumberOfDays: '', Title: '', Link: '', Description: '' };
	public days = [1, 2, 3, 4, 5, 6];
	public upItem: any;
	public addSponsorForm: FormGroup;
	public fileSelected: any;
	private agendaSub: Subscription;

	constructor(private _http: Api,
		public _router: Router,
		public _sponsors: SponsorsService,
		form: FormBuilder, private _notify: NotificationService) {
		this._notify = _notify;
		this.src = 'assets/images/avatar.png';
		this.addSponsorForm = form.group({
			Title: ['', Validators.required],
			Link: ['', Validators.required],
			Description: ['', Validators.required],
			NumberOfDays: []
		});
	}
	ngOnInit() {
		this.sponsor.NumberOfDays = '1';
		this.agendaSub = this._sponsors.activeAgenda$.subscribe(latest => {
			this.activeAgendaArray = latest;
		});
		//this._sponsors.getActiveAgenda();
	}

	ngOnDestroy(){
		if (this.agendaSub) this.agendaSub.unsubscribe();
	}

	onSubmit() {
		var formData = new FormData();
		formData.append('Title', this.addSponsorForm.value.Title);
		formData.append('Link', this.addSponsorForm.value.Link);
		formData.append('Description', this.addSponsorForm.value.Description);
		formData.append('NumberOfDays', this.addSponsorForm.value.NumberOfDays);
		formData.append('file', (<any>$('#file')[0]).files[0]);
		this._http.upload('Sponsors', formData)
			.then(res => {

				this._sponsors.getSponsors()
				//this._notify.success('Sponsor Ad created successfully');

			})
	}

}
