import { Component, Input } from '@angular/core';
import { SponsorsService } from './sponsors.service';
import { Subscription } from "rxjs/Subscription";

@Component({
	selector: 'agenda',
	templateUrl: 'agenda.html'
})

export class AgendaComponent {
	@Input() adInfo: any;
	@Input() mainpage: any;
	public activeAgenda: any;
	public activeAgendaArray: Array<any> = [];
	public activeArray: Array<any> = [];
	public dateList: any;
	public sponsorsArray: any;
	public agendaDateList: Array<any> = [];
	public sponsorItem: any = {};

	private sponsorSub: Subscription;
	private agendaSub: Subscription;

	constructor(public _sponsors: SponsorsService) {
		this._sponsors = _sponsors;
	}
	ngOnInit() {
		var startDate = new Date();
		var endDate: number | Date = new Date();
		endDate = endDate.setDate(endDate.getDate() + 45);
		this.dateList = this.betweenDate(startDate, endDate);
		this.sponsorSub = this._sponsors.sponsors$.subscribe(latest => {
			this.sponsorsArray = latest;
		});
		this.agendaSub = this._sponsors.activeAgenda$.subscribe((latest: any) => {
			this.activeArray = latest.Data;

			for (var i = 0; i < this.dateList.length; i++) {

				this.agendaDateList[i] = [];
				this.agendaDateList[i].objIndex = -1;
				this.agendaDateList[i].objId = 0;
				this.agendaDateList[i].date = this.dateList[i];
				this.agendaDateList[i].showEditAgenda = false;
				for (var j = 0; j < this.activeArray.length; j++) {

					if (this.agendaDateList[i].objIndex === -1) {
						if (this.stringAsDate(this.activeArray[j].Date).toDateString() === this.dateList[i].toDateString()) {
							this.agendaDateList[i].objIndex = j;
							this.agendaDateList[i].objId = this.activeArray[j].Id;

						}
					}
				}
			}


		});
		this._sponsors.getSponsors();
		this._sponsors.getActiveAgenda();

	}

	ngOnDestroy(){
		if (this.sponsorSub) this.sponsorSub.unsubscribe();
		if (this.agendaSub) this.agendaSub.unsubscribe();
	}

	getActiveAgenda() {

		this._sponsors.getActiveAgenda();

	}

	deleteAgenda(Id: string) {

		var conf = confirm('Do you want to remove this Agenda?');
		if (conf) {
			this._sponsors.deleteAgenda(Id);
			this._sponsors.getActiveAgenda();
		}

	}

	addSchedule(Id: string, agenda_date: any) {

		this.sponsorItem = {};
		this.sponsorItem.SponsorId = Id;
		this.sponsorItem.Date = agenda_date;
		var added = this._sponsors.addSchedule(this.sponsorItem);
		if (added) {
			//this.getActiveAgenda();
		}

	}
	reSchedule(oldId: string, Id: string, agenda_date: any) {
		this.sponsorItem = {};
		this.sponsorItem.OldSponsorId = oldId;
		this.sponsorItem.SponsorId = Id;
		this.sponsorItem.Date = agenda_date;
		this._sponsors.reSchedule(this.sponsorItem);

	}
	stringAsDate(dateStr: string) {
		return new Date(dateStr);
	}
	isDate(dateArg: any) {
		var t = (dateArg instanceof Date) ? dateArg : (new Date(dateArg));
		return !isNaN(t.valueOf());
	}
	isValidRange(minDate: any, maxDate: any) {
		return (new Date(minDate) <= new Date(maxDate));
	}

	betweenDate(startDt: any, endDt: any) {
		var error = ((this.isDate(endDt)) && (this.isDate(startDt)) && this.isValidRange(startDt, endDt)) ? false : true;
		var between: any = [];
		if (error) {
			console.log('error occured!!!... Please Enter Valid Dates');
		} else {
			var currentDate = new Date(startDt),
				end = new Date(endDt);
			while (currentDate <= end) {
				between.push(new Date(currentDate));
				currentDate.setDate(currentDate.getDate() + 1);
			}
		}
		return between;
	}
	enablePop(index: number, showElement: any) {
		if (showElement === 'editAgenda') {
			this.agendaDateList[index].showEditAgenda = true;
		}
	}
	disablePop(index: number, showElement: any) {
		if (showElement === 'editAgenda') {
			this.agendaDateList[index].showEditAgenda = false;
		}
	}
}
