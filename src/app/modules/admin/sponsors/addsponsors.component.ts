import { Component } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Api } from '../../../services/api';
import { SponsorsService } from './sponsors.service';
import { NotificationService } from '../../../services/notification-service';
declare var $: JQueryStatic;

@Component({
    templateUrl: './addsponsor.html',
})
export class AddSponsorComponent {

    public src = 'assets/images/avatar.png';
    public title = 'Sponsor Form';
    public sponsor = { NumberOfDays: '', Link: '', Description: '', Title: '' };
    public days = [1, 2, 3, 4, 5, 6];
    public upItem: any;
    public addSponsorForm: FormGroup;
    public fileSelected: any;

    constructor(private _http: Api,
        public _router: Router,
        public _sponsors: SponsorsService,
        form: FormBuilder,
        private _notify: NotificationService) {

        this.addSponsorForm = form.group({
            Title: ['', Validators.required],
            Link: ['', Validators.required],
            Description: ['', Validators.required],
            NumberOfDays: []
        });
    }

    ngOnInit() {
        this.sponsor.NumberOfDays = '1';
    }

    public submit() {
        var formData = new FormData();
        formData.append('Title', this.addSponsorForm.value.Title);
        formData.append('Link', this.addSponsorForm.value.Link);
        formData.append('Description', this.addSponsorForm.value.Description);
        formData.append('NumberOfDays', this.addSponsorForm.value.NumberOfDays);
        formData.append('file', (<any>$('#file')[0]).files[0]);
        this._http.upload('Sponsors', formData)
            .then(res => {

                this._sponsors.getSponsors()
                this._notify.success('Sponsor Ad created successfully');

            })
    }

}