import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EvolumiSuperAdminGuard } from '../../services/guards/evolumi-guard';
import { AuthGuard } from '../../services/guards/auth-guard';

// Components
import { AdminComponent } from './admin.component';
import { UsersComponent } from './users/users.component';
import { SponsorsComponent } from './sponsors/sponsors.component';
import { StatisticsComponent } from './statistics/statistics.component';
import { AddSponsorComponent } from './sponsors/addsponsors.component';
import { ActivityComponent } from './activity/activity.component';
import { AdminFeaturesComponent } from './packages/features/admin-features.component';
import { AdminPackagesComponent } from './packages/admin-packages.component';
import { ReferCodeComponent } from './refercode/refercode.component';

export const AdminRoutes: Routes = [
  {
    path: 'Admin',
    component: AdminComponent,
    canActivate: [AuthGuard, EvolumiSuperAdminGuard],
    children: [
      { path: '', redirectTo: 'Users', pathMatch: 'full' },
      { path: 'Users', component: UsersComponent },
      { path: 'Sponsors', component: SponsorsComponent },
      { path: 'Statistics', component: StatisticsComponent },
      { path: 'AddSponsor', component: AddSponsorComponent },
      { path: 'Activity', component: ActivityComponent },
      { path: 'Packages', component: AdminPackagesComponent },
      { path: 'Features', component: AdminFeaturesComponent },
      { path: 'ReferCode', component: ReferCodeComponent }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(AdminRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AdminRouteModule { }