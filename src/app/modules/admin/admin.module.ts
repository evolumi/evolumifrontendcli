import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { SharedModule } from '../shared/shared.module';
//import { AdminRouteModule} from './admin-routes.module';

// Components
import { AdminComponent } from './admin.component';
import { Headerbar } from './header/header.component';
import { AdminChangePasswordModal } from './modals/changePassword';
import { AddSponsorComponent } from './sponsors/addsponsors.component';
import { AdListComponent } from './sponsors/adList.component';
import { AgendaComponent } from './sponsors/agenda.component';
import { SponsorsComponent } from './sponsors/sponsors.component';
import { StatisticsComponent } from './statistics/statistics.component';
import { UsersComponent } from './users/users.component';
import { ActivityComponent } from './activity/activity.component';
import { AdminPackagesComponent } from './packages/admin-packages.component';
import { EditPackageComponent } from './packages/edit-package.component';
import { AdminFeaturesComponent } from './packages/features/admin-features.component';
import { EditFeatureComponent } from './packages/features/edit-feature.component';
import { EditInformationComponent } from './packages/information/information.component';
import { PriceComponent } from './packages/information/price.component';
import { FeatureSelectComponent } from './packages/information/feature-select.component';
import { ReferCodeComponent } from './refercode/refercode.component';
import { EditVolumePricesComponent } from './packages/features/volume-price-edit.component'

// Directives
import { ChartDirective } from './statistics/charts';

// Services
import { SponsorsService } from './sponsors/sponsors.service';
import { StatisticsService } from './statistics/statistics.service';
import { AdminUsersService } from './users/admin.users.service';

@NgModule({
    imports: [
        //AdminRouteModule,
        FormsModule,
        HttpModule,
        CommonModule,
        SharedModule
    ],
    declarations: [
        AdminComponent,
        Headerbar,
        AdminChangePasswordModal,
        AddSponsorComponent,
        AdListComponent,
        AgendaComponent,
        SponsorsComponent,
        StatisticsComponent,
        UsersComponent,
        ChartDirective,
        ActivityComponent,
        AdminPackagesComponent,
        EditPackageComponent,
        AdminFeaturesComponent,
        EditFeatureComponent,
        EditInformationComponent,
        PriceComponent,
        FeatureSelectComponent,
        ReferCodeComponent,
        EditVolumePricesComponent
    ],
    providers: [
        SponsorsService,
        StatisticsService,
        AdminUsersService
    ]
})
export class AdminModule { }