import { Component, OnInit } from '@angular/core';
import { AdminUsersService } from './admin.users.service';
import { AdminUser, AdminOrganization } from './admin.users.model';
import { Api } from '../../../services/api';
import { OrgService } from '../../../services/organisation.service';
import { PackagesCollection, Package, AdminAddPackageModel } from '../../../services/packages.service';
import { Subscription } from 'rxjs/Subscription';
import { UsersService } from '../../../services/users.service';
import { ChosenOptionModel } from '../../shared/components/chosen/chosen.models';
import { Configuration } from '../../../services/configuration';

@Component({

    templateUrl: 'users.html',
    styleUrls: ['users.css'],
})
export class UsersComponent implements OnInit {

    private userSub: any;
    private orgSub: any;

    public userfilterForm: any = {
        UserName: '',
        UserEmail: '',
        UserDateCreated: '',
        UserLastLogin: ''
    };

    public orgfilterForm: any = {
        OrgName: '',
        CreatedBy: '',
    };

    public filterLastLogin: any;
    public users: Array<AdminUser> = [];
    public usersnew: Array<AdminUser>;
    public orgs: Array<AdminOrganization> = [];
    public orgsnew: Array<AdminOrganization>;
    public apiProgress: any;
    public page: number;
    public org_page: number;
    public pageSize: number = 10;//items per load
    public org_pageSize: number = 10;//items per load

    public userDateCreated: any;
    public userDateCreatedEnd: any;
    public lastLoginEnd: any;
    public lastLoginStart: any;
    public clsTag: boolean;
    public clsTagOrg: boolean;
    public filterStartDate: any;
    public myDatePickerOptions: any;
    public select = "Select";

    public packageSub: Subscription;
    public packageList: Package[] = [];
    public packageNameLib: any;
    public adminAddPackage: AdminAddPackageModel = new AdminAddPackageModel();
    public selectedPackage: Package;
    public today: Date = new Date();

    public currencyList: string[] = [];

    public avalibleUsers: ChosenOptionModel[] = [];
    public orgUserSub: Subscription;
    public orgUsers: ChosenOptionModel[] = [];
    public addedUsers: ChosenOptionModel[] = [];

    public optionsPopup: AdminOrganization;
    public editPackMode: boolean = false;

    constructor(
        public _adminUserService: AdminUsersService,
        public _http: Api,
        public orgSvc: OrgService,
        public packageSvc: PackagesCollection,
        public userSvc: UsersService,
        public configSvc: Configuration
    ) {

        this.page = 1;
        this.org_page = 1;
        this.clsTag = true;
        this.clsTagOrg = true;
        this.filterStartDate = null; //new DatePipe().transform(new Date(), ['dd/MM/yyyy']).replace(/\//g, '.');
        this.filterLastLogin = null; //new DatePipe().transform(new Date(), ['dd/MM/yyyy']).replace(/\//g, '.');

        this.myDatePickerOptions = {
            todayBtnTxt: 'Today',
            dateFormat: 'mm.dd.yyyy',
            firstDayOfWeek: 'mo',
            sunHighlight: true,
            height: '33px',
            width: '100%'
        };

        this.userSub = this._adminUserService.admin_users$.subscribe(_admin_users => {
            this.usersnew = _admin_users;
            if (this.clsTag) {
                this.users = _admin_users;
            }
            else {
                this.users = this.users.concat(this.usersnew);
            }
        });

        this.orgSub = this._adminUserService.admin_organizations$.subscribe(_admin_orgs => {
            this.orgsnew = _admin_orgs;
            if (this.clsTagOrg) {
                this.orgs = _admin_orgs;
            }
            else {
                this.orgs = this.orgs.concat(this.orgsnew);
            }
        });
    }

    ngOnInit() {
        this.getAdminUsers();
        this.getAdminOrganisations();
        this.getPackageList();
        this.adminAddPackage.Licences = 0;
        this.currencyList = this.configSvc.currencies;
        this.orgUserSub = this.userSvc.userListObervable().subscribe(data => this.orgUsers = this.avalibleUsers = data);
        //document.getElementById('users').('scroll', )
    }

    getPackageList() {
        this.packageSub = this.packageSvc.getAllPackages().subscribe(data => {
            if (data.IsSuccess) {
                this.packageList = data.Data;
                this.packageNameLib = data.Data.reduce((res: any, current: any) => {
                    res[current.Id] = current.NameLabel;
                    return res;
                }, {});
            }
        });
    }

    setSelectedPackage(e: any) {
        this.adminAddPackage.PackageId = e;
        this.selectedPackage = this.packageList.find(x => x.Id == e);
        console.log(this.selectedPackage);
    }

    addPackage() {
        this.adminAddPackage.UserLicenses = this.addedUsers.map(x => x.value as string);

        if (this.editPackMode) {
            this.packageSvc.adminEditPackages(this.optionsPopup.Id, this.adminAddPackage, (success: boolean) => {
                if (success) {
                    this.adminAddPackage = new AdminAddPackageModel();
                    console.log("Package Successfully Updated");
                    this.editPackMode = false;
                }
                else {
                    console.log("something went wrong");
                }
            });
        }
        else {
            this.packageSvc.adminAddPackages(this.optionsPopup.Id, this.adminAddPackage, (success: boolean) => {
                if (success) {
                    this.adminAddPackage = new AdminAddPackageModel();
                    console.log("Package Successfully added");
                }
                else {
                    console.log("something went wrong");
                }
            });
        }
    }

    editPackage(pack: AdminAddPackageModel) {
        this.editPackMode = true;
        this.adminAddPackage = pack;
        this.addedUsers = this.orgUsers.filter(user => pack.UserLicenses.findIndex(x => x == user.value) != -1);
        this.avalibleUsers = this.orgUsers.filter(user => pack.UserLicenses.findIndex(x => x == user.value) == -1);
    }

    getAdminUsers() {
        this._adminUserService.getAdminUsersByPageonFilter(this.page, this.pageSize, this.userfilterForm);

    }
    getAdminOrganisations() {
        this._adminUserService.getOrganizationsByPageonFilter(this.org_page, this.org_pageSize, this.orgfilterForm);

    }

    stringAsDate(dateStr: any) {
        var date = new Date(dateStr);
        return date;
    }

    onScrollusers(event: any) {
        this.clsTag = false;
        if (event) {
            this.apiProgress = true;
            this.page++;
            this._adminUserService.getAdminUsersByPageonFilter(this.page, this.pageSize, this.userfilterForm);
        }
    }

    onScrollOrganization(event: any) {
        this.clsTagOrg = false;
        if (event) {
            this.apiProgress = true;
            this.org_page++;
            this._adminUserService.getOrganizationsByPageonFilter(this.org_page, this.org_pageSize, this.orgfilterForm);
        }
    }

    private searchDelay: any;
    getAdminUsersonFilter() {
        let that = this;
        clearTimeout(this.searchDelay);
        this.searchDelay = setTimeout(function () {
            that.page = 1;
            that.clsTag = true;
            that._adminUserService.getAdminUsersByPageonFilter(that.page, that.pageSize, that.userfilterForm);
        }, 500);
    }
    getOrganizationsonFilter() {
        let that = this;
        clearTimeout(this.searchDelay);
        this.searchDelay = setTimeout(function () {
            that.org_page = 1;
            that.clsTagOrg = true;
            that._adminUserService.getOrganizationsByPageonFilter(that.org_page, that.org_pageSize, that.orgfilterForm);
        }, 500);
    }

    onFilterDateChanged() {
        this.page = 1;
        this.clsTag = true;
        this._adminUserService.getAdminUsersByPageonFilter(this.page, this.pageSize, this.userfilterForm);
    }

    clickOnUser(id: string) {
        this._adminUserService.loginUnderUser(id);
    }

    clickToClose(e: any) {
        if (e.target.id == "PackageOrLoginPopup") {
            this.optionsPopup = null;
        }
    }

    addUserLicence(user: ChosenOptionModel) {
        if (this.adminAddPackage.Licences > this.addedUsers.length) {
            this.addedUsers.push(user);
            this.avalibleUsers.splice(this.avalibleUsers.findIndex(x => x.value == user.value), 1);
        }
    }

    removeUserLicence(user: ChosenOptionModel) {
        this.avalibleUsers.push(user);
        this.addedUsers.splice(this.addedUsers.findIndex(x => x.value == user.value), 1);
    }

    showOptionsPopup(org: any) {
        this.optionsPopup = org;
        this.adminAddPackage.Currency = org.CurrencyName;
        this.userSvc.getUsers(this.optionsPopup.Id);
    }

    changeOrganisation(orgId: string) {
        this.orgSvc.redirectUrl = "/CRM/Dashboard";
        this.orgSvc.getOrganisation(orgId)
    }

    ngOnDestroy() {
        if (this.userSub) this.userSub.unsubscribe();
        if (this.orgSub) this.orgSub.unsubscribe();
        if (this.packageSub) this.packageSub.unsubscribe();
        if (this.orgUserSub) this.orgUserSub.unsubscribe();
    }
}

