import { Inject } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Api } from '../../../services/api';
import { AuthService } from '../../../services/auth-service';
import { LocalStorage } from '../../../services/localstorage';
import { AdminUser, AdminUserResponse, AdminOrganizationResponse, AdminOrganization } from './admin.users.model';
import { LoaderService } from '../../../services/loader.service';

export class AdminUsersService {
	public _http: any;
	public admin_users$: Observable<Array<any>>;
	public admin_organizations$: Observable<Array<any>>;
	public users: Array<AdminUser>;
	public organizations: Array<AdminOrganization>;
	private admin_usersObserver: any;
	private admin_organizationsObserver: any;
	private _admin_users: AdminUserResponse;
	private _admin_organizations: AdminOrganizationResponse;
	constructor( @Inject(Api) _http: Api, @Inject(LoaderService) public _load: LoaderService, @Inject(AuthService) private auth: AuthService,
		@Inject(LocalStorage) public _localStore: LocalStorage,
		@Inject(Router) public router: Router) {
		this._http = _http;
		this.admin_users$ = new Observable<Array<any>>((observer: any) => {
			this.admin_usersObserver = observer;
		}).share();
		this.admin_usersObserver = new Array;
		this.admin_organizations$ = new Observable<Array<any>>((observer: any) => {
			this.admin_organizationsObserver = observer;
		}).share();
		this.admin_organizationsObserver = new Array;
	}

	getAdminUsersByPageonFilter(page: number, pageSize: number, filter: any) {
		this._load.clear();
		console.log(filter);
		var userName = typeof filter.UserName === 'undefined' || filter.UserName === '' ? '' : filter.UserName;
		var userEmail = typeof filter.UserEmail === 'undefined' || filter.UserEmail === '' ? '' : filter.UserEmail;
		var userDateCreated = typeof filter.UserDateCreated === 'undefined' || filter.UserDateCreated === '' || filter.UserDateCreated === '1970-01-01T00:00:00.000Z' ? '' : filter.UserDateCreated;
		var userLastLogin = typeof filter.userDateCreatedEnd === 'undefined' || filter.userDateCreatedEnd === '' || filter.userDateCreatedEnd === '1970-01-01T00:00:00.000Z' ? '' : filter.userDateCreatedEnd;
		var userLastLoginStart = typeof filter.DateLastLoginStart === 'undefined' || filter.DateLastLoginStart === '' || filter.DateLastLoginStart === '1970-01-01T00:00:00.000Z' ? '' : filter.DateLastLoginStart;
		var userLastLoginEnd = typeof filter.DateLastLoginEnd === 'undefined' || filter.DateLastLoginEnd === '' || filter.DateLastLoginEnd === '1970-01-01T00:00:00.000Z' ? '' : filter.DateLastLoginEnd;

		this._http.get('SuperAdmin/AllUsers?Page=' + page + '&PageSize=' + pageSize + '' + '&Name=' + userName + '&Email=' + userEmail + '&DateCreatedStart=' + userDateCreated + '&DateCreatedEnd=' + userLastLogin + '&DateLastLoginStart=' + userLastLoginStart + '&DateLastLoginEnd=' + userLastLoginEnd)
			.map((res: any) => res.json())
			.subscribe((res: any) => {
				this._admin_users = res;
				if (this._admin_users.IsSuccess) {
					this.users = [];
					this.users = this._admin_users.Data;
					this.admin_usersObserver.next(this.users);
				}
			}, (err: any) => {
				console.log('error 77');
			});
	}

	getOrganizationsByPageonFilter(page: number, pageSize: number, filter: any) {
		this._load.clear();
		var orgName = typeof filter.OrgName === 'undefined' || filter.OrgName === '' ? '' : filter.OrgName;
		var orgCreatedBy = typeof filter.CreatedBy === 'undefined' || filter.CreatedBy === '' ? '' : filter.CreatedBy;

		this._http.get('SuperAdmin/AllOrganisation?Page=' + page + '&PageSize=' + pageSize + '&Name=' + orgName + '&CreatedBy=' + orgCreatedBy)
			.map((res: any) => res.json())
			.subscribe((res: any) => {
				this._admin_organizations = res;

				if (this._admin_organizations.IsSuccess) {
					this.organizations = [];
					this.organizations = this._admin_organizations.Data;
					this.admin_organizationsObserver.next(this.organizations);
				}
			}, (err: any) => {
				console.log('error 77');
			});
	}

	loginUnderUser(id: string) {
		this._load.clear();
		this.auth.AdminLogin(id);
	}
}
