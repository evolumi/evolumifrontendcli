export class AdminUserResponse {
	IsSuccess: boolean;
	Data: Array<AdminUser>;
}

export class AdminUser {
	Id: string;
	HasImage: boolean;
	FullName: string;//Name
	DateCreated: string;//DateCreated
	Email: string;//Email
	UserName: string;
	DateActivate: string;
	DateInactivate: string;
	Language: string;
	DateLastLogin: string;
	ProfilePictureUrl: string;
}

export class AdminOrganizationResponse {
	IsSuccess: boolean;
	Data: Array<AdminOrganization>;
}

export class AdminOrganization {
	Id: string;
	HasImage: boolean;
	OrganisationName: string;//Name
	CreatedBy: string; //Email :created by
	ImageUrl: string; //organization
	DateCreated: string;//DateCreated
	NumberOfUsers: number; // no of users in the organisation
	NumberOfAccounts: number;// no of accounts in the organisation
	NumberOfDeals: number;// no of deals in the organisation
	NumberOfActions: number;// no of actions in the organisation
	Packages: any;
}
