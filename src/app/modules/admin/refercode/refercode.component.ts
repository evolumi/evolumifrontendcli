import { Component } from '@angular/core';
import { Api } from '../../../services/api';
import { ReferCodeModel } from '../../../models/refer-code.models';
import { ChosenOptionModel } from '../../shared/components/chosen/chosen.models';

@Component({
    selector: 'refercode',
    templateUrl: './refercode.html',
    styleUrls: ['./refercode.css']
})

export class ReferCodeComponent {

    public model: ReferCodeModel;
    private targetUrl: string;

    constructor(
        private api: Api
    ) { }

    ngOnInit() {
        this.resetModel();
    }

    resetModel() {
        this.api.get("VisitorTracker/GetCreateReferCodeModel").map(res => res.json()).subscribe(data => {
            if (data.IsSuccess) {
                data.Data.Sources = data.Data.Sources.map((x: string) => new ChosenOptionModel(x, x));
                data.Data.Types = data.Data.Types.map((x: string) => new ChosenOptionModel(x, x));
                data.Data.Campaigns = data.Data.Campaigns.map((x: string) => new ChosenOptionModel(x, x));
                this.model = data.Data;
            }
        });
    }

    createCode() {
        let model = {
            Name: this.model.Name,
            Source: this.model.Source,
            Type: this.model.Type,
            Campaign: this.model.Campaign
        }
        this.api.post("VisitorTracker/CreateReferenceCode", model).map(res => res.json()).subscribe(data => {
            this.targetUrl += "?ref=" + data.Data;
        });

    }

}