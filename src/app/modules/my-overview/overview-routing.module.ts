import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { AuthGuard } from '../../services/guards/auth-guard';

// Components
import { MyOverviewComponent } from '../my-overview/myoverview.component';
import { ViewComponent } from './overview/overview';
import { AddOrganisationComponent } from './add-organisation/add-organisation.component';


export const OverviewRoutes: Routes = [
    {
        path: 'Overview',
        component: MyOverviewComponent,
        canActivate: [AuthGuard],
        children: [
            { path: '', component: ViewComponent },
            { path: 'Add', component: AddOrganisationComponent },
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(OverviewRoutes)
    ],
    exports: [RouterModule]
})
export class OverviewRoutingModule { }