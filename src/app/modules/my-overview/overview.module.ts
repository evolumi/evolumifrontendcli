import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { SharedModule } from '../shared/shared.module';
import { HeaderModule } from '../header/header.module';
import { OverviewRoutingModule } from './overview-routing.module';

// Components
import { MyOverviewComponent } from './myoverview.component';
import { ViewComponent } from './overview/overview';
import { AddOrganisationComponent } from './add-organisation/add-organisation.component';

@NgModule({
    imports: [
        SharedModule,
        HeaderModule,
        OverviewRoutingModule
    ],
    declarations: [
        MyOverviewComponent,
        ViewComponent,
        AddOrganisationComponent,
    ],
    providers: []
})
export class OverviewModule { }