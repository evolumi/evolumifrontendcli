import { Component } from '@angular/core';
import { Location } from '@angular/common';

@Component({
    templateUrl: './myoverview.html',
})

export class MyOverviewComponent {

    constructor(
        private location: Location
    ) {

    }

    ngOnInit() {
    }

    back() {
        this.location.back();
    }

    ngOnDestroy() {
    }

}
