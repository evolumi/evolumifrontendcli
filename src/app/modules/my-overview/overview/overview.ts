import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { LocalStorage } from '../../../services/localstorage';
import { FilterCollection } from '../../../services/filterservice';
import { AssignmentShortModel } from '../../../models/assignment.models';
import { Api } from '../../../services/api';
import { OrgService } from '../../../services/organisation.service';
import { AuthService } from '../../../services/auth-service';
import { Subscription } from "rxjs/Subscription";

@Component({
    templateUrl: './overview.html',
    providers: [LocalStorage],
    styleUrls: ['overview.css']
})

export class ViewComponent {

    public timestamp: number;
    public currentUser: any = {};
    public uploadheaders: any = {};
    public assignments: Array<AssignmentShortModel> = [];

    private userSub: any;
    public invites: Array<any> = [];

    private firstOrgSub: Subscription;

    constructor(
        public organisationService: OrgService,
        private auth: AuthService,
        private router: Router,
        private translate: TranslateService,
        private _filterService: FilterCollection,
        private _api: Api,
    ) { }

    ngOnInit() {
        this.userSub = this.auth.currentUserObservable().subscribe(res => {
            if (res) this.currentUser = res;
        });

        this.organisationService.getOrganisations();
        this.organisationService.getInvites().subscribe(res => {
            if (res.IsSuccess) {
                this.invites = res.Data;
            }
        });

        this.firstOrgSub = this.organisationService.firstOrgCreatedObservable().subscribe(orgId => {
            if (this.invites.length == 0) {
                this.renderTrackScript();
            }
        });

        // this._marketplaceService.userAssignments$.subscribe(response => {
        //     this.assignments = response;
        // });
        // this._marketplaceService.getTopUserAppliedAssignments();

    }


    renderTrackScript() {
        let s1 = document.createElement("script");
        s1.type = "text/javascript";
        s1.innerHTML = `
        var google_conversion_id = 971999485;
        var google_conversion_language = "en";
        var google_conversion_format = "2";
        var google_conversion_color = "ffffff";
        var google_conversion_label = "w2s0CMOAwgcQ_ZG-zwM";
        var google_remarketing_only = false;
        `
        let s2 = document.createElement("script");
        s2.src = "//www.googleadservices.com/pagead/conversion.js";

        let noscript = document.createElement("noscript");
        let ns1 = document.createElement("div");
        ns1.style.display = "inline";
        let ns2 = document.createElement("img");
        ns2.style.height = "1";
        ns2.style.width = "1";
        ns2.style.borderStyle = "none";
        ns2.alt = "";
        ns2.src = "//www.googleadservices.com/pagead/conversion/971999485/?label=w2s0CMOAwgcQ_ZG-zwM&amp;guid=ON&amp;script=0";
        ns1.appendChild(ns2);
        noscript.appendChild(ns1);

        document.body.appendChild(s1);
        document.body.appendChild(s2);
        document.body.appendChild(noscript);
    }


    public setOrganisation(id: string): void {
        if (id && id != 'false') {
            localStorage.setItem("orgId", id);
            this.organisationService.isLoaded = false;
            this.organisationService.getOrganisation(id);

            // Todo: Remove this when all connections cut
            this._filterService.setOrganisation(id);

            this.router.navigate(['CRM/Dashboard']);
        }
    }

    public deleteOrganisation(id: string): void {
        var conf = confirm(this.translate.instant("WARNDELORG"));
        if (conf) {
            this.organisationService.deleteOrganisation(id);
        }
    }

    public acceptInvitation(id: string) {
        this._api.post("Users/Invite/" + id + "/Accept", null).map(res => res.json()).subscribe(res => {
            if (res.IsSuccess) {
                this.organisationService.getOrganisations();
                this.invites.splice(this.invites.findIndex(x => x.id == id), 1);
            }
        })
    }

    ngOnDestroy() {
        if (this.userSub) this.userSub.unsubscribe();
        if (this.firstOrgSub) this.firstOrgSub.unsubscribe();
    }

    public hideAddOrgForm() {
        // NOT IMPLEMENTED. Just added for AOT compilation errors. Someone check it sometime.
    }
}
