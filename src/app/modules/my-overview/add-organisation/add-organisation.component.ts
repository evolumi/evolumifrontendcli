import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

import { NotificationService } from '../../../services/notification-service';
import { OrgService } from '../../../services/organisation.service';
import { ValidationService } from '../../../services/validation.service';
import { Component } from '@angular/core';

@Component({
    selector: 'add-organisation',
    templateUrl: './add.html',
    styleUrls: ['./add.css']
})

export class AddOrganisationComponent {
    public org: string;
    public inviteUsers = [1];
    public inviteUsersArray: Array<any> = [];
    public displayAddOrganisation: boolean = false;

    private companyFocusOptions: string[] = ["PERSONS", "MOSTLYPERSONS", "BOTHEQUALLY", "MOSTLYCOMPANIES", "COMPANIES"];
    public selectedFocus: string = "BOTHEQUALLY";
    public selectedEmployeeCount: string;
    public selectedIndustry: string;

    public options: any;

    public invite: FormGroup;
    public organisation_name: string;

    constructor(
        public orgService: OrgService,
        private _form: FormBuilder,
        private _notify: NotificationService,
        public translate: TranslateService,
    ) {

        this.inviteUsersArray[1] = {};
        this.invite = this._form.group({
            Email: ['', Validators.compose([Validators.required, ValidationService.emailValidator])]
        });

    };

    ngOnInit() {
        this.orgService.getCreateOptions((data) => {
            data.Interactions = data.Interactions.reduce((res: any, curr: any) => {
                res.push({ Text: curr, Selected: false });
                return res;
            }, [])
            data.Industries = this.sortByTranslation(data.Industries);
            this.options = data
        });
    }

    sortByTranslation(array: string[]) {
        return array.sort((a: string, b: string) => {
            return this.translate.instant(a) < this.translate.instant(b) ? -1 : 1;
        })
    }

    changeFocus(e: any) {
        this.selectedFocus = this.companyFocusOptions[e.target.valueAsNumber - 1];
    }

    routerOnActivate() {
        return new Promise((res, rej) => setTimeout(() => res(1), 500));
    };

    routerOnDeactivate() {

        return new Promise((res, rej) => setTimeout(() => res(1), 500));
    };

    public create(organisation: any) {

        console.log(organisation);
        if (organisation.valid) {
            organisation.value.Questions = [
                { QuestionTag: "CREATEORGCUSTOMERTYPE", Answer: [this.selectedFocus] },
                { QuestionTag: "CREATEORGEMPLOYEES", Answer: [this.selectedEmployeeCount] },
                { QuestionTag: "CREATEORGINDUSTRY", Answer: [this.selectedIndustry] },
                { QuestionTag: "CREATEORGINTERACTION", Answer: this.options.Interactions.filter((x: any) => x.Selected).map((x: any) => x.Text) }
            ]
            this.orgService.addOrganisation(organisation.value, true);
            //this.back()
        } else {
            this._notify.error('Invalid Orgnisation Details');
        }

    };

    clickOutside(e: any) {
        if (e.target.id == "addOrganisationPopup") {
            this.back();
        }
    }

    public back() {
        this.displayAddOrganisation = false;
        this.inviteUsersArray[1] = {};

    };

    public show() {
        this.displayAddOrganisation = true;
    }

    public addNewInviteUser() {
        this.inviteUsersArray[this.inviteUsers.length + 1] = {};
        this.inviteUsers.push(this.inviteUsers.length + 1);
    }
}

