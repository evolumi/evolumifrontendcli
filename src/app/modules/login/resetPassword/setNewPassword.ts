import { Component } from '@angular/core';
import { Api } from '../../../services/api';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { NotificationService } from '../../../services/notification-service';
import { Validators, FormBuilder } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { ValidationService } from '../../../services/validation.service';
import { Configuration } from '../../../services/configuration';

@Component({
    templateUrl: './set.html',
    styles: [`
    .newPassValidation {
        position: absolute;
        bottom: -10px;
        left: calc(100% + 20px);
    }
    `]
})

export class ResetPasswordComponent {
    public resetPass: any;
    public token: string;
    public error: boolean;
    constructor(public _http: Api, public _router: Router, private loc: Location, public form: FormBuilder, public _notify: NotificationService, public translate: TranslateService, private config: Configuration) {

        this._http = _http;
        this.resetPass = this.form.group({
            newPassword: ['', Validators.compose([Validators.required, ValidationService.passwordValidator])],
            confPassword: ['', Validators.compose([Validators.required, ValidationService.passwordValidator])]
        });
        translate.use(this.config.defaultLanguageFile());
        let url = this.loc.prepareExternalUrl(loc.path());
        this.token = url.split('=')[1];
        console.log("[Password-Reset] Found token: " + this.token);

    }
    ngOnInit() {
        this.translate.setDefaultLang(this.config.defaultLanguageFile());
    }

    updatePass() {

        if (this.resetPass._value.newPassword !== this.resetPass._value.confPassword) {
            this.error = true;
            console.log("[PasswordReset] Error! -> Passwords must match");
            return false;
        }
        if (this.resetPass.dirty && this.resetPass.valid) {
            var data = {
                'Token': this.token,
                'NewPassword': this.resetPass._value.newPassword
            };
            console.log("[PasswordReset] Update Password: Started!");
            this._http.put('Users/SetNewPassword', data)
                .map(res => res.json())
                .subscribe(res => {
                    if (res.IsSuccess) {
                        this._notify.success(this.translate.instant('PASSWORDUPDATED'));
                        console.log("[PasswordReset] Update Password: Success!");
                        this.error = false;
                        this._router.navigate(['Login']);
                    } else {
                        this.error = true;
                        this._notify.error(this.translate.instant('ERROR'));
                        console.log("[PasswordReset] Update Password: Failed!");
                    }
                });
        }
        return true;
    }
}
