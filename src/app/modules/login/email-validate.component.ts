import { Component } from '@angular/core';
import { Api } from '../../services/api';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs/Subscription';
import { Configuration } from '../../services/configuration';

@Component({
    templateUrl: 'email-validate.html',
})

export class EmailValidateComponent {
    private sub: Subscription;
    private token: string;
    private user: string;

    public status: string;

    constructor(private api: Api, private route: ActivatedRoute, translate: TranslateService, config: Configuration) {

        translate.use((localStorage.getItem("lan") || config.defaultLanguage) + "." + config.languageFileVersion);
        this.sub = this.route.params.subscribe(params => {
            this.token = params['token'];
            this.user = params['user'];
            console.log("[Invite-login] Found Token: " + this.token);
            this.validate();
        });
    }

    public validate() {
        this.status = "REGESTERINGEMAIL";
        this.api.post("Users/" + this.user + "/EmailValidate", this.token).map(res => res.json()).subscribe(res => {
            if (res.IsSuccess) {
                this.status = "EMAILREGISTERSUCCESS";
            }
            else {
                this.status = "EMAILREGISTERFAIL";
            }
        },
            err => {
                this.status = "EMAILREGISTERFAIL";
            })
    }

    ngOnDestroy() {
        if (this.sub) this.sub.unsubscribe();
    }

}