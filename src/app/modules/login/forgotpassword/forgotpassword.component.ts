import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Api } from '../../../services/api';
import { TranslateService } from '@ngx-translate/core';
import { NotificationService } from '../../../services/notification-service';
import { Configuration } from '../../../services/configuration'

@Component({
	templateUrl: './forgetpassword.html',
})


export class ForgotPassword {
	public Email: string;
	public error: string;
	constructor(private translate: TranslateService, private router: Router, public _http: Api, public _notify: NotificationService, config: Configuration) {
		this.Email = '';
		translate.use('en.' + config.languageFileVersion);
	}
	ngOnInit() { }

	onSubmit() {
		var data = { 'Email': this.Email };
		this._http.post('Users/ResetPassword', data)
			.map(res => res.json())
			.subscribe(res => {
				if (res.IsSuccess === true) {
					this.error = "";
					this.Email = '';
					this._notify.success(this.translate.instant('RESETLINKSENT'));
					this.router.navigate(['Login']);
				} else {
					this._notify.error(res.Message);
				}
			}, err => {
				let error = err.json();
				this._notify.error(error.Message || this.translate.instant("SOMETHINGWRONG"));
			});
	}
}
