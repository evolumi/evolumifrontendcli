import { Component } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { CalendarService, AuthCodeObject } from '../../services/calendar.service';
import { Subscription } from "rxjs/Subscription";

@Component({

    template: `
    <div>
        This window will close after authorization is done
    </div>
    `
})

export class CalendarAuthComponent {
    // private code: string;
    // private userId: string;

    private routeSub: Subscription;

    constructor(private _route: ActivatedRoute, private _calendarSvc: CalendarService) { }

    ngOnInit() {
        this.routeSub = this._route.queryParams.subscribe((params: Params) => {
            let provider = (<any>this._route)._routerState.snapshot.url.match(/Auth(\w*)/)[1]; //Tar ut Microsoft eller Google ur Route.
            let userAndOrgId = JSON.parse(decodeURIComponent(params["state"]));
            let model: AuthCodeObject = {
                AuthCode: params["code"],
                UserId: userAndOrgId.UserId,
                OrganisationId: userAndOrgId.OrgId
            }
            this._calendarSvc.authorize(model, provider, () => {
                console.log(userAndOrgId, model);
                //window.close();
            });
        });
    }

    ngOnDestroy(){
        if(this.routeSub) this.routeSub.unsubscribe();
    }
}