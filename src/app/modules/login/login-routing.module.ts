import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { LoginComponent } from "./login.component";
import { ForgotPassword } from "./forgotpassword/forgotpassword.component";
import { SignupComponent } from "./signup.component";
import { EmailValidateComponent } from "./email-validate.component";
import { FirstLoginComponent } from "./firstLogin.component";
import { ResetPasswordComponent } from "./resetPassword/setNewPassword";
import { CalendarAuthComponent } from "./calendar-auth.component";
import { UserQuestionsComponent } from "./user-questions/user-questions";

export const LoginRoutes = [
      { path: 'Login', component: LoginComponent }, { path: 'login', redirectTo: 'Login', pathMatch: 'full' },
      { path: 'ForgotPassword', component: ForgotPassword },
      { path: 'Signup', component: SignupComponent }, { path: 'signup', redirectTo: 'Signup', pathMatch: 'full' },
      { path: 'Signup/:lang', component: SignupComponent },
      { path: 'Signup/:email', component: SignupComponent },
      { path: 'Signup/:lang/:firstName/:lastName/:email', component: SignupComponent },
      { path: 'Signup/:firstName/:lastName/:email', component: SignupComponent },
      { path: 'EmailValidate/:user/:token', component: EmailValidateComponent },
      { path: 'EmailValidate/:user/:token/:organisationName', component: EmailValidateComponent }, // This is not used anymore but some mails out there might still have it in the Url so keeping it here for a bit.
      { path: 'OrganisationInvitation', component: FirstLoginComponent },
      { path: 'OrganisationInvitation/:token', component: FirstLoginComponent },
      { path: 'ResetPassword', component: ResetPasswordComponent }, { path: 'resetPassword', component: ResetPasswordComponent },
      { path: 'ResetPassword/:token', component: ResetPasswordComponent }, { path: 'resetPassword:/:token', component: ResetPasswordComponent },
      { path: 'AuthMicrosoft', component: CalendarAuthComponent }, { path: 'AuthGoogle', component: CalendarAuthComponent },
      { path: 'UserQuestions', component: UserQuestionsComponent }
]

@NgModule({
  imports: [RouterModule.forChild(LoginRoutes)],
  exports: [RouterModule]
})
export class LoginRoutingModule {}