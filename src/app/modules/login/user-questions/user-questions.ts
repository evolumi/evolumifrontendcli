import { Component } from '@angular/core';
import { UsersService } from "../../../services/users.service";
import { TranslateService } from "@ngx-translate/core";
import { Configuration } from "../../../services/configuration";
import { Router } from "@angular/router";
import { NotificationService } from "../../../services/notification-service";

@Component({
    selector: 'user-questions',
    templateUrl: './user-questions.html',
    styleUrls: ['./user-questions.css']
})

export class UserQuestionsComponent {
    public questions = ["CRMSKILLS", "WHATDESCRIBESYOUFIELD", "WHATDESCRIBESYOUROLE", "HOWFINDUS"]
    public options: any;
    public crmSkills = ["CRMSKILLSNONE", "CRMSKILLSBEGINNER", "CRMSKILLSOK", "CRMSKILLSALOT"]

    private crmSkill: string;
    public field: string;
    public role: string;
    public selectedSource: string;
    private otherSource: string;

    constructor(
        private userSvc: UsersService,
        private translate: TranslateService,
        private config: Configuration,
        private router: Router,
        private notificationSvc: NotificationService
    ) {
        translate.use((localStorage.getItem("lan") || config.defaultLanguage) + "." + this.config.languageFileVersion);
    }

    ngOnInit() {
        this.userSvc.getUserAnswerOptions((options: any) => {
            options.Roles = options.Roles;
            options.Fields = options.Fields;
            this.options = options;
        });
        this.crmSkill = this.translate.instant("CRMSKILLSOK");
    }
    skillChanged(e: any) {
        this.crmSkill = this.crmSkills[e];
    }

    submit() {
        let obj = [
            { QuestionTag: this.questions[0], Answer: [this.crmSkill] },
            { QuestionTag: this.questions[1], Answer: [this.field || this.options.Fields[0]] },
            { QuestionTag: this.questions[2], Answer: [this.role || this.options.Roles[0]] },
            { QuestionTag: this.questions[3], Answer: this.selectedSource != 'HOWFINDOTHER' ? [this.selectedSource] : [this.otherSource != null ? this.otherSource : 'HOWFINDOTHER'] }
        ]
        this.notificationSvc.success(this.translate.instant("USERQUESTIONTHANKYOU"));
        this.userSvc.saveQuestions(obj, () => {
            this.router.navigate(['Overview']);
        });

        console.log(obj);
    }

    ngAfterViewInit() {
        console.log("script now");
    }

}