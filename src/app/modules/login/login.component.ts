import { Component } from '@angular/core';
import { AuthService } from '../../services/auth-service';

import { TranslateService } from '@ngx-translate/core'
import { Configuration } from '../../services/configuration';

@Component({
    templateUrl: './login.html',
})

export class LoginComponent {
    public login = { remeberMe: false, Username: "", Password: "" };

    constructor(
        public authService: AuthService,
        translate: TranslateService,
        config: Configuration
    ) {
        translate.use((localStorage.getItem("lan") || config.defaultLanguage) + "." + config.languageFileVersion);

    }

    onSubmit() {
        if (this.login.Username != '' && typeof this.login.Username != 'undefined' && this.login.Password != '' && typeof this.login.Password != 'undefined') {
            this.authService.Login(this.login.Username, this.login.Password);
        }

    }
}
