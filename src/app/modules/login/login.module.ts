import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { SharedModule } from '../shared/shared.module';

// Components
import { LoginComponent } from './login.component';
import { FirstLoginComponent } from './firstLogin.component';
import { ForgotPassword } from './forgotpassword/forgotpassword.component';
import { ResetPasswordComponent } from './resetPassword/setNewPassword';
import { SignupComponent } from './signup.component'
import { EmailValidateComponent } from './email-validate.component';
import { CalendarAuthComponent } from './calendar-auth.component';
import { UserQuestionsComponent } from './user-questions/user-questions';
import { LoginRoutingModule } from "./login-routing.module";

@NgModule({
    imports: [
        FormsModule,
        HttpModule,
        CommonModule,
        SharedModule,
        LoginRoutingModule
    ],
    declarations: [
        LoginComponent,
        FirstLoginComponent,
        ForgotPassword,
        ResetPasswordComponent,
        SignupComponent,
        EmailValidateComponent,
        CalendarAuthComponent,
        UserQuestionsComponent
    ],
    exports: [
        ResetPasswordComponent,
        EmailValidateComponent
    ]
})
export class LoginModule { }