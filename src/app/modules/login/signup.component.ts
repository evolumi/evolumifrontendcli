import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ValidationService } from '../../services/validation.service';
import { NotificationService } from '../../services/notification-service';
import { LocalStorage } from '../../services/localstorage';
import { Api } from '../../services/api';
import { ActivatedRoute, Router } from '@angular/router';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Http, Headers, RequestOptionsArgs } from '@angular/http';
import { CookieService } from 'ngx-cookie';
import { Configuration } from '../../services/configuration';

@Component({
    templateUrl: 'signUp.html',
})

export class SignupComponent {
    private sub: any;
    private querySub: any;
    public acceptTerms: boolean = false;
    public langVersion: number;
    public step1Form: FormGroup;
    public password: string;
    public defaultLanguage: string;
    public process = false;
    public error: boolean = false;
    public presetEmail: string;
    public presetFirstName: string;
    public presetLastName: string;
    public options: RequestOptionsArgs = {
        headers: new Headers({
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        })
    };

    private oldVisit: string;

    constructor(
        private route: ActivatedRoute,
        public _router: Router,
        form: FormBuilder,
        public translate: TranslateService,
        public _notify: NotificationService,
        private http: Http,
        public _store: LocalStorage,
        public _api: Api,
        private config: Configuration,
        private cookieSvc: CookieService
    ) {

        this.langVersion = this.config.languageFileVersion;

        this.step1Form = form.group({
            Firstname: ['', Validators.required],
            Lastname: ['', Validators.required],
            Email: ['', Validators.compose([Validators.required, ValidationService.emailValidator])],
            Password: ['', Validators.compose([Validators.required, ValidationService.passwordValidator])]
        });
    }

    private cookieName = "nav";
    setCookie(cvalue: string, exdays: number) {
        let d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        let expires = "expires=" + d.toUTCString();
        document.cookie = this.cookieName + "=" + cvalue + ";" + expires + ";path=/";
    }

    getCookie() {
        let name = this.cookieName + "=";
        let ca = document.cookie.split(';');
        for (let i = 0; i < ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    ngOnInit() {
        this.sub = this.route.params.subscribe(params => {
            this.defaultLanguage = params['lang'];
            if (!this.defaultLanguage) {
                this.defaultLanguage = this.config.defaultLanguage;
            }
            this.defaultLanguage += "." + this.config.languageFileVersion;
            this.setLanguage(this.defaultLanguage)
            this.translate.use(this.defaultLanguage);
        });
        this.querySub = this.route.queryParams.subscribe(params => {
            if (params['ref'] && !this.getCookie()) {
                var refObj = JSON.parse(params['ref']);
                console.log(refObj);
                this.setCookie(JSON.stringify(refObj), 30);
            }
            if (params['email']) {

                let validEmail = Boolean(params['email'].match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/));
                console.log(validEmail);
                if (validEmail) {
                    this.presetEmail = params['email'];
                    this.step1Form.controls["Email"].setValue(params['email']);
                    //this.saveNavigationsToDb(params['email']);
                }
            }
            if (params['firstName']) {
                this.presetFirstName = params['firstName'];
                this.step1Form.controls["Firstname"].setValue(params['firstName']);
            }
            if (params['lastName']) {
                this.presetLastName = params['lastName'];
                this.step1Form.controls["Lastname"].setValue(params['lastName']);
            }

        });
    }

    formSubmit(event: any) {
        this.process = true;
        let data = this.step1Form.value;
        data.Languge = this.defaultLanguage;
        this.http.post(this._api.apiUrl + 'Users/Create', JSON.stringify(data), this.options)
            .map(res => res.json())
            .subscribe(d => {
                this.process = false;
                if (d.IsSuccess) {
                    this._notify.success(this.translate.instant('REGISTRATIONCOMPLETED'));
                    this.onSubmit({ Username: this.step1Form.value.Email, Password: this.step1Form.value.Password, remeberMe: false });
                } else {
                    this._notify.error(d.Message);
                }
            }, err => {
                this.process = false;
            });
    }

    onSubmit(cred: any) {
        this.http.post(this._api.apiUrl + 'Users/Login', JSON.stringify(cred), this.options)
            .map(res => res.json())
            .subscribe(res => {
                this.error = false;
                if (res.IsSuccess) {

                    this.connectUserToVisits(res.Data.UserId);
                    this._notify.success(this.translate.instant('LOGINSUCCESS'));
                    if (res.Data.ActiveOrganisationId === null || typeof res.Data.ActiveOrganisationId == 'undefined') {
                        this._store.setObject('userDetails', res.Data);
                        this._api.setHeaders();
                        this._router.navigate(['UserQuestions']);
                    } else {
                        this._store.set('userDetails', JSON.stringify(res.Data));
                        this._router.navigateByUrl('CRM');
                    }
                } else {
                    this.error = true;
                }
            }, (err) => {
                this.process = false;
                this._notify.error(this.translate.instant('ERROR'));
            });
    }

    logoClicked() {
        console.log(this.oldVisit);
    }

    connectUserToVisits(userId: string) {
        this._api.get("VisitorTracker/ConnectUserVisits/" + userId + "/" + this.oldVisit).subscribe();
    }

    saveNavigationsToDb(email: string) {
        const cookieName = "nav";
        this.oldVisit = this.cookieSvc.get("previousVisit");
        if (!this.oldVisit) {
            let cookie = this.cookieSvc.get(cookieName);
            if (cookie) {
                let cookieObj = JSON.parse(cookie);
                cookieObj.Email = email;
                console.log(cookie);
                if (cookieObj.Email || cookieObj.ReferCode) {
                    this._api.post("VisitorTracker/SaveNavigations", cookieObj).map(res => res.json()).subscribe((data: any) => {
                        if (data.IsSuccess) {
                            this.oldVisit = data.Data;
                            let expires = new Date();
                            let month = expires.getMonth() == 11 ? 0 : expires.getMonth() + 1;
                            if (month == 0) {
                                expires.setFullYear(expires.getFullYear() + 1);
                            }
                            expires.setMonth(expires.getMonth() == 11 ? 0 : expires.getMonth() + 1)
                            console.log(data.Data);
                            this.cookieSvc.put("previousVisit", data.Data, { expires: expires });
                        }
                    });
                }
            }
        }
    }

    setLanguage(lang: string) {
        this.defaultLanguage = lang;
        this.translate.setDefaultLang(lang);
        this.translate.use(lang);
    }

    noRegister() {
        this._api.post("SignupCancelled", this.presetEmail).subscribe();
    }

    ngOnDestroy() {
        if (this.sub) this.sub.unsubscribe();
        if (this.querySub) this.querySub.unsubscribe();
    }
}