import { TranslateService } from '@ngx-translate/core';
import { NotificationService } from '../../services/notification-service';
import { Api } from '../../services/api';
import { AuthService } from '../../services/auth-service';
import { Component } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { ActivatedRoute } from '@angular/router';
import { Validators, FormBuilder } from '@angular/forms';
import { ValidationService } from '../../services/validation.service';
import { Configuration } from '../../services/configuration';

@Component({
    templateUrl: './firstLogin.html',
    styleUrls: ['./firstLogin.css']
})

export class FirstLoginComponent {

    private sub: any;
    public login = {
        Password: '',
        Email: ''
    };

    public invite: any;
    public token: string;
    public tokenInvalid: boolean;

    public useLogin: boolean;
    public useSignUp: boolean;

    public loginForm: any;
    public process = false;
    public error = false;
    public headers = {
        headers: new Headers({
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        })
    };

    constructor(
        private http: Http,
        private route: ActivatedRoute,
        _form: FormBuilder,
        private _notify: NotificationService,
        private _api: Api,
        public translate: TranslateService,
        private auth: AuthService,
        private config: Configuration) {

        this.loginForm = _form.group({
            Firstname: ['', Validators.required],
            Lastname: ['', Validators.required],
            Email: ['', Validators.compose([Validators.required, ValidationService.emailValidator])],
            Password: ['', Validators.compose([Validators.required, ValidationService.passwordValidator])]
        });
        translate.use(this.config.defaultLanguageFile());
        this.sub = this.route.params.subscribe(params => {
            let token = params['token'];
            this.token = token;
            console.log("[Invite-login] Found Token: " + token);
            this.getInvite(token);
        });

    }
    ngOnInit() {

    }

    ngOnDestroy(){
        if (this.sub) this.sub.unsubscribe();
    }

    private getInvite(token: string) {
        this._api.get("Invites/" + token).map(res => res.json()).subscribe(res => {
            if (res.IsSuccess) {
                console.log("[Invite-login] Fetched Invite: ", res.Data);
                this.invite = res.Data;
                this.translate.use((this.invite.language || this.config.defaultLanguage) + "." + this.config.languageFileVersion);
            }
        }, (res) => {
            let message = JSON.parse(res._body).Message;
            this._notify.error(message);
            this.tokenInvalid = true;
        })
    }

    formSubmit(event: any) {
        this.process = true;
        this.http.post(this._api.apiUrl + 'Users/FirstLogin/' + this.invite.Id, JSON.stringify(this.loginForm.value), this.headers)
            .map(res => res.json())
            .subscribe(d => {
                this.process = false;
                if (d.IsSuccess) {
                    this._notify.success(this.translate.instant('REGISTRATIONCOMPLETED'));
                    console.log("[Invite-Login] User Registration: Success!");
                    console.log("[Invite-Login] Organisation Claim Added");
                    this.auth.setRedirectUrl("UserQuestions");
                    this.auth.Login(this.loginForm.value.Email, this.loginForm.value.Password);

                } else {
                    this._notify.error(this.translate.instant('ERROR') + d.Message);
                    console.log("[Invite-Login] User Registration : Fail! -> " + d.Message);
                }
            },
            err => {
                this.process = false;
                console.log("[Invite-Login] User Registration : Fail! -> Unknown Error");
            },
            () => {
                this.process = false;
            });
    }
    loginProcess() {
        if (this.process === false && this.login.Email != '' && typeof this.login.Email != 'undefined' && this.login.Password != '' && typeof this.login.Password != 'undefined') {
            this.process = true;

            this.http.post(this._api.apiUrl + 'Users/FirstLogin/' + this.invite.Id, JSON.stringify(this.login), this.headers)
                .map(res => res.json())
                .subscribe(d => {
                    this.process = false;
                    if (d.IsSuccess) {
                        console.log("[Invite-Login] Adding Organisation-Claim: Success!");
                        this._notify.success(this.translate.instant('LOGINSUCCESS'));
                        localStorage.removeItem("orgId");
                        this.auth.setRedirectUrl("UserQuestions");
                        this.auth.Login(this.login.Email, this.login.Password);

                    } else {
                        console.log("[Invite-Login] Adding Organisation-Claim: Fail! -> " + d.Message);
                        this._notify.error(d.Message);
                    }
                },
                err => {
                    this.process = false;
                    console.log("[Invite-Login] Adding Organisation-Claim: Fail! -> Unkown error :(");
                    this._notify.error(this.translate.instant('ERROR'));
                },
                () => {
                    this.process = false;
                });
        }

    }

    onSubmit(cred: any) {


        //this.http.post(this._api.apiUrl + 'Users/Login', JSON.stringify(cred), this.headers)
        //	.map(res => res.json())
        //	.subscribe(res => {
        //		this.error = false;
        //		if (res.IsSuccess) {					
        //			if (res.Data.ActiveOrganisationId === null || typeof res.Data.ActiveOrganisationId == 'undefined') {
        //                      this._store.setObject('userDetails', res.Data);
        //                      this._api.setHeaders();
        //				this._router.navigateByUrl('CRM/Overview');
        //			} else {
        //				this._store.set('userDetails', JSON.stringify(res.Data));
        //				this._router.navigateByUrl('CRM');
        //			}
        //		} else {
        //			this.error = true;
        //		}
        //          },
        //          (err) => {
        //		this.process = false;
        //              this._notify.error(this.translate.instant('ERROR'));
        //          },
        //          () => {
        //		this.process = false;
        //	});
    }
}
