import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: 'beautifyRange', pure: false })
export class BeautifyRangePipe implements PipeTransform {
    transform(value: any, args: any[]): any {
        let priceIsTotal: boolean = args[1];
        var ranges = args[0].reduce(function (res: any, current: any) {
            res.push(Number(current.Range));
            return res;
        }, []);
        ranges = ranges.sort(function (a: any, b: any) {
            return (Number(a.Range) > Number(b.Range) ? 1 : ((Number(b.Range)) > Number(a.Range) ? -1 : 0));
        });
        if (ranges.indexOf(-1) == 0) {
            return "ALL";
        }

        var next = ranges[ranges.indexOf(value) + 1];
        let last = next ? null : ranges[ranges.length - 2];
        var result = `${last && !priceIsTotal ? ">" + (last * last) / last : value}`;
        return result;
    }
}

@Pipe({ name: 'sortRanges', pure: false })
export class SortRangesPipe implements PipeTransform {
    transform(value: any[], args: any[] = null): any {
        return value.sort(function (a, b) {
            if (a.Range == -1) { return 1; }
            if (b.Range == -1) { return -1; }
            return (Number(a.Range) > Number(b.Range) ? 1 : ((Number(b.Range)) > Number(a.Range) ? -1 : 0));
        })
    }
}



