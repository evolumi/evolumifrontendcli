import {Component, Input, Pipe, PipeTransform} from '@angular/core';

@Pipe({ name: 'url' })
export class UrlPipe implements PipeTransform {
    transform(value: string) {

        if (value) {
            if (value.toLowerCase().startsWith("http")){
                return value;
            }
            return "http://" + value;
        }
        return value;
    }
}
