import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'bigNumber' })
export class BigNumberPipe implements PipeTransform {
    transform(value: number) {
        let number = Math.round(value * 100) / 100;

        var parts = number.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, " ");
        var result = parts.join(".");

        return result;
    }
}
