import { Pipe } from '@angular/core';

@Pipe({
    name: 'CatalogueFilter'
})
export class CatalogueFilter {

    // Transforms Null or 0 to given value of res
    transform(catalogues: any, searchText: string) {
        if (searchText === undefined) {
            return catalogues;
        }

        return catalogues.filter(function (catalogue: any) {
            return catalogue.Name.toLowerCase().includes(searchText.toLowerCase());
        });
    }
}
