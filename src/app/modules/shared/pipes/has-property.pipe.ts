import { Pipe, PipeTransform } from '@angular/core'

@Pipe({
    name: 'hasproperty'
})
export class HasPropertyPipe implements PipeTransform {
    transform(items: any[], field: string): any[] { 
        if (!items) return [];        
        let m = items.filter(item => {
            if (item.hasOwnProperty(field) && item[field]){
                return true;
            }
            return false;
        });
        return m;
    }
}