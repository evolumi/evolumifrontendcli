import { Pipe } from '@angular/core';
import { UsersService } from '../../../services/users.service';

@Pipe({
    name: 'username',
    pure: false
})
export class UsernamePipe {

    private username: string = '';

    constructor(private usersService: UsersService) {

    }

    transform(id: string): any {
        if (!this.username) {
            this.username = this.usersService.getName(id).toString();
        }
        return this.username;
    }
}