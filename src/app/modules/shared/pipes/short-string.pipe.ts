import { Pipe, PipeTransform } from "@angular/core";
@Pipe({
    name: "shortstring"
})

export class ShortStringPipe implements PipeTransform {

    constructor() { }

    transform(value: string, count: number): string {
        if (value && value.length > count) {
            return value.substring(0, count) + '...';
        }
        return value;
    }
}