import { Pipe, PipeTransform } from "@angular/core";
import { DateService } from '../../../services/date.service';

@Pipe({
    name: "datetime"
})

export class DateTimePipe implements PipeTransform {

    constructor(private dateService: DateService) { }

    transform(value: string | number | Date, showTime?: boolean, showTimeAgo?: boolean): string {
        return this.dateService.defaultDateFormat(value);
    }
}

@Pipe({
    name: "dateonly"
})

export class DateOnlyPipe implements PipeTransform {

    constructor(private dateService: DateService) { }

    transform(value: string | number | Date): string {
        return this.dateService.defaultDateFormat(value, true);
    }
}

@Pipe({
    name: "timeonly"
})

export class TimeOnlyPipe implements PipeTransform {

    constructor(private dateService: DateService) { }

    transform(value: string | number | Date): string {
        return this.dateService.defaultDateFormat(value, false, true);
    }
}

@Pipe({
    name: "datetimeago"
})

export class DateTimeAgoPipe implements PipeTransform {

    constructor(private dateService: DateService) { }

    transform(value: string | number | Date): string {
        return this.dateService.defaultDateFormat(value, false, false, true);
    }
}