﻿import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name: "OptionGroup"
})

export class OptionGroupPipe implements PipeTransform {
    transform(items: any[], args: string): any {
        let filtered = items.filter(item => item.group === args);
        return filtered;
    }
}
