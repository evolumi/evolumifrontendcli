import { Directive, ElementRef } from '@angular/core';
@Directive({
    selector: 'no-scroll'
})
export class InputNumberNoScroll {

    constructor(private element: ElementRef) { }

    ngOnInit() {
        this.element.nativeElement.scroll = function (event: any) {
            event.stopPropagation();
            event.preventDefault();
            this.element.nativeElement.blur();
        }
    }
}

