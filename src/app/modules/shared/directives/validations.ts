import { Directive } from '@angular/core';
import { NG_VALIDATORS, AbstractControl, ValidatorFn, Validator, FormControl } from '@angular/forms';

// EMAIL VALIDATION
function validateEmailFactory(): ValidatorFn {
    return (control: AbstractControl) => {
        let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (control.value === null || typeof control.value === 'undefined' || control.value == "") {
            return null;
        } else if (control.value.match(re)) {
            return null;
        } else {
            return { emailValidator: true };
        }
    }
}

@Directive({
    selector: '[emailValidator][ngModel],[emailValidator][formControl]',
    providers: [
        { provide: NG_VALIDATORS, useExisting: EmailValidator, multi: true }
    ]
})
export class EmailValidator implements Validator {
    validator: ValidatorFn;

    constructor() {
        this.validator = validateEmailFactory();
    }

    validate(c: FormControl) {
        return this.validator(c);
    }
}
// EMAIL VALIDATION

// CREDIT CARD VALIDATION
function validateCreditCardFactory(): ValidatorFn {
    return (control: AbstractControl) => {
        // Visa, MasterCard, American Express, Diners Club, Discover, JCB
        let ex1 = /^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$/;
        let card = control.value.match(ex1);
        if (card) {
            return null;
        } else {
            return { creditCardValidator: true };
        }
    }
}

@Directive({
    selector: '[creditCardValidator][ngModel],[creditCardValidator][formControl]',
    providers: [
        { provide: NG_VALIDATORS, useExisting: CreditCardValidator, multi: true }
    ]
})
export class CreditCardValidator implements Validator {
    validator: ValidatorFn;

    constructor() {
        this.validator = validateCreditCardFactory();
    }

    validate(c: FormControl) {
        return this.validator(c);
    }
}
// CREDIT CARD VALIDATION

// CVV VALIDATION
function validateCvvFactory(): ValidatorFn {
    return (control: AbstractControl) => {
        // Visa, MasterCard, American Express, Diners Club, Discover, JCB
        if (control.value.length != 0 && control.value.length === 3 && control.value.match(/^[0-9]+$/)) {
            return null;
        } else {
            return { cvvValidator: true };
        }
    }
}

@Directive({
    selector: '[cvvValidator][ngModel],[cvvValidator][formControl]',
    providers: [
        { provide: NG_VALIDATORS, useExisting: CvvValidator, multi: true }
    ]
})
export class CvvValidator implements Validator {
    validator: ValidatorFn;

    constructor() {
        this.validator = validateCvvFactory();
    }

    validate(c: FormControl) {
        return this.validator(c);
    }
}
// CVV VALIDATION

// PASSWORD VALIDATION
function validatePasswordFactory(): ValidatorFn {
    return (control: AbstractControl) => {
        // {6,16}           - Assert password is between 6 and 100 characters
        // (?=.*[0-9])       - Assert a string has at least one number 
        // one none letter none number
        let re = /^(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])(?=.*[!@=?#$%^&*()\-\_{\}])[a-zA-Z0-9!@=?#$%^&*!@=?#$%^&*()\-\_\{\}\[\]\;\:\'\'?\/>\.\<\`\,]{6,16}$/;
        if (control.value === null || typeof control.value === 'undefined') {
            return { passwordValidator: true };
        } else if (control.value.match(re)) {
            return null;
        } else {
            return { passwordValidator: true };
        }
    }
}

@Directive({
    selector: '[passwordValidator][ngModel],[passwordValidator][formControl]',
    providers: [
        { provide: NG_VALIDATORS, useExisting: PasswordValidator, multi: true }
    ]
})
export class PasswordValidator implements Validator {
    validator: ValidatorFn;

    constructor() {
        this.validator = validatePasswordFactory();
    }

    validate(c: FormControl) {
        return this.validator(c);
    }
}
// PASSWORD VALIDATION

// NUMBER VALIDATION
function validateNumberFactory(): ValidatorFn {
    return (control: AbstractControl) => {
        var numbers = /^[0-9]+$/;
        if (control === null || typeof control === 'undefined') {
            return { numberValidator: true };
        }
        else if (control.toString().match(numbers)) {
            return null;
        }
        else {
            return { numberValidator: true };
        }
    }
}

@Directive({
    selector: '[numberValidator][ngModel],[numberValidator][formControl]',
    providers: [
        { provide: NG_VALIDATORS, useExisting: NumberValidator, multi: true }
    ]
})
export class NumberValidator implements Validator {
    validator: ValidatorFn;

    constructor() {
        this.validator = validateNumberFactory();
    }

    validate(c: FormControl) {
        return this.validator(c);
    }
}
// NUMBER VALIDATION

// HEX VALIDATION
function validateHexFactory(): ValidatorFn {
    return (control: AbstractControl) => {
        var hexformat = /(^#[0-9A-Fa-f]{6}$)|(^#[0-9A-Fa-f]{3}$)/;
        if (control.value === null || typeof control.value === 'undefined') {
            return { hexValidator: true };
        } else if (control.value.match(hexformat)) {
            return null;
        } else {
            return { hexValidator: true };
        }
    }
}

@Directive({
    selector: '[hexValidator][ngModel],[hexValidator][formControl]',
    providers: [
        { provide: NG_VALIDATORS, useExisting: HexValidator, multi: true }
    ]
})
export class HexValidator implements Validator {
    validator: ValidatorFn;

    constructor() {
        this.validator = validateHexFactory();
    }

    validate(c: FormControl) {
        return this.validator(c);
    }
}
// HEX VALIDATION