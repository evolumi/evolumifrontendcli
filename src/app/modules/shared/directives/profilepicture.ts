import { Directive, Input, SimpleChange } from '@angular/core';
import { UsersService } from '../../../services/users.service';
import { Subscription } from 'rxjs/Subscription';

@Directive({
  selector: 'img[profilepicture]',
  host: {
    '(error)': 'error()',
    '[src]': 'src',
  }
})
export class ProfilePicture {
  @Input() src: string;
  @Input() profilepicture: string;

  private userSub: Subscription;
  private default: string = '/assets/images/avatar.jpg';
  private profilePic: string;

  constructor(private userService: UsersService) {
    this.src = this.default;
  }

  ngOnChanges(change: SimpleChange) {
    if (this.profilepicture) {
      this.userSub = this.userService.usersObservable().subscribe(res => {
        if (res.length) {
          let user = res.find(x => x.Id == this.profilepicture);
          if (user) {
            this.src = user.ProfilePictureUrl ? this.smallUrl(user.ProfilePictureUrl) : this.default;
            this.profilePic = user.ProfilePictureUrl;
          }
          this.ngOnDestroy();
        }
      });
    }
  }

  error() {
    this.src = this.profilePic || this.default;
  }

  smallUrl(url: string): string {

    let index = url.indexOf('?');
    let newUrl: string;

    if (index != -1) {
      var strings = url.split('?');
      newUrl = strings[0] + "-small?" + strings[1];
    }
    else {
      newUrl = url + "-small";
    }
    return newUrl;
  }

  ngOnDestroy() {
    if (this.userSub) this.userSub.unsubscribe();
  }
}