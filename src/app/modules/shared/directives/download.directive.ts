import { Directive, Input } from '@angular/core';
import { FileModel } from '../../../models/file.models';
import { LocalStorage } from '../../../services/localstorage';
import { Api } from '../../../services/api';
// import { WindowRef } from './WindowRef';

@Directive({
    selector: '[downloadFile]',
    host: {
        '(click)': 'onDownload()'
    }
})
export class DownloadFileDirective {

    @Input() downloadFile: FileModel;

    constructor(private api: Api,
        private local: LocalStorage) { }

    ngOnInit() {
    }

    onDownload() {
        if ((<any>this.downloadFile).id) {
            this.downloadFile = this.capitalizeProperties(this.downloadFile);
        }
        const orgId = this.local.get('orgId');
        let a = document.createElement("a") as HTMLAnchorElement;
        document.body.appendChild(a);
        a.style.display = 'none';
        this.api.postDownloadFile(`Organisations/${orgId}/DownloadFile`, this.downloadFile)
            .subscribe(data => {
                const blob = new Blob([data], { type: 'application/octet-stream' });
                const url = window.URL.createObjectURL(blob);
                a.href = url;
                a.download = this.downloadFile.Name;
                a.click();
                window.URL.revokeObjectURL(url);
                a.parentNode.removeChild(a);
            });
    }

    private capitalizeProperties(obj: any): any {
        for (let prop in obj) {
            let capProp = prop.replace(/\w\S*/g, txt => txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase());
            obj[capProp] = obj[prop];
        }
        return obj;
    }
}