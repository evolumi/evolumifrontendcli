import { Directive, Input } from '@angular/core';

@Directive({
    selector: "[clickoutside]"
})
export class ClickOutsideDirective {
    @Input() outsideclick: () => any;
    @Input() clickoutside: boolean;

    constructor() { }
    clickedOutside() {
        if (typeof this.outsideclick == "function") {
            this.outsideclick();
        }
    }
}