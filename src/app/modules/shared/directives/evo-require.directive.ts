import { Directive, Input, Output, EventEmitter } from '@angular/core';
import { OrgService, Permissions, Features } from '../../../services/organisation.service';

@Directive({
    selector: '[evo-require]',
    host: {
        '[hidden]': 'hidden',
        '[class.locked]': 'locked'
    }
})
export class EvoRequireDirective {
    @Input() permissions: Array<Permissions | string>;
    @Input() features: Array<Features | string>;
    @Output() onFail: EventEmitter<String> = new EventEmitter();
    private hidden: boolean;
    private locked: boolean;

    constructor(private orgService: OrgService) {

    }
    ngOnChanges() {

        if (this.permissions && this.permissions.length) {

            this.permissions.forEach(p => {
                let str = typeof (p) == 'string' ? p : Permissions[<Permissions>p];
                if (p == Permissions.EvolumiAdmin || p == Permissions[Permissions.EvolumiAdmin]) {
                    this.locked = true;
                }
                if (!this.orgService.hasPermission(p)) {
                    this.onFail.next(str);
                    this.hidden = true;
                    console.log("[Evo-Require] Permission [" + str + "] not found. Hiding");
                }
            });

        }

        if (this.features && this.features.length) {
            this.features.forEach(f => {
                let str = typeof (f) == 'string' ? f : Features[<Features>f];
                if (!this.orgService.hasFeature(f)) {
                    this.onFail.next(str);
                    this.hidden = true;
                    console.log("[Evo-Require] Feature [" + str + "] not found. Hiding");
                }
            });
        }
    }
}