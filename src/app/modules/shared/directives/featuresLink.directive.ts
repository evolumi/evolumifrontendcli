import { Directive, Input } from '@angular/core';
import { PackagesCollection } from '../../../services/packages.service';

@Directive({
    selector: '[featureLink]',
    host:
    {
        '(click)': 'clickFeature()'
    }
})
export class FeatureLinkDirective {

    @Input() featureLink: string;

    constructor(private _packageService: PackagesCollection) {
    }

    clickFeature() {
        this._packageService.loadFeature(this.featureLink);
    }
}