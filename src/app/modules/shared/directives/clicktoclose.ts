import { Directive, ElementRef, Output, EventEmitter } from '@angular/core';

@Directive({
    selector: "click-to-close",
    host: {
        "(document: click)": "onClick($event)"
    }
})
export class ClickToClose {
    @Output() clickOutSide: EventEmitter<any> = new EventEmitter();
    constructor(public elementRef: ElementRef) { }
    onClick(globalEvent: any) {
        if (this.eventTriggeredInsideHost(globalEvent)) {
            return;
        }
        this.clickOutSide.emit(globalEvent);
    }
    eventTriggeredInsideHost(event: any) {
        var current = event.target;
        var host = this.elementRef.nativeElement;
        do {
            if (current === host) {
                return (true);
            }
            current = current.parentNode;
        } while (current);
        return (false);
    }
}