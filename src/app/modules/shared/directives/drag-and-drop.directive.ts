import { Directive, Input, OnInit, OnDestroy, Renderer, ElementRef } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { DragAndDropService } from '../../../services/drag-and-drop.service';

// export const HoverClassPlaceholder: string = ' drag-hover-default'; // for placeholder containers

@Directive({
    selector: '[draganddrop]'
})
export class DragAndDropDirective implements OnInit, OnDestroy {

    @Input() draganddrop: string; // matching containernames (could be made to Array for multiple)
    @Input() drop: boolean = false;
    @Input() drag: boolean = true;
    @Input() dragArr: Array<any>;
    @Input() index: number = 0; // the actual index of the element in the dragArr
    @Input() copy: boolean = false; // if you want the item to get copied or replaced
    @Input() placeholderClass = 'drag-placeholder-container';
    @Input() redactorClass = 'drag-redactor-container';
    @Input() hoverTop = ' drag-hover-top';
    @Input() hoverBottom = ' drag-hover-bottom';
    @Input() hoverPlaceholder = ' drag-hover-default';
    @Input() dragDisabled = false;

    private hostElement: HTMLElement;
    private hoverTopElement: HTMLElement;
    private hoverBottomElement: HTMLElement;

    private counter: number = 0; // because child elements fire enter, leave event, this is to keep track

    private listeners: Array<Function> = [];

    private currentDragArray: Array<any>;

    private dragArrSub: Subscription;

    constructor(private renderer: Renderer,
        private elRef: ElementRef,
        private dragSvc: DragAndDropService) {
    }

    ngOnInit() {
        if (!this.dragDisabled) {
            this.dragArrSub = this.dragSvc.dragArrayObs()
                .subscribe(arr => this.currentDragArray = arr);
        }
    }

    ngAfterViewInit() {
        if (!this.dragDisabled) {
            this.hostElement = this.elRef.nativeElement;
            this.hoverTopElement = <HTMLElement>this.hostElement.querySelectorAll('.hover-drop.top')[0];
            this.hoverBottomElement = <HTMLElement>this.hostElement.querySelectorAll('.hover-drop.bottom')[0];
            this.registerEventHandlers();
        }
    }

    ngOnDestroy() {
        if (this.listeners) this.listeners.map(x => x());
        if (this.dragArrSub) this.dragArrSub.unsubscribe();
    }

    private registerEventHandlers() {
        if (this.drop) {
            // dragover
            this.listeners.push(this.renderer.listen(this.hostElement, 'dragover', (event: DragEvent) => {
                event.preventDefault();
                if (this.dragSvc.checkIfSameDrag(this.draganddrop)) {
                    event.stopPropagation();
                    if (this.hostElement.className.indexOf(this.placeholderClass) === -1) {
                        const isTop = this.isTopOfElement(this.hostElement, event, this.hostElement.className.indexOf(this.hoverTop) === -1 ? -1 : 1);
                        if (isTop && this.hostElement.className.indexOf(this.hoverTop) === -1) {
                            this.hostElement.className = this.hostElement.className.replace(this.hoverBottom, '');
                            this.hostElement.className += this.hoverTop;
                            this.setVisibleHover('top');
                        } else if (!isTop && this.hostElement.className.indexOf(this.hoverBottom) === -1) {
                            this.hostElement.className = this.hostElement.className.replace(this.hoverTop, '');
                            this.hostElement.className += this.hoverBottom;
                            this.setVisibleHover('bottom');
                        }
                    }
                }
            }));
            // dragenter
            this.listeners.push(this.renderer.listen(this.hostElement, 'dragenter', (event: DragEvent) => {
                if (this.dragSvc.checkIfSameDrag(this.draganddrop)) {
                    if (this.hostElement.className.indexOf(this.placeholderClass) > -1) {
                        this.hostElement.className += this.hoverPlaceholder;
                    } else {
                        this.counter++;
                        const isTop = this.isTopOfElement(this.hostElement, event);
                        if (isTop && this.hostElement.className.indexOf(this.hoverTop) === -1) {
                            this.hostElement.className += this.hoverTop;
                            this.hostElement.className = this.hostElement.className.replace(this.hoverBottom, '');
                            this.setVisibleHover('top');
                        } else if (!isTop && this.hostElement.className.indexOf(this.hoverBottom) === -1) {
                            this.hostElement.className += this.hoverBottom;
                            this.hostElement.className = this.hostElement.className.replace(this.hoverTop, '');
                            this.setVisibleHover('bottom');
                        }
                    }
                }
            }));
            // dragleave
            this.listeners.push(this.renderer.listen(this.hostElement, 'dragleave', (event: DragEvent) => {
                if (this.dragSvc.checkIfSameDrag(this.draganddrop)) {
                    if (this.hostElement.className.indexOf(this.placeholderClass) > -1) {
                        this.hostElement.className = this.hostElement.className.replace(this.hoverPlaceholder, '');
                    } else {
                        this.counter--;
                        if (this.counter === 0) {
                            this.hostElement.className = this.hostElement.className.replace(this.hoverTop, '').replace(this.hoverBottom, '');
                            this.setVisibleHover();
                        }
                    }
                }
            }));
            // drop
            this.listeners.push(this.renderer.listen(this.hostElement, 'drop', (event: DragEvent) => {
                event.preventDefault();
                if (this.dragSvc.checkIfSameDrag(this.draganddrop)) {
                    event.stopPropagation();
                    const dragIndex = Number(event.dataTransfer.getData('index'));
                    const dragCopy = event.dataTransfer.getData('copy') === 'true';
                    // if is placeholder remove the placeholder otherwise insert new object
                    if (this.hostElement.className.indexOf(this.placeholderClass) > -1) {
                        this.dragArr.splice(this.index, 1, JSON.parse(JSON.stringify(this.currentDragArray[dragIndex])));
                        this.dragSvc.onDrop(this.dragArr[this.index]);
                        if (!dragCopy) {
                            this.currentDragArray.splice(dragIndex, 1);
                        }
                    } else {
                        // insert index depends on where you dropped it
                        const insertIndex = this.isTopOfElement(this.hostElement, event) ? this.index : this.index + 1;
                        // if same array replace the object otherwise only insert it
                        if (this.dragArr === this.currentDragArray) {
                            this.dragArr.splice(insertIndex, 0, JSON.parse(JSON.stringify(this.currentDragArray[dragIndex])));
                            this.dragSvc.onDrop(this.dragArr[insertIndex]);
                            if (insertIndex > dragIndex) {
                                this.dragArr.splice(dragIndex, 1);
                            } else {
                                this.dragArr.splice(dragIndex + 1, 1);
                            }
                        } else {
                            if (insertIndex >= this.dragArr.length) {
                                this.dragArr[insertIndex] = JSON.parse(JSON.stringify(this.currentDragArray[dragIndex]));
                            } else {
                                this.dragArr.splice(insertIndex, 0, JSON.parse(JSON.stringify(this.currentDragArray[dragIndex])));
                            }
                            this.dragSvc.onDrop(this.dragArr[insertIndex]);
                            if (!dragCopy) {
                                this.currentDragArray.splice(dragIndex, 1);
                            }
                        }
                        this.setVisibleHover();
                        this.counter = 0;
                        this.hostElement.className = this.hostElement.className.replace(this.hoverTop, '').replace(this.hoverBottom, '');
                    }
                }
            }));
        }

        if (this.drag) {
            // To avoid placeholders to be draggable
            if (this.hostElement.className.indexOf(this.placeholderClass) === -1) {
                this.renderer.setElementAttribute(this.hostElement, 'draggable', 'true');

                // dragstart
                this.listeners.push(this.renderer.listen(this.hostElement, 'dragstart', (event: DragEvent) => {
                    event.stopPropagation();
                    this.dragSvc.onStartDrag(this.draganddrop, this.dragArr);
                    event.dataTransfer.setData('index', this.index.toString());
                    event.dataTransfer.setData('copy', this.copy.toString())
                }));

                // dragend
                this.listeners.push(this.renderer.listen(this.hostElement, 'dragend', (event: DragEvent) => {
                    event.stopPropagation();
                    this.dragSvc.onEndDrag();
                    this.counter = 0;
                }));
            }
        }
    }

    private isTopOfElement(element: HTMLElement, event: any, offsetHeight?: number): boolean {
        let heightEl = element.offsetHeight;
        let rect = element.getBoundingClientRect();
        let center = (heightEl / 2) + rect.top;
        return event.clientY < center;
    }

    private setVisibleHover(dir?: string) {
        if (dir === 'top') {
            this.hoverTopElement.style.display = 'block';
            this.hoverBottomElement.style.display = 'none';
        } else if (dir === 'bottom') {
            this.hoverBottomElement.style.display = 'block';
            this.hoverTopElement.style.display = 'none';
        } else if (!dir) {
            this.hoverBottomElement.style.display = 'none';
            this.hoverTopElement.style.display = 'none';
        }
    }
}