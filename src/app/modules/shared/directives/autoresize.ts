import { ElementRef, HostListener, Directive } from '@angular/core';

@Directive({
  selector: 'textarea[autosize]'
})

export class Autosize {
  @HostListener('input', ['$event.target'])
  onInput(textArea: any) {
    this.adjust();
  }
  constructor(public element: ElementRef) {
    this.element.nativeElement.style.height = '20px';
  }
  ngOnInit() {
    this.adjust();
  }
  ngAfterViewChecked() {
    this.adjust();
  }
  adjust() {
    this.element.nativeElement.style.height = 'auto';
    this.element.nativeElement.style.minHeight = '30px';
    this.element.nativeElement.style.height = this.element.nativeElement.scrollHeight + "px";
  }
}
