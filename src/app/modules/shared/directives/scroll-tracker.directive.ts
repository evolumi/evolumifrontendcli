import { Directive, HostListener, Output, Input, EventEmitter } from '@angular/core';

@Directive({
  selector: '[scrollTracker]'
})
export class ScrollTrackerDirective {
  @Output() scrollBottom: EventEmitter<any> = new EventEmitter();
  @Input() scrollStart: number;

  private isAtBottom: boolean = false;

  @HostListener('scroll', ['$event'])
  onScroll(event: any) {

    let tracker = event.target;

    let limit = tracker.scrollHeight - tracker.clientHeight;
    if (event.target.scrollTop >= limit - (this.scrollStart || 1) && this.isAtBottom == false) {
      this.isAtBottom = true;
      this.scrollBottom.emit(true);
    }

    if (event.target.scrollTop < limit - (this.scrollStart || 1)) {
      this.isAtBottom = false;
    }
  }

  constructor() { }
}