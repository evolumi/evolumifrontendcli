import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { TranslateModule } from '@ngx-translate/core';
import { TabsModule, TooltipModule } from 'ngx-bootstrap';
import { CelebrationModule } from "./components/celebrations/celebration.module";
import { ChartModule } from 'angular2-highcharts';
import { ImageCropperModule } from 'ng2-img-cropper';
import { ColorPickerModule } from 'ngx-color-picker';
import { CookieModule } from 'ngx-cookie';

// Components
import { Chosen } from './components/chosen/chosen';
import { Click2CallControl, Click2CallPhoneNumber } from './components/click2call/click2call.component';
import { Click2MailComponent } from './components/click2mail/click2mail.component';
import { InfoComponent } from './components/information/information.component';
import { HelpComponent } from './components/help/help.component';
import { Loader } from './components/loader/loader';
import { Newsfeeds } from './components/newsfeed/newsfeed.component';
import { NewsItemComponent } from './components/newsfeed/newsitem.component';
import { EditNotes } from './components/newsfeed/edit/news.edit.component';
import { Likecomment } from './components/newsfeed/likecomment/likecomment.component';
import { AddNotes } from './components/newsfeed/notesblock/notes.component';
import { Notification } from './components/notification/notification';
import { PasswordComponent } from './components/password/password.component';
import { QuickAddModal } from './components/quick-actions/quick-add-modal';
import { ReminderSelectComponent } from './components/reminder/reminder';
import { TagEditorComponent } from './components/tag-editor/tag-editor.component';
import { ActionTypeComponent } from './components/actiontype/action-type.component';
import { Dragula } from '../../draggable/directives/dragula.directive';
import { VoidComponent } from './components/void';
import { CropModal } from "./components/cropmodal/cropmodal";
import { ConfirmBox } from "./components/confirmbox/confirmbox";
import { ProductPicker } from "./components/productpicker/productpicker";
import { QuickSelect } from "./components/quickselect/quickselect";
import { PdfViewerComponent } from 'ng2-pdf-viewer';
import { QuickActionLinks } from "./components/quickActionLinks/quickaction-links";
import { BubblePopup } from "./components/bubble-popup/bubble-popup";
import { HighchartComponent, HighchartMultiComponent } from './components/highchart/highchart.component';
import { HighchartFilterComponent } from './components/highchart/highchart.filter.component';
import { ItemListComponent } from './components/highchart/item-list.component';
import { AuthPopupComponent } from './components/auth-popup/auth-popup.component';
import { ButtonPanelComponent } from './components/button-panel/button-panel.component';
import { AccountSelectComponent } from './components/account-select/account-select.component'
import { EvoDatepickerComponent } from './components/evo-datepicker/evo-datepicker.component';
import { CustomFieldSettingsComponent } from './components/custom-field-settings/custom-fields-settings.component'
import { TagSelectComponent } from './components/tag-select/tag-select.component';
import { HighchartColumnComponent } from "./components/highchart/highchart-column.component";
import { ProgressChartComponent } from "./components/highchart/progress-chart.component";
import { HighchartModalComponent } from "./components/highchart/highchart-modal.component";
import { ExportComponent } from "./components/export/export.component";
import { TemplateSaveComponent } from "./components/export/save-template.component";
import { TimelineActionComponent } from './components/timeline-action/timeline-action.component';
import { DotlineComponent } from './components/dotline/dotline.component';
import { DotmenuComponent } from './components/dotmenu/dotmenu.component';
import { AddUserComponent } from "../crm/settings/users/add/add-user.component";
import { DealsByAccountComponent } from "../crm/deals/dealsblock/deals-block.component";

// // Directives
import { Autosize } from './directives/autoresize';
import { ProfilePicture } from './directives/profilepicture';
import { ClickToClose } from './directives/clicktoclose';
// import { FocusDirective } from './directives/focus.directive';
import { ScrollTrackerDirective } from './directives/scroll-tracker.directive'
import { NumberValidator, HexValidator, PasswordValidator, CvvValidator, CreditCardValidator, EmailValidator } from './directives/validations';
import { InputNumberNoScroll } from "./directives/number-no-scroll";
import { DownloadFileDirective } from './directives/download.directive';
import { ClickOutsideDirective } from './directives/clickoutside.directive';
import { EvoRequireDirective } from './directives/evo-require.directive';
import { FeatureLinkDirective } from './directives/featuresLink.directive';
import { DragAndDropDirective } from './directives/drag-and-drop.directive';
import { OnlyNumber } from './directives/OnlyNumber';

// // Pipes
import { BigNumberPipe } from './pipes/bignumber';
import { OptionGroupPipe } from './pipes/OptionGroup';
import { CatalogueFilter } from "./pipes/CatalogueFilter";
import { UrlPipe } from './pipes/url-pipe';
import { BeautifyRangePipe, SortRangesPipe } from './pipes/PriceRangePipe';
import { DateTimePipe, DateOnlyPipe, TimeOnlyPipe, DateTimeAgoPipe } from './pipes/date-time.pipe';
import { ShortStringPipe } from './pipes/short-string.pipe';
import { UsernamePipe } from './pipes/username.pipe';
import { HasPropertyPipe } from './pipes/has-property.pipe';
import { SafeHtmlPipe } from "./pipes/safe-html.pipe";

@NgModule({
    imports: [
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        RouterModule,
        HttpModule,
        TranslateModule, 
        TabsModule,
        TooltipModule,
        ColorPickerModule,
        CelebrationModule,
        ChartModule,
        ImageCropperModule,
        CookieModule
    ],
    declarations: [
        Chosen,
        Click2CallControl,
        Click2CallPhoneNumber,
        Click2MailComponent,
        HelpComponent,
        InfoComponent,
        Loader,
        Newsfeeds,
        NewsItemComponent,
        EditNotes,
        Likecomment,
        AddNotes,
        Notification,
        PasswordComponent,
        ReminderSelectComponent,
        QuickAddModal,
        TagEditorComponent,
        Autosize,
        ClickToClose,
        BigNumberPipe,
        OptionGroupPipe,
        ActionTypeComponent,
        ScrollTrackerDirective,
        ProfilePicture,
        Dragula,
        VoidComponent,
        CatalogueFilter,
        UrlPipe,
        BeautifyRangePipe,
        SortRangesPipe,
        CropModal,
        ConfirmBox,
        NumberValidator,
        HexValidator,
        PasswordValidator,
        CvvValidator,
        CreditCardValidator,
        EmailValidator,
        ProductPicker,
        QuickSelect,
        InputNumberNoScroll,
        DateTimePipe,
        DateOnlyPipe,
        TimeOnlyPipe,
        DateTimeAgoPipe,
        ShortStringPipe,
        UsernamePipe,
        HasPropertyPipe,
        PdfViewerComponent,
        DownloadFileDirective,
        QuickActionLinks,
        BubblePopup,
        EvoRequireDirective,
        HighchartComponent,
        HighchartFilterComponent,
        HighchartMultiComponent,
        ItemListComponent,
        ClickOutsideDirective,
        AuthPopupComponent,
        FeatureLinkDirective,
        ButtonPanelComponent,
        OnlyNumber,
        AccountSelectComponent,
        EvoDatepickerComponent,
        CustomFieldSettingsComponent,
        TagSelectComponent,
        HighchartColumnComponent,
        ProgressChartComponent,
        HighchartModalComponent,
        ExportComponent,
        TemplateSaveComponent,
        SafeHtmlPipe,
        TimelineActionComponent,
        DragAndDropDirective,
        DotmenuComponent,
        DotlineComponent,
        AddUserComponent,
        DealsByAccountComponent
    ],
    exports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule,
        HttpModule,
        TranslateModule,
        TabsModule,
        TooltipModule,
        ImageCropperModule,
        Chosen,
        Click2CallControl,
        Click2CallPhoneNumber,
        Click2MailComponent,
        HelpComponent,
        InfoComponent,
        Loader,
        Newsfeeds,
        NewsItemComponent,
        EditNotes,
        Likecomment,
        AddNotes,
        Notification,
        PasswordComponent,
        ReminderSelectComponent,
        QuickAddModal,
        TagEditorComponent,
        Autosize,
        ClickToClose,
        ActionTypeComponent,
        ChartModule,
        ScrollTrackerDirective,
        ProfilePicture,
        Dragula,
        ColorPickerModule,
        CropModal,
        ConfirmBox,
        NumberValidator,
        HexValidator,
        PasswordValidator,
        CvvValidator,
        CreditCardValidator,
        EmailValidator,
        ProductPicker,
        QuickSelect,
        InputNumberNoScroll,
        PdfViewerComponent,
        DownloadFileDirective,
        QuickActionLinks,
        BubblePopup,
        CelebrationModule,
        HighchartComponent,
        HighchartFilterComponent,
        HighchartMultiComponent,
        ItemListComponent,
        EvoRequireDirective,
        FeatureLinkDirective,
        ButtonPanelComponent,
        OnlyNumber,
        ClickOutsideDirective,
        AuthPopupComponent,
        AccountSelectComponent,
        EvoDatepickerComponent,
        CustomFieldSettingsComponent,
        TagSelectComponent,
        HighchartColumnComponent,
        ProgressChartComponent,
        HighchartModalComponent,
        ExportComponent,
        TemplateSaveComponent,
        TimelineActionComponent,
        DragAndDropDirective,
        DateTimePipe,
        DateOnlyPipe,
        DateTimeAgoPipe,
        DateTimePipe,
        UsernamePipe,
        BigNumberPipe,
        OptionGroupPipe,
        CatalogueFilter,
        UrlPipe,
        BeautifyRangePipe, SortRangesPipe,
        DateTimePipe, DateOnlyPipe, TimeOnlyPipe, DateTimeAgoPipe,
        ShortStringPipe,
        UsernamePipe,
        HasPropertyPipe,
        SafeHtmlPipe,
        DotmenuComponent,
        DotlineComponent,
        AddUserComponent,
        DealsByAccountComponent
    ],
    entryComponents: [
        HighchartMultiComponent
    ]
})
export class SharedModule {

}