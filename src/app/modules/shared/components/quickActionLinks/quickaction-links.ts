import { Component, Output, EventEmitter } from "@angular/core";
import { Router } from "@angular/router";
import { ActionsCollection, ActionGroup } from "../../../../services/actionservice";
import { DealAddEdit } from "../../../../services/dealAddEdit.service";
import { AccountsCollection } from "../../../../services/accounts.service";
import { DealsModel, Address } from "../../../../models/deals.models";

@Component({

    selector: "quickaction-links",
    templateUrl: "quickaction-links.html",
    styleUrls: ["quickaction-links.css"]
})

export class QuickActionLinks {

    public hasAccounts: boolean = false;

    @Output() close: EventEmitter<any> = new EventEmitter();

    constructor(
        private actionService: ActionsCollection,
        private dealAddEdit: DealAddEdit,
        private accountSvc: AccountsCollection,
        private router: Router,
    ) {
    }

    ngOnInit() {
        this.accountSvc.companyList("", 1, (data: any) => {
            if (data.length > 0) {
                this.hasAccounts = true;
            }
        });
    }

    addAccountModal() {
        this.close.next();
        this.router.navigate(["CRM/Accounts/AddAccount"]);
    }

    addActionModal() {
        if (this.hasAccounts) {
            this.actionService.selectedGroup = new ActionGroup();
            this.close.next();
        }
    }

    addContactModal() {
        if (this.hasAccounts) {
            this.close.next();
            this.router.navigate(["CRM/PersonAccounts/AddAccount"]);
        }
    }

    addDealModal() {
        if (this.hasAccounts) {
            let newdeal = new DealsModel();
            newdeal.ShippingAddress = new Address();
            newdeal.BillingAddress = new Address();
            this.dealAddEdit.showDeal(newdeal);
            this.close.next();
        }
    }
}