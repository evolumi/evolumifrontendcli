import { Component, Output, Input, EventEmitter } from '@angular/core';
import { ActionEvents, ActionTypes, ActionStatuses } from '../../../../services/actionservice';
import { ActionsModel } from '../../../../models/actions.models';
import { AccountsCollection } from '../../../../services/accounts.service';
import { ModalService } from '../../../../services/modal.service';
import { OrgService, Features } from '../../../../services/organisation.service';

@Component({
    selector: 'timeline-action',
    templateUrl: './timeline-action.component.html',
    styleUrls: ['./timeline.css']
})
export class TimelineActionComponent {

    @Input() action: ActionsModel;
    @Input() dateField: string;
    @Output() onEdit: EventEmitter<any> = new EventEmitter();
    @Output() onClick: EventEmitter<boolean> = new EventEmitter();
    @Input() hideConnection: boolean = false;
    @Input() hideEdit: boolean = false;
    @Input() selected: boolean = false;
    @Input() hideTimelineInfo: boolean = false;

    public expanded: boolean = false;
    public showComment: boolean = false;
    public showLabels: boolean = false;
    private emailTracking: boolean = false;

    constructor(private accountService: AccountsCollection, private modal: ModalService, private orgService: OrgService) {
        this.emailTracking = this.orgService.hasFeature(Features.EmailTracking);
    }

    getStatus(status: string) {
        var s = ActionStatuses.find((x: any) => x.value == status);
        return s ? s.label : "";
    }

    getType(type: string) {
        var t = ActionTypes.find((x: any) => x.value == type);
        return t ? t.label : "";
    }

    getEvent(event: string) {
        var l = ActionEvents.find((x: any) => x.value == event);
        return l ? l.label : "";
    }

    edit() {
        this.onEdit.emit();
    }

    expand() {
        if (!this.selected) {
            this.expanded = !this.expanded;
            this.onClick.emit(this.expanded);
        }
    }

    ngOnChanges(changes: any) {
        if (changes.selected) {
            if (!this.selected)
                this.expanded = false;
        }
        if (this.action.History.length) {
            this.action.History = this.action.History.sort(function (x, y) {
                return x.Date < y.Date ? 1 : 0;
            });
        }
        if (changes.action && changes.action.currentValue != null && this.action.icon == null) {
            this.action.icon = this.getIcon(this.action.Type);
        }
    }

    getIcon(actionType: string): string {
        if (actionType == null) return "";
        return ActionTypes.find(x => x.value == actionType).icon;
    }

    mouseover(icon: string) {
        if (icon == "Comments")
            this.showComment = true;
        if (icon == "Labels")
            this.showLabels = true;
    }

    mouseout() {
        this.showComment = false;
        this.showLabels = false;
    }

    clickMail() {
        console.log("[Timeline] Clicking Email for action", this.action);
        this.modal.showMailModal(this.action.Account.Email, this.action.Account, this.action.ActionGroupId);
    }

    clickContact(contact: any) {
        this.accountService.getRelationFromAccountIds(this.action.Account.Id, contact.Id).subscribe(res => {
            if (res.IsSuccess) {
                res.Data.currentAccount = this.action.Account;
                res.Data.actionGroupId = this.action.ActionGroupId;
                this.accountService.showAccountRelationPopup(res.Data);
            }
        })
    }
}