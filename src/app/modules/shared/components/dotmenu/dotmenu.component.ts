import { Component, Input } from '@angular/core';
import { DotmenuOption } from "./DotmenuOption";

@Component({
    selector: 'dotmenu',
    templateUrl: './dotmenu.html',
    styleUrls: ['./dotmenu.css'],
    host: {'(document:click)' : 'onClick($event)'}
})

export class DotmenuComponent {
    @Input() options: DotmenuOption[];

    public menuOpen: boolean;

    constructor() { }

    ngOnInit() {

    }

    toggleMenu(){
        this.menuOpen = !this.menuOpen;
    }
    onClick(ev: any) {
        if (this.menuOpen && ev.target.className.indexOf("dotmenuBtn") == -1) {
            this.menuOpen = false;
        }
    }


}