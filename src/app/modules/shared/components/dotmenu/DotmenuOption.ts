export class DotmenuOption {

    public Label: string;
    public Function: () => any;
    public Icon: string;
    
    [key: string]:any;

    constructor(obj: any = null) {
        for(let key in obj) {
            if(obj.hasOwnProperty(key)) {
                this[key] = obj[key]
            }
        }
    }
}