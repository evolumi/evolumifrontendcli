import { Component } from "@angular/core";
declare var Intercom: any

@Component({
    selector: "help-component",
    templateUrl: "./help.component.html",
    styleUrls: ["help.component.css"]

})
export class HelpComponent {

    public isExtended: boolean = false;

    public helpContainerStatus = "helpContainerClosed";

    public toggleHelp = function () {
        if (this.helpContainerStatus == "helpContainerClosed") {
            this.helpContainerStatus = "helpContainerOpen";
        }
        else {
            this.helpContainerStatus = "helpContainerClosed";
        }
    }

    expand() {
        this.isExtended = true;
    }
    collapse() {
        this.isExtended = false;
    }

    public openChat = function () {
        Intercom("boot");
    }

}