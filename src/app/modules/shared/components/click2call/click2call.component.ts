import { Component, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { CallService } from '../../../../services/callService';
import { AuthService } from '../../../../services/auth-service';
import { Subscription } from 'rxjs/Subscription';
import { ButtonPanelService } from '../../../../services/button-panel.service';
import { Shortcuts } from '../../../../services/configuration';

@Component({

    selector: 'click2call-control',
    templateUrl: './click2call-control.html',
})

export class Click2CallControl {
    public displayModal: boolean;
    public number: string;

    public textArea = new FormControl();
    private sub: Subscription;
    private buttonSub: Subscription;

    constructor(
        public auth: AuthService,
        public callService: CallService,
        private button: ButtonPanelService
    ) {

        if (auth.isEvolumi) {
            this.button.addHeaderButton("PHONECALL", "phone", "phone", Shortcuts.phone);
            this.buttonSub = this.button.commandObservable().subscribe(command => {
                if (command == "phone") {
                    this.toggleModal();
                }
            })
        }


        this.sub = this.textArea.valueChanges.debounceTime(1200).distinctUntilChanged().subscribe(text => {
            console.log(text);
            this.callService.updateActionComment(text);
        });
    }

    ngOnInit() {

    }

    ngOnDestroy() {
        if (this.sub) this.sub.unsubscribe();
        if (this.buttonSub) this.buttonSub.unsubscribe();
        this.button.removeButton("phone");
    }

    public call() {
        let user = localStorage.getItem("userId");
        this.callService.startPhoneCall(this.number, user, "", "", "");
    }

    public toggleModal() {
        this.displayModal = !this.displayModal;
    }

    public showModal(): void {
        this.displayModal = true;
    }

    public hideModal(): void {
        this.displayModal = false;
    }

    public hangUp() {
        console.log("[Click2Call- Control] Trying to Hangup");
        this.callService.hangUp();
    }
}


@Component({

    selector: 'click2call',
    templateUrl: 'click2call.html',
})
export class Click2CallPhoneNumber {
    @Input() number: string;
    @Input() showIcon: boolean = true;
    @Input() iconFixedWidth: boolean = false;
    @Input() account: string = "";
    @Input() contact: string = "";
    @Input() action: string = "";
    private userId: string = "";

    constructor(
        public auth: AuthService,
        public callService: CallService
    ) {

    }

    public call() {
        this.callService.startPhoneCall(this.number, this.userId, this.contact, this.account, this.action);
    }

    ngOnInit() {
        this.userId = localStorage.getItem("userId");
    }

    ngOnDestroy() {

    }
}
