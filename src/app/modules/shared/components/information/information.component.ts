import { Component } from "@angular/core";
import { Router } from '@angular/router';
import { NotificationService, Information } from '../../../../services/notification-service';
import { Subscription } from 'rxjs/Subscription';

@Component({
    selector: "info-component",
    templateUrl: "./information.component.html",
    styleUrls: ['information.component.css']

})
export class InfoComponent {

    private infoSub: Subscription;
    private info: Information
    public showing: boolean = false;
    public forcedExtend: boolean = false;

    public externalLink: string;

    private queu: Array<Information> = [];

    constructor(private notify: NotificationService, private router: Router) {
        this.infoSub = this.notify.informationObservable().subscribe(info => {
            if (!this.showing) {
                this.showInfo(info);

                if (!this.forcedExtend && info.urgent) {
                    this.isExtended = true;
                    this.forcedExtend = true;
                }
            }
            else if (this.info.message != info.message) {
                if (info.urgent) {
                    this.queu.push(this.info);
                    this.showInfo(info);
                    if (!this.forcedExtend) {
                        this.isExtended = true;
                        this.forcedExtend = true;
                    }
                }
                else {
                    this.queu.push(info);
                }
            }
        });
    }

    private showInfo(info: Information) {
        if (info.link.indexOf("http") != -1) {
            this.externalLink = info.link;
        }
        this.info = info;
        this.showing = true;
    }

    public isExtended: boolean = false;

    public close() {
        if (this.queu.length) {
            let i = this.queu.shift();
            this.showInfo(i);
        }
        else {
            this.isExtended = false;
            this.showing = false;
        }
    }

    public expand() {
        this.isExtended = true;
    }

    public collapse() {
        this.isExtended = false;
    }

    public navigate(link: string) {
        if (link) {
            this.router.navigate([link]);
        }
        else {
            window.location.reload();
        }
    }
}