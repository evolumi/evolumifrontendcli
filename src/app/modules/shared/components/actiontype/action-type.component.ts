import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ActionType, ActionTypes } from "../../../../services/actionservice";


@Component({

    selector: 'action-type',
    template: ` <div class="action-type-filter" [class.small]="!showLabels">
                    <div [class.hideTooltip]="showLabels">
                        <label *ngFor="let type of activeTypes" [class.active]="types[type.value]" (click)="toggle(type.value)" tooltip="{{type.label | translate }}" placement="bottom">
                            <i class="fa fa-{{type.icon}}"></i>
                            {{ showLabels ? ( type.label | translate) : '' }}
                        </label>                            
                    </div>
                </div>`,
    styles: [`
        .action-type-filter div label {
            cursor: pointer;
            display: inline-block!important;
            padding: 3px 5px 3px 5px;
            border-radius: 3px;
            margin-right: 5px;
            border: 1px solid #eee;
        }
        
        .action-type-filter.small label {
            width: 34px;
            height: 34px;
            margin-bottom: 0px !important;
        }
        
        .action-type-filter label .fa {
            font-size: 16px;
        }
        
        .action-type-filter.small label .fa {
            font-size: 23px;
        }
        
        .action-type-filter label.active {
            background-color: #a7feab;
            border: 1px solid #ccc;
        }
    `]
})

export class ActionTypeComponent {

    @Output() change = new EventEmitter();
    @Input() model: any;

    @Input() multi: boolean = false;
    @Input() showLabels: boolean = false;

    public activeTypes: Array<ActionType> = [];

    public types = {
        'Email': false,
        'Phone': false,
        'Meeting': false,
        'Reminder': false,
        'Events': false,
        'Other': false
    }

    constructor() {
        this.activeTypes = ActionTypes.filter(x => x.active);
    }

    ngOnChanges(change: any) {
        if (change['model'] && change['model']['currentValue']) {
            this.setModel(change['model']['currentValue']);
        }
    }

    private setModel(model: any) {

        if (typeof model == "string") {
            this.toggle(model);
        }
        else {
            for (let t of model) {
                (<any>this.types)[t] = true;
            }
        }
    }

    public toggle(type: string) {
        if (this.multi) {
            (<any>this.types)[type] = !(<any>this.types)[type];
            var typeArray: Array<any> = [];
            for (let t in this.types) {
                if ((<any>this.types)[t]) {
                    typeArray.push(t);
                }
            }
            this.change.next(typeArray);
        }
        else {
            for (let t in this.types)
                (<any>this.types)[t] = false;
            (<any>this.types)[type] = true;
            this.change.next(type);
        }
    }

}