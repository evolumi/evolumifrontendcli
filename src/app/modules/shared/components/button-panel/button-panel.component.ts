import { Component, HostListener } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { ButtonPanelService, ClassToggle } from '../../../../services/button-panel.service';

declare var $: JQueryStatic;

@Component({
    selector: "button-panel",
    templateUrl: './button-panel.component.html',
    styleUrls: ['button-panel.css']
})

export class ButtonPanelComponent {

    private sub: Subscription;

    private dataContainer: any = {}

    @HostListener('window:resize', ['$event.target'])
    onResize() {
        this.service.setWidth(window.innerWidth)
    }

    @HostListener('window:keydown', ['$event'])
    keyDown(event: KeyboardEvent) {
        for (let button of this.service.headerButtons) {
            if (event.altKey && event.keyCode == button.shortcut) {
                this.click(button.command);
            }
        }
    }

    constructor(public service: ButtonPanelService) {
        this.sub = this.service.toggleClassObservable().subscribe(res => {
            console.log("[Buttons] Updating button", res);
            this.toggleClass(res);
        })
    }

    public click(command: string) {
        console.log("[Buttons] Click: " + command);
        this.service.click(command);
    }

    public toggleClass(data: ClassToggle) {
        let el = $('#' + data.id);
        el.hasClass(data.className) ? el.removeClass(data.className) : el.addClass(data.className);
        this.dataContainer[data.id] = data.data;
    }
}
