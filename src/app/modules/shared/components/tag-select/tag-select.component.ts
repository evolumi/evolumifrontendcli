import { Component, Input, Output, EventEmitter, OnInit, OnChanges } from '@angular/core';
import { ChosenOptionModel } from '../chosen/chosen.models';

const pageSize = 50;

@Component({
    selector: 'tag-select',
    templateUrl: './tag-select.component.html'
})

export class TagSelectComponent implements OnInit, OnChanges {

    @Input() value: Array<string> = [];
    @Input() options: Array<ChosenOptionModel> = [];
    @Input() placeholder: string;
    @Output() onSelect: EventEmitter<Array<string>> = new EventEmitter();

    public tags: Array<ChosenOptionModel> = [];

    constructor() {

    }

    ngOnInit() {

    }

    ngOnChanges(changes: any) {
        console.log("[Tag-Select] Change", this.options.length, this.value);
        if (changes["options"]) {
            this.search("");
        }
    }

    search(term: any) {
        this.tags = [].concat(this.getSelected());
        console.log("[Tag-Select] Search", term);
        this.scroll(term, 1);
    }

    private getSelected(): Array<ChosenOptionModel> {
        let selected: Array<ChosenOptionModel> = [];
        for (let val of this.value) {
            selected.push(new ChosenOptionModel(val, val));
        }
        return selected;
    }

    scroll(term: any, page: number) {
        console.log("[Tag-Select] Scroll", term, page);
        let skip = pageSize * (page - 1);
        let result = this.options.filter(x => x.value.toString().indexOf(term) >= 0).slice(skip, skip + pageSize - 1);
        console.log("[Tag-Select] Start " + skip + " End " + (skip + pageSize - 1), result[0], result[result.length - 1]);
        setTimeout(() => {
            this.tags = [...this.tags].concat(result);
        }, 100);
    }

    setValue(value: Array<string>) {
        let obj: any = value
        // This just checks that the value is not a Event. (Chosen sometimes send event instead of Value).
        if (obj.target) { }
        // Do nothing
        else {
            this.onSelect.emit(value);
        }
    }
}