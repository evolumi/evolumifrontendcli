import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { HighchartService } from '../../../../services/highchart.service';

@Component({

    selector: 'highchart-column',
    templateUrl: 'highchart-column.component.html',
    styleUrls: ["highchart.css"]
})
export class HighchartColumnComponent implements OnInit {

    @Output() pin = new EventEmitter<string>();

    constructor(public highchart: HighchartService) { }

    ngOnInit() { }

    showModal(option: any) {
        this.highchart.showModal = true;
        this.highchart.modalOption = option;
    }
}