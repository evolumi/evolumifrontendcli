import { Component, Input } from '@angular/core';
import { HighchartService } from '../../../../services/highchart.service';

@Component({
    selector: 'highchart',
    template: ` 
            <chart style="display: block; width:100%" *ngIf="highchartService.options && highchartService.options.length > 0" [options]="highchartService.options[0]"></chart> 
    `
})
export class HighchartComponent {

    constructor(public highchartService: HighchartService) {

    }
}

@Component({
    selector: 'highchart-multi',
    templateUrl: 'highchart.multi.html',
})
export class HighchartMultiComponent {

    @Input() minHeight: string = '600px';

    constructor(public highchartService: HighchartService) { }

    public pin(option: any) {
        console.log("Options:", option)
        this.highchartService.pin(option.id);
    }
}