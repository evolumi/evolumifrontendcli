import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { ActionsCollection } from '../../../../services/actionservice';
import { DealAddEdit } from '../../../../services/dealAddEdit.service';
import * as moment from 'moment';

@Component({
    selector: 'item-list',
    templateUrl: './item-list.html',
    styles: [`
        .report-item-row {
            font-size: 14px;
        }
        
            .report-item-row:hover {
                font-size: 14px;
                background-color: #eee;
                cursor: pointer;
            }
    `]
})
export class ItemListComponent {

    @Input() items: Array<any>;
    @Input() itemType: string;
    @Input() shortenedList: boolean;
    @Input() columns: Array<string>;

    constructor(private router: Router, private service: ActionsCollection, private translate: TranslateService, private dealService: DealAddEdit) {

    }

    public getColumnHeader(col: any): string {
        switch (col) {
            case 'StartDate': return 'DATE';
            case 'CreatedDate': return 'CREATEDDATE';
            case 'ClosedDate': return 'CLOSEDDATE';
            case 'UserAssigned': return 'ASSIGNEDTO';
            case 'Account': return 'ACCOUNT';
            case 'Type': return 'ACTIONTYPE';
            case 'Name': return 'NAME';
            case 'TotalInCompanyCurrency': return 'VALUE';
            case 'TimeInStatus': return 'TIMEINSTATUS';
            case 'Owner': return "DEALOWNERS";

            case 'DealStatusName': return "DEALSTATUS";
            case 'AccountManagerName': return "ACCMAN";
            case 'ShownName': return "NAME";
            case 'AccountTypeName': return "ACCTYPE";
            default: return col;
        }
    }

    public getColumnText(col: any, item: any): string {
        switch (col) {
            case 'StartDate': return moment(item.StartDate).format('DD') + " " + moment(item.StartDate).format('MMM') + (moment(item.StartDate).format('MMDD') == moment(item.EndDate).format('MMDD') ? '' : " - " +
                moment(item.EndDate).format('DD') + " " + moment(item.EndDate).format('MMM'));
            case 'CreatedDate': return moment(item.DateCreated).format('DD') + " " + moment(item.DateCreated).format('MMM');
            case 'ClosedDate': return moment(item.DateClosed).format('DD') + " " + moment(item.DateClosed).format('MMM');
            case 'UserAssigned': return item.UserAssigned ? item.UserAssigned.FullName : "";
            case 'Account': return item.Account ? item.Account.ShownName : "";
            case 'TimeInStatus': return item.StatusHistory ? Math.round(moment.duration(moment().diff(moment(item.StatusHistory[item.StatusHistory.length - 1].UpdateDate))).asDays()) + " " + this.translate.instant("DAYS") : "**Error**";
            case 'Owner': return this.getOwnerNames(item.Owners);
            default: return item[col] + '';
        }
    }

    private getOwnerNames(owners: Array<any>): string {
        let names = "";
        for (var owner of owners) {
            names += (names.length ? ", " : "") + owner.OwenerName;
        }
        return names;
    }

    public clickItem(item: any) {
        console.log("Click", item, this.itemType);
        if (this.itemType == 'Action') {
            this.service.showGroupByActionId(item.Id);
        }
        if (this.itemType == 'Deal') {
            this.dealService.showDeal(item);
        }
        if (this.itemType == 'Account') {
            this.router.navigate(["CRM/", item.IsPerson ? "PersonAccounts" : "Accounts", item.Id]);
        }
    }
}

