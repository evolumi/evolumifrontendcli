import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'progress-chart',
    templateUrl: 'progress-chart.component.html',
    styleUrls: ["highchart.css"]
})
export class ProgressChartComponent implements OnInit {

    @Input() chart: any;
    @Input() sliced: number;

    constructor() { }

    ngOnInit() { }
}