import { TranslateService } from '@ngx-translate/core';
import { Component, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { UsersService } from '../../../../services/users.service';
import { ChosenOption } from '../../../shared/components/chosen/chosen';
import { ChosenOptionModel, ChosenOptionGroupModel } from '../../../shared/components/chosen/chosen.models';
import { AccountsCollection } from '../../../../services/accounts.service';
import { OrgService } from '../../../../services/organisation.service';
import { InsightFilter } from '../../../../services/highchart.service';
import { GoalsService } from '../../../../services/goals.service';
import { DateService } from '../../../../services/date.service';
import { SalesCommissionService } from '../../../../services/sales-commission.service';
import { Subscription } from 'rxjs/Subscription';
import * as moment from 'moment';
import { OrganisationModel } from "../../../../models/organisation.models";

declare var _: any

@Component({
    selector: 'chartfilter',
    templateUrl: './highchart.filter.component.html',
    styleUrls: ['highchart.css']
})

export class HighchartFilterComponent {

    private orgSub: Subscription;
    private userSub: Subscription;
    private salesCommSub: Subscription;

    @Input() type: string;
    @Input() id: string;
    @Input() horizontal: boolean;

    @Output() changeFilter: EventEmitter<any> = new EventEmitter();

    public today: any = undefined;

    public org: OrganisationModel;

    public dealStatuses: Array<any>;
    public saleProcess: Array<any>;
    public accounts: Array<any>;
    public users: Array<any>;
    public accountTypes: Array<any>;
    public accountTags: Array<any>;

    public actionStatuses: Array<any>;
    public actionTypes: Array<any>;
    public timeSpans: Array<any>;
    public groupValues: Array<any>;

    public dealDateFieldValues: Array<ChosenOption>;
    public actionDateFieldValues: Array<ChosenOption>;

    public goalIntervals: Array<ChosenOptionModel> = [];

    public salesCommissions: Array<ChosenOptionModel>;

    advancedAccountFilters: boolean = false;
    advancedDealFilters: boolean = false;
    advancedActionFilters: boolean = false;

    @Input() filter: InsightFilter;

    dateCustom: any = {
        "accountCreateDate": false,
        "dealDate": false,
        "actionCreateDate": false,
        "actionClosedDate": false,
        "actionDate": false,
        "salesCommissionDate": false,
    }

    accountCreateDateCustom: boolean = false;

    constructor(private orgService: OrgService,
        public userService: UsersService,
        public accountService: AccountsCollection,
        private translate: TranslateService,
        public goalsService: GoalsService,
        private dateService: DateService,
        private salesCommSvc: SalesCommissionService) { }

    ngOnInit() {
        if (!this.filter) {
            this.filter = new InsightFilter({});
        }
        this.orgSub = this.orgService.currentOrganisationObservable().subscribe(org => {
            if (org) {
                this.org = org;
                if (!this.dealStatuses || !this.saleProcess || this.dealStatuses.length === 0 || this.saleProcess.length === 0) {
                    this.getDealStatusesAndSalesProcesses();
                }

                if (!this.accountTypes || this.accountTypes.length === 0) {
                    this.getAccountTypes();
                }
            }
        });

        // Users
        this.userSub = this.userService.userListObervable().subscribe(res => {
            if (res) {
                this.users = [];
                for (var user of res) {
                    this.users.push(user);
                }
            } else {
                this.userService.getAllUsers();
            }
        });
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes && changes['type']) {
            this.initFilter();
        }
    }

    ngOnDestroy() {
        if (this.orgSub) this.orgSub.unsubscribe();
        if (this.userSub) this.userSub.unsubscribe();
        if (this.salesCommSub) this.salesCommSub.unsubscribe();
    }

    public updateFilter() {
        this.changeFilter.next(this.filter);
    }

    private initFilter() {
        switch (this.type) {
            case 'Action':
                this.initAction();
                break;
            case 'Deal':
                this.initDeal();
                break;
            case 'Account':
                this.initAccount();
                break;
            case 'Goals':
                this.initGoals();
                break;
            case 'SalesCommission':
                this.initSalesCommission();
                break;
        }
    }

    private initAccount() {
        this.getAccountTypes();
        this.getAccountTags();
        this.getTimeSpans();
    }

    private initAction() {
        this.initAccount();
        this.getGroupValues();
        this.getActionTypes();
        this.getActionStatuses();
        this.getActionDateFields();
        this.setTimeSpan("actionDate", "Last30Days");
    }

    private initDeal() {
        this.initAccount();
        this.getDealDateFields();
        this.getDealStatusesAndSalesProcesses();
        this.setTimeSpan("dealDate", "Last30Days");
        this.getGroupValues();
    }

    private initGoals() {
        for (let i = 5; i < 17; i++){
            this.goalIntervals.push(new ChosenOptionModel(i, i));
        }
    }

    private initSalesCommission() {
        this.salesCommSub = this.salesCommSvc.salesCommissionListObs()
            .subscribe(salesComms => {
                if (salesComms) {
                    this.salesCommissions = salesComms.map(x => new ChosenOptionModel(x.Id, x.Name));
                } else {
                    this.salesCommSvc.getManySalesCommission();
                }
            })

        this.getTimeSpans();
        this.getGroupValues();
        this.setTimeSpanTimestamp("salesCommission", "Last30Days");
    }

    private getTimeSpans() {
        if (!this.timeSpans || this.timeSpans.length === 0) {
            this.timeSpans = [
                new ChosenOptionModel("All", this.translate.instant("SHOWALL")),
                new ChosenOptionModel("Last6Months", this.translate.instant("LAST6MONTHS")),
                new ChosenOptionModel("Today", this.translate.instant("TODAY")),
                new ChosenOptionModel("Yesterday", this.translate.instant("YESTERDAY")),
                new ChosenOptionModel("ThisWeek", this.translate.instant("THISWEEK")),
                new ChosenOptionModel("Last7Days", this.translate.instant("LAST7DAYS")),
                new ChosenOptionModel("LastWeek", this.translate.instant("LASTWEEK")),
                new ChosenOptionModel("Last30Days", this.translate.instant("LAST30DAYS")),
                new ChosenOptionModel("ThisMonth", this.translate.instant("THISMONTH")),
                new ChosenOptionModel("LastMonth", this.translate.instant("LASTMONTH")),
                new ChosenOptionModel("ThisYear", this.translate.instant("THISYEAR")),
                new ChosenOptionModel("LastYear", this.translate.instant("LASTYEAR")),
                new ChosenOptionModel("Custom", this.translate.instant("CUSTOM"))
            ];
        }
    }

    private getGroupValues() {
        if (!this.groupValues || this.groupValues.length === 0) {
            this.groupValues = [
                new ChosenOptionModel("Auto", this.translate.instant("AUTO")),
                new ChosenOptionModel("Day", this.translate.instant("DAY")),
                new ChosenOptionModel("Week", this.translate.instant("WEEK")),
                new ChosenOptionModel("Month", this.translate.instant("MONTH")),
                new ChosenOptionModel("Year", this.translate.instant("YEAR")),
            ];
        }
    }

    private getActionTypes() {
        if (!this.actionTypes || this.actionTypes.length === 0) {
            this.actionTypes = [
                new ChosenOptionModel("Email", this.translate.instant("ACTIONTYPEEMAIL")),
                new ChosenOptionModel("Phone", this.translate.instant("ACTIONTYPEPHONECALL")),
                new ChosenOptionModel("Meeting", this.translate.instant("ACTIONTYPEMEETING")),
                new ChosenOptionModel("Reminder", this.translate.instant("ACTIONTYPEREMINDER")),
                new ChosenOptionModel("Events", this.translate.instant("ACTIONTYPEEVENT")),
                new ChosenOptionModel("Other", this.translate.instant("ACTIONTYPEOTHER")),
            ];
        }
    }

    private getActionStatuses() {
        if (!this.actionStatuses || this.actionStatuses.length === 0) {
            this.actionStatuses = [
                new ChosenOptionModel("Open", this.translate.instant("OPEN")),
                new ChosenOptionModel("Closed", this.translate.instant("CLOSED")),
            ];
        }
    }

    private getActionDateFields() {
        if (!this.actionDateFieldValues || this.actionDateFieldValues.length === 0) {
            this.actionDateFieldValues = [
                new ChosenOptionModel("DateCreated", this.translate.instant("CREATEDDATE")),
                new ChosenOptionModel("DateStart", this.translate.instant("STARTDATE")),
                new ChosenOptionModel("DateEnd", this.translate.instant("ENDDATE")),
            ];
        }
    }

    private getDealDateFields() {
        if (!this.dealDateFieldValues || this.dealDateFieldValues.length === 0) {
            this.dealDateFieldValues = [
                new ChosenOptionModel("DateCreated", this.translate.instant("CREATEDDATE")),
                new ChosenOptionModel("DateStart", this.translate.instant("STARTDATE")),
                new ChosenOptionModel("DateEnd", this.translate.instant("ENDDATE")),
                new ChosenOptionModel("DateInvoice", this.translate.instant("INVOICEDATE")),
                new ChosenOptionModel("DateInvoiceExpire", this.translate.instant("INVOICEDATEEXPIRE")),
            ];
        }
    }

    private getDealStatusesAndSalesProcesses() {
        if (this.org && (!this.dealStatuses || !this.saleProcess || this.dealStatuses.length === 0 || this.saleProcess.length === 0)) {
            if (this.org.Settings.SaleProcesses.length > 0) {
                for (var group of this.org.Settings.SaleProcesses) {
                    this.saleProcess = [];
                    this.dealStatuses = [];
                    this.saleProcess.push(new ChosenOptionGroupModel(group.Id, group.Name));
                    for (var stat of group.DealStatuses) {
                        this.dealStatuses.push(new ChosenOptionModel(stat.Id, stat.Name, group.Id));
                    }
                }
            }
        }
    }

    private getAccountTypes() {
        if (this.org && (!this.accountTypes || this.accountTypes.length === 0)) {
            this.accountTypes = [];
            for (var accType of this.org.AccountTypes) {
                this.accountTypes.push(new ChosenOptionModel(accType.Id, accType.Name));
            }
        }
    }

    private getAccountTags() {
        if (!this.accountTags || this.accountTags.length === 0) {
            this.accountTags = this.accountService.tagList;
        }
    }

    public setTimeSpan(field: string, value: string) {
        this.dateCustom[field] = value == "Custom";

        let dates: Array<Date> = [new Date(), new Date()];

        if (value == "All") {

            dates[0].setTime(0);
            dates[1].setTime(0);
            this.filter[field] = dates;
        }

        if (value == "Last6Months") {

            dates[0] = new Date(moment().subtract(6, 'months').toString());
            this.filter[field] = dates;
        }

        else if (value == "Today") {
            this.filter[field] = dates;
        }

        else if (value == "Yesterday") {
            dates[0] = new Date(moment().subtract(1, 'd').toString());
            dates[1] = new Date(moment().subtract(1, 'd').toString());
            this.filter[field] = dates;
        }

        else if (value == "ThisWeek") {
            dates[0] = new Date(moment().day(1).toString());
            this.filter[field] = dates;
        }

        else if (value == "Last7Days") {
            dates[0] = new Date(moment().subtract(7, 'd').toString());
            dates[1] = new Date(moment().subtract(1, 'd').toString());
            this.filter[field] = dates;

        }

        else if (value == "LastWeek") {
            dates[0] = new Date(moment().day(-6).toString());
            dates[1] = new Date(moment().day(0).toString());
            this.filter[field] = dates;
        }

        else if (value == "Last30Days") {
            dates[0] = new Date(moment().subtract(30, 'days').toString());
            dates[1] = new Date(moment().subtract(1, 'days').toString());
            this.filter[field] = dates;
        }

        else if (value == "ThisMonth") {
            dates[0] = new Date(moment().startOf('month').utc().toString());
            this.filter[field] = dates;
        }

        else if (value == "LastMonth") {
            dates[0] = new Date(moment().subtract(1, 'months').date(1).toString());
            dates[1] = new Date(moment().date(0).toString());
            this.filter[field] = dates;
        }

        else if (value == "ThisYear") {
            dates[0] = new Date(dates[0].getFullYear(), 0, 1);
            this.filter[field] = dates;
        }

        else if (value == "LastYear") {
            dates[0] = new Date(dates[0].getFullYear() - 1, 0, 1);
            dates[1] = new Date(dates[1].getFullYear() - 1, 11, 31);
            this.filter[field] = dates;
        }

        console.log("[Insight] 1 Setting filter value:" + field + value);
        this.filter[field + "Timespan"] = value;
    }

    public setTimeSpanTimestamp(field: string, value: string) {
        const startDateField = field + 'StartDate';
        const endDateField = field + 'EndDate';
        this.dateCustom[field] = value == "Custom";

        let dates: Array<number> = [this.dateService.changeTimeOfDate(new Date().getTime()), this.dateService.changeTimeOfDate(new Date().getTime(), true)];
        if (value == "All") {
            dates[0] = 0;
            dates[1] = 0;
            this.filter[startDateField] = dates[0];
            this.filter[endDateField] = dates[1];
        }

        if (value == "Last6Months") {

            dates[0] = +this.dateService.subtractFromDate(dates[0], 6, 'months');
            this.filter[startDateField] = dates[0];
            this.filter[endDateField] = dates[1];
        }

        else if (value == "Today") {
            this.filter[startDateField] = dates[0];
            this.filter[endDateField] = dates[1];
        }

        else if (value == "Yesterday") {
            dates[0] = +this.dateService.subtractFromDate(dates[0], 1, 'days');
            dates[1] = +this.dateService.subtractFromDate(dates[0], 1, 'days');
            this.filter[startDateField] = dates[0];
            this.filter[endDateField] = dates[1];
        }

        else if (value == "ThisWeek") {
            dates[0] = this.dateService.getFirstDateOfPeriod('week');
            this.filter[startDateField] = dates[0];
            this.filter[endDateField] = dates[1];
        }

        else if (value == "Last7Days") {
            dates[0] = +this.dateService.subtractFromDate(dates[0], 1, 'weeks');
            this.filter[startDateField] = dates[0];
            this.filter[endDateField] = dates[1];
        }

        else if (value == "LastWeek") {
            dates[0] = new Date(moment().day(-6).toString()).getTime();
            dates[1] = new Date(moment().day(0).toString()).getTime();
            this.filter[startDateField] = dates[0];
            this.filter[endDateField] = dates[1];
        }

        else if (value == "Last30Days") {
            dates[0] = +this.dateService.subtractFromDate(dates[0], 30, 'days');
            this.filter[startDateField] = dates[0];
            this.filter[endDateField] = dates[1];
        }

        else if (value == "ThisMonth") {
            dates[0] = this.dateService.getFirstDateOfPeriod('month');
            this.filter[startDateField] = dates[0];
            this.filter[endDateField] = dates[1];
        }

        else if (value == "ThisMonth") {
            dates[0] = new Date(moment().subtract(1, 'months').date(1).toString()).getTime();
            dates[1] = new Date(moment().date(0).toString()).getTime();
            this.filter[startDateField] = dates[0];
            this.filter[endDateField] = dates[1];
        }

        else if (value == "ThisYear") {
            dates[0] = this.dateService.getFirstDateOfPeriod('year');
            this.filter[startDateField] = dates[0];
            this.filter[endDateField] = dates[1];
        }

        else if (value == "LastYear") {
            const prevYear = new Date(dates[0]).getFullYear() - 1;
            dates[0] = this.dateService.getFirstDateOfPeriod('year', 'x', new Date().setFullYear(prevYear))
            dates[1] = this.dateService.getLastDateOfPeriod('year', 'x', new Date().setFullYear(prevYear))
            this.filter[startDateField] = dates[0];
            this.filter[endDateField] = dates[1];
        }

        console.log("[Insight] 2 Setting filter value:" + field + value);
        this.filter[field + "Timespan"] = value;
    }
}