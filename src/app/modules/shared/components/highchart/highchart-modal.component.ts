import { Component, OnInit, Input } from '@angular/core';
import { HighchartService } from "../../../../services/highchart.service";

@Component({
    selector: 'highchart-modal',
    templateUrl: 'highchart-modal.component.html'
})
export class HighchartModalComponent implements OnInit {

    @Input() big: boolean;

    constructor(public highchart: HighchartService) { }

    ngOnInit() { }

    ngOnDestroy() {
        this.back();
    }

    clickToClose(event: any) {
        let target = event.target || event.srcElement;
        if (target.id === 'chartModal') {
            this.back();
        }
    }

    back() {
        this.highchart.showModal = false;
    }
}