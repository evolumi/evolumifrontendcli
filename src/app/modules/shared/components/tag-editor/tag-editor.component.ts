import { Component, Input, Output, EventEmitter, OnInit, OnChanges, ElementRef, ViewChild } from '@angular/core';
import { OrgService } from '../../../../services/organisation.service';

@Component({

    selector: 'tag-edit-text',
    template: ` 
    <span class="label-container">
        <span [style.color]="color" [style.background-color]="bgColor" *ngFor="let item of items; let i=index" id="ispan" class="label" style="font-size:15px;">
           {{item}}
           <i class="fa fa-times prevent-close" aria-hidden="true" style="font-size:16px;padding:3px;cursor:pointer;" (click)="deletethisItem(i)"></i>
        </span>
    </span> 
    <div class="tagEdit" #tagEditContainer>
        <input type="text" [hidden]="(singleSelect && items?.length === 1) || hide" [(ngModel)]="tag1" #tag id="tageditor" (keyup)="key($event)" (focus)="onFocus()" autocomplete="off" placeholder="{{placeholder ? placeholder : ('TAGS' | translate)}}"/>
        <div class="suggestions" *ngIf="filteredList.length > 0">
            <ul>
                <li *ngFor="let item of filteredList; let i = index;" (click)="select(item)" [class.selected]="i==selected" class="prevent-close">
                    {{item}}
                </li>
            </ul>
        </div>
    </div>
`,
    host: {
        '(document:click)': 'handleClick($event)',
    },
    styles: [`
        .tagEdit {
            display: inline-block;
            position: relative;
            width: 200px;
        }
        
        .suggestions {
            position: absolute;
            top: 25px;
            left: 0px;
            width: 150px;
            z-index: 20000000 !important;
            background-color: #fff;
            border: 1px solid #eee;
        }
        
            .suggestions ul {
                list-style-type: none;
                margin-bottom: 0px;
                padding-left: 0px;
            }
        
            .suggestions li {
                height: 20px;
                padding-left: 5px;
                text-align: left;
            }
        
                .suggestions li:hover {
                    background-color: #eee;
                    cursor: pointer;
                }
            
            .suggestions .selected {
                background-color: #eee;
            }
    `]
})

export class TagEditorComponent implements OnInit, OnChanges {
    @Input('clear') clear: boolean; // not in use
    @Input('items') items: Array<any>;
    @Input('itemsSource') itemsSource: Array<any>; // if not using tags
    @Input('bgColor') bgColor: string = '#5bc0de';
    @Input('color') color: string = '#FFF';
    @Input('showFilter') showFilter: boolean;
    @Input('singleSelect') singleSelect: boolean;
    @Input('disableEdit') disableEdit: boolean;
    @Input('hide') hide: boolean;
    @Input('placeholder') placeholder: string;

    @Output('itemsChange') itemsChange = new EventEmitter<Array<any>>();
    @Output('textboxChange') textboxChange = new EventEmitter<Array<any>>();
    @ViewChild('tag') textbox: any;
    @ViewChild('tagEditContainer') container: any;
    public tag1: any;

    public tags: Array<any> = [];
    public filteredList: Array<any> = [];
    public selected: number;

    private elementRef: ElementRef;
    private orgSub: any;

    constructor(myElement: ElementRef, private orgService: OrgService) {
        this.elementRef = myElement;
    }

    ngOnInit() {
        // backwards-compatibility
        if (!this.itemsSource) {
            this.orgSub = this.orgService.currentOrganisationObservable().subscribe(org => {
                if (org) {
                    this.tags = org.Tags;
                    this.itemsSource = this.tags;
                }
            });
        } else {
            this.tags = this.itemsSource;
        }
    }

    ngOnDestroy(){
        if (this.orgSub) this.orgSub.unsubscribe();
    }

    handleClick(event: any) {
        let clickedComponent = event.target;
        if (clickedComponent == this.elementRef.nativeElement) {
            this.textbox.nativeElement.focus();
        } else if (!this.container.nativeElement.contains(event.target)) {
            this.filteredList = [];
            this.selected = undefined;
        }
    }

    select(item: any) {
        this.tag1 = item;
        this.addItemtoList(this.tag1);
        this.filteredList = [];
    }

    key(event: any) {
        let key = event.keyCode;

        if (key == 38) {
            if (this.selected != undefined && this.selected > 0) {
                this.selected--;
                this.tag1 = this.filteredList[this.selected];
            }

            return;
        }
        if (key == 40) {

            if (this.selected == undefined && this.filteredList.length > 0) {
                this.selected = 0;
                this.tag1 = this.filteredList[0];
                return;
            }

            if (this.selected != undefined && this.selected < this.filteredList.length - 1) {
                this.selected++;
                this.tag1 = this.filteredList[this.selected];
            }
            return;
        }

        if (this.tag1 && this.tag1 !== "" && key != 13 && key != 188) {
            this.filteredList = this.tags.filter(function (el: any) {
                return el.toLowerCase().indexOf(this.tag1.toLowerCase()) > -1;
            }.bind(this));
        } else {
            this.filteredList = [];
        }
        if ((key == 13 || key == 188) && !this.disableEdit) {
            this.addItemtoList(this.tag1);
        }

        this.selected = undefined;
    }

    onFocus() {
        if (this.showFilter) {
            this.filteredList = this.itemsSource;
        }
    }

    blur() {
        this.addItemtoList(this.tag1);
    }

    addItemtoList(newItem: any) {
        if (newItem && newItem.trim().length > 0) {
            if (newItem.substring(newItem.length - 1) === ',') {
                newItem = newItem.substring(0, newItem.length - 1);
            }
            if (!this.items) this.items = [];
            if (this.items.find(x => x === newItem)) {
                return;
            }
            console.log("[Tag-Edtor] -> Current Tags", this.items);
            this.items.push(newItem.trim());
            this.tag1 = '';
            this.itemsChange.next(this.items);
            console.log("[Tag-Editor] Updated Tags ", this.items);
            this.selected = undefined;
        }
    }

    ngOnChanges() {

    }

    deletethisItem(index: any) {
        var tags = this.items.splice(index, 1);
        console.log("[Tag-Edtor] Deleting Tags -> ", tags);
        this.itemsChange.emit(this.items);
    }
}
