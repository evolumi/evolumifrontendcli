import { Component, ElementRef } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { LocalStorage } from '../../../../services/localstorage';
import { Api } from '../../../../services/api';
import { NotificationService } from '../../../../services/notification-service';
import { ActionsCollection } from '../../../../services/actionservice';
import { Click2MailModel } from '../../../../models/click2mail.models';
import { OrgService, Features } from '../../../../services/organisation.service';
import { AuthService } from '../../../../services/auth-service';
import { ModalService } from '../../../../services/modal.service';
import { ChosenOptionModel, ChosenOptionGroupModel } from '../chosen/chosen.models';
declare var $: JQueryStatic;

@Component({

    selector: 'click-2-mail',
    templateUrl: './click2mail.html',
})

export class Click2MailComponent {

    private IsInitialized: boolean = false;

    public isMassMail: boolean = false;
    private searchQuery: string;
    private accountList: Array<string> = [];
    public contactPriority: boolean = true;

    public displayModal: boolean;
    public emailData: Click2MailModel = new Click2MailModel(null);

    public activeTemplate: any;

    public sender: string;
    public hasSMTPsettings: boolean = false;
    public orgSubscription: any;
    public modalSubscription: any;
    private userSub: any;
    public defaultBranding: string = "Sent via Evolumi";

    public relatedAccounts: Array<any> = [];
    private relatedAccountName: string;

    private hasBranding: boolean = false;
    public hasTemplates: boolean = false;
    public user: any;

    public isTemplateMode: boolean = false;
    public templates: Array<Click2MailModel> = [];
    public templatelist: Array<ChosenOptionModel> = [];
    public templateGroups: Array<ChosenOptionGroupModel> = [];

    public account: any;
    public contact: any;
    public actionGroupId: string = "";
    public groups: Array<ChosenOptionModel> = [];
    public selectedGroupName: string;

    public processing: boolean = false;
    public processingMessage: string = "";

    public showMailTemplateFeature: boolean;

    public tagWarning: boolean = false;

    // jquery wrapped element
    private _$element: any;
    constructor(
        private _api: Api,
        private _notify: NotificationService,
        private _orgService: OrgService,
        private _localstore: LocalStorage,
        private _el: ElementRef,
        private auth: AuthService,
        private modalService: ModalService,
        private translate: TranslateService,
        private actionService: ActionsCollection
    ) {

    }

    ngOnInit() {

        this.templateGroups = [
            new ChosenOptionGroupModel("OPTIONS", this.translate.instant("OPTIONS")),
            new ChosenOptionGroupModel("PERSONAL", this.translate.instant("PERSONALTEMPLATES")),
            new ChosenOptionGroupModel("ORG", this.translate.instant("ORGTEMPLATES"))
        ];

        this.userSub = this.auth.currentUserObservable().subscribe(user => {
            this.user = user;
        })

        this.orgSubscription = this._orgService.currentOrganisationObservable().subscribe(org => {
            if (org) {
                this.defaultBranding = org.DefaultSignature;
                this.hasBranding = this._orgService.hasFeature(Features.Branding);
                this.hasTemplates = this._orgService.hasFeature(Features.MailTemplates);

                console.log("[Click2mail] Feature Branding : " + this.hasBranding + (this.hasBranding ? " -> No Branding" : " -> Adding branding"));

                this.hasSMTPsettings = org.SMTPSettings && (!org.SMTPSettings.SendWithSMTP || (
                    org.SMTPSettings.SendWithSMTP &&
                    org.SMTPSettings.Password &&
                    org.SMTPSettings.UserName &&
                    org.SMTPSettings.Port &&
                    org.SMTPSettings.Host));
                console.log("[Click2mail] SMTP settings: " + (this.hasSMTPsettings ? "Found" : "Not found! -> Disabeling form"));
                if (org.SMTPSettings) this.sender = org.SMTPSettings.Sender;
                this.getMailTemplates();
            }
        });

        this.modalSubscription = this.modalService.ClickToMailTrigger().subscribe(res => {
            if (res && res != '') {
                console.log("[Click2Mail] Trigger", res);
                this.showModal(res.email, res.contact, res.account, res.actionGroupid, res.accounts, res.searchQuery);
                this.buildRedactorPlugins();
            }
        });

        // jquery wrap and store element
        this._$element = (<any>$(this._el.nativeElement)).find('.redactor');
        this._$element.redactor();
    }

    buildRedactorPlugins() {
        this._$element.redactor('core.destroy');
        var self = this;
        (function ($: any) {
            //Dropdown for templates
            $.Redactor.prototype.taglist = function () {
                return {
                    init: function () {
                        var dropdown = {
                            FirstName: { title: 'First Name', func: this.taglist.insertHtml },
                            LastName: { title: 'Last Name', func: this.taglist.insertHtml },
                            FullName: { title: 'Full Name', func: this.taglist.insertHtml },
                            AccountName: { title: 'Account Name', func: this.taglist.insertHtml },
                            MyFirstName: { title: 'My First Name', func: this.taglist.insertHtml },
                            MyLastName: { title: 'My Last Name', func: this.taglist.insertHtml }
                        }
                        var button = this.button.add('taglist', 'Add tag');
                        this.button.addDropdown(button, dropdown);
                    },
                    insertHtml: function (buttonName) {
                        self._$element.redactor('insert.html', `[${buttonName}]`);
                    }
                };
            };
            //Dropdown for mailing
            $.Redactor.prototype.datalist = function () {
                return {
                    init: function () {
                        var firstName = self.contact ? self.contact.FirstName : self.account.FirstName;
                        var lastName = self.contact ? self.contact.LastName : self.account.LastName;
                        var dropdown = {
                            [firstName]: { title: firstName, func: this.datalist.insertHtml },
                            [lastName]: { title: lastName, func: this.datalist.insertHtml },
                            [firstName + " " + lastName]: { title: firstName + " " + lastName, func: this.datalist.insertHtml },
                            [self.account.ShownName]: { title: self.account.ShownName, func: this.datalist.insertHtml },
                            [self.user.FirstName]: { title: self.user.FirstName, func: this.datalist.insertHtml },
                            [self.user.LastName]: { title: self.user.LastName, func: this.datalist.insertHtml }
                        }
                        var button = this.button.add('datalist', 'Add data');
                        this.button.addDropdown(button, dropdown);
                    },
                    insertHtml: function (buttonName) {
                        self._$element.redactor('insert.html', buttonName);
                    }
                };
            };
        })(jQuery);

        this._$element.redactor({
            focus: true,
            linebreaks: true,
            callbacks: {
                change: function () {
                    self.emailData.body = this.code.get();
                }
            },
            plugins: ['datalist']
        });
    }

    ngOnDestroy() {
        if (this.orgSubscription) this.orgSubscription.unsubscribe();
        if (this.modalSubscription) this.modalSubscription.unsubscribe();
        if (this.userSub) this.userSub.unsubscribe();
        this._$element.redactor('core.destroy');
    }

    public send() {
        console.log("[Click2mail] Send Mail: Started");
        this.hideModal();
        let orgid = this._orgService.currentOrganisation().Id;
        this.emailData.organisaonId = orgid;

        if (this.isMassMail) {

            var searchQuery: any = "";
            if (this.searchQuery) {
                searchQuery = "/?" + this.searchQuery;
            }

            this._api.requestPost(
                `Click2Mail/SendMany` + searchQuery,
                {
                    accountList: this.accountList,
                    body: this.activeTemplate ? this.activeTemplate.body : this.emailData.body,
                    subject: this.activeTemplate ? this.activeTemplate.subject : this.emailData.subject,
                    attachments: this.emailData.attachments,
                    organisationId: orgid,
                    getAccountsFromFilter: (this.searchQuery ? true : false)
                },
                res => {
                    console.log("[Click2mail] Send Mails: Success!");
                    this._notify.success(res);
                    this.actionService.actionsChange();
                },
                err => {
                    console.log("[Click2mail] Send Mails: Fail! -> " + err);
                    this._notify.error(err)
                });
        }
        else {

            this.emailData.accountId = this.account ? this.account.Id : null;
            this.emailData.contactId = this.contact ? this.contact.PersonId : null;
            this.emailData.actionGroupId = this.actionGroupId;
            this._api.requestPost(
                `Click2Mail/Send`,
                this.emailData,
                res => {
                    console.log("[Click2mail] Send Mail: Success!");
                    this._notify.success(res);
                    this.actionService.actionsChange();
                },
                err => {
                    console.log("[Click2mail] Send Mail: Fail! -> " + err);
                    this._notify.error(err)
                });
        }
    }

    public getMailTemplates() {
        if (this.IsInitialized && this.hasTemplates) {
            var orgId = this._localstore.get('orgId');
            this._api.requestGet("Organisations/" + orgId + "/MailTemplates", (templates) => {
                console.log("[Click2Mail] Load Templates: Success! Found [" + templates.length + "] of them");

                this.templates = templates;
                this.templatelist = [new ChosenOptionModel("", this.translate.instant("NONE"), "OPTIONS"), new ChosenOptionModel("NEW", this.translate.instant("CREATETEMPLATE"), "OPTIONS")];
                for (var t of templates) {
                    if (t.isPersonal)
                        this.templatelist.push(new ChosenOptionModel(t.id, t.name, "PERSONAL"));
                    else
                        this.templatelist.push(new ChosenOptionModel(t.id, t.name, "ORG"));
                }
            }, (err) => {
                console.log("[Click2Mail] Load Templates: Failed!");
            });
        }
    }

    public createTemplate() {
        this.activeTemplate = undefined;
        this.emailData.id = "";
        this.emailData.name = "";
        console.log("[Click2Mail] Creating new  Template" + this.emailData.id);
        console.log(this.emailData);
        this.templateMode(true);
    }

    public editTemplate() {
        this.templateMode(true);
        this.loadTemplate(this.emailData.id);
    }

    public saveMailTemplate() {
        if (this.emailData.id && this.emailData.id != "NEW") {
            this._api.requestPut("MailTemplates/" + this.emailData.id, this.emailData, (res) => {
                console.log("[Click2Mail] Save Template: Success!");
                let index = this.templates.findIndex(x => x.id == this.emailData.id);
                this.templates.splice(index, 1, new Click2MailModel(this.emailData));

                console.log("new templatelist", this.templates);
                this.isTemplateMode = false;
                this.templateMode(false);
                this.replaceTags();
            }, (err) => {
                console.log("[Click2Mail] Save Template: Failed!");
            });
        }
        else {
            var orgId = this._localstore.get('orgId');
            this._api.requestPost("Organisations/" + orgId + "/MailTemplates/", this.emailData, (res) => {
                console.log("[Click2Mail] Create Template: Success!");
                this.templateMode(false);
                this.emailData.id = res;
                var template = new Click2MailModel(this.emailData);
                this.templates.push(template);
                this.templatelist.push(new ChosenOptionModel(res, this.emailData.name, this.emailData.isPersonal ? "PERSONAL" : "ORG"));
                this.activeTemplate = template;
                console.log("[Click2Mail] Setting id: " + res);
            }, (err) => {
                console.log("[Click2Mail] Create Template: Failed!");
            });
        }
    }

    public newMail() {
        let email = this.emailData.email;
        this.emailData = new Click2MailModel({ email: email, subject: " ", body: " ", attachments: [] });
        this._$element.redactor('code.set', '');
        this.activeTemplate = undefined;
    }

    public loadTemplate(id: string) {

        console.log("[Click2Mail] Loading Template: [" + id + "]");

        if (!id) {
            this.newMail();
            return;
        }

        if (id == "NEW") {
            this.createTemplate();
            return;
        }

        console.log(this.templates);
        let template = this.templates.find(x => x.id == id);
        this.activeTemplate = template;
        console.log(template);
        let email = this.emailData.email;
        if (template) {
            console.log("[Click2Mail] Loaded Template:" + template.name);
            this.emailData = new Click2MailModel(template);
            this.emailData.email = email;
            if (!this.isTemplateMode)
                this.replaceTags();
            else
                this._$element.redactor('code.set', this.emailData.body);
        }
    }

    templateMode(value: boolean) {
        var self = this;
        this.isTemplateMode = value;
        this._$element.redactor('core.destroy');
        if (value) {
            this._$element.redactor({
                focus: true,
                linebreaks: true,
                callbacks: {
                    change: function () {
                        self.emailData.body = this.code.get();
                    }
                },
                plugins: ['taglist']
            });
        }
        else {
            this._$element.redactor({
                focus: true,
                linebreaks: true,
                callbacks: {
                    change: function () {
                        self.emailData.body = this.code.get();
                    }
                },
                plugins: ['datalist']
            });
        }
    }

    public deleteTemplate() {

        if (this.emailData.id && this.emailData.id != "NEW") {
            var conf = confirm(this.translate.instant("WARNDELTEMPLAE"));
            if (conf) {
                this._api.requestDelete("MailTemplates/" + this.emailData.id, (res) => {
                    console.log("[Click2Mail] Delete Template: Success!");
                    this.getMailTemplates();
                    this.newMail()
                    this.templateMode(false);
                }, (err) => {
                    console.log("[Click2Mail] Delete Template: Failed!");
                });
            }
        }
    }

    public upload(): void {
        this.processingMessage = this.translate.instant("UPLOADING");
        this.processing = true;
        let formData: FormData = new FormData();
        formData.append('file', (<any>$('#file-attachment')[0]).files[0]);
        console.log("[Click2Mail] Uploading attachment file");

        this._api.upload(`Click2Mail/UpploadAttachment`, formData)
            .then(response => {
                var resp = JSON.parse(response);
                if (resp.IsSuccess) {
                    console.log("[Click2Mail] Uploading attachment file: Success!", response);
                    this.processingMessage = "";
                    console.log("[Click2Mail] Attachments: ", this.emailData.attachments);
                    this.emailData.attachments.push(resp.Data);
                }
                else {
                    console.log("[Click2Mail] Uploading attachment file: Failed!", response);
                    this.processingMessage = this.translate.instant("ERROR");
                }
                this.processing = false;
            });
    }

    public deleteFile(index: number) {
        var file = this.emailData.attachments.splice(index, 1);
        console.log("[Click2Mail] Deleting file", file);
    }

    public submitDisabled(): boolean {
        return !this.emailData.subject || !this.emailData.body;
    }

    // Opens up the modal. You can provice contact(relation) and account(person/company) or for massmails you can provide a accountList or searchQuery.
    public showModal(email: string, contact: any, account: any, actionGroupId: string, accountList: Array<string>, searchQuery: string): void {

        this.relatedAccounts = [];
        this.IsInitialized = true;
        this.getMailTemplates();
        var orgId = this._localstore.get('orgId');
        this.tagWarning = false;

        this.emailData = new Click2MailModel({
            subject: "",
            body: "",
            email: email,
            organisaonId: orgId,
            attachments: []
        });

        if (searchQuery) {
            this.isMassMail = true;
            this.searchQuery = searchQuery;
            this.contact = null;
            this.account = null;
        }
        else if (accountList && accountList.length > 0) {
            this.isMassMail = true;
            this.accountList = accountList;
            this.contact = null;
            this.account = null;

        }
        else {
            this.isMassMail = false;
            this.account = account;
            this.contact = contact;
            console.log("[Click2Mail] Getting groups for account: " + account.Id);

            this.emailData.accountId = this.account.Id;
            this.relatedAccountName = this.account.ShownName;

            if (this.account.IsPerson) {
                this.getRelatedAccounts(this.account.Id);
            }
            else if (this.contact) {
                this.getRelatedAccounts(this.contact.PersonId);
                this.emailData.contactId = this.contact.Id;
            }

            this.getGroups(this.account.Id);
        }

        this.actionGroupId = actionGroupId;
        console.log("[Click2Mail] Setting GroupId:" + this.actionGroupId);

        this.displayModal = true;
    }

    getRelatedAccounts(accountId: string) {
        this._api.get("Accounts/" + accountId + "/RelatedCompanies").map(res => res.json()).subscribe(res => {
            if (res.IsSuccess) {
                let label = this.account.IsPerson ? this.account.ShownName : (this.contact.FirstName + " " + this.contact.LastName);
                this.relatedAccounts = [{ CompanyName: label }, ...res.Data];
            }
        });
    }

    selectAccount(relation: any) {

        if (!relation || !relation.CompanyId) {
            this.emailData.contactId = null;
            if (this.account.IsPerson) {
                this.emailData.accountId = this.account.Id;
            }
            else {
                this.emailData.accountId = this.contact.PersonId;
            }

        }
        else {
            this.emailData.accountId = relation.CompanyId;
            this.emailData.contactId = relation.PersonId;
        }

        this.relatedAccountName = relation.CompanyName;
        this.getGroups(this.emailData.accountId);
    }

    selectGroup(group: any) {
        this.selectedGroupName = group.label;
        this.actionGroupId = group.value;
    }

    getGroups(id: string) {
        this.actionService.getGroupList(id).subscribe(list => {
            console.log("[Click2Mail] Groups Found: ", list);
            let label = this.translate.instant('SAVETONEWACTIVITY');
            this.groups = [new ChosenOptionModel("", label), ...list];
            let group = this.groups.find(x => x.value == this.actionGroupId);

            this.selectedGroupName = group ? group.label : label;
            this.actionGroupId = group ? group.value.toString() : "";
        });
    }

    public replaceTags() {
        console.log("[Click2Mail] Replacing tags in.", this.emailData);

        this.tagWarning = false;

        let firstName = this.contact ? this.contact.FirstName : this.account ? this.account.FirstName : "";
        let lastName = this.contact ? this.contact.LastName : this.account ? this.account.LastName : "";
        let fullName = firstName + " " + lastName;
        let accountName = this.account ? this.account.ShownName : "";
        let fullAccountName = this.account ? this.account.FullName : "";

        if (this.emailData.subject.indexOf("[FirstName]") != -1 || this.emailData.body.indexOf("[FirstName]") != -1) {
            console.log("FirstName");
            if (!firstName) this.tagWarning = true;
            else{
                this.emailData.subject = this.emailData.subject.split("[FirstName]").join(firstName);
                this.emailData.body = this.emailData.body.split("[FirstName]").join(firstName);
            }
        }

        if (this.emailData.subject.indexOf("[LastName]") != -1 || this.emailData.body.indexOf("[LastName]") != -1) {
            console.log("LastName");
            if (!lastName) this.tagWarning = true;
            else {
                this.emailData.subject = this.emailData.subject.split("[LastName]").join(lastName);
                this.emailData.body = this.emailData.body.split("[LastName]").join(lastName);
            }
        }

        if (this.emailData.subject.indexOf("[FullName]") != -1 || this.emailData.body.indexOf("[FullName]") != -1) {
            console.log("FullName");
            if (!fullName || fullName == " ") this.tagWarning = true;
            else {
                this.emailData.subject = this.emailData.subject.split("[FullName]").join(fullName);
                this.emailData.body = this.emailData.body.split("[FullName]").join(fullName);
            }
        }

        if (this.emailData.subject.indexOf("[AccountName]") != -1 || this.emailData.body.indexOf("[AccountName]") != -1) {
            console.log("AccountName");
            if (!accountName) this.tagWarning = true;
            else {
                this.emailData.subject = this.emailData.subject.split("[AccountName]").join(accountName);
                this.emailData.body = this.emailData.body.split("[AccountName]").join(accountName);
            }
        }

        if (this.emailData.subject.indexOf("[AccountFullName]") != -1 || this.emailData.body.indexOf("[AccountFullName]") != -1) {
            console.log("AccountFullName");
            if (!fullAccountName) this.tagWarning = true;
            else {
                this.emailData.subject = this.emailData.subject.split("[AccountFullName]").join(fullAccountName);
                this.emailData.body = this.emailData.body.split("[AccountFullName]").join(fullAccountName);
            }
        }

        this.emailData.body = this.emailData.body.split("[MyFirstName]").join(this.user.FirstName);
        this.emailData.subject = this.emailData.subject.split("[MyFirstName]").join(this.user.FirstName);
        this.emailData.body = this.emailData.body.split("[MyLastName]").join(this.user.LastName);
        this.emailData.subject = this.emailData.subject.split("[MyLastName]").join(this.user.LastName);

        this._$element.redactor('code.set', this.emailData.body);
    }

    public cancle() {
        if (this.isTemplateMode) {
            if (this.emailData.id == "NEW") {
                this.emailData.id = "";
            }
            this.templateMode(false);
            this.replaceTags();
        }
        else {
            this.hideModal();
        }
    }

    public hideModal(): void {
        this.displayModal = false;
        this._$element.redactor('code.set', '');
    }

    public clickToClose(event: any) {
        let target = event.target || event.srcElement;
        if (target.id === 'emailModal') {
            this.hideModal();
        }
    }
}

