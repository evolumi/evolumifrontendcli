import { Component, Input, ElementRef, Output, EventEmitter } from "@angular/core";

@Component({
    selector: "div.bubble-popup",
    templateUrl: "bubble-popup.html",
    styleUrls: ["bubble-popup.css"],
    host: {
        '(document:click)': 'onClick($event)',
    },
})

export class BubblePopup {

    @Input() arrowPosition: string = "top";
    @Input() classAvoidClose: string;
    @Input() classes: string;
    @Output() closed = new EventEmitter<boolean>();
    @Output() opened = new EventEmitter<boolean>();
    public popupVisible: boolean;

    constructor(private elem: ElementRef) {

    }

    getClass(tri2: boolean) {
        return `${this.arrowPosition}Arrow`;
    }

    open() {
        if (!this.popupVisible) {
            setTimeout(() => {
                this.popupVisible = true;
                this.opened.emit(true);
            }, 0);
        }
    }

    close() {
        this.popupVisible = false;
        this.closed.emit(true);
    }

    onClick(event: any) {
        if (this.popupVisible) {
            if (this.classAvoidClose) {
                if (event.target.className.indexOf(this.classAvoidClose) > -1) {
                    return;
                }
            }
            // const updateElem = document.getElementById('bubblePopupElem');
            const images = Array.from(this.elem.nativeElement.getElementsByTagName('img'));
            const contains = images.some((img: any) => img.isEqualNode(event.target));

            if (!contains && !this.elem.nativeElement.contains(event.target)) {
                this.popupVisible = false;
                this.closed.emit(true);
            }
        }
    }
}