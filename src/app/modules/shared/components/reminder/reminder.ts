import { Component, Input, Output, EventEmitter } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ChosenOptionModel } from '../../../shared/components/chosen/chosen.models';

@Component({

    selector: 'reminder',
    template: ` 
                <div class="text-left" *ngIf="reminder">
                        <label *ngIf="showLabel">{{'REMINDER' | translate}}</label><label class="pull-right link" style="margin-top:5px;padding-bottom:0" (click)="change.emit()" *ngIf="showLabel">{{'REMOVE' | translate}}</label>
                        <div class="row" (click)="edit=true">
                            <div class="col-md-6">
                                <chosen [width]="'100%'" [options]="options" [ngModel]="reminder.TimeSpan" (change)="reminder.TimeSpan = $event;update()"></chosen>
                            </div>
                            <div class="col-md-6">
                                <chosen [width]="'100%'" [options]="dateFieldNames" [ngModel]=reminder.DateFieldName (change)="reminder.DateFieldName = $event;update()"></chosen>
                            </div>
                        </div>
                </div>

                <div *ngIf="!reminder">
                    <span (click)="newReminder()" class="link" [style.font-size]="size + 'px'"><i class="fa fa-clock-o" tooltip="{{'REMINDER' | translate}}" placement="bottom"></i>&nbsp;{{ (showLabel ? ('REMINDME'| translate) : "")}}</span>
                </div>
             `
})

export class ReminderSelectComponent {

    @Output() change = new EventEmitter();

    @Input() reminder: any;
    @Input() showLabel: boolean = true;
    @Input() size: number = 12;

    @Input() dateFieldNames: Array<ChosenOptionModel>;
    public options: Array<ChosenOptionModel>;

    constructor(private translate: TranslateService) {

        this.options = [
            new ChosenOptionModel(900000, this.translate.instant("15MINBEFORE")),
            new ChosenOptionModel(6300000, this.translate.instant("60MINBEFORE")),
            new ChosenOptionModel(151200000, this.translate.instant("1DAYBEFORE")),
            new ChosenOptionModel(1058400000, this.translate.instant("1WEEKBEFORE")),
            new ChosenOptionModel(4536000000, this.translate.instant("30DAYSBEFORE")),
        ];
    }

    public update() {
        this.change.next(this.reminder);
    }

    public getTimeString(): string {
        return "";
    }

    public newReminder() {
        this.change.next({ TimeSpan: 900000, DateFieldName: this.dateFieldNames[0].value });
    }

}