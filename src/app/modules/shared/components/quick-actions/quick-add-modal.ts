import { Component } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { ButtonPanelService } from '../../../../services/button-panel.service';
import { Shortcuts } from '../../../../services/configuration';

@Component({
    selector: 'quick-add-modal',
    templateUrl: "quick-add-modal.html",
})

export class QuickAddModal {

    public displayModal: boolean;

    private sub: Subscription;

    constructor(
        private button: ButtonPanelService
    ) {
        this.button.addHeaderButton("QUICKADD", "quickadd", "bolt", Shortcuts.quickadd);
        this.sub = this.button.commandObservable().subscribe(command => {
            if (command == "quickadd") {
                this.toggleModal();
            }
        })
    }

    toggleModal() {
        this.displayModal = !this.displayModal;
    }

    public showModal(): void {
        this.displayModal = true;
    }

    public hideModal(): void {
        this.displayModal = false;
    }


    clickToClose(event: any) {
        var target = event.target || event.srcElement;
        if (target.id === 'quick-add-popup') {
            this.hideModal();
        }
    }
}
