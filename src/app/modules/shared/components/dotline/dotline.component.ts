import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'dotline',
    templateUrl: './dotline.html',
    styleUrls: ['./dotline.css']
})

export class DotlineComponent {
    @Input() options: any[];
    @Input() value: number;
    @Input() tooltips: boolean;
    @Input() clickable: boolean;
    @Output() valueChanged = new EventEmitter<number>();

    optionClicked(e: any, index: number){
        this.valueChanged.emit(index);
    }
    constructor() { }    
}