import { Component, Input, ViewChild, Output, EventEmitter } from "@angular/core";
import { ImageCropperComponent, CropperSettings } from "ng2-img-cropper";

@Component({

    selector: 'cropmodal',
    templateUrl: "cropmodal.html",
    styleUrls: ["cropmodal.css"]
})
export class CropModal {
    @Input() settings: CropperSettings;
    @Input() data: any;
    @Output() cropped = new EventEmitter();
    public cropperSettings: CropperSettings;
    public displayModal: boolean;
    @ViewChild('imgCropper') imgCropper: ImageCropperComponent;

    constructor() {
        this.setupSettings();
    }

    setupSettings() {
        this.cropperSettings = new CropperSettings();
        this.cropperSettings.width = 200;
        this.cropperSettings.height = 200;
        this.cropperSettings.croppedWidth = 200;
        this.cropperSettings.croppedHeight = 200;
        this.cropperSettings.canvasWidth = 400;
        this.cropperSettings.canvasHeight = 300;
        this.cropperSettings.noFileInput = true;

    }
    getCroppedImage(saveTwice?: boolean) {
        var img = this.imgCropper.cropper.getCroppedImage();
        this.cropped.emit({ image: img });
    }

    croppedEvent($event: any) {

    }
}