﻿import { ChosenOption, ChosenOptionsGroup } from "./chosen";

export class ChosenOptionModel implements ChosenOption {
    public value: string | number;
    public label: string | number;
    public group: string;
    public color: string;
    constructor(value: string | number, label: string | number, group?: string, color?: string) {
        this.value = value;
        this.label = label;
        this.group = group;
        this.color = color;
    }
}

export class ChosenOptionGroupModel implements ChosenOptionsGroup {
    public value: string | number;
    public label: string | number;
    constructor(value: string | number, label: string | number) {
        this.value = value;
        this.label = label;
    }
}