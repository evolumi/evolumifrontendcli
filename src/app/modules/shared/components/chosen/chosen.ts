import { Component, OnInit, OnChanges, AfterViewInit, OnDestroy, EventEmitter, ElementRef, Input, Output, SimpleChanges } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { Host, Directive, forwardRef } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ChosenOptionModel } from './chosen.models';

declare var $: JQueryStatic;

export interface ChosenOption {
    value: string | number;
    label: string | number;
    group?: string;
    color?: string;
}

export interface ChosenOptionsGroup {
    value: string | number;
    label: string | number;
}

@Component({
    selector: 'chosen',
    templateUrl: './chosen.html',
})
export class ChosenComponent implements OnInit, AfterViewInit, OnChanges, OnDestroy {

    @Output() change: EventEmitter<any>;
    @Output() onSearch: EventEmitter<string> = new EventEmitter();
    @Output() onScroll: EventEmitter<any> = new EventEmitter();

    @Input() multiple: boolean;

    @Input() allowSingleDeselect: boolean;
    @Input() disableSearch: boolean;
    @Input() disableSearchThreshold: number;
    @Input() enableSplitWordSearch: boolean;

    @Input() maxSelectedOptions: number;
    @Input() noResultsText: string;
    @Input() placeholderTextMultiple: string = this.translate.instant('SELECTOPTIONS');
    @Input() placeholderTextSingle: string = this.translate.instant('SELECTOPTION');
    @Input() placeholderText: string;
    @Input() searchContains: boolean;
    @Input() singleBackstrokeDelete: boolean;
    @Input() width: number | string = '100%';
    @Input() displayDisabledOptions: boolean;
    @Input() displaySelectedOptions: boolean;
    @Input() getEventOnChange: boolean;
    @Input() emitChosenModel: boolean;
    @Input() labelHtml: boolean;
    @Input() useColors: boolean;

    @Input() options: Array<ChosenOption>;
    @Input() stringOptions: Array<string>;
    @Input() optionsGroups: Array<ChosenOptionsGroup>;

    chosenConfig: Chosen.Options = {};

    elementRef: ElementRef;
    selectElement: JQuery;
    inputElement: JQuery;

    value: any = null;

    constructor(elementRef: ElementRef,
        private translate: TranslateService) {
        this.elementRef = elementRef;
        this.change = new EventEmitter();
    }

    private page: number = 1;
    private term: string = "";

    private doSearch(term: string) {
        this.term = term || "";
        this.page = 1;
        console.log("searching chosen", this.term);
        this.onSearch.emit(term);
    }

    private scroll() {
        this.page++;
        this.term = this.term || "";
        console.log("scrolling chosen", this.term, this.page);
        this.onScroll.emit({ term: this.term, page: this.page });
    }

    test() {
        console.log('TEST');
    }

    ngOnInit() {
        if (this.allowSingleDeselect != null) {
            this.chosenConfig.allow_single_deselect = this.allowSingleDeselect;
        }

        if (this.disableSearch != null) {
            this.chosenConfig.disable_search = this.disableSearch;
        }

        if (this.disableSearchThreshold != null) {
            this.chosenConfig.disable_search_threshold = this.disableSearchThreshold;
        }

        if (this.enableSplitWordSearch != null) {
            this.chosenConfig.enable_split_word_search = this.enableSplitWordSearch;
        }

        if (this.maxSelectedOptions != null) {
            this.chosenConfig.max_selected_options = this.maxSelectedOptions;
        }

        if (this.noResultsText != null) {
            this.chosenConfig.no_results_text = this.noResultsText;
        }

        if (this.multiple) {
            this.chosenConfig.placeholder_text_multiple = this.placeholderTextMultiple || this.placeholderText;
        }

        if (!this.multiple) {
            this.chosenConfig.placeholder_text_single = this.placeholderTextSingle || this.placeholderText;
        }

        if (this.searchContains != null) {
            this.chosenConfig.search_contains = this.searchContains;
        }

        if (this.singleBackstrokeDelete != null) {
            this.chosenConfig.single_backstroke_delete = this.singleBackstrokeDelete;
        }

        if (this.width != null) {
            this.chosenConfig.width = this.width.toString();
        }

        if (this.displayDisabledOptions != null) {
            this.chosenConfig.display_disabled_options = this.displayDisabledOptions;
        }

        if (this.displaySelectedOptions != null) {
            this.chosenConfig.display_selected_options = this.displaySelectedOptions;
        }
        // this.chosenConfig.scroll_to_highlighted = false;
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes["options"] != null) {
            setTimeout(() => {
                this.selectElement.val(this.value);
                this.selectElement.trigger("chosen:updated");
            });
        }
        if (this.stringOptions && this.stringOptions.length) {
            console.log("[Chosen] Converting options");
            let newList: Array<ChosenOptionModel> = [];
            for (let op of this.stringOptions) {
                newList.push(new ChosenOptionModel(op, op));
            }
            this.options = [...newList];
            console.log("[Chosen] list", this.options);
        }
    }

    ngAfterViewInit() {


        let el: any = this.elementRef.nativeElement;
        this.selectElement = $(el).find("select");

        this.selectElement.chosen(this.chosenConfig);

        this.selectElement.on('change', (ev, e) => {
            if (this.getEventOnChange) {
                // console.log("Emitting 0", ev);
                this.change.emit(ev);
            } else
                if (this.emitChosenModel) {
                    let selectEl = el.querySelectorAll('select')[0] as HTMLSelectElement;
                    if (this.multiple) {
                        let selected: Array<ChosenOptionModel> = [];
                        selected.push(...Array.from(selectEl.options).filter((x: any) => x.selected).map((x: any) => new ChosenOptionModel(x.value, x.text)));
                        // console.log("Emitting 1", selected);
                        this.change.emit(selected);
                    } else {
                        const text = selectEl[selectEl.selectedIndex].text;
                        const value = selectEl.value;
                        // console.log("Emitting 2", value, text);
                        this.change.emit(new ChosenOptionModel(value, text));
                    }
                } else {
                    let values = this.selectElement.val();
                    // console.log("Emitting 3", values);
                    this.change.emit(values);
                }
        });

        this.selectElement.trigger("chosen:updated");

        //Emit search
        this.inputElement = $(el).find('input');
        this.inputElement.on('keyup', () => {
            let that = this;
            searchDelay(function () {
                let val = that.inputElement.val();
                that.doSearch(val.toString());
            });
        });

        var searchDelay = (function () {
            var timer = 0;
            return function (callback: any) {
                clearTimeout(timer);
                timer = setTimeout(callback, 500);
            };
        })();

        // Check for scrollBottom 
        let drop = $(el).find('.chosen-results');
        drop.on('scroll', this.chk_scroll.bind(this));
    }

    ngOnDestroy() {
        this.selectElement.chosen(<any>'destroy');
    }

    private chk_scroll(e: any) {
        var elem = $(e.currentTarget);
        if (elem[0].scrollHeight - elem.scrollTop() <= elem.outerHeight()) {
            this.scroll();
        }
    }
}

const CHOSEN_VALUE_ACCESSOR: any = {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => ChosenControlValueAccessor),
    multi: true
}

@Directive({
    selector: 'chosen[ngControl],chosen[ngFormControl],chosen[ngModel]',
    host: { "(change)": 'onChange($event)', '(blur)': 'onTouched()' },
    providers: [CHOSEN_VALUE_ACCESSOR]
})
export class ChosenControlValueAccessor implements ControlValueAccessor {

    el: any;

    onChange = (_: any) => {
    };

    onTouched = () => {

    };

    constructor(private _elementRef: ElementRef, @Host() private chosenComponent: ChosenComponent) {
        this.el = this._elementRef.nativeElement;
    }

    writeValue(value: any): void {

        setTimeout(() => {
            var selectElement = $(this.el).find("select");
            selectElement.val(value);
            this.chosenComponent.value = value;
            selectElement.trigger('chosen:updated');
        });
    }

    registerOnChange(fn: (_: any) => {}): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: () => {}): void {
        this.onTouched = fn;
    }
}

export var Chosen: Array<any> = [
    ChosenComponent,
    ChosenControlValueAccessor
];


export function CONST_EXPR<T>(expr: T): T {
    return expr;
}