import { Component, Inject, Input, Output, EventEmitter, ElementRef } from '@angular/core';
import { NewsfeedService } from '../../../../../services/newfeed.service';
import { LocalStorage } from '../../../../../services/localstorage';
import { IntercomService } from '../../../../../services/interecomService';
import { NewsfeedModel } from '../../../../../models/newsfeed.models';
import { Subject } from 'rxjs/Subject';

declare var $: JQueryStatic;

@Component({

    selector: 'addnotes',
    template: ` <div class="tab-pane active" id="stab2">
                    <div id="binding-container-newsfeed">
                        <form role="form" class="add-note">
                            <div class="form-group" [class.small-redactor]="isSmall" style="text-align:left!important;">
                                <input type="hidden" id="newsTextArea" [(ngModel)]="newsTextArea" rows="2" class="form-control"
                                (keyup)="newsError=false" [ngModelOptions]="{standalone: true}">
                                <textarea class="redactor" style="margin-right:100px;" name="content" [(ngModel)]="newsTextArea"></textarea>
                                <div [hidden]="!newsError" class="bg-danger">Invalid</div>
                            </div>
                            <button *ngIf="!isEdit" type="button" (click)="addNews()" class="btn custom pull-right"><i class="fa fa-pencil"></i> {{ 'ADDNOTE' | translate }}</button>
                            <div class="clearfix"></div>
                        </form>
                        <div class="clearfix"></div>`

})

export class AddNotes {

    @Input() account: any;
    @Input() isEdit: boolean = false;
    @Input() news: NewsfeedModel;
    @Input() isSmall: boolean;
    @Output() textChange = new EventEmitter<string>();
    public DocumentId: string;
    public newsTextArea: string;
    private _$element: any;
    private keyUpSubject: Subject<string> = new Subject();
    public newsError: boolean;

    constructor(private _el: ElementRef,
        @Inject(IntercomService) public intercomService: IntercomService,
        private _newsfeed: NewsfeedService,
        private _localStore: LocalStorage) { }

    ngOnInit() {
        this.keyUpSubject
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe(value => this.textChange.emit(value));

        this._$element = (<any>$(this._el.nativeElement)).find('.redactor');
        var self = this;
        this._$element.redactor({
            //air:true,
            focus: true,
            buttons: [],
            callbacks: {
                change: function () {
                    self.newsTextArea = this.code.get();
                    self.onKeyUp(this.code.get());
                }
            }
        });

        if (this.isEdit) {
            this.newsTextArea = this.news.Text;
            this._$element.redactor('code.set', this.newsTextArea);
        }
    }

    onKeyUp(value: string) {
        this.keyUpSubject.next(value);
    }

    addNews() {
        if (typeof this.account) {
            this.DocumentId = this.account;
        }
        this._$element.redactor('code.set', '');
        var data = { 'AccountId': this.account, 'DocumentId': this.DocumentId, 'Type': 'Account', 'Text': this.newsTextArea, 'UserId': this._localStore.get("userId"), accountName: this.account.FullName };
        this._newsfeed.addNews(data);
        this.intercomService.trackEvent('notes-saved');
        this.newsTextArea = '';
    }

    ngOnChanges(changes: any) {
        if (typeof changes['account'] != "undefined" && typeof changes['account']['currentValue'] != "undefined" && changes['account']['currentValue'] != "") {

            this.account = changes['account']['currentValue'];
        }
    }

    ngOnDestroy() {
        this._$element.redactor('core.destroy');
    }
}
