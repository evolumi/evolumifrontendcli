import { Component, Input } from '@angular/core';
import { NewsfeedService } from '../../../../services/newfeed.service';
import { TranslateService } from '@ngx-translate/core';
import { NewsfeedModel } from '../../../../models/newsfeed.models';
import { FilterCollection } from '../../../../services/filterservice';
import { ActionsCollection } from '../../../../services/actionservice';

import { AccountsCollection } from '../../../../services/accounts.service';
import { DealAddEdit } from "../../../../services/dealAddEdit.service";

@Component({

    selector: 'newsitem',
    templateUrl: 'newsitem.component.html',
})
export class NewsItemComponent {

    @Input() news: NewsfeedModel;
    @Input() showEdit: boolean;

    public text: Array<string> = [];

    constructor(
        public service: NewsfeedService,
        private translateService: TranslateService,
        public _filter: FilterCollection,
        public actionServie: ActionsCollection,
        private _dealAddEdit: DealAddEdit,
        private accService: AccountsCollection
    ) {

    }

    ngOnChanges() {

        var translation: string = this.news.LabelString ? this.translateService.instant(this.news.LabelString, this.news.LabelNames) : "";
        this.text = translation.split(" ");
    }

    showContact(accountId: string, contactId: string) {
        this.accService.getRelationFromAccountIds(accountId, contactId).subscribe(res => {
            if (res.IsSuccess) {
                this.accService.showAccountRelationPopup(res.Data);
            }
        })
    }

    public showAction(id: string) {
        this.actionServie.showGroupByActionId(id);
    }

    public showDeal(id: string) {
        this._dealAddEdit.getDealById(this.news.DocumentId);
    }

    deleteNews(id: string) {
        this.service.deleteNews(id);
    }

    showEditBtn(item: any): boolean {
        return this.showEdit && item.Type == "Note";
    }

}