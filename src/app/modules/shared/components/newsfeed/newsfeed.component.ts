import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { NewsfeedService } from '../../../../services/newfeed.service';
import { FilterCollection } from '../../../../services/filterservice';
import { ModalService } from '../../../../services/modal.service';
import { AccountsCollection } from '../../../../services/accounts.service';

@Component({

    selector: 'newsfeeds',
    templateUrl: 'newsfeed.html',
})
//
export class Newsfeeds implements OnChanges {
    @Input() newsSearch: string;
    @Input() showEdit: boolean;
    @Input() scroll: number;
    public dealChangeDetector: any;
    public contactChangeDetector: any;
    public accountChangeDetector: any;
    public actionChangeDetector: any;
    constructor(
        public news: NewsfeedService,
        public _filter: FilterCollection,
        public modal: ModalService,
        private accService: AccountsCollection
    ) {
        this.news.resetFeeds();
    }
    ngOnInit() {
        this.news.continueLoading = true;
        this.dealChangeDetector = this._filter.dealchange$.subscribe(res => {
            this.news.getNewsFeeds()
        });
        this.contactChangeDetector = this._filter.contactchange$.subscribe(res => {
            this.news.getNewsFeeds()
        });
        this.accountChangeDetector = this.accService.accountChange().subscribe(res => {
            this.news.getNewsFeeds()
        });
        this.actionChangeDetector = this._filter.actionchange$.subscribe(res => {
            this.news.getNewsFeeds()
        });
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes && changes['scroll'] && changes['scroll'].currentValue) {
            this.news.makeSroll();
        }
        if (changes['newsSearch']) {
            this.news.reset();
            this.news.UpdateSearchFields(changes['newsSearch'].currentValue);
        }
    }

    ngOnDestroy() {
        if (this.dealChangeDetector) this.dealChangeDetector.unsubscribe();
        if (this.contactChangeDetector) this.contactChangeDetector.unsubscribe();
        if (this.accountChangeDetector) this.accountChangeDetector.unsubscribe();
        if (this.actionChangeDetector) this.actionChangeDetector.unsubscribe();
        this.news.continueLoading = false;
    }

}
