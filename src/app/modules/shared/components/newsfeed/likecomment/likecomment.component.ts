import { Component, Input } from '@angular/core';
import { NewsfeedService } from '../../../../../services/newfeed.service';
import { Api } from '../../../../../services/api';


@Component({

	selector: 'likecomment',
	templateUrl: 'likecomment.html',
})

export class Likecomment {
	@Input() like: any;
	constructor(public newsfeed: NewsfeedService, public _http: Api) { }

	likeit(newsid: string) {
		this._http.post(`News/${newsid}/Like`, '')
			.map(res => res.json())
			.subscribe(res => { });
	}
	unlikeit(newsid: string) {
		this._http.delete(`News/${newsid}/Unlike`)
			.map(res => res.json())
			.subscribe(res => { });
	}
}
