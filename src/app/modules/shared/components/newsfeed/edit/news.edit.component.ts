import { Component, Input } from '@angular/core';
import { NewsfeedService } from '../../../../../services/newfeed.service';
import { NewsfeedModel } from '../../../../../models/newsfeed.models';

@Component({
	selector: 'edit-note',
	template: `
	<div class="form-group" >
		<addnotes *ngIf="news.newEdit" [isSmall]="true" [isEdit]="true" [news]="news" (textChange)="onTextChange($event)"></addnotes>
		<!--<textarea *ngIf="news.newEdit!=false" [(ngModel)]="txt" rows="2" class="form-control" ></textarea>-->
		<a  (click)="news.newEdit=false;editNews(news)" *ngIf="news.newEdit" class="pull-right link tiny"  >{{ 'UPDATE' | translate }}</a>
	    <p *ngIf="!news.newEdit" id="edtNewsP{{news.Id}}">
	</div>
	<p style="display:inline-block; white-space: pre-line; width:calc(100% - 60px);"  *ngIf="!news.newEdit" [innerHtml]="news.Text"></p>
	
	<span *ngIf="showEdit==true" class="commentsActions">
		<a  *ngIf="!news.newEdit" class="comments-icon" style="color: #333;" (click)="news.newEdit=true;txt=news.Text">
	    	<i class="fa fa-edit"></i>
		</a>
		<a  *ngIf="!news.newEdit" class="comments-icon" style="color: #333; margin-left: 3px;"  id="edtNewsDeleButton{{news.Id}}" (click)="news.newEdit=false;deleteNews(news)">
			<i class="fa fa-trash"></i>
		</a>
	</span>

	`,
	styles: [
		`.commentsActions {
			display: inline-block;
			float: right;
		}
		.comments-icon:hover {
			color: #5fc764;
		}
		`
	]
})

export class EditNotes {

	@Input() news: any;
	@Input() showEdit: boolean;
	public txt = '';

	constructor(public _newsfeed: NewsfeedService) { }

	editNews(news: NewsfeedModel) {
		// console.log('EDIT', this.news.Text);
		this._newsfeed.editNews(news.Id, this.txt)
			.subscribe(res => {
				console.log(res.json);
			})
		news.Text = this.txt;
	}

	deleteNews(news: NewsfeedModel) {
		this._newsfeed.deleteNews(news.Id);
		var n = this._newsfeed.feeds.find(x => x.Id == news.Id);
		this._newsfeed.feeds.splice(this._newsfeed.feeds.indexOf(n), 1);
	}

	onTextChange(value: string) {
		this.txt = value;
	}
}
