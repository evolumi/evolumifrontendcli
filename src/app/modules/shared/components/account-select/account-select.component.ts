import { Component, Input, Output, EventEmitter } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ChosenOptionModel } from "../../../shared/components/chosen/chosen.models";
import { AccountsCollection } from '../../../../services/accounts.service';

@Component({
    selector: "account-select",

    template: `<chosen 
                    [width]="'100%'" 
                    [multiple]="multi" 
                    [class.multichosen]="multi" 
                    [placeholderTextSingle]="placeholder" 
                    [placeholderTextMultiple]="placeholder" 
                    [options]="accounts" 
                    [ngModel]="value" 
                    [ngModelOptions]="{standalone: true}"
                    (change)="setValue($event)" 
                    (onSearch)="search($event)" 
                    (onScroll)="scroll($event.term, $event.page)" >
                </chosen>`
})

export class AccountSelectComponent {

    @Input() onlyCompanies: boolean = false;
    @Input() onlyPerson: boolean = false;
    @Input() required: boolean = false;
    @Input() value: string | Array<string>;
    @Input() valueLabel: string | Array<string>;
    @Input() multi: boolean = false;
    @Input() placeholder: string;
    @Input() noSelectText: string;

    @Output() selectAccount: EventEmitter<string | Array<string>> = new EventEmitter();

    public accounts: Array<ChosenOptionModel> = [];
    private default: Array<ChosenOptionModel> = [];

    private deselect: ChosenOptionModel;
    private loaded: boolean = false;

    constructor(private accountService: AccountsCollection, private translate: TranslateService) {

    }

    ngOnChanges(changes: any) {
        if (changes["value"] && changes["value"].currentValue) {
            if (this.value && !this.multi) {
                if (this.default.findIndex(x => x.value == this.value) == -1) {
                    if (this.valueLabel && this.valueLabel.length) {
                        this.default.push(new ChosenOptionModel(<string>this.value, <string>this.valueLabel));
                    }
                    else {
                        if (this.value && this.value != "00000000-0000-0000-0000-000000000000") {
                            this.accountService.getAccountName(<string>this.value).subscribe(name => {
                                this.default.push(new ChosenOptionModel(<string>this.value, name.Data));
                            });
                        }
                    }
                }
            }
            if (this.value && this.multi) {

                if (this.value && this.value != "00000000-0000-0000-0000-000000000000") {
                    console.log("[Account-Select] Getting named from API", this.value);
                    this.accountService.getAccountNames(<Array<string>>this.value).subscribe(nameList => {
                        for (let acc of nameList) {
                            if (this.default.findIndex(x => x.value == acc.Id) == -1) {
                                console.log("[Account-Select] Adding default value", acc);
                                this.default.push(new ChosenOptionModel(acc.Id, acc.Name));
                            }
                        }
                    });
                }
            }
        }
        var name = this.noSelectText || "";
        if (!name && !this.required) {
            name = this.translate.instant("NOSELECTEDACCOUNT");
            if (this.onlyCompanies) name = this.translate.instant("NOSELECTEDCOMPANY");
            if (this.onlyPerson) name = this.translate.instant("NOSELECTEDPERSON");
        }
        this.deselect = new ChosenOptionModel("", name);

        if (!this.loaded) {
            this.search("");
        }
    }

    setValue(value: any) {
        // This will not accept Events as valus. (yeah i know its ugly :)
        if ((value && value.currentTarget == undefined) || value == "") {
            if (this.multi) {
                for (let val of value)
                    this.addToDefault(val);
            }
            else {
                this.addToDefault(<string>value);
            }
            console.log("[Account-Select] Emitting", value);
            this.selectAccount.emit(value);
        }
    }

    private addToDefault(val: string) {
        if (this.default.findIndex(x => x.value == val) == -1) {
            let acc = this.accounts.find(x => x.value == val);
            this.default.push(new ChosenOptionModel(acc.value, acc.label))
        }
    }

    search(term: string) {
        this.accounts = [];
        if (!this.required || (this.noSelectText && this.noSelectText.length)) this.accounts.push(this.deselect);
        this.scroll(term, 1);
    }

    scroll(term: string, page: number = 1) {
        console.log("[Account-Select] Searching", term, page);
        if (this.onlyPerson) this.accountService.personList(term, page, accounts => this.handleResponse(accounts));
        else if (this.onlyCompanies) this.accountService.companyList(term, page, accounts => this.handleResponse(accounts));
        else this.accountService.accountList(term, page, accounts => this.handleResponse(accounts));
    }

    private handleResponse(accounts: Array<any>) {
        this.accounts = [...this.accounts].concat(accounts);
        this.addDefault();
        this.loaded = true;
    }


    private addDefault() {
        let accs: ChosenOptionModel[] = [];
        for (let acc of this.default) {
            if (this.accounts.findIndex(x => x.value == acc.value) == -1) {
                accs.push(acc);

            }
        }
        this.accounts = [...this.accounts].concat(accs);
    }
}