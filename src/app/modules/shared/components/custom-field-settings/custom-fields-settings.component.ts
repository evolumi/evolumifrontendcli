import { Component, Input, Output, EventEmitter } from "@angular/core";

@Component({

    selector: "custom-field-settings",
    templateUrl: "./custom-fields-settings.component.html"

})
export class CustomFieldSettingsComponent {

    @Input() fields: Array<any>
    @Input() title: string;

    @Output() save: EventEmitter<Array<any>> = new EventEmitter();

    private fieldTypes: Array<any> = [];

    constructor() {
        this.fieldTypes.push(
            { value: "Text", label: "TEXT" },
            { value: "Number", label: "NUMBER" },
            { value: "Options", label: "OPTIONS" },
            { value: "Date", label: "DATE" });
    }

    newField() {
        this.fields.push({ Name: "", Type: "Text", Options: [] });
    }

    saveFields() {
        this.save.emit(this.fields);
    }

    delete(field: any) {
        if (confirm("WARNDDELFIELD")) {
            this.fields.splice(this.fields.findIndex(x => x.Id == field.Id), 1);
            this.save.next(this.fields);
        }
    }
}