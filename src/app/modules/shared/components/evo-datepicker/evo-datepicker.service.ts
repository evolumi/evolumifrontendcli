import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { MonthData, DisplayNameData, Day } from './evo-datepicker.models';
import { Subscription } from 'rxjs/Subscription';

@Injectable()
export class EvoDatepickerService {

    private days: number[] =
    [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31];

    private weekends: number[] = [0, 6];

    private daysOfWeek: DisplayNameData[];
    private months: DisplayNameData[];

    private transSub: Subscription;

    constructor(private translate: TranslateService) {
        this.daysOfWeek = this.getDaysOfWeek();
        this.months = this.getMonths();
        this.transSub = this.translate.onLangChange
            .subscribe((x: any) => {
                this.daysOfWeek = this.getDaysOfWeek();
                this.months = this.getMonths();
            });
    }

    defaultDateFormat(timestamp: number) {
        let date = new Date(timestamp);
        return date.toString();
    }

    getDateStartOfDay(timestamp: number) {
        let date = new Date(timestamp);
        date.setHours(0);
        date.setMinutes(0);
        date.setSeconds(0);
        date.setMilliseconds(0);
        return date.getTime();
    }

    getDateEndOfDay(timestamp: number) {
        let date = new Date(timestamp);
        date.setHours(23);
        date.setMinutes(59);
        date.setSeconds(59);
        date.setMilliseconds(999);
        return date.getTime();
    }

    getDaysOfWeek() {
        return [
            { fullname: this.translate.instant('SUNDAY'), shortname: this.translate.instant('SUNDAY').substring(0, 2), isWeekend: true },
            { fullname: this.translate.instant('MONDAY'), shortname: this.translate.instant('MONDAY').substring(0, 2) },
            { fullname: this.translate.instant('TUESDAY'), shortname: this.translate.instant('TUESDAY').substring(0, 2) },
            { fullname: this.translate.instant('WEDNESDAY'), shortname: this.translate.instant('WEDNESDAY').substring(0, 2) },
            { fullname: this.translate.instant('THURSDAY'), shortname: this.translate.instant('THURSDAY').substring(0, 2) },
            { fullname: this.translate.instant('FRIDAY'), shortname: this.translate.instant('FRIDAY').substring(0, 2) },
            { fullname: this.translate.instant('SATURDAY'), shortname: this.translate.instant('SATURDAY').substring(0, 2), isWeekend: true },
        ];
    }

    getMonths() {
        return [
            { fullname: this.translate.instant('JANUARY'), shortname: this.translate.instant('JANUARY').substring(0, 3) },
            { fullname: this.translate.instant('FEBRUARY'), shortname: this.translate.instant('FEBRUARY').substring(0, 3) },
            { fullname: this.translate.instant('MARCH'), shortname: this.translate.instant('MARCH').substring(0, 3) },
            { fullname: this.translate.instant('APRIL'), shortname: this.translate.instant('APRIL').substring(0, 3) },
            { fullname: this.translate.instant('MAY'), shortname: this.translate.instant('MAY').substring(0, 3) },
            { fullname: this.translate.instant('JUNE'), shortname: this.translate.instant('JUNE').substring(0, 3) },
            { fullname: this.translate.instant('JULY'), shortname: this.translate.instant('JULY').substring(0, 3) },
            { fullname: this.translate.instant('AUGUST'), shortname: this.translate.instant('AUGUST').substring(0, 3) },
            { fullname: this.translate.instant('SEPTEMBER'), shortname: this.translate.instant('SEPTEMBER').substring(0, 3) },
            { fullname: this.translate.instant('OCTOBER'), shortname: this.translate.instant('OCTOBER').substring(0, 3) },
            { fullname: this.translate.instant('NOVEMBER'), shortname: this.translate.instant('NOVEMBER').substring(0, 3) },
            { fullname: this.translate.instant('DECEMBER'), shortname: this.translate.instant('DECEMBER').substring(0, 3) },
        ];
    }

    getMonthData(year: number, month: number, weekStart: number): MonthData {
        const firstDayOfMonth = new Date(year, month, 1);
        const lastDayOfMonth = new Date(year, month + 1, 0);
        const lastDayOfPreviousMonth = new Date(year, month, 0);
        const daysInMonth = lastDayOfMonth.getDate();
        const daysInLastMonth = lastDayOfPreviousMonth.getDate();
        const firstDayOfWeek = firstDayOfMonth.getDay();
        const leadingDays = ((firstDayOfWeek + 7) - weekStart) % 7;
        const trailingDays = (6 + weekStart - lastDayOfMonth.getDay()) % 7;
        const leadingDaysArr = this.days.slice(daysInLastMonth - leadingDays, daysInLastMonth);
        const trailingDaysArr = this.days.slice(0, trailingDays);

        let translatedDaysOfWeek = this.daysOfWeek.concat(this.daysOfWeek).splice(weekStart, 7);

        return new MonthData(year, month, this.weekends, weekStart, this.months[month].fullname, this.months[month].shortname, translatedDaysOfWeek, this.days.slice(0, daysInMonth).map(x => { let day: Day = { date: x }; return day }), leadingDaysArr.map(x => { let day: Day = { date: x }; return day }), trailingDaysArr.map(x => { let day: Day = { date: x }; return day }));
    }
}
