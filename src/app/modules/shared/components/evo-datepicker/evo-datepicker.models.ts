export class MonthData {
    constructor(
        public year: number,
        public month: number,
        public weekends: number[],
        public firstDayOfWeek: number,
        public fullName: string,
        public shortName: string,
        public translatedDaysOfWeek: DisplayNameData[],
        public days: Day[],
        public leadingDays: Day[],
        public trailingDays: Day[]) { }
}

export interface DisplayNameData {
    fullname: string;
    shortname: string;
    isWeekend?: boolean;
}

export interface Day {
    date: number;
    selectable?: boolean;
    selected?: boolean;
    today?: boolean;
    weekend?: boolean;
    inRange?: boolean;
}