import { Component, OnInit, OnDestroy, forwardRef, Input, ElementRef, ViewChild, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { EvoDatepickerService } from './evo-datepicker.service';
import { MonthData } from './evo-datepicker.models';
import { AuthService } from '../../../../services/auth-service';
import { LogedInUserModel } from '../../../../models/user.models';
import { Subscription } from 'rxjs/Subscription';

const noop = () => {
};

export const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => EvoDatepickerComponent),
    multi: true
};

@Component({
    selector: 'evo-datepicker',
    templateUrl: './evo-datepicker.component.html',
    styleUrls: ['./evo-datepicker.component.css'],
    providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR],
    host: {
        '(document:click)': 'onClick($event)'
    }
})
export class EvoDatepickerComponent implements OnInit, OnChanges, OnDestroy, ControlValueAccessor {

    @ViewChild('hourInput') hourInput: ElementRef;
    @ViewChild('minuteInput') minuteInput: ElementRef;

    @Input() defaultDate: number = new Date().getTime() // If not set, todays date
    @Input() timeOnly: boolean; // Show only TimePicker
    @Input() dateOnly: boolean; // Show only DatePicker
    @Input() minDate: number; // Dates eariler than this disabled
    @Input() maxDate: number; // Dates later than this disabled
    @Input() disabledDates: number[];
    @Input() noClose: boolean; // If you dont want datepicker to be closed on select
    @Input() hideClear: boolean; // Hide the x
    @Input() iconTopZero: boolean; // Depending on HTML it is sometimes needed to be zero position
    @Output() onShow = new EventEmitter<boolean>();
    @Output() valueChange = new EventEmitter<number>(); // only to be used if the ordinary ngModel and rangeEndDate, rangeStartDate are not sufficient

    // Range specific
    @Input() range: boolean;
    @Input() rangeStartDate: number;
    @Input() rangeEndDate: number;
    @Output() rangeStartDateChange = new EventEmitter<number>();
    @Output() rangeEndDateChange = new EventEmitter<number>();

    public inputId = this.getRandomId();

    public show: boolean;
    public is12h: boolean;
    public isPm: boolean;
    private weekStart: number // 0 = Sunday, 1 = Monday
    public monthData: MonthData; // The calendar data

    private currentUser: LogedInUserModel;
    private authSub: Subscription;

    private date: number; // The date of the picker, to separate from selectedDate when date is not selected
    private rangeCounter: number;

    private _selectedDate: number;
    get selectedDate() {
        return this._selectedDate;
    }

    set selectedDate(value: number) {
        if (value || value === 0) {
            this._selectedDate = value;
            // To trigger ngModelChange
            this.onChangeCallback(value);
        }
    }

    get today(): number {
        return this.dateSvc.getDateStartOfDay(new Date().getTime());
    }

    get year(): number {
        return new Date(this.date).getFullYear();
    }

    get month(): number {
        return new Date(this.date).getMonth();
    }

    get day(): number {
        return new Date(this.date).getDate();
    }

    private _hour: number = 0;
    get hour(): number {
        return this._hour;
    }
    set hour(value: number) {
        if (value !== null && value !== undefined) {
            this._hour = value;
            const maxHour = this.is12h ? 12 : 23;
            const minHour = this.is12h ? 1 : 0;
            if (value <= maxHour && value >= minHour) {
                let newDate = new Date(this.date);
                newDate.setHours(this.is12h ? this.convertFromAmPm(this.hour) : this.hour);
                this.date = newDate.getTime();
                this.selectedDate = this.date;
            }
        }
    }

    private _minute: number = 0;
    get minute(): number {
        return this._minute;
    }
    set minute(value: number) {
        if (value !== null && value !== undefined) {
            this._minute = value || 0;
            if (value <= 59 && value >= 0) {
                let newDate = new Date(this.date);
                newDate.setMinutes(this.minute);
                this.date = newDate.getTime();
                this.selectedDate = this.date;
            }
        }
    }

    get second(): number {
        return new Date(this.date).getSeconds();
    }

    get milliseconds(): number {
        return new Date(this.date).getMilliseconds();
    }

    //Placeholders for the callbacks which are later provided by the Control Value Accessor
    private onTouchedCallback: () => void = noop;
    private onChangeCallback: (_: any) => void = noop;

    constructor(private dateSvc: EvoDatepickerService,
        private authSvc: AuthService,
        private elRef: ElementRef) { }

    ngOnInit() {
        if (this.range) {
            this.timeOnly = false;
            this.dateOnly = true;
        }
        this.authSub = this.authSvc.currentUserObservable()
            .subscribe(user => {
                if (user) {
                    this.currentUser = user;
                    this.weekStart = this.currentUser.IsWeekStartFromSunday ? 0 : 1;
                    this.is12h = !this.currentUser.Is24DateTimeFormat;
                };
            })
        if (!this.defaultDate) this.defaultDate = Date.now();
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes) {
            if (changes['minDate']) {
                if (this.selectedDate && this.selectedDate < changes['minDate'].currentValue) {
                    this.selectedDate = changes['minDate'].currentValue;
                    this.valueChange.emit(this.selectedDate);
                }
            }
            if (changes['maxDate']) {
                if (this.selectedDate && this.selectedDate > changes['maxDate'].currentValue) {
                    this.selectedDate = changes['maxDate'].currentValue;
                    this.valueChange.emit(this.selectedDate);
                }
            }
        }
    }

    ngOnDestroy() {
        if (this.authSub) this.authSub.unsubscribe();
    }

    onIconClick() {
        const el = document.getElementById(this.inputId);
        el.click();
    }

    onFocus() {
        if (this.range) {
            this.date = this.rangeCounter === 2 ? this.rangeEndDate : (this.rangeStartDate || this.defaultDate)
        } else {
            this.date = this.selectedDate || this.defaultDate;
        }
        // this.selectedDate = this.selectedDate || this.defaultDate;
        this.monthData = this.dateSvc.getMonthData(this.year, this.month, this.weekStart);
        this.handleDays();

        if (this.range) {
            this.rangeCounter = Boolean(this.rangeStartDate && !this.rangeEndDate) ? 1 : 0;
        }

        this.show = true;
        this.onShow.emit(true);
        setTimeout(() => {
            this.initSetup();
        }, 0);
    }

    // To fix the inputs and other elements when picker is shown
    private initSetup() {
        if (!this.dateOnly) {
            const date = new Date(this.date);
            const hours = date.getHours();
            this.isPm = hours > 11 && hours !== 0;
            this.hour = this.is12h ? this.convertToAmPm(hours) : hours;
            this.minute = date.getMinutes();
        }

        if (this.disabledDates) {
            this.disabledDates = this.disabledDates.map(x => this.dateSvc.getDateStartOfDay(x));
        }

        if (!this.dateOnly) {
            const hourInput = <HTMLInputElement>this.hourInput.nativeElement;
            const minInput = <HTMLInputElement>this.minuteInput.nativeElement;
            hourInput.focus();
            hourInput.blur();
            minInput.focus();
            minInput.blur();
        }
    }

    selectDate(day: number, month?: number) {
        if (!this.isDateDisabled(day)) {
            // Change calendar if day of other month is picked
            if (month < this.monthData.month) {
                if (month < 0) {
                    this.monthData = this.dateSvc.getMonthData(this.monthData.year - 1, 11, this.weekStart);
                } else {
                    this.monthData = this.dateSvc.getMonthData(this.monthData.year, month, this.weekStart);
                }
            } else if (month > this.monthData.month) {
                if (month > 11) {
                    this.monthData = this.dateSvc.getMonthData(this.monthData.year + 1, 0, this.weekStart);
                } else {
                    this.monthData = this.dateSvc.getMonthData(this.monthData.year, month, this.weekStart);
                }
            }

            this.date = new Date(this.monthData.year, this.monthData.month, day, this.hour, this.minute, this.second, this.milliseconds).getTime()
            if (this.range) {
                this.rangeCounter++;
                if (this.rangeCounter === 2) {
                    this.rangeEndDate = this.date;
                    this.rangeEndDateChange.emit(this.rangeEndDate);
                    this.valueChange.emit(this.rangeEndDate);
                } else {
                    this.rangeStartDate = this.date;
                    this.rangeEndDate = null;
                    this.rangeStartDateChange.emit(this.rangeStartDate);
                    this.valueChange.emit(this.rangeStartDate);
                    this.rangeCounter = 1;
                }
            } else {
                this.selectedDate = this.date;
                this.valueChange.emit(this.selectedDate);
            }
            this.handleDays();

            // Close on select if not noClose
            if (!this.noClose && !this.range) {
                this.show = false;
                this.onShow.emit(false)
                this.onTouchedCallback();
            } else if (this.range && this.rangeCounter == 2 && !this.noClose) {
                this.show = false;
                this.onShow.emit(false)
                this.onTouchedCallback();
                this.rangeCounter = 0;
            }
        }
    }

    clearDates() {
        if (this.range) {
            this.rangeStartDate = 0;
            this.rangeEndDate = 0;
        } else {
            this.selectedDate = 0;
        }
    }

    updateMonthData(num: number, period: string) {
        switch (period) {
            case 'month':
                this.monthData.month += num;
                break;
            case 'year':
                this.monthData.year += num;
        }
        this.monthData = this.dateSvc.getMonthData(this.monthData.year, this.monthData.month, this.weekStart);
        this.handleDays();
    }

    private handleDays() {
        let todayDate = new Date(this.today);
        for (let i = 0; i < this.monthData.days.length; i++) {
            let day = this.monthData.days[i];
            day.weekend = this.isWeekend(day.date, this.monthData.month);
            day.today = day.date === todayDate.getDate() && this.monthData.month === todayDate.getMonth() && this.monthData.year === todayDate.getFullYear();
            day.selectable = !this.isDateDisabled(day.date, this.monthData.month);
            day.selected = this.isDateSelected(day.date, this.monthData.month, this.monthData.year);
            if (this.range) day.inRange = this.isInRange(day.date, this.monthData.month);
        }
        for (let i = 0; i < this.monthData.leadingDays.length; i++) {
            let day = this.monthData.leadingDays[i];
            day.weekend = this.isWeekend(day.date, this.monthData.month - 1);
            day.today = day.date === todayDate.getDate() && this.monthData.month - 1 === todayDate.getMonth() && this.monthData.year === todayDate.getFullYear();
            day.selectable = !this.isDateDisabled(day.date, this.monthData.month - 1);
            day.selected = this.isDateSelected(day.date, this.monthData.month - 1, this.monthData.year);
            if (this.range) day.inRange = this.isInRange(day.date, this.monthData.month - 1);
        }
        for (let i = 0; i < this.monthData.trailingDays.length; i++) {
            let day = this.monthData.trailingDays[i];
            day.weekend = this.isWeekend(day.date, this.monthData.month + 1);
            day.today = day.date === todayDate.getDate() && this.monthData.month + 1 === todayDate.getMonth() && this.monthData.year === todayDate.getFullYear();
            day.selectable = !this.isDateDisabled(day.date, this.monthData.month + 1);
            day.selected = this.isDateSelected(day.date, this.monthData.month + 1, this.monthData.year)
            if (this.range) day.inRange = this.isInRange(day.date, this.monthData.month + 1);
        }
    }

    private isDateSelected(day: number, month: number, year: number) {
        if (!this.selectedDate && !this.rangeStartDate && !this.rangeEndDate) return false;
        if (this.range) {
            const startDay = new Date(this.rangeStartDate).getDate();
            const endDay = new Date(this.rangeEndDate).getDate();
            const startMonth = new Date(this.rangeStartDate).getMonth();
            const endMonth = new Date(this.rangeEndDate).getMonth();
            const startYear = new Date(this.rangeStartDate).getFullYear();
            const endYear = new Date(this.rangeEndDate).getFullYear();
            return (day === startDay && month === startMonth && year == startYear) || (day === endDay && month === endMonth && year === endYear);
        } else {
            return day === this.day && month === this.month && year === this.year;
        }
    }

    private isDateDisabled(day: number, month?: number) {
        let date: number;
        if (month && month !== this.monthData.month) {
            if (month < 0) {
                date = new Date(this.monthData.year - 1, 11, day).getTime();
            } else if (month > 11) {
                date = new Date(this.monthData.year + 1, 0, day).getTime();
            } else {
                date = new Date(this.monthData.year, month, day).getTime();
            }
        } else {
            date = new Date(this.monthData.year, this.monthData.month, day).getTime();
        }
        if (this.minDate && (date < this.toDateOnly(this.minDate))) {
            return true;
        }
        if (this.maxDate && (date > this.toDateOnly(this.maxDate))) {
            return true;
        }
        if (this.disabledDates && this.disabledDates.indexOf(date) > -1) {
            return true
        }
        return false;
    }

    private isWeekend(dayIndex: number, month: number) {
        let weekday: number;
        if (month < 0) {
            weekday = new Date(this.monthData.year - 1, 11, dayIndex).getDay();
        } else if (month > 11) {
            weekday = new Date(this.monthData.year + 1, 0, dayIndex).getDay();
        } else {
            weekday = new Date(this.monthData.year, month, dayIndex).getDay();
        }
        return this.monthData.weekends.indexOf(weekday) > -1;
    }

    private isInRange(day: number, month: number) {
        const date = new Date(this.monthData.year, month, day).getTime();
        return date >= this.rangeStartDate && date <= this.rangeEndDate;
    }

    getDateOfDay(day: number, month?: number): number {
        return new Date(this.monthData.year, month || this.monthData.month, day).getTime();
    }

    toDateOnly(timestamp: number): number {
        return this.dateSvc.getDateStartOfDay(timestamp);
    }

    changeAmPm(isPm: boolean) {
        this.isPm = isPm;
        this.hour = this.hour;
    }

    validateHour() {
        const input = <HTMLInputElement>this.hourInput.nativeElement;
        const maxHour = this.is12h ? 12 : 23;
        const minHour = this.is12h ? 1 : 0;

        if (this.hour > maxHour) {
            this.hour = this.is12h ? 11 : maxHour;
        } else if (this.hour < minHour) {
            this.hour = this.is12h ? 12 : minHour;
        }
        input.value = `0${this.hour}`.slice(-2);
    }

    validateMinute() {
        const input = <HTMLInputElement>this.minuteInput.nativeElement;
        const maxMinute = 59;
        const minMinute = 0;

        if (this.minute > maxMinute) {
            this.minute = maxMinute;
        } else if (this.minute < minMinute) {
            this.minute = minMinute;
        }

        input.value = `0${this.minute}`.slice(-2);
    }

    private convertToAmPm(hour: number): number {
        if (hour < 12) {
            return hour === 0 ? 12 : hour;
        } else {
            return hour === 12 ? hour : hour - 12;
        }
    }

    private convertFromAmPm(hour: number): number {
        if (this.isPm) {
            return hour === 12 ? 12 : (hour + 12);
        } else {
            return hour === 12 ? 0 : hour;
        }
    }

    private getRandomId() {
        return Math.random().toString(36).slice(-8);
    }

    // Click to close
    public onClick(event: any) {
        if (this.show && !this.elRef.nativeElement.contains(event.target)) {
            this.show = false;
            this.onShow.emit(false);
            this.onTouchedCallback();
        }
    }

    // ngModel - ControlValueAccessor interface
    writeValue(value: any) {
        if (!this.range && value !== this.selectedDate) {
            this.selectedDate = value;
        }
    }

    registerOnChange(fn: any) {
        this.onChangeCallback = fn;
    }

    registerOnTouched(fn: any) {
        this.onTouchedCallback = fn;
    }
    //
}
