import { Component, Input, SimpleChange } from "@angular/core";

@Component({
    selector: "password-component",
    templateUrl: "password.component.html",
    styleUrls: ["password.component.css"]

})
export class PasswordComponent {
    @Input() password: string;

    public mypass = {
        six: false,
        num: false,
        both: false,
        sign: false,
        upper: false,
        lower: false
    };
    public SetClass = function (val: boolean) {
        if (val) {
            return "fa fa-check";
        }
        else {
            return "fa fa-times fa-green";
        }
    }
    public UpdateValidation = function (pass: string) {
        if (pass != null) {
            this.mypass.six = (pass.length >= 6 && pass.length <= 16);
            this.mypass.num = (/\d+/g.test(pass));
            this.mypass.lower = (/[a-z]/.test(pass));
            this.mypass.upper = (/[A-Z]/.test(pass));
            this.mypass.sign = (/[!@=?#$ %^&*()\-\_{\ }]/.test(pass));
        }
    }

    ngOnChanges(change: SimpleChange) {
        this.UpdateValidation((<any>change)["password"].currentValue)
    }

}