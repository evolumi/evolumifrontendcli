import { Component, Renderer } from "@angular/core";
import { ProductService, Product, Catalogue, ProductFilterParameters } from "../../../../services/products.service";
import { AccountsCollection } from "../../../../services/accounts.service";
import { OrgService } from "../../../../services/organisation.service";
import { TranslateService } from "@ngx-translate/core";
import { AccountsModel } from "../../../../models/account.models"
import { Subscription } from "rxjs/Subscription";
import { DealAddEdit } from "../../../../services/dealAddEdit.service";
import { NotificationService } from "../../../../services/notification-service";
import { OrganisationModel } from "../../../../models/organisation.models";

@Component({
    selector: "productpicker",
    styleUrls: ["productpicker.css"],
    templateUrl: "productpicker.html"
})
export class ProductPicker {

    public showModal = false;
    private displayProduct: Product;
    public displayCatalogue: Catalogue;
    public currentProductSum: number;
    public cart: Product[] = [];
    public cartSum: number = 0;
    public viewCart: boolean;
    private catalogues: Catalogue[] = [];
    private displayProductPrices: any[];
    public org: OrganisationModel;
    private unitsinput: HTMLElement;
    private cartClick: any;
    private pickerClick: any;
    private cartButtonClick: any;
    private searchFilters: ProductFilterParameters = new ProductFilterParameters();
    private noMoreProducts: boolean = false;
    public account: AccountsModel;

    private modalSub: Subscription;
    private orgSub: Subscription;
    private catSub: Subscription;

    constructor(public productSvc: ProductService,
        private accSvc: AccountsCollection,
        private orgSvc: OrgService,
        private renderer: Renderer,
        private translator: TranslateService,
        private _deal: DealAddEdit,
        private _notificationSvc: NotificationService
    ) {

        let allproductstrans = this.translator.instant("ALLPRODUCTS");
        this.catalogues.push(new Catalogue({
            Id: "ALL",
            Name: allproductstrans,
            Description: allproductstrans,
            Image: "/assets/images/barcode.png",
            IsActive: true,
            ProductsCount: 0
        }));
    }

    ngOnInit() {
        this.modalSub = this.productSvc.showProductPicker().subscribe(data => {
            this.showModal = data;
            if (data) {
                this.accSvc.getAccount(this.productSvc.productPickerDeal.AccountId, null, false, (acc) => {
                    this.account = acc;
                });
            }
        });

        this.catSub = this.productSvc.Catalogues().subscribe(data => {
            this.catalogues = this.catalogues.concat(data.map((catalogue: any) => catalogue = new Catalogue(catalogue)));
            this.catalogues[0].ProductsCount = this.productSvc.productsCount;
        });
        this.orgSub = this.orgSvc.currentOrganisationObservable().subscribe((data) => this.org = data);
    }

    updatePrice() {
        this.displayProduct.Sum = this.displayProduct.Units * this.displayProduct.Price;
    }

    getPrices() {
        let pricelistid = this.account.PriceList.Id;
        this.displayProductPrices = this.displayProduct.PriceLists.reduce(function (res: any, current: any) {
            res.push({
                Range: current.Range,
                Price: current.Prices[pricelistid]
            });
            return res;
        }, [])
    }

    displayProductUnitsChange(event: any) {
        if (!this.unitsinput) {
            this.unitsinput = document.getElementById("productPickerUnitsInput");
        }
        let num = event;
        if (Boolean(Number(num))) {
            num = Number(num);
            let max = this.displayProductPrices[this.displayProductPrices.length - 1].Range;
            if (this.displayProduct.PricesAsTotal && !this.displayProduct.PriceIsManual && num > max) {
                (this.unitsinput as HTMLInputElement).value = max;
                this.displayProduct.Units = max;
            }
            else {
                this.displayProduct.Units = num;
            }
        }
        this.setPrice()
    }
    setPrice() {
        let price = this.getPrice(this.displayProduct)
        if (!this.displayProduct.PricesAsTotal) {
            price = price * this.displayProduct.Units;
        }
        this.currentProductSum = price;
    }
    getPrice(prod: Product) {
        if (!prod.Units) {
            return 0;
        }
        if (prod.PriceIsManual) {
            return prod.Price;
        }
        let priceRange = this.displayProductPrices.find(x => x.Range >= prod.Units);
        if(!priceRange) priceRange = this.displayProductPrices.find(x => x.Range == -1);
        
        return priceRange.Price;
    }

    setDisplayCatalogue(catalogue: Catalogue) {
        this.displayCatalogue = catalogue;
        if (this.displayCatalogue.Products.length == 0 && this.displayCatalogue.ProductsCount > 0) {
            this.getProductsForCatalogue();
        }
    }

    setDisplayProduct(product: Product) {
        product.Units = 1;
        this.displayProduct = new Product(product);
        this.getPrices();
        this.displayProduct.Price = this.displayProduct.PriceIsManual ? 1 : this.getPrice(product) || 0;
        if (!this.unitsinput) {
            this.unitsinput = document.getElementById("productPickerUnitsInput");
        }
        if (this.unitsinput) {
            this.unitsinput.focus();
        }
        this.setPrice();
    }

    private searchTimeout: any;
    searchForProducts(event: any) {
        this.searchFilters.Name = event;
        if (this.searchTimeout) {
            clearTimeout(this.searchTimeout);
        }
        this.searchTimeout = setTimeout(() => {
            this.displayCatalogue.Products = [];
            this.getProductsForCatalogue(false)
        }, 2000);
    }

    getProductsForCatalogue(concat: boolean = true) {
        this.noMoreProducts = false;
        if (this.displayCatalogue.Products.length != this.displayCatalogue.ProductsCount) {
            let page: number = this.displayCatalogue.Products.length == 0 ? 0 : this.displayCatalogue.Products.length / this.productSvc.pageSize;
            if (this.displayCatalogue.Id != "ALL") {
                this.productSvc.GetProductsForCatalogue(this.displayCatalogue.Id, page, this.searchFilters).subscribe((data: any) => {
                    if (data.length == 0) {
                        this.noMoreProducts = true;
                    }
                    else {
                        if (concat) {
                            this.displayCatalogue.Products = this.displayCatalogue.Products.concat(data.map((product: any) => product = new Product(product)));
                        }
                        else {
                            this.displayCatalogue.Products = data.map((product: any) => product = new Product(product));
                        }
                    }
                });
            }
            else {
                this.productSvc.Products(page, this.searchFilters).subscribe((data: any) => {
                    if (data.length == 0) {
                        this.noMoreProducts = true;
                    }
                    else {
                        if (concat) {
                            this.displayCatalogue.Products = this.displayCatalogue.Products.concat(data.map((product: any) => product = new Product(product)));
                        }
                        else {
                            this.displayCatalogue.Products = data.map((product: any) => product = new Product(product));
                        }
                    }
                })
            }

        }
        else {
            this.noMoreProducts = true;
        }
    }

    browseCatalogues() {
        this.displayProduct = null;
        this.displayCatalogue = null;
    }

    addToCart() {
        if (this.displayProduct.Units != 0) {
            var prodCopy: Product = new Product(this.displayProduct);
            prodCopy.PriceIsManual = this.displayProduct.PriceIsManual;
            prodCopy.Price = this.getPrice(prodCopy);
            prodCopy.Sum = prodCopy.PricesAsTotal && !prodCopy.PriceIsManual ? prodCopy.Price : prodCopy.Price * prodCopy.Units;
            this.cartSum += prodCopy.Sum;
            this.cart.push(prodCopy);
            let msg = this.translator.instant("ADDEDTOCART", { Product: prodCopy.Name });
            this._notificationSvc.success(msg);
        }
    }

    removeFromCart(index: number) {
        this.cartSum -= this.cart[index].PricesAsTotal ? this.cart[index].Price : this.cart[index].Price * this.cart[index].Units;
        this.cart.splice(index, 1);
        if (this.cart.length == 0) {
            this.viewCart = false;
        }
    }
    addCartToDeal() {
        this.productSvc.addPickedProducts(this.cart);
        this.cart = [];
        this.cartSum = 0;
        this._deal.deal.CurrencyName = this.account.PriceList.Currency;
        this.productSvc.hideProductPickerModal();
    }
    closePicker() {
        this.productSvc.hideProductPickerModal();
    }
    ngOnDestroy() {
        if (this.orgSub) this.orgSub.unsubscribe();
        if (this.catSub) this.catSub.unsubscribe();
        this.pickerClick();
        this.cartClick()
        this.cartButtonClick();
        if (this.modalSub) this.modalSub.unsubscribe();
    }

    ngAfterViewInit() {
        let cartelem = document.getElementById("cartContainer");
        this.cartClick = this.renderer.listen(cartelem, "click", (event: any) => {
            event.stopPropagation();
        });
        let productpickerelem = document.getElementById("productpicker-modal");
        this.pickerClick = this.renderer.listen(productpickerelem, "click", (event: any) => {
            setTimeout(() => {
                if (this.viewCart) {
                    this.viewCart = false;
                }
            }, 0)
        });
        let cartbuttonelem = document.getElementById("cartButton");
        this.cartButtonClick = this.renderer.listen(cartbuttonelem, "click", (event: any) => {
            event.stopPropagation();
            this.viewCart = !this.viewCart;
        });
    }
}