import { Component, ViewChild } from '@angular/core';
import { CelebrationService } from "../../../../../services/celebration.service";
import { Subscription } from "rxjs/Subscription";

@Component({

    selector: 'fireworks',
    templateUrl: './fireworks.html',
    styleUrls: ['./fireworks.css']
})

export class FireworksComponent {

    private fireworkSub: Subscription;
    @ViewChild("fireworkcont", { read: HTMLDivElement }) fwCont: HTMLDivElement;

    public fireworksActive: boolean = false;

    constructor(
        private celebSvc: CelebrationService
    ) { }

    ngOnInit() {
        this.fireworkSub = this.celebSvc.fireworksObserable().subscribe(data => {
            this.startFirework(data);
        });
    }

    ngOnDestroy(){
        if (this.fireworkSub) this.fireworkSub.unsubscribe();
    }

    startFirework(start: boolean) {
        if (!start) {
            return;
        }
        this.fireworksActive = true;

        console.log("FIREWORK");
        setTimeout(() => {
            this.fwCont.style.borderColor = "green";
        }, 3000);

        // this.fireworksActive = false;
    }

}