import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { MoneyRain } from "./moneyrain/moneyrain";
import { Money } from "./moneyrain/money";
import { FireworksComponent } from "./fireworks/fireworks";

@NgModule({
    entryComponents: [
        Money
    ],
    imports: [
        CommonModule
    ],
    declarations: [
        MoneyRain,
        Money,
        FireworksComponent
    ],
    exports: [
        MoneyRain,
        Money,
        FireworksComponent
    ],

})
export class CelebrationModule {

}