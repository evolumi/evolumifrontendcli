import { Component, ElementRef, ViewChild } from "@angular/core";

@Component({

    selector: "money",
    template: `
        <i #icon class="fa fa-money rain" ></i>
    `,
    styleUrls: ["money.css"]
})

export class Money {

    @ViewChild("icon", { read: ElementRef }) icon: ElementRef;

    constructor() {

    }

    ngOnInit() {
        let screenwidth = window.innerWidth;
        let x = new Date().getTime();
        let margin = Math.round(Math.sin(x) * screenwidth);
        this.icon.nativeElement.style.left = `${margin}px`;
    }
}