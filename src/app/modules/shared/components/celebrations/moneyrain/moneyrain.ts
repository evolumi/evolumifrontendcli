import { Component, ViewContainerRef, ComponentFactoryResolver, ViewChild } from "@angular/core";
import { CelebrationService } from "../../../../../services/celebration.service";
import { Money } from "./money";
import { Subscription } from "rxjs/Subscription";

@Component({

    selector: "moneyrain",
    templateUrl: "moneyrain.html",
    styleUrls: ["moneyrain.css"]
})

export class MoneyRain {

    private rainSub: Subscription;
    @ViewChild("moneyraincontainer", { read: ViewContainerRef }) containerRef: ViewContainerRef;

    constructor(private celebSvc: CelebrationService, private factory: ComponentFactoryResolver) { }

    ngOnInit() {
        this.rainSub = this.celebSvc.moneyRainObserable().subscribe(data => {
            this.generateMoney();
        });
    }

    generateMoney() {
        console.log("making it rain");
        let timer = setInterval(() => {
            let money = this.factory.resolveComponentFactory(Money);
            let created = this.containerRef.createComponent(money);
            setTimeout(() => {
                created.destroy();
            }, 10000);
        }, 80);
        setTimeout(() => {
            clearInterval(timer);
            console.log("making it stop");
        }, 10000);
    }

    ngOnDestroy() {
        if (this.rainSub) this.rainSub.unsubscribe();
    }
}