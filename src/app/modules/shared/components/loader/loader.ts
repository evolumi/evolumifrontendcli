import { Component, ChangeDetectorRef } from '@angular/core';
import { LoaderService } from '../../../../services/loader.service';
import { Subscription } from "rxjs/Subscription";

@Component({
	selector: 'loader',
	template: `<div [hidden]="!show">
		<div class="loadingBar">
			<div class="barLoader" [style.width.%]="percentage"></div>
		</div>	
	</div>`,
	styles: [`
		.loadingBar {
		    position: fixed;
		    top: 0;
		    left: 0;
		    width: 100%;
		    z-index: 20000;
		}
		    .loadingBar .barLoader {
		        float: left;
		        width: 30%;
		        background: #1173cd;
		        height: 2px;
		        -webkit-transition: all 0.5s ease;
		        -moz-transition: all 0.5s ease;
		        -o-transition: all 0.5s ease;
		        transition: all 0.5s ease;
		        box-shadow: 0 0 15px 2px #1173cd;
		        -moz-box-shadow: 0 0 15px 2px #1173cd;
		        -webkit-box-shadow: 0 0 15px 2px #1173cd;
		        -o-box-shadow: 0 0 15px 2px #1173cd;
		    }
	`]
})
export class Loader {
	public show: boolean;
	public loderCount: any;
	public percentage: number;
	private loaderSub: Subscription;
	constructor(private _loader: LoaderService, private cdRef: ChangeDetectorRef) { }

	ngOnInit() {

	}

	ngAfterViewInit() {
		this.loaderSub = this._loader.loaderObservable().subscribe(latestCollection => {
			this.loderCount = latestCollection;
			this.show = true;
			this.cdRef.detectChanges();
			this.calpercentage();
		});
	}

	ngOnDestroy() {
		if (this.loaderSub) this.loaderSub.unsubscribe();
	}

	calpercentage() {
		this.percentage = (this.loderCount.completed / this.loderCount.called) * 100;
		this.show = false;
		this.cdRef.detectChanges();
		// if (this.percentage === 100) {
		// 	setTimeout(() => {
		// 		this.show = false;
		// 	}, 1000);
		// }
	}
}
