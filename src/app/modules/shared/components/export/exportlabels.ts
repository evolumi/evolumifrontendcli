export const AccountLabels: { [key: string]: any } = {
    'ShownName': 'ACCDISPLAYNAME',
    'CompanyNumber': 'ACCCOMPANYID',
    'AccountType': 'ACCTYPE',
    'AccountOwner': 'ACCMAN',
    'AccountData': 'ACCOUNTINFO',
    'AccountInfo': 'CONTACTINFO',
    'AccountRelation': 'CONTACTS',
    'AccountFinancial': 'FINANCIALSTATMENT',
}

export const ActionLabels: { [key: string]: any } = {

}