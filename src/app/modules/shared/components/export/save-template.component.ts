import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { ExportService } from "../../../../services/export.service";
import { ExportModel } from "../../../../models/export.models";

@Component({
    selector: 'export-template-save',
    templateUrl: './save-template.component.html'
})

export class TemplateSaveComponent implements OnInit {

    @Input() exportTemplate: ExportModel;
    @Input() type: string;
    @Output() close = new EventEmitter<boolean>();

    public isDefaultTemplate: boolean;

    constructor(private exportSvc: ExportService) { }

    ngOnInit() {
        this.isDefaultTemplate = this.exportTemplate.Id === 'Full' || this.exportTemplate.Id === 'Empty'
    }

    save(isNew: boolean) {
        if (isNew) {
            this.exportSvc.addTemplate(this.exportTemplate, this.type, this.back.bind(this));
        } else {
            this.exportSvc.updateTemplate(this.exportTemplate, this.type, this.back.bind(this));
        }
    }

    clickToClose(event: any) {
        let target = event.target || event.srcElement;
        if (target.id === 'saveTemplate') {
            this.back();
        }
    }

    back() {
        this.close.emit(false);
    }
}