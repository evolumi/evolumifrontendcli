import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { AccountsCollection } from "../../../../services/accounts.service";
import { ChosenOptionModel } from "../../../shared/components/chosen/chosen.models";
import { TranslateService } from "@ngx-translate/core";
import { ExportModel, ExportPersonAccountModel, ExportCompanyAccountModel, ExportActionModel, PropertyHolder } from "../../../../models/export.models";
import { ExportService } from "../../../../services/export.service";
import { Subscription } from "rxjs/Subscription";
import { OrgService } from "../../../../services/organisation.service";
import { ActionsCollection } from "../../../../services/actionservice";
import { OrganisationModel } from "../../../../models/organisation.models";
import { ActionLabels, AccountLabels } from "./exportlabels";

// The translation of the properties where the label doesn't correspond
// is loaded from the exportlabels.ts file
@Component({
    moduleId: module.id,
    selector: 'export',
    templateUrl: 'export.component.html',
    styleUrls: ['export.component.css']
})
export class ExportComponent implements OnInit {

    @Input() type: string;
    @Input() extra: string;
    @Output() close = new EventEmitter();

    public count: number = 0;
    public anyChecked: boolean;
    public showModal: boolean;
    public processing: boolean;

    public exportModel: ExportModel;
    public modelProps: PropertyHolder = { MainProps: [] };
    public org: OrganisationModel;
    public fullExportModel: any;
    public emptyExportModel: any;

    public templates: Array<any> = [];
    public templatesChosen: Array<ChosenOptionModel> = [];

    public labels: { [key: string]: any } = {};

    public orgSub: Subscription;
    public exportSub: Subscription;

    constructor(private accSvc: AccountsCollection,
        private translate: TranslateService,
        private orgSvc: OrgService,
        private exportSvc: ExportService,
        private actionSvc: ActionsCollection) { }

    // Lite customs för person/company
    ngOnInit() {
        this.fullExportModel = Object.assign({}, this.exportSvc.getFullExportModel(this.type));
        this.getEmptyExportModel();
        this.exportSvc.getTemplates(this.type);
        this.exportSub = this.exportSvc.exportTemplatesObs()
            .subscribe((res: any) => {
                if (res) {
                    this.templatesChosen = [];
                    this.templatesChosen.push(new ChosenOptionModel('Empty', this.translate.instant('EMPTY')));
                    this.templatesChosen.push(new ChosenOptionModel('Full', this.translate.instant('FULLEXPORT')));
                    this.templatesChosen.push(...res.map((x: any) => new ChosenOptionModel(x.Id, x.Name)));
                    this.templates = res;
                }
            });

        this.changeTemplate('Full');
        this.buildHtml();

        this.orgSub = this.orgSvc.currentOrganisationObservable()
            .subscribe(org => {
                this.org = org;

                this.getCustomFields();
            });

        this.getCount();
        this.getLabels()
    }

    ngOnDestroy() {
        if (this.orgSub) this.orgSub.unsubscribe();
    }

    export() {
        this.checkForInExport();
        this.processing = true;
        switch (this.type) {
            case 'Action':
                this.actionSvc.exportNew(this.exportModel, this.extra, this.endLoading.bind(this), this.endLoading.bind(this));
                break;
            case 'Company':
                this.accSvc.exportAccountsNew(this.exportModel, this.type, this.endLoading.bind(this), this.endLoading.bind(this));
                break;
            case 'Person':
                this.accSvc.exportAccountsNew(this.exportModel, this.type, this.endLoading.bind(this), this.endLoading.bind(this));
                break;
        }
    }

    private getEmptyExportModel() {
        switch (this.type) {
            case 'Action':
                this.emptyExportModel = new ExportActionModel();
                break;
            case 'Company':
                this.emptyExportModel = new ExportCompanyAccountModel();
                break;
            case 'Person':
                this.emptyExportModel = new ExportPersonAccountModel();
                break;
        }
    }

    private getCustomFields() {
        switch (this.type) {
            case 'Action':
                if (!this.modelProps.ActionPerson) this.modelProps.ActionPerson = [];
                this.modelProps.ActionPerson.push(...this.org.PersonCustomFields.map(x => { this.fullExportModel.ActionPerson.CustomFields[x.Id] = true; return { key: x.Id, value: x.Name } }));
                break;
            case 'Company':
                if (!this.modelProps.AccountCustomFields) this.modelProps.AccountCustomFields = [];
                if (!this.modelProps.AccountRelation) this.modelProps.AccountRelation = [];
                this.modelProps.AccountCustomFields.push(...this.org.AccountCustomFields.map(x => { this.fullExportModel.AccountCustomFields.CustomFields[x.Id] = true; return { key: x.Id, value: x.Name } }));
                this.modelProps.AccountRelation.push(...this.org.PersonCustomFields.map(x => { this.fullExportModel.AccountRelation.CustomFields[x.Id] = true; return { key: x.Id, value: x.Name } }));
                break;
            case 'Person':
                if (!this.modelProps.AccountCustomFields) this.modelProps.AccountCustomFields = [];
                if (!this.modelProps.AccountRelation) this.modelProps.AccountRelation = [];
                this.modelProps.AccountCustomFields.push(...this.org.PersonCustomFields.map(x => { this.fullExportModel.AccountCustomFields.CustomFields[x.Id] = true; return { key: x.Id, value: x.Name } }));
                this.modelProps.AccountRelation.push(...this.org.PersonCustomFields.map(x => { this.fullExportModel.AccountRelation.CustomFields[x.Id] = true; return { key: x.Id, value: x.Name } }));
                break;
        }
    }

    private getCount() {
        switch (this.type) {
            case 'Action':
                this.actionSvc.getCount(this.extra)
                    .subscribe(x => this.count = x.Data);
                break;
            case 'Company':
            case 'Person':
                if (this.accSvc.selectedAccounts.length) {
                    this.count = this.accSvc.selectedAccounts.length;
                } else {
                    this.accSvc.getCount()
                        .subscribe(x => this.count = x.Data);
                }
                break;
        }
    }

    private buildHtml() {
        for (let prop in this.exportModel) {
            if (this.exportModel.hasOwnProperty(prop) && prop !== 'Id') {
                this.modelProps.MainProps.push(prop);
                for (let innerProp in this.exportModel[prop]) {
                    if (innerProp !== 'InExport' && innerProp !== 'CustomFields') {
                        if (!this.modelProps[prop]) this.modelProps[prop] = [];
                        this.modelProps[prop].push(innerProp);
                    }
                }
            }
        }
    }

    private checkForInExport() {
        for (let prop in this.exportModel) {
            if (this.exportModel.hasOwnProperty(prop)) {
                if (this.exportModel[prop] && !this.exportModel[prop].InExport) {
                    for (let innerProp in this.exportModel[prop]) {
                        if (innerProp !== 'InExport' && innerProp !== 'CustomFields' && this.exportModel[prop][innerProp] === true) {
                            this.exportModel[prop].InExport = true;
                            break;
                        } else if (innerProp === 'CustomFields') {
                            for (let key in this.exportModel[prop][innerProp]) {
                                if (this.exportModel[prop][innerProp][key] === true) {
                                    this.exportModel[prop].InExport = true;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private getLabels(){
        switch (this.type){
            case 'Action':
                this.labels = ActionLabels;
                break;
            case 'Company':
            case 'Person':
                this.labels = AccountLabels;
                break;
        }
    }

    changeTemplate(id: string) {
        if (id === 'Full') {
            this.exportModel = JSON.parse(JSON.stringify(this.fullExportModel));
            this.exportModel.Id = 'Full';
        } else if (id === 'Empty') {
            this.exportModel = JSON.parse(JSON.stringify(this.emptyExportModel));
            this.exportModel.Id = 'Empty';
        } else {
            this.exportModel = Object.assign({}, this.templates.find(x => x.Id == id));
        }
        this.anyChecked = this.validateChecked();
    }

    propChecked(prop: string, innerProp: string, checked: boolean) {
        this.exportModel[prop][innerProp] = checked;
        this.anyChecked = this.validateChecked();
    }

    customPropChecked(prop: string, key: string, checked: boolean) {
        this.exportModel[prop]['CustomFields'][key] = checked;
        this.anyChecked = this.validateChecked();
    }

    validateChecked(): boolean {
        let any = false;
        for (let prop in this.exportModel) {
            if (this.exportModel.hasOwnProperty(prop)) {
                for (let innerProp in this.exportModel[prop]) {
                    if (innerProp !== 'InExport' && innerProp !== 'CustomFields' && this.exportModel[prop][innerProp] === true) {
                        return true;
                    } else if (innerProp === 'CustomFields') {
                        for (let key in this.exportModel[prop][innerProp]) {
                            if (this.exportModel[prop][innerProp][key] === true) {
                                return true;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    private endLoading() {
        this.processing = false;
    }

    clickToClose(event: any) {
        let target = event.target || event.srcElement;
        if (target.id === 'export') {
            this.back();
        }
    }

    public back() {
        if (this.processing) this.exportSvc.aborted = true;
        this.close.emit();
    }
}