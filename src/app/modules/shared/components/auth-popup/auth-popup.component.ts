import { Component } from '@angular/core';
import { CalendarService } from '../../../../services/calendar.service';

@Component({
    selector: 'auth-popup',
    templateUrl: './auth-popup.html',
    styleUrls: ['./auth-popup.css']
})

export class AuthPopupComponent {
    constructor(public calendarSvc: CalendarService) { }

    sendAuthRequest() {
        console.log(this.calendarSvc.authModel);
    }
    close() {
        this.calendarSvc.showAuthModal = false;
    }
    closeModal(event: any) {
        if (!event || event.target.id == "authModal") {
            this.close();
        }
    }
}