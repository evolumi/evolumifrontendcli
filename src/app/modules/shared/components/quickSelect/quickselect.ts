import { Component, Input, Output, EventEmitter, ElementRef } from "@angular/core";


@Component({
    selector: "quickselect",
    templateUrl: "./quickselect.html",
    styleUrls: ["./quickselect.css"],
    host: {
        '(document:click)': 'onClick($event)',
    },
})

export class QuickSelect {

    @Input() items: any[];
    @Input() width: string;
    @Input() displayProperty: string;
    @Output() select = new EventEmitter()
    public showQuickSelectBox: boolean;

    constructor(private elem: ElementRef) {

    }

    toggleQuickSelectBox() {
        event.stopPropagation();
        this.showQuickSelectBox = !this.showQuickSelectBox;
    }

    itemSelected(event: any, item: any) {
        event.stopPropagation();
        this.select.emit(item);
        this.showQuickSelectBox = false;
    }

    onClick(event: any) {
        if (this.showQuickSelectBox) {
            let target = event.target || event.srcElement;
            if (!this.elem.nativeElement.contains(target)) {
                this.toggleQuickSelectBox();
            }
        }
    }
}