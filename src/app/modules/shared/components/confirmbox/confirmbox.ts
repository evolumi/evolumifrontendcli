import { Component } from "@angular/core";
import { ConfirmationService } from "../../../../services/confirmation.service";

@Component({

    selector: "confirmPopup",
    styleUrls: ["confirmbox.css"],
    templateUrl: "confirmbox.html"
})
export class ConfirmBox {

    constructor(public confirmationService: ConfirmationService) {

    }

    cancelClicked() {
        this.confirmationService.hideModal(false);
    }

    confirmClicked() {
        this.confirmationService.hideModal(true);
    }
    clickToClose(event: any) {
        var target = event.target || event.srcElement;
        if (target.id === 'confirmbox') {
            this.cancelClicked();
        }
    }
}

// @Directive({
//     selector: "[confirmbox]"
// })
// export class ConfirmDirective {
//     @Input() confirmTitle: string;
//     @Input() confirmText: string;
//     @Input() confirmClick: any;
//     private confirmBox: any;
//     private test: ComponentRef<ConfirmBox>;

//     @Output() answer = new EventEmitter();

//     constructor(private elem: ElementRef, private viewContainerRef: ViewContainerRef, private componentFactoryResolver: ComponentFactoryResolver) {
//         // let factory = this.componentFactoryResolver.resolveComponentFactory(ConfirmBox);
//         // this.confirmBox.createComponent(factory);
//         // console.log(this.confirmBox);
//     }

//     addConfirmation() {
//         this.test = this.createDialog(ConfirmBox);
//         this.elem.nativeElement.onclick = function (this: ConfirmDirective) {
//            this.test.instance.showModal(this.confirmTitle,this.confirmText);
//        }
//     }

//     createDialog(dialogComponent: { new(): ConfirmBox }): ComponentRef<ConfirmBox> {
//         this.viewContainerRef.clear();

//         let dialogComponentFactory = this.componentFactoryResolver.resolveComponentFactory(dialogComponent);
//         let dialogComponentRef = this.viewContainerRef.createComponent(dialogComponentFactory);

//         return dialogComponentRef;
//     }

//     ngOnInit(){
//         this.addConfirmation();
//     }
//}