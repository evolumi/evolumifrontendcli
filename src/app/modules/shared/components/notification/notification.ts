import { Component } from '@angular/core';
import { NotificationService } from '../../../../services/notification-service';
import { Subscription } from "rxjs/Subscription";

@Component({
  selector: 'notification',
  template: ` 
  <div class="notificationDiv notiActive">
    <div *ngFor="let elem of myArray" class="notificationbar notiActive {{elem.class}}" (click)="closeNotification(elem)">
    <i class="close link fa fa-times"></i>
      {{elem.msg}}
    </div>
  </div>
`
})
export class Notification {
  public myArray: string[] = [];

  private noteSub: Subscription;

  constructor(private _notify: NotificationService) { }
  ngOnInit() {
    this.noteSub = this._notify.collection$().subscribe(latestCollection => {
      this.myArray = latestCollection;
    });
    this._notify.load();
  }

  ngOnDestroy(){
    if (this.noteSub) this.noteSub.unsubscribe();
  }

  closeNotification(value: string) {
    this.myArray.splice(this.myArray.indexOf(value), 1);
  }
}
