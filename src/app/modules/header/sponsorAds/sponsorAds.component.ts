import { Component, Inject } from '@angular/core';
import { Api } from '../../../services/api';
import { LocalStorage } from '../../../services/localstorage';
import { IntercomService } from '../../../services/interecomService';
import { OrgService, Features } from '../../../services/organisation.service';
declare var $: JQueryStatic;

@Component({
    selector: 'sponsor-ads',
    template: '  <div *ngIf="overview==true && showBox ==true && showAd == true"  class="adBox"><a href="{{getLink(link)}}" (click)="clickOnLink();" target="_blank"><img src="{{ad}}"/></a></div>',
    styles: [`
        .adBox {
            background: rgba(255, 162, 0, 0.71);
            height: 50px;
            position: fixed;
            right: 210px;
            top: 0;
            width: 300px;
            z-index: 101;
            overflow: hidden;
        }
        
        @media screen and (max-width: 991px) {
            .adBox {
                top: 0px;
                right: 230px;
            }
        }
        
        @media screen and (max-width: 767px) {
            .adBox {
                left: 50%;
                margin: 0 auto 0 -141px;
                overflow: hidden;
                top: 50px;
                z-index: 101;
            }
        }
    `]
})

export class SponsorAds {
    public sponsor: any;
    public ad: string;
    public link: string;
    public showBox: boolean = false;
    public overview: boolean = true;
    private _agendaLoading: boolean = false;
    private _timerId: any = null;
    public addApiUrl: string;
    public showAd: boolean = false;
    private orgSub: any;


    constructor(
        @Inject(IntercomService) public intercomService: IntercomService,
        public http: Api,
        public localStorage: LocalStorage,
        private orgService: OrgService
    ) {

    }

    ngOnInit() {
        this.orgSub = this.orgService.currentOrganisationObservable().subscribe(res => {
            if (res) {
                this.showAd = this.orgService.hasFeature(Features.Branding);
                console.log("[Sponsor]" + (this.showAd ? "Showing Add" : "Hiding Add"));
            }
        });

        this.getAgenda();

        if (this._timerId == null) {
            this._timerId = setInterval(() => {
                var day = new Date().getDay();
                if (this.sponsor == null || this.sponsor == undefined || new Date(this.sponsor.Date).getDay() != day) {
                    this.getAgenda();
                }
            }, 600000); // check every 10 min
        }

    }

    clickOnLink() {
        this.intercomService.trackEvent('click-on-add');
    }

    getLink(link: string): string {
        var orgId = this.localStorage.get("orgId");
        var userId = this.localStorage.get("userId");
        this.addApiUrl = `${this.http.apiUrl}Organisations/${orgId}/Sponsors/Redirect?userId=${userId}&url=`;
        return this.addApiUrl + link;
    }

    getAgenda(): void {
        if (!this._agendaLoading) {
            this._agendaLoading = true;
            this.http.get('Sponsors/TodayAgenda')
                .map(res => res.json())
                .subscribe(res => {
                    if (res.Data) {
                        if (res.Data === 'Agenda not found for Today') {
                            this.showBox = false;
                            this.link = '';
                            this.ad = '';
                        } else {
                            this.showBox = true;
                            this.sponsor = res.Data;
                            this.link = res.Data.Link;
                            this.ad = res.Data.AdImage;
                            $('body').addClass('hasAd');
                        }

                        this._agendaLoading = false;
                    }
                }, (err) => {
                    this.link = '';
                    this.ad = '';

                    this._agendaLoading = false;
                });
        }
    }

    ngOnDestroy() {
        if (this.orgSub) this.orgSub.unsubscribe();
        clearInterval(this._timerId);
    }
}

