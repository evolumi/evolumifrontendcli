﻿import { Component } from '@angular/core';
import { AuthService } from '../../../services/auth-service';
import { TranslateService } from '@ngx-translate/core';
import { LocalStorage } from '../../../services/localstorage';
import { Api } from '../../../services/api';
import { LogedInUserModel } from '../../../models/user.models';
import { NotificationService } from '../../../services/notification-service';
import { ChosenOption } from "../../shared/components/chosen/chosen";
import { ChosenOptionModel } from '../../shared/components/chosen/chosen.models';

declare var $: JQueryStatic;

@Component({

    selector: 'header-white',
    templateUrl: './headerwhite.html',
    styleUrls: ['../header.css']
})

export class HeaderWhite {
    public currentUser: LogedInUserModel = new LogedInUserModel({});;
    public Name: string;
    public timestamp: any;

    public show: boolean = false;
    private userSub: any;
    public isEvolumiAdmin: boolean = false;

    public links: Array<any> = [];

    public languages: Array<ChosenOption> = [
        new ChosenOptionModel("en", "English"),
        new ChosenOptionModel("sv-se", "Svenska"),
        //new ChosenOptionModel("nl", "Nederlands"),
        new ChosenOptionModel("de", "Deutsch"),
        //new ChosenOptionModel("ja", "日本語")
    ];
    public selectedLanguage: string = 'en';
    constructor(
        private auth: AuthService,
        public _store: LocalStorage,
        public _translate: TranslateService,
        public _http: Api,
        public _notify: NotificationService) {
    }

    ngOnInit() {

        this.userSub = this.auth.currentUserObservable().subscribe(user => {
            if (user) {
                this.currentUser = user;
                this.selectedLanguage = user.Language;
                this.Name = this.currentUser.FirstName;
                this.isEvolumiAdmin = this.auth.evolumiAdminType == "SuperAdmin";
                this.buildLinks();
            }
        });
    }

    buildLinks() {
        this.links = [];
        this.links.push(
            { path: "/Admin", text: "ADMIN", id: "AdminLink", showing: this.isEvolumiAdmin },
        );
    }

    showSettingMenu() {
        this.show = !this.show;
    }

    hideSettingMenu(event: any) {
        event = event.target.id;
        if (event !== 'headerWhiteImage') {
            this.show = false;
        }
    }

    showLanguageMenu() {
        this.show = false;
        $("#language1").addClass("systemActive");
    }

    closeLanguageMenu(event: any) {
        if (event && event.target.id === 'language1' || event.target.id === "close") {
            $("#language1").removeClass("systemActive");
        } else if (!event && event.target.id === 'language1') {
            $("#language1").removeClass("systemActive");
        } else {
            return;
        }
    }

    changeLanguage(selectedLanguage: string) {
        this.auth.setLanguage(selectedLanguage);

        $("#language1").removeClass("systemActive");
        this._http.post('Users/Language/' + selectedLanguage, {})
            .map(res => res.json())
            .subscribe(res => {
                this._store.set('lan', selectedLanguage);
                console.log("[Language] Update: Success!");
                this._notify.success(this._translate.instant("LANGUAGETRANSLATEDTO"));
            });
    }

    openSideMenu() {
        $(".primaryMenu, #sidemenuPopUpHeader").addClass("systemActive");
    }

    closeSideMenu() {
        $(".primaryMenu, #sidemenuPopUpHeader").removeClass("systemActive");
    }

    openHelp() {
        window.open('http://evolumi.helpscoutdocs.com', '_blank');
    }

    logOff() {
        this.auth.Logout();
    }

    ngOnDestroy() {
        if (this.userSub) this.userSub.unsubscribe();
    }
}