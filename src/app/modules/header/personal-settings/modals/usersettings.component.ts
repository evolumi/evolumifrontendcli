import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Api } from '../../../../services/api';
import { NotificationService } from '../../../../services/notification-service';
import { ValidationService } from '../../../../services/validation.service';
import { AuthService } from '../../../../services/auth-service';
import { SMTPSettingsModel } from '../../../../models/smtp-settings.models';
import { CropModal } from "../../../shared/components/cropmodal/cropmodal";
import { CropperSettings } from "ng2-img-cropper";
declare var $: JQueryStatic;

@Component({

    selector: 'user-settings',
    templateUrl: './usersettings.html',
})

export class UserSettingsComponent {

    public resetPass: any;
    public userBasic: FormGroup;
    public pass: any;
    public resetpass: FormGroup;
    public displayModal: boolean;
    public user: any = {
        FirstName: '',
        LastName: '',
        Email: '',
        IsWeekStartFromSunday: true,
        Is24DateTimeFormat: true
    };

    @ViewChild('cropmodal') cropmodal: CropModal;
    public data: any;
    public image: any;
    public cropping: boolean;
    public croppersettings: CropperSettings;

    public smtpSettings: SMTPSettingsModel = new SMTPSettingsModel(null);

    private userSub: any;

    constructor(
        private _api: Api,
        _form: FormBuilder,
        private _notify: NotificationService,
        private auth: AuthService,
        public translate: TranslateService
    ) {
        this.resetPass = _form.group({
            OldPassword: ['', Validators.required],
            NewPassword: ['', Validators.compose([Validators.required, ValidationService.passwordValidator])],
            ConfirmPassword: ['', Validators.compose([Validators.required, ValidationService.passwordValidator])]
        });

        this.userBasic = _form.group({
            FirstName: [this.user.FirstName, Validators.required],
            LastName: [this.user.LastName, Validators.required],
            Email: [this.user.Email, Validators.compose([Validators.required, ValidationService.emailValidator])]
        });
        this.croppersettings = new CropperSettings()
        this.croppersettings.width = 100;
        this.croppersettings.height = 100;
        this.croppersettings.croppedWidth = 50;
        this.croppersettings.croppedHeight = 50;
        this.croppersettings.canvasWidth = 200;
        this.croppersettings.canvasHeight = 200;
        this.croppersettings.noFileInput = true;
    }

    ngOnInit() {
        this.userSub = this.auth.currentUserObservable().subscribe(response => {
            if (response) {
                this.user = response;

                this.smtpSettings = new SMTPSettingsModel({
                    Sender: response.Sender,
                    Url: response.Url,
                    Host: response.Host,
                    Port: response.Port,
                    EnableSSL: response.EnableSSL,
                    UserName: response.UserName,
                    Password: response.Password,
                    Signature: response.Signature
                });
            }
        });
    }

    updateUser() {
        var user = {
            "UserId": this.user.Id,
            "FirstName": this.user.FirstName,
            "LastName": this.user.LastName,
            "Email": this.user.Email,
            "IsWeekStartFromSunday": this.user.IsWeekStartFromSunday,
            "Is24DateTimeFormat": this.user.Is24DateTimeFormat
        };
        this._api.requestPut(
            'Users/NameChange',
            user,
            (res) => {
                this._notify.success(this.translate.instant('USERDATAUPDATED'));
                console.log("[User] Update User data: Success! -> " + user.FirstName + " " + user.LastName);
                this.auth.GetCurrentUser();
                this.hideModal();
            },
            (error) => {
                this._notify.error(this.translate.instant('UPDATEFAILED'));
                console.log("[User] Update User data: Fail! -> " + error);
            });
    }

    public upload($event: any): void {
        let formData: FormData = new FormData();
        formData.append('file', (<any>$('#file-input')[0]).files[0]);
        this.user.ProfilePictureUrl = "assets/images/preloader.gif";
        this._api.upload('Users/Upload/ProfilePicture', formData)
            .then(res => {
                console.log("[User] Update profile picture: Success!");
                this.auth.GetCurrentUser();
            });

        this.data = $event.srcElement.files;
        this.cropmodal.imgCropper.fileChangeListener($event);
        this.cropping = true;
    }

    public cropped($event: any) {

        this.image = $event.image;
        this._api.post('Users/Upload/ProfilePictureSmall', this.image.src.toString()).map(res => res.json())
            .subscribe(res => {
                console.log("[User] Cropped profile pic uploaded");
                this.auth.GetCurrentUser();
                console.log(res);
                this.cropping = false;
            });
    }


    public updatePass() {
        if (this.resetPass.value.NewPassword !== this.resetPass.value.ConfirmPassword) {
            this._notify.error(this.translate.instant('PASSWORDDOESNTMATCH'));
            return false;
        }
        if (this.resetPass.dirty && this.resetPass.valid) {
            if (this.pass !== this.resetpass) {
                this._notify.error(this.translate.instant('PASSWORDDOESNTMATCH'));
                return false;
            }
            this._api.requestPost(
                'Users/ChangePassword',
                this.resetPass.value,
                (res) => {
                    this._notify.success(this.translate.instant('PASSWORDUPDATED'));
                    console.log("[User] Password update: Success!");
                    for (var i in this.resetPass.controls) {
                        this.resetPass.controls[i].updateValue('');
                        this.resetPass.controls[i].setErrors(null);
                    }
                    this.hideModal();
                },
                (err) => {
                    this._notify.error(this.translate.instant('ERROR'));
                    console.log("[User] Password update: Fail! -> Unknown Error");
                });
        }
        return true;
    }

    public showModal(): void {
        this.displayModal = true;
    }

    public hideModal(): void {
        this.displayModal = false;
    }

    ngOnDestroy() {
        if (this.userSub) this.userSub.unsubscribe();
    }
}