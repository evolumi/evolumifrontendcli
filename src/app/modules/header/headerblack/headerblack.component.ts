﻿import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../../services/auth-service';
import { LocalStorage } from '../../../services/localstorage';
import { OrgService, Permissions, Features } from '../../../services/organisation.service';
import { LogedInUserModel } from '../../../models/user.models';
import { ChosenOption } from "../../shared/components/chosen/chosen";
import { ChosenOptionModel } from '../../shared/components/chosen/chosen.models';
import { PermissionService } from '../../../services/permissionService';
import { Subscription } from 'rxjs/Subscription';
import { ButtonPanelService } from '../../../services/button-panel.service';
import { Shortcuts } from '../../../services/configuration';
import { Api } from "../../../services/api";
import { NotificationService } from "../../../services/notification-service";
import { TranslateService } from "@ngx-translate/core";

@Component({
    selector: 'header-black',
    styleUrls: ['../header.css'],
    templateUrl: './headerblack.html',
})

export class HeaderBlack {
    public currentUser: LogedInUserModel = new LogedInUserModel({});;
    public Name: string;
    public orgName: string;
    public logoUrl: string = "assets/images/evolumi.png";
    public timestamp: any;
    public show: boolean = false;
    public userMenu: boolean = false;

    public isGuest: boolean = false;
    public isEvolumi: boolean = false;

    private userSub: Subscription;
    private orgSub: Subscription;
    private buttonSub: Subscription;

    public languageActive: boolean;
    public menuActive: boolean;

    private userIsPlus: boolean;

    public links: { CRM: Array<any>, Sales: Array<any> } = { CRM: [], Sales: [] };
    public selectedMenu = "CRM";
    //private navMenus = ["CRM", "Sales"]; 
    public navMenus = ["CRM"];

    public languages: Array<ChosenOption> = [
        new ChosenOptionModel("en", "English"),
        new ChosenOptionModel("sv-se", "Svenska"),
        //new ChosenOptionModel("nl", "Nederlands"),
        new ChosenOptionModel("de", "Deutsch"),
        //new ChosenOptionModel("ja", "日本語")
    ];
    public selectedLanguage: string = 'en';
    constructor(private _http: Api,
        private _notify: NotificationService,
        private _translate: TranslateService,
        private auth: AuthService,
        private router: Router,
        public permissions: PermissionService,
        public _store: LocalStorage,
        public orgService: OrgService,
        private button: ButtonPanelService
    ) {

    }
    ngOnInit() {
        if (this._store.get('lan')) {
            this.selectedLanguage = this._store.get('lan');
        }
        this.orgSub = this.orgService.currentOrganisationObservable().subscribe(currentOrg => {
            if (currentOrg) {
                if (currentOrg.HasImage && this.orgService.hasFeature(Features.Branding)) {
                    this.logoUrl = currentOrg.ImageUrl;
                }
                else {
                    this.logoUrl = "/assets/images/evolumi.png";
                }
                this.orgName = currentOrg.OrganisationName;
                this.buildLinks();
                console.log(currentOrg);
                this.userIsPlus = currentOrg.IsPlus;
            }
        });

        this.userSub = this.auth.currentUserObservable().subscribe(user => {
            if (user) {
                this.currentUser = user;
                this.selectedLanguage = user.Language;
                console.log("[Language] Setting ->", this.selectedLanguage);
                this.Name = this.currentUser.FirstName;
                this.isEvolumi = this.auth.isEvolumi;
            }
        });

        if (this.orgService.hasFeature(Features.CalendarSync)) {
            this.button.addHeaderButton("CALENDAR", "calendar", "calendar-o");
        }

        this.button.addHeaderButton("CHAT", "chat", "comments", Shortcuts.chat);
        this.buttonSub = this.button.commandObservable().subscribe(command => {
            if (command == 'chat') this.router.navigate(["CRM/Chat"]);
            if (command == "calendar") { this.router.navigate(['CRM', 'Calendar']) }
        });
    }

    buildLinks() {
        this.links.CRM = [];
        if (this.permissions.isGuest) {

        }
        else {
            this.links.CRM.push(
                { path: "/CRM/Dashboard", icon: "tachometer", text: "DASHBOARD", id: "DashboardLink" },
                { path: "/CRM/Accounts", icon: "industry", text: "COMPANIES", id: "CompaniesLink" },
                { path: "/CRM/PersonAccounts", icon: "user", text: "PERSONACCOUNTS", id: "PersonAccountsLink" },
                { path: "/CRM/Actions", icon: "check-square-o", text: "ACTIONS", id: "ActionsLink" },
                { path: "/CRM/Deals", icon: "money", text: "DEALS", id: "DealsLink" },
                { path: "/CRM/SalesCompetition", icon: "trophy", text: "SALESCOMPETITION", id: "SalesComplLink", features: [Features.SalesCompetition] },
                { path: "/CRM/Insights", icon: "bar-chart", text: "INSIGHTS", id: "InsightsLink" },
                { path: "/CRM/Contracts", icon: "file-text-o", text: "CONTRACTS", id: "ContractsLink", features: [Features.Contracts], permissions: [Permissions.EvolumiAdmin] },
                { path: "/CRM/Packages", icon: "plus-circle", text: "PACKAGES", id: "PackageLink" },
                { path: "/CRM/Newsletters", icon: "envelope", text: "NEWSLETTERS", id: "NewslettersLink", permissions: [Permissions.EvolumiAdmin] }
            );

            this.links.Sales.push(
                { path: "/CRM/Dashboard", icon: "tachometer", text: "DASHBOARD", id: "DashboardLink" },
                { path: "/CRM/Dashboard", icon: "tachometer", text: "DASHBOARD", id: "DashboardLink" },
                { path: "/CRM/Dashboard", icon: "tachometer", text: "DASHBOARD", id: "DashboardLink" }
            )
        }
    }

    showSettingMenu() {
        this.show = !this.show;
    }
    hideSettingMenu(event: any) {
        event = event.target.id;
        this.userMenu = false;
        if (event !== 'headerBlackImage') {
            this.show = false;
        }
    }

    showLanguageMenu() {
        this.show = false;
        this.languageActive = true;
    }
    closeLanguageMenu(event: any) {
        if (event && event.target.id === 'language' || event.target.id === "close1") {
            this.languageActive = false;
        } else if (!event && event.target.id === 'language') {
            this.languageActive = false;
        } else {
            return;
        }
    }

    showmenu() {
        this.userMenu = !this.userMenu;
    }

    openSideMenu() {
        this.menuActive = true;
    }
    closeSideMenu() {
        this.menuActive = false;
    }

    openHelp() {
        window.open('http://guide.evolumi.com', '_blank');
    }

    changeLanguage(selectedLanguage: string) {
        this.auth.setLanguage(selectedLanguage);

        this.languageActive = false;

        this._http.post('Users/Language/' + selectedLanguage, null)
            .map(res => res.json())
            .subscribe(res => {
                this._store.set('lan', selectedLanguage);
                console.log("[Language] Update: Success!");
                this._notify.success(this._translate.instant("LANGUAGETRANSLATEDTO"));
            });
    }
    logOff() {
        this.auth.Logout();
    }

    ngOnDestroy() {
        if (this.userSub) this.userSub.unsubscribe();
        if (this.orgSub) this.orgSub.unsubscribe();
        if (this.buttonSub) this.buttonSub.unsubscribe();
        this.button.removeButton("chat");
    }
}