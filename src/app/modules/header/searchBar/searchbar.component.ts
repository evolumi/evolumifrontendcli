import { Component, HostListener } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';

import { DealsModel } from '../../../models/deals.models';
import { ActionsModel } from '../../../models/actions.models';
import { AccountsModel } from '../../../models/account.models';
import { ContactsCollectionModel } from '../../../models/contact.models';

import { Api } from '../../../services/api';
import { LocalStorage } from '../../../services/localstorage';
import { ActionsCollection } from '../../../services/actionservice';
import { DealAddEdit } from "../../../services/dealAddEdit.service";
import { Subscription } from 'rxjs/Subscription';
import { ButtonPanelService } from '../../../services/button-panel.service';
import { Shortcuts } from '../../../services/configuration';

@Component({
    selector: 'searchbar',
    templateUrl: './searchbar.html',
    styleUrls: ['searchbar.css']
})

export class SearchBarComponent {
    private searchResults: Array<any> = [];

    public Deals: DealsModel[];
    public Actions: ActionsModel[];
    public Accounts: AccountsModel[];
    public Contacts: ContactsCollectionModel[];
    public searchText: string = '';
    public term = new FormControl();
    public outPut: Array<any> = [];
    public orgId: string = '';
    public showing: boolean = false;
    public searching: number = 0;

    private termSub: Subscription;
    private buttonSub: Subscription;
    private firstClick = true;

    private selectedIndex = 0;

    constructor(public _http: Api, private router: Router, public _store: LocalStorage, public actionService: ActionsCollection, private _dealAddEdit: DealAddEdit, private buttons: ButtonPanelService) {
        this.Deals = [];
        this.Actions = [];
        this.Accounts = [];
        this.Contacts = [];
        this.termSub = this.term.valueChanges
            .debounceTime(400)
            .subscribe(term => this.search(term));
        this.buttons.addHeaderButton("SEARCH", "search", "search", Shortcuts.search);
    }
    ngOnInit() {
        this.showing = false;
        this.buttonSub = this.buttons.commandObservable().subscribe(command => {
            if (command == "search") {
                this.showSearchBox();
            }
        })
    }

    showSearchBox() {
        this.showing = true;
        window.setTimeout(function () {
            document.getElementById("search_input").focus();
        }, 0);
    }

    @HostListener('window:keydown', ['$event'])
    keyDown(event: KeyboardEvent) {
        if (event.keyCode == 40) {
            this.selectedIndex++;
            if (this.selectedIndex > this.searchResults.length) {
                this.selectedIndex = this.searchResults.length;
            }
        }
        if (event.keyCode == 38) {
            this.selectedIndex--;
            if (this.selectedIndex <= 0) {
                this.selectedIndex = 1;
            }
        }
        if (event.keyCode == 13) {
            let result = this.searchResults.find(x => x.index == this.selectedIndex);
            this.click(result);
        }
    }

    public click(result: any) {
        if (result) {
            this.hideSearchBox();
            if (result.Type == "Deal")
                this._dealAddEdit.getDealById(result.Id);
            if (result.Type == "Action")
                this.actionService.showGroupByActionId(result.Id);
            if (result.Type == "Account")
                this.router.navigate(["CRM/Accounts/", result.Id]);

        }
    }

    search(term: string) {
        this.orgId = this._store.get('orgId');
        this.searching += 3;

        this.searchResults = [];
        this.Actions = [];
        this.Deals = [];
        this.Accounts = [];

        this._http.get('Search/Accounts?searchString=' + term).map(res => res.json())
            .subscribe(res => {
                this.searching--;
                this.Accounts = [...res.Data];
                this.sortResults();
            }, err => {
                this.searching--;
                console.log("[Search] Error getting accounts", err);
            });

        this._http.get('Search/Deals?searchString=' + term).map(res => res.json())
            .subscribe(res => {
                this.searching--;
                this.Deals = [...res.Data];
                this.sortResults();
            }, err => {
                this.searching--;
                console.log("[Search] Error getting deals", err);
            });

        this._http.get('Search/Actions?searchString=' + term).map(res => res.json())
            .subscribe(res => {
                this.searching--;
                for (let action of res.Data) {
                    action.Note = this.getActionIcon(action.Note);
                    this.Actions.push(action);
                }
                this.sortResults();
            }, err => {
                this.searching--;
                console.log("[Search] Error getting actions", err);
            });

        // this._http.get('Search/Contacts?searchString=' + term).map(res => res.json())
        //     .subscribe(res => {
        //         this.searching--;
        //         this.Contacts = [...res.Data];
        //         this.addResults(this.Contacts);
        //     });

        // if (this.orgId)
        //     this._http.get('Search?searchString=' + term)
        //         .map(res => res.json())
        //         .subscribe(res => {
        //             this.searchResults = [];
        //             this.Deals = [];
        //             this.Actions = [];
        //             this.Contacts = [];
        //             this.Accounts = [];
        //             this.outPut = [];
        //             this.outPut = res.Data;
        //             this.searching--;
        //             let index = 1;
        //             for (let a of this.outPut) {
        //                 a.index = index;
        //                 index++;
        //                 this.searchResults.push(a);
        //                 if (a.Type === 'Deal') {
        //                     this.Deals.push(a);
        //                 } else if (a.Type === 'Action') {
        //                     a.Note = this.getActionIcon(a.Note);
        //                     this.Actions.push(a);
        //                 } else if (a.Type === 'Account') {
        //                     this.Accounts.push(a);
        //                 } else if (a.Type === 'Contact') {
        //                     this.Contacts.push(a);
        //                 }
        //             }
        //         })
    }

    private sortResults() {
        let index = 1
        this.searchResults = [];
        for (let res of this.Accounts) {
            res.index = index;
            this.searchResults.push(res);
            index++;
        }
        for (let res of this.Deals) {
            res.index = index;
            this.searchResults.push(res);
            index++;
        }
        for (let res of this.Actions) {
            res.index = index;
            this.searchResults.push(res);
            index++;
        }
    }



    getActionIcon(type: string): string {
        switch (type) {
            case 'Email': return "envelope-o";
            case 'Newsletter': return "envelope-o";
            case 'Phone': return "phone";
            case 'Meeting': return "clock-o";
            case 'Events': return "calendar-o";
            case 'Other': return "tree";

        }
        return "";
    }

    hideSearchBox() {

        if (this.firstClick && this.showing) {
            this.firstClick = false;
            return;
        }

        this.Deals = [];
        this.Actions = [];
        this.Contacts = [];
        this.Accounts = [];
        this.outPut = [];
        this.searchText = '';
        this.showing = false;
        this.firstClick = true;
    }

    ngOnDestroy() {
        if (this.buttonSub) this.buttonSub.unsubscribe();
        if (this.termSub) this.termSub.unsubscribe();
        this.buttons.removeButton("search");
    }
}

