import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';

// Components
import { SearchBarComponent } from './searchBar/searchbar.component';
import { HeaderBlack } from './headerblack/headerblack.component';
import { HeaderWhite } from './headerwhite/headerwhite.component';
import { UserSettingsComponent } from './personal-settings/modals/usersettings.component';
import { SponsorAds } from './sponsorAds/sponsorAds.component';

@NgModule({
    imports: [
        SharedModule
    ],
    declarations: [
        UserSettingsComponent,
        SearchBarComponent,
        HeaderBlack,
        HeaderWhite,
        SponsorAds,
    ],
    exports: [
        SponsorAds,
        HeaderBlack,
        HeaderWhite
    ]
})
export class HeaderModule { }