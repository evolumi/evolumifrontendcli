export const environment = {
    production: false,
    apiUrl: 'http://evolumidev.azurewebsites.net',
    intercomAppId: 'zfbzudgl',
    useSinchTestNumber: false,
    sinchApplicationKey: '6a5d904e-30d8-4a6d-a00c-c85eaa3576ee',
    customerSuccessOrg: "dfe4d4d3-ac7d-406d-a0a5-f3be5d5132bf",
    lastActiveCustomField: 'unknown',
    sinchTestNumber: "+46000000000",
    faviconUrl: "assets/favicon/favicon-TEST.png"
};