export const environment = {
  production: true,
  apiUrl: "https://api.evolumi.se",
  intercomAppId: 'vw8751x3',
  useSinchTestNumber: false,
  sinchApplicationKey: 'ee822eee-6e0b-48db-9512-27a5bb1351ee',
  customerSuccessOrg: "f43f32fa-7427-4321-848b-02f7bf277e96",
  lastActiveCustomField: 'f2b2a1a7-d695-4c82-89f4-58204a6631ed',
  sinchTestNumber: "+46000000000",
  faviconUrl: "assets/favicon/favicon-PROD.png"
};
