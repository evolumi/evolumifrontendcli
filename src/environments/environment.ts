// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  apiUrl: "http://localhost:30409",
  intercomAppId: 'zfbzudgl',
  useSinchTestNumber: false,
  sinchApplicationKey: '6a5d904e-30d8-4a6d-a00c-c85eaa3576ee',
  customerSuccessOrg: "dfe4d4d3-ac7d-406d-a0a5-f3be5d5132bf",
  lastActiveCustomField: 'unknown',
  sinchTestNumber: "+46000000000",
  faviconUrl: "assets/favicon/favicon-DEV.png"
};