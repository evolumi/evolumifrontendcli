(function($)
{
	$.Redactor.prototype.fontsize = function()
	{
		return {
			init: function()
			{
				var fonts = [10, 12, 14, 16, 18, 20, 22, 24, 28, 32, 40, 48, 64, 72];
				var that = this;
				var dropdown = {};

				$.each(fonts, function(i, s)
				{
					dropdown['s' + i] = { title: s + 'px', func: function() { that.fontsize.set(s); } };
				});

				dropdown.remove = { title: 'Remove Font Size', func: that.fontsize.reset };

				var button = this.button.add('fontsize', 'Size');
				this.button.setIcon(button, '<i class="fa fa-plus"></i>');
				this.button.addDropdown(button, dropdown);
			},
			set: function(size)
			{
				// this.inline.format('span', 'style', 'font-size: ' + size + 'px;', 'remove');
				this.inline.format('span', 'style', 'font-size: ' + size + 'px;');
			},
			reset: function()
			{
				this.inline.removeStyleRule('font-size');
			}
		};
	};
})(jQuery);